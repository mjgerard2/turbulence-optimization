# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 12:20:39 2021

@author: micha
"""

import numpy as np
import h5py as hf
from sklearn.linear_model import LinearRegression

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import vmecTools.wout_files.coord_convert as cc
import vmecTools.wout_files.curveB_tools as cBT
import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.read_boozmn as rb

import vtkTools.vtk_grids as vg
import directory_dict as dd
import plot_define as pd

class trapped_resonance():
    """ A class object used to calculated the TEM stability condition for
    trapped particles.

    ...

    Attribute
    ---------
    geomDir : str
        Directory path where wout file is stored
    geomFile : str
        File name for wout file
    r_init : arr
        Initial position of particle (s, u, v)
    Vmod : float
        Velocity magnitude
    pitch : float
        Pitch angle of particle
    spec : str, optional
        Particle type, options are 'electron' or 'ion'.  Default is 'electron'.

    Method
    ------

    """
    def __init__(self, geomDir, geomName, r_init, Vmod, pitch, spec='electron', bounce_num=10):
        ### Instantiate WOUT Object ###
        wout = wr.readWout( geomDir, name=geomName, diffAmps=True, curvAmps=True, linApprox=True )
        wout.transForm_1D( r_init[0], r_init[1], r_init[2], ['Bmod'] )
        self.geomPath = os.path.join(geomDir, geomName)

        ### Calculate Perpendicular/Parallel Velcoity ###
        Vperp = Vmod * np.sin(pitch)
        Vpar = Vmod * np.cos(pitch)
        moment = ( 0.5 * Vperp * Vperp ) / wout.invFourAmps['Bmod']

        ### Define Parameters ###
        ampKeys = ['B_u','B_v','Bs','Bu','Bv','Bmod','dBmod_ds','dBmod_du','dBmod_dv','dBs_du','dBs_dv','dBu_ds','dBu_dv','dBv_ds','dBv_du','R','Jacobian']
        self.params = { 'wout' : wout,
                        'ampKeys' : ampKeys,
                        'mu' : moment,
                        'pitch' : pitch,
                        'velocity' : Vmod,
                        'species' : spec,
                        'bounce_num' : bounce_num+1 }

        ### Specify Plasma Species ###
        if spec=='electron':
            self.params['mass'] = 9.1094e-31
            self.params['charge inv'] = -1.6022e19
        elif spec=='ion':
            self.params['mass'] = 1.672621e-27
            self.params['charge inv'] = 1.6022e19
        elif spec=='test charge':
            self.params['mass'] = 1.672621e-27
            self.params['charge inv'] = -1.6022e19

        ### Define Initial Conditon and Time Domain ###
        self.y_init = np.r_[ r_init, Vpar ]

    def calc_guiding_force(self, y, t, params):
        """ Calculate the guiding force and velocity components of paticle in
        a toroidal coordinates system.

        Parameters
        ----------
        y : arr
            Particla phase state (s, u, v, V_parallel).
        t : str
            Time of particle phase.
        params : dict
            Parameters needed for caculation.
                'wout' : wout file object
                'ampKes' : values that will be Fourier transformed
                'charge_inv' : one divided by particle charge
                'mass' : particle mass
                'mu' : particle mangnetic moment

        Returns
        -------
        Arr
            Phase state derivatives (ds/dt, du/dt, dv/dt, dV_parallel/dt.

        """
        ### Calculate WOUT Transforms ###
        params['wout'].linApprox_1D( y[0], y[1], y[2], params['ampKeys'] )

        ### Jacobian ###
        jacob = params['wout'].invFourAmps['Jacobian']
        jacob_inv = 1. / jacob

        ### B-field Magniturde ###
        Bmod = params['wout'].invFourAmps['Bmod']
        Bmod_inv = 1. / Bmod

        ### B-field Covariant Vector ###
        Bs = params['wout'].invFourAmps['Bs']
        Bu = params['wout'].invFourAmps['Bu']
        Bv = params['wout'].invFourAmps['Bv']

        ### B-field Contravariant Vector ###
        B_u = params['wout'].invFourAmps['B_u']
        B_v = params['wout'].invFourAmps['B_v']

        ### Gradient of B-field Magniturde ###
        dB_ds = params['wout'].invFourAmps['dBmod_ds']
        dB_du = params['wout'].invFourAmps['dBmod_du']
        dB_dv = params['wout'].invFourAmps['dBmod_dv']

        ### B_{s} Field Derivatives ###
        dBs_du = params['wout'].invFourAmps['dBs_du']
        dBs_dv = params['wout'].invFourAmps['dBs_dv']

        ### B_{u} Field Derivatives ###
        dBu_ds = params['wout'].invFourAmps['dBu_ds']
        dBu_dv = params['wout'].invFourAmps['dBu_dv']

        ### B_{v} Field Derivatives ###
        dBv_ds = params['wout'].invFourAmps['dBv_ds']
        dBv_du = params['wout'].invFourAmps['dBv_du']

        ### Coefficients in ODEs ###
        v_par_sqrd = y[3] * y[3]
        coeff = params['charge inv'] * params['mass'] * Bmod_inv * jacob_inv
        coeff_mu = params['mu'] * Bmod_inv
        coeff_v_B = y[3] * Bmod_inv
        coeff_vv_B = v_par_sqrd * Bmod_inv
        coeff_vv_BB = v_par_sqrd * Bmod_inv * Bmod_inv

        ### ODE Calculations ###
        ds_dt = coeff * ( coeff_mu * ( Bu * dB_dv - Bv * dB_du ) + coeff_vv_B * ( dBv_du - dBu_dv ) + coeff_vv_BB * ( Bu * dB_dv - Bv * dB_du ) )
        dv_dt = coeff_v_B * B_v + coeff * ( coeff_mu * ( Bs * dB_du - Bu * dB_ds ) + coeff_vv_B * ( dBu_ds - dBs_du ) + coeff_vv_BB * ( Bs * dB_du - Bu * dB_ds ) )
        du_dt = coeff_v_B * B_u + coeff * ( coeff_mu * ( Bv * dB_ds - Bs * dB_dv ) + coeff_vv_B * ( dBs_dv - dBv_ds ) + coeff_vv_BB * ( Bv * dB_ds - Bs * dB_dv ) )
        accel = - coeff_mu * ( ( B_u * dB_du + B_v * dB_dv ) + y[3] * coeff * ( dB_ds * ( dBv_du - dBu_dv ) + dB_du * ( dBs_dv - dBv_ds ) + dB_dv * ( dBu_ds - dBs_du) ) )

        return np.array([ ds_dt, du_dt, dv_dt, accel ])

    def RG_integrate(self, tstp):
        """ Fourth order Runge-Kutta integration scheme.

        Parameters
        ----------
        tstp : float
            Time step in integrator.

        Raises
        ------
        ValueError
            Particle is not trapped and will be ignored.
        """
        stp_const = tstp / 6
        Dtor_thresh = 2*np.pi

        Vpar_sgn_prev = np.sign(self.y_init[3])
        phase = self.y_init

        phase_set = []
        time_set = []

        Dtor = 0
        tend = 0
        cnt = 0
        while cnt < self.params['bounce_num'] and np.abs(Dtor) < Dtor_thresh:
            k1 = self.calc_guiding_force(phase, tend, self.params)

            phase_2 = phase + 0.5 * tstp * k1
            k2 = self.calc_guiding_force(phase_2, tend, self.params)

            phase_3 = phase + 0.5 * tstp * k2
            k3 = self.calc_guiding_force(phase_3, tend, self.params)

            phase_4 = phase + tstp * k3
            k4 = self.calc_guiding_force(phase_4, tend, self.params)

            Dphase = stp_const * ( k1 + 2 * ( k2 + k3 ) + k4 )

            Dtor = Dtor + Dphase[2]
            phase = phase + Dphase

            if np.sign(phase[3]) != Vpar_sgn_prev:
                Vpar_sgn_prev = np.sign(phase[3])
                cnt+=1

            if cnt>=1 and cnt < self.params['bounce_num']:
                phase_set.append(phase)
                time_set.append(tend)
                tend+=tstp

        if Dtor < Dtor_thresh:
            self.t_dom = np.array(time_set)
            self.data_out = np.array(phase_set)

        else:
            raise ValueError('----Untrapped Particle----')

    def calc_drift_freq(self, k_psi=0, k_alf=1e-2, temp=1.6022e-16, plot_chk=False):
        """ Calculate the magnetic drift frequency along the particle bounce
        trajectory.

        Parameters
        ----------
        k_psi : float, optional
            Wave number in the Grad(\psi) direction. The default is 0.
        k_alf : float, optional
            Wave number in the gRad(\alpha) direction. The default is 1e-2.
        plot_chk : bool, optional
            Set to True if you want to plot the drift frequency. The default
            is False.

        Returns
        -------
        bounce_avg : float
            The integrated average magnetic drift frequency calculated along a
            trapped particle bounce trajectory.
        """
        ### Perform WOUT Fourier Transforms ###
        ampKeys = ['R','Z','Jacobian','dR_ds','dR_du','dR_dv','dZ_ds','dZ_du','dZ_dv','Bmod','dBmod_ds','dBmod_du','dBmod_dv','Bs','Bu','Bv','B_u','B_v','dBs_du','dBs_dv','dBu_ds','dBu_dv','dBv_ds','dBv_du']
        self.params['wout'].linear_flux_approx(self.data_out, ampKeys)

        ### Calculate Flux Surface Excursion from Initial Condition ###
        t_dom = self.t_dom.reshape(self.t_dom.shape[0], 1) / (self.t_dom[-1] * (self.params['bounce_num'] - 1))
        data_out = self.data_out[:,0].reshape(self.t_dom.shape[0], 1)

        model = LinearRegression().fit(t_dom, data_out)
        ds = model.coef_[0][0]

        ### Calculate Drift Velocity ###
        nrg = 0.5 * self.params['mass'] * self.params['velocity'] * self.params['velocity']

        curB = cBT.BcurveTools( self.params['wout'] )
        curB.calc_curvature()
        curB.calc_drift_velocity( self.params['pitch'], nrg, mass=self.params['mass'], charge=( 1. / self.params['charge inv'] ) )
        v_drift_contra = curB.V_drift

        ### Calculate Differential Path Segment and Total Path Length ###
        R = self.params['wout'].invFourAmps['R']
        z = self.params['wout'].invFourAmps['Z']
        t = self.data_out[:,2]

        x = R * np.cos( t )
        y = R * np.sin( t )
        r_vec = np.stack( ( x, y, z ), axis=1 )

        dr_vec = r_vec[1:] - r_vec[0:-1]
        dr = np.insert(np.linalg.norm( dr_vec, axis=1 ), 0, 0)
        leng = np.sum(dr)

        ### Compute Metric ###
        npts = self.data_out.shape[0]
        Gss, Gsu, Gsv, Guu, Guv, Gvv, G_ss, G_su, G_sv, G_uu, G_uv, G_vv = cc.compute_metric( self.params['wout'] )

        ### Compute B-field Magnitude ###
        Bmod = self.params['wout'].invFourAmps['Bmod']
        Bmod_inv = 1. / Bmod

        ### Normalized B-field - covariant ###
        bs = ( self.params['wout'].invFourAmps['Bs'] ) * Bmod_inv
        bu = ( self.params['wout'].invFourAmps['Bu'] ) * Bmod_inv
        bv = ( self.params['wout'].invFourAmps['Bv'] ) * Bmod_inv

        ### Normalized B-field - contravariant ###
        b_s = G_ss * bs + G_su * bu + G_sv * bv
        b_u = G_su * bs + G_uu * bu + G_uv * bv
        b_v = G_sv * bs + G_uv * bu + G_vv * bv

        ### Compute Jacobian ###
        jacob = self.params['wout'].invFourAmps['Jacobian']
        root_gss_inv = 1. / np.sqrt( G_ss )

        ### Gradient of Psi - covariant ###
        Grad_psi_covar_s = root_gss_inv * ( np.ones(npts) - b_s * bs )
        Grad_psi_covar_u = - root_gss_inv * b_s * bu
        Grad_psi_covar_v = - root_gss_inv * b_s * bv
        Grad_psi_covar = np.stack( ( Grad_psi_covar_s, Grad_psi_covar_u, Grad_psi_covar_v ), axis=1 )

        ### Gradient of Psi - contravariant ###
        Grad_psi_contra = np.empty( Grad_psi_covar.shape )
        Grad_psi_contra[:,0] = G_ss * Grad_psi_covar[:,0] + G_su * Grad_psi_covar[:,1] + G_sv * Grad_psi_covar[:,2]
        Grad_psi_contra[:,1] = G_su * Grad_psi_covar[:,0] + G_uu * Grad_psi_covar[:,1] + G_uv * Grad_psi_covar[:,2]
        Grad_psi_contra[:,2] = G_sv * Grad_psi_covar[:,0] + G_uv * Grad_psi_covar[:,1] + G_vv * Grad_psi_covar[:,2]

        ### Gradient of Psi - normalized ###
        Grad_psi_norm_inv = 1. / np.sum( Grad_psi_covar * Grad_psi_contra, axis=1 )
        Grad_psi_norm_inv = np.stack( ( Grad_psi_norm_inv, Grad_psi_norm_inv, Grad_psi_norm_inv ), axis=1 )

        Grad_psi_covar = Grad_psi_norm_inv * Grad_psi_covar
        Grad_psi_contra = Grad_psi_norm_inv * Grad_psi_contra

        ### Gradient of Alpha - covariant ###
        Grad_alf_covar_s = jacob * ( b_u * Grad_psi_contra[:,2] - b_v * Grad_psi_contra[:,1] )
        Grad_alf_covar_u = jacob * ( b_v * Grad_psi_contra[:,0] - b_s * Grad_psi_contra[:,2] )
        Grad_alf_covar_v = jacob * ( b_s * Grad_psi_contra[:,1] - b_u * Grad_psi_contra[:,0] )
        Grad_alf_covar = np.stack( ( Grad_alf_covar_s, Grad_alf_covar_u, Grad_alf_covar_v ), axis=1 )

        ### Gradient of Alpha - contravariant ###
        Grad_alf_contra = np.empty( Grad_alf_covar.shape )
        Grad_alf_contra[:,0] = G_ss * Grad_alf_covar[:,0] + G_su * Grad_alf_covar[:,1] + G_sv * Grad_alf_covar[:,2]
        Grad_alf_contra[:,1] = G_su * Grad_alf_covar[:,0] + G_uu * Grad_alf_covar[:,1] + G_uv * Grad_alf_covar[:,2]
        Grad_alf_contra[:,2] = G_sv * Grad_alf_covar[:,0] + G_uv * Grad_alf_covar[:,1] + G_vv * Grad_alf_covar[:,2]

        ### Gradient of Psi - normalized ###
        Grad_alf_norm_inv = 1. / np.sum( Grad_alf_covar * Grad_alf_contra, axis=1 )
        Grad_alf_norm_inv = np.stack( ( Grad_alf_norm_inv, Grad_alf_norm_inv, Grad_alf_norm_inv ), axis=1 )

        Grad_alf_covar = Grad_alf_norm_inv * Grad_alf_covar
        Grad_alf_contra = Grad_alf_norm_inv * Grad_alf_contra

        ### Drift Wave Frequency ###
        k_perp_covar = k_psi * Grad_psi_covar + k_alf * Grad_alf_covar
        omega_drift = np.sum( k_perp_covar * v_drift_contra, axis=1 )

        ### Energy Correction Term ###
        nrg_term = 0 # -1 * self.params['charge inv'] * k_alf * (nrg - 0.5 * temp)

        dr_track = np.empty(dr.shape)
        for idx in range( dr.shape[0] ):
            dr_track[idx] = np.sum( dr[0:idx+1] )

        bounce_avg = np.trapz(omega_drift, dr_track) / leng

        if plot_chk:
            #plot = pd.plot_define()
            plot_chk.plt.plot( dr_track, omega_drift, c='k' )
            plot_chk.plt.plot( dr_track, [bounce_avg] * self.t_dom.shape[0], c='k',
                           ls='--', label=r'$\langle \omega_{de} \rangle$ = '+'{0:0.3f}'.format(bounce_avg) )

            plot_chk.plt.plot( dr_track, [nrg_term] * self.t_dom.shape[0], c='tab:blue',
                           ls='--', label=r'$\omega_{*e}^T$ = '+'{0:0.3f}'.format(nrg_term) )

            bounce_shft = bounce_avg - nrg_term
            plot_chk.plt.plot( dr_track, [bounce_shft] * self.t_dom.shape[0], c='tab:red',
                           ls='--', label=r'$\langle \bar{\omega}_{de} - \omega_{*e}^T \rangle$ = '+'{0:0.3f}'.format(bounce_shft) )

            plot_chk.plt.xlabel('Length [m]')
            plot_chk.plt.ylabel(r'$\omega_{de}$')

            #plot.plt.title( r'$\angle \bar{\omega}_{de} \rangle$ = '+'{0:0.3f}'.format(bounce_avg) )
            plot_chk.plt.grid()
            plot_chk.plt.legend(loc='lower right')
            # plot.plt.show()

        return bounce_avg, leng, ds

    def plot_nrg(self, plot):
        """ Plot the energy along a particle bounce trajectory.

        Parameters
        ----------
        plot : Obj
            Matplotlib plotting object.
        """
        nrg_tot = 0.5 * self.params['mass'] * self.params['velocity'] * self.params['velocity']

        points_in = np.stack( ( self.data_out[:,0], self.data_out[:,1], self.data_out[:,2] ), axis=1 )
        self.params['wout'].transForm_listPoints( points_in, ['Bmod'] )

        nrg_perp = self.params['wout'].invFourAmps['Bmod'] * self.params['mu'] * self.params['mass']
        nrg_par = 0.5 * self.params['mass'] * self.data_out[:,3] * self.data_out[:,3]

        plot.plt.plot( self.t_dom * 1e6, nrg_perp / nrg_tot, label=r'$E_{\perp}$' )
        plot.plt.plot( self.t_dom * 1e6, nrg_par / nrg_tot, label=r'$E_{\parallel}$' )

        plot.plt.plot( self.t_dom * 1e6, ( nrg_par + nrg_perp ) / nrg_tot, c='k', label=r'$E_{total}$' )
        plot.plt.plot( self.t_dom * 1e6, [0] * self.t_dom.shape[0], ls='--', c='k' )

        plot.plt.xlabel(r'$\mu s$')
        plot.plt.ylabel(r'$E_x/E_{tot}$')

        plot.plt.legend()
        plot.plt.grid()

    def plot_vmec_coords(self, plot):
        """ Plot the VMEC {s,u,v} coordinates along particle trajectory

        Parameters
        ----------
        plot : object
            Matplotlib axis on which the plot will be generated.
        """
        ax1 = plot.ax
        ax2 = ax1.twinx()

        plt1, = ax1.plot(self.t_dom * 1e6, self.data_out[0:,0], c='tab:red', label=r'$\psi_N$' )
        plt2, = ax2.plot( self.t_dom * 1e6, self.data_out[:,1] / np.pi, c='tab:blue', ls='--', label='pol' )
        ax2.plot( self.t_dom * 1e6, self.data_out[:,2] / np.pi, c='tab:blue', label='tor' )

        ax1.spines['right'].set_color(plt1.get_color())
        ax1.tick_params(axis='y', colors=plt1.get_color())
        ax1.set_ylabel(r'$\psi_{n}$', c=plt1.get_color() )

        ax2.spines['right'].set_color(plt2.get_color())
        ax2.tick_params(axis='y', colors=plt2.get_color())
        ax2.set_ylabel(r'$[\pi]$', c=plt2.get_color() )

        ax1.set_xlabel(r'$\mu s$', c='k')

        plot.plt.grid()

        box = ax1.get_position()
        ax1.set_position([box.x0, box.y0, box.width * .9, box.height])
        ax2.legend( loc='upper left', bbox_to_anchor=(1.15, 1) )

    def plot_Vpar(self, plot):
        """ Plot the parallel velocity over the simulation time domain.

        Parameters
        ----------
        plot : object
            Matplotlib axis on which the plot will be generated.
        """
        plot.plt.plot( self.t_dom * 1e6, self.data_out[0:,3] *1e-3, c='k' )

        plot.plt.xlabel(r'$\mu s$')
        plot.plt.ylabel(r'$v_{\parallel}$ [km/s]')

        plot.plt.grid()

    def save_trajectory(self, savePath):
        """ Save the trajcetory data, time domain, integration parameters and initial conditions

        Parameters
        ----------
        savePath : str
            Absolute path to save file.
        """
        with hf.File(savePath, 'w') as hf_:
            hf_.create_dataset('Geometry Path', data=self.geomPath)
            for key in self.params:
                if key != 'wout' and key != 'ampKeys':
                    hf_.create_dataset(key, data=self.params[key])

            hf_.create_dataset('trajectory', data=self.data_out)
            hf_.create_dataset('time domain', data=self.t_dom)
            hf_.create_dataset('initial conditions', data=self.y_init)

    def make_VTK(self, savePath, saveName):
        """ Generate a vtk file of the particle trajectory.

        Parameters
        ----------
        savePath : str
            Directory where vtk file will be stored.
        saveName : str
            Name of vtk file.
        """
        self.params['wout'].transForm_listPoints(self.data_out[0:,0:3], ['R','Z'])

        r_traj = self.params['wout'].invFourAmps['R']
        z_traj = self.params['wout'].invFourAmps['Z']

        x_traj = r_traj * np.cos(self.data_out[0:,2])
        y_traj = r_traj * np.sin(self.data_out[0:,2])

        cart_grid = np.stack((x_traj, y_traj, z_traj), axis=1)
        vg.scalar_line_mesh(savePath, saveName, cart_grid, self.data_out[0:,3]*1e-3)

if __name__ == '__main__':
    geomDir = os.path.join('/mnt','HSX_Database','HSX_Configs','main_coil_0','set_1','job_0')
    geomFile = 'wout_HSX_main_opt0.nc'

    #m = 9.1094e-31
    m = 1.6726e-27
    k = 1.3807e-23
    temp_eV = 1e3
    T = temp_eV * 11604
    alf = np.sqrt( m / ( 2 * k * T ) )

    if True:
        ### Save Trajectory Data ###
        Vmod = np.sqrt( ( 2 * 100 * 1.6022e-19 ) / m )

        s_pnt = 0.25
        u_pnt = -0.55 * np.pi # -0.05 * np.pi
        v_pnt = 0.18 * np.pi # -0.05 * np.pi
        
        pol_dom = np.linspace(0, 2*np.pi, 161)
        tor_dom = np.linspace(0, 2*np.pi, 161 * 4)

        wout = wr.readWout(geomDir, name=geomFile)
        wout.transForm_2D_sSec(s_pnt, pol_dom, tor_dom, ['Bmod'])
        Bmax = np.max( wout.invFourAmps['Bmod'] )

        wout.transForm_1D(s_pnt, u_pnt, v_pnt, ['Bmod'])
        Bo = wout.invFourAmps['Bmod']
        print(Bo)
        if Bo <= 1.05:
            pitch_thresh = np.arctan( np.sqrt( Bo / ( Bmax - Bo ) ) )
            pitch_dom = np.linspace(pitch_thresh, 0.5 * np.pi, 15)
            pitch = pitch_dom[1]

            pos_init = np.array([ s_pnt, u_pnt, v_pnt ])
            t_res = trapped_resonance(geomDir, geomFile, pos_init, Vmod, pitch, spec='test charge', bounce_num=17)
            try:
                t_res.RG_integrate(1e-7)
                savePath = os.path.join('/home','michael','Desktop','trajData_stable.h5')
                t_res.save_trajectory(savePath)

            except ValueError:
                print('----Untrapped----\n')

    if False:
        ### Test Single Randomized Particle ###
        s_pnt = 0.25

        upts = 161
        sym = 4
        pol_dom = np.linspace(0, 2*np.pi, upts)
        tor_dom = np.linspace(0, 2*np.pi, upts * sym)

        wout = wr.readWout(geomDir, name=geomFile)
        wout.transForm_2D_sSec(s_pnt, pol_dom, tor_dom, ['Bmod'])
        Bmax = np.max( wout.invFourAmps['Bmod'] )

        Vmod_pop = np.linspace( 0, 1e8, int(1e6)+1 )
        Vmod_PDF = Vmod_pop * Vmod_pop * np.exp( - ( Vmod_pop * alf)**2 )
        Vmod_PDF = Vmod_PDF / np.sum( Vmod_PDF )

        u_pnt = 2 * np.pi * np.random.random()
        v_pnt = 2 * np.pi * np.random.random()

        wout.transForm_1D(s_pnt, u_pnt, v_pnt, ['Bmod'])
        Bo = wout.invFourAmps['Bmod']

        pos_init = np.array([ s_pnt, u_pnt, v_pnt ])
        pitch_thresh = np.arctan( np.sqrt( Bo / ( Bmax - Bo ) ) )

        pitch_pop = np.linspace( pitch_thresh, 0.5 * np.pi, 100000 )
        pitch_PDF = np.sin( pitch_pop )
        pitch_PDF = pitch_PDF / np.sum( pitch_PDF )
        pitch = np.random.choice( pitch_pop, p=pitch_PDF )

        Vmod = np.random.choice( Vmod_pop, p=Vmod_PDF )

        t_res = trapped_resonance(geomDir, geomFile, pos_init, Vmod, pitch)
        try:
            t_res.RG_integrate(1e-8)
            
            ### Drift Frequency vs. Time ###
            if True:
                plot = pd.plot_define()
                t_res.calc_drift_freq(temp=k*T, plot_chk=plot)
                plot.plt.show()
            
            ### Energy vs. Time ###
            if False:
                plot = pd.plot_define()
                t_res.plot_nrg(plot)
                plot.plt.show()
    
            ### VMEC Coordinates vs. Time ###
            if False:
                plot = pd.plot_define()
                t_res.plot_vmec_coords(plot)
                plot.plt.show()

        except ValueError:
            print('----Untrapped----\n')

    if False:
        ### Generate Error Testing Plots ###
        savePath = os.path.join('/home','michael','Desktop')
        pitch = 0.485 * np.pi
        Vmod = np.sqrt( ( 2 * 100 * 1.6022e-19 ) / m )

        s_pnt = 0.25
        u_pnt = -0.05 * np.pi
        v_pnt = -0.05 * np.pi

        pos_init = np.array([ s_pnt, u_pnt, v_pnt ])
        t_res = trapped_resonance(geomDir, geomFile, pos_init, Vmod, pitch, spec='electron')
        try:
            t_res.RG_integrate(1e-7, bounce_num=50)

            ### Energy vs. Time ###
            if True:
                plot = pd.plot_define()
                t_res.plot_nrg(plot)
                plot.plt.show()
                plot.plt.tight_layout()
                savePlot_nrg = os.path.join(savePath, 'ion_traj_psiN_0p25_pitch_0p4_Ti_1eV_energy.png')
                plot.plt.savefig(savePlot_nrg)

            ### VMEC Coordinates vs. Time ###
            if True:
                plot = pd.plot_define()
                t_res.plot_vmec_coords(plot)
                plot.plt.show()
                plot.plt.tight_layout()
                savePlot_crd = os.path.join(savePath, 'ion_traj_psiN_0p25_pitch_0p4_Ti_1eV_coords.png')
                plot.plt.savefig(savePlot_crd)
            
            ### Drift Frequency vs. Time ###
            if True:
                plot = pd.plot_define()
                t_res.calc_drift_freq(k_psi=0, k_alf=1e-2, plot_chk=plot)
                plot.plt.tight_layout()
                savePlot_crd = os.path.join(savePath, 'ion_traj_psiN_0p25_pitch_0p4_Ti_10eV_omega.png')
                plot.plt.savefig(savePlot_crd)

        except ValueError:
            print('----Untrapped----\n')

