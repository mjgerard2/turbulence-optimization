# -*- coding: utf-8 -*-
"""
Created on Wed Jan 13 08:20:23 2021

@author: micha
"""

import numpy as np
import wout_read as wr
import coord_convert as cc

from scipy.integrate import odeint

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import directory_dict as dd
import plot_define as pd
import vtkTools.vtk_grids as vg

    
class guiding_center:
    
    def __init__(self, prams, pos_init):
        pitch = prams['pitch']
        vel = prams['velocity']
        
        vel_perp = vel * np.sin(pitch)
        vel_par = vel * np.cos(pitch)
        
        ampKeys = ['B_u','B_v','Bs','Bu','Bv','Bmod','dBmod_ds','dBmod_du','dBmod_dv','dBs_du','dBs_dv','dBu_ds','dBu_dv','dBv_ds','dBv_du','R','Jacobian']#,'dR_ds','dR_du','dR_dv','dZ_ds','dZ_du','dZ_dv','R','Jacobian']

        wout = wr.readWout( prams['geometry'], diffAmps=True, curvAmps=True )
        wout.transForm_1D( pos_init[0], pos_init[1], pos_init[2], ampKeys )
        
        moment = ( 0.5 * vel_perp * vel_perp ) / wout.invFourAmps['Bmod']
                
        self.y_init = np.r_[ pos_init, vel_par ]
        
        self.params = { 'wout' : wout,
                        'ampKeys' : ampKeys,
                        'charge inv' : 1. / prams['charge'],
                        'mass' : prams['mass'],
                        'mu' : moment,
                        'pitch' : pitch,
                        'velocity' : vel }
        
    def func(self, y, t, params):
        params['wout'].transForm_1D( y[0], y[1], y[2], params['ampKeys'] )
        
        jacob = params['wout'].invFourAmps['Jacobian']
        jacob_inv = 1. / jacob
        
        Bmod = params['wout'].invFourAmps['Bmod']
        Bmod_inv = 1. / Bmod
        
        Bs = params['wout'].invFourAmps['Bs']
        Bu = params['wout'].invFourAmps['Bu']
        Bv = params['wout'].invFourAmps['Bv']
        
        B_u = params['wout'].invFourAmps['B_u'] 
        B_v = params['wout'].invFourAmps['B_v'] 
        
        dB_ds = params['wout'].invFourAmps['dBmod_ds']
        dB_du = params['wout'].invFourAmps['dBmod_du']
        dB_dv = params['wout'].invFourAmps['dBmod_dv']
        
        dBs_du = params['wout'].invFourAmps['dBs_du']
        dBs_dv = params['wout'].invFourAmps['dBs_dv']
        
        dBu_ds = params['wout'].invFourAmps['dBu_ds']
        dBu_dv = params['wout'].invFourAmps['dBu_dv']
        
        dBv_ds = params['wout'].invFourAmps['dBv_ds']
        dBv_du = params['wout'].invFourAmps['dBv_du']
        
        v_par_sqrd = y[3] * y[3]
        coeff = params['charge inv'] * params['mass'] * Bmod_inv * jacob_inv
        coeff_mu = params['mu'] * Bmod_inv
        coeff_v_B = y[3] * Bmod_inv
        coeff_vv_B = v_par_sqrd * Bmod_inv
        coeff_vv_BB = v_par_sqrd * Bmod_inv * Bmod_inv
        
        ds_dt = coeff * ( coeff_mu * ( Bu * dB_dv - Bv * dB_du ) + coeff_vv_B * ( dBv_du - dBu_dv ) + coeff_vv_BB * ( Bu * dB_dv - Bv * dB_du ) )
        dv_dt = coeff_v_B * B_v + coeff * ( coeff_mu * ( Bs * dB_du - Bu * dB_ds ) + coeff_vv_B * ( dBu_ds - dBs_du ) + coeff_vv_BB * ( Bs * dB_du - Bu * dB_ds ) )
        du_dt = coeff_v_B * B_u + coeff * ( coeff_mu * ( Bv * dB_ds - Bs * dB_dv ) + coeff_vv_B * ( dBs_dv - dBv_ds ) + coeff_vv_BB * ( Bv * dB_ds - Bs * dB_dv ) )
        accel = - coeff_mu * ( ( B_u * dB_du + B_v * dB_dv ) + y[3] * coeff * ( dB_ds * ( dBv_du - dBu_dv ) + dB_du * ( dBs_dv - dBv_ds ) + dB_dv * ( dBu_ds - dBs_du) ) )
            
        return np.array([ ds_dt, du_dt, dv_dt, accel ])
        
    def integrate(self, t_dom):
        self.t_dom = t_dom
        self.data_out = odeint( self.func, self.y_init, self.t_dom, args=(self.params,) )
        
    def make_VTK(self, savePath, saveName):
        self.params['wout'].transForm_listPoints(self.data_out[0:,0:3], ['R','Z'])

        r_traj = self.params['wout'].invFourAmps['R']
        z_traj = self.params['wout'].invFourAmps['Z']
        x_traj = r_traj * np.cos(self.data_out[0:,2])
        y_traj = r_traj * np.sin(self.data_out[0:,2])
        cart_grid = np.stack((x_traj, y_traj, z_traj), axis=1)

        vg.scalar_line_mesh(savePath, saveName, cart_grid, self.data_out[0:,3]*1e-3)

    def plot_nrg(self, plot):
        nrg_tot = 0.5 * self.params['mass'] * self.params['velocity'] * self.params['velocity']

        points_in = np.stack( ( self.data_out[:,0], self.data_out[:,1], self.data_out[:,2] ), axis=1 )
        self.params['wout'].transForm_listPoints( points_in, ['Bmod'] )

        nrg_perp = self.params['wout'].invFourAmps['Bmod'] * self.params['mu'] * self.params['mass']
        nrg_par = 0.5 * self.params['mass'] * self.data_out[:,3] * self.data_out[:,3]

        plot.plt.plot( self.t_dom * 1e3, nrg_perp / nrg_tot, label=r'$E_{\perp}$' )
        plot.plt.plot( self.t_dom * 1e3, nrg_par / nrg_tot, label=r'$E_{\parallel}$' )

        plot.plt.plot( self.t_dom * 1e3, ( nrg_par + nrg_perp ) / nrg_tot, c='k', label=r'$E_{total}$' )
        plot.plt.plot( self.t_dom * 1e3, [0] * self.t_dom.shape[0], ls='--', c='k' )

        plot.plt.xlabel(r'$ms$')
        plot.plt.ylabel(r'$E_x/E_{tot}$')

        plot.plt.legend()
        plot.plt.grid()

    def is_trapped(self, thresh=1e-1):
        idx_beg = int( 0.25 * self.data_out.shape[0] )
        idx_end = int( 0.75 * self.data_out.shape[0] )
        v_idx = np.argmin( np.abs( self.data_out[idx_beg:idx_end, 3] ) )
        
        t_1 = self.t_dom[v_idx-1]
        t_2 = self.t_dom[v_idx]
        t_3 = self.t_dom[v_idx+1]
        
        y_1 = self.data_out[v_idx-1,3]**2
        y_2 = self.data_out[v_idx,3]**2
        y_3 = self.data_out[v_idx+1,3]**2
        
        mat = np.array([ [ t_1**2, t_1, 1 ],
                         [ t_2**2, t_2, 1 ],
                         [ t_3**2, t_3, 1 ] ])
        vec = np.array([ y_1, y_2, y_3 ])
        prams = np.matmul( np.linalg.inv(mat), vec )
        y_min = prams[2] - 0.25 * ( prams[1]**2 / prams[0] )
        
        if y_min < thresh:
            return True
        else:
            return False
    
    def plot_sVal(self, plot):
        plot.plt.plot(self.t_dom * 1e3, self.data_out[0:,0])

        plot.plt.xlabel(r'$ms$')
        plot.plt.ylabel(r'$\psi / \psi_{edge}$')
        
    def plot_uVal(self, plot):
        plot.plt.plot( self.t_dom * 1e3, self.data_out[0:,1] / np.pi )

        plot.plt.xlabel(r'$ms$')
        plot.plt.ylabel(r'pol [$\pi$]')
        
    def plot_vVal(self, plot):
        plot.plt.plot( self.t_dom * 1e3, self.data_out[0:,2] / np.pi )

        plot.plt.xlabel(r'$ms$')
        plot.plt.ylabel(r'tor [$\pi$]')
        
if __name__ == '__main__':
    #import h5py as hf
    import time
    start_time = time.time()
    
    prams = { 'geometry' : dd.pathQHS,
              'velocity' : 1e3,
              'charge' : 1.6022e-19}
    
    prams['pitch'] = 0.35 * np.pi
    
    t_end = 1
    spec='elec'
    
    if spec=='elec':
        t_stp = 1e-8
        prams['mass'] = 9.1094e-31
        
    elif spec=='ion':
        t_stp = 1e-4
        prams['mass'] = 1.672621e-27 
    
    t_dom = np.arange( 0, t_end, t_stp )
    pos_init = np.array([0.5, 0, 0]) 
    
    g_cent = guiding_center(prams, pos_init)
    g_cent.integrate(t_dom)
    
    end_time = time.time() - start_time
    print( 'time : {} sec'.format( end_time ) )
    
    '''
    if g_cent.is_trapped():
        print('Trapped!')
    '''
    '''
    new_file = os.path.join( dd.pathDrive, 'pitch_angle_QHS', 'pitch_data.h5' )
    
    with hf.File( new_file, 'r' ) as hf_file:
        tor_dom = hf_file['toroidal domain'][:]
        pol_dom = hf_file['poloidal domain'][:]
        pitch_data = hf_file['pitch data'][:]
        
    for idx, tor_val in enumerate( tor_dom ):
        plot = pd.plot_define()
        plot.plt.plot( pol_dom / np.pi, pitch_data[:,idx] / np.pi, marker='s', ms=10, markerfacecolor='None', c='k')
        
        plot.plt.xlabel( r'pol [$\pi$]' )
        plot.plt.ylabel( r'pitch [$\pi$]' )
        
        plot.plt.ylim(0.2, 0.55)
        plot.plt.title('tor = {0:0.3f}'.format(tor_val) + r' [$\pi$]')
        
        plot.plt.grid()
        plot.plt.tight_layout()
        plot.plt.show()
        
        #saveName = os.path.join( dd.pathDrive, 'pitch_angle_QHS', 'tor_{0:0.3f}.png'.format(tor_val) )
        #plot.plt.savefig( saveName )
    '''
    '''
    with hf.File( new_file, 'w' ) as hf_file:
        hf_file.create_dataset( 'toroidal domain', data=tor_set )
        hf_file.create_dataset( 'poloidal domain', data=pol_set )
        hf_file.create_dataset( 'pitch data', data=trap_bound )
    '''    
    ### Plot s-Flux Coordinate ###
    if True:
        plot = pd.plot_define()
        g_cent.plot_sVal( plot )

        plot.plt.grid()
        plot.plt.tight_layout()
        plot.plt.show()
        #plot.plt.savefig( os.path.join( dd.pathDrive, 's_guide.png' ) )
    
    ### Plot u-Flux Coordinate
    if True:
        plot = pd.plot_define()
        g_cent.plot_uVal( plot )

        plot.plt.grid()
        plot.plt.tight_layout()
        plot.plt.show()
        #plot.plt.savefig( os.path.join( dd.pathDrive, 'u_guide.png' ) )
    
    ### Plot v-Flux Coordinate
    if True:
        plot = pd.plot_define()
        g_cent.plot_vVal( plot )
        
        plot.plt.grid()
        plot.plt.tight_layout()
        plot.plt.show()
        #plot.plt.savefig( os.path.join( dd.pathDrive, 'v_guide.png' ) )
        
    ### Plot Energy
    if False:        
        plot = pd.plot_define()
        g_cent.plot_nrg( plot )
        
        plot.plt.tight_layout()
        #plot.plt.show()
        plot.plt.savefig( os.path.join( dd.pathDrive, 'nrg_guide.png' ) )
    
    ### Make VTK ###
    if False:
        savePath = os.path.join(dd.base_path,'vtkTools','figures','HSX','vtk_files')
        saveName = 'qhs_1keV 040pi_{}.vtk'.format( spec )
        g_cent.make_VTK(savePath, saveName)
    
