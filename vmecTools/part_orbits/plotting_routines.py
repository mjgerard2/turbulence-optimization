# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 12:18:55 2021

@author: micha
"""


import numpy as np
import h5py as hf

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import directory_dict as dd
import plot_define as pd


def convergence_plot(data, key, saveDict=None):
    om_drift = data[key]
    npts = om_drift.shape[0]
    
    om_drift_avg = np.empty(npts)
    for i in range(npts):
        idx = i + 1
        om_drift_avg[i] = np.mean(om_drift[0:idx])
    
    om_std = np.std(om_drift)
    om_mean = om_drift_avg[-1]
        
    plot = pd.plot_define()
    plt = plot.plt
    
    plt.plot(1 + np.arange(npts), om_drift_avg, c='k')
    plt.plot([1, npts], [om_mean]*2, c='k', ls='--', label=r'$\langle \omega_{de} \rangle =$'+'{0:0.2f}'.format( om_mean ) )
    
    plt.xlabel('particles simulated')
    plt.ylabel(r'$\langle \omega_{de} \rangle$')
    
    keyTitle = key
    plt.title(keyTitle)
    
    plt.grid()
    plt.legend()
    plt.tight_layout()
    
    if saveDict:
        plt.savefig(saveDict['savePath'])
    else:
        plot.plt.show()
        

def histogram_data(data, key, saveDict=False):
    plot = pd.plot_define()
    plt = plot.plt
    
    plt.hist(data[key], bins=50)
    
    plt.xlabel(r'$\omega_{de}$')
    plt.ylabel('counts')
    
    keyTitle = key
    plt.title(keyTitle)
    plt.tight_layout()
    
    if saveDict:
        plt.savefig(saveDict['savePath'])
    else:
        plot.plt.show()
        

if __name__ == '__main__':
    dataPath = os.path.join('D:\\','GENE','eps_valley','monte_carlo_data','trapRes_wShft_psiN_0p5_Te_1keV_new.h5')
    with hf.File(dataPath, 'r') as hf_file:
        conData = {}
        for key in hf_file:
            conData[key] = hf_file[key][:]
    
    # for key in conData:
    #     print(key)
        
    #     saveBase = os.path.join('D:\\','GENE','eps_valley','figures','trapRes_figs','conID_figs')
        
    #     savePath_conPlot = os.path.join( saveBase, key+'_psiN_0p25_Te_1keV_trigFix.png')
    #     savePath_histPlot = os.path.join( saveBase, key+'_hist_psiN_0p25_Te_1keV_trigFix.png')
        
    #     conDict = {'savePath' : savePath_conPlot}
    #     histDict = {'savePath' : savePath_histPlot}
        
    #     histogram_data(conData, key, saveDict=conDict)
    #     convergence_plot(conData, key, saveDict=histDict)