# -*- coding: utf-8 -*-
"""
Created on Sat Jan 30 11:02:33 2021

@author: micha
"""


from scipy.special import erf

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from matplotlib.colors import LogNorm

def plot_define():
    plt.close('all')
        
    font = {'family' : 'sans-serif',
            'weight' : 'normal',
            'size'   : 18}

    mpl.rc('font', **font) 
    
    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2
    
    fig, ax = plt.subplots(1, 1)
    
    return fig, ax

def PDF_unNorm(v_perp, v_par, alf):
    return v_perp * np.exp( - ( alf * v_perp )**2 ) * np.exp( - ( alf * v_par )**2 )

def plot_2D_PDF(v_perp_dom, v_par_dom, pitch):
    perp_pts = v_perp_dom.shape[0]
    par_pts = v_par_dom.shape[0]
    
    prob_mesh = np.empty( ( perp_pts, par_pts ) )
    for idx, v_perp in enumerate(v_perp_dom[0:-1]):
        v_par_lim_temp = v_perp / np.tan( pitch )
        
        v_par_dom_temp = v_par_dom[ ( v_par_dom >= - v_par_lim_temp ) & ( v_par_dom <= v_par_lim_temp ) ]
        prob_unNorm = PDF_unNorm(v_perp, v_par_dom_temp, alf)
        
        idx_list_low = np.argwhere( v_par_dom < - v_par_lim_temp )
        idx_list_hgh = np.argwhere( v_par_dom > v_par_lim_temp )
        idx_list_mid = np.arange( idx_list_low[-1] + 1, idx_list_hgh[0] )
        
        prob_mesh[idx, idx_list_low] = np.nan
        prob_mesh[idx, idx_list_mid] = prob_unNorm
        prob_mesh[idx, idx_list_hgh] = np.nan
        
    prob_mesh[idx+1,:] = PDF_unNorm(v_perp_dom[-1], v_par_dom, alf)
    prob_mesh = prob_mesh / np.nansum( prob_mesh )    
    
    fig, ax = plot_define()
    
    s_map = ax.pcolormesh(v_par_dom * (1./3) * 1e-8, v_perp_dom * (1./3) * 1e-8, prob_mesh * 1e6, cmap=plt.get_cmap('jet'))
    cbar = fig.colorbar(s_map, ax=ax)
    cbar.ax.set_ylabel(r'Probability [$10^{-6}$]')
    
    plt.title( r'$T_{e}$ = ' + '{0:0.1f} keV'.format( temp_eV * 1e-3 ) + r', $\theta_{B}$ = ' + '{0:0.2f}'.format(pitch/np.pi) + r'$\pi$')
    plt.xlabel(r'$\frac{ v_{\parallel} }{ c }$')
    plt.ylabel(r'$\frac{ v_{\perp} }{ c } $')
    
    plt.show()
    
def plot_2D_untrapped(v_perp_dom, v_par_dom, pitch):
    perp_pts = v_perp_dom.shape[0]
    par_pts = v_par_dom.shape[0]
    
    prob_mesh = np.empty( ( perp_pts, par_pts ) )
    for idx, v_perp in enumerate(v_perp_dom[0:-1]):
        prob_unNorm = PDF_unNorm(v_perp, v_par_dom, alf)
        prob_mesh[idx,:] = prob_unNorm
        
    prob_mesh[idx+1,:] = PDF_unNorm(v_perp_dom[-1], v_par_dom, alf)
    prob_mesh = prob_mesh / np.nansum( prob_mesh )    
    
    #v_perp = v_perp_dom * (1./3) * 1e-8
    #v_par = (1. / np.arctan(pitch) ) * v_perp
    
    v_par = np.array([0, v_par_dom[-1]]) * (1./3) * 1e-8
    v_perp = np.arctan(pitch) * v_par
    
    fig, ax = plot_define()
    
    ax.plot(v_par, v_perp, ls='--', c='k')
    ax.plot(-v_par, v_perp, ls='--', c='k')    
    s_map = ax.pcolormesh(v_par_dom * (1./3) * 1e-8, v_perp_dom * (1./3) * 1e-8, prob_mesh * 1e6, cmap=plt.get_cmap('jet'))
    cbar = fig.colorbar(s_map, ax=ax)
    cbar.ax.set_ylabel(r'Probability [$10^{-6}$]')
    
    plt.title( r'$T_{e}$ = ' + '{0:0.1f} keV'.format( temp_eV * 1e-3 ) + r', $\theta_{B}$ = ' + '{0:0.2f}'.format(pitch/np.pi) + r'$\pi$')
    plt.xlabel(r'$\frac{ v_{\parallel} }{ c }$')
    plt.ylabel(r'$\frac{ v_{\perp} }{ c } $')
    
    plt.show()

def plot_Vperp_PDF(Vperp, pitch, alf):
    perp_dist = Vperp * ( 1 - np.cos(pitch) ) * np.exp( - ( Vperp * alf)**2 )
    perp_norm = np.trapz( perp_dist, Vperp )
    
    fig, ax = plot_define()
    
    plt.plot(Vperp  * (1./3) * 1e-8, ( perp_dist / perp_norm ) * 1e8 )
    
    plt.ylabel(r'PDF [$10^{-8}$]')
    plt.xlabel(r'$\frac{v_{\perp}}{c}$')
    plt.title( r'$T_{e}$ = ' + '{0:0.1f} keV'.format( temp_eV * 1e-3 ) + r', $\theta_{B}$ = ' + '{0:0.2f}'.format(pitch/np.pi) + r'$\pi$')
    plt.grid()
    plt.show()
    
def plot_theta_PDF(pitch):
    t_perp = np.linspace( pitch, 0.5 * np.pi, 10000 )
    sin_t = np.sin( t_perp )
    pitch_PDF = sin_t / np.sum( sin_t )
    
    fig, ax = plot_define()
    
    plt.plot( t_perp / np.pi , pitch_PDF * 1e4 )
    
    plt.ylabel(r'PDF [$10^{-4}$]')
    plt.xlabel(r'$\theta \, [\pi]$')
    plt.title( r'$T_{e}$ = ' + '{0:0.1f} keV'.format( temp_eV * 1e-3 ) + r', $\theta_{B}$ = ' + '{0:0.2f}'.format(pitch/np.pi) + r'$\pi$')    
    plt.grid()
    plt.show()
    
if __name__ == '__main__':
    m = 9.1094e-31
    k = 1.3807e-23
    temp_eV = 10e3
    T = temp_eV * 11604
    alf = np.sqrt( m / ( 2 * k * T ) )
    
    #Bmax, Bo = 1.2, .8
    #pitch = np.arctan( np.sqrt( Bo / ( Bmax - Bo ) ) )
    pitch = 0.45*np.pi
    
    v_perp_lim = .75 * 3e8#1 * ( 3e8 * np.sin( pitch ) )
    v_perp_dom = np.linspace(0, v_perp_lim, int(1e3))
           
    if True:
        ### Plot 2D PDF Trapped ###
        v_par_lim = v_perp_lim / np.tan( pitch )
        v_par_dom = np.linspace(-v_par_lim, v_par_lim, int(1e3))
        plot_2D_PDF(v_perp_dom, v_par_dom, pitch)
        
    if True:
        ### Plot 2D PDF Untrapped ###
        v_par_dom = np.linspace(-v_perp_lim, v_perp_lim, int(1e3))
        plot_2D_untrapped(v_perp_dom, v_par_dom, pitch)
    
    if False:
        ### Perpendicular Velocity PDF ###
        plot_Vperp_PDF(v_perp_dom, pitch, alf)
    if False:
        ### Theta PDF ###
        plot_theta_PDF(pitch)
