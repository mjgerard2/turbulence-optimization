import numpy as np
import h5py as hf

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd

# Import metric data #
metPath = os.path.join('/mnt', 'HSX_Database', 'Epsilon_Filter', 'metric_data_chi_thrsh_0p01.h5')
with hf.File(metPath, 'r') as hf_:
    met_data = hf_['metric data'][()]

met_data = np.array(sorted(met_data, key=lambda x: x[3]))

met_data = met_data[met_data[:,9] >= 1.02, :]

# Plot Data #
if False:
    plot = pd.plot_define()
    plt, fig, ax = plot.plt, plot.fig, plot.ax

    smap = ax.scatter(met_data[:,9], met_data[:,4], c=met_data[:,3], s=1, marker='s', cmap='jet')

    plt.xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
    plt.ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')

    cbar = fig.colorbar(smap, ax=ax)
    cbar.ax.set_ylabel(r'$\mathcal{E} / \mathcal{E}^*$')

    plt.tight_layout()
    plt.show()

# Save Data #
if True:
    savePath = os.path.join('/mnt', 'HSX_Database', 'Epsilon_Filter'
                            , 'wout_files', 'chi_thresh_0p01', 'kappa_low_1p02'
                            , 'conID.txt')

    with open(savePath, 'w') as file:
        for met in met_data:
            conID = '-'.join('{0:0.0f}'.format(ID) for ID in met[0:3])
            file.write(conID+'\n')
