# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 11:27:23 2020

@author: micha
"""


import profile_reader as pr

import h5py as hf
import numpy as np

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import directory_dict as dd

def sort_config_list(config_list):
    """ Sort the {mainID, setID, jobID} configuration list in ascending order,
    sorting mainIDs first, then the setIDs within the sorted mainIDs.  The
    jobIDs are not sorted within the sorted setIDs.

    Parameters
    ----------
    config_list : arr
        Unsorted configuration list {mainID, setID, jobID}.

    Returns
    -------
    Sorted configuration list.
    """
    config_list = np.array( sorted(config_list, key=lambda x: x[0]) )
    main_bounds = np.array([0, config_list.shape[0]])
    main_chng = np.insert(main_bounds, 1, 1 + np.where(config_list[:-1,0] != config_list[1:,0])[0])
    for b, idxBeg in enumerate(main_chng[0:-1]):
        idxEnd = main_chng[b+1]
        config_list[idxBeg:idxEnd] = np.array( sorted(config_list[idxBeg:idxEnd], key=lambda x: x[1]) )
        
        set_bounds = np.array([0, idxEnd-idxBeg])
        set_chng = np.insert(set_bounds, 1, 1 + np.where(config_list[idxBeg:idxEnd-1,1] != config_list[idxBeg+1:idxEnd,1])[0])
        for c, jdxBeg in enumerate(set_chng[0:-1]):
            jdxEnd = set_chng[c+1]
            config_list[idxBeg+jdxBeg:idxBeg+jdxEnd] = np.array( sorted(config_list[idxBeg+jdxBeg:idxBeg+jdxEnd], key=lambda x: x[2]) )
    
    return config_list
                    
def order_profile(path, set_ID, data_key, rng=[0, 0.5], name='conData.h5'):
    """ Order the specified set from lowest to largest average value.

    Parameters
    ----------
    path : str
        Path to the directory where the hdf5 file is stored.
    set_ID : str
        Set ID to be read in profile_reader.
    data_key : str
        Data key to be read from hd5f file.
    name : str, optional
        name of hdf5 file storing datailon effective data 
        (default is 'conData.h5')
    """
    print('Ordering profiles in '+set_ID+' from '+os.path.join(path, name))
    data = pr.profile_data(path, set_ID, data_key, name=name)
    
    idxMin, idxMax = data.find_extrema(rLow=rng[0], rHigh=rng[1])
    data.order_compare(data.data_profile[idxMin], rLow=rng[0], rHigh=rng[1])
    data.save_order()
    
def order_profile_set(path, main_configs, set_ID, data_key):
    for i in main_configs:
        mainID = 'main_coil_{}'.format(i)
        mainPath = os.path.join(path, mainID)    
        
        hf_file = hf.File(os.path.join(mainPath,'conData.h5'),'r')
        data_chk = data_key in hf_file[set_ID]
        hf_file.close()
        
        if data_chk:
            order_profile(mainPath, set_ID, data_key)
        else:
            print(set_ID+'/'+data_key+' does not exist in '+os.path.join(mainID,'conData.h5'))

if __name__ == '__main__':
    
    path = dd.exp_ext_base
    '''
    ### Order Metrics ###    
    #rng = [0.25, 0.75]
    #order_profile(path, 'superSet/all', 'GssKn_metric', rng=rng, name='profile_data.h5')
    ### Test Comparison Values ###
    prof = pr.profile_data(path, 'superSet/all', 'GssKn_metric', name='profile_data.h5')
    
    crnt_idx, qhs_idx = prof.find_config(np.array([0,1,0]))
    crnt_idx, tst_idx = prof.find_config(np.array([75,1,486]))
    
    qhs_prof = prof.data_profile[qhs_idx]
    tst_prof = prof.data_profile[tst_idx]
    
    leng = qhs_prof.shape[0]
    beg, end = prof.begIdx + int(0 * leng), prof.begIdx + int(0.5 * leng)
    
    qhs_mean = np.mean(qhs_prof[beg:end])
    tst_mean = np.mean(tst_prof[beg:end])
    
    print(tst_mean / qhs_mean)
    '''
    '''
    for mID in range(4):
        mainID = 'main_coil_{}'.format(mID)
        print(mainID)
        pathID = os.path.join(path, mainID, 'conData.h5')
        with hf.File(pathID, 'r') as hf_file:
            for key in hf_file['set_1']:
                print('   '+key)
    '''
    with hf.File(os.path.join(path, 'profile_data.h5'), 'r') as hf_file:
        #data = hf_file['superSet/all/Gss_metric'][:]
        for key in hf_file['superSet']:
            print(key)
    '''
    nan_data = np.isnan(data[0::,3])
    not_nan = ~nan_data
    data_real = data[not_nan]
    
    print('\nLowest Bk Overlap')
    print(data_real[0,0:3])
    
    print('\nMiddlest Bk Overlap')
    idx = int(0.8*data_real.shape[0])
    print(data_real[idx,0:3])
    
    print('\nHighest Bk_Overlap')
    print(data_real[-1,0:3])
    
    #crnt_idx, idx_1 = set_1.find_config(conID)
    '''
    '''
    with hf.File(os.path.join(path, 'profile_data.h5'), 'r') as hf_file:
        data = hf_file['superSet/first_600_Bk_overlap/Bk_overlap'][:]
    
    nan_data = np.isnan(data[0::,3])
    not_nan = ~nan_data
    data_real = data[not_nan]
    
    print('\nLowest Bk Overlap')
    print(data_real[0:10,0:3])
    print('\nHighest Bk_Overlap')
    print(data_real[-10::,0:3])
    
    idx = int(0.75*data_real.shape[0])
    print(data_real[idx-2:idx+2,0:3])
    '''