# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 12:51:36 2020

@author: micha
"""

import h5py as hf
import numpy as np

import profile_reader as pr
import profile_extraction as pe
import profile_operations as po

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import directory_dict as dd
import plot_define as pd


def get_qhs_metric(data_key):
    pathQHS = dd.pathQHS
    
    if data_key == 'eps_effective':
        data_comp = pe.extract_epsEff(pathQHS)
    elif data_key == 'qhs_metric':
        data_comp = pe.extract_qhsMetric(pathQHS)
    elif data_key == 'modB_average':
        data_comp = pe.extract_meanB(pathQHS)
    elif data_key == 'Bk_overlap':
        data_comp = pe.extract_Bk_overlap(pathQHS)
    elif data_key == 'GssKn_metric':
        data_comp = pe.extract_GssKn_metric(pathQHS)
    else:
        raise KeyError('{} is not a valid data key.'.format(data_key))
        
    return data_comp

def plot_ColorMap(data, data_comp, plot, jscl=1e-3, nJobs=False, log=True, save=False, savePath=os.path.join(os.getcwd(),'figures'), saveName='color_map.png'):    
    data.plot_profile_map(plot.fig, plot.ax, jscl=jscl, nJobs=nJobs, log=log)
    idxComp = data.find_compare(data_comp)
    plot.ax.plot([0, 1], [idxComp*jscl, idxComp*jscl], c='k', ls='--')
    
    if jscl==1e-6:
        plot.plt.ylabel('Configurations (1M)')
    elif jscl==1e-3:
        plot.plt.ylabel('Configurations (1k)')
    elif jscl==1e-2:
        plot.plt.ylabel('Configurations (100\'s)')
    elif jscl==1e-1:
        plot.plt.ylabel('Configurations (10\'s)')
    elif jscl==1:
        plot.plt.ylabel('Configurations')
    
    plot.plt.xlabel('r/a')
    
    plot.plt.tight_layout()
    if save:
        plot.plt.savefig(os.path.join(savePath, saveName))
    else:
        plot.plt.show()
        
def plot_ColorMapDiff(data, data_comp, plot, jscl=1e-3, nJobs=False, save=False, savePath=os.path.join(os.getcwd(),'figures'), saveName='color_map_diff.png'):
    data.plot_difference_map(plot.fig, plot.ax, data_comp, jscl=jscl, nJobs=nJobs)
    idxComp = data.find_compare(data_comp)
    plot.ax.plot([0, 1], [idxComp*jscl, idxComp*jscl], c='k', ls='--')
    
    if jscl==1e-6:
        plot.plt.ylabel('Configurations (1M)')
    elif jscl==1e-3:
        plot.plt.ylabel('Configurations (1k)')
    elif jscl==1e-2:
        plot.plt.ylabel('Configurations (100\'s)')
    elif jscl==1e-1:
        plot.plt.ylabel('Configurations (10\'s)')
    elif jscl==1:
        plot.plt.ylabel('Configurations')
        
    plot.plt.xlabel('r/a')
    
    plot.plt.tight_layout()
    if save:
        plot.plt.savefig(os.path.join(savePath, saveName))
    else:
        plot.plt.show()
    
def plot_QHS_Diff(data, data_comp, plot, rng=[0,0.5], diff_thresh=100, jscl=1e-3, save=False, savePath=os.path.join(os.getcwd(),'figures'), saveName='qhs_difference.png'):
    conID = data.plot_difference(plot.ax, data_comp, rLow=rng[0], rHigh=rng[1], diff_thresh=diff_thresh, jscl=jscl)
    
    if jscl==1e-6:
        plot.plt.xlabel('# of Configurations (1M)')
    elif jscl==1e-3:
        plot.plt.xlabel('# of Configurations (1k)')
    elif jscl==1e-2:
        plot.plt.xlabel('# of Configurations (100\'s)')
    elif jscl==1e-1:
        plot.plt.xlabel('# of Configurations (10\'s)')
    elif jscl==1:
        plot.plt.xlabel('# of Configurations')
        
    plot.plt.ylabel('% Diff')
    
    plot.plt.grid()
    plot.plt.legend()
    plot.plt.tight_layout()
    
    if save:
        plot.plt.savefig(os.path.join(savePath, saveName))
    else:
        plot.plt.show()
    
    return conID

def plot_QHS_Diff_Hist(data, data_comp, plot, bins=20, diff_thresh=100, save=False, savePath=os.path.join(os.getcwd(),'figures'), saveName='qhs_difference_hist.png'):
    conID = data.plot_difference_histogram(plot.plt, data_comp, bins, diff_thresh=diff_thresh)
    
    plot.plt.xlabel('% Diff w/ QHS')
    plot.plt.ylabel('Counts')
    #plot.plt.title(r'$\epsilon_{eff}^{3/2}$ Histogram')
    plot.plt.title('QHS Metric Histogram')
    
    plot.plt.grid()
    plot.plt.tight_layout()
    
    if save:
        plot.plt.savefig(os.path.join(savePath, saveName))
    else:
        plot.plt.show()
        
    return conID

def plot_profile(plot, conIDs, data_key, plotDict, setID='superSet/all', filePath=os.path.join(dd.exp_ext_base), fileName='profile_data.h5'):    
    """ Plot the specified metric profiles.
    
    Parameters
    ----------
    plot : obj
        Matplotlib plotting object.
    conIDs : list
        Set of configuration IDs, structured as 'mainID-setID-jobID'.
    data_key : str
        Metric to be plotted.
    plotDict : dict
        Dictionary of plotting parameters.
    setID : str, optional
        Set identifier. The default is 'superSet/all'.
    filePath : str, optional
        Path to directory where the profile data is stored. The default is os.path.join(dd.exp_ext_base).
    fileName : str, optional
        Name of the profile data file stored in filePath. The default is 'profile_data.h5'.

    Returns
    -------
    arr     
        Configuration ID and auxiliary coil currents, stored in a numpy array.
    """
    prof = pr.profile_data(filePath, setID, data_key, name=fileName)    
    s_dom = prof.s_dom
    
    cnt = len(conIDs)    
    profs = np.empty((cnt, s_dom.shape[0]))
    crnts = np.empty((cnt, 7))
    for c, conID in enumerate(conIDs):
        if conID == 'qhs':
            conArr = np.array([0,1,0])
        else:
            conID = conID.split('-')
            conArr = np.array([int(i) for i in conID])
        
        crnt_idx, data_idx = prof.find_config(conArr[2])
        crnts[c] = prof.crnt_profile[crnt_idx]
        profs[c] = prof.data_profile[data_idx][prof.begIdx:]
           
    for p, pro in enumerate(profs):
        pltD = {}
        for key in plotDict:
            pltD[key] = plotDict[key][p]
        plot.plt.plot(s_dom, pro, **pltD)

    plot.plt.xlabel(r'$\Psi_n$')
    plot.plt.ylabel(prof.data_label)
        
    if 'label' in plotDict:
        box = plot.ax.get_position()
        plot.ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
        plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1), fontsize=16)
    
    plot.plt.grid()
    plot.plt.tight_layout()

    # return crnts
              
        
def metric_comparison(conIDs, metric_1, metric_2, rLow_1=0, rHigh_1=0.5, rLow_2=0, rHigh_2=0.5):
    """ Generate metric comparison data and corresponding scatter plot.  This 
    comparison includes normalized profile averages of two specified metrics.
    The normalization is the corresponding profile average of the QHS metric 
    value.

    Parameters
    ----------
    conIDs : arr
        Set of configuration IDs, structured as [mainID, setID, jobID].
    metric_1 : str
        First metric in comparison.  This metric will be plotted on the 
        horizontal axis.
    metric_2 : str
        Second metric in comparison.  This metric will be plotted on the 
        vertical axis.
    rLow_1 : float, optional
        Lower \Psi_N boundary for the first metric over the profile average. 
        The default is 0.
    rHigh_1 : float, optional
        Higher \Psi_N boundary for the first metric over the profile average. 
        The default is 0.5.
    rLow_2 : float, optional
        Lower \Psi_N boundary for the second metric over the profile average. 
        The default is 0.
    rHigh_2 : float, optional
        Higher \Psi_N boundary for the second metric over the profile average. 
        The default is 0.5.

    Returns
    -------
    data_real : arr
        Configuration data and corresponding normalized metric averages, 
        structured as [mainID, setID, jobID, met_1, met_2].
    """
    ### Read QHS Profile Metrics ###
    qhs_met_1 = get_qhs_metric(metric_1)
    qhs_met_2 = get_qhs_metric(metric_2)
    
    ### Compute QHS Normalization Values ###
    len_1, len_2 = qhs_met_1.shape[0], qhs_met_2.shape[0]
    beg_1, end_1 = int(rLow_1 * len_1), int(rHigh_1 * len_2)
    beg_2, end_2 = int(rLow_2 * len_2), int(rHigh_2 * len_2)
    
    met_norm_1 = np.mean(qhs_met_1[beg_1:end_1])
    met_norm_2 = np.mean(qhs_met_2[beg_2:end_2])
    
    ### Sort conIDs and Partition by MainIDs ###
    conID_set = po.sort_config_list(conIDs)
    mainIDX = np.r_[0, 1 + np.where(conID_set[1:,0] != conID_set[0:-1,0])[0], conID_set.shape[0]]
    
    c=0
    data_comp = np.full((conIDs.shape[0],5), np.nan)
    for i, idx in enumerate(mainIDX[1:]):
        idx_beg = mainIDX[i]
        mainID = 'main_coil_{}'.format(int(conID_set[idx_beg,0]))
        print(mainID)
        
        ### Partition by SetIDs ###
        setIDX = np.r_[idx_beg, 1 + np.where(conID_set[idx_beg+1:idx, 1] != conID_set[idx_beg:idx-1, 1])[0], idx]
        for j, jdx in enumerate(setIDX[1:]):
            jdx_beg = setIDX[j]
            setID = 'set_{}'.format(int(conID_set[jdx_beg,1]))
            
            ### Read in Set Data ###
            path = os.path.join(dd.exp_ext_base, mainID)
            
            set_1 = pr.profile_data(path, setID, metric_1)
            set_2 = pr.profile_data(path, setID, metric_2)
            for job_idx in np.arange(jdx_beg, jdx):
                try:
                    ### Find Specific Jobs in Set Data ###
                    jobID = conID_set[job_idx,2] 
                    
                    crnt_idx, idx_1 = set_1.find_config(jobID)
                    crnt_idx, idx_2 = set_2.find_config(jobID)
                    
                    ### Read Job Profile Data ###
                    met_prof_1 = set_1.data_profile[idx_1]
                    met_prof_2 = set_2.data_profile[idx_2]
                    
                    ### Compute Normalized Metric Values ###
                    len_1, len_2 = met_prof_1.shape[0], met_prof_2.shape[0]
                    beg_1, end_1 = set_1.begIdx + int(rLow_1 * len_1), set_1.begIdx + int(rHigh_1 * len_2)
                    beg_2, end_2 = set_2.begIdx + int(rLow_2 * len_2), set_2.begIdx + int(rHigh_2 * len_2)
        
                    datum_1 = np.mean(met_prof_1[beg_1:end_1]) / met_norm_1
                    datum_2 = np.mean(met_prof_2[beg_2:end_2]) / met_norm_2
                    
                    ### Broadcast Metric Data ###
                    data_comp[c] = np.append(conID_set[job_idx], np.array([datum_1, datum_2]))
                    c+=1
                    
                except ValueError:
                    ### Unable to Find Specific Job ###
                    c+=1
                    continue    
    
    ### Filter Out NANs ###
    nan = np.isnan(data_comp[0::,0])
    not_nan = ~nan
    data_real = data_comp[not_nan]
    return data_real
    
if __name__ == '__main__':  
    path = os.path.join('D:\\','HSX_Configs','main_coil_0')
    proQHS = pr.profile_data(path, 'set_1', 'qhs_booz', name='conData.h5')
    crntIDX, dataIDX = proQHS.find_config(0)
    qhs_prof = proQHS.data_profile[dataIDX, proQHS.begIdx::]
    
    conID_list = ['105-1-88','240-1-88','794-1-373','710-1-328','864-1-166','585-1-277']
    for conID in conID_list:
        conArr = conID.split('-')
        mainID = 'main_coil_{}'.format(conArr[0])
        proPath = os.path.join('D:\\','HSX_Configs',mainID)
        
        conIDs = [conID]
        plotDict = {'label' : [conID],
                    'c' : ['tab:red']}
        
        plot = pd.plot_define(lineWidth=2)
        
        plot.plt.plot(proQHS.s_dom, qhs_prof, label='QHS', c='k')
        plot_profile(plot, conIDs, 'qhs_booz', plotDict, setID='set_1', filePath=proPath, fileName='conData.h5')
        
        # plot.plt.show()
        
        saveName = os.path.join('D:\\','HSX_Configs','10_Low_EpsEffective_Configs',conID,'qhs_profile.png')
        plot.plt.savefig(saveName)
