# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 11:03:18 2020

@author: micha
"""


from matplotlib.colors import LogNorm
import numpy as np
import h5py as hf
import os


class profile_data():
    """ A class to perform a comparative analysis on large sets of profile 
    data.
    
    ...
    
    Attributes
    ----------
    path : str
        path to the directory in which the data is stored
    setID : str
        set ID of datailon effective data
    data_key : str
        Data key to be read from hd5f file.
    name : str, optional
        name of hdf5 file storing datailon effective data 
        (default is 'conData.h5')
        
    Methods
    -------
    find_crnt_profile(crnt_pro)
        Returns index of the specified current configuration.
        
    find_config(jobID)
        Returns index of configuration for the specified job ID.
        
    find_extrema(rBnd=0.5)
        Returns indices of the minimum and maximum averaged data values
        within the specified radial domain.
        
    find_compare(data_init, rBnd=0.5, ordered=False)
        Returns the index of the profile data whose percent comaprison with 
        the input data is smallest over the specified radial domain.
        
    order_compare(data_init, rBnd=.5)
        Orders all profile data by their percent comparison to the input
        data, calculated over the specified radial domain.
        
    order_permute(self, sort_order)
        Orders all profile data by the change in their current profile,
        permuted as specified by input.
        
    plot_profile_map(fig, ax, rBnd=0.5, jscl=1e-3, ordered=False, nJobs=False)
        Plot color map of profile data along their index order.
        
    plot_difference_map(fig, ax, data_init, jscl=1e-3, ordered=False, nJobs=False):
        Plots color map of profile data percent difference with input data.
        
    plot_difference(ax, data_init, rBnd=.5, diff_thresh=100, jscl=1e-3, comp_name='QHS')
        Plot the percent difference of all configurations against the input 
        data across the specified radial domain.
        
    plot_difference_histogram(ax, data_init, bins, rBnd=.5, diff_thresh=100):
        Plot a histogram of profile data with a percent difference below the 
        specified threshold when compared to the input data across the 
        specified radial domain.
        
    save_order()
        Save the present configuration order.
        
    Raises
    ------
    IOError
        hdf5 file specified does not exist
    """
    
    
    def __init__(self, path, setID, data_key, name='conData.h5'):
        
        self.path = path
        self.name = name
        self.setID = setID
        self.data_key = data_key
        
        if data_key == 'eps_effective':
            self.data_label = r'$\epsilon_{eff}^{3/2}$'
        elif data_key == 'qhs_metric':
            self.data_label = r'$\mathcal{Q}$'
        elif data_key == 'modB_average':
            self.data_label = r'$\langle B \rangle$ [$Tm^{-1}$]'
        elif data_key == 'Bk_overlap':
            self.data_label = r'$\mathcal{F}_{\kappa}$ [$m^{-2}$]'
        elif data_key == 'Gss_metric':
            self.data_label = r'$g_{ss}$ [m$^2$]'
        elif data_key == 'GssKn_metric':
            self.data_label = r'$\langle g_{ss}\kappa_n\rangle$ [$m^{-2}$]'
        elif data_key == 'qhs_booz':
            self.data_label = r'$\mathcal{Q}$'
        elif data_key == 'avg_shear':
            self.data_label = r'$\hat{S}$'
        
        beg_3_keys = ['eps_effective']
        beg_4_keys = ['qhs_metric', 'modB_average', 'Gss_metric', 'avg_shear','F_kappa','G_ss']
        beg_5_keys = ['qhs_booz']
        beg_8_keys = ['Bk_overlap', 'GssKn_metric']
        
        if setID.split('/')[0] == 'superSet':
            if any(ikey == data_key for ikey in beg_3_keys):
                self.begIdx = 3
            elif any(ikey == data_key for ikey in beg_4_keys):
                self.begIdx = 4
            elif any(ikey == data_key for ikey in beg_5_keys):
                self.begIdx = 5
            elif any(ikey == data_key for ikey in beg_8_keys):
                self.begIdx = 8
                
            self.jobIdx = 2
            self.setIdx = 1
            self.mainIdx = 0
            
        else:
            if any(ikey == data_key for ikey in beg_3_keys):
                self.begIdx = 1
            elif any(ikey == data_key for ikey in beg_4_keys):
                self.begIdx = 2
            elif any(ikey == data_key for ikey in beg_5_keys):
                self.begIdx = 3
            elif any(ikey == data_key for ikey in beg_8_keys):
                self.begIdx = 6
                
            self.jobIdx = 0
        
        pathName = os.path.join(path, name)
        try:
            f = hf.File(pathName, 'r')
        except IOError:
            print('File does not exists: '+pathName)
            raise 
            
        try:
            self.crnt_profile = f[setID+'/config'][:]
            self.data_profile = f[setID+'/'+data_key][:]
        except KeyError:
            raise IOError(data_key+' data not available for '+pathName)
        
        self.job_num = self.data_profile.shape[0]
        
        if data_key == 'eps_effective' or data_key == 'qhs_booz':
            self.s_dom = np.linspace(0, 1, 128)[1:]
        else:            
            self.s_dom = np.linspace(0, 1, 128)[self.begIdx-(self.jobIdx+1):]
        
        self.roa_dom = np.sqrt( self.s_dom )
        self.ns = self.s_dom.shape[0]

        f.close()
        
    
    def find_crnt_profile(self, crnt_pro):
        """ Returns index of the specified current configuration.
        
        Parameters
        ----------
        crnt_pro : array
            Current profile whose index we want to identify
            
        Returns
        -------
        int
            job index of the specified current profile 
            
        Raises
        ------
        ValueError
            specified current profile does not exist
        """
        print(self.crnt_profile[0:10])
        idx = np.argmin( np.sum(np.abs(self.crnt_profile[0::,self.begIdx::] - crnt_pro), axis=1) )
        
        diff = np.sum( np.abs( self.crnt_profile[idx,self.begIdx::] - crnt_pro ) )
        if diff == 0:
            return idx
        else:
            crnt_str = '['+', '.join(['{}'.format(c) for c in crnt_pro])+']'
            raise ValueError('Current profile '+crnt_str+' does not exist.')


    def find_config(self, conID):
        """ Returns index of configuration for the specified job ID.
        
        Parameters
        ----------
        conID : int
            job index to identify
            
        Returns
        -------
        int
            index of configuration found for specified job ID
            
        Raises
        ------
        ValueError
            specified job ID does not exist
        """        
        if self.setID.split('/')[0] == 'superSet':            
            is_nan = np.isnan(self.data_profile[0::,self.begIdx])
            not_nan = ~is_nan
            
            if isinstance(conID, str):
                conID = np.array([float(i) for i in conID.split('-')])
            
            crnt_idx = np.nanargmin(np.sum(np.abs(self.crnt_profile[0::,0:3] - conID), axis=1))
            data_idx = np.nanargmin(np.sum(np.abs(self.data_profile[not_nan,0:3] - conID), axis=1))
            
            crnt_diff = np.sum(np.abs(self.crnt_profile[crnt_idx, 0:3] - conID))
            data_diff = np.sum(np.abs(self.data_profile[data_idx, 0:3] - conID))
        
        else:
            conVal = int(conID)
            
            crnt_idx = np.nanargmin(np.abs(self.crnt_profile[0::,0] - conVal))
            data_idx = np.nanargmin(np.abs(self.data_profile[0::,0] - conVal))
            
            crnt_diff = self.crnt_profile[crnt_idx,0] - conVal
            data_diff = self.data_profile[data_idx,0] - conVal
            
        if crnt_diff == 0 and data_diff == 0:
            return crnt_idx, data_idx
        else:
            raise ValueError('conID {} does not exist.'.format(conID))
            
            
    def find_extrema(self, rLow=0, rHigh=0.5):
        """ Returns indices of the minimum and maximum averaged data values
        within the specified radial domain.
        
        Parameters
        ----------
        rLow : float, optional
            Lower psiN boundary for averaged domain.  Default is 0.
        rHigh : float
            Higher psiN boundary for averaged domain.  Default is 0.5.
            
        Returns
        -------
        tuple
            tuple of indices for the minimum and maximum averaged profile data
            values, respectively 
        """
        ns_beg = self.begIdx + int(rLow * self.ns)
        ns_end = self.begIdx + int(rHigh * self.ns)
        
        ind_min = np.nanargmin( np.mean(self.data_profile[0::, ns_beg:ns_end], axis=1) )
        ind_max = np.nanargmax( np.mean(self.data_profile[0::, ns_beg:ns_end], axis=1) )

        return [ind_min, ind_max]
    
    
    def find_compare(self, data_init, rLow=0, rHigh=0.5, ordered=False):
        """ Returns the index of the profile data whose percent comaprison with 
        the input data is smallest over the specified radial domain.
        
        Parameters
        ----------
        data_init : array
            input profile data against which all other profiles are compared
        rLow : float, optional
            Lower psiN boundary for averaged domain.  Default is 0.
        rHigh : float
            Higher psiN boundary for averaged domain.  Default is 0.5.
            
        Returns
        -------
        integer
            index of the profile data with smallest percent comparison
        """
        ns_beg = self.begIdx + int(rLow * self.ns)
        ns_end = self.begIdx + int(rHigh * self.ns)
        
        size_diff = self.data_profile.shape[1] - data_init.shape[0]
        data_init = np.insert(data_init, 0, np.zeros(size_diff))[ns_beg:ns_end]
        data_init_inv = 1. / data_init
        
        if ordered:
            data_search = self.data_ordered
        else:
            data_search = self.data_profile            
        
        compare = np.empty(self.job_num)
        for ind, data in enumerate(data_search[0::,ns_beg:ns_end]):
            compare[ind] = np.mean(np.abs(data - data_init) * data_init_inv)
            
        return np.nanargmin(compare)
    
    
    def order_compare(self, data_init, rLow=0, rHigh=0.5):
        """ Orders all profile data by their percent comparison to the input
        data, calculated over the specified radial domain.
        
        Parameters
        ----------
        data_init : array
            profile data to compare other profiles to
        rLow : float, optional
            Lower psiN boundary for averaged domain.  Default is 0.
        rHigh : float
            Higher psiN boundary for averaged domain.  Default is 0.5.
        """        
        ns_beg = self.begIdx + int(rLow * self.ns)
        ns_end = self.begIdx + int(rHigh * self.ns)
        
        size_diff = self.data_profile.shape[1] - data_init.shape[0]
        data_init = np.insert(data_init, 0, np.zeros(size_diff))[ns_beg:ns_end]
        data_init_inv = 1. / data_init
        
        self.data_ordered = np.empty(self.data_profile.shape)

        compare = np.empty((self.job_num, 2))        
        for e, data in enumerate(self.data_profile[0::,ns_beg:ns_end]):
            data_chk = np.mean( np.abs(data - data_init) * data_init_inv )
            compare[e] = np.array([data_chk, e])
        
        compare = np.array( sorted(compare, key=lambda x: np.inf if np.isnan(x[0]) else x[0]) )
        for c, com in enumerate(compare):
            ind = int(com[1])
            self.data_ordered[c] = self.data_profile[ind]            

            
    def order_permute(self, sort_order):
        """ Orders all profile data by the change in their current profile,
        permuted as specified by input.
        
        Parameters
        ----------
        sort_order : list
            list of coil permutation order, for example, input of [1,2,3,4,5,6]
            would order permutations in coil 1 first and coil 6 last.
        """
        ind = sort_order[0]
    
        self.crnt_ordered = np.array( sorted(self.crnt_profile, key=lambda x: x[ind]) )
        chng_bounds = np.array([0, self.job_num])
        for i, ind in enumerate(sort_order[0:-1]):
            ind1 = sort_order[i+1]
            crnt_chng = np.insert(chng_bounds, 1, 1 + np.where(self.crnt_ordered[:-1,ind] != self.crnt_ordered[1:,ind])[0])
            for b, idxBeg in enumerate(crnt_chng[0:-1]):
                idxEnd = crnt_chng[b+1]
                self.crnt_ordered[idxBeg:idxEnd] = np.array( sorted(self.crnt_ordered[idxBeg:idxEnd], key=lambda x: x[ind1]) )
                
        self.data_ordered = np.empty(self.data_profile.shape)
        for c, crnt in enumerate(self.crnt_ordered):
            jobID = crnt[0]
            idx = self.find_config(jobID)
            self.data_ordered[c] = self.data_profile[idx]

            
    def plot_profile_map(self, fig, ax, rLow=0, rHigh=0.5, jscl=1e-3, ordered=False, nJobs=False, log=True):
        """ Plot color map of profile data along their index order.
        
        Parameters
        ----------
        fig : object
            Figure object on which to construct color scale.
        ax : object
            Axis object on which to construct color plot.
        rLow : float, optional
            Lower psiN boundary for averaged domain.  Default is 0.
        rHigh : float
            Higher psiN boundary for averaged domain.  Default is 0.5. 
        jscl : float, optional
            Scale factor for job numbering. Default is 1e-3.
        ordered : bool, optional
            Plot profile data by data_ordered instead of data_profile.
            Default is False
        nJobs : bool (int), optional
            Number of jobs to plot. Default is False.
        """        
        if ordered:
            if nJobs:
                ax.set_ylim(0, nJobs*jscl)
                
                j_dom = np.arange(nJobs) * jscl
                data_pro = self.data_ordered[0:nJobs,self.begIdx::]
            else:
                ax.set_ylim(0, self.job_num*jscl)
                
                j_dom = np.arange(self.job_num) * jscl
                data_pro = self.data_ordered[0::,self.begIdx::]
        else:
            if nJobs:
                ax.set_ylim(0, nJobs*jscl)
                
                j_dom = np.arange(nJobs) * jscl
                data_pro = self.data_profile[0:nJobs,self.begIdx::]
            else:
                ax.set_ylim(0, self.job_num*jscl)
                
                j_dom = np.arange(self.job_num) * jscl
                data_pro = self.data_profile[0::,self.begIdx::]
            
        ns_beg = self.begIdx + int(rLow * self.ns)
        ns_end = self.begIdx + int(rHigh * self.ns)
        
        data_min = np.nanmin(data_pro[0::, ns_beg:ns_end])
        if data_min < 1e-5:
            data_min = 1e-5
        data_max = np.nanmax(data_pro[0::, ns_beg:ns_end])
        
        if log:
            s_map = ax.pcolormesh(self.s_dom, j_dom, data_pro, norm=LogNorm(vmin=data_min, vmax=data_max), cmap='jet')
        else:
            s_map = ax.pcolormesh(self.s_dom, j_dom, data_pro, vmin=data_min, vmax=data_max, cmap='jet')

        
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(self.data_label)
        
    
    def plot_difference_map(self, fig, ax, data_init, jscl=1e-3, ordered=False, nJobs=False):
        """ Plots color map of profile data percent difference with input data.

        Parameters
        ----------
        fig : object
            Figure object on which to construct color scale.
        ax : object
            Axis object on which to construct color plot.
        data_init : array
            Profile data to compare other profiles to.
        jscl : float, optional
            Scale factor for job numbering. Default is 1e-3.
        ordered : bool, optional
            Plot profile datas by data_ordered instead of data_profile 
            The default is False.
        nJobs : bool (int), optional
            Number of jobs to plot. Default is False
        """        
        if ordered:
            if nJobs:
                ax.set_ylim(0, nJobs*jscl)
                
                j_dom = np.arange(nJobs) * jscl
                data_pro = self.data_ordered[0:nJobs,self.begIdx::]
            else:
                ax.set_ylim(0, self.job_num*jscl)
                
                j_dom = np.arange(self.job_num) * jscl
                data_pro = self.data_ordered[0::,self.begIdx::]
        else:
            if nJobs:
                ax.set_ylim(0, nJobs*jscl)
                
                j_dom = np.arange(nJobs) * jscl
                data_pro = self.data_profile[0:nJobs,self.begIdx::]
            else:
                ax.set_ylim(0, self.job_num*jscl)
                
                j_dom = np.arange(self.job_num) * jscl
                data_pro = self.data_profile[0::,self.begIdx::]
        
        size_diff = self.data_profile.shape[1] - data_init.shape[0]
        data_init = np.insert(data_init, 0, np.zeros(size_diff))[self.begIdx::]
        data_init_inv = 1. / data_init
        
        diff_map = np.empty(data_pro.shape)
        for e, data in enumerate(data_pro):
            diff_map[e][:] = np.abs(data - data_init) * data_init_inv * 100
            
        diff_min = np.nanmin(diff_map[np.nonzero(diff_map)])
        diff_max = np.nanmax(diff_map)
            
        idx = np.nanargmin( np.sum(diff_map, axis=1) )
        #ax.plot([0, 1], [idx*jscl, idx*jscl], ls='--', c='k', linewidth=1)    
        if np.sum(diff_map[idx]) == 0:
            diff_map[idx][:] = 1e-10
        
        #s_map = ax.pcolormesh(self.s_dom, j_dom, diff_map, norm=LogNorm(vmin=diff_min, vmax=diff_max), cmap='jet')
        s_map = ax.pcolormesh(self.s_dom, j_dom, diff_map, vmin=diff_min, vmax=diff_max, cmap='jet')
        
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'% Difference')


    def plot_difference(self, ax, data_init, rLow=0, rHigh=0.5, diff_thresh=100, jscl=1e-3, plot_dict=dict(color='tab:orange'), comp_dict=dict(color='k', zorder=0, label='QHS')):#, color='tab:orange', comp_name='QHS', diff_name=None):
        """ Plot the percent difference of all configurations against the 
        input data across the specified radial domain.
        
        Parameters
        ----------
        ax : object
            Axis object on which to construct plot
        data_init : array
            Input datailon profile
        rLow : float, optional
            Lower psiN boundary for averaged domain.  Default is 0.
        rHigh : float
            Higher psiN boundary for averaged domain.  Default is 0.5.
        diff_thresh : float, optional
            Only configurations with a percent difference below 
            this theshold will be plotted. Default is 1.
        jscl : float, optional
            Scale factor for job numbering. Default is 1e-3.
        comp_name : str, optional
            Plot label for input profile data. Default is 'QHS'.
            
        Returns
        -------
        list
            list of job indices with percent differences below the specified
            threshold
        """
        ns_beg = self.begIdx + int(rLow * self.ns)
        ns_end = self.begIdx + int(rHigh * self.ns)
        
        size_diff = self.data_profile.shape[1] - data_init.shape[0]
        data_init = np.insert(data_init, 0, np.zeros(size_diff))[ns_beg:ns_end]
        data_init_inv = 1. / data_init
        
        compare = np.empty((self.job_num, 2+self.jobIdx))
        for ind, data in enumerate(self.data_profile[0::,ns_beg:ns_end]):
            comp = np.mean((data - data_init) * data_init_inv) * 100
            compare[ind] = np.r_[comp, self.data_profile[ind][0:self.jobIdx+1]]
        
        compare_nan = np.isnan(compare[0::,0])
        compare_not_nan = ~compare_nan
        compare = compare[compare_not_nan]
        
        compare = np.array( sorted(compare, key=lambda x: x[0])  )
        
        comps = compare[0::,0]
        compDiff = comps[comps < diff_thresh]
        
        jobID = compare[0::,1::]
        jobID = jobID[comps < diff_thresh]
        
        jPts = jobID.shape[0] 
        job_dom = np.arange(jPts) * jscl
        
        ax.plot(job_dom, compDiff, **plot_dict)
        if comp_dict:
            ax.plot([0, jPts*jscl], [0, 0], **comp_dict)
        
        return np.insert(jobID, self.jobIdx+1, compDiff, axis=1)
    
    def plot_difference_histogram(self, ax, data_init, bins, rLow=0, rHigh=0.5, diff_thresh=100):
        """ Plot a histogram of profile data with a percent difference below 
        the specified threshold when compared to the input data across the 
        specified radial domain.

        Parameters
        ----------
        ax : object
            Axis object on which to construct plot
        data_init : array
            Input datailon profile
        bins : int
            Number of bins in histogram.
        rLow : float, optional
            Lower psiN boundary for averaged domain.  Default is 0.
        rHigh : float
            Higher psiN boundary for averaged domain.  Default is 0.5.
        diff_thresh : float 
            Only configurations with a percent difference below 
            this theshold will be plotted. Default is 1.
            
        Returns
        -------
        list
            list of configuration ID numbers with percent differences below 
            the specified threshold.
        """
        ns_beg = self.begIdx + int(rLow * self.ns)
        ns_end = self.begIdx + int(rHigh * self.ns)
        
        size_diff = self.data_profile.shape[1] - data_init.shape[0]
        data_init = np.insert(data_init, 0, np.zeros(size_diff))[ns_beg:ns_end]
        data_init_inv = 1. / data_init
        
        compare = np.empty((self.job_num, 2+self.jobIdx))
        for ind, data in enumerate(self.data_profile[0::,ns_beg:ns_end]):
            comp = np.mean((data - data_init) * data_init_inv) * 100
            compare[ind] = np.r_[comp, self.data_profile[ind][0:self.jobIdx+1]]
        
        compare_nan = np.isnan(compare[0::,0])
        compare_not_nan = ~compare_nan
        compare = compare[compare_not_nan]
        
        comps = compare[0::,0]
        compDiff = comps[comps <= diff_thresh]
        
        jobID = compare[0::,1::]
        jobID = jobID[comps <= diff_thresh]
        
        ax.hist(compDiff, bins=bins, color='tab:red')
        return np.insert(jobID, self.jobIdx+1, compDiff, axis=1)

    def save_order(self):
        """ Save the present configuration order.
        
        Raises
        ------
        ValueError
            no ordered profiles are available
        """
        try:
            self.data_ordered
        except:
            raise ValueError("No ordered profiles available (did run order_compare?)")
        
        name = os.path.join(self.path, self.name)
        f = hf.File(name, 'a')
        
        del f[self.setID+'/'+self.data_key]
        f.create_dataset(self.setID+'/'+self.data_key, data=self.data_ordered)
        
        f.close()

if __name__ == '__main__':
    import sys
    path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
    sys.path.append(path)
    
    import plot_define as pd
    import directory_dict as dd
    
    proPath = os.path.join('D:\\','HSX_Configs')
    proName = 'profile_data.h5'
    proSet = 'superSet/all'
    
    if False:
        hf_path = os.path.join(dd.base_path, 'vmecTools', 'profile_analysis', 'figures', 'metric_data_shear.h5')
        with hf.File(hf_path, 'r') as hf_file:
            shear_data = hf_file['shear'][:]
            
        hf_path = os.path.join(dd.base_path, 'vmecTools', 'profile_analysis', 'figures', 'metric_data.h5')
        with hf.File(hf_path, 'r') as hf_file:
            met_data = hf_file['metric data'][:]
        
        met_data_new = np.empty((met_data.shape[0], 9))
        
        is_nan = np.isnan(shear_data[:,0])
        not_nan = ~is_nan
        shear_data = shear_data[not_nan]
        
        for s, shear in enumerate(shear_data[not_nan]):
            print('Working : {0:0.0f} of {1:0.0f}'.format(s+1, shear_data.shape[0]))
            conArr = shear[0:3]
            
            sum_abs = np.sum( np.abs( met_data[:,0:3] - conArr ), axis=1 )
            idx = np.argmin(sum_abs)
            if sum_abs[idx] == 0:
                met_data_new[idx] = np.r_[met_data[idx], shear[3]]
                
        hf_path = os.path.join(dd.base_path, 'vmecTools', 'profile_analysis', 'figures', 'metric_data_new.h5')
        with hf.File(hf_path, 'w') as hf_file:
            met_data = hf_file['metric data'][:]
    
    if True:
        
        metrics = ['eps_effective','qhs_booz','modB_average','F_kappa','G_ss','avg_shear']
        
        core_mets = ['eps_effective','qhs_booz','modB_average']
        mid_mets = ['F_kappa','G_ss','avg_shear']
        
        metric_data_full = np.empty((1561*729, len(metrics)+3))
        # metric_data_full = np.empty((20*729, len(metrics)+3))
        metric_data_full[:] = np.nan
        
        for i in range(1561):
            mainID = 'main_coil_{0:0.0f}'.format(i)
            print('Working : ' + mainID)
            conPath = os.path.join('D:\\','HSX_Configs',mainID)
            
            metric_data = np.empty((729, len(metrics)+3))
            metric_data[:] = np.nan
            
            for m, metric in enumerate(metrics):
                try:
                    pro = profile_data(conPath, 'set_1', metric, name='conData.h5')
                except OSError:
                    print('   Skipped : '+metric)
                    continue
                
                print('   '+metric)
                
                is_nan = np.isnan(pro.data_profile[:,pro.begIdx])
                not_nan = ~is_nan
                
                for p_idx, pro_datum in enumerate(pro.data_profile[not_nan]):
                    if any([met == metric for met in core_mets]):
                        s_min = np.argmin( np.abs( pro.s_dom - 0 ) )
                        s_max = np.argmin( np.abs( pro.s_dom - 0.25 ) )
                    else:
                        s_min = np.argmin( np.abs( pro.s_dom - 0.0625 ) )
                        s_max = np.argmin( np.abs( pro.s_dom - 0.5625 ) )
                    
                    roa_dom = np.sqrt(pro.s_dom[s_min:s_max+1])
                    roa_len = np.trapz(np.ones(roa_dom.shape), roa_dom)
            
                    conArr = np.array([i,1,pro_datum[0]])
                    if metric == 'eps_effective':                        
                        prof = pro_datum[pro.begIdx+s_min:pro.begIdx+s_max+1]
                        metric_data[p_idx, 0:4] = np.r_[conArr, np.trapz(prof, roa_dom) / roa_len]
                      
                    else:
                        sum_abs = np.nansum( np.abs( metric_data[:,0:3] - conArr ), axis=1 )
                        p_idx = np.argmin(sum_abs)
                        if sum_abs[p_idx] == 0:                           
                            prof = pro_datum[pro.begIdx+s_min:pro.begIdx+s_max+1]
                            metric_data[p_idx, m+3] = np.trapz(prof, roa_dom) / roa_len
                      
                        else:
                            conID = '-'.join(['{0:0.0f}'.format(c) for c in conArr])
                            print('    Configuration ID '+conID+' is not found for metric '+metric+'.')
                            
            metric_data_full[i*729:(i+1)*729] = metric_data
                          
        hf_path = os.path.join(dd.base_path, 'vmecTools', 'profile_analysis', 'figures', 'metric_data_new.h5')
        with hf.File(hf_path, 'w') as hf_file:
            hf_file.create_dataset('shear', data=metric_data_full)
    
