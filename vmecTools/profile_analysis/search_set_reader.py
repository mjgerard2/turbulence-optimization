# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 10:26:32 2020

@author: micha
"""


import h5py as hf
import numpy as np

import metric_reader as metR

from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import directory_dict as dd
import plot_define as pd


def extract_search_set(dataPath, dataName, saveName, rng_x, rng_y):
    met_comp = metR.metric_comparison(dataPath, dataName)
    
    limit_set = met_comp.limiting_range(rng_x, rng_y)
    
    if limit_set.shape[0] != 0:
        
        if met_comp.met_names[0] == 'eps':
            lim_idx_1 = 0
            key_idx_1 = 1
            met_1 = 'qhs'
        elif met_comp.met_names[0] == 'qhs':
            lim_idx_1 = 1
            key_idx_1 = 0
            met_1 = 'eps'
            
        if met_comp.met_names[1] == 'Bk':
            lim_idx_2 = 2
            key_idx_2 = 3
            met_2 = 'GssKn'
        elif met_comp.met_names[1] == 'GssKn':
            lim_idx_2 = 3
            key_idx_2 = 2
            met_2 = 'Bk'
        
        found_data = {}
        for limit in limit_set:
            key = '{0:0.0f}-{1:0.0f}-{2:0.0f}'.format(limit[0], limit[1], limit[2])
            found_data[key] = np.full(4, np.nan)
            found_data[key][lim_idx_1] = limit[3]
            found_data[key][lim_idx_2] = limit[4]
                            
        fileName = '{0}_{1}_Comp_all.h5'.format(met_1, met_2)
        met_comp = metR.metric_comparison(dataPath, fileName)
            
        found, failed = met_comp.find_configs(limit_set[0:,0:3])
        
        for find in found:
            key = '{0:0.0f}-{1:0.0f}-{2:0.0f}'.format(find[0], find[1], find[2])
            found_data[key][key_idx_1] = find[3]
            found_data[key][key_idx_2] = find[4]
        
    hdf5_data = np.empty((limit_set.shape[0], 7))
    for k, key in enumerate(found_data):
        key_arr = np.array([int(i) for i in key.split('-')])
        hdf5_data[k] = np.r_[key_arr, found_data[key]]
    
    with hf.File(saveName, 'w') as hf_file:
        hf_file.create_dataset('con data', data=hdf5_data)


def search_set_plots(met_path, set_path, set_name):
    data_file = os.path.join(set_path, set_name)
    with hf.File(data_file, 'r') as hf_file:
        data = hf_file['con data'][:]
    
    print('\nField Line Curvature vs. Epsilon Effective\n')
    plot = pd.plot_define()
    met_comp = metR.metric_comparison(met_path, 'eps_Bk_Comp_All.h5')
    met_comp.plot_data(plot)        
    plot.plt.scatter(data[0:,3], data[0:,5], c='k', s=50, marker='*')
    saveName = os.path.join(set_path, 'eps_Fk.png')
    plot.plt.savefig(saveName)
    
    print('Flux Surface Expansion vs. Epsilon Effective\n')
    plot = pd.plot_define()
    met_comp = metR.metric_comparison(met_path, 'eps_GssKn_Comp_All.h5')
    met_comp.plot_data(plot)        
    plot.plt.scatter(data[0:,3], data[0:,6], c='k', s=50, marker='*')
    saveName = os.path.join(set_path, 'eps_GssKn.png')
    plot.plt.savefig(saveName)
    
    print('Field Line Curvature vs. QHS Metric\n')
    plot = pd.plot_define()
    met_comp = metR.metric_comparison(met_path, 'qhs_Bk_Comp_All.h5')
    met_comp.plot_data(plot)        
    plot.plt.scatter(data[0:,4], data[0:,5], c='k', s=50, marker='*')
    saveName = os.path.join(set_path, 'qhs_Fk.png')
    plot.plt.savefig(saveName)
    
    print('Flux Surface Expansion vs. QHS Metric\n')
    plot = pd.plot_define()
    met_comp = metR.metric_comparison(met_path, 'qhs_GssKn_Comp_All.h5')
    met_comp.plot_data(plot)        
    plot.plt.scatter(data[0:,4], data[0:,6], c='k', s=50, marker='*')
    saveName = os.path.join(set_path, 'qhs_GssKn.png')
    plot.plt.savefig(saveName)
    
    print('QHS Metric vs. Epsilon Effective wtih Field Line Curvature Color Scale\n')
    data = np.array( sorted(data, key=lambda x: x[5]) )
    plot = pd.plot_define()
    s_map = plot.plt.scatter(data[0:,3], data[0:,4], c=data[0::,5], s=10)
    cbar = plot.fig.colorbar(s_map, ax=plot.ax)
    cbar.ax.set_ylabel(r'$\mathcal{F}_{\kappa}/\mathcal{F}_{\kappa}^*$')
    plot.plt.xlabel(r'$\epsilon_{eff}/\epsilon_{eff}^{*}$')
    plot.plt.ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')
    plot.plt.tight_layout()
    plot.plt.savefig(os.path.join(set_path, 'eps_qhs_Fk.png'))
    
    print('QHS Metric vs. Epsilon Effective wtih Flux Surface Expansion Color Scale')
    data = np.array( sorted(data, key=lambda x: x[6]) )
    plot = pd.plot_define()
    s_map = plot.plt.scatter(data[0:,3], data[0:,4], c=data[0::,6], s=10)
    cbar = plot.fig.colorbar(s_map, ax=plot.ax)
    cbar.ax.set_ylabel(r'$\langle g_{ss}\kappa_n\rangle / \langle g_{ss}\kappa_n\rangle^*$')
    plot.plt.xlabel(r'$\epsilon_{eff}/\epsilon_{eff}^{*}$')
    plot.plt.ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')
    plot.plt.tight_layout()
    plot.plt.savefig(os.path.join(set_path, 'eps_qhs_GssKn.png'))
    

def plot_range_stats(plot, data, rng):
    from scipy.stats import mode

    data_set = data[(data[0:,6] >= rng[0]) & (data[0:,6] <= rng[1])]
    
    mode_set = mode(data_set[0:,2])
    label_job = 'Job {0:0.0f} : {1:0.1f}%'.format(mode_set[0][0], 1e2*(mode_set[1][0]/data_set.shape[0]) )
    
    main_set = mode(data_set[0:,0])
    label_main = 'main {0:0.0f} : {1:0.1f}%'.format(main_set[0][0], 1e2*(main_set[1][0]/data_set.shape[0]) )
    
    label = '({0:0.0f}, {1:0.0f}) : ({2:0.1f}, {3:0.1f})%'.format(main_set[0][0], mode_set[0][0], 1e2*(main_set[1][0]/data_set.shape[0]), 1e2*(mode_set[1][0]/data_set.shape[0]))
    #x_rng = [np.min(data[0:,5]), np.max(data[0:,5])]
    #plt1, = plot.plt.plot(x_rng, [rng[0]]*2, ls='--')
    #plot.plt.plot(x_rng, [rng[1]]*2, ls='--', c=plt1.get_color())
    
    plot.plt.scatter(data_set[0:,5], data_set[0:,6], label=label)#label_job+', '+label_main)

def read_data(set_path, set_name):
    data_file = os.path.join(set_path, set_name)
    with hf.File(data_file, 'r') as hf_file:
        #data = hf_file['con data'][:]
        data = hf_file['metric data'][:]
        
    return data

def plot_turbo_metrics(plot, data, neo_met='eps_met'):
    if neo_met == 'eps_met':
        c_lab = r'$\epsilon_{eff}/\epsilon_{eff}^{*}$'
        s_map = plot.plt.scatter(data[0:,6], data[0:,7], c=data[0:,3], norm=LogNorm( vmin=0.1, vmax=10 ), s=1, cmap='jet' )    

    elif neo_met == 'qhs_met':
        c_lab = r'$\mathcal{Q}/\mathcal{Q}^*$'
        s_map = plot.plt.scatter(data[0:,6], data[0:,7], c=data[0:, 4], s=1 )
    
    cbar = plot.fig.colorbar(s_map, ax=plot.ax)
    cbar.ax.set_ylabel(c_lab)
    
    plot.plt.xlabel(r'$\mathcal{F}_{\kappa}/\mathcal{F}_{\kappa}^{*}$')
    plot.plt.ylabel(r'$\mathcal{G}_{ss}/\mathcal{G}_{ss}^*$')
    
def plot_neo_metrtics(plot, data, turbo_met='Fk_met'):
    if turbo_met == 'Fk_met':
        idx = 5
        c_lab = r'$\mathcal{F}_{\kappa}/\mathcal{F}_{\kappa}^{*}$'
    elif turbo_met == 'Gss_met':
        idx = 6
        c_lab = r'$\mathcal{G}_{ss}/\mathcal{G}_{ss}^*$'
    
    s_map = plot.plt.scatter(data[0:,3], data[0:,4], c=data[0:,idx])
    cbar = plot.fig.colorbar(s_map, ax=plot.ax)
    cbar.ax.set_ylabel(c_lab)
    
    plot.plt.xlabel(r'$\epsilon_{eff}/\epsilon_{eff}^{*}$')
    plot.plt.ylabel(r'$\mathcal{Q}/\mathcal{Q}^*$')
    
def write_meta_set(saveName, desc, data):
     with open(saveName, 'w') as file:
         file.write(desc+'\n\n')
         file.write('mainID-setID-jobID Epsilon_Effective QHS_Metric Field_Line_Curvature Flux_Expansion\n')
         for datum in data:
             file.write( '{0:0.0f}-{1:0.0f}-{2:0.0f} {3} {4} {5} {6}\n'.format( datum[0], datum[1], datum[2], datum[3], datum[4], datum[5], datum[6] ) )

def lin_func(x, m, b):
    return m * x + b

if __name__ == '__main__':
    met_path = os.path.join(dd.base_path, 'vmecTools', 'profile_analysis', 'figures')
    set_name = 'metric_data.h5'
    
    data = read_data(met_path, set_name)
    data_subset = np.array( sorted( data, key=lambda x: x[3], reverse=True ) )
    '''
    Gss_bins = 26
    Gss_dom = np.linspace( np.min( data_subset[0:,7] ), 2.75, Gss_bins)
    
    bound_pnts = np.empty( ( Gss_dom.shape[0] - 1, 2, 2 ) )
    for idx, Gss_bnd_low in enumerate(Gss_dom[0:-1]):
        idx_nxt = idx + 1
        Gss_bnd_hgh = Gss_dom[idx_nxt]
        
        data_sub = data_subset[ ( data_subset[0:,7] >= Gss_bnd_low ) & ( data_subset[0:,7] < Gss_bnd_hgh), : ]
        
        idx_low = np.argmin( data_sub[0:,6] )
        idx_hgh = np.argmax( data_sub[0:,6] )
        
        bound_pnts[idx] = np.array([ [ data_sub[idx_low, 6], data_sub[idx_hgh, 6] ] , 
                                     [ data_sub[idx_low, 7], data_sub[idx_hgh, 7] ] ])
    
    log_y_low = np.log( bound_pnts[0:,1,0] )
    log_y_hgh = np.log( bound_pnts[0:,1,1] )
    
    log_x_low = np.log( bound_pnts[0:,0,0] )
    log_x_hgh = np.log( bound_pnts[0:,0,1] )
    
    fit_low, con_low = curve_fit( lin_func, log_x_low, log_y_low )
    fit_hgh, con_hgh = curve_fit( lin_func, log_x_hgh, log_y_hgh )
    
    y_min = np.min( data_subset[0:,7] )
    y_max = np.max( data_subset[0:,7] )
    
    x_hgh_min = ( 1. / fit_hgh[0] ) * ( np.log( y_min ) - fit_hgh[1] )
    x_hgh_max = ( 1. / fit_hgh[0] ) * ( np.log( y_max ) - fit_hgh[1] )

    x_dom_hgh = np.linspace( x_hgh_min, x_hgh_max, 1000)
    y_dom_hgh = lin_func( x_dom_hgh, fit_hgh[0], fit_hgh[1] )
    
    x_low_min = ( 1. / fit_low[0] ) * ( np.log( y_min ) - fit_low[1] )
    x_low_max = ( 1. / fit_low[0] ) * ( np.log( y_max ) - fit_low[1] )
    
    x_dom_low = np.linspace( x_low_min, x_low_max, 1000)
    y_dom_low = lin_func( x_dom_low, fit_low[0], fit_low[1] )
    
    crn_bot = np.exp( np.array([ [ x_low_min, x_hgh_min ],
                                 [ y_dom_low[0], y_dom_hgh[0] ] ]) )
    
    crn_top = np.exp( np.array([ [ x_low_max, x_hgh_max ],
                                 [ y_dom_low[-1], y_dom_hgh[-1] ] ]) )
    
    ngrid = 11
    
    x_equiv = ( fit_hgh[1] - fit_low[1] ) / ( fit_low[0] - fit_hgh[0] )
    m_set = np.flip( np.linspace( fit_hgh[0], fit_low[0], ngrid ) )
    b_set = ( fit_hgh[0] - m_set ) * x_equiv + fit_hgh[1]
    '''
    '''
    Fk_hgh = np.array([ 0.91, 1.18 ])
    Fk_low = ( 1. / fit_low[0] ) * ( fit_hgh[0] * Fk_hgh + fit_hgh[1] - fit_low[1] )
    
    Gss_hgh = np.exp( fit_hgh[1] + fit_hgh[0] * Fk_hgh )
    Gss_low = np.exp( fit_low[1] + fit_low[0] * Fk_low )
    
    Fk_bot = np.linspace( Fk_low[0], Fk_hgh[0], ngrid )
    Fk_top = np.linspace( Fk_low[1], Fk_hgh[1], ngrid )
    
    Gss_dom = np.linspace( Gss_low[0], Gss_low[1], ngrid )
    Fk_dom_low = ( np.log(Gss_dom) - fit_low[1] ) / fit_low[0]
    Fk_dom_hgh = ( np.log(Gss_dom) - fit_hgh[1] ) / fit_hgh[0]
    grid_horz = np.stack( ( Fk_dom_low, Fk_dom_hgh, Gss_dom, Gss_dom ), axis=1 )
    
    grid_vert = np.empty( ( ngrid, 2, 2 ) )
    for i in range(ngrid):        
        Fk_set = np.array([ Fk_bot[i], Fk_top[i] ])
        Gss_set = m_set[i] * Fk_set + b_set[i]
        
        grid_vert[i, 0] = Fk_set
        grid_vert[i, 1] = Gss_low
    '''
    '''
    corners = np.exp( np.array([ [ x_low_min, y_dom_low[0] ], 
                                 [ x_low_max, y_dom_low[-1] ],
                                 [ x_hgh_min, y_dom_hgh[0] ],
                                 [ x_hgh_max, y_dom_hgh[-1] ] ]) )
    
    exp_data = np.stack( ( np.exp(b_set), m_set ), axis=1 )
    
    filePath = os.path.join(met_path, 'eps_eff_valley', 'expBox_data_new.h5')
    with hf.File(filePath, 'w') as hf_file:
        hf_file.create_dataset( 'exponential data', data=exp_data )
        hf_file.create_dataset( 'corner data', data=corners )
    '''
    '''
    ### In Transit ###
    plot = pd.plot_define()
    plot_turbo_metrics(plot, data_subset, neo_met='eps_met')
    
    #for bnd_pnts in bound_pnts:
    #    plot.plt.scatter( bnd_pnts[0], bnd_pnts[1], c='k', s=10 )
        
    plot.plt.plot( np.exp( x_dom_hgh ), np.exp( y_dom_hgh ), c='k' )
    plot.plt.plot( np.exp( x_dom_low ), np.exp( y_dom_low ), c='k' )
    plot.plt.plot( crn_bot[0], crn_bot[1], c='k' )
    plot.plt.plot( crn_top[0], crn_top[1], c='k' )
    
    for i in range(ngrid-2):
        idx = i + 1
        
        x_min = ( 1. / m_set[idx] ) * ( np.log( y_min ) - b_set[idx] )
        x_max = ( 1. / m_set[idx] ) * ( np.log( y_max ) - b_set[idx] )
    
        x_dom = np.linspace( x_min, x_max, 1000 )
        y_dom = lin_func( x_dom, m_set[idx], b_set[idx] )
        
        plot.plt.plot( np.exp( x_dom ), np.exp( y_dom ), c='k', ls='--' )
        
    #plot.plt.xscale('log')
    #plot.plt.yscale('log')
    
    plot.plt.show()
    '''
    '''
    plot = pd.plot_define()
    plt, ax, fig = plot.plt, plot.ax, plot.fig
    
    s_map = ax.scatter( data_subset[0:,6], data_subset[0:,7], c=data_subset[0:,3], s=1, norm=LogNorm( vmin=1e-1, vmax=10 ) )
    cbar = fig.colorbar(s_map, ax=ax)
    cbar.ax.set_ylabel(r'$ \epsilon_{eff} / \epsilon_{eff}^* $')
    
    npts = 100
    nodes = exp_data.shape[0]
    
    Gss_low, Gss_hgh = corners[0,1], corners[1,1]
    Gss_dom = np.linspace( Gss_low, Gss_hgh, nodes )
    
    Fk_dom_low = np.linspace( corners[0,0], corners[2,0], nodes )
    Fk_dom_hgh = np.linspace( corners[1,0], corners[3,0], nodes )
    
    x_dom_low = np.linspace( corners[0, 0], corners[1, 0], npts )
    y_dom_low = exp_data[0,0] * np.exp( exp_data[0,1] * x_dom_low )
    
    x_dom_hgh = np.linspace( corners[2, 0], corners[3, 0], npts )
    y_dom_hgh = exp_data[-1,0] * np.exp( exp_data[-1,1] * x_dom_hgh )
    
    ax.plot( x_dom_low, y_dom_low, c='k' )
    ax.plot( x_dom_hgh, y_dom_hgh, c='k' )
     
    ax.plot ( Fk_dom_low, [Gss_low] * nodes, c='k' )
    ax.plot ( Fk_dom_hgh, [Gss_hgh] * nodes, c='k' )
    
    for i in range( exp_data.shape[0]-2 ):
        idx = i + 1
        exp_datum = exp_data[idx]
        
        # Vertical Grid 
        Fk_lim = ( 1. / exp_datum[1] ) * ( np.log( Gss_hgh ) - np.log( exp_datum[0] ) )
        Fk_dom_idx = np.linspace( Fk_dom_low[idx], Fk_lim, npts )
        Gss_dom_idx = exp_datum[0] * np.exp( exp_datum[1] * Fk_dom_idx )
        ax.plot( Fk_dom_idx, Gss_dom_idx, c='k', ls='--' )
        
        # Horizontal Grid
        Fk_low = ( 1. / exp_data[0, 1] ) * ( np.log( Gss_dom[idx] ) - np.log( exp_data[0, 0] ) )
        Fk_hgh = ( 1. / exp_data[-1, 1] ) * ( np.log( Gss_dom[idx] ) - np.log( exp_data[-1, 0] ) )
        ax.plot( [Fk_low, Fk_hgh], [Gss_dom[idx]]*2, c='k', ls='--' )
    
    ax.set_xlabel(r'$ \mathcal{F}_{\kappa} / \mathcal{F}_{\kappa}^* $')
    ax.set_ylabel(r'$ \mathcal{G}_{ss} / \mathcal{G}_{ss}^* $')
    
    plt.grid()
    plt.tight_layout()
    plt.show()
    '''
    
    plot_3D = False
    if plot_3D:
        plot = pd.plot_define(proj3D=True)
        plt, ax, fig = plot.plt, plot.ax, plot.fig
        
        s_map = ax.scatter( data_subset[0:,6], data_subset[0:,7], data_subset[0:,3], c=data_subset[0:,3], norm=LogNorm(), s=1 )
        ax.view_init(25, 60)
        
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'$ \epsilon_{eff} / \epsilon_{eff}^* $')
        
        ax.set_xlabel(r'$ \mathcal{F}_{\kappa} / \mathcal{F}_{\kappa}^* $')
        ax.set_ylabel(r'$ \mathcal{G}_{ss} / \mathcal{G}_{ss}^* $')
        ax.set_zlabel(r'$ \epsilon_{eff} / \epsilon_{eff}^* $')
        
        plt.show()
    
    else:
        plot = pd.plot_define()
        plt, ax, fig = plot.plt, plot.ax, plot.fig
        
        s_map = ax.scatter( data_subset[0:,6], data_subset[0:,7], c=data_subset[0:,3], s=1 , norm=LogNorm( ), cmap='jet' )
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'$ \epsilon_{eff} / \epsilon_{eff}^* $')
        
        #ax.scatter( bound_pnts[0:,0,0], bound_pnts[0:,1,0], c='k', s=10 )
        #ax.scatter( bound_pnts[0:,0,1], bound_pnts[0:,1,1], c='k', s=10 )
        
        #ax.plot( Fk_low, Gss_low, c='k' )
        #ax.plot( Fk_hgh, Gss_hgh, c='k' )
        
        #ax.plot( [Fk_low[0], Fk_hgh[0]], [Gss_low[0], Gss_hgh[0]], c='k' )
        #ax.plot( [Fk_low[1], Fk_hgh[1]], [Gss_low[1], Gss_hgh[1]], c='k' )
        '''
        for i in range(ngrid-2):
            idx = i + 1
            ax.plot( grid_vert[idx,0], grid_vert[idx,1], c='k', ls='--' )
            ax.plot( grid_horz[idx,0:2], grid_horz[idx,2:], c='k', ls='--' )
        '''
        #ax.set_yscale('log')
        
        ax.set_xlabel(r'$ \mathcal{F}_{\kappa} / \mathcal{F}_{\kappa}^* $')
        ax.set_ylabel(r'$ \mathcal{G}_{ss} / \mathcal{G}_{ss}^* $')
        
        plt.grid()
        plt.tight_layout()
        
        saveName = os.path.join( 'D:\\', 'GENE', 'eps_valley', 'figures', 'full_scatter.png' )
        plt.savefig( saveName )
    