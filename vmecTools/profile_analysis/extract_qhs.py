# -*- coding: utf-8 -*-
"""
Created on Tue Sep  1 00:11:03 2020

@author: micha
"""

import numpy as np
import profile_extraction as pe

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import directory_dict as dd
'''
### Range of Bk Overlap configurations ###
mainIDs = np.arange(1084, 1561)
setIDs = np.array([1])
pe.extract_GssKn_set(os.path.join('D:\\','HSX_Configs'), mainIDs, setIDs)
'''
### Bk Overlap Superset ###
main_num = 1561
job_num = 729

conIDs = np.empty((main_num*job_num, 3), dtype=np.int)
for i in range(main_num):
    for j in range(job_num):
        conIDs[i*job_num + j] = np.array([i, 1, j])

superPath = os.path.join(dd.exp_ext_base, 'profile_data.h5')
pe.extract_superSet(superPath, 'GssKn_metric', 'all', conIDs, basePath=os.path.join('D:\\','HSX_Configs'))
