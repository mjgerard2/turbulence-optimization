# -*- coding: utf-8 -*-
"""
Created on Mon May  4 11:46:16 2020

@author: micha
"""

import numpy as np
import h5py as hf
import profile_reader as pr

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.curveB_tools as cBT
import vmecTools.wout_files.coord_convert as cc

import directory_dict as dd


def extract_superSet(superPath, data_key, set_name, config_list, basePath=os.path.join('D:\\','HSX_Configs'), nCoil=6):
    """ Extract the specified list of configuration profile data from the 
    conData.h5 files in the databse, and store the extracted data locally in a
    separate hdf5 file.  These data sets are referred to as superSets.  The
    configuration list does not need to be ordered.  This script will order
    the list by main ID first, in ascending order.  Then, within an ordered 
    main set, it will order the set IDs, also in ascending order.
    
    Parameters
    ----------
    superPath : str
        Directory path to super set hdf5 file.
    data_key : str
        Profile data to be extracted.
    set_name : str
        New super set name in hdf5 file.
    config_list : arr
        Numpy array in which the main coil, set and job IDs are all stored.
        [mainId, setID, jobID]
    basePath : str, optional
        Base directory from which conData.h5 files will be searched. Default 
        is D:\\HSX_Configs
    nSurf : int
        Number of flux surfaces in profile data. Default is 128
    nCoil : int
        Number of coil currents stored in coil data.  Default is 6
    
    Raises
    ------
    KeyError 
        Data key being created already exists.
    ValueError
        Configuration identifier does not exist.
    """
    keys_127 = ['eps_effective']
    keys_128 = ['qhs_metric','modB_average','Bk_overlap','Gss_metric','GssKn_metric']
    
    if any(ikey == data_key for ikey in keys_127):
        nSurf = 127 
    elif any(ikey == data_key for ikey in keys_128):
        nSurf = 128
    else:
        raise KeyError(data_key+' is not a valid data_key.')
        
    ### Sort Configuration List ###
    config_list = np.array( sorted(config_list, key=lambda x: x[0]) )
    main_bounds = np.array([0, config_list.shape[0]])
    main_chng = np.insert(main_bounds, 1, 1 + np.where(config_list[:-1,0] != config_list[1:,0])[0])
    for b, idxBeg in enumerate(main_chng[0:-1]):
        idxEnd = main_chng[b+1]
        config_list[idxBeg:idxEnd] = np.array( sorted(config_list[idxBeg:idxEnd], key=lambda x: x[1]) )
                
    ### Check if SuperSet File Exists ###
    if os.path.isfile(superPath):
        file_exists = True
        with hf.File(superPath, 'r') as hf_file:
            set_chk = set_name in hf_file['superSet']
            if set_chk:
                crnt_chk = 'config' in hf_file['superSet/'+set_name] 
                data_chk = data_key in hf_file['superSet/'+set_name]
            else:
               crnt_chk = False
               data_chk = False 
               
    else:
        file_exists = False
        crnt_chk = False
        data_chk = False
        
    if data_chk:
        ### Specified Data Key Already in Use ###
        raise KeyError('superSet'+set_name+'/'+data_key+' already exists in '+superPath)
        
    else:
        ### Instantiate Numpy Array to Populate ###
        super_data = np.empty((len(config_list), int(3+nSurf)))
        super_data[:] = np.nan
        if not crnt_chk:
            super_crnt = np.empty((len(config_list), int(3+nCoil)))
            super_crnt[:] = np.nan
        
        ### Iterate Over MainID in Ascending Order ###
        main_bounds = np.array([0, config_list.shape[0]])
        main_chng = np.insert(main_bounds, 1, 1 + np.where(config_list[:-1,0] != config_list[1:,0])[0])
        for i, idxEnd in enumerate(main_chng[1::]):
            idxBeg = main_chng[i]                
            mainID = 'main_coil_{}'.format(int(config_list[idxBeg, 0]))
            mainPath = os.path.join(basePath, mainID)  
            
            ### Iterate Over SetID in Ascending Order, Within Current MainID ###
            list_set = config_list[idxBeg:idxEnd]
            set_bounds = np.array([0, idxEnd-idxBeg])
            set_chng = np.insert(set_bounds, 1, 1 + np.where(list_set[:-1,1] != list_set[1:,1])[0])
            for j, jdxEnd in enumerate(set_chng[1::]):
                jdxBeg = set_chng[j]
                setID = 'set_{}'.format(int(list_set[jdxBeg,1]))
                
                ### Check if Data Key Exists in conData.h5 File
                with hf.File(os.path.join(mainPath,'conData.h5'), 'r') as hf_file:
                    data_chk = data_key in hf_file[setID]
                
                if data_chk:
                    ### Read Specified Jobs ###
                    print('Reading in profiles from '+os.path.join(mainPath, setID))
                    main = pr.profile_data(mainPath, setID, data_key)
                    list_job = list_set[jdxBeg:jdxEnd]
                    for k, con_id in enumerate(list_job):
                        job_id = int(con_id[2])
                        try:
                            crnt_idx, data_idx = main.find_config(job_id)
                            
                            ### Broadcast Job To Numpy Array ###
                            ind = idxBeg + jdxBeg + k               
                            super_data[ind] = np.insert(np.insert(main.data_profile[int(data_idx)], 0, con_id[1]), 0, con_id[0])
                            if not crnt_chk:
                                super_crnt[ind] = np.insert(np.insert(main.crnt_profile[int(crnt_idx)], 0, con_id[1]), 0, con_id[0])

                        except ValueError:
                            ind = idxBeg + jdxBeg + k  
                            super_data[ind,0:3] = con_id
                            if not crnt_chk:
                                super_crnt[ind,0:3] = con_id
                            
                            print('==================================================\n'+
                                  'Job Identifier {0}-{1}-{2} does not seem to exist.\n'.format(int(con_id[0]),int(con_id[1]),int(con_id[2]))+
                                  '==================================================')     
               
    ### Write Data to HDF5 File ###           
    if file_exists:
        with hf.File(superPath, 'a') as hf_file:
            hf_file.create_dataset('superSet/'+set_name+'/'+data_key, data=super_data)
            if not crnt_chk:
                hf_file.create_dataset('superSet/'+set_name+'/config', data=super_crnt)
             
    else:
        with hf.File(superPath, 'w') as hf_file:
            hf_file.create_dataset('superSet/'+set_name+'/'+data_key, data=super_data)
            hf_file.create_dataset('superSet/'+set_name+'/config', data=super_crnt)
    
                
def extract_meanB_set(basePath, mainIDs, setIDs, nSurf=128, u_num=11):
    """ Extract the average magnitude of the magnetic field across all flux
    surfaces from the specified wout file.

    Parameters
    ----------
    basePath : str
        Directory path to working database.
    mainIDs : arr
        1D array of main coil configuration IDs.
    setIDs : arr
        1D array of set IDs.
    nSurf : int, optional
        Number of flux surfaces. Default is 128.
    """    
    for mID in mainIDs:
        main_id = 'main_coil_{}'.format(mID)
        mainPath = os.path.join(basePath, main_id)        
        for sID in setIDs:
            set_id = 'set_{}'.format(sID)
            setPath = os.path.join(mainPath, set_id)
            
            with hf.File(os.path.join(mainPath, 'conData.h5'), 'a') as hf_file:
                data_chk = set_id+'/modB_average' in hf_file
            
            if not data_chk:
                print('Working in '+setPath)
                job_list = [f.name for f in os.scandir(setPath) if f.is_dir()]
                Bmod_profile = np.empty((len(job_list), nSurf+1))
                for jNum, job_name in enumerate(job_list):
                    jID = int(job_name.split('_')[1])
                    path = os.path.join(setPath, job_name)
                    
                    if os.path.isfile(os.path.join(path,'wout_HSX_main_opt0.nc')):
                        name = 'wout_HSX_main_opt0.nc'
                    elif os.path.isfile(os.path.join(path,'wout_HSX_aux_opt0.nc')):
                        name = 'wout_HSX_aux_opt0.nc'
                    else:
                        print('No readable wout file in '+path)
                        continue
                    
                    wout = wr.readWout(path, name=name, diffAmps=True)
                    
                    u_dom = np.linspace(0, 2*np.pi, u_num)
                    v_dom = np.linspace(0, 2*np.pi, wout.nfp*u_num)
    
                    wout.transForm_3D(u_dom, v_dom, ['R','Jacobian','dR_ds','dR_du','dR_dv','dZ_ds','dZ_du','dZ_dv','Bmod'])
                    
                    R = wout.invFourAmps['R']
                    jacob = wout.invFourAmps['Jacobian']
                    
                    dR_ds = wout.invFourAmps['dR_ds']
                    dR_du = wout.invFourAmps['dR_du']
                    dR_dv = wout.invFourAmps['dR_dv']
                    
                    dZ_ds = wout.invFourAmps['dZ_ds']
                    dZ_du = wout.invFourAmps['dZ_du']
                    dZ_dv = wout.invFourAmps['dZ_dv']
                    
                    B = wout.invFourAmps['Bmod']
                    
                    du = u_dom[1] - u_dom[0]
                    dv = v_dom[1] - v_dom[0]
                    da = du * dv
                                
                    det_gxx_inv = 1. /  (R * R * ((dR_du * dZ_ds - dR_ds * dZ_du)**2))
                    g_ss = det_gxx_inv * ((dR_du * dZ_dv - dR_dv * dZ_du)**2 + R * R * (dZ_du * dZ_du + dR_du * dR_du))
                    da_norm = np.abs(da * jacob[1::]) * np.sqrt(g_ss[1::])
                    B_da = B[1::] * da_norm
                    
                    Bmod_avg = np.zeros(wout.s_num)
                    for idx in range(1,wout.s_num-1):
                        SA = np.sum(da_norm[idx,0:-1,0:-1])
                        Bmod_avg[idx] = np.sum(B_da[idx,1:,1:]) / SA

                    Bmod_profile[jNum] = np.insert(Bmod_avg, 0, jID)
                
                with hf.File(os.path.join(mainPath, 'conData.h5'), 'a') as hf_file:
                    hf_file.create_dataset(set_id+'/modB_average', data=Bmod_profile)
                    
                
def extract_overlap_set(basePath, mainIDs, setIDs, nSurf=128, u_num=11):    
    for mID in mainIDs:
        main_id = 'main_coil_{}'.format(mID)
        mainPath = os.path.join(basePath, main_id)
        
        for sID in setIDs:
            set_id = 'set_{}'.format(sID)
            setPath = os.path.join(mainPath, set_id)
            
            with hf.File(os.path.join(mainPath, 'conData.h5'), 'r') as hf_file:
                data_chk = set_id+'/F_kappa' in hf_file
            
            if not data_chk:
                print('Working in '+setPath)
                job_list = [f.name for f in os.scandir(setPath) if f.is_dir()]
                F_kappa_set = np.empty((len(job_list), 1+nSurf))
                for jNum, job_name in enumerate(job_list):
                    jID = int(job_name.split('_')[1])
                    path = os.path.join(setPath, job_name)
                    
                    if os.path.isfile(os.path.join(path,'wout_HSX_main_opt0.nc')):
                        name = 'wout_HSX_main_opt0.nc'
                    elif os.path.isfile(os.path.join(path,'wout_HSX_aux_opt0.nc')):
                        name = 'wout_HSX_aux_opt0.nc'
                    else:
                        print('No readable wout file in '+path)
                        continue
                    
                    F_kappa = extract_Bk_overlap(path, name=name, u_num=u_num)
                    F_kappa_set[jNum] = np.insert(F_kappa, 0, jID)
                
                with hf.File(os.path.join(mainPath, 'conData.h5'), 'a') as hf_file:
                    hf_file.create_dataset(set_id+'/F_kappa', data=F_kappa_set)
                    
                    
def extract_GssKn_set(basePath, mainIDs, setIDs, nSurf=128, u_num=11):    
    for mID in mainIDs:
        main_id = 'main_coil_{}'.format(mID)
        mainPath = os.path.join(basePath, main_id)
        
        for sID in setIDs:
            set_id = 'set_{}'.format(sID)
            setPath = os.path.join(mainPath, set_id)
            
            with hf.File(os.path.join(mainPath, 'conData.h5'), 'r') as hf_file:
                data_chk = set_id+'/G_ss' in hf_file
            
            if not data_chk:
                print('Working in '+setPath)
                job_list = [f.name for f in os.scandir(setPath) if f.is_dir()]
                G_ss_set = np.empty((len(job_list), 1+nSurf))
                for jNum, job_name in enumerate(job_list):
                    jID = int(job_name.split('_')[1])
                    path = os.path.join(setPath, job_name)
                    
                    if os.path.isfile(os.path.join(path,'wout_HSX_main_opt0.nc')):
                        name = 'wout_HSX_main_opt0.nc'
                    elif os.path.isfile(os.path.join(path,'wout_HSX_aux_opt0.nc')):
                        name = 'wout_HSX_aux_opt0.nc'
                    else:
                        print('No readable wout file in '+path)
                        continue
                    
                    G_ss = extract_GssKn_metric(path, name=name, u_num=u_num)
                    G_ss_set[jNum] = np.insert(G_ss, 0, jID)
                
                with hf.File(os.path.join(mainPath, 'conData.h5'), 'a') as hf_file:
                    hf_file.create_dataset(set_id+'/G_ss', data=G_ss_set)
                    
                    
def extract_Gss_set(basePath, mainIDs, setIDs, nSurf=128, u_num=11):
    for mID in mainIDs:
        main_id = 'main_coil_{}'.format(mID)
        mainPath = os.path.join(basePath, main_id)
        
        for sID in setIDs:
            set_id = 'set_{}'.format(sID)
            setPath = os.path.join(mainPath, set_id)
            
            with hf.File(os.path.join(mainPath, 'conData.h5'), 'r') as hf_file:
                data_chk = set_id+'/Gss_metric' in hf_file
            
            if not data_chk:
                print('Working in '+setPath)
                job_list = [f.name for f in os.scandir(setPath) if f.is_dir()]
                over_profile = np.empty((len(job_list), 1+nSurf))
                for jNum, job_name in enumerate(job_list):
                    jID = int(job_name.split('_')[1])
                    path = os.path.join(setPath, job_name)
                    
                    if os.path.isfile(os.path.join(path,'wout_HSX_main_opt0.nc')):
                        name = 'wout_HSX_main_opt0.nc'
                    elif os.path.isfile(os.path.join(path,'wout_HSX_aux_opt0.nc')):
                        name = 'wout_HSX_aux_opt0.nc'
                    else:
                        print('No readable wout file in '+path)
                        continue
                    
                    keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs', 'Bu', 'Bv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']    
        
                    wout = wr.readWout(path, name=name, diffAmps=True, curvAmps=True)
                    
                    u_dom = np.linspace(0, 2*np.pi, u_num)
                    v_dom = np.linspace(0, 2*np.pi, wout.nfp*u_num)
                    
                    wout.transForm_3D(u_dom, v_dom, keys)
                    gss_metric = cc.calc_Gss_metric(wout)
                    over_profile[jNum] = np.insert(gss_metric, 0, jID) 
                    
                with hf.File(os.path.join(mainPath, 'conData.h5'), 'a') as hf_file:
                    hf_file.create_dataset(set_id+'/Gss_metric', data=over_profile)
                    

def set_num_jobs(path, setID, name='conData.txt'):
    """ Reads the number of jobs within the specified set from the specified
    metadata file.

    Parameters
    ----------
    path : str
        directory path of metadata file
    setID : str
        specified set identifier 
    name : str, optional
        metadata file name (default is 'conData.txt')

    Raises
    ------
    ValueError
        The specified set identifier does not exists.

    Returns
    -------
    int
        number of jobs within set specified
    """
    conPath = os.path.join(path, name)
    with open(conPath, 'r') as file:
        for line in file:
            line = line.strip()
            line = line.split()
            if line:
                if line[-1] == setID:
                    return int(line[0])
                             
    raise ValueError(setID+' is not an existing set identifier')

def extract_Bk_overlap(path, name='wout_HSX_main_opt0.nc', u_num=11):    
    keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs', 'Bu', 'Bv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']    
    
    wout = wr.readWout(path, name=name, diffAmps=True, curvAmps=True)
    
    u_dom = np.linspace(0, 2*np.pi, u_num)
    v_dom = np.linspace(0, 2*np.pi, wout.nfp*u_num)
    
    wout.transForm_3D(u_dom, v_dom, keys)
    curB = cBT.BcurveTools(wout)
    
    curB.calc_curvature()
    curB.calc_Bk_overlap()
    
    return curB.Bk_overlap

def extract_GssKn_metric(path, name='wout_HSX_main_opt0.nc', u_num=11):
    keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs', 'Bu', 'Bv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']    
    
    wout = wr.readWout(path, name=name, diffAmps=True, curvAmps=True)
    
    u_dom = np.linspace(0, 2*np.pi, u_num)
    v_dom = np.linspace(0, 2*np.pi, wout.nfp*u_num)
    
    wout.transForm_3D(u_dom, v_dom, keys)
    curB = cBT.BcurveTools(wout)
    
    curB.calc_curvature()
    curB.calc_Gss_Kn_metric()
    
    return curB.GssKn_metric

def extract_meanB(path, name='wout_HSX_main_opt0.nc', u_num=10, v_num=40):
    """ Extract the Average B field on each flux surface.

    Parameters
    ----------
    path : TYPE
        DESCRIPTION.
    name : TYPE, optional
        DESCRIPTION. The default is 'wout_HSX_main_opt0.nc'.
    u_num : TYPE, optional
        DESCRIPTION. The default is 10.
    v_num : TYPE, optional
        DESCRIPTION. The default is 40.

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    u_dom = np.linspace(0, 2*np.pi, u_num)
    v_dom = np.linspace(0, 2*np.pi, v_num)
    
    wout = wr.readWout(path, name=name)
    wout.transForm_3D(u_dom, v_dom, ['Bmod'])

    Bmod = wout.invFourAmps['Bmod']
    return np.array([np.mean(B) for B in Bmod])


def extract_qhsMetric(path, name='wout_HSX_main_opt0.nc'):
    """ Extract the QHS metric profile for the specified wout file.

    Parameters
    ----------
    path : str
        Directory path to wout file.
    name : str, optional
        Name of wout file. The default is 'wout_HSX_main_opt0.nc'.

    Returns
    -------
    Arr
        QHS metric profile.

    """
    wout = wr.readWout(path)
    return wout.qhs_metric()

def extract_epsEff(path, name='neo_out.HSX_main.00000'):
    """ Extracts epsilon effective data from neo_out file.
    
    Parameters
    ----------
    path : str
        directory path to neo_out file
    name : str (optional)
        name of neo_out file. Default is 'neo_out.HSX_main.00000'.
    """
    file_name = os.path.join(path, name)
    
    if os.path.isfile(file_name):
        with open(file_name, 'r') as neoFile:
            lines = neoFile.readlines()
            
        neo = np.empty(127)
        for n, line in enumerate(lines):
            line = line.strip()
            line = line.split()
            
            neo[n] = float(line[1])
        
        return neo
    
    else:
        raise NameError(file_name+' does not exist.')
        
def extract_shear(path, name='wout_HSX_main_opt0.nc'):
    """ Extract the magnetic shear from a wout file.

    Parameters
    ----------
    path : str
        Full directory path to the parent directory of the wout file.
    name : str, optional
        Name of the wout file. The default is 'wout_HSX_main_opt0.nc'.
    
    Returns
    -------
    Array
        Average shear profile.
    """
    wout = wr.readWout(path, name=name, iotaPro=True)
    iota = wout.iota
    s_dom = wout.s_dom
    
    return -np.gradient(iota, s_dom)

def extract_shear_from_set(main_path, setID, name_out='conData.h5'):
    """ Extracts average magnetic shear from wout files within the specified 
    set, then saves data to specified hdf5 file.

    Parameters
    ----------
    main_path : str
        directory path of main coil configuration
    setID : str
        set identifier for specified set
    name_out : str, optional
        name of output hdf5 file (default is 'conData.h5')
    """
    ### Get maximal number of jobs ###
    meta_file = os.path.join(main_path, 'conData.txt')
    with open(meta_file, 'r') as my_file:
        lines = my_file.readlines()
        for line in lines:
            line = line.strip().split()
            if (len(line) > 0) and (line[-1] == setID):
                job_num = int(line[0])
                break
    
    shearData = np.empty((job_num, 129))
    shearData[:] = np.nan
    
    set_path = os.path.join(main_path, setID)
    job_list = [f.name for f in os.scandir(set_path) if f.is_dir()]
    for job in job_list:
        jobID = int(job[4::])
        
        shearData[jobID][0] = jobID
        
        job_path = os.path.join(set_path, job)
        jobName = os.path.join(job_path, 'wout_HSX_main_opt0.nc')
        if os.path.isfile(jobName):
            shear = extract_shear(job_path)
            shearData[jobID][1:] = shear
        
    dataName = os.path.join(main_path, name_out)
    
    hf_file = hf.File(dataName, 'a')
    hf_file.create_dataset(setID+'/avg_shear', data=shearData)
    hf_file.close()
                
def extract_from_set(path_in, path_out, job_list, job_num, setID, tag='main', name_out='conData.h5'):
    """ Extracts epsilon effective data from neo_out files within the 
    specified set, then saves data to specified hdf5 file.

    Parameters
    ----------
    path_in : str
        directory path of specified set
    path_out : str
        directory path to hdf5 output file
    job_list : list
        list of job IDs to be extracted within set
    job_num : int
        number of jobs attempted within set, doesn't need to equal number of 
        jobs in job_list
    setID : str
        set identifier for specified set
    tag : str, optional
        name convention for neo_out files (default is 'main')
    name_out : str, optional
        name of output hdf5 file (default is 'conData.h5')
    """
    neoData = np.empty((job_num, 128))
    neoData[:] = np.nan
    
    for job in job_list:
        jobID = int(job[4::])
        
        neoData[jobID][0] = jobID
        
        jobName = os.path.join(path_in, job, 'neo_out.HSX_{}.00000'.format(tag))
        if os.path.isfile(jobName):
            neoFile = open(jobName, 'r')
            lines = neoFile.readlines()
            neoFile.close()
            
            neo = np.empty(127)
            for n, line in enumerate(lines):
                line = line.strip()
                line = line.split()
                
                neo[n] = float(line[1])
            neoData[jobID][1::] = neo
        
    dataName = os.path.join(path_out, name_out)
    
    hf_file = hf.File(dataName, 'a')
    hf_file.create_dataset(setID+'/eps_effective', data=neoData)
    hf_file.close()


def extract_from_list(path_in, name_in, path_out, name_out, setID, basePath='D:\\HSX_Configs' , tag='main'):
    """ Extracts epsilon effective data from neo_out files specified within 
    the input job list file.  Extracted data is then saved, along with 
    current configurations data, in the specified hdf5 file.

    Parameters
    ----------
    path_in : str
        directory path of input job list file
    name_in : str
        input job list file name
    path_out : str
        directory path of output hdf5 file
    name_out : str
        output hdf5 file name
    setID : str
        set identifier for job list in hdf5 file
    basePath : str, optional
        directory path of sourced database (default is 'D:\\HSX_Configs')
    tag : str, optional
        name convention for neo_out files (default is 'main')

    Raises
    ------
    IOError
        neo_out file for job queued for extraction does not exist
    """
    job_list = []
    
    pathName = os.path.join(path_in, name_in)
    with open(pathName, 'r') as file:
        num_jobs = 0
        for line in file:
            line = line.strip()
            line = line.split(' : ')
            
            jobID = int(line[0])
            jobPath = line[1]
            jobCrnt = np.array([float(i) for i in line[2][1:-1].split(', ')])
            job_list.append([jobID, jobPath, jobCrnt])
            
            num_jobs+=1
            
    neoData = np.empty((num_jobs, 128))
    crntData = np.empty((num_jobs, 13))
    
    for job in job_list:
        jobID = job[0]
        jobPath = job[1]
        jobCrnt = job[2]
        
        neoData[jobID][0] = jobID
        crntData[jobID][0] = jobID
        crntData[jobID][1::] = jobCrnt
        
        jobName = os.path.join(basePath, jobPath, 'neo_out.HSX_{}.00000'.format(tag))
        try:
            neoFile = open(jobName, 'r')
            lines = neoFile.readlines()
            neoFile.close()
        except:
            raise IOError('{} does not exist.'.format(jobName))
        
        neo = np.empty(127)
        for n, line in enumerate(lines):
            line = line.strip()
            line = line.split()
            
            neo[n] = float(line[1])
        neoData[jobID][1::] = neo
        
    dataName = os.path.join(path_out, name_out)
    if os.path.exists(dataName):
        hf_file = hf.File(dataName, 'a')
    else:
        hf_file = hf.File(dataName, 'w')
        
    hf_file.create_dataset(setID+'/eps_effective', data=neoData)
    hf_file.create_dataset(setID+'/config', data=crntData)
    hf_file.close()    
    
    
if __name__ == '__main__':
    # data_keys = ['Bk_overlap', 'Gss_metric', 'modB_average','eps_effective', 'qhs_metric']
    # for data_key in data_keys:
    #     print('Working with {}\n'.format(data_key))
        
    #     path = dd.exp_ext_base
    #     data = pr.profile_data(path, 'superSet/all', data_key, name='profile_data.h5')
        
    #     nan_idx = np.where(np.isnan(data.data_profile[0::,data.begIdx]))[0][0]
    #     idx_list = np.linspace(0, nan_idx-1, int(0.01*nan_idx), dtype=np.int)
        
    #     conID = np.empty((idx_list.shape[0], 3))
    #     for i, idx in enumerate(idx_list):
    #         conID[i] = data.data_profile[idx,0:3]
        
    #     superFile = os.path.join(path, 'profile_data.h5')
    #     extract_superSet(superFile, data_key, 'sparse_all_100%', conID)
    
    if False:
        basePath = os.path.join('D:\\','HSX_Configs')
        # extract_overlap_set(basePath, range(1319,1561), [1]) 
        extract_GssKn_set(basePath, range(1135,1561), [1])           
    
    if True:
        metric_full = np.empty((int(1561*729), 131))
        metric_full[:] = np.nan
                
        for i in range(1561):
            mainID = 'main_coil_{0:0.0f}'.format(i)
            print(mainID)
                        
            con_path = os.path.join('D:\\','HSX_Configs',mainID,'conData.h5')
            with hf.File(con_path, 'r') as hf_file:
                metric = hf_file['set_1/G_ss'][:]
                
                met_leng = metric.shape[0]
                set_arr = np.full(met_leng, 1)
                main_arr = np.full(met_leng, i)
                
                set_idx = np.where(~np.isnan(metric_full[:,0]))[0]
                if any(set_idx):
                    idx_beg = np.max(set_idx) + 1
                else:
                    idx_beg = 0
                idx_end = idx_beg + met_leng
                
                metric = np.insert(metric, 0, set_arr, axis=1)
                metric_full[idx_beg:idx_end] = np.insert(metric, 0, main_arr, axis=1)
                
        prof_path = os.path.join('D:\\','HSX_Configs','profile_data.h5')
        with hf.File(prof_path, 'a') as hf_file:
            hf_file.create_dataset('superSet/all/G_ss', data=metric_full)
                
    if False:
        import plot_define as pd
        
        mainID = 'main_coil_0'
        jobID = 'job_0'
        
        jobNum = int(jobID.split('_')[1])
        
        main_path = os.path.join('D:\\','HSX_Configs',mainID,'set_1',jobID)
        # Fkappa_new = extract_Bk_overlap(main_path)
        Fkappa_new = extract_GssKn_metric(main_path)
        
        hf_path = os.path.join('D:\\','HSX_Configs',mainID,'conData.h5')
        with hf.File(hf_path, 'r') as hf_file:
            Fkappa_data = hf_file['set_1/Gss_metric'][:]
        
        idx = np.argmin( np.abs( Fkappa_data[:,0] - jobNum ) )
        Fkappa_old = Fkappa_data[idx, 1:]
        
        plot = pd.plot_define(lineWidth=2)
        plt = plot.plt
        
        beg_idx = 0
        s_dom = np.linspace(0,1,128)
        plt.plot(np.sqrt(s_dom[beg_idx:]), Fkappa_new[beg_idx:], c='k', label=r'$\mathcal{G}_{ss}^{new}$')
        # plt.plot(np.sqrt(s_dom[beg_idx:]), Fkappa_old[beg_idx:], c='k', ls='--', label=r'$\mathcal{G}_{ss}^{old}$')
        
        # plt.xlabel(r'$\psi / \psi_{edge}$')
        plt.xlabel(r'$r/a$')
        
        plt.ylim(0, 0.0003)
        plt.xlim(0.25, 0.75)
        
        plt.grid()
        plt.legend()
        plt.grid()
        plt.show()