import numpy as np
import h5py as hf
import os, sys

import profile_extraction as pe

WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.booz_read as br


if __name__ == '__main__':
    PATH = os.path.join('/home','michael','Desktop','set_2')
    job_list = [job.name for job in os.scandir(PATH) if os.path.isdir(job)]

    norm_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations.h5')
    met_norms = {}
    with hf.File(norm_path, 'r') as hf_:
        for key in hf_:
            met_norms[key] = hf_[key][()]

    # Read QHS Values #
    qhs_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1','job_0')
    wout = wr.readWout(qhs_path)

    s_beg_low = 1
    s_end_low = np.argmin(np.abs(wout.s_grid - 0.25)) + 1

    s_beg_mid = np.argmin(np.abs(wout.s_grid - 0.0625))
    s_end_mid = np.argmin(np.abs(wout.s_grid - 0.5625)) + 1

    s_dom_low = wout.s_grid[s_beg_low:s_end_low]
    s_dom_mid = wout.s_grid[s_beg_mid:s_end_mid]

    s_norm_low = 1. / np.trapz(np.ones(s_dom_low.shape), s_dom_low)
    s_norm_mid = 1. / np.trapz(np.ones(s_dom_mid.shape), s_dom_mid)

    # Get Epsilon Effective Data #
    neo_data = pe.extract_epsEff(qhs_path)
    eps_qhs = np.trapz(neo_data[0:s_end_low-1], s_dom_low) * s_norm_low

    # Get Boozer Data #
    booz_path = os.path.join(qhs_path, 'boozmn_wout_HSX_main_opt0.nc')
    booz = br.readBooz(booz_path)
    qhs_metric = booz.qhs_metric()
    qhs_qhs = np.trapz(qhs_metric[0:s_end_low-1], s_dom_low) * s_norm_low

    # Check Set_3 Jobs #
    met_data = np.empty((len(job_list), 12))
    for jdx, job in enumerate(job_list):
        print('{0}-{1}'.format(jdx+1, len(job_list)))
        jobID = int(job.split('_')[1])
        job_path = os.path.join(PATH, job)

        # Get Epsilon Effective Data #
        neo_data = pe.extract_epsEff(job_path, name='neo_out.HSX_aux.00000')
        eps_avg = np.trapz(neo_data[0:s_end_low-1], s_dom_low) * s_norm_low / eps_qhs

        # Get Boozer Data #
        booz_path = os.path.join(job_path, 'boozmn_wout_HSX_aux_opt0.nc')
        booz = br.readBooz(booz_path)
        qhs_metric = booz.qhs_metric()
        qhs_avg = np.trapz(qhs_metric[0:s_end_low-1], s_dom_low) * s_norm_low / qhs_qhs

        # Get Shear Data #
        wout = wr.readWout(job_path, name='wout_HSX_aux_opt0.nc')
        shear_profile = np.gradient(wout.iota(wout.s_grid), wout.s_grid)
        shear = np.trapz(shear_profile[s_beg_mid:s_end_mid], s_dom_mid) * s_norm_mid / met_norms['shear']

        # Read Metrics #
        met_path = os.path.join(job_path, 'output.txt')
        with open(met_path, 'r') as f:
            lines = f.readlines()
            line = lines[1].strip().split()

            B_avg = float(line[0]) / met_norms['B average']
            F_kappa = float(line[1]) / met_norms['F kappa']
            G_psi = float(line[2]) / met_norms['G ss']
            kappa = float(line[3]) / met_norms['kappa']
            delta = float(line[4]) / met_norms['delta']
            rho = float(line[5]) / met_norms['rho']

        met_data[jdx] = np.array([0, 2, jobID, eps_avg, qhs_avg, B_avg, F_kappa, G_psi, shear, kappa, delta, rho])

    save_path = os.path.join('/home','michael','Desktop','metric_data_main0_set2.h5')
    with hf.File(save_path, 'w') as hf_:
        hf_.create_dataset('metric data', data=met_data)
