# -*- coding: utf-8 -*-
"""
Created on Fri Nov  6 16:51:12 2020

@author: micha
"""

import h5py as hf
import numpy as np

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import directory_dict as dd
import plot_define as pd

def check_metric(metric):
    if metric == 'eps':
        metric_idx = 3
        metric_lab = r'$\epsilon_{eff} / \epsilon_{eff}^*$'
    elif metric == 'qhs':
        metric_idx = 4
        metric_lab = r'$\mathcal{Q} / \mathcal{Q}^*$'
    elif metric == 'modB':
        metric_idx = 5
        metric_lab = r'$\langle B\rangle / \langle B\rangle^*$'
    elif metric == 'Fk':
        metric_idx = 6
        metric_lab = r'$\mathcal{F}_{\kappa} / \mathcal{F}_{\kappa}^*$'
    elif metric == 'Gss':
        metric_idx = 7
        metric_lab = r'$\mathcal{G}_{ss} / \mathcal{G}_{ss}^*$'
    elif metric == 'shear':
        metric_idx = 8
        metric_lab = r'$\langle S \rangle / \langle S \rangle^*$'

    return metric_idx, metric_lab

class metric_comparison():

    def __init__(self, path, fileName, met_1, met_2, met_3=None):
        self.pathName = os.path.join(path, fileName)

        met_1_idx, met_1_lab = check_metric(met_1)
        met_2_idx, met_2_lab = check_metric(met_2)
        if met_3:
            self.met_3_chk = True
            met_3_idx, met_3_lab = check_metric(met_3)
        else:
            self.met_3_chk = False

        with hf.File(self.pathName, 'r') as hf_file:
            conIDs = hf_file['metric data'][:,0:3]

            is_nan = np.isnan(conIDs[:,0])
            not_nan = ~is_nan
            self.conIDs = conIDs[not_nan,:]

            met_1 = hf_file['metric data'][:,met_1_idx]
            met_2 = hf_file['metric data'][:,met_2_idx]

            self.met_1 = met_1[not_nan]
            self.met_2 = met_2[not_nan]

            if met_3:
                self.met_3 = hf_file['metric data'][:,met_3_idx]
                self.met_3 = self.met_3[not_nan]

        if met_3:
            self.met_names = [ met_1_lab, met_2_lab, met_3_lab ]
        else:
            self.met_names = [ met_1_lab, met_2_lab ]

    def get_subset(self, conID_set):
        """ Return all metric data for the specified configuration IDs.

        Parameters
        ----------
        conID_set : arr
            Configuration IDs, stored as [mainID, setID, jobID].

        Returns
        -------
        data : arr
            Metric data for specified configurations.  Returned as [ mainID,
            setID, jobID, eps_eff, qhs_met, avg modB, F-kappa, G-ss ]
        """
        found, failed, idx_set = self.find_configs(conID_set, return_idx=True)

        with hf.File(self.pathName, 'r') as hf_file:
            data = hf_file['metric data'][:]

            is_nan = np.isnan(data[:,0])
            not_nan = ~is_nan

            data = data[not_nan,:]
            data = data[idx_set,:]

        return data

    def find_configs(self, conID_set, return_idx=False):
        """ Find the metric data for the specified configuration IDs.

        Parameters
        ----------
        conID_set : arr
            Configuration IDs, stored as [mainID, setID, jobID].
        return_idx : bool, optional
            Mark as True if you want the index of the configurations to be
            returned.  Default is False.

        Returns
        -------
        found_set : arr
            Metric data that was successfully found, returned as
            [mainID, setID, jobID, metric_1, metric_2].
        failed_set : arr
            Metric data that was unsuccessfully found, returned as
            [mainID, setID, jobID, metric_1, metric_2].
        """
        idx_set = np.full(conID_set.shape[0], np.nan)
        for c, conID in enumerate(conID_set):
            print('Working : {0} of {1}'.format(c+1, conID_set.shape[0]))
            sum_abs = np.sum( np.abs( self.conIDs - conID ), axis=1)
            idx_chk = np.nanargmin(sum_abs)
            if np.sum(np.abs(conID - self.conIDs[idx_chk])) == 0:
                idx_set[c] = idx_chk

        nan_set = np.isnan(idx_set)
        not_nan = ~nan_set

        int_not = idx_set[not_nan].astype(int)

        idx_chk = int_not[0]

        found_IDs = self.conIDs[int_not]
        found_met_1 = self.met_1[int_not]
        found_met_2 = self.met_2[int_not]

        found_mets = np.stack((found_met_1, found_met_2), axis=1)
        found_set = np.append(found_IDs, found_mets, axis=1)

        failed_set = conID_set[nan_set]

        if return_idx:
            return found_set, failed_set, int_not
        else:
            return found_set, failed_set


    def limiting_conditon(self, limit, MaxOrMin='max'):
        """ Print the specified limiting condition for the comparison set.
        This works by first finding the values of metric_1 below the limiting
        value and then identifying the extremized value of metric_2 within the
        subset of data that met the constraint for metric_1.

        Parameters
        ----------
        limit : float
            This sets the limiting conditon for metric_1.
        MaxOrMin : str, optional
            Specify whether the extemized value for metric_2 should be a
            minimum or maximum. The default is 'max'.
        """
        met_1_idx = np.where(self.met_1 < limit)[0]

        conID_set = self.conIDs[met_1_idx]
        met_1_set = self.met_1[met_1_idx]
        met_2_set = self.met_2[met_1_idx]

        if MaxOrMin == 'max':
            met_2_idx = np.argmax(met_2_set)
            met_1_ext = met_1_set[met_2_idx]
            met_2_ext = np.max(met_2_set)
        elif MaxOrMin == 'min':
            met_2_idx = np.argmin(met_2_set)
            met_1_ext = met_1_set[met_2_idx]
            met_2_ext = np.min(met_2_set)

        print('ConID = {0}\n   ({1}, {2}) = ({3:0.4f}, {4:0.4f})'.format(conID_set[met_2_idx], self.met_name_1, self.met_name_2, met_1_ext, met_2_ext))

    def limiting_range(self, rng_1, rng_2):
        """ Find configurations within the metric limiting ranges specified.

        Parameters
        ----------
        rng_1 : arr
            Range of metric_1 values, structured as [low, high].
        rng_2 : arr
            Range of metric_2 values, structured as [low, high].

        Returns
        -------
        limit_set : arr
            Found configurations with specified range. Structured as
            [mainID, setID, jobID, met_1, met_2].
        """
        met_rngX_1 = self.met_1[(self.met_1 >= rng_1[0]) & (self.met_1 <= rng_1[1])]
        met_rngX_2 = self.met_2[(self.met_1 >= rng_1[0]) & (self.met_1 <= rng_1[1])]
        conID_rngX = self.conIDs[(self.met_1 >= rng_1[0]) & (self.met_1 <= rng_1[1])]

        met_rng_1 = met_rngX_1[(met_rngX_2 >= rng_2[0]) & (met_rngX_2 <= rng_2[1])]
        met_rng_2 = met_rngX_2[(met_rngX_2 >= rng_2[0]) & (met_rngX_2 <= rng_2[1])]
        conID_rng = conID_rngX[(met_rngX_2 >= rng_2[0]) & (met_rngX_2 <= rng_2[1])]

        limit_set = np.stack((met_rng_1, met_rng_2), axis=1)
        limit_set = np.append(conID_rng, limit_set, axis=1)

        return limit_set

    def select_from_range(self, rng_1, rng_2, npts):
        """ Extracts some number of configurations from the subset of
        configrations found within the specified ranges.

        Parameters
        ----------
        rng_1 : arr
            Range of metric_1 values, structured as [low, high].
        rng_2 : arr
            Range of metric_2 values, structured as [low, high].
        npts : int
            Number of configurations sampled from subset.

        Returns
        -------
        Arr
            An array of the extracted configurations, stored as
            [mainID, setID, jobID, met_1, met_2].
        """
        limit_set = self.limiting_range(rng_1, rng_2)

        eps_rng = np.linspace(np.min(limit_set[0:,3]), np.max(limit_set[0:,3]), npts)
        idx_set = np.empty(npts, dtype=int)
        for n in range(npts):
            idx_set[n] = np.argmin( np.abs( limit_set[0:,3] - eps_rng[n] ) )
        return limit_set[idx_set]


    def plot_data(self, plot, plotLog=False, plot3D=False):
        """ Generate a plot of the metric comparison data.

        Parameters
        ----------
        plot : obj
            Matplotlib plotting object.
        """
        if plotLog:
            from matplotlib.colors import LogNorm

        if self.met_3_chk and not plot3D:
            met_set = np.stack((self.met_1, self.met_2, self.met_3), axis=1)
            met_set = np.array( sorted( met_set, key=lambda x: x[2], reverse=False ) )

            if plotLog:
                s_map = plot.plt.scatter(met_set[:,0], met_set[:,1], c=met_set[:,2], s=1, norm=LogNorm( vmin=np.nanmin( met_set[:,2] ), vmax=np.nanmax( met_set[:,2] ) ), cmap='jet')
            else:
                s_map = plot.plt.scatter(met_set[:,0], met_set[:,1], c=met_set[:,2], s=1, vmin=np.nanmin(met_set[:,2]), vmax=np.nanmax(met_set[:,2]), cmap='jet')

            cbar = plot.fig.colorbar(s_map, ax=plot.ax)
            cbar.ax.set_ylabel(self.met_names[2])

        elif self.met_3_chk and plot3D:
            if plotLog:
                s_map = plot.ax.scatter(self.met_1, self.met_2, self.met_3, s=1, c=self.met_3, norm=LogNorm( vmin=np.nanmin( self.met_3 ), vmax=np.nanmax( self.met_3 ) ), cmap='jet')
            else:
                s_map = plot.ax.scatter(self.met_1, self.met_2, self.met_3, s=1, c=self.met_3, vmin=np.nanmin(self.met_3), vmax=np.nanmax(self.met_3), cmap='jet')

            plot.ax.set_zlim(np.min(self.met_3), np.max(self.met_3))

            cbar = plot.fig.colorbar(s_map, ax=plot.ax)
            cbar.ax.set_ylabel(self.met_names[2])

        else:
            plot.plt.scatter(self.met_1, self.met_2, c='tab:red', s=1)

        plot.plt.xlabel(self.met_names[0])
        plot.plt.ylabel(self.met_names[1])

        plot.plt.tight_layout()


if __name__ == '__main__':
    fileName = 'metric_data.h5'
    filePath = os.path.join('/mnt','HSX_Database','HSX_Configs',fileName)

    if False:
        ### Print Metric Extrema ###
        with hf.File(filePath, 'r') as hf_file:
            met_data = hf_file['metric data'][()]

        path = os.path.join('/mnt','HSX_Database','HSX_Configs','metric_normalizations.h5')
        with hf.File(path, 'r') as hf_:
            met_norms = {}
            for key in hf_:
                met_norms[key] = hf_[key][()]

        print(np.nanmin(met_data[:,10]) * met_norms['delta'])
        print(np.nanmax(met_data[:,10]) * met_norms['delta'])

    if False:
        met = metric_comparison(path, fileName, 'shear', 'qhs', met_3='Fk')

        plot = pd.plot_define(proj3D=True)
        met.plot_data(plot, plotLog=False, plot3D=True)

        met_3 = met.met_3

        # plot.plt.xlim(0.85, 1.35)

        plot.plt.grid()
        plot.plt.show()

    #    plot.plt.savefig(os.path.join(path,'figures','metric_figures','eps_qhs_Fk.png'))

    if False:
        met = metric_comparison(path, fileName, 'eps', 'qhs')
        limit_set = met.limiting_range([0,10],[0,np.inf])
        met_data = met.get_subset(limit_set[:,0:3])

        path_new = os.path.join(path,'Epsilon_Valley','metric_data.h5')
        with hf.File(path_new, 'w') as hf_file:
            hf_file.create_dataset('metric data', data=met_data)
