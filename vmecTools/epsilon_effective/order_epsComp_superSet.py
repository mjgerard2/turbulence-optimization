# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 00:01:55 2020

@author: micha
"""

import read_epsSet 

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import directory_dict as dd

### Import SuperSet File ###
path = dd.exp_local['HSX']
epsSet = read_epsSet.epsData(path, 'superSet/all', name='epsEff_profiles.h5')

### Order by Minimum Mean Profile ###
idxMin, idxMax = epsSet.find_extrema()
epsSet.order_compare(epsSet.eps_profile[idxMin])
epsSet.save_order()