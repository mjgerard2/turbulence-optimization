#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  9 09:26:27 2019

@author: michael
"""

import extraction_tools as exTool

import h5py as hf
import os

### Working Directory ###
basePath = 'D:\\'
setID = 'set_1'
path = os.path.join(basePath, 'HSX_Configs')

for i in range(1561):
    mainID = 'main_coil_{}'.format(i)
    
    print('Working on '+mainID)
    
    mainPath = os.path.join(basePath,'HSX_Configs',mainID)
    setPath = os.path.join(mainPath, setID)
    
    hf_file = hf.File(os.path.join(mainPath,'conData.h5'), 'r')
    eps_chk = 'eps_effective' in hf_file[setID]
    hf_file.close()
    
    if not eps_chk:
        print('   Extracting from '+setPath)
        job_list = [f.name for f in os.scandir(setPath) if f.is_dir()]
        job_num = exTool.set_num_jobs(mainPath, setID)
        
        exTool.extract_from_set(setPath, mainPath, job_list, job_num, setID)