#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 27 13:53:55 2019

@author: michael
"""


from matplotlib import animation as an  
from matplotlib.colors import LogNorm
import matplotlib.pyplot as plt

import numpy as np
import h5py as hf

import os


class epsData():
    """ A class to perform a comparative analysis on large sets of epsilon 
    effective profiles.
    
    ...
    
    Attributes
    ----------
    path : str
        path to the directory in which the epsilon data is stored
    setID : str
        set ID of epsilon effective data
    name : str, optional
        name of hdf5 file storing epsilon effective data 
        (default is 'conData.h5')
        
    Methods
    -------
    find_crnt_profile(crnt_pro)
        Returns index of the specified current configuration.
        
    find_config(jobID)
        Returns index of configuration for the specified job ID.
        
    find_extrema(rBnd=0.5)
        Returns indices of the minimum and maximum averaged epsEff values
        within the specified radial domain.
        
    find_compare(eps_init, rBnd=0.5, ordered=False)
        Returns the index of the epsEff profile whose percent comaprison with 
        the input epsEff profile is smallest over the specified radial domain.
        
    order_compare(eps_init, rBnd=.5)
        Orders all epsEff profiles by their percent comparison to the input
        epsEff profile, calculated over the specified radial domain.
        
    order_permute(self, sort_order)
        Orders all epsEff profiles by the change in their current profile,
        permuted as specified by input.
        
    plot_profile_map(fig, ax, ordered=False, nJobs=False)
        Plot color map of epsEff profiles along their 
        index order.
        
    plot_difference_map(fig, ax, eps_init, ordered=False, nJobs=False):
        Plots color map of epsEff profile percent difference with 
        specified epsEff profile.
        
    plot_difference(ax, eps_init, rBnd=.5, diff_thresh=1, comp_name='QHS')
        Plot the percent difference of all configurations against the input 
        epsEff profile across the specified radial domain.
        
    plot_difference_histogram(ax, eps_init, bins, rBnd=.5, diff_thresh=100):
        Plot the histogram of epsilon effective profiles with a percent 
        difference below the specified threshold when compared to the input
        epsEff profile across the specified radial domain.
        
    save_order()
        Save the present configuration order.
        
    Raises
    ------
    IOError
        hdf5 file specified does not exist
    """
    
    
    def __init__(self, path, setID, name='conData.h5'):
        
        self.path = path
        self.name = name
        self.setID = setID
        
        if setID.split('/')[0] == 'superSet':
            self.begIdx = 3
            self.jobIdx = 2
            self.setIdx = 1
            self.mainIdx = 0
        else:
            self.begIdx = 1
            self.jobIdx = 0
        
        pathName = os.path.join(path, name)
        try:
            f = hf.File(pathName, 'r')
        except IOError:
            print('File does not exists: '+pathName)
            raise 
        
        try:
            self.crnt_profile = f[setID+'/config'][:]
            self.eps_profile = f[setID+'/eps_effective'][:]
        except KeyError:
            raise IOError('Epsilon Effective data not available for '+pathName)
        
        self.job_num = self.crnt_profile.shape[0]
        self.ns = self.eps_profile.shape[1] - self.begIdx
        self.s_dom = np.linspace(0, 1, self.ns)
        
        f.close()
        
    
    def find_crnt_profile(self, crnt_pro):
        """ Returns index of the specified current configuration.
        
        Parameters
        ----------
        crnt_pro : array
            Current profile whose index we want to identify
            
        Returns
        -------
        int
            job index of the specified current profile 
            
        Raises
        ------
        ValueError
            specified current profile does not exist
        """
        idx = np.argmin( np.sum(np.abs(self.crnt_profile[0::,self.begIdx::] - crnt_pro), axis=1) )
        
        diff = np.sum( np.abs( self.crnt_profile[idx,self.begIdx::] - crnt_pro ) )
        if diff == 0:
            return idx
        else:
            crnt_str = '['+', '.join(['{}'.format(c) for c in crnt_pro])+']'
            raise ValueError('Current profile '+crnt_str+' does not exist.')


    def find_config(self, jobID):
        """ Returns index of configuration for the specified job ID.
        
        Parameters
        ----------
        jobID : int
            job index to identify
            
        Returns
        -------
        int
            index of configuration found for specified job ID
            
        Raises
        ------
        ValueError
            specified job ID does not exist
        """
        idx = np.argmin(np.abs(self.crnt_profile[0::,self.jobIdx] - jobID))
        
        diff = self.crnt_profile[idx,self.jobIdx] - jobID
        if diff == 0:
            return idx
        else:
            raise ValueError('job_{} does not exist.'.format(jobID))
            
            
    def find_extrema(self, rBnd=0.5):
        """ Returns indices of the minimum and maximum averaged epsilon 
        effective values within the specified radial domain.
        
        Parameters
        ----------
        rBnd : float
            effective radius boundary of averaged domain (default is 0.5)
            
        Returns
        -------
        tuple
            tuple of indices for the minimum and maximum averaged epsEff
            values, respectively 
        """
        ns_hlf = self.begIdx + int(rBnd*self.ns)
        
        ind_min = np.nanargmin( np.mean(self.eps_profile[0::, self.begIdx:ns_hlf], axis=1) )
        ind_max = np.nanargmax( np.mean(self.eps_profile[0::, self.begIdx:ns_hlf], axis=1) )
                        
        return [ind_min, ind_max]
    
    
    def find_compare(self, eps_init, rBnd=0.5, ordered=False):
        """ Returns the index of the epsEff profile whose percent comaprison with 
        the input epsEff profile is smallest over the specified radial domain.
        
        Parameters
        ----------
        eps_init : array
            input epsEff profile against which all other profiles are compared
        rBnd : float
            effective radial boundary over which comparison is performed 
            (default is 0.5)
        
        Returns
        -------
        integer
            index of the epsEff profile with smallest percent comparison
        """
        ns_hlf = self.begIdx + int(rBnd*self.ns)
        
        size_diff = self.eps_profile.shape[1] - eps_init.shape[0]
        eps_init = np.insert(eps_init, 0, np.zeros(size_diff))[self.begIdx:ns_hlf]
        eps_init_inv = 1. / eps_init
        
        if ordered:
            eps_search = self.eps_ordered
        else:
            eps_search = self.eps_profile            
        
        compare = np.empty(self.job_num)
        for ind, eps in enumerate(eps_search[0::,self.begIdx:ns_hlf]):
            compare[ind] = np.mean(np.abs(eps - eps_init) * eps_init_inv)
            
        return np.nanargmin(compare)
    
    
    def order_compare(self, eps_init, rBnd=.5):
        """ Orders all epsEff profiles by their percent comparison to the input
        epsEff profile, calculated over the specified radial domain.
        
        Parameters
        ----------
        eps_init : array
            epsilon profile to compare other profiles to
             
        rBnd : float
            effective radius at which comparison ends (default is 0.5)
        """        
        ns_hlf = self.begIdx + int(rBnd*self.ns)
        
        size_diff = self.eps_profile.shape[1] - eps_init.shape[0]
        eps_init = np.insert(eps_init, 0, np.zeros(size_diff))[self.begIdx:ns_hlf]
        eps_init_inv = 1. / eps_init
        
        self.eps_ordered = np.empty(self.eps_profile.shape)
        self.crnt_ordered = np.empty(self.crnt_profile.shape)
        
        compare = np.empty((self.job_num, 2))        
        for e, eps in enumerate(self.eps_profile[0::,self.begIdx:ns_hlf]):
            eps_chk = np.mean( np.abs(eps - eps_init) * eps_init_inv )
            compare[e] = np.array([eps_chk, e])

        compare = np.array( sorted(compare, key=lambda x: np.inf if np.isnan(x[0]) else x[0]) )

        for c, com in enumerate(compare):
            ind = int(com[1])
            
            self.eps_ordered[c] = self.eps_profile[ind]
            self.crnt_ordered[c] = self.crnt_profile[ind]
            
            
    def order_permute(self, sort_order):
        """ Orders all epsEff profiles by the change in their current profile,
        permuted as specified by input.  

        Parameters
        ----------
        sort_order : list
            list of coil permutation order, for example, input of [1,2,3,4,5,6]
            would order permutations in coil 1 first and coil 6 last.
        """
        ind = sort_order[0]
    
        self.crnt_ordered = np.array( sorted(self.crnt_profile, key=lambda x: x[ind]) )
        chng_bounds = np.array([0, self.job_num])
        for i, ind in enumerate(sort_order[0:-1]):
            ind1 = sort_order[i+1]
            crnt_chng = np.insert(chng_bounds, 1, 1 + np.where(self.crnt_ordered[:-1,ind] != self.crnt_ordered[1:,ind])[0])
            for b, idxBeg in enumerate(crnt_chng[0:-1]):
                idxEnd = crnt_chng[b+1]
                self.crnt_ordered[idxBeg:idxEnd] = np.array( sorted(self.crnt_ordered[idxBeg:idxEnd], key=lambda x: x[ind1]) )
                
        self.eps_ordered = np.empty(self.eps_profile.shape)
        for c, crnt in enumerate(self.crnt_ordered):
            jobID = crnt[0]
            idx = self.find_config(jobID)
            self.eps_ordered[c] = self.eps_profile[idx]

            
    def plot_profile_map(self, fig, ax, rBnd=0.5, jscl=1e-3, ordered=False, nJobs=False):
        """ Plot color map of epsEff profiles along their index order.
        
        Parameters
        ----------
        fig : object
            figure object on which to construct color scale
        ax : object
            axis object on which to construct color plot
        rBnd : float, optional
            effective radius in which max and min values are calculted
            (default is 0.5)
        ordered : bool, optional
            plot epsEff profiles by eps_ordered instead of eps_profile 
            (default is False)
        nJobs : bool (int), optional
            number of jobs to plot. Default is False
        """
        if ordered:
            if nJobs:
                ax.set_ylim(0, nJobs*jscl)
                
                j_dom = np.arange(nJobs) * jscl
                eps_pro = self.eps_ordered[0:nJobs,self.begIdx::]
            else:
                ax.set_ylim(0, self.job_num*jscl)
                
                j_dom = np.arange(self.job_num) * jscl
                eps_pro = self.eps_ordered[0::,self.begIdx::]
        else:
            if nJobs:
                ax.set_ylim(0, nJobs*jscl)
                
                j_dom = np.arange(nJobs) * jscl
                eps_pro = self.eps_profile[0:nJobs,self.begIdx::]
            else:
                ax.set_ylim(0, self.job_num*jscl)
                
                j_dom = np.arange(self.job_num) * jscl
                eps_pro = self.eps_profile[0::,self.begIdx::]
            
        ns_hlf = self.begIdx + int(rBnd*self.ns)
        
        eps_min = np.nanmin(eps_pro[0::, self.begIdx:ns_hlf])
        if eps_min < 1e-5:
            eps_min = 1e-5
        eps_max = np.nanmax(eps_pro[0::, self.begIdx:ns_hlf])
                            
        s_map = ax.pcolormesh(self.s_dom, j_dom, eps_pro, norm=LogNorm(vmin=eps_min, vmax=eps_max), cmap='jet')
        
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'$\epsilon_{eff}^{3/2}$')
        
    
    def plot_difference_map(self, fig, ax, eps_init, jscl=1e-3, ordered=False, nJobs=False):
        """ Plots color map of epsEff profile percent difference with 
        specified epsEff profile.

        Parameters
        ----------
        fig : object
            figure object on which to construct color scale
        ax : object
            axis object on which to construct color plot
        eps_init : array
            epsEff profile to compare other profiles to
        ordered : bool, optional
            plot epsEff profiles by eps_ordered instead of eps_profile 
            The default is False.
        nJobs : bool (int), optional
            number of jobs to plot. default is False
        """
        if ordered:
            if nJobs:
                ax.set_ylim(0, nJobs*jscl)
                
                j_dom = np.arange(nJobs) * jscl
                eps_pro = self.eps_ordered[0:nJobs,self.begIdx::]
            else:
                ax.set_ylim(0, self.job_num*jscl)
                
                j_dom = np.arange(self.job_num) * jscl
                eps_pro = self.eps_ordered[0::,self.begIdx::]
        else:
            if nJobs:
                ax.set_ylim(0, nJobs*jscl)
                
                j_dom = np.arange(nJobs) * jscl
                eps_pro = self.eps_profile[0:nJobs,self.begIdx::]
            else:
                ax.set_ylim(0, self.job_num*jscl)
                
                j_dom = np.arange(self.job_num) * jscl
                eps_pro = self.eps_profile[0::,self.begIdx::]
        
        size_diff = self.eps_profile.shape[1] - eps_init.shape[0]
        eps_init = np.insert(eps_init, 0, np.zeros(size_diff))[self.begIdx::]
        eps_init_inv = 1. / eps_init
        
        diff_map = np.empty(eps_pro.shape)
        for e, eps in enumerate(eps_pro):
            diff_map[e][:] = np.abs(eps - eps_init) * eps_init_inv * 100
            
        diff_min = np.nanmin(diff_map[np.nonzero(diff_map)])
        diff_max = np.nanmax(diff_map)
            
        idx = np.nanargmin( np.sum(diff_map, axis=1) )
        #ax.plot([0, 1], [idx*jscl, idx*jscl], ls='--', c='k', linewidth=1)    
        if np.sum(diff_map[idx]) == 0:
            diff_map[idx][:] = 1e-10
        
        #s_map = ax.pcolormesh(self.s_dom, j_dom, diff_map, norm=LogNorm(vmin=diff_min, vmax=diff_max), cmap='jet')
        s_map = ax.pcolormesh(self.s_dom, j_dom, diff_map, vmin=diff_min, vmax=diff_max, cmap='jet')
        
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'% Difference')


    def plot_difference(self, ax, eps_init, rBnd=.5, diff_thresh=100, jscl=1e-3, comp_name='QHS'):
        """ Plot the percent difference of all configurations against the input
        epsEff profile across the specified radial domain.
        
        Parameters
        ----------
        ax : object
            axis object on which to construct plot
        eps_init : array
            input epsilon profile
        rBnd : float
            effective radial boundary over which comparison is performed 
            (default is 0.5)
        diff_thresh : float 
            only configurations with a percent difference below 
            this theshold will be plotted (default is 1)
        comp_name : str 
            plot label for input epsEff profile (default is 'QHS')
            
        Returns
        -------
        list
            list of job indices with percent differences below the specified
            threshold
        """
        ns_hlf = self.begIdx + int(rBnd*self.ns)
        
        size_diff = self.eps_profile.shape[1] - eps_init.shape[0]
        eps_init = np.insert(eps_init, 0, np.zeros(size_diff))[self.begIdx:ns_hlf]
        eps_init_inv = 1. / eps_init
        
        compare = np.empty((self.job_num, 1+self.begIdx))
        for ind, eps in enumerate(self.eps_profile[0::,self.begIdx:ns_hlf]):
            comp = np.mean((eps - eps_init) * eps_init_inv) * 100
            compare[ind] = np.r_[comp, self.crnt_profile[ind][0:self.begIdx]]
        
        compare_nan = np.isnan(compare[0::,0])
        compare_not_nan = ~compare_nan
        compare = compare[compare_not_nan]
        
        compare = np.array( sorted(compare, key=lambda x: x[0])  )
        
        comps = compare[0::,0]
        compDiff = comps[comps < diff_thresh]
        
        jobID = compare[0::,1::]
        jobID = jobID[comps < diff_thresh]
        
        jPts = jobID.shape[0] 
        job_dom = np.arange(jPts) * jscl
        
        ax.plot([0, jPts*jscl], [0, 0], ls='--', c='k', label=comp_name)
        ax.plot(job_dom, compDiff, c='tab:orange')
        
        return np.insert(jobID, self.begIdx, compDiff, axis=1)
    
    def plot_difference_histogram(self, ax, eps_init, bins, rBnd=.5, diff_thresh=100):
        """ Plot the histogram of epsilon effective profiles with a percent 
        difference below the specified threshold when compared to the input
        epsEff profile across the specified radial domain.

        Parameters
        ----------
        ax : object
            Axis object on which to construct plot
        eps_init : array
            Input epsilon profile
        bins : int
            Number of bins in histogram.
        rBnd : float
            effective radial boundary over which comparison is performed 
            (default is 0.5)
        diff_thresh : float 
            only configurations with a percent difference below 
            this theshold will be plotted (default is 1)
            
        Returns
        -------
        list
            list of configuration ID numbers with percent differences below 
            the specified threshold.
        """
        ns_hlf = self.begIdx + int(rBnd*self.ns)
        
        size_diff = self.eps_profile.shape[1] - eps_init.shape[0]
        eps_init = np.insert(eps_init, 0, np.zeros(size_diff))[self.begIdx:ns_hlf]
        eps_init_inv = 1. / eps_init
        
        compare = np.empty((self.job_num, 1+self.begIdx))
        for ind, eps in enumerate(self.eps_profile[0::,self.begIdx:ns_hlf]):
            comp = np.mean((eps - eps_init) * eps_init_inv) * 100
            compare[ind] = np.r_[comp, self.crnt_profile[ind][0:self.begIdx]]
        
        compare_nan = np.isnan(compare[0::,0])
        compare_not_nan = ~compare_nan
        compare = compare[compare_not_nan]
        
        comps = compare[0::,0]
        compDiff = comps[comps <= diff_thresh]
        
        jobID = compare[0::,1::]
        jobID = jobID[comps <= diff_thresh]
        
        ax.hist(compDiff, bins=bins)
        return np.insert(jobID, self.begIdx, compDiff, axis=1)

    def save_order(self):
        """ Save the present configuration order.
        
        Raises
        ------
        ValueError
            no ordered profiles are available
        """
        try:
            self.crnt_ordered
        except:
            raise ValueError("No ordered profiles available (did run eps_compare?)")
        
        name = os.path.join(self.path, self.name)
        f = hf.File(name, 'a')
        
        del f[self.setID+'/config']
        del f[self.setID+'/eps_effective']
        
        f.create_dataset(self.setID+'/config', data=self.crnt_ordered)
        f.create_dataset(self.setID+'/eps_effective', data=self.eps_ordered)
        
        f.close()


    ###  Development Area ###
    def crnt_profile_prams_3D(self, pts=1000):
        tst = np.empty(pts)
        inv = np.empty(pts)
        mrr = np.empty(pts)
        
        for idx, crt in enumerate(self.crnt_profile[0:pts]):
            inv_evn = np.array([crt[0], crt[2], crt[4]])
            inv_odd = np.array([crt[5], crt[1], crt[3]])
            
            tst_evn = np.array([crt[1], crt[4], crt[5]])
            tst_odd = np.array([crt[0], crt[2], crt[4]])
            
            tst[idx] = int(np.sum((tst_evn - tst_odd)**2 / 16e6))
            inv[idx] = int(np.sum((inv_evn - inv_odd)**2 / 16e6))
            mrr[idx] = int(np.sum((crt[0:3] - crt[3::])**2 / 16e6))
            
        fnt=14
        cm = plt.cm.get_cmap('jet')
        fig = plt.figure()
        axs = fig.add_subplot(111, projection='3d')
        
        axs.set_xlim(0, 200)
        axs.set_ylim(0, 200)
        axs.set_zlim(0, 200)
        
        axs.set_xlabel('Mirror Value', fontsize=fnt)
        axs.set_ylabel('Inversion Value', fontsize=fnt)
        
        #axs.set_yscale('log')
        #axs.set_xscale('log')
        
        mrr_inv = {}
        for i in range(pts):
            key = '{0} {1} {2}'.format(mrr[i], inv[i], tst[i])
            if key in mrr_inv:
                mrr_inv[key]+=1
            else:
                mrr_inv[key]=1
        
        mrr_seg = []
        inv_seg = []
        tst_seg = []
        cnts = []
        for key in mrr_inv:
            key_spt = key.split()
            mrr_seg.append( float(key_spt[0]) )
            inv_seg.append( float(key_spt[1]) )
            tst_seg.append( float(key_spt[2]) )
            
            cnts.append(float(mrr_inv[key]))
        
        ax = axs.scatter(mrr_seg, inv_seg, tst_seg, c=cnts, s=1, marker='X', norm=LogNorm(vmin=1, vmax=68), cmap=cm)
        fig.colorbar(ax)
        
        axs.view_init(0, 270)
        
        if pts==self.job_num:
            plt.savefig('crnt_pram.png')
        else:
            plt.savefig('crnt_pram3D_{}.png'.format(pts))
        plt.close()
    
    def crnt_profile_prams(self, dirc, pts=1000):
        inv = np.empty(pts)
        mrr = np.empty(pts)
        
        for idx, crt in enumerate(self.crnt_profile[0:pts]):
            inv_evn = np.array([crt[0], crt[2], crt[4]])
            inv_odd = np.array([crt[5], crt[1], crt[3]])
            
            inv[idx] = int(np.sum((inv_evn - inv_odd)**2 / 16e6))
            mrr[idx] = int(np.sum((crt[0:3] - crt[3::])**2 / 16e6))
            
        fnt=14
        cm = plt.cm.get_cmap('jet')
        fig, axs = plt.subplots(1, 1)
        
        axs.set_xlim(0, 200)
        axs.set_ylim(0, 200)
        
        axs.set_xlabel('Mirror Value', fontsize=fnt)
        axs.set_ylabel('Inversion Value', fontsize=fnt)
        
        #axs.set_yscale('log')
        #axs.set_xscale('log')
        
        mrr_inv = {}
        for i in range(pts):
            key = '{0} {1}'.format(mrr[i], inv[i])
            if key in mrr_inv:
                mrr_inv[key]+=1
            else:
                mrr_inv[key]=1
        
        mrr_seg = np.array([])
        inv_seg = np.array([])
        cnts = np.array([])
        for key in mrr_inv:
            key_spt = key.split()            
            mrr_seg = np.append(mrr_seg, float(key_spt[0]))
            inv_seg = np.append(inv_seg, float(key_spt[1]))

            cnts = np.append(cnts, float(mrr_inv[key]))
        
        net_cnts_inv = 1. / np.sum(cnts)
        mrr_cen = net_cnts_inv * np.sum(mrr_seg * cnts)
        inv_cen = net_cnts_inv * np.sum(inv_seg * cnts)
                
        ax = axs.scatter(mrr_seg, inv_seg, c=cnts, s=1, norm=LogNorm(vmin=1, vmax=1144), cmap=cm)
        axs.scatter(36.05569212584262, 36.04722952659123, c='k', s=50, marker='X')
        axs.scatter(mrr_cen, inv_cen, c='tab:red', s=50, marker='X')
        fig.colorbar(ax)
                
        if pts==self.job_num:
            name = os.path.join(dirc, 'crnt_pram.png')
            plt.savefig(name)
        else:
            name = os.path.join(dirc, 'crnt_pram_{}.png'.format(pts))
            plt.savefig(name)
        plt.close()
    
    def animate_crnt_profile_prams(self, pts=1000):
        inv = np.empty(pts)
        mrr = np.empty(pts)
        
        for idx, crt in enumerate(self.crnt_profile[0:pts]):
            crt_evn = np.array([crt[0], crt[2], crt[4]])
            crt_odd = np.array([crt[5], crt[1], crt[3]])
            
            inv[idx] = int(np.sum((crt_evn - crt_odd)**2 / 16e6))
            mrr[idx] = int(np.sum((crt[0:3] - crt[3::])**2 / 16e6))
            
        fnt=14
        cm = plt.cm.get_cmap('jet')
        fig, axs = plt.subplots(1, 1)
        
        axs.set_xlim(0, 200)
        axs.set_ylim(0, 200)
        
        axs.set_xlabel('Mirror Value', fontsize=fnt)
        axs.set_ylabel('Inversion Value', fontsize=fnt)
        
        #axs.set_yscale('log')
        #axs.set_xscale('log')
        
        artist = []
        segs = np.linspace(1, pts, 750, dtype=int)
        for s in segs:
            mrr_inv = {}
            for i in range(s):
                key = '{0} {1}'.format(mrr[i], inv[i])
                if key in mrr_inv:
                    mrr_inv[key]+=1
                else:
                    mrr_inv[key]=1
            
            mrr_seg = np.array([])
            inv_seg = np.array([])
            cnts = np.array([])
            for key in mrr_inv:
                key_spt = key.split()
                mrr_seg = np.append(mrr_seg, float(key_spt[0]))
                inv_seg = np.append(inv_seg, float(key_spt[1]))
    
                cnts = np.append(cnts, float(mrr_inv[key]))
                    
            net_cnts_inv = 1. / np.sum(cnts)
            mrr_cen = net_cnts_inv * np.sum(mrr_seg * cnts)
            inv_cen = net_cnts_inv * np.sum(inv_seg * cnts)
                
            ax = axs.scatter(mrr_seg, inv_seg, c=cnts, s=1, marker='X', norm=LogNorm(vmin=1, vmax=1144), cmap=cm)
            pt = axs.scatter(mrr_cen, inv_cen, c='tab:red', s=50, marker='X')
            artist.append([ax, pt,])
            
        fig.colorbar(ax)
        axs.scatter(36.05569212584262, 36.04722952659123, c='k', s=50, marker='X')
        
        ani = an.ArtistAnimation(fig, artist)
        writer = an.FFMpegWriter(fps=25, codec='h264')
        if pts == self.job_num:
            ani.save('crnt_pram.mp4', writer=writer)
        else:
            ani.save('crnt_pram_{}.mp4'.format(pts), writer=writer)
        
        plt.close()