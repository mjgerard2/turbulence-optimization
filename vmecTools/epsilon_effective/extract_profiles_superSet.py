# -*- coding: utf-8 -*-
"""
Created on Wed Aug 26 23:09:36 2020

@author: micha
"""

import h5py as hf
import numpy as np
import read_epsSet as epsSet

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import directory_dict as dd

### Working Directory ###
basePath = 'E:\\'
setNUM = 1
setID = 'set_{}'.format(setNUM)
path = os.path.join(basePath, 'HSX_Configs')

### Extract Standard Main Coil Configuration ###
pathQHS = os.path.join(path, 'main_coil_0')
epsQHS = epsSet.epsData(pathQHS, 'set_1')

super_crnt = np.insert(np.insert(epsQHS.crnt_profile, 0, 1, axis=1), 0, 0, axis=1)
super_eps = np.insert(np.insert(epsQHS.eps_profile, 0, 1, axis=1), 0, 0, axis=1)

### Extracting Data From Specified Configurations ###
for i in range(1561):
    mainID = 'main_coil_{}'.format(i)
    mainPath = os.path.join(path, mainID)    
    
    hf_file = hf.File(os.path.join(mainPath,'conData.h5'),'r')
    eps_chk = 'eps_effective' in hf_file[setID]
    hf_file.close()
    
    if eps_chk:
        print('Reading in profiles from '+os.path.join(mainPath, setID))
        main = epsSet.epsData(mainPath, setID)
        
        main_crnt = np.insert(np.insert(main.crnt_profile, 0, setNUM, axis=1), 0, i, axis=1)
        main_eps = np.insert(np.insert(main.eps_profile, 0, setNUM, axis=1), 0, i, axis=1)
        
        super_crnt = np.append(super_crnt, main_crnt, axis=0)
        super_eps = np.append(super_eps, main_eps, axis=0)

### Save Super Set ###
hf_name = os.path.join(dd.exp_local['HSX'], 'epsEff_profiles.h5')
hf_new = hf.File(hf_name, 'w')

hf_new.create_dataset('superSet/all/config', data=super_crnt)
hf_new.create_dataset('superSet/all/eps_effective', data=super_eps)

hf_new.close()