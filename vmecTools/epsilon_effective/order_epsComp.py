# -*- coding: utf-8 -*-
"""
Created on Tue Mar 31 22:36:56 2020

@author: micha
"""

import read_epsSet as epsSet

import h5py as hf
import numpy as np

import os

### working directory ###
setID = 'set_1'
basePath = 'D:\\'
main_configs = np.arange(419, 1561)

path = os.path.join(basePath, 'HSX_Configs')

### runs comparison analysis ###
for i in main_configs:
    mainID = 'main_coil_{}'.format(i)
    mainPath = os.path.join(path, mainID)    
    
    hf_file = hf.File(os.path.join(mainPath,'conData.h5'),'r')
    eps_chk = 'eps_effective' in hf_file[setID]
    hf_file.close()
    
    if eps_chk:
        # read in working profiles    
        print('Ordering profiles in '+os.path.join(mainPath,setID))
        main_comp = epsSet.epsData(mainPath, setID)
        idxMin, idxMax = main_comp.find_extrema()
        main_comp.order_compare(main_comp.eps_profile[idxMin])
        main_comp.save_order()
