# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 00:31:43 2020

@author: micha
"""

import read_epsSet
import extraction_tools as exTools

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import directory_dict as dd
import plot_define as pd


def plot_ColorMap(epsData, epsComp, plot, jscl=1e-3, nJobs=False, save=False, savePath=os.path.join(os.getcwd(),'figures'), saveName='epsEff_ColorMap.png'):
    epsData.plot_profile_map(plot.fig, plot.ax, jscl=jscl, nJobs=nJobs)
    idxComp = epsData.find_compare(epsComp)
    plot.ax.plot([0, 1], [idxComp*jscl, idxComp*jscl], c='k', ls='--')
    
    if jscl==1e-6:
        plot.plt.ylabel('Configurations (1M)')
    elif jscl==1e-3:
        plot.plt.ylabel('Configurations (1k)')
    elif jscl==1e-2:
        plot.plt.ylabel('Configurations (100\'s)')
    elif jscl==1e-1:
        plot.plt.ylabel('Configurations (10\'s)')
    elif jscl==1:
        plot.plt.ylabel('Configurations')
        
    plot.plt.xlabel('r/a')
    
    plot.plt.tight_layout()
    if save:
        plot.plt.savefig(os.path.join(savePath, saveName))
    else:
        plot.plt.show()
        
def plot_ColorMapDiff(epsData, epsComp, plot, jscl=1e-3, nJobs=False, save=False, savePath=os.path.join(os.getcwd(),'figures'), saveName='epsEff_ColorMapDiff.png'):
    epsData.plot_difference_map(plot.fig, plot.ax, epsComp, jscl=jscl, nJobs=nJobs)
    idxComp = epsData.find_compare(epsComp)
    plot.ax.plot([0, 1], [idxComp*jscl, idxComp*jscl], c='k', ls='--')
    
    if jscl==1e-6:
        plot.plt.ylabel('Configurations (1M)')
    elif jscl==1e-3:
        plot.plt.ylabel('Configurations (1k)')
    elif jscl==1e-2:
        plot.plt.ylabel('Configurations (100\'s)')
    elif jscl==1e-1:
        plot.plt.ylabel('Configurations (10\'s)')
    elif jscl==1:
        plot.plt.ylabel('Configurations')
        
    plot.plt.xlabel('r/a')
    
    plot.plt.tight_layout()
    if save:
        plot.plt.savefig(os.path.join(savePath, saveName))
    else:
        plot.plt.show()
    
def plot_QHS_Diff(epsData, epsComp, plot, diff_thresh=100, jscl=1e-3, save=False, savePath=os.path.join(os.getcwd(),'figures'), saveName='epsEff_Diff.png'):
    conID = epsData.plot_difference(plot.ax, epsComp, diff_thresh=diff_thresh, jscl=jscl)
    
    if jscl==1e-6:
        plot.plt.xlabel('# of Configurations (1M)')
    elif jscl==1e-3:
        plot.plt.xlabel('# of Configurations (1k)')
    elif jscl==1e-2:
        plot.plt.xlabel('# of Configurations (100\'s)')
    elif jscl==1e-1:
        plot.plt.xlabel('# of Configurations (10\'s)')
    elif jscl==1:
        plot.plt.xlabel('# of Configurations')
        
    plot.plt.xlabel('# of Configurations (1k)')
    plot.plt.ylabel('% Diff')
    
    plot.plt.grid()
    plot.plt.legend()
    plot.plt.tight_layout()
    
    if save:
        plot.plt.savefig(os.path.join(savePath, saveName))
    else:
        plot.plt.show()
    
    return conID

def plot_QHS_Diff_Hist(epsData, epsComp, plot, bins=20, diff_thresh=100, save=False, savePath=os.path.join(os.getcwd(),'figures'), saveName='epsEff_Diff_Hist.png'):
    conID = epsData.plot_difference_histogram(plot.plt, epsComp, bins, diff_thresh=diff_thresh)
    
    plot.plt.xlabel('% Diff. Bins')
    plot.plt.ylabel('Counts')
    
    plot.plt.grid()
    plot.plt.tight_layout()
    
    if save:
        plot.plt.savefig(os.path.join(savePath, saveName))
    else:
        plot.plt.show()
        
    return conID

### Import QHS profile ###
pathQHS = dd.pathQHS_epsEff
epsQHS = exTools.extract_from_file(pathQHS)

### Import SuperSet File ###
path = dd.exp_local['HSX']
epsData = read_epsSet.epsData(path, 'superSet/all', name='epsEff_profiles.h5')

### Construct Plot ###
plot = pd.plot_define(fontSize=18, labelSize=24, lineWidth=3)
conID = plot_QHS_Diff(epsData, epsQHS, plot, bins=50, diff_thresh=10000, save=True, saveName='epsEff_Diff_Hist_10k.png')
