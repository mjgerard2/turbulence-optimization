import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.read_boozmn as rb
import gistTools.gist_reader as gt


def transform_periodic_grid(tor_norm, pol_norm):
    tor_in = tor_norm[(tor_norm >= -1) & (tor_norm <= 1)]
    while tor_in.shape[0] < tor_norm.shape[0]:
        tor_out_lft = 2 + tor_norm[tor_norm < -1]
        tor_out_rgt = -2 + tor_norm[tor_norm > 1]

        tor_in = np.insert(tor_in, 0, tor_out_lft)
        tor_in = np.append(tor_in, tor_out_rgt)
        tor_norm = tor_in
        tor_in = tor_norm[(tor_norm >= -1) & (tor_norm <= 1)]

    pol_in = pol_norm[(pol_norm >= -1) & (pol_norm <= 1)]
    while pol_in.shape[0] < pol_norm.shape[0]:
        pol_out_lft = 2 + pol_norm[pol_norm < -1]
        pol_out_rgt = -2 + pol_norm[pol_norm > 1]

        pol_in = np.insert(pol_in, 0, pol_out_lft)
        pol_in = np.append(pol_in, pol_out_rgt)
        pol_norm = pol_in
        pol_in = pol_norm[(pol_norm >= -1) & (pol_norm <= 1)]

    return tor_norm, pol_norm

def partition_periodic_grid(tor_norm, pol_norm):
    booz_set = []
    crnt_set = [[tor_norm[0], pol_norm[0]]]
    for i, pol in enumerate(pol_norm[1::]):
        tor = tor_norm[i]
        Dpol = pol - pol_norm[i-1]
        Dtor = tor - tor_norm[i-1]
        if np.abs(Dpol) > 1 or np.abs(Dtor) > 1:
            booz_set.append(np.array(crnt_set))
            crnt_set = [[tor, pol]]
        else:
            crnt_set.append([tor, pol])
    booz_set.append(np.array(crnt_set))

    return booz_set, crnt_set

def incremental_flux_surface_calculation(booz, psiN, num_of_incr, num_of_pol, num_of_tor):
    """ Calculate a modB straight field-line plot incrementally. This is necessary
    when the desired poloidal/toroidal resolution exceeds you system's memory capacity.

    Parameters
    ----------
    booz : obj
        Boozer file object
    psiN : float
        Radial like s coordinate of flux surface
    num_of_incr : int
        Number of inremental calculations
    num_of_pol : int
        Number of polidal grid points
    num_of_tor : int
        Number of toroidal grid points

    Returns
    -------
    pol_dom : arr
        1D array of poloidal grid points
    tor_dom : arr
        1D array of toroidal grid points
    B : arr
        2D array of |B| on poloidal/toroidal grid
    """
    pol_dom = np.linspace(-np.pi, np.pi, num_of_pol)
    tor_dom = np.linspace(-np.pi, np.pi, num_of_tor)
    B = np.zeros((num_of_tor, num_of_pol))

    # Calculate Flux-Surface Data Incrementally #
    tor_end = tor_dom[0]
    end_idx = 0
    tor_interval = (tor_dom[-1] - tor_dom[0]) / (num_of_incr)
    for j in range(num_of_incr):
        print('\t({0}|{1})'.format(j+1, num_of_incr))
        tor_beg = tor_end
        tor_end = tor_dom[0] + (j+1)*tor_interval
        tor_sub_dom = tor_dom[(tor_dom >= tor_beg) & (tor_dom < tor_end)]
        booz.boozCoord_2D_sSec(psiN, pol_dom, tor_sub_dom, ['Bmod'])

        beg_idx = end_idx
        end_idx = beg_idx + tor_sub_dom.shape[0]
        B[beg_idx:end_idx, :] = booz.invFourAmps['Bmod']

    booz.boozCoord_2D_sSec(psiN, pol_dom, np.array([tor_dom[-1]]), ['Bmod'])
    B[num_of_tor-1, :] = booz.invFourAmps['Bmod']

    return pol_dom, tor_dom, B


# Absolute Path of Data Files #
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'wout_QHS_best.nc')
booz_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'boozmn_wout_QHS_best.nc')
gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'gist_QHS_best_alf0_s0p5_res2048_npol8.dat')

# Read Boozer and GIST Files #
booz = rb.readBooz(booz_path, wout_path)
gist = gt.read_gist(gist_path)
gist.partition_wells()

num_of_wells = len(gist.partition) - 1
idx_lim = -np.floor(0.5*num_of_wells)
well_idx = np.arange(idx_lim, -idx_lim+1, dtype=int)

# Calculate Mod-B on Grid #
# u_dom = np.linspace(-np.pi, np.pi, upts)
# v_dom = np.linspace(-np.pi, np.pi, vpts)

#ampKeys = ['Bmod']
# booz.boozCoord_2D_sSec(gist.s0, u_dom, v_dom, ampKeys)
#Bmod = booz.invFourAmps['Bmod']
upts = 15
vpts = 4*upts

u_dom, v_dom, Bmod = incremental_flux_surface_calculation(booz, gist.s0, 10, upts, vpts)
v_norm = v_dom / np.pi
u_norm = u_dom / np.pi

# Get Field Line Boozer Coordinates #
pol_dom = gist.pol_dom

booz_well = []
crnt_well = []

pol_min, pol_max = 0, 0
well_IDs = np.arange(-2, 1, dtype=int)
for well_ID in well_IDs:
    well_key = 'well {}'.format(well_ID)
    pol_lft, pol_mid, pol_rgt = gist.partition[well_key]['pol']
    pol = gist.pol_dom[(pol_dom >= pol_lft) & (pol_dom <= pol_rgt)]

    if pol[0] < pol_min:
        pol_min = pol[0]
    if pol[-1] > pol_max:
        pol_max = pol[-1]

    p_norm = pol / np.pi
    t_norm = (pol * gist.q0) / np.pi

    t_norm, p_norm = transform_periodic_grid(t_norm, p_norm)
    b_well, c_well = partition_periodic_grid(t_norm, p_norm)

    booz_well.append(b_well)
    c_well.append(c_well)

tor_min = pol_min * gist.q0
tor_max = pol_max * gist.q0

pol_dom_min = gist.pol_dom[gist.pol_dom < pol_min]
pol_dom_max = gist.pol_dom[gist.pol_dom > pol_max]

tor_dom_min = pol_dom_min * gist.q0
tor_dom_max = pol_dom_max * gist.q0

pol_norm_min = pol_dom_min / np.pi
pol_norm_max = pol_dom_max / np.pi

tor_norm_min = tor_dom_min / np.pi
tor_norm_max = tor_dom_max / np.pi

tor_norm_min, pol_norm_min = transform_periodic_grid(tor_norm_min, pol_norm_min)
tor_norm_max, pol_norm_max = transform_periodic_grid(tor_norm_max, pol_norm_max)

booz_set_min, crnt_set_min = partition_periodic_grid(tor_norm_min, pol_norm_min)
booz_set_max, crnt_set_max = partition_periodic_grid(tor_norm_max, pol_norm_max)

# Define Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 16}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['lines.linewidth'] = 3

# Construct Plotting Axis #
fig, ax = plt.subplots(1, 1, tight_layout=True)

# Plot Mod-B #
Bmin_scl = 0.9 * np.min(Bmod)
Bmax_scl = 1.1 * np.max(Bmod)
# smap = ax.pcolormesh(v_norm, u_norm, Bmod.T, vmin=Bmin_scl, vmax=Bmax_scl, cmap='gist_gray')
smap = ax.pcolormesh(v_norm, u_norm, Bmod.T, vmin=Bmin_scl, vmax=Bmax_scl, cmap='magma')

# Plot Field Line #
for i, well_ID in enumerate(well_IDs):
    for bz_set in booz_well[i]:
        if bz_set.shape[0] > 1:
            if i % 3 == 0:
                ax.plot(bz_set[:, 0], bz_set[:, 1], c='w', ls=':')  # (0, (1, 1)))
            elif i % 3 == 1:
                ax.plot(bz_set[:, 0], bz_set[:, 1], c='w', ls='--')  # (0, (3, 1, 1, 1)))
            elif i % 3 == 2:
                ax.plot(bz_set[:, 0], bz_set[:, 1], c='w', ls='-')

rot = np.arctan(1./gist.q0) * (180/np.pi)
ax.text(-0.15, -0.02, r'$i = 0$', rotation=rot, color='w', fontsize=20)
ax.text(-0.85, -0.75, r'$i = 1$', rotation=rot, color='w', fontsize=20)
ax.text(0.5, 0.55, r'$i = 2$', rotation=rot, color='w', fontsize=20)
# ax.text(-0.25, -0.5, r'$i = 3$', rotation=rot, color='w', fontsize=20)
# ax.text(-0.7, -1., r'$i = 4$', rotation=rot, color='w', fontsize=20)
# ax.text(0.4, 0.05, r'$i = 5$', rotation=rot, color='w', fontsize=20)
"""
colors = plt.cm.viridis(np.linspace(0, 1, 3))
lines = ['-', '--']
labs = [r'$\Delta \zeta \approx 2\pi$', r'$\Delta \zeta \approx 4\pi$']
for i, well_ID in enumerate(np.flip(well_IDs)):
    color = colors[abs(well_ID) % 3]
    line = lines[int(abs(well_ID)/3)]
    lab = labs[int(abs(well_ID)/3)]
    for bz_set in np.flip(booz_well)[i]:
        if bz_set.shape[0] > 1:
            if (well_ID % 3) == 0:
                ax.plot(bz_set[:, 0], bz_set[:, 1], c=color, ls=line, label=lab)
            else:
                ax.plot(bz_set[:, 0], bz_set[:, 1], c=color, ls=line)

# Axis Legend #
ax.legend()
leg = ax.get_legend()
[lgd.set_color('black') for lgd in leg.legendHandles]
"""
# Axis Labels #
cax = fig.colorbar(smap, ax=ax)
cax.ax.set_ylabel(r'$B \ (T)$')

ax.set_xlabel(r'$\zeta / \pi$')
ax.set_ylabel(r'$\theta / \pi$')

# Show or Save Figure #
plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'Prelim_Defense', 'figures', 'flat_torus_flux_tube.png')
# plt.savefig(save_path)
