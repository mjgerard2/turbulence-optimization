import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization')
sys.path.append(WORKDIR)

import plot_define as pd
import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.read_boozmn as rb


def compute_B_field(booz, psiN, tor_dom, pol_dom, num_of_incr):
    tor_end = tor_dom[0]
    end_idx = 0
    tor_interval = (tor_dom[-1] - tor_dom[0]) / (num_of_incr)
    B_show = np.zeros((num_of_tor, num_of_pol))
    for j in range(num_of_incr):
        print('\t({0}|{1})'.format(j+1, num_of_incr))
        tor_beg = tor_end
        tor_end = tor_dom[0] + (j+1)*tor_interval
        tor_sub_dom = tor_dom[(tor_dom >= tor_beg) & (tor_dom < tor_end)]
        booz.boozCoord_2D_sSec(psiN, pol_dom, tor_sub_dom, ['Bmod'])

        beg_idx = end_idx  # np.argmin(np.abs(tor_dom - tor_beg))
        end_idx = beg_idx + tor_sub_dom.shape[0]
        B_show[beg_idx:end_idx, :] = booz.invFourAmps['Bmod']

    booz.boozCoord_2D_sSec(psiN, pol_dom, np.array([tor_dom[-1]]), ['Bmod'])
    B_show[num_of_tor-1, :] = booz.invFourAmps['Bmod']

    return B_show

# Define Data Paths HE-QHS #
path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data')
booz_path_he = os.path.join(path, 'boozmn_Flip14_10p0_mn1824_ns101.nc')
vmec_path_he = os.path.join(path, 'wout_Flip14_10p0_mn1824_ns101.nc')

# Define Data Paths QHS #
path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data')
booz_path_qhs = os.path.join(path, 'boozmn_wout_QHS_best.nc')
vmec_path_qhs = os.path.join(path, 'wout_QHS_best.nc')

# grid parameters #
psi_qhs = 0.5
num_of_incr = 20
num_of_pol = 501
num_of_tor = num_of_pol

# get minor radii #
wout_qhs = wr.readWout(vmec_path_qhs)
a_qhs = wout_qhs.a_minor
wout_he = wr.readWout(vmec_path_he)
psi_he = 0.5 # psi_qhs*((a_qhs/wout_he.a_minor)**2)

# generate grid #
pol_dom = np.linspace(-np.pi, np.pi, num_of_pol)
tor_dom = np.linspace(-np.pi/4, np.pi/4, num_of_tor)

# compute QHS data #
booz_qhs = rb.readBooz(booz_path_qhs, vmec_path_qhs)
iota_qhs = booz_qhs.iota(psi_qhs)
B_qhs = compute_B_field(booz_qhs, psi_qhs, tor_dom, pol_dom, num_of_incr)

# compute HE-QHS data #
booz_he = rb.readBooz(booz_path_he, vmec_path_he)
iota_he = booz_he.iota(psi_he)
B_he = compute_B_field(booz_he, psi_he, tor_dom, pol_dom, num_of_incr)

# Plotting Parameters #
plot = pd.plot_define(fontSize=18, labelSize=20)
plt = plot.plt
fig, axs = plt.subplots(1, 2, tight_layout=True, figsize=(16, 6))
ax1, ax2 = axs[0], axs[1]

# imshow args #
ncolors = 16
cmap = 'Spectral_r'
cmap_dom = plt.get_cmap(cmap, ncolors)
extent = [tor_dom[0]/np.pi, tor_dom[-1]/np.pi, pol_dom[0]/np.pi, pol_dom[-1]/np.pi]
args = dict(cmap=cmap_dom, extent=extent, origin='lower', aspect='auto')

# colorbar boundaries #
cmap_bounds_qhs = np.linspace(B_qhs.min(), B_qhs.max(), ncolors+1)
cbar_norm_qhs = mpl.colors.BoundaryNorm(cmap_bounds_qhs, ncolors=cmap_dom.N)
cmap_bounds_he = np.linspace(B_he.min(), B_he.max(), ncolors+1)
cbar_norm_he = mpl.colors.BoundaryNorm(cmap_bounds_he, ncolors=cmap_dom.N)

# plot QHS Boozer data #
args['norm'] = cbar_norm_qhs
smap_qhs = ax1.imshow(B_qhs.T, **args)
ax1.contour(tor_dom/np.pi, pol_dom/np.pi, B_qhs.T, cmap_bounds_qhs, colors='k', linewidths=0.5)
ax1.plot(tor_dom/np.pi, (tor_dom*iota_qhs)/np.pi, c='k', ls='--', lw=4)

# plot HE-QHS Boozer data #
args['norm'] = cbar_norm_he
smap_he = ax2.imshow(B_he.T, **args)
ax2.contour(tor_dom/np.pi, pol_dom/np.pi, B_he.T, cmap_bounds_he, colors='k', linewidths=0.5)
ax2.plot(tor_dom/np.pi, (tor_dom*iota_he)/np.pi, c='k', ls='--', lw=4)

# colorbars #
cbar_qhs = fig.colorbar(smap_qhs, ax=ax1)
cbar_qhs.set_ticklabels(['{0:0.2f}'.format(x) for x in cmap_bounds_qhs])
cbar_qhs.ax.set_ylabel(r'$B \ / \ T$')
cbar_he = fig.colorbar(smap_he, ax=ax2)
cbar_he.set_ticklabels(['{0:0.2f}'.format(x) for x in cmap_bounds_he])
cbar_he.ax.set_ylabel(r'$B \ / \ T$')

# axis text #
xtxt, ytxt = 0.05, 0.95
props = dict(boxstyle='round', facecolor='w', alpha=1.0)
ax1.text(xtxt, ytxt, r'QHS $\psi/\psi_\mathrm{{edge}} = {0:0.2f}$'.format(psi_qhs), transform=ax1.transAxes, va='top', ha='left', bbox=props)
ax2.text(xtxt, ytxt, r'Mirror $\psi/\psi_\mathrm{{edge}} = {0:0.2f}$'.format(psi_he), transform=ax2.transAxes, va='top', ha='left', bbox=props)

# loop axes #
for ax in axs.flat:
    # labels #
    ax.set_xlabel(r'$\zeta / \pi$')
    ax.set_ylabel(r'$\theta / \pi$')

    # limits #
    ax.set_xlim(tor_dom[0]/np.pi, tor_dom[-1]/np.pi)
    ax.set_ylim(pol_dom[0]/np.pi, pol_dom[-1]/np.pi)

    # ticks #
    ax.tick_params(axis='both', which='major', direction='out', color='k', length=10)
    ax.tick_params(axis='both', which='minor', direction='out', color='k', length=5)
    ax.xaxis.set_ticks_position('default')
    ax.yaxis.set_ticks_position('default')
    ax.xaxis.set_minor_locator(MultipleLocator(0.02))
    ax.yaxis.set_minor_locator(MultipleLocator(0.05))

# save/show #
# plt.show()
save_name = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'figures', 'flat_torus_QHS_vs_Flip14.pdf')
plt.savefig(save_name, format='pdf')
