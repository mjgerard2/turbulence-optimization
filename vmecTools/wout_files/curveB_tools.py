# -*- coding: utf-8 -*-
"""
Created on Wed May 27 16:44:31 2020

@author: micha
"""

import numpy as np
import matplotlib.pyplot as plt

from scipy.interpolate import griddata

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

from vtkTools import vtk_grids
from vmecTools.wout_files import coord_convert

class BcurveTools():

    def __init__(self, wout):

        self.phi_dom = wout.phi_dom

        self.s_dom = wout.s_dom
        self.v_dom = wout.v_dom
        self.u_dom = wout.u_dom

        self.s_num, self.v_num, self.u_num = wout.s_num, wout.v_num, wout.u_num

        self.R_coord = wout.invFourAmps['R']
        self.Z_coord = wout.invFourAmps['Z']

        self.ndim = self.R_coord.ndim

        if self.ndim > 1:

            if self.ndim == 2:

                if self.s_num == 1:
                    self.T_coord = np.empty(self.R_coord.shape)
                    for idx, v in enumerate(wout.v_dom):
                        self.T_coord[idx,0::][:] = v

                elif self.v_num == 1:
                    self.T_coord = np.full(self.R_coord.shape, wout.v_dom)

            elif self.ndim == 3:
                self.T_coord = np.empty(self.R_coord.shape)
                for idx, v in enumerate(wout.v_dom):
                    self.T_coord[0::,idx,0::][:] = v

            else:
                raise ValueError('{} is not an appropriate number of dimesnions.'.format(self.ndim))

            self.coord_data = np.stack((self.R_coord, self.T_coord, self.Z_coord), axis=self.ndim)

        else:
            self.T_coord = wout.v_dom
            self.coord_data = np.array([self.R_coord, self.T_coord, self.Z_coord])

        self.wout = wout

        try:
            # self.gss, self.gsu, self.gsv, self.guu, self.guv, self.gvv, self.g_ss, self.g_su, self.g_sv, self.g_uu, self.g_uv, self.g_vv = coord_convert.compute_metric(wout)
            self.g_ss, self.g_uu, self.g_vv, self.g_su, self.g_uv, self.g_vs, self.gss, self.guu, self.gvv, self.gsu, self.guv, self.gvs = coord_convert.compute_metric(wout)
            self.jacob = wout.invFourAmps['Jacobian']
            self.jacob_inv = 1. / self.jacob

            self.Bu = wout.invFourAmps['Bu_contra']
            self.Bv = wout.invFourAmps['Bv_contra']

            self.B_s = wout.invFourAmps['Bs_covar']
            self.B_u = wout.invFourAmps['Bu_covar']
            self.B_v = wout.invFourAmps['Bv_covar']

            self.B = wout.invFourAmps['Bmod']
            self.B_inv = 1. / self.B

            self.dB_ds = wout.invFourAmps['dBmod_ds']
            self.dB_du = wout.invFourAmps['dBmod_du']
            self.dB_dv = wout.invFourAmps['dBmod_dv']

            self.dBs_du = wout.invFourAmps['dBs_du']
            self.dBs_dv = wout.invFourAmps['dBs_dv']

            self.dBu_ds = wout.invFourAmps['dBu_ds']
            self.dBu_dv = wout.invFourAmps['dBu_dv']

            self.dBv_ds = wout.invFourAmps['dBv_ds']
            self.dBv_du = wout.invFourAmps['dBv_du']

            self.dL_ds = wout.invFourAmps['dL_ds']
            self.dL_du = wout.invFourAmps['dL_du']
            self.dL_dv = wout.invFourAmps['dL_dv']

        except KeyError:
            raise KeyError('Not all keys needed for curvature calculations are provided in wout.invFourAmps.')
        """
        # self.bs, self.bu, self.bv = B_inv*Bs, B_inv*Bu, B_inv*Bv
        self.Bu = Bu
        self.Bv = Bv
        self.B_s = B_s
        self.B_u = B_u
        self.B_v = B_v

        self.B = B
        self.B_inv = B_inv

        self.dbs_du = B_inv * (dBs_du - self.bs * dB_du)
        self.dbs_dv = B_inv * (dBs_dv - self.bs * dB_dv)

        self.dbu_ds = B_inv * (dBu_ds - self.bu * dB_ds)
        self.dbu_dv = B_inv * (dBu_dv - self.bu * dB_dv)

        self.dbv_ds = B_inv * (dBv_ds - self.bv * dB_ds)
        self.dbv_du = B_inv * (dBv_du - self.bv * dB_du)

        self.dB_ds = dB_ds
        self.dB_du = dB_du
        self.dB_dv = dB_dv
        """

    def calc_curvature(self, vtk_make=None, vtk_dict=None):
        """ Calculates the magnetic field normalized and geodesic curvature.

        vtk_make : None or str, optional
            If not None, the string should be a key entry for the feature to
            be plotted as VTK file. The default is None.
                options
                    'normCurve' : Normalized curvature
                    'geodCurve' : Geodesic of curvature
                    'vecCurve' : Vector of curvature, [\kappa_n, \kappa_g]
        vtk_dict : None or dict, optional
            If not None, the dictionary should contain the directory path and
            file name of the VTK file to be saved. The default is None.
                'savePath' : save directory
                'name' : name of VTK file being saved

        Returns
        -------
        Array
            Curvature on specified grid.
        """
        # k_s = (self.g_sv*self.bs + self.g_uv*self.bu + self.g_vv*self.bv)*(self.dbs_dv - self.dbv_ds) - (self.g_su*self.bs + self.g_uu*self.bu + self.g_uv*self.bv)*(self.dbu_ds - self.dbs_du)
        # k_u = (self.g_ss*self.bs + self.g_su*self.bu + self.g_sv*self.bv)*(self.dbu_ds - self.dbs_du) - (self.g_sv*self.bs + self.g_uv*self.bu + self.g_vv*self.bv)*(self.dbv_du - self.dbu_dv)
        # k_v = (self.g_su*self.bs + self.g_uu*self.bu + self.g_uv*self.bv)*(self.dbv_du - self.dbu_dv) - (self.g_ss*self.bs + self.g_su*self.bu + self.g_sv*self.bv)*(self.dbs_dv - self.dbv_ds)
        self.k_s = (1./(self.B_inv**2))*(self.Bu*(self.B_u*self.B_inv*self.dB_ds + self.dBu_ds - self.B_s*self.B_inv*self.dB_du - self.dBs_du) - self.Bv*(self.B_v*self.B_inv*self.dB_ds - self.dBv_ds - self.B_s*self.B_inv*self.dB_dv + self.dBs_dv))
        self.k_u = (self.Bv/(self.B_inv**2))*(self.B_v*self.B_inv*self.dB_du + self.dBv_du - self.B_u*self.B_inv*self.dB_dv - self.dBu_dv)
        self.k_v = (self.Bu/(self.B_inv**2))*(self.B_u*self.B_inv*self.dB_dv + self.dBu_dv - self.B_v*self.B_inv*self.dB_du - self.dBv_du)
        
        # self.k_norm = k_s
        # self.k_geod = self.jacob_inv * (self.gss * (k_v*self.bu - k_u*self.bv) + self.gsu * (k_s*self.bv - k_v*self.bs) + self.gsv * (k_u*self.bs - k_s*self.bu))
        sign = np.sign(np.mean(self.jacob))
        self.k_norm = sign*(self.k_s*self.gss + self.k_u*self.gsu + self.k_v*self.gvs)/np.sqrt(self.gss)
        self.k_geod = sign*(self.k_v*self.B_u - self.k_u*self.B_v)/(self.jacob*self.B*np.sqrt(self.gss))
        
        if self.ndim == 1:
            self.kappa = np.stack((self.k_norm, self.k_geod, np.zeros(self.k_norm.shape)), axis=1)  
        elif self.ndim == 2:
            self.kappa = np.stack((self.k_norm, self.k_geod, np.zeros(self.k_norm.shape)), axis=2)  
        else:
            self.kappa = np.stack((self.k_norm, self.k_geod, np.zeros(self.k_norm.shape)), axis=3)  
            
        if vtk_make and vtk_dict:
            if self.ndim == 2:
                cartCoord = coord_convert.cartCoord(self.wout)
                if vtk_make == 'normCurve':
                    vtk_grids.scalar_mesh(vtk_dict['savePath'], vtk_dict['name'], cartCoord, self.k_norm)
                elif vtk_make == 'geodCurve':
                    vtk_grids.scalar_mesh(vtk_dict['savePath'], vtk_dict['name'], cartCoord, self.k_geod)
                elif vtk_make == 'vecCurve':
                    vtk_grids.vector_mesh(vtk_dict['savePath'], vtk_dict['name'], cartCoord, self.kappa)
                else:
                    raise KeyError('{} is not a valide vtk_make key.'.format(vtk_make))
            else:
                raise ValueError('Cannon make vtk file with {} dimensional grid.'.format(self.ndim))                             

    def calc_Bk_overlap(self, vtk_make=None, vtk_dict=None):
        """ Calculate the amount of overlap between the magnetic wells and 
        positive normalized curvature and magnetic hills and negative 
        normalized curvature on a flux surface.  The calculation is
        approximately
        
        \frac{ \int ((B - <B>) / <B>) \vec{\kappa} \cdot \vec{da} }{ \int da }

        Parameters
        ----------
        vtk_make : None or str, optional
            If not None, the string should be a key entry for the feature to
            be plotted as VTK file. The default is None.
                options
                    'Bk_overlap' : differential overlap calculation
                    'dA_Norm' : differential suface area
        vtk_dict : None or dict, optional
            If not None, the dictionary should contain the directory path and
            file name of the VTK file to be saved. The default is None.
                'savePath' : save directory
                'name' : name of VTK file being saved
                
        Raises
        ------
        KeyError
            vtk_make string provided was invalid.
        ValueError
            Invalid dimensionality in wout data.  Must be dimesnion 2 or 3.
        """
        du = self.u_dom[1] - self.u_dom[0]
        dv = self.v_dom[1] - self.v_dom[0]
        da = du * dv
        
        if self.ndim == 3:
            da_norm = np.abs(da * self.jacob[1::]) * np.sqrt(self.g_ss[1::])
            k_dot_da = np.abs(da * self.jacob[1::]) * self.k_s[1::]
            B_da = self.B[1::] * da_norm            
            
            self.surf_area = np.zeros(self.s_num)
            self.Bk_overlap = np.zeros(self.s_num)
            for idx in range(1,self.s_num-1):
                SA = np.sum(da_norm[idx,0:-1,0:-1])
                B_avg = np.sum(B_da[idx,0:-1,0:-1]) / SA
                B_shft = (self.B[idx,0:-1,0:-1] - B_avg) / B_avg
                
                self.Bk_overlap[idx] = np.sum(B_shft * k_dot_da[idx,0:-1,0:-1]) / SA
                self.surf_area = SA

            if vtk_make and vtk_dict:
                print('Sorry, this code is not yet ready to make 3D flux surface VTKs')
            
        elif self.ndim == 2:
            da_norm = np.abs(da * self.jacob) * np.sqrt(self.g_ss)
            k_dot_da = np.abs(da * self.jacob) * self.k_s
            
            self.surf_area = np.sum(da_norm)
            SA_inv = 1. / self.surf_area
            B_avg = np.sum(self.B[0:-1,0:-1]*da_norm[0:-1,0:-1]) * SA_inv
            
            B_shft = (self.B - B_avg) / B_avg
            self.Bk_overlap = B_shft * k_dot_da * SA_inv
            
            if vtk_make and vtk_dict:
                if self.ndim == 2:
                    cartCoord = coord_convert.cartCoord(self.wout)
                    if vtk_make == 'Bk_Overlap':
                        vtk_grids.scalar_mesh(vtk_dict['savePath'], vtk_dict['name'], cartCoord, self.Bk_overlap)
                    elif vtk_make == 'dA_Norm':
                        vtk_grids.scalar_mesh(vtk_dict['savePath'], vtk_dict['name'], cartCoord, da_norm)
                    else:
                        raise KeyError('{} is not a valide vtk_make key.'.format(vtk_make))
                else:
                    raise ValueError('Cannon make vtk file with {} dimensional grid.'.format(self.ndim))                             
        
        else:
            raise ValueError('{} is not a workabe dimensionality for the Bk overlap calculation')
            
    def calc_Gss_Kn_metric(self, vtk_make=None, vtk_dict=None):
        """ Calculate the flux surface average of the product of the Gss 
        metric and normlized curvature.  The calculation is
        approximately
        
        \frac{ \int G_{ss} \vec{\kappa} \cdot \vec{da} }{ \int da }

        Parameters
        ----------
        vtk_make : None or str, optional
            If not None, the string should be a key entry for the feature to
            be plotted as VTK file. The default is None.
                options
                    'GssKn_metric' : differential overlap calculation
        vtk_dict : None or dict, optional
            If not None, the dictionary should contain the directory path and
            file name of the VTK file to be saved. The default is None.
                'savePath' : save directory
                'name' : name of VTK file being saved
                
        Raises
        ------
        KeyError
            vtk_make string provided was invalid.
        ValueError
            Invalid dimensionality in wout data.  Must be dimesnion 2 or 3.
        """
        du = self.u_dom[1] - self.u_dom[0]
        dv = self.v_dom[1] - self.v_dom[0]
        da = du * dv
        
        if self.ndim == 3:
            da_norm = np.abs(da * self.jacob[1::]) * np.sqrt(self.g_ss[1::])
            Gss_k_dot_da = np.abs(da * self.jacob[1::]) * self.k_s[1::] * self.gss[1::]
            
            self.GssKn_metric = np.zeros(self.s_num)
            for idx in range(1,self.s_num-1):
                SA = np.sum(da_norm[idx,0:-1,0:-1])
                self.GssKn_metric[idx] = np.sum(Gss_k_dot_da[idx,1:,1:]) / SA

            if vtk_make and vtk_dict:
                print('Sorry, this code is not yet ready to make 3D flux surface VTKs')
            
        elif self.ndim == 2:
            da_norm = np.abs(da * self.jacob) * np.sqrt(self.g_ss)
            Gss_k_dot_da = np.abs(da * self.jacob) * self.k_s * self.gss
            
            SA_inv = 1. / np.sum(da_norm)
            self.GssKn_metric = Gss_k_dot_da * SA_inv
            
            if vtk_make and vtk_dict:
                if self.ndim == 2:
                    cartCoord = coord_convert.cartCoord(self.wout)
                    if vtk_make == 'GssKn_metric':
                        vtk_grids.scalar_mesh(vtk_dict['savePath'], vtk_dict['name'], cartCoord, self.GssKn_metric)
                    else:
                        raise KeyError('{} is not a valide vtk_make key.'.format(vtk_make))
                else:
                    raise ValueError('Cannon make vtk file with {} dimensional grid.'.format(self.ndim))                             
        
        else:
            raise ValueError('{} is not a workabe dimensionality for the Bk overlap calculation')
    
    def calc_drift(self, vtk_make=None, vtk_dict=None):
        """ Calculate the radial drift equation.
        
        \frac{ \hat{\vec{b}} \cdot (\nabla\Psi \times \nabla B) }{ B_{surf} }

        Parameters
        ----------
        vtk_make : None or str, optional
            If not None, the string should be a key entry for the feature to
            be plotted as VTK file. The default is None.
                options
                    'part_drift' : particle drift
        vtk_dict : None or dict, optional
            If not None, the dictionary should contain the directory path and
            file name of the VTK file to be saved. The default is None.
                'savePath' : save directory
                'name' : name of VTK file being saved
                
        Raises
        ------
        KeyError
            vtk_make string provided was invalid.
        ValueError
            Invalid dimensionality in wout data.  Must be dimesnion 2 or 3.
        """
        du = self.u_dom[1] - self.u_dom[0]
        dv = self.v_dom[1] - self.v_dom[0]
        da = du * dv
        
        if self.ndim == 3:
            da_norm = np.abs(da * self.jacob[1::]) * np.sqrt(self.g_ss[1::])
            gradB_surfNorm = np.sqrt((self.dB_ds[1::]**2) * (self.gss[1::] + self.g_ss[1::] - 2) + self.g_uu[1::]*(self.dB_du[1::]**2) + self.g_vv[1::]*(self.dB_dv[1::]**2) + 2*self.g_su[1::]*self.dB_ds[1::]*self.dB_du[1::] + 2*self.g_sv[1::]*self.dB_ds[1::]*self.dB_dv[1::] + 2*self.g_uv[1::]*self.dB_du[1::]*self.dB_dv[1::])
            drift = (self.jacob_inv[1::] * (self.bs[1::] * (self.gsu[1::]*self.dB_dv[1::] - self.gsv[1::]*self.dB_du[1::]) + self.bu[1::] * (self.gsv[1::]*self.dB_ds[1::] - self.gss[1::]*self.dB_dv[1::]) + self.bv[1::] * (self.gss[1::]*self.dB_du[1::] - self.gsu[1::]*self.dB_ds[1::]))) / gradB_surfNorm        
            drift_da = drift * da_norm
            
            self.surf_area = np.zeros(self.s_num)
            self.drift = np.zeros(self.s_num)
            for idx in range(1,self.s_num-1):
                SA = np.sum(da_norm[idx][1::,1::])
                self.surf_area[idx] = SA
                self.drift[idx] = np.sum(drift_da[idx][1::,1::]) / SA
                            
            if vtk_make and vtk_dict:
                print('Sorry, this code is not yet ready to make 3D flux surface VTKs')
                
        elif self.ndim == 2:
            gradB_surfNorm = np.sqrt((self.dB_ds**2) * (self.gss + self.g_ss - 2) + self.g_uu*(self.dB_du**2) + self.g_vv*(self.dB_dv**2) + 2*self.g_su*self.dB_ds*self.dB_du + 2*self.g_sv*self.dB_ds*self.dB_dv + 2*self.g_uv*self.dB_du*self.dB_dv)
            self.drift = (self.jacob_inv * (self.bs * (self.gsu*self.dB_dv - self.gsv*self.dB_du) + self.bu * (self.gsv*self.dB_ds - self.gss*self.dB_dv) + self.bv * (self.gss*self.dB_du - self.gsu*self.dB_ds))) / gradB_surfNorm        
            
            if vtk_make and vtk_dict:
                if self.ndim == 2:
                    if vtk_make == 'part_drift':
                        cartCoord = coord_convert.cartCoord(self.wout)
                        vtk_grids.scalar_mesh(vtk_dict['savePath'], vtk_dict['name'], cartCoord, self.drift)
                    else:
                        raise KeyError('{} is not a valide vtk_make key.'.format(vtk_make))
                else:
                    raise ValueError('Cannon make vtk file with {} dimensional grid.'.format(self.ndim))                             
                    
    def calc_drift_velocity(self, pitch, energy, mass=9.1094e-31, charge=-1.6022e-19, vtk_dict=None):
        """ This function calculates the drift wave velocity for particles with
        the specified parameters.  The particle parameters are provided in SI
        units, except for energy, which is provided in eV.

        Parameters
        ----------
        pitch : float
            Pitch angle of the particle velocity.
        energy : float
            The energy of the particle, provided in Joules.
        mass : float, optional
            The particle mass, provided in kg. The default is 9.1094e-31, 
            which is the electron mass.
        charge : float, optional
            The particle charge, provided in C. The default is -1.6022e-19, 
            which is the electron charge.
        vtk_dict : None or dict, optional
            If not None, the dictionary should contain the directory path and
            file name of the VTK file to be saved. The default is None.
                'savePath' : save directory
                'name' : name of VTK file being saved

        Raises
        ------
        ValueError
            Cannot make vtk file with a grid that is not 2 dimensional.
        """
        v_mag_sqrd = 2 * energy / mass
        
        v_perp_sqrd_div_2B = 0.5 * v_mag_sqrd * ( np.sin(pitch)**2 ) * self.B_inv
        v_par_sqrd = v_mag_sqrd * ( np.cos(pitch)**2 )
        
        cyc_freq = ( charge / mass ) * self.B        
        jacob_cycFreq_inv = self.jacob_inv / cyc_freq 
        
        Vs_drift = jacob_cycFreq_inv * ( self.bu * ( v_perp_sqrd_div_2B * self.dB_dv + v_par_sqrd * self.k_v ) - self.bv * ( v_perp_sqrd_div_2B * self.dB_du + v_par_sqrd * self.k_u ) )
        Vu_drift = jacob_cycFreq_inv * ( self.bv * ( v_perp_sqrd_div_2B * self.dB_ds + v_par_sqrd * self.k_s ) - self.bs * ( v_perp_sqrd_div_2B * self.dB_dv + v_par_sqrd * self.k_v ) )
        Vv_drift = jacob_cycFreq_inv * ( self.bs * ( v_perp_sqrd_div_2B * self.dB_du + v_par_sqrd * self.k_u ) - self.bu * ( v_perp_sqrd_div_2B * self.dB_ds + v_par_sqrd * self.k_s ) )
        
        self.V_drift = np.stack( ( Vs_drift, Vu_drift, Vv_drift ), axis=self.ndim )
        
        if vtk_dict:
            if self.ndim == 2:
                cartCoord = coord_convert.cartCoord(self.wout)
                vtk_grids.vector_mesh(vtk_dict['savePath'], vtk_dict['name'], cartCoord, self.V_drift * 1e-3)
            else:
                raise ValueError('Cannont make vtk file with {} dimensional grid.'.format(self.ndim))                             
    
    
    def interpMagAxis(self):
        """ Interpolates magnetic axis using r, z grid data.
        
        Parameters
        ----------
        r : array
            3D array of r values in B_val grid
        z : array
            3D array of z values in B_val grid
            
        Raises
        ------
        ValueError
            Given the shape of B_val, the magnetic axis is undetermined.
        """
        '''
        if all(i != 1 for i in self.B_val.shape):
            for v_idx in range(self.v_num):
                Bmap = self.B_val[0::,v_idx,0::]
            
                r_grd = r[0::,v_idx,0::]
                z_grd = z[0::,v_idx,0::]
                
                z_grd_mask = z_grd[1::,0::].flatten()
                r_grd_mask = r_grd[1::,0::].flatten()
                
                Bmap_mask = Bmap[1::,0::].flatten()
                
                Binterp = griddata((r_grd_mask, z_grd_mask), Bmap_mask, (r_grd[0,0], z_grd[0,0]))
                
                self.B_val[0,v_idx,0::] = Binterp
        '''
        if (self.v_num==1) and (self.s_num!=1) and (self.u_num!=1):
            k_norm = self.kappa[0::,0::,0]
            
            r_grd = self.R_coord[0::,0::]
            z_grd = self.Z_coord[0::,0::]
            
            z_grd_mask = z_grd[1::,0::].flatten()
            r_grd_mask = r_grd[1::,0::].flatten()
            
            Kmap_mask = k_norm[1::,0::].flatten()
            
            
            Kinterp = griddata((r_grd_mask, z_grd_mask), Kmap_mask, (r_grd[0,0], z_grd[0,0]))
            
            self.kappa[0,0::,0] = Kinterp
            
        else:
            raise ValueError('Cannont interperate magnetic axis from shape {}.'.format(self.B_val.shape))
            
    
    def plot_K(self, fig, ax, vLst=[0], contMan=None):
        """ Plots poloidal cross sections of the normalized curvature.
        
        Parameters
        ----------
        fig : object
            figure object on which the colorbar is constructed
        ax : object
            axis object on which to construct color map
        vLst : list
            Toroidal angles of cross section (default is [0])
        contMan : list 
            List of contour levels to plot manually (default is None)
            
        Raises
        ------
        ValueError
            The shape of B_val does not allow for a poloidal cross section.
        """
                
        if (self.s_num!=1) and (self.v_num!=1) and (self.u_num!=1):
            k_norm = self.k_norm[3::,0::,0::]
            
            k_min = np.min(k_norm[np.nonzero(k_norm)])
            k_max = np.max(k_norm)
            if np.abs(k_min) > k_max:
                k_max = -k_min
            else:
                k_min = -k_max
                
            for v in vLst:
                v_ind = np.argmin(np.abs(self.v_dom - v))

                rMA = self.R_coord[0,v_ind,0]
                zMA = self.Z_coord[0,v_ind,0]
                
                r_show = self.R_coord[3::,v_ind,0::]
                z_show = self.Z_coord[3::,v_ind,0::]
                k_show = self.k_norm[3::,v_ind,0::]
                
                s_map = ax.pcolormesh(r_show, z_show, k_show, vmin=k_min, vmax=k_max, cmap=plt.get_cmap('seismic'))
                ax.plot(r_show[-1,0::], z_show[-1,0::], linewidth=3, c='k')
                ax.scatter([rMA], [zMA], c='k', marker='X', s=100, zorder=10)
                
                if contMan:
                    CS = ax.contour(r_show, z_show, k_show, contMan, colors='k', linewidths=1)
                    #ax.clabel(CS, inline=5, fontsize=16)

                ax.plot(r_show[-1,0::], z_show[-1,0::], c='k')
                
            cbar = fig.colorbar(s_map, ax=ax)
            cbar.ax.set_ylabel(r'$\kappa_{norm}$')
        
        elif (self.v_num==1) and (self.s_num!=1) and (self.u_num!=1):
            k_norm = self.k_norm[3::,0::]
            
            k_min = np.min(k_norm[np.nonzero(k_norm)])
            k_max = np.max(k_norm)
            if np.abs(k_min) > k_max:
                k_max = -k_min
            else:
                k_min = -k_max
                        
            rMA = self.R_coord[0,0]
            zMA = self.Z_coord[0,0]
            
            r_show = self.R_coord[3::,0::]
            z_show = self.Z_coord[3::,0::]
            
            s_map = ax.pcolormesh(r_show, z_show, k_norm, vmin=k_min, vmax=k_max, cmap=plt.get_cmap('seismic'))
            ax.plot(r_show[-1,0::], z_show[-1,0::], linewidth=3, c='k')
            ax.scatter([rMA], [zMA], c='k', marker='X', s=100, zorder=10)
            
            cbar = fig.colorbar(s_map, ax=ax)
            cbar.ax.set_ylabel(r'$\kappa_{norm}$')
            
            if contMan:
                CS = ax.contour(r_show, z_show, k_norm, contMan, colors='k', linewidths=1)
                ax.clabel(CS, inline=5, fontsize=16)
            
            ax.plot(self.R_coord[-1,0::], self.Z_coord[-1,0::], c='k')
                    
        else:
            raise ValueError('Cannont plot poloidal cross section with k_norm shape {}.'.format(self.k_norm.shape))
    
    def plot_gss(self, fig, ax, vLst=[0], contMan=None):
        """ Plots poloidal cross sections of the normalized curvature.
        
        Parameters
        ----------
        fig : object
            figure object on which the colorbar is constructed
        ax : object
            axis object on which to construct color map
        vLst : list
            Toroidal angles of cross section (default is [0])
        contMan : list 
            List of contour levels to plot manually (default is None)
            
        Raises
        ------
        ValueError
            The shape of B_val does not allow for a poloidal cross section.
        """
        import matplotlib as mpl
        
        Gss, Gsu, Gsv, Guu, Guv, Gvv, G_ss, G_su, G_sv, G_uu, G_uv, G_vv = coord_convert.compute_metric(self.wout)

        if (self.s_num!=1) and (self.v_num!=1) and (self.u_num!=1):
            gss = Gss[3::,0::,0::]
            
            gss_min = np.min(gss) 
            gss_max = np.max(gss)
            
            for v in vLst:
                v_ind = np.argmin(np.abs(self.v_dom - v))

                rMA = self.R_coord[0,v_ind,0]
                zMA = self.Z_coord[0,v_ind,0]
                
                r_show = self.R_coord[3::,v_ind,0::]
                z_show = self.Z_coord[3::,v_ind,0::]
                k_show = self.gss[3::,v_ind,0::]
                
                s_map = ax.pcolormesh(r_show, z_show, k_show, norm=mpl.colors.LogNorm(vmin=gss_min, vmax=gss_max), cmap=plt.get_cmap('jet'))
                ax.plot(r_show[-1,0::], z_show[-1,0::], linewidth=3, c='k')
                ax.scatter([rMA], [zMA], c='k', marker='X', s=100, zorder=10)
                
                if contMan:
                    CS = ax.contour(r_show, z_show, k_show, contMan, colors='k', linewidths=1)
                    #ax.clabel(CS, inline=5, fontsize=16)

                ax.plot(r_show[-1,0::], z_show[-1,0::], c='k')
                
            cbar = fig.colorbar(s_map, ax=ax)
            cbar.ax.set_ylabel(r'$g_{ss}$')
        
        elif (self.v_num==1) and (self.s_num!=1) and (self.u_num!=1):
            gss = Gss[3::,0::]
            
            gss_min = np.min(gss) 
            gss_max = np.max(gss)
                        
            rMA = self.R_coord[0,0]
            zMA = self.Z_coord[0,0]
            
            r_show = self.R_coord[3::,0::]
            z_show = self.Z_coord[3::,0::]
            
            s_map = ax.pcolormesh(r_show, z_show, gss, norm=mpl.colors.LogNorm(vmin=gss_min, vmax=gss_max), cmap=plt.get_cmap('jet'))
            ax.plot(r_show[-1,0::], z_show[-1,0::], linewidth=3, c='k')
            ax.scatter([rMA], [zMA], c='k', marker='X', s=100, zorder=10)
            
            cbar = fig.colorbar(s_map, ax=ax)
            cbar.ax.set_ylabel(r'$g_{ss}$')
            
            if contMan:
                CS = ax.contour(r_show, z_show, gss, contMan, colors='k', linewidths=1)
                ax.clabel(CS, inline=5, fontsize=16)
            
            ax.plot(self.R_coord[-1,0::], self.Z_coord[-1,0::], c='k')
                    
        else:
            raise ValueError('Cannont plot poloidal cross section with gss shape {}.'.format(Gss.shape))
            
    def paraPlot_kappa(self, fig, ax, psiN=1):
        """ Make Parametric plot of the magnetic field along a flux surface.
        
        Parameters
        ----------
        fig : object
            figure on which to place colorbar
        ax : object
            axis on which to plot parametric field
        psiN : float
            Effective radius (r/a) of flux surface to plot (default is 1)
            
        Raises
        ------
        ValueError
            The shape of B_val does not allow for a parametric plot.
        """        
        k_min = np.min(self.k_norm) 
        k_max = np.max(self.k_norm)
        
        if abs(k_min) > abs(k_max):
            k_lim = abs(k_min)
        else:
            k_lim = k_max
        
        if (self.s_num==1) and (self.v_num!=1) and (self.u_num!=1):
            k_show = self.k_norm.T
            
        elif (self.s_num!=1) and (self.v_num!=1) and (self.u_num!=1):
            s_idx = np.argmin(np.abs(self.s_dom - psiN))
            k_show = self.k_norm[s_idx, 0::, 0::].T
            
        else:
            raise ValueError('Cannont make parametric plot with B_val shape {}.'.format(self.k_norm.shape))
        
        v_dom_norm = self.v_dom/np.pi - 1
        u_dom_norm = self.u_dom/np.pi - 1
        s_map = ax.pcolormesh(v_dom_norm, u_dom_norm, k_show, vmin=-k_lim, vmax=k_lim, cmap=plt.get_cmap('seismic'))
        
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'$\kappa_n$')
        
        ax.set_xlabel(r'tor [$\pi$]')
        ax.set_ylabel(r'pol [$\pi$]')
        
        ax.set_title(r'$\Psi_N = $'+'{0:0.2f}'.format(psiN))
        
    def paraPlot_gss(self, fig, ax, psiN=1):
        """ Make Parametric plot of the magnetic field along a flux surface.
        
        Parameters
        ----------
        fig : object
            figure on which to place colorbar
        ax : object
            axis on which to plot parametric field
        psiN : float
            Effective radius (r/a) of flux surface to plot (default is 1)
            
        Raises
        ------
        ValueError
            The shape of B_val does not allow for a parametric plot.
        """        
        import matplotlib as mpl
        
        Gss, Gsu, Gsv, Guu, Guv, Gvv, G_ss, G_su, G_sv, G_uu, G_uv, G_vv = coord_convert.compute_metric(self.wout)

        gss_min = np.min(Gss) 
        gss_max = np.max(Gss)
        
        if (self.s_num==1) and (self.v_num!=1) and (self.u_num!=1):
            gss_show = Gss.T
            
        elif (self.s_num!=1) and (self.v_num!=1) and (self.u_num!=1):
            s_idx = np.argmin(np.abs(self.s_dom - psiN))
            gss_show = Gss[s_idx, 0::, 0::].T
            
        else:
            raise ValueError('Cannont make parametric plot with B_val shape {}.'.format(Gss.shape))
        
        v_dom_norm = self.v_dom/np.pi - 1
        u_dom_norm = self.u_dom/np.pi - 1
        s_map = ax.pcolormesh(v_dom_norm, u_dom_norm, gss_show, norm=mpl.colors.LogNorm(vmin=gss_min, vmax=gss_max), cmap=plt.get_cmap('jet'))
        
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'$g_{ss}$')
        
        ax.set_xlabel(r'tor [$\pi$]')
        ax.set_ylabel(r'pol [$\pi$]')
        
        ax.set_title(r'$\Psi_N = $'+'{0:0.2f}'.format(psiN))
            
 
if __name__ == '__main__':
    import vmecTools.wout_files.wout_read as wr
    import directory_dict as dd

    psiN = 0.25
    
    upts = 61
    u_dom = np.linspace(0, 2*np.pi, upts)
    v_dom = np.linspace(0, 2*np.pi, 4 * upts)
    
    wout = wr.readWout(dd.pathQHS, diffAmps=True, curvAmps=True)
    
    keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs', 'Bu', 'Bv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']    
    wout.transForm_3D(u_dom, v_dom, keys)
    curB = BcurveTools(wout)
