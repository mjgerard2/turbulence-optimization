import os, sys

import numpy as np
import scipy.interpolate as spi
from netCDF4 import Dataset
from scipy.optimize import brentq

script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_dir, '..', '..'))
import plot_define as pDef
import vmecTools.wout_files.coord_convert as cc

class readWout:
    """
    A class to read wout netCDF files from VMEC

    ...

    Attributes
    ----------
    path : str
        path to the directory in which the wout file is located
    name : str, optional
        name of the wout file being read.  Default is 'wout_HSX_main_opt0.nc'
    space_derivs : bool, optional
        compute fourier amplitudes of flux coordinate differential
        Default is False
    field_derivs : bool, optional
        compute fourier amplitudes of B field vector component differentials
        Default is False

    Methods
    -------
    transForm_3D(s_dom, u_dom, v_dom, ampKeys)
        Performs 3D Fourier transform on specified keys

    transForm_2D_sSec(s_val, u_dom, v_dom, ampKeys)
        Performs 2D Fourier transform along one flux surface on specified keys

    transForm_2D_uSec(s_dom, u_val, v_dom, ampKeys)
        Performs 2D Fourier transform along a poloidal cross section on
        specified keys

    transForm_2D_vSec(s_dom, u_dom, v_val, ampKeys)
        Performs 2D Fourier transform along a toroidal cross section on
        specified keys

    transForm_1D(s_val, u_val, v_val, ampKeys)
        Performs 1D Fourier transform at a particular flux coordinate on
        specified keys

    transForm_listPoints(points, ampKeys):
        Performs 1D Fourier transform at listed flux coordinates on
        specified keys

    calc_metric_tensor(s_dom, u_dom, v_dom):
        Calculate the components of the covariant and contravariant metric tensor.

    Raises
    ------
    IOError
        wout file specified does not exist
    """
    def __init__(self, path, space_derivs=False, field_derivs=False):
        try:
            rootgrp = Dataset(path, 'r')
        except IOError:
            print('File does not exists: '+path)
            raise

        self.space_derivs = space_derivs
        self.field_derivs = field_derivs

        self.nfp = rootgrp['/nfp'][0]
        self.mpol = rootgrp['/mpol'][0]
        self.ntor = rootgrp['/ntor'][0]

        self.ns = rootgrp['/ns'][0]
        self.s_grid = np.linspace(0, 1, self.ns)

        self.volume_grid = np.linspace(0, rootgrp['/volume_p'], self.ns)
        self.volume = spi.interp1d(self.s_grid, self.volume_grid)

        self.phi_dom = rootgrp['/phi'][:]
        self.iota = spi.interp1d(self.s_grid, rootgrp['/iotaf'][:])

        self.a_minor = rootgrp['/Aminor_p'][:]
        self.R_major = rootgrp['/Rmajor_p'][:]
        self.r_hat = self.a_minor * np.sqrt(self.s_grid)

        self.xm = rootgrp['/xm'][:]
        self.xn = rootgrp['/xn'][:]
        self.md = len(self.xm)

        self.xm_nyq = rootgrp['/xm_nyq'][:]
        self.xn_nyq = rootgrp['/xn_nyq'][:]
        self.md_nyq = len(self.xm_nyq)

        self.ds = 1. / (self.ns-1)

        self.fourierAmps = {'R': spi.interp1d(self.s_grid, rootgrp['/rmnc'][:, :], axis=0),
                            'Z': spi.interp1d(self.s_grid, rootgrp['/zmns'][:, :], axis=0),
                            'Jacobian': spi.interp1d(self.s_grid, rootgrp['/gmnc'][:, :], axis=0),
                            'Lambda': spi.interp1d(self.s_grid, rootgrp['/lmns'][:, :], axis=0),
                            'Bu_covar': spi.interp1d(self.s_grid, rootgrp['/bsubumnc'][:, :], axis=0),
                            'Bv_covar': spi.interp1d(self.s_grid, rootgrp['/bsubvmnc'][:, :], axis=0),
                            'Bs_covar': spi.interp1d(self.s_grid, rootgrp['/bsubsmns'][:, :], axis=0),
                            'Bu_contra': spi.interp1d(self.s_grid, rootgrp['/bsupumnc'][:, :], axis=0),
                            'Bv_contra': spi.interp1d(self.s_grid, rootgrp['/bsupvmnc'][:, :], axis=0),
                            'Bmod': spi.interp1d(self.s_grid, rootgrp['/bmnc'][:, :], axis=0)
                            }

        if self.md == self.md_nyq:
            self.nyq_limit = False

            self.cosine_keys = ['R', 'Jacobian', 'Bu_covar', 'Bv_covar', 'Bu_contra', 'Bv_contra', 'Bmod']
            self.sine_keys = ['Z', 'Lambda', 'Bs_covar']

        else:
            self.nyq_limit = True

            self.cosine_keys = ['R']
            self.sine_keys = ['Z', 'Lambda']

            self.cosine_nyq_keys = ['Jacobian', 'Bu_covar', 'Bv_covar', 'Bu_contra', 'Bv_contra', 'Bmod']
            self.sine_nyq_keys = ['Bs_covar']

        if space_derivs:
            self.fourierAmps['dR_ds'] = spi.interp1d(self.s_grid, np.gradient(rootgrp['/rmnc'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dR_du'] = spi.interp1d(self.s_grid, -rootgrp['/rmnc'][:, :]*self.xm, axis=0)
            self.fourierAmps['dR_dv'] = spi.interp1d(self.s_grid, rootgrp['/rmnc'][:, :]*self.xn, axis=0)

            self.fourierAmps['dZ_ds'] = spi.interp1d(self.s_grid, np.gradient(rootgrp['/zmns'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dZ_du'] = spi.interp1d(self.s_grid, rootgrp['/zmns'][:, :]*self.xm, axis=0)
            self.fourierAmps['dZ_dv'] = spi.interp1d(self.s_grid, -rootgrp['/zmns'][:, :]*self.xn, axis=0)

            self.fourierAmps['dL_ds'] = spi.interp1d(self.s_grid, np.gradient(rootgrp['/lmns'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dL_du'] = spi.interp1d(self.s_grid, rootgrp['/lmns'][:, :]*self.xm, axis=0)
            self.fourierAmps['dL_dv'] = spi.interp1d(self.s_grid, -rootgrp['/lmns'][:, :]*self.xn, axis=0)

            self.cosine_keys.extend(['dR_ds', 'dZ_du', 'dZ_dv', 'dL_du', 'dL_dv'])
            self.sine_keys.extend(['dR_du', 'dR_dv', 'dZ_ds', 'dL_ds'])

        if field_derivs:
            self.fourierAmps['dBs_du'] = spi.interp1d(self.s_grid, rootgrp['/bsubsmns'][:, :]*self.xm_nyq, axis=0)
            self.fourierAmps['dBs_dv'] = spi.interp1d(self.s_grid, -rootgrp['/bsubsmns'][:, :]*self.xn_nyq, axis=0)

            self.fourierAmps['dBu_ds'] = spi.interp1d(self.s_grid, np.gradient(rootgrp['/bsubumnc'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dBu_dv'] = spi.interp1d(self.s_grid, rootgrp['/bsubumnc'][:, :]*self.xn_nyq, axis=0)

            self.fourierAmps['dBv_ds'] = spi.interp1d(self.s_grid, np.gradient(rootgrp['/bsubvmnc'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dBv_du'] = spi.interp1d(self.s_grid, -rootgrp['/bsubvmnc'][:, :]*self.xm_nyq, axis=0)

            self.fourierAmps['dBmod_ds'] = spi.interp1d(self.s_grid, np.gradient(rootgrp['/bmnc'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dBmod_du'] = spi.interp1d(self.s_grid, -rootgrp['/bmnc'][:, :]*self.xm_nyq, axis=0)
            self.fourierAmps['dBmod_dv'] = spi.interp1d(self.s_grid, rootgrp['/bmnc'][:, :]*self.xn_nyq, axis=0)

            if self.nyq_limit:
                self.cosine_nyq_keys.extend(['dBs_du', 'dBs_dv', 'dBu_ds', 'dBv_ds', 'dBmod_ds'])
                self.sine_nyq_keys.extend(['dBu_dv', 'dBv_du', 'dBmod_du', 'dBmod_dv'])

            else:
                self.cosine_keys.extend(['dBs_du', 'dBs_dv', 'dBu_ds', 'dBv_ds', 'dBmod_ds'])
                self.sine_keys.extend(['dBu_dv', 'dBv_du', 'dBmod_du', 'dBmod_dv'])

        rootgrp.close()

    def transForm_3D(self, s_dom, u_dom, v_dom, ampKeys):
        """ Performs 3D Fourier transform on specified keys

        Parameters
        ----------
        u_dom : array
            poloidal domain on which to perform Fourier transform
        v_dom : array
            toroidal domain on which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        self.s_num = int(s_dom.shape[0])
        self.u_num = int(u_dom.shape[0])
        self.v_num = int(v_dom.shape[0])

        self.s_dom = s_dom
        self.u_dom = u_dom
        self.v_dom = v_dom

        pol, tor = np.meshgrid(self.u_dom, self.v_dom)

        pol_xm = np.dot(self.xm.reshape(self.md, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn = np.dot(self.xn.reshape(self.md, 1), tor.reshape(1, self.v_num * self.u_num))

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        if self.nyq_limit:
            for key in ampKeys:
                if any(ikey == key for ikey in self.cosine_nyq_keys) or any(ikey == key for ikey in self.sine_nyq_keys):
                    pol_nyq_xm = np.dot(self.xm_nyq.reshape(self.md_nyq, 1), pol.reshape(1, self.v_num * self.u_num))
                    tor_nyq_xn = np.dot(self.xn_nyq.reshape(self.md_nyq, 1), tor.reshape(1, self.v_num * self.u_num))

                    cos_nyq_mu_nv = np.cos(pol_nyq_xm - tor_nyq_xn)
                    sin_nyq_mu_nv = np.sin(pol_nyq_xm - tor_nyq_xn)

        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_dom)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.s_num, self.v_num, self.u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.s_num, self.v_num, self.u_num)

            elif self.nyq_limit and any(ikey == key for ikey in self.cosine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_nyq_mu_nv).reshape(self.s_num, self.v_num, self.u_num)

            elif self.nyq_limit and any(ikey == key for ikey in self.sine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_nyq_mu_nv).reshape(self.s_num, self.v_num, self.u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_2D_sSec(self, s_val, u_dom, v_dom, ampKeys, errKeys=False):
        """ Performs 2D Fourier transform along one flux surface on specified keys

        Parameters
        ----------
        s_val : float
            effective radius of flux surface on which Fourier tronsfrom will
            be performed
        u_dom : array
            poloidal domain on which to perform Fourier transform
        v_dom : array
            toroidal domain on which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed
        errKeys : bool, optional
            flag to include flux coordinate error propogation. Default is False

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        self.s_num = 1
        self.u_num = int(u_dom.shape[0])
        self.v_num = int(v_dom.shape[0])

        self.s_dom = np.array([s_val])
        self.u_dom = u_dom
        self.v_dom = v_dom

        pol, tor = np.meshgrid(self.u_dom, self.v_dom)
        pol_xm = np.dot(self.xm.reshape(self.md, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn = np.dot(self.xn.reshape(self.md, 1), tor.reshape(1, self.v_num * self.u_num))

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        if self.nyq_limit:
            for key in ampKeys:
                if any(ikey == key for ikey in self.cosine_nyq_keys) or any(ikey == key for ikey in self.sine_nyq_keys):
                    pol_nyq_xm = np.dot(self.xm_nyq.reshape(self.md_nyq, 1), pol.reshape(1, self.v_num * self.u_num))
                    tor_nyq_xn = np.dot(self.xn_nyq.reshape(self.md_nyq, 1), tor.reshape(1, self.v_num * self.u_num))

                    cos_nyq_mu_nv = np.cos(pol_nyq_xm - tor_nyq_xn)
                    sin_nyq_mu_nv = np.sin(pol_nyq_xm - tor_nyq_xn)

        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.v_num, self.u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.v_num, self.u_num)

            elif self.nyq_limit and any(ikey == key for ikey in self.cosine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_nyq_mu_nv).reshape(self.v_num, self.u_num)

            elif self.nyq_limit and any(ikey == key for ikey in self.sine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_nyq_mu_nv).reshape(self.v_num, self.u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_2D_uSec(self, s_dom, u_val, v_dom, ampKeys):
        """ Performs 2D Fourier transform along a poloidal cross section on
        specified keys

        Parameters
        ----------
        s_dom : array
            radial domain on which to perform Fourier transform
        u_val : float
            poloidal coordinate at which to perform Fourier transform
        v_dom : array
            toroidal domain on which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        self.s_num = int(s_dom.shape[0])
        self.u_num = 1
        self.v_num = int(v_dom.shape[0])

        self.s_dom = s_dom
        self.u_dom = np.array([u_val])
        self.v_dom = v_dom

        pol_xm = self.xm.reshape(self.md, 1) * u_val
        tor_xn = np.dot(self.xn.reshape(self.md, 1), self.v_dom.reshape(1, self.v_num))

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        if self.nyq_limit:
            for key in ampKeys:
                if any(ikey == key for ikey in self.cosine_nyq_keys) or any(ikey == key for ikey in self.sine_nyq_keys):
                    pol_nyq_xm = self.xm_nyq.reshape(self.md_nyq, 1) * u_val
                    tor_nyq_xn = np.dot(self.xn_nyq.reshape(self.md_nyq, 1), self.v_dom.reshape(1, self.v_num))

                    cos_nyq_mu_nv = np.cos(pol_nyq_xm - tor_nyq_xn)
                    sin_nyq_mu_nv = np.sin(pol_nyq_xm - tor_nyq_xn)

        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_dom)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.s_num, self.v_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.s_num, self.v_num)

            elif self.nyq_limit and any(ikey == key for ikey in self.cosine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_nyq_mu_nv).reshape(self.s_num, self.v_num)

            elif self.nyq_limit and any(ikey == key for ikey in self.sine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_nyq_mu_nv).reshape(self.s_num, self.v_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_2D_vSec(self, s_dom, u_dom, v_val, ampKeys):
        """ Performs 2D Fourier transform along a toroidal cross section on
        specified keys

        Parameters
        ----------
        s_dom : float
            radial domain on which to perform Fourier transform
        u_dom : array
            poloidal domain on which to perform Fourier transform
        v_val : float
            toroidal coordinate at which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        self.s_num = int(s_dom.shape[0])
        self.u_num = int(u_dom.shape[0])
        self.v_num = 1

        self.s_dom = s_dom
        self.u_dom = u_dom
        self.v_dom = np.array([v_val])

        pol_xm = np.dot(self.xm.reshape(self.md, 1), self.u_dom.reshape(1, self.u_num))
        tor_xn = self.xn.reshape(self.md, 1) * v_val

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        if self.nyq_limit:
            for key in ampKeys:
                if any(ikey == key for ikey in self.cosine_nyq_keys) or any(ikey == key for ikey in self.sine_nyq_keys):
                    pol_nyq_xm = np.dot(self.xm_nyq.reshape(self.md_nyq, 1), self.u_dom.reshape(1, self.u_num))
                    tor_nyq_xn = self.xn_nyq.reshape(self.md_nyq, 1) * v_val

                    cos_nyq_mu_nv = np.cos(pol_nyq_xm - tor_nyq_xn)
                    sin_nyq_mu_nv = np.sin(pol_nyq_xm - tor_nyq_xn)

        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_dom)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.s_num, self.u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.s_num, self.u_num)

            elif self.nyq_limit and any(ikey == key for ikey in self.cosine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_nyq_mu_nv).reshape(self.s_num, self.u_num)

            elif self.nyq_limit and any(ikey == key for ikey in self.sine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_nyq_mu_nv).reshape(self.s_num, self.u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_1D(self, s_val, u_val, v_val, ampKeys):
        """ Performs 1D Fourier transform at specified flux coordinates on
        specified keys

        Parameters
        ----------
        s_val : float
            effective radius at which to perform Fourier tronsfrom
        u_val : float
            poloidal coordinate at which to perform Fourier transform
        v_val : float
            toroidal coordinate at which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        self.s_dom = np.array([s_val])
        self.u_dom = np.array([u_val])
        self.v_dom = np.array([v_val])

        pol_xm = self.xm.reshape(self.md, 1) * u_val
        tor_xn = self.xn.reshape(self.md, 1) * v_val

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        if self.nyq_limit:
            for key in ampKeys:
                if any(ikey == key for ikey in self.cosine_nyq_keys) or any(ikey == key for ikey in self.sine_nyq_keys):
                    pol_nyq_xm = self.xm_nyq.reshape(self.md_nyq, 1) * u_val
                    tor_nyq_xn = self.xn_nyq.reshape(self.md_nyq, 1) * v_val

                    cos_nyq_mu_nv = np.cos(pol_nyq_xm - tor_nyq_xn)
                    sin_nyq_mu_nv = np.sin(pol_nyq_xm - tor_nyq_xn)

        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv)[0]

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv)[0]

            elif self.nyq_limit and any(ikey == key for ikey in self.cosine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_nyq_mu_nv)[0]

            elif self.nyq_limit and any(ikey == key for ikey in self.sine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_nyq_mu_nv)[0]

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_listPoints(self, points, ampKeys):
        """ Performs 1D Fourier transform at listed flux coordinates on
        specified keys

        Parameters
        ----------
        points : array
            List of flux coordinates to perform 1D transform on, ordered as
            {s,u,v}
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        npts = points.shape[0]
        self.s_num = npts
        self.u_num = npts
        self.v_num = npts

        self.s_dom = points[:,0]
        self.u_dom = points[:,1]
        self.v_dom = points[:,2]

        pol_xm = np.tile(self.xm.reshape(self.md, 1), npts).reshape(self.md, npts) * np.tile(self.u_dom, self.md).reshape(self.md, npts)
        tor_xn = np.tile(self.xn.reshape(self.md, 1), npts).reshape(self.md, npts) * np.tile(self.v_dom, self.md).reshape(self.md, npts)

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        if self.nyq_limit:
            for key in ampKeys:
                if any(ikey == key for ikey in self.cosine_nyq_keys) or any(ikey == key for ikey in self.sine_nyq_keys):
                    pol_nyq_xm = np.tile(self.xm_nyq.reshape(self.md_nyq, 1), npts).reshape(self.md_nyq, npts) * np.tile(self.u_dom, self.md_nyq).reshape(self.md_nyq, npts)
                    tor_nyq_xn = np.tile(self.xn_nyq.reshape(self.md_nyq, 1), npts).reshape(self.md_nyq, npts) * np.tile(self.v_dom, self.md_nyq).reshape(self.md_nyq, npts)

                    cos_nyq_mu_nv = np.cos(pol_nyq_xm - tor_nyq_xn)
                    sin_nyq_mu_nv = np.sin(pol_nyq_xm - tor_nyq_xn)

        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](self.s_dom)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.sum(fourAmps*cos_mu_nv.T, axis=1)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.sum(fourAmps*sin_mu_nv.T, axis=1)

            elif self.nyq_limit and any(ikey == key for ikey in self.cosine_nyq_keys):
                self.invFourAmps[key] = np.sum(fourAmps*cos_nyq_mu_nv.T, axis=1)

            elif self.nyq_limit and any(ikey == key for ikey in self.sine_nyq_keys):
                self.invFourAmps[key] = np.sum(fourAmps*sin_nyq_mu_nv.T, axis=1)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_listPoints_sSec(self, s_val, points, ampKeys):
        """ Performs 2D Fourier transform at listed flux coordinates on
        specified keys. The function can be used when the s coordinate is constant.

        Parameters
        ----------
        s_val : float
            VMEC s coordinate for all points.
        points : array
            List of flux coordinates to perform 1D transform on, ordered as {u,v}.
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        npts = points.shape[0]
        self.s_num = 1
        self.u_num = npts
        self.v_num = npts

        self.u_dom = points[:,0]
        self.v_dom = points[:,1]

        pol_xm = np.tile(self.xm.reshape(self.md, 1), npts).reshape(self.md, npts) * np.tile(points[:,0], self.md).reshape(self.md, npts)
        tor_xn = np.tile(self.xn.reshape(self.md, 1), npts).reshape(self.md, npts) * np.tile(points[:,1], self.md).reshape(self.md, npts)

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        if self.nyq_limit:
            for key in ampKeys:
                if any(ikey == key for ikey in self.cosine_nyq_keys) or any(ikey == key for ikey in self.sine_nyq_keys):
                    pol_nyq_xm = np.tile(self.xm_nyq.reshape(self.md_nyq, 1), npts).reshape(self.md_nyq, npts) * np.tile(points[:,0], self.md_nyq).reshape(self.md_nyq, npts)
                    tor_nyq_xn = np.tile(self.xn_nyq.reshape(self.md_nyq, 1), npts).reshape(self.md_nyq, npts) * np.tile(points[:,1], self.md_nyq).reshape(self.md_nyq, npts)

                    cos_nyq_mu_nv = np.cos(pol_nyq_xm - tor_nyq_xn)
                    sin_nyq_mu_nv = np.sin(pol_nyq_xm - tor_nyq_xn)

        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv)

            elif self.nyq_limit and any(ikey == key for ikey in self.cosine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_nyq_mu_nv)

            elif self.nyq_limit and any(ikey == key for ikey in self.sine_nyq_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_nyq_mu_nv)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_straight_coords(self, s_dom, u_dom, v_dom, ampKeys):
        """ Compute VMEC variables in straight-field-line coordinates. This is done
        by first computing Lambda on the specified grid, then generating a 2D spline
        fit of the VMEC poloidal angle as a function of the straight-field-line
        coordinates. Then, the specified variable are computed on the provided grid 
        assuming the poloidal angles provided are the straight-field-line angle, and
        are therefore used in the spline fit of the VMEC poloidal angle.

        Parameters
        ----------
        s_dom : arr
            Array of radial coordinates.
        u_dom : arr
            Array of poloidal angles.
        v_dom : arr
            Array of toroidal angles.
        ampkeys : list
            A list of the VMEC varaibles to be computed.
        """
        # define global variables #
        self.s_dom = s_dom
        self.u_dom = u_dom
        self.v_dom = v_dom

        # generate mesh grid #
        s_mesh, v_mesh, u_mesh = np.meshgrid(s_dom, v_dom, u_dom, indexing='ij')
        if s_dom.shape[0] == 1:
            s_mesh = s_mesh[0]
            u_mesh = u_mesh[0]
            v_mesh = v_mesh[0]

        # compute Lambda #
        if s_dom.shape[0] == 1:
            wout.transForm_2D_sSec(s_dom[0], u_dom, v_dom, ['Lambda'])
        else:
            wout.transForm_3D(s_dom, u_dom, v_dom, ['Lambda'])
        lam = wout.invFourAmps['Lambda']

        # generate poloidal spline fit #
        u_star = u_mesh + lam
        u_star_flat = u_star.flatten()
        s_flat = s_mesh.flatten()
        u_flat = u_mesh.flatten()
        v_flat = v_mesh.flatten()
        if s_dom.shape[0] == 1:
            u_model = spi.LinearNDInterpolator(list(zip(u_star_flat, v_flat)), u_flat)
            u_interp = u_model(u_mesh, v_mesh)
        else:
            u_model = spi.LinearNDInterpolator(list(zip(s_flat, u_star_flat, v_flat)), u_flat)
            u_interp = u_model(s_mesh, u_mesh, v_mesh)

        # compute variables in straight-field-line coordinates #
        points = np.stack((s_flat, u_interp.flatten(), v_flat), axis=1)
        self.transForm_listPoints(points, ampKeys)
        for key, arr in wout.invFourAmps.items():
            self.invFourAmps[key] = arr.reshape(s_mesh.shape)

    def invert_symmetry_coordinates_new(self, eta_dom, xi_dom, mode=[1., 4.]):
        if not hasattr(self, u_model) and not hasattr(self, v_model):
            raise NameError('eta/xi interpolator has not been generated.')

        # define mode numbers #
        M, N = mode

        # define global variables #
        self.s_num = s_dom.shape[0]
        self.u_num = u_dom.shape[0]
        self.v_num = v_dom.shape[0]
        self.s_dom = s_dom
        self.u_dom = u_dom
        self.v_dom = v_dom

        # compute Lambda #
        s_mesh, v_mesh, u_mesh = np.meshgrid(s_dom, v_dom, u_dom, indexing='ij')
        self.transForm_3D(s_dom, u_dom, v_dom, ['Lambda'])
        if self.s_num == 1 and self.u_num == 1:
            lam = self.invFourAmps['Lambda'][0,:,0]
            s_mesh = s_mesh[0,:,0]
            u_mesh = u_mesh[0,:,0]
            v_mesh = v_mesh[0,:,0]
        elif self.s_num == 1:
            lam = self.invFourAmps['Lambda'][0]
            s_mesh = s_mesh[0]
            u_mesh = u_mesh[0]
            v_mesh = v_mesh[0]

        # tile VMEC angles #
        if tile[0] > 0:
            for i in range(int(tile[0])):
                u_mesh = np.append(u_mesh, u_mesh[1:self.v_num], axis=0)
                u_mesh = np.insert(u_mesh, 0, u_mesh[0:self.v_num-1], axis=0)
                v_mesh = np.append(v_mesh, v_mesh[1:self.v_num]+2*np.pi*(1+2*i), axis=0)
                v_mesh = np.insert(v_mesh, 0, v_mesh[0:self.v_num-1]-2*np.pi, axis=0)
                lam = np.append(lam, lam[1:self.v_num], axis=0)
                lam = np.insert(lam, 0, lam[0:self.v_num-1], axis=0)
        if tile[1] > 0:
            for i in range(int(tile[1])):
                u_mesh = np.append(u_mesh, u_mesh[:,1:self.u_num]+2*np.pi*(1+2*i), axis=1)
                u_mesh = np.insert(u_mesh, 0, u_mesh[:,0:self.u_num-1].T-2*np.pi, axis=1)
                v_mesh = np.append(v_mesh, v_mesh[:,1:self.u_num], axis=1)
                v_mesh = np.insert(v_mesh, 0, v_mesh[:,0:self.u_num-1].T, axis=1)
                lam = np.append(lam, lam[:,1:self.u_num], axis=1)
                lam = np.insert(lam, 0, lam[:,0:self.u_num-1].T, axis=1)

        # import lambda map and compute symmetry angles #
        psi = s_mesh
        eta = M*u_mesh+lam-N*v_mesh
        xi = (1./M)*(u_mesh+lam)+(1./N)*v_mesh

        if self.s_num == 1 and self.u_num == 1:
            data = np.array(sorted(np.stack((xi, v_dom), axis=1), key=lambda x: x[0]))
            v_fit = spi.CubicSpline(data[:,0], data[:,1])
            return v_fit
        elif self.s_num == 1:    
            u_fit = spi.LinearNDInterpolator(list(zip(eta.flatten(), xi.flatten())), u_mesh.flatten())
            v_fit = spi.LinearNDInterpolator(list(zip(eta.flatten(), xi.flatten())), v_mesh.flatten())
            return u_fit, v_fit
        else:
            u_fit = spi.LinearNDInterpolator(list(zip(psi.flatten(), eta.flatten(), xi.flatten())), u_mesh.flatten())
            v_fit = spi.LinearNDInterpolator(list(zip(psi.flatten(), eta.flatten(), xi.flatten())), v_mesh.flatten())
            return u_fit, v_fit

    def generate_symmetry_coordinate_maps(self, s_dom, u_dom, v_dom, mode=[1., 4.], tile=[0,0]):
        # define mode numbers #
        M, N = mode

        # define global variables #
        self.s_num = s_dom.shape[0]
        self.u_num = u_dom.shape[0]
        self.v_num = v_dom.shape[0]
        self.s_dom = s_dom
        self.u_dom = u_dom
        self.v_dom = v_dom

        # compute Lambda #
        s_mesh, v_mesh, u_mesh = np.meshgrid(s_dom, v_dom, u_dom, indexing='ij')
        self.transForm_3D(s_dom, u_dom, v_dom, ['Lambda'])
        if self.s_num == 1:
            lam = self.invFourAmps['Lambda'][0]
            s_mesh = s_mesh[0]
            u_mesh = u_mesh[0]
            v_mesh = v_mesh[0]

        # tile VMEC angles #
        if tile[0] > 0:
            for i in range(int(tile[0])):
                u_mesh = np.append(u_mesh, u_mesh[1:self.v_num], axis=0)
                u_mesh = np.insert(u_mesh, 0, u_mesh[0:self.v_num-1], axis=0)
                v_mesh = np.append(v_mesh, v_mesh[1:self.v_num]+2*np.pi*(1+2*i), axis=0)
                v_mesh = np.insert(v_mesh, 0, v_mesh[0:self.v_num-1]-2*np.pi, axis=0)
                lam = np.append(lam, lam[1:self.v_num], axis=0)
                lam = np.insert(lam, 0, lam[0:self.v_num-1], axis=0)
        if tile[1] > 0:
            for i in range(int(tile[1])):
                u_mesh = np.append(u_mesh, u_mesh[:,1:self.u_num]+2*np.pi*(1+2*i), axis=1)
                u_mesh = np.insert(u_mesh, 0, u_mesh[:,0:self.u_num-1].T-2*np.pi, axis=1)
                v_mesh = np.append(v_mesh, v_mesh[:,1:self.u_num], axis=1)
                v_mesh = np.insert(v_mesh, 0, v_mesh[:,0:self.u_num-1].T, axis=1)
                lam = np.append(lam, lam[:,1:self.u_num], axis=1)
                lam = np.insert(lam, 0, lam[:,0:self.u_num-1].T, axis=1)

        # import lambda map and compute symmetry angles #
        psi = s_mesh
        eta = M*u_mesh+lam-N*v_mesh
        xi = (1./M)*(u_mesh+lam)+(1./N)*v_mesh

        if self.s_num == 1:    
            self.sacs_model_ndim = 2
            self.sacs_xi_bounds = [np.min(xi), np.max(xi)]
            self.sacs_eta_bounds = [np.min(eta), np.max(eta)]
            self.poloidal_sacs_model = spi.LinearNDInterpolator(list(zip(eta.flatten(), xi.flatten())), u_mesh.flatten())
            self.toroidal_sacs_model = spi.LinearNDInterpolator(list(zip(eta.flatten(), xi.flatten())), v_mesh.flatten())
        else:
            self.sacs_model_ndim = 3
            self.poloidal_sacs_model = spi.LinearNDInterpolator(list(zip(psi.flatten(), eta.flatten(), xi.flatten())), u_mesh.flatten())
            self.toroidal_sacs_model = spi.LinearNDInterpolator(list(zip(psi.flatten(), eta.flatten(), xi.flatten())), v_mesh.flatten())

    def compute_axial_planes(self, s_val, u_dom, v_dom):
        """ Find the intersection of a flux surface with the set of planes lying perpendicular to the magnetic axis.

        Parameters
        ----------
        s_val : float
            VMEC psi coordinate of flux surface being investigated
        u_dom : arr
            VMEC poloidal angles over which to calculate intersections
        v_dom : arr
            VMEC toroidal angles over which to calculate intersections

        Returns
        -------
        arr : Cartesian coordinates for the magnetic axis.
        arr : Cartesian coordinates for the tangent basis vector along axis.
        arr : Cartesian coordinates for the flux-surface intersections.
        arr : VMEC coordinates for the flux-surface intersections.
        """
        # global variables #
        v_num = v_dom.shape[0]
        u_num = u_dom.shape[0]

        # compute magnetic axis #
        s_ma = np.array([0])
        v_ma = v_dom
        self.transForm_2D_uSec(s_ma, 0, v_ma, ['R', 'Z', 'dR_dv', 'dZ_dv'])

        # import variables along axis #
        dRdv_ma = self.invFourAmps['dR_dv'][0]
        dZdv_ma = self.invFourAmps['dZ_dv'][0]
        R_ma = self.invFourAmps['R'][0]
        X_ma_x = R_ma*np.cos(v_ma)
        X_ma_y = R_ma*np.sin(v_ma)
        X_ma_z = self.invFourAmps['Z'][0]
        X_ma = np.stack((X_ma_x, X_ma_y, X_ma_z), axis=1)
           
        # compute axial basis vector
        g_vv_ma = (dRdv_ma**2) + (R_ma**2) + (dZdv_ma**2)
        norm = 1./np.sqrt(g_vv_ma)
        b_ma_x = (dRdv_ma*np.cos(v_ma)-R_ma*np.sin(v_ma))*norm
        b_ma_y = (dRdv_ma*np.sin(v_ma)+R_ma*np.cos(v_ma))*norm
        b_ma_z = dZdv_ma*norm
        b_ma = np.stack((b_ma_x, b_ma_y, b_ma_z), axis=1)

        # compute perpendicular basis vectors #
        e_r = np.stack((np.cos(v_ma), np.sin(v_ma), np.zeros(v_num)), axis=1)
        r_dot_b = np.sum(e_r*b_ma, axis=1)
        r_ma = (e_r - np.repeat(r_dot_b, 3).reshape(v_num, 3)*b_ma)
        r_ma = r_ma / np.repeat(np.sqrt(1 - r_dot_b**2), 3).reshape(v_num, 3)
        z_ma = np.cross(r_ma, b_ma)

        # compute magnetic surface #
        self.transForm_2D_sSec(s_val, u_dom, v_dom, ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv'])
        v_fs, u_fs = np.meshgrid(v_dom, u_dom, indexing='ij')
        X_fs_x = self.invFourAmps['R']*np.cos(v_fs)
        X_fs_y = self.invFourAmps['R']*np.sin(v_fs)
        X_fs_z = self.invFourAmps['Z']
        X_fs = np.stack((X_fs_x, X_fs_y, X_fs_z), axis=2)

        # identify buffer domain #
        Dphi = v_dom[-1]-v_dom[0]
        phi_shft = np.pi/self.nfp
        low_idx = int(np.ceil(phi_shft/(v_dom[1]-v_dom[0])))+1
        hgh_idx = v_num-low_idx
        ilft = low_idx-1
        irgt = v_num+low_idx-1

        # buffer toroidal domain #
        v_dom_lft = v_dom[1:low_idx]
        v_dom_rgt = v_dom[hgh_idx:-1]
        v_dom_buff = np.insert(v_dom, 0, v_dom_rgt-Dphi)
        v_dom_buff = np.append(v_dom_buff, v_dom_lft+Dphi)
        vfs_buff, ufs_buff = np.meshgrid(v_dom_buff, u_dom, indexing='ij')

        # buffer magnetic axis #
        X_ma_lft = np.copy(X_ma[1:low_idx])
        X_ma_rgt = np.copy(X_ma[hgh_idx:-1])
        X_ma_buff = np.insert(X_ma, 0, X_ma_rgt, axis=0)
        X_ma_buff = np.append(X_ma_buff, X_ma_lft, axis=0)

        # buffer flux surface #
        X_fs_lft = np.copy(X_fs[1:low_idx])
        X_fs_rgt = np.copy(X_fs[hgh_idx:-1])
        X_fs_buff = np.insert(X_fs, 0, X_fs_rgt, axis=0)
        X_fs_buff = np.append(X_fs_buff, X_fs_lft, axis=0)

        # buffer magnetic field vector on axis #
        b_ma_lft = np.copy(b_ma[1:low_idx])
        b_ma_rgt = np.copy(b_ma[hgh_idx:-1])
        b_ma_buff = np.insert(b_ma, 0, b_ma_rgt, axis=0)
        b_ma_buff = np.append(b_ma_buff, b_ma_lft, axis=0)

        # buffer vmec arrays #
        vmec = {}
        for key, arr in self.invFourAmps.items():
            arr_lft = np.copy(arr[1:low_idx])
            arr_rgt = np.copy(arr[hgh_idx:-1])
            arr_buff = np.insert(arr, 0, arr_rgt, axis=0)
            arr_buff = np.append(arr_buff, arr_lft, axis=0)
            vmec[key] = arr_buff

        # loop over toroidal planes #
        X_fs_cart = np.empty((v_num, u_num, 3))
        X_fs_vmec = np.zeros((v_num, u_num, 3))
        for idx in range(v_num):
            # define indexing varaibles #
            i = ilft+idx
            ibeg = np.argmin(np.abs(v_dom_buff-(v_dom_buff[i]-phi_shft)))
            iend = np.argmin(np.abs(v_dom_buff-(v_dom_buff[i]+phi_shft)))+1
            inum = iend-ibeg

            # translate reference frame to magnetic axis #
            X = X_fs_buff[ibeg:iend] - X_ma_buff[i]
            b_vec = np.tile(b_ma_buff[i], inum*u_num).reshape(inum, u_num, 3)
            X_dot_b = np.sum(X*b_vec, axis=2)

            # interpolate flux-surface intersection with axial plane #
            b_sign = np.sign(X_dot_b)
            b_sign[np.where(b_sign == 0)] = 1
            b_sign_chng = ((np.roll(b_sign, 1, axis=0)-b_sign) != 0).astype(int)
            ipos = np.flatnonzero(b_sign_chng[1:])
            ineg = ipos - np.full(ipos.shape, u_num, dtype=int)
            Xn1 = X[1:].reshape((inum-1)*u_num, 3)[ineg]
            Xp1 = X[1:].reshape((inum-1)*u_num, 3)[ipos]
            X_delta = Xp1 - Xn1
            X_delta = X_delta / np.repeat(np.linalg.norm(X_delta, axis=1), 3).reshape(u_num, 3)
            Delta = -np.sum(Xn1*b_vec[0], axis=1)/np.sum(X_delta*b_vec[0], axis=1)
            X0_cart = Xn1+np.repeat(Delta, 3).reshape(u_num, 3)*X_delta
            
            # approximate vmec \theta with left grid points #
            u_neg = ufs_buff[ibeg:iend][1:].reshape((inum-1)*u_num)[ineg]
            v_neg = vfs_buff[ibeg:iend][1:].reshape((inum-1)*u_num)[ineg]
            e_r = np.stack((np.cos(v_neg), np.sin(v_neg), np.zeros(v_neg.shape)), axis=1)
            e_p = np.stack((-np.sin(v_neg), np.cos(v_neg), np.zeros(v_neg.shape)), axis=1)
            e_z = np.stack((np.zeros(v_neg.shape), np.zeros(v_neg.shape), np.ones(v_neg.shape)), axis=1)
            local = {}
            for key, arr in vmec.items():
                arr_local = arr[ibeg:iend][1:].reshape((inum-1)*u_num)[ineg]
                local[key] = np.repeat(arr_local, 3).reshape(u_num, 3)
            eu = (local['dR_ds']*local['dZ_dv']-local['dR_dv']*local['dZ_ds'])*e_p + (local['R']/local['Jacobian'])*(local['dZ_ds']*e_r - local['dR_ds']*e_z)
            ev = (1./local['R'])*e_p
            un0 = u_neg + np.sum(eu*(X0_cart-Xn1), axis=1)

            # approximate vmec \theta with right grid points #
            u_pos = ufs_buff[ibeg:iend][1:].reshape((inum-1)*u_num)[ipos]
            v_pos = vfs_buff[ibeg:iend][1:].reshape((inum-1)*u_num)[ipos]
            e_r = np.stack((np.cos(v_pos), np.sin(v_pos), np.zeros(v_pos.shape)), axis=1)
            e_p = np.stack((-np.sin(v_pos), np.cos(v_pos), np.zeros(v_pos.shape)), axis=1)
            e_z = np.stack((np.zeros(v_pos.shape), np.zeros(v_pos.shape), np.ones(v_pos.shape)), axis=1)
            local = {}
            for key, arr in vmec.items():
                arr_local = arr[ibeg:iend][1:].reshape((inum-1)*u_num)[ipos]
                local[key] = np.repeat(arr_local, 3).reshape(u_num, 3)
            eu = (local['dR_ds']*local['dZ_dv']-local['dR_dv']*local['dZ_ds'])*e_p + (local['R']/local['Jacobian'])*(local['dZ_ds']*e_r - local['dR_ds']*e_z)
            ev = (1./local['R'])*e_p
            up0 = u_pos + np.sum(eu*(X0_cart-Xp1), axis=1)

            # caclulate vmec coordinates for flux-surface intersection #
            dist_n1 = np.linalg.norm(X0_cart-Xn1, axis=1)
            dist_p1 = np.linalg.norm(X0_cart-Xp1, axis=1)
            dist_tot = np.linalg.norm(Xp1-Xn1, axis=1)
            u0 = (dist_p1*un0 + dist_n1*up0)/dist_tot
            s0 = np.full(u_num, s_val)
            v0 = np.arctan2(X0_cart[:,1]+X_ma_buff[i,1], X0_cart[:,0]+X_ma_buff[i,0])
            X_fs_vmec[idx] = np.array(sorted(np.stack((s0, u0, v0), axis=1), key=lambda x: x[1]))

            # sort cartesian points of intersection #
            X_dot_r = np.sum(X0_cart*r_ma[idx], axis=1)
            X_dot_z = np.sum(X0_cart*z_ma[idx], axis=1)
            X_phi = np.arctan2(X_dot_z, X_dot_r)
            X0_cart = np.array(sorted(np.insert(X0_cart, 0, X_phi, axis=1), key=lambda x: x[0]))[:,1:]
            X_fs_cart[idx] = X0_cart
            
        return X_ma, b_ma, X_fs_cart, X_fs_vmec

    def compute_axial_intersection(self, s_val, u_dom, v_dom, X_ma, b_ma):
        """ Compute flux-surface intersection with axial planes, with the axis location and tangent vectors provided.
        
        Parameters
        ----------
        s_val : float
            Toroidal-flux coordinate of flux surface.
        u_dom : 1D arr 
            Poloidal domain over which to calculate intersections.
        v_dom : 1D arr
            Toroidal domain of candidate cross-sections.
        X_ma : 1D arr
            Location of magnetic axis in Cartesian coordinates.
        b_ma : 1D arr
            Tangent vector in magnetic axis in Cartesian coordinates.

        Returns
        -------
        1D arr : Cartesian coordinates of flux-surface intersection with axial plane.
        1D arr : VMEC coordinates of flux-surface intersection with axial plane.
        """
        # define array size variables #
        v_num = v_dom.shape[0]
        u_num = u_dom.shape[0]
        uv_num = u_num*v_num

        # compute VMEC quantities #
        v_mesh, u_mesh = np.meshgrid(v_dom, u_dom, indexing='ij')
        self.transForm_2D_sSec(s_val, u_dom, v_dom, ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv'])
        X_surf_x = self.invFourAmps['R']*np.cos(v_mesh)
        X_surf_y = self.invFourAmps['R']*np.sin(v_mesh)
        X_surf_z = self.invFourAmps['Z']
        X_surf = np.stack((X_surf_x, X_surf_y, X_surf_z), axis=2)

        # translate reference frame to magnetic axis #
        X = X_surf - X_ma
        b_vec = np.tile(b_ma, uv_num).reshape(v_num, u_num, 3)
        X_dot_b = np.sum(X*b_vec, axis=2)

        # interpolate flux-surface intersection with axial plane #
        b_sign = np.sign(X_dot_b)
        b_sign[np.where(b_sign == 0)] = 1
        b_sign_chng = ((np.roll(b_sign, 1, axis=0)-b_sign) != 0).astype(int)
        ipos = np.flatnonzero(b_sign_chng[1:])
        ineg = ipos - np.full(ipos.shape, u_num, dtype=int)
        Xn1 = X[1:].reshape((v_num-1)*u_num, 3)[ineg]
        Xp1 = X[1:].reshape((v_num-1)*u_num, 3)[ipos]
        X_delta = Xp1 - Xn1
        X_delta = X_delta / np.repeat(np.linalg.norm(X_delta, axis=1), 3).reshape(u_num, 3)
        Delta = -np.sum(Xn1*b_vec[0], axis=1)/np.sum(X_delta*b_vec[0], axis=1)
        X0_cart = Xn1+np.repeat(Delta, 3).reshape(u_num, 3)*X_delta
        
        # approximate vmec \theta with left grid points #
        u_neg = u_mesh[1:].reshape((v_num-1)*u_num)[ineg]
        v_neg = v_mesh[1:].reshape((v_num-1)*u_num)[ineg]
        e_r = np.stack((np.cos(v_neg), np.sin(v_neg), np.zeros(v_neg.shape)), axis=1)
        e_p = np.stack((-np.sin(v_neg), np.cos(v_neg), np.zeros(v_neg.shape)), axis=1)
        e_z = np.stack((np.zeros(v_neg.shape), np.zeros(v_neg.shape), np.ones(v_neg.shape)), axis=1)
        local = {}
        for key, arr in self.invFourAmps.items():
            arr_local = arr[1:].reshape((v_num-1)*u_num)[ineg]
            local[key] = np.repeat(arr_local, 3).reshape(u_num, 3)
        eu = (local['dR_ds']*local['dZ_dv']-local['dR_dv']*local['dZ_ds'])*e_p + (local['R']/local['Jacobian'])*(local['dZ_ds']*e_r - local['dR_ds']*e_z)
        ev = (1./local['R'])*e_p
        un0 = u_neg + np.sum(eu*(X0_cart-Xn1), axis=1)

        # approximate vmec \theta with right grid points #
        u_pos = u_mesh[1:].reshape((v_num-1)*u_num)[ipos]
        v_pos = v_mesh[1:].reshape((v_num-1)*u_num)[ipos]
        e_r = np.stack((np.cos(v_pos), np.sin(v_pos), np.zeros(v_pos.shape)), axis=1)
        e_p = np.stack((-np.sin(v_pos), np.cos(v_pos), np.zeros(v_pos.shape)), axis=1)
        e_z = np.stack((np.zeros(v_pos.shape), np.zeros(v_pos.shape), np.ones(v_pos.shape)), axis=1)
        local = {}
        for key, arr in self.invFourAmps.items():
            arr_local = arr[1:].reshape((v_num-1)*u_num)[ipos]
            local[key] = np.repeat(arr_local, 3).reshape(u_num, 3)
        eu = (local['dR_ds']*local['dZ_dv']-local['dR_dv']*local['dZ_ds'])*e_p + (local['R']/local['Jacobian'])*(local['dZ_ds']*e_r - local['dR_ds']*e_z)
        ev = (1./local['R'])*e_p
        up0 = u_pos + np.sum(eu*(X0_cart-Xp1), axis=1)

        # caclulate vmec coordinates for flux-surface intersection #
        dist_n1 = np.linalg.norm(X0_cart-Xn1, axis=1)
        dist_p1 = np.linalg.norm(X0_cart-Xp1, axis=1)
        dist_tot = np.linalg.norm(Xp1-Xn1, axis=1)
        u0 = (dist_p1*un0 + dist_n1*up0)/dist_tot
        s0 = np.full(u_num, s_val)
        v0 = np.arctan2(X0_cart[:,1]+X_ma[1], X0_cart[:,0]+X_ma[0])
        X_vmec = np.array(sorted(np.stack((s0, u0, v0), axis=1), key=lambda x: x[1]))

        # sort cartesian points of intersection #
        # X_dot_r = np.sum(X0_cart*r_ma[idx], axis=1)
        # X_dot_z = np.sum(X0_cart*z_ma[idx], axis=1)
        # X_phi = np.arctan2(X_dot_z, X_dot_r)
        # X0_cart = np.array(sorted(np.insert(X0_cart, 0, X_phi, axis=1), key=lambda x: x[0]))[:,1:]
        X_cart = X0_cart

        return X_cart, X_vmec

    def compute_straight_coord_frame(self, s_val, u_dom, v_dom, xi_dom, eta_dom, mode=[1., 4.], normalize=False):
        """ Compute real-space and VMEC coordinates on specified xi/eta grid, and
        caclualte the grad(R'), grad(Z'), and e_xi basis vectors on that grid.

        Parameters
        ----------
        s_val : float
            Normalized toroidal flux coordinate.
        u_dom : 1D arr
            Poloidal domain for xi/eta interpolator inputs.
        v_dom : 1D arr
            Toroidal domain for xi/eta interpolator inputs.
        xi_dom : 1D arr
            xi coordinates over which to generate interpolated xi/eta grid in VMEC coordinates.
        eta_dom : 1D arr
            eta coordinates over which to generate interpolated xi/eta grid in VMEC coordinates.
        mode : tuple, optional
            Toroidal and poloidal mode numbers, respectively of xi/eta coordinates. Default is (1, 4).
        normalize : bool, optional
            When True, the grad(R'), grad(Z'), and e_xi basis vectors will be normalized. Default is False.

        Returns
        -------
        2D arr : Cartesian position vectors of xi/eta grid points.
        2D arr : VMEC position vectors of xi/eta grid points.
        2D arr : grad(R') basis vectors at xi/eta grid points.
        2D arr : grad(Z') basis vectors at xi/eta grid points.
        2D arr : e_xi basis vectors at xi/eta grid points.
        """
        # mode numbers #
        M, N = mode
        M2, N2 = M**2, N**2
        NoM = N/M
        MoN = M/N

        # generate xi/eta maps and compute VMEC coordinates on xi/eta domains #
        u_fit, v_fit = self.invert_symmetry_coordinates(np.array([s_val]), u_dom, v_dom, mode=mode)
        eta_mesh, xi_mesh = np.meshgrid(eta_dom, xi_dom, indexing='ij')
        r_eff = self.a_minor*np.sqrt(s_val)
        s_grid = np.full(eta_mesh.shape, s_val)
        u_grid = u_fit(eta_mesh, xi_mesh)
        v_grid = v_fit(eta_mesh, xi_mesh)
        points = np.stack((s_grid.flatten(), u_grid.flatten(), v_grid.flatten()), axis=1)

        # map VMEC coordinates to real space #
        self.transForm_listPoints(points, ['R', 'Z', 'Jacobian', 'Bmod', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'dL_ds', 'dL_du', 'dL_dv', 'dBmod_ds', 'dBmod_du', 'dBmod_dv'])
        vmec = self.invFourAmps

        # compute straight-field-linear metric components #
        g_ss, g_uu, g_vv, g_su, g_uv, g_vs, gss, guu, gvv, gsu, guv, gvs = cc.compute_metric(self)
        gnn = M2*((vmec['dL_ds']**2)*gss+((1+vmec['dL_du'])**2)*guu+((vmec['dL_dv']-NoM)**2)*gvv+2*vmec['dL_ds']*(1+vmec['dL_du'])*gsu+2*(1+vmec['dL_du'])*(vmec['dL_dv']-NoM)*guv+2*vmec['dL_ds']*(vmec['dL_dv']-NoM)*gvs)
        gsn = M*(vmec['dL_ds']*gss+(1+vmec['dL_du'])*gsu+(vmec['dL_dv']-NoM)*gvs)
        g_xx = M2*(((vmec['dL_dv']-NoM)**2)*g_uu + ((1+vmec['dL_du'])**2)*g_vv - 2*(1+vmec['dL_du'])*(vmec['dL_dv']-NoM)*g_uv)

        # compute grad(B) projections #
        gradB_es = (r_eff/vmec['Bmod'])*(vmec['dBmod_ds'])
        gradB_en = (np.sign(N)/M)*(r_eff/vmec['Bmod'])*(vmec['dBmod_du']*(vmec['dL_dv']+MoN)-vmec['dBmod_dv']*(vmec['dL_du']+1))

        # compute Cartesian points #
        cart_x = vmec['R']*np.cos(points[:,2])
        cart_y = vmec['R']*np.sin(points[:,2])
        cart_z = vmec['Z']
        cart_points = np.stack((cart_x, cart_y, cart_z), axis=1).reshape(eta_dom.shape[0], xi_dom.shape[0], 3)
        vmec_points = points.reshape(eta_dom.shape[0], xi_dom.shape[0], 3)
        
        # repeat variables to construct basis vectors #
        R = np.repeat(vmec['R'], 3).reshape(points.shape[0], 3)
        Z = np.repeat(vmec['Z'], 3).reshape(points.shape[0], 3)
        Jacob = np.repeat(vmec['Jacobian'], 3).reshape(points.shape[0], 3)
        dR_ds = np.repeat(vmec['dR_ds'], 3).reshape(points.shape[0], 3)
        dR_du = np.repeat(vmec['dR_du'], 3).reshape(points.shape[0], 3)
        dR_dv = np.repeat(vmec['dR_dv'], 3).reshape(points.shape[0], 3)
        dZ_ds = np.repeat(vmec['dZ_ds'], 3).reshape(points.shape[0], 3)
        dZ_du = np.repeat(vmec['dZ_du'], 3).reshape(points.shape[0], 3)
        dZ_dv = np.repeat(vmec['dZ_dv'], 3).reshape(points.shape[0], 3)
        dL_ds = np.repeat(vmec['dL_ds'], 3).reshape(points.shape[0], 3)
        dL_du = np.repeat(vmec['dL_du'], 3).reshape(points.shape[0], 3)
        dL_dv = np.repeat(vmec['dL_dv'], 3).reshape(points.shape[0], 3)
        gradB_es = np.repeat(gradB_es, 3).reshape(points.shape[0], 3)
        gradB_en = np.repeat(gradB_en, 3).reshape(points.shape[0], 3)
        gss = np.repeat(gss, 3).reshape(points.shape[0], 3)
        gnn = np.repeat(gnn, 3).reshape(points.shape[0], 3)
        gsn = np.repeat(gsn, 3).reshape(points.shape[0], 3)
        g_xx_sqrt_inv = np.repeat(1./np.sqrt(g_xx), 3).reshape(points.shape[0], 3)

        # compute contravariant basis vectors #
        er = np.stack((np.cos(points[:,2]), np.sin(points[:,2]), np.zeros(points.shape[0])), axis=1)
        ep = np.stack((-np.sin(points[:,2]), np.cos(points[:,2]), np.zeros(points.shape[0])), axis=1)
        ez = np.stack((np.zeros(points.shape[0]), np.zeros(points.shape[0]), np.ones(points.shape[0])), axis=1)
        es = (1./Jacob)*(dR_dv*dZ_du-dR_du*dZ_dv)*ep + (R/Jacob)*(dR_du*ez-dZ_du*er)
        eu = (1./Jacob)*(dR_ds*dZ_dv-dR_dv*dZ_ds)*ep + (R/Jacob)*(dZ_ds*er-dR_ds*ez)
        ev = (1./R)*ep
        en = M*dL_ds*es+M*(1+dL_du)*eu+M*(dL_dv-NoM)*ev
        
        # compute covariant basis vectors #
        e_u = dR_du*er + dZ_du*ez
        e_v = dR_dv*er + R*ep + dZ_dv*ez
        e_x = -M*(dL_dv-NoM)*e_u + M*(dL_du+1)*e_v

        # compute primed basis vectors #
        R_prime = -gradB_es*es - gradB_en*en
        Z_prime = -Jacob*r_eff*(gsn*gradB_es+gnn*gradB_en)*es + Jacob*r_eff*(gss*gradB_es+gsn*gradB_en)*en
        # Z_prime = -Jacob*r_eff*gsn*gradB_es*es + Jacob*r_eff*gss*gradB_es*en
        X_prime = e_x
        if normalize:
            R_prime = R_prime / np.repeat(np.linalg.norm(R_prime, axis=1), 3).reshape(R_prime.shape)
            Z_prime = Z_prime / np.repeat(np.linalg.norm(Z_prime, axis=1), 3).reshape(Z_prime.shape)
            X_prime = X_prime*g_xx_sqrt_inv
        R_prime = R_prime.reshape(eta_dom.shape[0], xi_dom.shape[0], 3)
        Z_prime = Z_prime.reshape(eta_dom.shape[0], xi_dom.shape[0], 3)
        X_prime = X_prime.reshape(eta_dom.shape[0], xi_dom.shape[0], 3)

        return cart_points, vmec_points, R_prime, Z_prime, X_prime

    def locate_axis_in_symmetry_planes(self, v_dom, xi_cart, R_prime, Z_prime, X_prime):
        # define number of grid points #
        num_of_eta = R_prime.shape[0]
        num_of_xi = R_prime.shape[1]
        num_of_tor = v_dom.shape[0]

        # compute magnetic axis #
        self.transForm_2D_uSec(np.array([0]), 0, v_dom, ['R', 'Z'])
        ma_vec_x = self.invFourAmps['R'][0]*np.cos(v_dom)
        ma_vec_y = self.invFourAmps['R'][0]*np.sin(v_dom)
        ma_vec_z = self.invFourAmps['Z'][0]
        ma_vec = np.stack((ma_vec_x, ma_vec_y, ma_vec_z), axis=1)

        # compute flux-surface geometry #
        self.transForm_2D_sSec(s_val, u_dom, v_dom, ['R', 'Z'])

        # find axis in symmetry planes #
        axis_location = np.empty((num_of_eta, num_of_xi, 2))
        surf_location_cart = np.empty((num_of_eta, num_of_xi, num_of_pol, 3))
        surf_location_proj = np.empty((num_of_eta, num_of_xi, num_of_pol, 3))
        for i in range(num_of_eta):
            for j in range(num_of_xi):
                # define basis vectors #
                Rp = R_prime[i,j]
                Zp = Z_prime[i,j]
                Xp = X_prime[i,j]
                xi_vec = xi_cart[i,j]

                # find axial intersection with plane #
                ma_from_xi = ma_vec-xi_vec
                Xp_tiled = np.tile(Xp, v_dom.shape[0]).reshape(v_dom.shape[0], 3)
                ma_dot_X = np.sum(ma_from_xi*Xp_tiled, axis=1)
                dot_min, dot_max = np.argmin(ma_dot_X), np.argmax(ma_dot_X)
                dot_sign = np.sign(ma_dot_X[dot_min:dot_max+1])
                dot_sign[np.where(dot_sign == 0)] = 1
                sign_chng = ((np.roll(dot_sign, 1)-dot_sign) != 0).astype(int)
                ilft = dot_min+np.flatnonzero(sign_chng[1:])[0]
                irgt = ilft+1
                dvec = ma_vec[irgt]-ma_vec[ilft]
                dvec = dvec/np.linalg.norm(dvec)
                vec0 = (ma_from_xi[ilft]) - (np.dot(ma_from_xi[ilft], Xp)/np.dot(dvec, Xp))*dvec
                vec_R = -np.dot(vec0, Rp)
                vec_Z = -np.dot(vec0, Zp)
                axis_location[i,j] = [vec_R, vec_Z]

        return axis_location

    def compute_symmetry_aligned_cross_sections(self, s_val, u_dom, v_dom, R_prime, Z_prime, X_prime, space_curve):
        # define number of grid points #
        num_of_eta = R_prime.shape[0]
        num_of_xi = R_prime.shape[1]
        num_of_pol = u_dom.shape[0]
        num_of_tor = v_dom.shape[0]

        # compute magnetic axis #
        self.transForm_2D_uSec(np.array([0]), 0, v_dom, ['R', 'Z'])
        ma_vec_x = self.invFourAmps['R'][0]*np.cos(v_dom)
        ma_vec_y = self.invFourAmps['R'][0]*np.sin(v_dom)
        ma_vec_z = self.invFourAmps['Z'][0]
        ma_vec = np.stack((ma_vec_x, ma_vec_y, ma_vec_z), axis=1)

        # compute flux-surface geometry #
        v_mesh, u_mesh = np.meshgrid(v_dom, u_dom, indexing='ij')
        self.transForm_2D_sSec(s_val, u_dom, v_dom, ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv'])
        surf_vec_x = self.invFourAmps['R']*np.cos(v_mesh)
        surf_vec_y = self.invFourAmps['R']*np.sin(v_mesh)
        surf_vec_z = self.invFourAmps['Z']
        surf_vec = np.stack((surf_vec_x, surf_vec_y, surf_vec_z), axis=2)

        # find axis in symmetry planes #
        axis_location = np.empty((num_of_eta, num_of_xi, 3))
        surf_location_cart = np.empty((num_of_eta, num_of_xi, num_of_pol, 3))
        surf_location_proj = np.empty((num_of_eta, num_of_xi, num_of_pol, 3))
        for i in range(num_of_eta):
            for j in range(num_of_xi):
                # locate space curve and define basis vectors #
                sp_curve = space_curve[i,j]
                Rp = R_prime[i,j]
                Zp = Z_prime[i,j]
                Xp = X_prime[i,j]

                # find axial intersection with plane #
                curve_to_ma = ma_vec-sp_curve
                Xp_tiled = np.tile(Xp, num_of_tor).reshape(num_of_tor, 3)
                ma_dot_X = np.sum(curve_to_ma*Xp_tiled, axis=1)
                dot_min, dot_max = np.argmin(ma_dot_X), np.argmax(ma_dot_X)
                dot_sign = np.sign(ma_dot_X[dot_min:dot_max+1])
                dot_sign[np.where(dot_sign == 0)] = 1
                sign_chng = ((np.roll(dot_sign, 1)-dot_sign) != 0).astype(int)
                ilft = dot_min+np.flatnonzero(sign_chng[1:])[0]
                irgt = ilft+1
                dvec = ma_vec[irgt]-ma_vec[ilft]
                dvec = dvec/np.linalg.norm(dvec)
                curve_to_intersection = (curve_to_ma[ilft]) - (np.dot(curve_to_ma[ilft], Xp)/np.dot(dvec, Xp))*dvec
                vec_R = -np.dot(curve_to_intersection, Rp)
                vec_Z = -np.dot(curve_to_intersection, Zp)
                ma_loc = sp_curve + curve_to_intersection
                axis_location[i,j] = ma_loc

                # find flux-surface intersection with plane #
                inum = (dot_max-dot_min)+1
                surf_vec_chk = surf_vec[dot_min:dot_max+1]-ma_loc
                Xp_tiled = np.tile(Xp, inum*num_of_pol).reshape(inum, num_of_pol, 3)
                surf_dot_Xp = np.sum(surf_vec_chk*Xp_tiled, axis=2)
                chk_sign = np.sign(surf_dot_Xp)
                chk_sign[np.where(chk_sign == 0)] = 1
                chk_sign_chng = ((np.roll(chk_sign, 1, axis=0)-chk_sign) != 0).astype(int)
                ipos = np.flatnonzero(chk_sign_chng[1:])
                ineg = ipos - np.full(ipos.shape, num_of_pol, dtype=int)
                Xn1 = surf_vec_chk[1:].reshape((inum-1)*num_of_pol, 3)[ineg]
                Xp1 = surf_vec_chk[1:].reshape((inum-1)*num_of_pol, 3)[ipos]
                X_delta = Xp1 - Xn1
                X_delta = X_delta / np.repeat(np.linalg.norm(X_delta, axis=1), 3).reshape(num_of_pol, 3)
                Delta = -np.sum(Xn1*Xp, axis=1)/np.sum(X_delta*Xp, axis=1)
                ma_to_intersection = Xn1+np.repeat(Delta, 3).reshape(num_of_pol, 3)*X_delta
                surf_cart = ma_loc + ma_to_intersection
                vec_R = np.sum(ma_to_intersection*np.tile(Rp, num_of_pol).reshape(num_of_pol, 3), axis=1)
                vec_Z = np.sum(ma_to_intersection*np.tile(Zp, num_of_pol).reshape(num_of_pol, 3), axis=1)
                vec_P = np.arctan2(vec_Z, vec_R)
                surf_cart = np.array(sorted(np.insert(surf_cart, 0, vec_P, axis=1), key=lambda x: x[0]))[:,1:]
                surf_proj = np.array(sorted(np.stack((vec_R, vec_Z, vec_P), axis=1), key=lambda x: x[2]))
                surf_location_cart[i,j] = surf_cart
                surf_location_proj[i,j] = surf_proj
            
        return axis_location, surf_location_cart, surf_location_proj

    def compute_helical_frame(self, b_axis, points):
        """ Calculate the helical reference frame in axial cross-section.

        Parameters
        ----------
        b_axis : arr
            Tangent vector to the magnetic axis.
        points : arr
            Flux-surface intersection in VMEC coordinates.

        Returns
        -------
        arr : Radial-like vectors along flux-surface intersection.
        arr : Vertical-like vectors along flux-surface intersection.
        arr : Space-curve-averaged radial-like vector.
        arr : Space-curve-averaged vertical-like vector.
        """
        # define helical grid variables #
        gradR = np.empty(points.shape)
        gradZ = np.empty(points.shape)
        gradR_avg = np.empty(b_axis.shape)
        gradZ_avg = np.empty(b_axis.shape)

        npts = points.shape[1]
        for i in range(b_axis.shape[0]):
            # define intersection points #
            s_val = points[i,0,0]
            u_dom = points[i,:,1]
            v_dom = points[i,:,2]

            # compute VMEC variables #
            self.transForm_listPoints_sSec(s_val, points[i,:,1:], ['R', 'Z', 'Jacobian', 'Bmod', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'dBmod_ds', 'dBmod_du', 'dBmod_dv'])
            vmec = {}
            for key, arr in self.invFourAmps.items():
                vmec[key] = np.repeat(arr, 3).reshape(npts, 3)
            
            # compute cylindrical basis vectors #
            e_r = np.stack((np.cos(v_dom), np.sin(v_dom), np.zeros(npts)), axis=1)
            e_p = np.stack((-np.sin(v_dom), np.cos(v_dom), np.zeros(npts)), axis=1)
            e_z = np.stack((np.zeros(npts), np.zeros(npts), np.ones(npts)), axis=1)

            # compute VMEC contravariant basis vectors #
            es = (vmec['R']/vmec['Jacobian'])*(vmec['dR_du']*e_z - vmec['dZ_du']*e_r) + (1./vmec['Jacobian'])*(vmec['dR_dv']*vmec['dZ_du'] - vmec['dR_du']*vmec['dZ_dv'])*e_p
            eu = (vmec['R']/vmec['Jacobian'])*(vmec['dZ_ds']*e_r - vmec['dR_ds']*e_z) + (1./vmec['Jacobian'])*(vmec['dR_ds']*vmec['dZ_dv'] - vmec['dR_dv']*vmec['dZ_ds'])*e_p
            ev = (1./vmec['R'])*e_p

            # compute radial basis vector #
            a = self.a_minor * np.sqrt(s_val)
            b_ma = np.tile(b_axis[i], npts).reshape(npts, 3)
            gradB = (a/vmec['Bmod'])*(vmec['dBmod_ds']*es + vmec['dBmod_du']*eu + vmec['dBmod_dv']*ev)
            B_dot_b = np.repeat(np.sum(gradB*b_ma, axis=1), 3).reshape(npts, 3)
            gR = -gradB + B_dot_b*b_ma
            gZ = np.cross(gR, b_ma)

            # parameterize space curve #
            du_dl = np.gradient(u_dom)
            dv_dl = np.gradient(v_dom)
            g_uu = (self.invFourAmps['dR_du']**2) + (self.invFourAmps['dZ_du']**2)
            g_vv = (self.invFourAmps['dR_dv']**2) + (self.invFourAmps['R']**2) + (self.invFourAmps['dZ_dv']**2)
            g_uv = self.invFourAmps['dR_du']*self.invFourAmps['dR_dv'] + self.invFourAmps['dZ_du']*self.invFourAmps['dZ_dv']
            dx_dl = np.sqrt((du_dl**2)*g_uu + 2*du_dl*dv_dl*g_uv + (dv_dl**2)*g_vv)

            # compute line-averaged basis vectors #
            gR_avg = np.empty(3)
            gZ_avg = np.empty(3)
            norm_inv = 1./(np.trapz(dx_dl*du_dl, u_dom) + np.trapz(dx_dl*dv_dl, v_dom))
            for j in range(3):
                gR_avg[j] = (np.trapz(gR[:,j]*dx_dl*du_dl, u_dom) + np.trapz(gR[:,j]*dx_dl*dv_dl, v_dom))*norm_inv
                gZ_avg[j] = (np.trapz(gZ[:,j]*dx_dl*du_dl, u_dom) + np.trapz(gZ[:,j]*dx_dl*dv_dl, v_dom))*norm_inv
            gR_avg = gR_avg/np.linalg.norm(gR_avg)
            gZ_avg = gZ_avg/np.linalg.norm(gZ_avg)

            gradR[i] = gR
            gradZ[i] = gZ
            gradR_avg[i] = gR_avg
            gradZ_avg[i] = gZ_avg

        return gradR, gradZ, gradR_avg, gradZ_avg

    def compute_projected_xi_curve(self, s_val, u_dom, v_dom, xi_dom, eta_dom, N=4.):
        """ Project the xi curves onto the grad(B)-aligned reference frame in the axial cross-sections.
        
        Parameters
        ----------
        s_val : float
           Toroidal flux coordinate used to label the flux surface being investigated.
        u_dom : 1D arr
            Poloidal coordinates for computation.
        v_dom : 1D arr
            Toroidal coordinates for computation.
        xi_dom : 1D arr
            xi coordinates for computation.
        eta_dom : 1D arr
            eta coordinates for computation.
        N : float, optional
            Toroidal symmetry mode. Default is N=4.

        Returns
        2D vector array : xi/eta grid of Cartesian coordinates of xi curves
        2D vector array : xi/eta grid of VMEC coordinates of xi curves
        2D vector array : xi/eta grid of Projected coordinates of xi curves
        """
        # generate xi/eta maps and axial plane data #
        u_fit, v_fit = self.invert_symmetry_coordinates(np.array([s_val]), u_dom, v_dom, N=N)
        Xma, bma, Xfs_cart, Xfs_vmec = self.compute_axial_planes(s_val, u_dom, v_dom)
        gradR, gradZ, gradR_avg, gradZ_avg = self.compute_helical_frame(bma, Xfs_vmec)

        # generate interpolators #
        model_Xma_x = spi.CubicSpline(v_dom, Xma[:,0])
        model_Xma_y = spi.CubicSpline(v_dom, Xma[:,1])
        model_Xma_z = spi.CubicSpline(v_dom, Xma[:,2])
        model_Rp_x = spi.CubicSpline(v_dom, gradR_avg[:,0])
        model_Rp_y = spi.CubicSpline(v_dom, gradR_avg[:,1])
        model_Rp_z = spi.CubicSpline(v_dom, gradR_avg[:,2])
        model_Zp_x = spi.CubicSpline(v_dom, gradZ_avg[:,0])
        model_Zp_y = spi.CubicSpline(v_dom, gradZ_avg[:,1])
        model_Zp_z = spi.CubicSpline(v_dom, gradZ_avg[:,2])

        # loop over xi values #
        npts_eta = eta_dom.shape[0]
        npts_xi = xi_dom.shape[0]
        xi_cart = np.empty((npts_xi, npts_eta, 3))
        xi_vmec = np.empty((npts_xi, npts_eta, 3))
        xi_prime = np.empty((npts_xi, npts_eta, 3))
        for i, xi_val in enumerate(xi_dom):
            # get VMEC coordinates for xi curve #
            u_vals = u_fit(eta_dom, np.full(npts_eta, xi_val))
            v_vals = v_fit(eta_dom, np.full(npts_eta, xi_val))
            s_vals = np.full(npts_eta, s_val)

            # comute xi curve #
            points = np.stack((u_vals, v_vals), axis=1)
            self.transForm_listPoints_sSec(s_val, points, ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv'])
            xi_local = {}
            for key, arr in self.invFourAmps.items():
                xi_local[key] = np.repeat(arr, 3)
            xi_curve_x = self.invFourAmps['R']*np.cos(v_vals)
            xi_curve_y = self.invFourAmps['R']*np.sin(v_vals)
            xi_curve_z = self.invFourAmps['Z']
            xi_curve = np.stack((xi_curve_x, xi_curve_y, xi_curve_z), axis=1)
            xi_cart[i] = xi_curve
            xi_vmec[i] = np.stack((s_vals, u_vals, v_vals), axis=1)
            
            # identify toroidal domain of candidate planes #
            # X_min = np.array([self.invFourAmps['R'][0]*np.cos(points[0,1]), self.invFourAmps['R'][0]*np.sin(points[0,1]), self.invFourAmps['Z'][0]])
            # X_max = np.array([self.invFourAmps['R'][-1]*np.cos(points[-1,1]), self.invFourAmps['R'][-1]*np.sin(points[-1,1]), self.invFourAmps['Z'][-1]])
            X_min = np.array([self.invFourAmps['R'][-1]*np.cos(points[-1,1]), self.invFourAmps['R'][-1]*np.sin(points[-1,1]), self.invFourAmps['Z'][-1]])
            X_max = np.array([self.invFourAmps['R'][0]*np.cos(points[0,1]), self.invFourAmps['R'][0]*np.sin(points[0,1]), self.invFourAmps['Z'][0]])
            X_dot_b_min = np.sum(bma*(np.tile(X_min, bma.shape[0]).reshape(bma.shape[0], 3)-Xma), axis=1)
            X_dot_b_max = np.sum(bma*(np.tile(X_max, bma.shape[0]).reshape(bma.shape[0], 3)-Xma), axis=1)
            idx_min = np.where(X_dot_b_min <= 0)[0]-1
            idx_max = np.where(X_dot_b_max >= 0)[0]+1
            idx_chk = np.intersect1d(idx_min, idx_max)
            num_of_plns = idx_chk.shape[0]

            # loop over eta values #
            for j, eta_val in enumerate(eta_dom):
                # get shifted index #
                ibeg = idx_chk[0]
                jdx = ibeg+j
                
                # identify adjacent planes #
                xi_vec = xi_curve[j]
                b_dot_xi = np.sum(bma[idx_chk]*(np.tile(xi_vec, num_of_plns).reshape(num_of_plns, 3)-Xma[idx_chk]), axis=1)
                dot_sign = np.sign(b_dot_xi)
                dot_sign[np.where(dot_sign == 0)] = 1
                sign_chng = ((np.roll(dot_sign, 1)-dot_sign) != 0).astype(int)
                ilft = np.flatnonzero(sign_chng[1:])[0]
                irgt = ilft+1
                v_pln =  (b_dot_xi[ilft]*v_dom[ibeg+irgt] - b_dot_xi[irgt]*v_dom[ibeg+ilft])/(b_dot_xi[ilft]-b_dot_xi[irgt])
                
                # interpolate magnetic axis #
                Xma_xi_x = model_Xma_x(v_pln)
                Xma_xi_y = model_Xma_y(v_pln)
                Xma_xi_z = model_Xma_z(v_pln)
                Xma_xi = np.array([Xma_xi_x, Xma_xi_y, Xma_xi_z])

                # interpolate R prime vector #
                Rp_xi_x = model_Rp_x(v_pln)
                Rp_xi_y = model_Rp_y(v_pln)
                Rp_xi_z = model_Rp_z(v_pln)
                Rp_xi = np.array([Rp_xi_x, Rp_xi_y, Rp_xi_z])

                # interpolate Z prime vector #
                Zp_xi_x = model_Zp_x(v_pln)
                Zp_xi_y = model_Zp_y(v_pln)
                Zp_xi_z = model_Zp_z(v_pln)
                Zp_xi = np.array([Zp_xi_x, Zp_xi_y, Zp_xi_z])

                # project xi curve onto helical frame #
                xi_vec = xi_curve[j] - Xma_xi
                xi_Rp = np.dot(xi_vec, Rp_xi)
                xi_Zp = np.dot(xi_vec, Zp_xi)
                xi_prime[i,j] = np.array([xi_Rp, v_pln, xi_Zp])
        
        return xi_cart, xi_vmec, xi_prime

    def compute_shaping_parameters(self, X_fs, R_prime, Z_prime, p_set=[2,3,4]):
        # define minor radius of flux surfaces #
        # X_ma = np.tile(X_ma, X_surf.shape[1]).reshape(X_ma.shape[0], X_surf.shape[1], 3)
        # X = X_surf-X_ma
        X_norm = np.linalg.norm(X_fs, axis=2)

        # compute theta mapping #
        R_prime = np.tile(R_prime, X_fs.shape[1]).reshape(X_fs.shape[0], X_fs.shape[1], 3)
        Z_prime = np.tile(Z_prime, X_fs.shape[1]).reshape(X_fs.shape[0], X_fs.shape[1], 3)
        X_dot_R = np.sum(X_fs*R_prime, axis=2)
        X_dot_Z = np.sum(X_fs*Z_prime, axis=2)
        theta = np.arctan2(X_dot_Z, X_dot_R)
        shaping_parameters = np.empty((theta.shape[0], len(p_set)))
        for i, the in enumerate(theta):
            the_map = np.stack((the, X_norm[i]), axis=1)
            the_sort = np.array(sorted(the_map, key=lambda x: x[0]))
            the = the_sort[:,0]
            X2_the = the_sort[:,1]**2
            X2_norm_inv = 1./np.trapz(X2_the, the)
            for j, p in enumerate(p_set):
                shape = ((-1)**(p-1))*np.trapz(X2_the*np.cos(p*the), the)*X2_norm_inv
                shaping_parameters[i,j] = shape

        return shaping_parameters

    def calc_metric_tensor(self, s_dom, u_dom, v_dom):
        """ Calculate the components of the covariant and contravariant metric tensor.

        Parameters
        ----------
        s_dom : array
            1D array of radial s coordinates
        u_dom : array
            1D array of poloidal u coordinates
        v_dom : array
            1D array of toroidal v coordinates
        """
        if self.space_derivs:
            s_num = len(s_dom)
            u_num = len(u_dom)
            v_num = len(v_dom)

            # Perform Fourier Transform #
            ampKeys = ['R', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv']
            if (s_num > 1) and (u_num > 1) and (v_num > 1):
                self.transForm_3D(s_dom, u_dom, v_dom, ampKeys)
            elif (s_num == 1) and (u_num > 1) and (v_num > 1):
                self.transForm_2D_sSec(s_dom[0], u_dom, v_dom, ampKeys)
            elif (s_num > 1) and (u_num == 1) and (v_num > 1):
                self.transForm_2D_uSec(s_dom, u_dom[0], v_dom, ampKeys)
            elif (s_num > 1) and (u_num > 1) and (v_num == 1):
                self.transForm_2D_vSec(s_dom, u_dom, v_dom[0], ampKeys)
            elif (s_num == 1) and (u_num == 1) and (v_num == 1):
                self.transForm_1D(s_dom[0], u_dom[0], v_dom[0], ampKeys)

            # Retreive Spatial Coordinates and Derivatives #
            R = self.invFourAmps['R']

            dR_ds = self.invFourAmps['dR_ds']
            dR_du = self.invFourAmps['dR_du']
            dR_dv = self.invFourAmps['dR_dv']

            dZ_ds = self.invFourAmps['dZ_ds']
            dZ_du = self.invFourAmps['dZ_du']
            dZ_dv = self.invFourAmps['dZ_dv']

            # Calculate Covariant Metric Components #
            self.gss_covar = dR_ds**2 + dZ_ds**2
            self.gsu_covar = dR_ds * dR_du + dZ_ds * dZ_du
            self.gsv_covar = dR_ds * dR_dv + dZ_ds * dZ_dv
            self.guu_covar = dR_du**2 + dZ_du**2
            self.guv_covar = dR_du * dR_dv + dZ_du * dZ_dv
            self.gvv_covar = dR_dv**2 + dZ_dv**2 + R**2

            # Calculate Contravariant Metric Components #
            jacob_inv = 1. / (R * R * ((dR_du * dZ_ds - dR_ds * dZ_du)**2))
            self.gss_contra = jacob_inv * ((dR_du*dZ_dv - dR_dv*dZ_du)**2
                                     + (R**2) * (dR_du**2 + dZ_du**2))
            self.guu_contra = jacob_inv * ((dR_ds*dZ_dv - dR_dv*dZ_ds)**2
                                     + (R**2) * (dZ_ds**2 + dR_ds**2))
            self.gvv_contra = jacob_inv * ((dR_ds*dZ_du - dR_du*dZ_ds)**2)
            self.gsu_contra = jacob_inv * ((dR_ds*dZ_dv - dR_dv*dZ_ds)
                                     * (dR_dv*dZ_du - dR_du*dZ_dv)
                                     - (R**2) * (dZ_ds*dZ_du + dR_ds*dR_du))
            self.gsv_contra = jacob_inv * ((dR_du*dZ_dv - dR_dv*dZ_du)
                                     * (dR_ds*dZ_du - dR_du*dZ_ds))
            self.guv_contra = jacob_inv * ((dR_ds*dZ_dv - dR_dv*dZ_ds)
                                     * (dR_du*dZ_ds - dR_ds*dZ_du))

        else:
            raise KeyError("space_derivs flag must be set to True")


    def plot_spectrum(self, plot, specKey, nSpec=10, excFirst=True):
        """ Plots the most prominant modes according to the radially
        integrated Fourier amplitudes.  The first mode is excluded by default.

        Parameters
        ----------
        ax : object
            axis on which to plot the spectrum
        ampKey : str
            key specifying Fourier amplitude to be plotted
        nSpec : int
            number of modes to plot (default is 10)
        excFirst : boolean
            defaults to exclude the first mode (default is True)
        """
        roa_dom = np.sqrt(self.s_grid)

        roa_beg = 0.; roa_end = 1.
        roa_beg_idx = np.argmin(np.abs(roa_dom - roa_beg))
        roa_end_idx = np.argmin(np.abs(roa_dom - roa_end)) + 1

        roa_dom = roa_dom[roa_beg_idx:roa_end_idx]
        roa_norm = 1. / np.mean(roa_dom)

        specData = {}
        specOrder = []
        for i in range(0, self.md):
            key = '[{0}, {1}]'.format(int(self.xn[i]), int(self.xm[i]))
            spec = self.fourierAmps[specKey](self.s_grid)[0::, i]

            spec_avg = np.trapz(np.abs(spec[roa_beg_idx:roa_end_idx]), roa_dom) * roa_norm

            specData[key] = spec
            specOrder.append([spec_avg, key])

        if excFirst:
            specOrder = specOrder[1::]

        specOrder = sorted(specOrder, key=lambda x: x[0], reverse=True)
        for i in range(nSpec):
            key = specOrder[i][1]
            spec = specData[key]

            plot.plt.plot(self.s_grid, spec, label=key)

        plot.plt.xlabel(r'$\Psi_N$')
        plot.plt.ylabel(specKey)

        box = plot.ax.get_position()
        plot.ax.set_position([box.x0, box.y0, box.width * 1, box.height])

        plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1))


if __name__ == '__main__':
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from matplotlib.ticker import AutoMinorLocator, MultipleLocator

    import sys
    ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
    sys.path.append(ModDir)
    import plot_define as pd
    import vtkTools.vtk_grids as vtkG
    import vmecTools.wout_files.coord_convert as cc
    import vmecTools.wout_files.vecB_tools as vb

    # wout_path_eq1 = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec', 'wout_0-3-84_n10p0_mn1824_ns101.nc')
    wout_path_eq1 = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
    wout_path_eq2 = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_3', 'job_84', 'wout_0-3-84_7p0_mn1824_ns101.nc')
    wout_path_eq3 = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_3', 'job_84', 'wout_0-3-84_n10p0_mn1824_ns101.nc')

    wout_paths = [wout_path_eq1, wout_path_eq2, wout_path_eq3]
    num_of_configs = len(wout_paths)

    # equilibrium grid #
    s_eq1 = 0.5
    upts = 251
    vpts = 3
    u_dom = np.linspace(-np.pi, np.pi, upts)
    v_dom = np.linspace(0, 0.25*np.pi, vpts)

    # compute first equilibrium surface #
    wout_eq1 = readWout(wout_path_eq1)
    wout_eq1.transForm_2D_sSec(s_eq1, u_dom, v_dom, ['R', 'Z'])
    R_eq1 = wout_eq1.invFourAmps['R']
    Z_eq1 = wout_eq1.invFourAmps['Z']
        
    # plotting parameters #
    plot = pd.plot_define(lineWidth=2)
    plt = plot.plt
    fig, ax = plt.subplots(1, 1, tight_layout=True)

    # plot first equilibrium surface #
    for i in range(3):
        if i == 0:
            plt1, = ax.plot(R_eq1[i], Z_eq1[i], label='Eq. 1')
        else:
            ax.plot(R_eq1[i], Z_eq1[i], c=plt1.get_color())

    # plot additional equilibria #
    for i in range(1, num_of_configs):
        wout = readWout(wout_paths[i])
        s = ((wout_eq1.a_minor/wout.a_minor)**2)*s_eq1
        wout.transForm_2D_sSec(s, u_dom, v_dom, ['R', 'Z'])
        R = wout.invFourAmps['R']
        Z = wout.invFourAmps['Z']
        for j in range(3):
            if j == 0:
                plt1, = ax.plot(R[j], Z[j], label='Eq. {}'.format(i+1))
            else:
                ax.plot(R[j], Z[j], c=plt1.get_color())

    # axis labels #
    ax.set_xlabel(r'$R \ / \ \mathrm{m}$')
    ax.set_ylabel(r'$Z \ / \ \mathrm{m}$')
    ax.legend(frameon=False)

    # axis ticks #
    ax.tick_params(axis='both', which='both', direction='in')
    ax.xaxis.set_ticks_position('default')
    ax.yaxis.set_ticks_position('default')

    # minor ticks #
    tick_space = 0.025
    ax.xaxis.set_minor_locator(MultipleLocator(tick_space))
    ax.yaxis.set_minor_locator(MultipleLocator(tick_space))

    plt.show()
    save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'thesis', 'figures', 'chapter_9', 'flux_shape_comparison.pdf')
    # plt.savefig(save_path, format='pdf')
