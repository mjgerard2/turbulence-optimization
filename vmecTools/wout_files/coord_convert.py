# -*- coding: utf-8 -*-
"""
Created on Thu May  7 11:40:22 2020

@author: micha
"""

import numpy as np

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)


def compute_metric(wout, basis='both'):
    """ Compute the covariant metric matrix from the input wout file.
    
    Parmaters
    ---------
    wout : object
        wout file object containing all necessary variables to construct 
        covariant metric matrix
        
    Raises
    ------
    KeyError
        invFourAmps does not contain all variables necessary to construct the 
        metric matrix
    """
    try:
        R = wout.invFourAmps['R']
        jacob = wout.invFourAmps['Jacobian']
        
        dR_ds = wout.invFourAmps['dR_ds']
        dR_du = wout.invFourAmps['dR_du']
        dR_dv = wout.invFourAmps['dR_dv']
        
        dZ_ds = wout.invFourAmps['dZ_ds']
        dZ_du = wout.invFourAmps['dZ_du']
        dZ_dv = wout.invFourAmps['dZ_dv']
    
    except KeyError:
        raise KeyError('invFourAmps does not have all necessary variables to perform cylindrical transformation.')

    if basis == 'covariant' or basis == 'both':
        # compute covariant metric components #
        g_ss = dR_ds**2 + dZ_ds**2
        g_uu = dR_du**2 + dZ_du**2
        g_vv = dR_dv**2 + dZ_dv**2 + R**2
        g_su = dR_ds*dR_du + dZ_ds*dZ_du
        g_uv = dR_du*dR_dv + dZ_du*dZ_dv
        g_vs = dR_ds*dR_dv + dZ_ds*dZ_dv

    if basis == 'contravariant' or basis == 'both':
        # compute contravariant metric components #
        R_inv = (1./R)
        jacob_inv = (1./jacob)
        jacob2_inv = jacob_inv**2
        RvZu = dR_dv*dZ_du-dR_du*dZ_dv
        RsZv = dR_ds*dZ_dv-dR_dv*dZ_ds
        gss = jacob2_inv*(RvZu*RvZu+R*R*(dR_du*dR_du+dZ_du*dZ_du))
        guu = jacob2_inv*(RsZv*RsZv+R*R*(dZ_ds*dZ_ds+dR_ds*dR_ds))
        gvv = R_inv*R_inv
        gsu = jacob2_inv*(RvZu*RsZv-R*R*(dR_ds*dR_du+dZ_ds*dZ_du))
        guv = R_inv*jacob_inv*RsZv
        gvs = R_inv*jacob_inv*RvZu
    
    if basis == 'both':
        return g_ss, g_uu, g_vv, g_su, g_uv, g_vs, gss, guu, gvv, gsu, guv, gvs
    elif basis == 'covariant':
        return g_ss, g_uu, g_vv, g_su, g_uv, g_vs
    elif basis == 'contravariant':
        return gss, guu, gvv, gsu, guv, gvs
    else:
        raise KeyError('{} is not a valid input for \'basis\''.format(basis))

def calc_Gss_metric(wout, vtk_make=None, vtk_dict=None):
    """ Calculate the Gss metric.  The calculation is approximately
        
    \frac{ \int Gss da }{ \int da }

    Parameters
    ----------
    vtk_make : None or str, optional
        If not None, the string should be a key entry for the feature to
        be plotted as VTK file. The default is None.
            options
                'gss_metric' : Gss metric
    vtk_dict : None or dict, optional
        If not None, the dictionary should contain the directory path and
        file name of the VTK file to be saved. The default is None.
            'savePath' : save directory
            'name' : name of VTK file being saved
            
    Raises
    ------
    KeyError
        vtk_make string provided was invalid.
    ValueError
        Invalid dimensionality in wout data.  Must be dimesnion 2 or 3.
    """
    try:
        R = wout.invFourAmps['R']
        jacob = wout.invFourAmps['Jacobian']
        
        ndim = R.ndim
        
        dR_ds = wout.invFourAmps['dR_ds']
        dR_du = wout.invFourAmps['dR_du']
        dR_dv = wout.invFourAmps['dR_dv']
        
        dZ_ds = wout.invFourAmps['dZ_ds']
        dZ_du = wout.invFourAmps['dZ_du']
        dZ_dv = wout.invFourAmps['dZ_dv']
        
        B = wout.invFourAmps['Bmod']
    
    except KeyError:
            raise KeyError('Not all keys needed for curvature calculations are provided in wout.invFourAmps.')
        
    gss = dR_ds**2 + dZ_ds**2
    det_gxx_inv = 1. /  (R * R * ((dR_du * dZ_ds - dR_ds * dZ_du)**2))
    g_ss = det_gxx_inv * ((dR_du * dZ_dv - dR_dv * dZ_du)**2 + R * R * (dZ_du * dZ_du + dR_du * dR_du))
        
    du = wout.u_dom[1] - wout.u_dom[0]
    dv = wout.v_dom[1] - wout.v_dom[0]
    da = du * dv
    
    if ndim == 3:
        da_norm = np.abs(da * jacob[1::]) * np.sqrt(g_ss[1::])
        gss_da = B[1::] * gss[1::] * da_norm
        
        gss_metric = np.zeros(wout.s_num)
        for idx in range(1,wout.s_num-1):
            SA = np.sum(da_norm[idx,0:-1,0:-1])
            gss_metric[idx] = np.sum(gss_da[idx,0:-1,0:-1]) / SA

        if vtk_make and vtk_dict:
            print('Sorry, this code is not yet ready to make 3D flux surface VTKs')
        
        return gss_metric
        
    elif ndim == 2:
        da_norm = np.abs(da * jacob) * np.sqrt(g_ss)
        gss_da = B * gss * da_norm
        
        surf_area = np.sum(da_norm)
        SA_inv = 1. / surf_area
        
        gss_metric = gss_da * SA_inv
        
        if vtk_make and vtk_dict:
            import vtkTools.vtk_grids as vtkG
            
            if ndim == 2:
                coord = cartCoord(wout)
                if vtk_make == 'gss_metric':
                    vtkG.scalar_mesh(vtk_dict['savePath'], vtk_dict['name'], coord, gss_metric)
                elif vtk_make == 'gss_surface':
                        vtkG.scalar_mesh(vtk_dict['savePath'], vtk_dict['name'], coord, gss)
                else:
                    raise KeyError('{} is not a valide vtk_make key.'.format(vtk_make))
            else:
                raise ValueError('Cannot make vtk file with {} dimensional grid.'.format(ndim)) 
        
        return gss_metric
                            
    else:
        raise ValueError('{} is not a workabe dimensionality for the Bk overlap calculation')
                       

def cylnd_Bvec(wout):
    """ Transforms magnetic field vector from flux coordinates to cylindrical
    coordinates {Br,Bp,Bz}.
    
    Parameters
    ----------
    wout : object
        wout file object containing all necessary variables to perform 
        cylindrical transformation
        
    Raises
    ------
    KeyError
        invFourAmps does not contain all variables necessary to perform 
        transformation

    Returns
    -------
    array
        4D array of magnetic vector field in cylindrical coordinates 
        {Br,Bp,Bz}, indexed by {s,v,u,B}
    """
    try:
        Bs = wout.invFourAmps['Bs']
        Bv = wout.invFourAmps['Bv']
        Bu = wout.invFourAmps['Bu']
        
        R = wout.invFourAmps['R']
        dR_ds = wout.invFourAmps['dR_ds']
        dR_dv = wout.invFourAmps['dR_dv']
        dR_du = wout.invFourAmps['dR_du']
        
        dZ_ds = wout.invFourAmps['dZ_ds']
        dZ_dv = wout.invFourAmps['dZ_dv']
        dZ_du = wout.invFourAmps['dZ_du']
            
    except KeyError:
        raise KeyError('invFourAmps does not have all necessary variables to perform cylindrical transformation.')
        
    B_norm = 1. / (dR_ds * dZ_du - dR_du * dZ_ds)
    Br = (dZ_du * Bs - dZ_ds * Bu) * B_norm 
    Bt = ( ( (Bs * (dR_du*dZ_dv - dR_dv*dZ_du) + Bu * (dR_dv*dZ_ds - dR_ds*dZ_dv)) * B_norm ) + Bv) / R
    Bz = (dR_ds * Bu - dR_du * Bs) * B_norm
    
    return np.stack((Br, Bt, Bz), axis=R.ndim)


def cyldCoord_Bfield(coord_data, B_data, r_res=0.001, z_res=0.001):
    """ Interpolates the magnetic field in real space in cylindrical coordinates,
    indexed by the cylindrical domain [r,p,z].

    Parameters
    ----------
    r_res : float
        r coordinate resolution in meters
    z_res : array
        z coordinate resolution in meters
    """
    from scipy.interpolate import griddata

    vec_ma = coord_data[0, :, 0, :]

    r_grid = coord_data[:, :, :, 0]
    z_grid = coord_data[:, :, :, 1]

    t_num = coord_data.shape[1]

    r_lim = [-.25, .25]
    r_num = int((r_lim[1] - r_lim[0]) / r_res)
    r_dom = np.linspace(r_lim[0], r_lim[1], r_num)

    z_lim = [-.25, .25]
    z_num = int((z_lim[1] - z_lim[0]) / z_res)
    z_dom = np.linspace(z_lim[0], z_lim[1], z_num)

    B_cyld = np.empty((t_num, r_num, z_num, 3))
    coord_cyld = np.empty((t_num, r_num, z_num, 2))
    for t in range(t_num):
        z_grid_new, r_grid_new = np.meshgrid(z_dom+vec_ma[t, 1], r_dom+vec_ma[t, 0])

        r_grd = r_grid[:, t, :].flatten()
        z_grd = z_grid[:, t, :].flatten()

        Br = griddata((r_grd, z_grd), B_data[:, t, :, 0].flatten(), (r_grid_new, z_grid_new))
        Bt = griddata((r_grd, z_grd), B_data[:, t, :, 1].flatten(), (r_grid_new, z_grid_new))
        Bz = griddata((r_grd, z_grd), B_data[:, t, :, 2].flatten(), (r_grid_new, z_grid_new))


        B_cyld[t, :, :] = np.stack((Br, Bt, Bz), axis=2)
        coord_cyld[t, :, :] = np.stack((r_grid_new, z_grid_new), axis=2)

    return B_cyld, coord_cyld


def cartCoord(wout):
    R = wout.invFourAmps['R'].reshape(wout.v_num, wout.u_num)
    Z = wout.invFourAmps['Z'].reshape(wout.v_num, wout.u_num)

    v_dom = wout.v_dom

    cartCoord = np.empty((wout.v_num, wout.u_num, 3))

    for v_idx, v in enumerate(v_dom):
        r = R[v_idx, :]
        z = Z[v_idx, :]

        x = r * np.cos(v)
        y = r * np.sin(v)

        coord = np.stack([x, y, z], axis=1)
        cartCoord[v_idx][:] = coord

    return cartCoord


def cyld_to_flux(points, wout, thresh=1e-6):
    from scipy.interpolate import splprep, splev

    sym = 5
    upts = 251

    u_dom = np.linspace(-np.pi, np.pi, upts)
    v_dom = np.linspace(-np.pi, np.pi, sym*upts)

    wout.transForm_3D(wout.s_grid, u_dom, v_dom, ['R', 'Z'])

    s_dom = wout.s_dom

    R_valsGrid = wout.invFourAmps['R'][0::, 0::, 0::]
    Z_valsGrid = wout.invFourAmps['Z'][0::, 0::, 0::]

    u_num = u_dom.shape[0]

    ampKeys = ['R', 'Z', 'dR_du', 'dR_ds', 'dZ_du', 'dZ_ds']

    v_idx = np.empty(points.shape[0])

    error = np.empty(points.shape[0])
    flux_coord = np.empty((points.shape[0],3))
    for idx, v in enumerate(points[0::,2]):
        v_idx = np.argmin(np.abs(v_dom - v))

        R = points[idx,0]
        Z = points[idx,1]

        R_vals = R_valsGrid[0::,v_idx,0::]
        Z_vals = Z_valsGrid[0::,v_idx,0::]

        R_flat_idx = np.argmin(np.abs(R_vals - R))
        s_Ridx = int( (R_flat_idx - (R_flat_idx % u_num)) / u_num )
        u_Ridx = int( R_flat_idx - (s_Ridx * u_num) )
        R_guess = np.hypot((R-R_vals[s_Ridx, u_Ridx]), (Z-Z_vals[s_Ridx, u_Ridx]))

        Z_flat_idx = np.argmin(np.abs(Z_vals - Z))
        s_Zidx = int( (Z_flat_idx - (Z_flat_idx % u_num)) / u_num )
        u_Zidx = int( Z_flat_idx - (s_Zidx * u_num) )
        Z_guess = np.hypot((R-R_vals[s_Zidx, u_Zidx]), (Z-Z_vals[s_Zidx, u_Zidx]))

        if R_guess <= Z_guess:
            s, u = s_dom[s_Ridx], u_dom[u_Ridx]
        elif R_guess > Z_guess:
            s, u = s_dom[s_Zidx], u_dom[u_Zidx]

        s, u = 0.1, np.pi
        wout.transForm_1D(s, u, v, ampKeys)
        print('Initial Guess: ')
        print('    (s, u, v) = ({0:0.4f}, {1:0.2f}, {2:0.2f})'.format(s, u*(180./np.pi), v*(180./np.pi)))
        print('    (R, Phi, Z) = ({0:0.4f}, {1:0.2f}, {2:0.4f})\n'.format(wout.invFourAmps['R'], v*(180./np.pi), wout.invFourAmps['Z']))

        aprx = {}
        for key in ampKeys:
            aprx[key] = wout.invFourAmps[key]

        err = (R - aprx['R'])**2 + (Z - aprx['Z'])**2
        err_prev = err
        cnt = 0

        u_prev = u
        s_prev = s
        while (err > thresh) and (cnt < 25):
            jacob = aprx['dR_du'] * aprx['dZ_ds'] - aprx['dR_ds'] * aprx['dZ_du']
            u = u_prev + ( (aprx['dZ_ds'] * (R - aprx['R']) - aprx['dR_ds'] * (Z - aprx['Z'])) / jacob )
            s = s_prev + ( (aprx['dR_du'] * (Z - aprx['Z']) - aprx['dZ_du'] * (R - aprx['R'])) / jacob )

            print('Guess {0:0.0f}: '.format(cnt+1))
            print('    (s, u, v) = ({0:0.4f}, {1:0.2f}, {2:0.2f})'.format(s, u*(180./np.pi), v*(180./np.pi)))
            wout.transForm_1D(s, u, v, ampKeys)
            print('    (R, Phi, Z) = ({0:0.4f}, {1:0.2f}, {2:0.4f})\n'.format(wout.invFourAmps['R'], v*(180./np.pi), wout.invFourAmps['Z']))

            aprx_new = {}
            for key in ampKeys:
                aprx_new[key] = wout.invFourAmps[key]

            err = (R - aprx_new['R'])**2 + (Z - aprx_new['Z'])**2

            if (err > err_prev) or (s < 0):
                jacob_new = aprx_new['dR_du'] * aprx_new['dZ_ds'] - aprx_new['dR_ds'] * aprx_new['dZ_du']

                u = u_prev + .5 * ( (aprx['dZ_ds'] * (R - aprx['R']) - aprx['dR_ds'] * (Z - aprx['Z'])) / (.75*jacob + .25*jacob_new) )
                s = s_prev + .5 * ( (aprx['dR_du'] * (Z - aprx['Z']) - aprx['dZ_du'] * (R - aprx['R'])) / (.75*jacob + .25*jacob_new) )

                wout.transForm_1D(s, u, v, ampKeys)

                aprx_new = {}
                for key in ampKeys:
                    aprx_new[key] = wout.invFourAmps[key]

                err = (R - aprx_new['R'])**2 + (Z - aprx_new['Z'])**2

                if (err > err_prev) or (s < 0):
                    jacob_new_new = aprx_new['dR_du'] * aprx_new['dZ_ds'] - aprx_new['dR_ds'] * aprx_new['dZ_du']

                    u = u_prev + .25 * ( (aprx['dZ_ds'] * (R - aprx['R']) - aprx['dR_ds'] * (Z - aprx['Z'])) / (.5*jacob + .25*jacob_new + .25*jacob_new_new) )
                    s = s_prev + .25 * ( (aprx['dR_du'] * (Z - aprx['Z']) - aprx['dZ_du'] * (R - aprx['R'])) / (.5*jacob + .25*jacob_new + .25*jacob_new_new) )

                    wout.transForm_1D(s, u, v, ampKeys)

                    aprx_new = {}
                    for key in ampKeys:
                        aprx_new[key] = wout.invFourAmps[key]

                    err = (R - aprx_new['R'])**2 + (Z - aprx_new['Z'])**2

                    if (err > err_prev) or (s < 0):
                        jacob_new_new_new = aprx_new['dR_du'] * aprx_new['dZ_ds'] - aprx_new['dR_ds'] * aprx_new['dZ_du']

                        u = u_prev + .125 * ( (aprx['dZ_ds'] * (R - aprx['R']) - aprx['dR_ds'] * (Z - aprx['Z'])) / (.25*(jacob + jacob_new + jacob_new_new + jacob_new_new_new) ) )
                        s = s_prev + .125 * ( (aprx['dR_du'] * (Z - aprx['Z']) - aprx['dZ_du'] * (R - aprx['R'])) / (.25*(jacob + jacob_new + jacob_new_new + jacob_new_new_new) ) )

                        wout.transForm_1D(s, u, v, ampKeys)

                        aprx_new = {}
                        for key in ampKeys:
                            aprx_new[key] = wout.invFourAmps[key]

                        err = (R - aprx_new['R'])**2 + (Z - aprx_new['Z'])**2
            aprx['R'] = aprx_new['R']
            aprx['Z'] = aprx_new['Z']

            aprx['dR_du'] = aprx_new['dR_du']
            aprx['dZ_du'] = aprx_new['dZ_du']

            aprx['dR_ds'] = aprx_new['dR_ds']
            aprx['dZ_ds'] = aprx_new['dZ_ds']

            cnt += 1
            s_prev = s
            u_prev = u
            err_prev = err

        if np.isnan(s):
            flux_coord[idx] = np.full(3, np.nan)
            error[idx] = np.nan
        else:
            flux_coord[idx] = np.array([s,u,v])
            error[idx] = err

    ### Calculate percent of failed point conversions ###
    num_of_nans = np.count_nonzero(np.isnan(error))
    num_of_true = flux_coord.shape[0] - num_of_nans
    fail_rate = (num_of_nans / error.shape[0]) * 100

    ### Mask failed point conversions ###
    flux_coord_mask = np.ma.masked_invalid(flux_coord)
    flux_coord_mask_bool = ~flux_coord_mask.mask
    flux_coord = flux_coord[flux_coord_mask_bool].reshape(num_of_true, 3)

    ### Interpolate failed points ###
    s_samp = flux_coord[0::,0]
    u_samp = ((2*np.pi) + flux_coord[0::,1]) % (2*np.pi)
    v_samp = flux_coord[0::,2]

    u_samp_diff = u_samp[0:-1] - u_samp[1::]

    mult = 0
    for idx, u_diff in enumerate(u_samp_diff):
        u_samp[idx] = u_samp[idx] + mult * (2*np.pi)
        if u_diff > 6:
            mult+=1
    u_samp[-1] = u_samp[-1] + mult * (2*np.pi)

    tck, u = splprep([s_samp, u_samp, v_samp])
    s_knots, u_knots, v_knots = splev(tck[0], tck)
    v_fine = np.linspace(0, 1, points.shape[0])
    s_fine, u_fine, v_fine = splev(v_fine, tck)
    flux_coords = np.stack((s_fine, u_fine, v_fine), axis=1)

    ### Return ###
    return flux_coords, fail_rate
