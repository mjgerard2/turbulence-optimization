import os
import numpy as np
import wout_read as wr


# Import wout Files #
s1 = 0.5
config_id = '60-1-84'

main_id = 'main_coil_{}'.format(config_id.split('-')[0])
set_id = 'set_{}'.format(config_id.split('-')[1])
job_id = 'job_{}'.format(config_id.split('-')[2])

wout_path_1 = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data')
wout_name_1 = 'wout_QHS_best.nc'

wout_path_2 = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
wout_name_2 = 'wout_HSX_'+config_id+'.nc'

wout1 = wr.readWout(wout_path_1, name=wout_name_1)
wout2 = wr.readWout(wout_path_2, name=wout_name_2)

# Get r_eff for wout1 #
minor1 = wout1.a_minor
minor2 = wout2.a_minor
s2 = s1 * (minor1/minor2)**2

print(s2)
