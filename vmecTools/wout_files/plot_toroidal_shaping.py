import os, sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import vmecTools.wout_files.vecB_tools as vb
import vmecTools.wout_files.wout_read as wr
import plot_define as pd

# equilibrium grids #
u_dom = np.linspace(-np.pi, np.pi, 101)
v_dom = np.linspace(-.25*np.pi, .25*np.pi, 101)
amp_keys = ['R', 'Z', 'Bmod', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'dBmod_ds', 'dBmod_du', 'dBmod_dv']

# magnetic axis grid #
u_ma = np.array([0])
v_ma = v_dom

# compute equilibrium grids #
wout_path_qhs = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
wout_path_he = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_60', 'set_1', 'job_84', 'wout_60-1-84_mn1824_ns101.nc')
wout_path_sq = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_152', 'set_1', 'job_485', 'wout_152-1-485_mn1824_ns101.nc')
wout_paths = [wout_path_qhs, wout_path_he, wout_path_sq]

shapes = np.empty((len(wout_paths), v_dom.shape[0], 3))
for wdx, wout_path in enumerate(wout_paths):
    print(wout_path)
    wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)

    # compute magnetic axis #
    wout.transForm_2D_sSec(0, u_ma, v_ma, ['R', 'Z'])
    R_ma = wout.invFourAmps['R'][:,0]
    Z_ma = wout.invFourAmps['Z'][:,0]

    # compute flux surface quantities #
    wout.transForm_2D_sSec(0.5, u_dom, v_dom, amp_keys)
    R_grid = wout.invFourAmps['R']
    Z_grid = wout.invFourAmps['Z']
    B_grid = wout.invFourAmps['Bmod']

    # compute helical basis vectors #
    vecB = vb.BvecTools(wout)
    R_vec, Z_vec = vecB.compute_helical_frame()

    # compute poloidally averaged basis vectors #
    X_ma = np.stack((R_ma, Z_ma), axis=1)
    X_surf = np.stack((R_grid, Z_grid), axis=2)
    R_vec_avg, Z_vec_avg = vecB.compute_average_helical_frame(X_ma, X_surf, R_vec, Z_vec)
    
    # compute shaping parameters #
    shapes[wdx] = vecB.compute_shaping_parameters(X_ma, X_surf, R_vec_avg, Z_vec_avg)

# plotting parameters #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(3, 1, sharex=True, tight_layout=True, figsize=(8, 12))
ax1, ax2, ax3 = axs

ax1.plot(v_dom/np.pi, shapes[0,:,0], c='tab:blue', label='QHS')
ax1.plot(v_dom/np.pi, shapes[1,:,0], c='tab:red', label='HE-QHS')
ax1.plot(v_dom/np.pi, shapes[2,:,0], c='tab:green', label='Sq-QHS')

ax2.plot(v_dom/np.pi, shapes[0,:,1], c='tab:blue', label='QHS')
ax2.plot(v_dom/np.pi, shapes[1,:,1], c='tab:red', label='HE-QHS')
ax2.plot(v_dom/np.pi, shapes[2,:,1], c='tab:green', label='Sq-QHS')

ax3.plot(v_dom/np.pi, shapes[0,:,2], c='tab:blue', label='QHS')
ax3.plot(v_dom/np.pi, shapes[1,:,2], c='tab:red', label='HE-QHS')
ax3.plot(v_dom/np.pi, shapes[2,:,2], c='tab:green', label='Sq-QHS')

ax3.set_xlabel(r'$\zeta \ / \ \pi$')
ax1.set_ylabel(r'$\kappa$')
ax2.set_ylabel(r'$\delta$')
ax3.set_ylabel(r'$\zeta$')

ax1.set_xlim(v_dom[0]/np.pi, v_dom[-1]/np.pi)
y1_max = np.max(np.abs(ax1.get_ylim()))
y2_max = np.max(np.abs(ax2.get_ylim()))
y3_max = np.max(np.abs(ax3.get_ylim()))
ax1.set_ylim(-y1_max, y1_max)
ax2.set_ylim(-y2_max, y2_max)
ax3.set_ylim(-y3_max, y3_max)

ax1.legend()
ax2.legend()
ax3.legend()
plt.show()
