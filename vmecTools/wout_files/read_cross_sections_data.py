import os, sys
import h5py as hf
import numpy as np

from scipy.interpolate import CubicSpline
from scipy.optimize import brentq

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import vmecTools.wout_files.coord_convert as cc
import vmecTools.wout_files.wout_read as wr
import plot_define as pDef


# read cross section data #
# hf_path = os.path.join('/home', 'michael', 'Desktop', 'preciseQH_xi_proj', 'eta_family_cross_sections_M1Nn4_res_test.h5')
# hf_path = os.path.join('/home', 'michael', 'Desktop', 'sqHSX_xi_proj', 'eta_family_cross_sections_M1N4.h5')
hf_path = os.path.join('/home', 'michael', 'Desktop', 'stdHSX_xi_proj', 'eta_family_cross_sections_M1N4.h5')
# config_id = '150-1-702'
with hf.File(hf_path, 'r') as hf_:
    tor_dom = hf_['toroidal domain'][()]
    psi_dom = hf_['psi domain'][()]
    eta_dom = hf_['eta domain'][()]
    CS_cart = hf_['cart coordinates'][()]
    CS_vmec = hf_['vmec coordinates'][()]

# define grid points #
M, N = 1., 4.
num_of_tor = tor_dom.shape[0]
num_of_psi = psi_dom.shape[0]
num_of_eta = eta_dom.shape[0]

# import wout file #
# wout_path = os.path.join('/home', 'michael', 'Desktop', 'wout_20221002-01-009_QH_nfp4_A6p5_n0_3_T0_15_avoidIota1_MercierSMin0p1.nc')
# wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_185', 'set_1', 'job_647', 'wout_185-1-647_mn1824_ns101.nc')
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)

# compute magnetic axis #
MA_points = np.stack((np.zeros(num_of_tor), np.zeros(num_of_tor), tor_dom), axis=1)
wout.transForm_listPoints(MA_points, ['R', 'Z', 'dR_dv', 'dZ_dv'])
Xma_x = wout.invFourAmps['R']*np.cos(tor_dom)
Xma_y = wout.invFourAmps['R']*np.sin(tor_dom)
Xma_z = wout.invFourAmps['Z']
Xma = np.stack((Xma_x, Xma_y, Xma_z), axis=1)

# get tangent curve to magnetic axis #
er = np.stack((np.cos(tor_dom), np.sin(tor_dom), np.zeros(num_of_tor)), axis=1)
ep = np.stack((-np.sin(tor_dom), np.cos(tor_dom), np.zeros(num_of_tor)), axis=1)
ez = np.stack((np.zeros(num_of_tor), np.zeros(num_of_tor), np.ones(num_of_tor)), axis=1)
Tma_r = np.repeat(wout.invFourAmps['dR_dv'], 3).reshape(num_of_tor, 3)*er
Tma_p = np.repeat(wout.invFourAmps['R'], 3).reshape(num_of_tor, 3)*ep
Tma_z = np.repeat(wout.invFourAmps['dZ_dv'], 3).reshape(num_of_tor, 3)*ez
g_vv_sqrt = np.sqrt((wout.invFourAmps['dR_dv']**2) + (wout.invFourAmps['R']**2) + (wout.invFourAmps['dZ_dv']**2))
g_vv_sqrt_norm = 1./np.repeat(g_vv_sqrt, 3).reshape(num_of_tor, 3)
Tma = Tma_r + Tma_p + Tma_z
Tma = Tma * g_vv_sqrt_norm

# compute VMEC varaibles on flux surface #
Xfs = np.empty((num_of_tor, num_of_psi, num_of_eta, 3))
for i in range(num_of_tor):
    wout.transForm_listPoints(CS_vmec[i].reshape(num_of_psi*num_of_eta, 3), ['R', 'Z', 'Lambda'])
    vmec = wout.invFourAmps
    for key, arr in vmec.items():
        vmec[key] = arr.reshape(num_of_psi, num_of_eta)
    Xfs_x = vmec['R']*np.cos(CS_vmec[i,:,:,2])
    Xfs_y = vmec['R']*np.sin(CS_vmec[i,:,:,2])
    Xfs_z = vmec['Z']
    Xfs[i] = np.stack((Xfs_x, Xfs_y, Xfs_z), axis=2)

# compute eta and xi coordinates #
pol_mesh = CS_vmec[:,:,:,1]
tor_mesh = CS_vmec[:,:,:,2]
eta_mesh = M*(pol_mesh+vmec['Lambda']) - N*tor_mesh
xi_mesh = (1./M)*(pol_mesh+vmec['Lambda']) + (1./N)*tor_mesh

# identify centeral eta and xi coordinates #
p_set = np.array([2, 3, 4])
R_set = np.empty((num_of_tor, num_of_psi, num_of_eta))
the_set = np.empty((num_of_tor, num_of_psi, num_of_eta))
phi_set = np.empty((num_of_tor, num_of_psi, num_of_eta))
shapes = np.empty((num_of_tor, num_of_psi, p_set.shape[0]))
xi_prox = tor_dom[0]*N*((1./(M*M))+(1./(N*N)))
for i in range(num_of_tor):
    print('({}|{})'.format(i+1, num_of_tor))
    for j in range(num_of_psi):
        # compute dot product with vector tangent to the magnetic axis #
        X = Xfs[i,j,:] - np.tile(Xma[i], num_of_eta).reshape(num_of_eta, 3)
        Tma_tile = np.tile(Tma[i], num_of_eta).reshape(num_of_eta, 3)
        X_dot_Tma = np.sum(X*Tma_tile, axis=1)

        # find roots of dot product #
        dot_sign = np.sign(X_dot_Tma)
        dot_sign[np.where(dot_sign == 0)] = 1
        sign_chng = ((np.roll(dot_sign, 1)-dot_sign) != 0).astype(int)
        zero_idx = np.flatnonzero(sign_chng[1:])

        # select eta root for e_x vector #
        dot_model = CubicSpline(eta_dom, X_dot_Tma)
        xi_model = CubicSpline(eta_dom, xi_mesh[i,j])
        eta_roots = np.empty(zero_idx.shape[0])
        for k, idx in enumerate(zero_idx):
            eta_root = brentq(dot_model, eta_dom[idx], eta_dom[idx+1])
            eta_roots[k] = eta_root
        xi_chk = xi_model(eta_roots)
        idx = np.argmin(np.abs(xi_chk-xi_prox))
        xi_prox = xi_chk[idx]
        eta_root = eta_roots[idx]

        # compute Cartesian basis vectors #
        Xfs_x_model = CubicSpline(eta_dom, Xfs[i,j,:,0])
        Xfs_y_model = CubicSpline(eta_dom, Xfs[i,j,:,1])
        Xfs_z_model = CubicSpline(eta_dom, Xfs[i,j,:,2])
        e_x = np.array([Xfs_x_model(eta_root), Xfs_y_model(eta_root), Xfs_z_model(eta_root)])-Xma[i]
        e_x = e_x/np.linalg.norm(e_x)
        e_y = np.cross(Tma[i], e_x)
        e_z = Tma[i]

        # tile basis vectors and compute Cartesian coordinates #
        e_x_tile = np.tile(e_x, num_of_eta).reshape(num_of_eta, 3)
        e_y_tile = np.tile(e_y, num_of_eta).reshape(num_of_eta, 3)
        e_z_tile = np.tile(e_z, num_of_eta).reshape(num_of_eta, 3)
        X_x = np.sum(X*e_x_tile, axis=1)
        X_y = np.sum(X*e_y_tile, axis=1)
        X_z = np.sum(X*e_z_tile, axis=1)

        # convert to spherical coordinates #
        R = np.sqrt((X_x**2) + (X_y**2) + (X_z**2))
        phi = np.arccos(X_z/R)
        the_chk = np.arctan2(X_y, X_x)
        shift_idx = np.argmin(the_chk)
        if shift_idx < (0.5*num_of_eta):
            the_chk[0:shift_idx+1] = the_chk[0:shift_idx+1]+2*np.pi
        else:
            the_chk[shift_idx+1:] = the_chk[shift_idx+1:]-2*np.pi
        the = the_chk

        # compute line average #
        dRdn = np.gradient(R, eta_dom)
        dphidn = np.gradient(phi, eta_dom)
        dthedn = np.gradient(the, eta_dom)
        dRvecdn = np.sqrt((dRdn**2)+(R**2)*(dphidn**2)*(np.sin(phi)**2)+(R**2)*(dthedn**2))
        Sp = np.empty(p_set.shape)
        for idx, p in enumerate(p_set):
            integ_norm = 1./np.trapz(R*dRvecdn, eta_dom)
            Sp[idx] = ((-1)**(p+1))*np.trapz(R*dRvecdn*np.cos(p*eta_dom), eta_dom)*integ_norm

        shapes[i,j] = Sp
        R_set[i,j] = R
        the_set[i,j] = the
        phi_set[i,j] = phi

if True:
    plot = pDef.plot_define()
    plt = plot.plt
    fig, axs = plt.subplots(1, 3, tight_layout=True, figsize=(18, 6))

    for tdx, tor in enumerate(np.linspace(tor_dom[0], tor_dom[-1], 3)):
        i = np.argmin(np.abs(tor_dom-tor))
        for j in range(num_of_psi):
            axs[tdx].scatter(R_set[i,j]*np.cos(the_set[i,j]), R_set[i,j]*np.sin(the_set[i,j]), c=np.cos(phi_set[i,j]), s=1, cmap='coolwarm', zorder=10)
            axs[tdx].plot(R_set[i,j]*np.cos(the_set[i,j]), R_set[i,j]*np.sin(the_set[i,j]), c='k', ls='--', lw=0.5)
            # axs[tdx].plot(Xfs_cyld[i,j,:,0], Xfs_cyld[i,j,:,1], c='k', ls='--', lw=0.5)
            # axs[tdx].scatter(Xfs_cyld[i,j,:,0], Xfs_cyld[i,j,:,1], c=eta_mesh[i,j,:], cmap='magma', s=1, zorder=10)
            # axs[tdx].scatter(Xfs_cyld[i,j,:,0], Xfs_cyld[i,j,:,1], c=eta_dom, cmap='magma', s=1, zorder=10)

    for ax in axs.flat:
        ax.set_aspect('equal')
        ax.set_xlabel(r'$R \ / \ \mathrm{m}$')
        ax.set_ylabel(r'$Z \ / \ \mathrm{m}$')
        # ax.set_xlim(np.min(Xfs_cyld[:,:,:,0]), np.max(Xfs_cyld[:,:,:,0]))
        # ax.set_ylim(np.min(Xfs_cyld[:,:,:,1]), np.max(Xfs_cyld[:,:,:,1]))

    plt.show()

if False:
    # plotting parameters #
    plot = pDef.plot_define()
    plt = plot.plt
    fig, axs = plt.subplots(1, 3, tight_layout=True, figsize=(18, 6))

    for psi in np.linspace(0.1, 1.0, 5):
        i = np.argmin(np.abs(psi_dom-psi))
        axs[0].plot(tor_dom/np.pi, shapes[:,i,0], label=r'$\psi \ / \ \psi_\mathrm{{N}} = {0:0.2f}$'.format(psi_dom[i]))
        axs[1].plot(tor_dom/np.pi, shapes[:,i,1], label=r'$\psi \ / \ \psi_\mathrm{{N}} = {0:0.2f}$'.format(psi_dom[i]))
        axs[2].plot(tor_dom/np.pi, shapes[:,i,2], label=r'$\psi \ / \ \psi_\mathrm{{N}} = {0:0.2f}$'.format(psi_dom[i]))

    axs[0].set_ylabel(r'$\kappa$')
    axs[1].set_ylabel(r'$\delta$')
    axs[2].set_ylabel(r'$\zeta$')
    for ax in axs.flat:
        ax.set_xlabel(r'$\zeta \ / \ \pi$')
        ax.set_xlim(tor_dom[0]/np.pi, tor_dom[-1]/np.pi)
        ax.legend(frameon=False)

    plt.show()
