import os
import numpy as np

import wout_read as wr
import coord_convert as cc
import vecB_tools as vecB

import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from flfTools import flf_funcs


# Plotting Variables #
plt.close('all')

font = {'family' : 'sans-serif',
        'weight' : 'normal',
        'size'   : 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

fig, ax = plt.subplots(1, 1, tight_layout=True)
ax.set_aspect('equal')

# Import wout File #
wout_dirc = os.path.join('/home', 'michael', 'onedrive', 'Stellarators', 'ETOS', 'wout_files', 'c506')
wout_name = 'wout_etos_506.nc'

wout = wr.readWout(wout_dirc, name=wout_name, space_derivs=True)

points = np.array([[0.14274, 0, 0]])
"""
flux_coords, fail_rate = cc.cyld_to_flux(points, wout, thresh=1e-6)

print(flux_coords)
"""

v_dom = np.linspace(-np.pi, np.pi, 3)
u_dom = np.linspace(-np.pi, np.pi, 251)

wout.transForm_3D(wout.s_grid, u_dom, v_dom, ['R', 'Z', 'Bmod'])
vB = vecB.BvecTools(wout, 'Bmod')
vB.plot_B(fig, ax, wout.invFourAmps['R'], wout.invFourAmps['Z'], vLst=[0])

fileName = os.path.join('/home', 'michael', 'onedrive', 'Stellarators', 'ETOS', 'wout_files', 'c506', 'poincare_set.h5')
flf_funcs.plot_poin_data(fig, ax, fileName, 0, scale='mT')

ax.scatter(points[0, 0], points[0, 1], marker='x', s=250, c='k')

saveName = os.path.join('/home', 'michael', 'onedrive', 'Stellarators', 'ETOS', 'wout_files', 'c506', 'vmec_flf.png')
plt.savefig(saveName)
# plt.show()
