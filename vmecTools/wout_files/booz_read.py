# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 10:53:09 2020

@author: micha
"""

import os

import numpy as np
import h5py as hf

from netCDF4 import Dataset
from scipy.interpolate import interp1d

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization')
sys.path.append(WORKDIR)

import vmecTools.wout_files.wout_read as wr

import plot_define as pd
import directory_dict as dd


class readBooz():

    def __init__(self, filePath):
        # pathName = os.path.join(path, name)
        rootgrp = Dataset(filePath, 'r')

        ns = rootgrp['/ns_b'][0] - 1
        nsurf = rootgrp['/rmnc_b'][()].shape[0]

        self.ns = ns - (ns - nsurf)
        self.ds = 1. / ns
        self.s_full = np.linspace(0.5 * self.ds, 1 - 0.5 * self.ds, ns)

        s_dom_beg = 0.5 * self.ds
        s_dom_end = 1 - (ns - nsurf + 0.5) * self.ds
        self.s_dom = np.linspace(s_dom_beg, s_dom_end, self.ns)

        self.iota = interp1d(self.s_full, rootgrp['/iota_b'][1::])

        self.mpol = rootgrp['/mboz_b'][0]
        self.ntor = rootgrp['/nboz_b'][0]

        self.xm = rootgrp['/ixm_b'][:]
        self.xn = rootgrp['/ixn_b'][:]
        self.modes = self.xm.shape[0]

        # self.Bmod = rootgrp['/bmnc_b'][:,:]
        self.fourierAmps = {'R': interp1d(self.s_dom, rootgrp['/rmnc_b'][:, :], axis=0),
                            'Z': interp1d(self.s_dom, rootgrp['/zmns_b'][:, :], axis=0),
                            'P': interp1d(self.s_dom, rootgrp['/pmns_b'][:, :], axis=0),
                            'Jacobian': interp1d(self.s_dom, rootgrp['/gmn_b'][:, :], axis=0),
                            'Bmod': interp1d(self.s_dom, rootgrp['/bmnc_b'][:, :], axis=0)}

        self.cosine_keys = ['R', 'Jacobian', 'Bmod']
        self.sine_keys = ['Z', 'P']

        # self.begIdx = self.ns - self.Bmod.shape[0]

        rootgrp.close()

    def transForm_2D_sSec(self, s_val, u_dom, v_dom, ampKeys=['Bmod']):
        self.s_num = 1
        self.u_num = u_dom.shape[0]
        self.v_num = v_dom.shape[0]

        pol, tor = np.meshgrid(u_dom, v_dom)

        pol_xm = np.dot(self.xm.reshape(self.md, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn = np.dot(self.xn.reshape(self.md, 1), tor.reshape(1, self.v_num * self.u_num))

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.v_num, self.u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.v_num, self.u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))


    def plot_spectrum(self, ax, nSpec=10, excFirst=True):
        """ Plots the most prominant modes according to the Fourier amplitude
        specified.  The first mode is excluded by default.

        Parameters
        ----------
        ax : object
            axis on which to plot the spectrum
        ampKey : str
            key specifying Fourier amplitude to be plotted
        nSpec : int
            number of modes to plot (default is 10)
        excFirst : boolean
            defaults to exclude the first mode (default is True)
        """
        from matplotlib.ticker import MultipleLocator

        if excFirst:
            io = 1
        else:
            io = 0

        specData = {}
        specOrder = []

        full_spectrum = self.fourierAmps['Bmod'](self.s_dom)
        if self.modes > io+nSpec:
            for i in range(io+nSpec):
                key = '({0}, {1})'.format(int(self.xn[i]), int(self.xm[i]))
                spec = full_spectrum[:, i]
                specMax = np.abs(np.trapezoid(spec, self.s_dom))

                specData[key] = spec
                specOrder.append([specMax, key])

            specOrder = np.array(specOrder, dtype=object)
            for i in range(io+nSpec, self.modes):
                key = '({0}, {1})'.format(int(self.xn[i]), int(self.xm[i]))
                spec = full_spectrum[:, i]
                specMax = np.abs(np.trapezoid(spec, self.s_dom))

                if specMax > np.min(specOrder[:, 0]):
                    idx = np.argmin(specOrder[:, 0])
                    rm_key = specOrder[idx, 1]
                    del specData[rm_key]

                    specData[key] = spec
                    specOrder[idx] = np.array([specMax, key], dtype=object)

        else:
            for i in range(0, self.modes):
                key = '({0}, {1})'.format(int(self.xn[i]), int(self.xm[i]))
                spec = self.fourierAmps['Bmod'](self.s_dom)
                specMax = np.abs(np.trapezoid(spec, self.s_dom))

                specData[key] = spec
                specOrder.append([specMax, key])

        specOrder = np.array(sorted(specOrder, key=lambda x: x[0], reverse=True))
        for i in range(nSpec):
            key = specOrder[io+i][1]
            spec = specData[key]

            ax.plot(self.s_dom, spec, label=key)

        ax.set_xlabel(r'$\hat{\psi}$')
        ax.set_ylabel(r'$B_{nm}$')

        ax.set_xlim(0, 1)
        ax.plot(ax.get_xlim(), [0,0], c='k', ls='--', zorder=0)

        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 1, box.height])

        ax.legend(loc='upper left', bbox_to_anchor=(1.01, 1), title=r'$(n,\, m)$')

        # major ticks #
        ax.tick_params(axis='both', which='major', direction='in', length=5)
        ax.tick_params(axis='both', which='minor', direction='in', length=2.5)
        ax.xaxis.set_ticks_position('default')
        ax.yaxis.set_ticks_position('default')
        ax.xaxis.set_minor_locator(MultipleLocator(0.05))
        ax.yaxis.set_minor_locator(MultipleLocator(0.00625))

    def qhs_metric(self, s_dom):
        """ Calculate the root sum square of the symmetry breaking
        modes in the boozer spectrum.
        """
        num_of_modes = self.modes
        Bmn = np.zeros(len(s_dom))
        Bmod_model = self.fourierAmps['Bmod']

        for i in range(num_of_modes):
            # print('({0:0.0f}|{1:0.0f})'.format(i+1, num_of_modes))
            if self.xm[i] == 0 and self.xn[i] == 0:
                B_00 = Bmod_model(s_dom)[:, i]
            elif self.xm[i] == 0 and self.xn[i] != 0:
                Bmn = Bmn + Bmod_model(s_dom)[:, i]**2
            else:
                sym_chk = self.xn[i]/self.xm[i]
                if sym_chk != 4:
                    Bmn = Bmn + Bmod_model(s_dom)[:, i]**2

        return np.sqrt(Bmn)/B_00

    def qhs_sym_metric(self):
        """ Calculate the root sum square of the symmetry preserving
        modes in the boozer spectrum.
        """
        num_of_modes = self.modes
        Bmn = np.zeros(self.ns)
        for i in range(num_of_modes):
            if self.xm[i] == 0 and self.xn[i] == 0:
                B_00 = self.fourierAmps['Bmod'](self.s_dom)[:,i]
            elif self.xm[i] != 0:
                sym_chk = self.xn[i]/self.xm[i]
                if sym_chk == 4:
                    Bmn = Bmn + self.fourierAmps['Bmod'](self.s_dom)[:,i]**2

        return np.sqrt(Bmn)/B_00

    def qhs_sym_metric_surf(self, s_val):
        """ Calculate the root sum square of the symmetry preserving
        modes in the boozer spectrum on a single flux surface.

        Parameters
        ----------
        s_val : float
            Flux surface label
        """
        num_of_modes = self.modes
        Bmn = 0
        for i in range(num_of_modes):
            if self.xm[i] == 0 and self.xn[i] == 0:
                B_00 = self.fourierAmps['Bmod'](s_val)[i]
            elif self.xm[i] != 0:
                sym_chk = self.xn[i]/self.xm[i]
                if sym_chk == 4:
                    Bmn = Bmn + self.fourierAmps['Bmod'](s_val)[i]**2

        return np.sqrt(Bmn)/B_00

    def sym_metric(self, symNum):
        """ Calculate the root sum square of the symmetry breaking
        modes in the boozer spectrum.
        """
        # num_of_modes = self.Bmod.shape[1]
        num_of_modes = self.modes
        Bmn_pres = np.zeros(self.ns)
        Bmn_brek = np.zeros(self.ns)
        for i in range(num_of_modes):
            if self.xm[i] == 0 and self.xn[i] == 0:
                B_00 = self.fourierAmps['Bmod'](self.s_dom)[:, i]
            elif self.xm[i] != 0:
                sym_chk = self.xn[i]/self.xm[i]
                if sym_chk == symNum:
                    Bmn_pres = Bmn_pres + self.fourierAmps['Bmod'](self.s_dom)[:, i]**2
                else:
                    Bmn_brek = Bmn_brek + self.fourierAmps['Bmod'](self.s_dom)[:, i]**2
            else:
                Bmn_brek = Bmn_brek + self.fourierAmps['Bmod'](self.s_dom)[:, i]**2

        return np.sqrt(Bmn_brek) / np.sqrt(Bmn_pres)


    def plot_integral_spectrum(self, plot, nSpec=10, excFirst=True):
        """ Plots the most prominant modes according to the radially
        integrated Fourier amplitudes.  The first mode is excluded by default.

        Parameters
        ----------
        ax : object
            axis on which to plot the spectrum
        ampKey : str
            key specifying Fourier amplitude to be plotted
        nSpec : int
            number of modes to plot (default is 10)
        excFirst : boolean
            defaults to exclude the first mode (default is True)
        """
        roa_dom = np.sqrt(self.s_dom)

        roa_beg = 0.25; roa_end = 0.75
        roa_beg_idx = np.argmin(np.abs(roa_dom - roa_beg))
        roa_end_idx = np.argmin(np.abs(roa_dom - roa_end)) + 1

        roa_dom = roa_dom[roa_beg_idx:roa_end_idx]
        roa_norm = 1. / np.mean(roa_dom)

        specData = {}
        specOrder = []
        for i in range(0, self.modes):
            key = '[{0}, {1}]'.format(int(self.xn[i]), int(self.xm[i]))
            spec = self.Bmod[0::,i]

            spec_avg = np.trapezoid(np.abs(spec[roa_beg_idx:roa_end_idx]), roa_dom) * roa_norm

            specData[key] = spec
            specOrder.append([spec_avg, key])

        if excFirst:
            io=1
        else:
            io=0

        specOrder = sorted(specOrder, key=lambda x: x[0], reverse=True)
        for i in range(nSpec):
            key = specOrder[io+i][1]
            spec = specData[key]

            plot.plt.plot(self.s_dom[self.begIdx:], spec, label=key)

        plot.plt.xlabel(r'$\Psi_N$')
        plot.plt.ylabel(r'$B_{nm}$')

        box = plot.ax.get_position()
        plot.ax.set_position([box.x0, box.y0, box.width * 1, box.height])

        plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1))

    def plot_mode(self, s_val, modes, pol_pnts=251, tor_pnts=1001):
        tor_dom = np.linspace(-np.pi, np.pi, tor_pnts)
        pol_dom = np.linspace(-np.pi, np.pi, pol_pnts)

        pol, tor = np.meshgrid(pol_dom, tor_dom)

        B_mod = 0
        for mode in modes:
            idx_xn = np.where(self.xn == mode[0])[0]
            idx_xm = np.where(self.xm == mode[1])[0]
            idx = np.intersect1d(idx_xn, idx_xm)[0]

            pol_xm = self.xm[idx] * pol
            tor_xn = self.xn[idx] * tor
            cos_mu_nv = np.cos(pol_xm - tor_xn)

            B_mod = B_mod + mode[2] * self.fourierAmps['Bmod'](s_val)[idx] * cos_mu_nv

        return B_mod

    def sym_break_modes(self, psiN, N_mode, M_mode, symNum=4):
        num_of_modes = self.modes
        Bmn = np.zeros(psiN.shape)
        Bmod_model = self.fourierAmps['Bmod']

        for i in range(num_of_modes):
            if self.xm[i] == 0 and self.xn[i] == 0:
                B_00 = Bmod_model(psiN)[i]
            elif self.xm[i] == 0 and self.xn[i] != 0:
                Bmn = Bmn + Bmod_model(psiN)[i]**2
            elif self.xm[i] != 0 and self.xn[i] == 0:
                Bmn = Bmn + Bmod_model(psiN)[i]**2
            else:
                sym_chk = self.xn[i]/self.xm[i]
                if not sym_chk == symNum:
                    Bmn = Bmn + Bmod_model(psiN)[i]**2

        idx_xn = np.where(self.xn == N_mode)[0]
        idx_xm = np.where(self.xm == M_mode)[0]
        idx = np.intersect1d(idx_xn, idx_xm)[0]

        return Bmod_model(psiN)[idx] / np.sqrt(Bmn)

    def sym_preserve_modes(self, psiN, N_mode, M_mode, symNum=4):
        num_of_modes = self.modes
        Bmn = 0
        Bmod_model = self.fourierAmps['Bmod']

        for i in range(num_of_modes):
            if self.xm[i] == 0 and self.xn[i] == 0:
                B_00 = Bmod_model(psiN)[i]
            elif self.xm[i] != 0:
                sym_chk = self.xn[i]/self.xm[i]
                if sym_chk == symNum:
                    Bmn = Bmn + Bmod_model(psiN)[i]**2

        idx_xn = np.where(self.xn == N_mode)[0]
        idx_xm = np.where(self.xm == M_mode)[0]
        idx = np.intersect1d(idx_xn, idx_xm)[0]

        return Bmod_model(psiN)[idx] / np.sqrt(Bmn)

if __name__ == '__main__':
    if False:
        ### Read Boozer Spectra ###
        booz_path = os.path.join('/mnt','HSX_Database','GENE','eps_valley','geomdir','booz_nc','booz_8-8')
        booz = readBooz(booz_path, 'boozmn_wout_HSX_60-1-84.nc')

        modes = [[0, 0, 0],
                [4, 1, 0],
                [8, 2, 0],
                [4, 2, 5]]

        plot = pd.plot_define()
        booz.plot_mode(plot, 0.5, modes)

        plot.plt.tight_layout()
        #plot.plt.show()
        savePath = os.path.join('/home', 'michael', 'onedrive', 'Presentations', '3D_Transport', '2021', '2021-12', '2021-12-08', '60-1-84_42mode_exag.png')
        plot.plt.savefig(savePath)

    if False:
        ### Read Configuration IDs ###
        con_list = os.path.join('/mnt','HSX_Database','GENE','eps_valley','configuration_data.txt')
        with open(con_list, 'r') as f:
            lines = f.readlines()
            conDict = {}
            for line in lines:
                line = line.strip().split(' : ')
                conDict[line[0]] = line[1]

        ### Read TEM Data ###
        tem_path = os.path.join('/mnt','HSX_Database','GENE','eps_valley','data_files','gene_data','TEM_omega.h5')
        with hf.File(tem_path, 'r') as hf_:
            tem_dict = {}
            for key in hf_:
                tem_dict[conDict[key]] = hf_[key][0, 1]

        ### Read Metric Data ###
        met_path = os.path.join('/mnt','HSX_Database','GENE','eps_valley','data_files','metric_data.h5')
        with hf.File(met_path, 'r') as hf_:
            met_dict = {}
            for datum in hf_['metric data']:
                conID = '-'.join(['{0:0.0f}'.format(ID) for ID in datum[0:3]])
                met_dict[conID] = datum[9]

        ### Read Boozer Spectra ###
        booz_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir', 'booz_nc', 'booz_16-16')
        booz_jobs = [j.name for j in os.scandir(booz_path) if j.name.endswith('.nc')]
        npts = len(booz_jobs)

        plot_data = np.empty((npts, 4))
        for jdx, job in enumerate(booz_jobs):
            booz = readBooz(booz_path, job)

            idx_xn = np.where(booz.xn == 0)[0]
            idx_xm = np.where(booz.xm == 0)[0]
            idx = np.intersect1d(idx_xn, idx_xm)[0]
            B00 = np.abs(booz.Bmod[:,idx])

            modes = [[4, 2]]

            Bmn = 0
            for mode in modes:
                idx_xn = np.where(booz.xn == mode[0])[0]
                idx_xm = np.where(booz.xm == mode[1])[0]
                idx = np.intersect1d(idx_xn, idx_xm)[0]
                Bmn = Bmn + booz.Bmod[:,idx]**2

            qhs_red = np.sqrt(Bmn) / B00
            qhs = booz.qhs_metric()

            ### Integrate Amplitude Profile ###
            roa_dom = np.sqrt(booz.s_dom)

            roa_beg = 0.25; roa_end = 0.75
            roa_beg_idx = np.argmin(np.abs(roa_dom - roa_beg))
            roa_end_idx = np.argmin(np.abs(roa_dom - roa_end)) + 1

            roa_dom = roa_dom[roa_beg_idx:roa_end_idx]
            roa_norm = 1. / np.mean(roa_dom)

            plot_data[jdx,2] = np.trapezoid(qhs[roa_beg_idx:roa_end_idx], roa_dom) * roa_norm
            plot_data[jdx,3] = np.trapezoid(qhs_red[roa_beg_idx:roa_end_idx], roa_dom) * roa_norm

            ### Get TEM Data ###
            conID = job.split('_')[-1].split('.')[0]
            plot_data[jdx,0] = met_dict[conID]
            plot_data[jdx,1] = tem_dict[conID]


        plot = pd.plot_define()
        plt = plot.plt

        if False:
            # Symmetry Breaking Modes #
            qhs_max = np.max(plot_data[:,2])
            plt.plot([0, 1.1*qhs_max], [0, 1.1*qhs_max], ls='--', c='k')

            # Elongation Color Bar
            if False:
                plot_data = np.array(sorted(plot_data, key=lambda x: x[0], reverse=False))
                smap = plt.scatter(plot_data[:, 2], plot_data[:, 3], c=plot_data[:, 0], edgecolor='k', marker='o', s=150, cmap='jet')
                cbar = plot.fig.colorbar(smap, ax=plot.ax)
                cbar.ax.set_ylabel(r'$\mathcal{K} / \mathcal{K}^*$')

            # TEM Color Bar
            if True:
                plot_data = np.array(sorted(plot_data, key=lambda x: x[1], reverse=False))
                smap = plt.scatter(plot_data[:, 2], plot_data[:, 3], c=plot_data[:, 1], edgecolor='k', marker='o', s=150, cmap='jet')
                cbar = plot.fig.colorbar(smap, ax=plot.ax)
                cbar.ax.set_ylabel(r'$\gamma (c_s/a)$')

            plt.xlabel(r'$\mathcal{Q}$')
            plt.ylabel(r'$|B_{42}| / B_{00}$')

            plt.xlim(0, 1.1*qhs_max)
            plt.ylim(0, 1.1*qhs_max)

        if False:
            # Symmetry Preserving Modes #
            smap = plt.scatter(plot_data[:, 0], plot_data[:, 3], c=plot_data[:, 1], edgecolor='k', marker='o', s=150, cmap='jet')
            cbar = plot.fig.colorbar(smap, ax=plot.ax)
            cbar.ax.set_ylabel(r'$\gamma (c_s/a)$')

            plt.ylim(0, 1.1 * np.max(plot_data[:, 3]))
            plt.xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
            plt.ylabel(r'$|B_{82}| / B_{00}$')

        plt.grid()
        plt.tight_layout()
        plt.show()

        #saveName = os.path.join('/home', 'michael', 'onedrive', 'Presentations', '3D_Transport', '2021', '2021-12', '2021-12-08', 'B01mode_vs_Elongation.png')
        saveName = os.path.join('/home', 'michael', 'onedrive', 'Presentations', '3D_Transport', '2021', '2021-12', '2021-12-08', 'B01mode_vs_TEMgamma.png')
        #plt.savefig(saveName)

    ### Organize New Boozer Data into Database ###
    if False:
        import shutil

        path_base = os.path.join(dd.exp_ext_base,'queued_in')
        path_list = [f.name for f in os.scandir(path_base) if os.path.isdir(f)]
        for path in path_list:
            main_dirc = 'main_coil_{0}'.format(path.split('_')[2])
            print('Working: '+main_dirc)

            do_path = os.path.join(path_base, path)
            job_path = [f.name for f in os.scandir(do_path) if os.path.isdir(f)]

            booz_data = np.empty((729, 129))
            booz_data[:] = np.nan
            for j, job in enumerate(job_path):
                jobID = float(job.split('_')[1])
                print('    '+job)

                database_path = os.path.join(dd.exp_ext_base,main_dirc,'set_1',job,'booz_spectrum.png')
                booz_path = os.path.join(dd.exp_ext_base,'queued_in',path,job)
                booz = readBooz(booz_path, 'boozmn_wout_HSX_main_opt0.nc')

                data = booz.qhs_metric()
                booz_data[j][0] = jobID
                booz_data[j][3:] = data

                plot = pd.plot_define(fontSize=14, labelSize=16)
                booz.plot_spectrum(plot)
                plot.plt.tight_layout()
                plot.plt.savefig(database_path)

            hf_path = os.path.join(dd.exp_ext_base, main_dirc, 'conData.h5')
            with hf.File(hf_path, 'a') as hf_file:
                hf_file.create_dataset('set_1/qhs_booz', data=booz_data)

            shutil.rmtree(do_path)

    ### Make Single Boozer Spectrum Plot ###
    if True:
        #database_path = os.path.join(dd.pathDrive, 'Presentations', 'Conferences', 'TTF_2021', 'booz_flip14.png')
        basePath = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'boozmn_Flip14_10p0_mn1824_ns101.nc')
        # basePath = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'boozmn_QHS_mn1824_ns101.nc')
        booz = readBooz(basePath)

        plot = pd.plot_define(fontSize=14, labelSize=16)
        plt = plot.plt
        fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(7, 5))

        booz.plot_spectrum(ax)
        ax.set_ylim(ax.get_ylim())
        # ax.plot([0.25, 0.25], ax.get_ylim(), c='k', ls='--', linewidth=1)

        plot.plt.tight_layout()
        # plot.plt.show()
        save_name = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'figures', 'boozer_plot_Flip14.pdf')
        plot.plt.savefig(save_name, format='pdf')

    ### Make Set of Boozer Spectrum Plots ###
    if False:
        path = os.path.join('/home', 'michael', 'Desktop', 'Tokamak_model_kScan', 'scan_1')
        dircs = [j.name for j in os.scandir(path) if os.path.isdir(j)]

        for dirc in dircs:
            print(dirc)

            dircPath = os.path.join(path, dirc)
            savePath = os.path.join(dircPath, 'boozer_spectrum.png')

            bz = readBooz(dircPath, 'boozmn_wout_tok_model_{}.nc'.format(dirc))

            plot = pd.plot_define(fontSize=14, labelSize=plot_spectrum(plot))
            plot.plt.tight_layout()
            plot.plt.savefig(savePath)

    # Plot linear fit to Boozer modes 3D model #
    if False:
        scan_dirc = os.path.join('/home', 'michael', 'Desktop', '3D_chrisModel_kScan', 'scan_2')
        k_vals = [float(f.name[1::]) for f in os.scandir(scan_dirc) if os.path.isdir(f)]
        k_vals = np.array(sorted(k_vals))

        modes = ['20-2']
        mode_dict = {}
        for mode in modes:
            mode_dict[mode] = np.empty(k_vals.shape)

        for kdx, k_val in enumerate(k_vals):
            k_dirc = 'k{0:0.2f}'.format(k_val)
            booz_path = os.path.join(scan_dirc, k_dirc)
            booz_name = 'boozmn_wout_3D_model_'+k_dirc+'.nc'
            booz_full = os.path.join(booz_path, booz_name)

            booz = readBooz(booz_full)

            idx_xn = np.where(booz.xn == 0)[0]
            idx_xm = np.where(booz.xm == 0)[0]
            idx_00 = np.intersect1d(idx_xn, idx_xm)[0]

            idx_xn = np.where(booz.xn == 10)[0]
            idx_xm = np.where(booz.xm == 1)[0]
            idx_N1 = np.intersect1d(idx_xn, idx_xm)[0]

            Bmod_abs = np.abs(booz.Bmod)
            mode_sum = np.sum(Bmod_abs, axis=1) - (Bmod_abs[:, idx_00] + Bmod_abs[:, idx_N1])

            for mode in modes:
                xn = int(mode.split('-')[0])
                xm = int(mode.split('-')[1])

                idx_xn = np.where(booz.xn == xn)[0]
                idx_xm = np.where(booz.xm == xm)[0]
                idx = np.intersect1d(idx_xn, idx_xm)[0]

                mode_dict[mode][kdx] = booz.Bmod[-1, idx]
                # mode_dict[mode][kdx] = Bmod_abs[-1, idx] / mode_sum[-1]

        plot = pd.plot_define(lineWidth=2)
        plt, fig, ax = plot.plt, plot.fig, plot.ax

        ms = ['^', 's', 'o']
        ls = [':', '-.', '--']
        cl = ['tab:green', 'tab:blue', 'tab:red']
        for i, key in enumerate(mode_dict):
            key_split = key.split('-')
            lab = '({}, {})'.format(key_split[0], key_split[1])
            ax.plot(k_vals, mode_dict[key], c='k', markerfacecolor=cl[i], marker=ms[i], markersize=15, ls=ls[i], label=lab)

        ax.set_xlabel(r'$\kappa$')
        ax.set_ylabel(r'$B_{nm}$')
        # ax.set_ylabel(r'$\frac{B_{xy}}{\sum B_{nm} - (B_{00} + B_{10\,1})}$')

        # ax.set_ylim(-.1, .1)
        # ax.set_ylim(0, .5)

        plt.grid()
        plt.legend()
        plt.tight_layout()
        plt.show()

        saveName = os.path.join('/home', 'michael', 'onedrive', 'Presentations', '3D_Transport', '2022', '2022-02', '2022-02-02', '3D_model_modesChris.png')
        # saveName = os.path.join('/home', 'michael', 'onedrive', 'Presentations', '3D_Transport', '2022', '2022-02', '2022-02-02', '3D_model_modes_ampChris.png')
        # plt.savefig(saveName)

    # Plot linear fit to Boozer modes against elongation #
    if False:
        # Read Metric Normalizations #
        norm_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations.h5')
        with hf.File(norm_path, 'r') as hf_:
            kappa_norm = hf_['kappa'][()]

        # Read in Metric Data #
        met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
        with hf.File(met_path, 'r') as hf_file:
            met_data = hf_file['metric data'][()]

        # Read in Boozer Data #
        booz_dirc = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir', 'booz_nc', 'booz_16-16')
        booz_files = [f.name for f in os.scandir(booz_dirc) if f.name.endswith('.nc')]

        modes = ['0-1', '8-1', '8-2']
        mode_dict = {}
        for mode in modes:
            mode_dict[mode] = np.empty(len(booz_files))

        for bdx, booz_file in enumerate(booz_files):
            conID = booz_file.split('_')[-1][0:-3]
            print(conID+': {0}-{1}'.format(bdx+1, len(booz_files)))
            conArr = np.array([int(ID) for ID in conID.split('-')])
            conIdx = np.argmin(np.sum(np.abs(met_data[:, 0:3] - conArr), axis=1))

            booz = readBooz(booz_dirc, booz_file)

            idx_xn = np.where(booz.xn == 0)[0]
            idx_xm = np.where(booz.xm == 0)[0]
            idx_00 = np.intersect1d(idx_xn, idx_xm)[0]

            idx_xn = np.where(booz.xn == 4)[0]
            idx_xm = np.where(booz.xm == 1)[0]
            idx_41 = np.intersect1d(idx_xn, idx_xm)[0]

            Bmod_abs = np.abs(booz.Bmod)
            mode_sum = np.sum(Bmod_abs, axis=1) - (Bmod_abs[:, idx_00] + Bmod_abs[:, idx_41])

            for mode in modes:
                xn = int(mode.split('-')[0])
                xm = int(mode.split('-')[1])

                idx_xn = np.where(booz.xn == xn)[0]
                idx_xm = np.where(booz.xm == xm)[0]
                idx = np.intersect1d(idx_xn, idx_xm)[0]

                mode_dict[mode][conIdx] = np.trapezoid(booz.Bmod[:, idx], booz.s_dom) / (booz.s_dom[-1] - booz.s_dom[0])
                # mode_dict[mode][conIdx] = np.trapezoid(Bmod_abs[:, idx] / mode_sum, booz.s_dom) / (booz.s_dom[-1] - booz.s_dom[0])

        plot = pd.plot_define()
        plt, fig, ax = plot.plt, plot.fig, plot.ax

        ms = ['^', 's', 'o']
        ls = [':', '-.', '--']
        cl = ['tab:green', 'tab:blue', 'tab:red']
        for i, key in enumerate(mode_dict):
            key_split = key.split('-')
            lab = '({}, {})'.format(key_split[0], key_split[1])
            ax.scatter(met_data[:, 9] * kappa_norm, mode_dict[key], facecolor=cl[i], edgecolor='k', marker=ms[i], s=150, label=lab)
        ax.plot([kappa_norm, kappa_norm], [-.01, .01], ls='--', c='k')

        # ax.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
        ax.set_xlabel(r'$\langle \kappa \rangle$')
        ax.set_ylabel(r'$\langle B_{nm} \rangle$')
        # ax.set_ylabel(r'$\langle \frac{B_{xy} }{ \sum B_{nm} - (B_{00} + B_{41}) } \rangle$')

        ax.set_ylim(-.01, .01)
        # ax.set_ylim(0, .2)

        plt.grid()
        plt.legend()
        plt.tight_layout()
        # plt.show()

        saveName = os.path.join('/home', 'michael', 'onedrive', 'Presentations', '3D_Transport', '2022', '2022-02', '2022-02-02', 'HSX_modesChris.png')
        # saveName = os.path.join('/home', 'michael', 'onedrive', 'Presentations', '3D_Transport', '2022', '2022-02', '2022-02-02', 'HSX_mode_ampFalse.png')
        plt.savefig(saveName)
