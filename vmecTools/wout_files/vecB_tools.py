import os, sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch

from scipy.interpolate import griddata
from scipy.interpolate import LinearNDInterpolator

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.coord_convert as cc
import plot_define as pd

class BvecTools:
    """
    A class to analyze the magnitude of the magnetic field
    in VMEC flux coordinate geometry

    ...

    Attributes
    ----------
    B_val : array
        3D array of the component of the magnetic field,
        indexed by {s,v,u} coordinates
    s_dom : array
        1D array of the s coordinate domain
    v_dom : array
        1D array of the v coordinate domain
    u_dom : array
        1D array of the u coordinate domain
    s_num : int
        number of points in s domain
    v_num : int
        number of points in v domain
    u_num : int
        number of points in u domain

    Methods
    -------
    interpMagAxis(r, z)
        Interpolates magnetic axis using r, z grid data.

    paraPlot_B(ax, rEff=1)
        Make Parametric plot of the magnetic field along a flux surface.

    plot_B(fig, ax, r, z, v=0, contMan=None)
        Plots poloidal cross sections of the magnetic field.
    """
    def __init__(self, wout):
        # define internal wout object #
        self.wout = wout

        # import number of grid points in each dimension #
        self.s_num = wout.s_num
        self.v_num = wout.v_num
        self.u_num = wout.u_num

        # import dimensional grid #
        self.s_dom = wout.s_dom
        self.v_dom = wout.v_dom
        self.u_dom = wout.u_dom

        # import R and Z grids #
        # self.R_coord = wout.invFourAmps['R']

        # import number of dimensions #
        for key in wout.invFourAmps:
            self.ndim = wout.invFourAmps[key].ndim
            break

        """
        # define cylindrical grid #
        if self.ndim > 1:
            if self.ndim == 2:
                if self.s_num == 1:
                    self.T_coord = np.empty(self.R_coord.shape)
                    for idx, v in enumerate(wout.v_dom):
                        self.T_coord[idx,:][:] = v
                elif self.v_num == 1:
                    self.T_coord = np.full(self.R_coord.shape, wout.v_dom)
            elif self.ndim == 3:
                self.T_coord = np.empty(self.R_coord.shape)
                for idx, v in enumerate(wout.v_dom):
                    self.T_coord[:,idx,:][:] = v
            else:
                raise ValueError('{} is not an appropriate number of dimesnions.'.format(self.ndim))
            self.coord_data = np.stack((self.R_coord, self.T_coord, self.Z_coord), axis=self.ndim)
        else:
            self.T_coord = wout.v_dom
            self.coord_data = np.array([self.R_coord, self.T_coord, self.Z_coord])
        """

    def compute_toroidal_helical_frame(self):
        """ Calculate the helical reference frame where the radial-like
        direction points in the -Grad(B) direction with the toroidal
        component removed, and the vertical-like direction is perpendiucular
        to the radial and toroidal direction.
        """
        try:
            # import radial coordinate and Jacobian #
            R = self.wout.invFourAmps['R']
            jacob = self.wout.invFourAmps['Jacobian']
            a = self.wout.a_minor*np.sqrt(self.s_dom)
            a = np.repeat(a, self.v_num*self.u_num).reshape(R.shape)

            # import radial derivatives #
            dRds = self.wout.invFourAmps['dR_ds']
            dRdu = self.wout.invFourAmps['dR_du']
            dRdv = self.wout.invFourAmps['dR_dv']

            # import vertical derivatives #
            dZds = self.wout.invFourAmps['dZ_ds']
            dZdu = self.wout.invFourAmps['dZ_du']
            dZdv = self.wout.invFourAmps['dZ_dv']

            # import field strength and derivatives #
            Bmod = self.wout.invFourAmps['Bmod']
            dBds = self.wout.invFourAmps['dBmod_ds']
            dBdu = self.wout.invFourAmps['dBmod_du']
            dBdv = self.wout.invFourAmps['dBmod_dv']
        except KeyError:
            raise KeyError('Must include the following list in AmpKeys: '+
                    'R, Jacobian, dR_ds, dR_du, dR_dv, dZ_ds, dZ_du, dZ_dv, '+
                    'Bmod', 'dBmod_ds, dBmod_du, dBmod_dv')

        # compute repeated expresseions #
        R_inv = (1./R)
        B_inv = (1./Bmod)
        jacob_inv = (1./jacob)
        jacob2_inv = jacob_inv**2
        RvZu = dRdv*dZdu-dRdu*dZdv
        RsZv = dRds*dZdv-dRdv*dZds

        # compute contravariant metric components #
        gss = jacob2_inv*(RvZu*RvZu+R*R*(dZdu*dZdu+dRdu*dRdu))
        guu = jacob2_inv*(RsZv*RsZv+R*R*(dZds*dZds+dRds*dRds))
        gvv = R_inv*R_inv
        gsu = jacob2_inv*(RvZu*RsZv-R*R*(dZds*dZdu+dRds*dRdu))
        guv = R_inv*jacob_inv*RsZv
        gvs = R_inv*jacob_inv*RvZu

        # compute covariant metric components #
        g_uv = dRdu*dRdv+dZdu*dZdv
        g_vs = dRdv*dRds+dZdv*dZds

        # compute repeated expression #
        gradBv = dBds*gvs+dBdu*guv+dBdv*gvv

        # compute contravariant components of helical basis vectors #
        Rs = -a*B_inv*(dBds*gss+dBdu*gsu+dBdv*gvs)
        Ru = -a*B_inv*(dBds*gsu+dBdu*guu+dBdv*guv)
        Zs = -(a**2)*B_inv*jacob_inv*(dBdu-gradBv*g_uv)
        Zu = (a**2)*B_inv*jacob_inv*(dBds-gradBv*g_vs)

        # compute cylindrical components of helical basis vectors #
        Rr = Rs*dRds+Ru*dRdu
        Rz = Rs*dZds+Ru*dZdu
        Zr = Zs*dRds+Zu*dRdu
        Zz = Zs*dZds+Zu*dZdu

        # normalize helical basis vectors #
        R_norm_inv = 1./np.sqrt(Rr*Rr+Rz*Rz)
        Z_norm_inv = 1./np.sqrt(Zr*Zr+Zz*Zz)
        Rr_norm = Rr*R_norm_inv
        Rz_norm = Rz*R_norm_inv
        Zr_norm = Zr*Z_norm_inv
        Zz_norm = Zz*Z_norm_inv

        # return helical vectors #
        R_vec = np.stack((Rr_norm, Rz_norm), axis=self.ndim)
        Z_vec = np.stack((Zr_norm, Zz_norm), axis=self.ndim)
        return R_vec, Z_vec

    def compute_xi_helical_frame(self, N=4., coord='cartesian', list_method=False):
        """ Calculate the helical reference frame where the radial-like
        direction points in the -Grad(B) direction with the component in the
        symmetry direction removed, and the vertical-like direction is
        perpendiucular to the radial and symmetry direction.

        Parameters
        ----------
        N : float, optional
            Defines the symmetry mode in the magnetic field strength. Default is 4.
        coord : str
            Determines the coordinate system in which the basis vectors are 
            returned. Default is cylindrical, but can also be in VMEC coordinates.
        """
        try:
            # import radial coordinate and Jacobian #
            R = self.wout.invFourAmps['R']
            jacob = self.wout.invFourAmps['Jacobian']
            a = self.wout.a_minor*np.sqrt(self.s_dom)
            if list_method == False:
                a = np.repeat(a, self.v_num*self.u_num).reshape(R.shape)

            # import radial derivatives #
            dRds = self.wout.invFourAmps['dR_ds']
            dRdu = self.wout.invFourAmps['dR_du']
            dRdv = self.wout.invFourAmps['dR_dv']

            # import vertical derivatives #
            dZds = self.wout.invFourAmps['dZ_ds']
            dZdu = self.wout.invFourAmps['dZ_du']
            dZdv = self.wout.invFourAmps['dZ_dv']

            # import lambda derivates #
            dLds = self.wout.invFourAmps['dL_ds']
            dLdu = self.wout.invFourAmps['dL_du']
            dLdv = self.wout.invFourAmps['dL_dv']

            # import field strength and derivatives #
            Bmod = self.wout.invFourAmps['Bmod']
            dBds = self.wout.invFourAmps['dBmod_ds']
            dBdu = self.wout.invFourAmps['dBmod_du']
            dBdv = self.wout.invFourAmps['dBmod_dv']
        except KeyError:
            raise KeyError('Must include the following list in AmpKeys: '+
                    'R, Jacobian, dR_ds, dR_du, dR_dv, dZ_ds, dZ_du, dZ_dv, '+
                    'dL_ds', 'dL_du', 'dL_dv', 'Bmod', 'dBmod_ds, dBmod_du, dBmod_dv')

        # compute metric components #
        gss, guu, gvv, gsu, guv, gvs = cc.compute_metric(self.wout, basis='contravariant')
        g_ss, g_uu, g_vv, g_su, g_uv, g_vs = cc.compute_metric(self.wout, basis='covariant')

        # compute repeated expression #
        gradBs = dBds*gss + dBdu*gsu + dBdv*gvs
        gradBu = dBds*gsu + dBdu*guu + dBdv*guv
        gradBv = dBds*gvs + dBdu*guv + dBdv*gvv

        # compute inverse \xi and \eta partial derivative components #
        Ls = dLds
        Lu = 1+dLdu
        Lve = dLdv - N
        Lvx = dLdv + (1./N)

        # compute contravariant components of radial basis vector #
        Rsp = -(a/Bmod)*(gradBs - (Lu/Ls)*gradBu)
        Re = (a/Bmod)*(N/((N*N)+1))*Lve*(Lu*gradBu - Lvx*gradBv)

        # compute contravariant components of vertical basis vector #
        g_xs = (g_ss/Ls) + (g_su/Lu) + (g_vs/Lvx)
        g_ex = (g_ss/(Ls*Ls)) + (g_uu/(Lu*Lu)) + (g_vv/(Lve*Lvx)) + 2*(g_su/(Ls*Lu)) + 2*(dLdv/(Lvx*Lve))*((g_uv/Lu) + (g_vs/Ls))
        dBde = (dBds/Ls) + (dBdu/Lu) + (dBdv/Lve)
        gradBx = (N/((N*N)+1))*Lvx*(Lu*gradBu - Lve*gradBv)
        Zsp = -((a*a)/(Bmod*jacob))*(dBde - gradBx*g_ex)
        Ze = ((a*a)/(Bmod*jacob))*(dBds - gradBx*g_xs)

        if coord == 'cartesian':
            # compute cylindrical components of basis vectors #
            dRde = (dRds/Ls) + (dRdu/Lu) + (dRdv/Lve)
            dZde = (dZds/Ls) + (dZdu/Lu) + (dZdv/Lve)
            Rr = Rsp*dRds + Re*dRde
            Rp = (R*Re/Lve)
            Rz = Rsp*dZds + Re*dZde
            Zr = Zsp*dRds + Ze*dRde
            Zp = (R*Ze/Lve)
            Zz = Zsp*dZds + Ze*dZde

            # convert to cartesian components #
            Rx = Rr*np.cos(self.v_dom)-Rp*np.sin(self.v_dom)
            Ry = Rr*np.sin(self.v_dom)+Rp*np.cos(self.v_dom)
            Zx = Zr*np.cos(self.v_dom)-Zp*np.sin(self.v_dom)
            Zy = Zr*np.sin(self.v_dom)+Zp*np.cos(self.v_dom)

            # return helical vectors #
            R_vec = np.stack((Rx, Ry, Rz), axis=self.ndim)
            Z_vec = np.stack((Zx, Zy, Zz), axis=self.ndim)
            return R_vec, Z_vec

        elif coord == 'cylindrical':
            # compute cylindrical components of helical basis vectors #
            dRde = (dRds/Ls) + (dRdu/Lu) + (dRdv/Lve)
            dZde = (dZds/Ls) + (dZdu/Lu) + (dZdv/Lve)
            Rr = Rsp*dRds + Re*dRde
            Rp = (R*Re/Lve)
            Rz = Rsp*dZds + Re*dZde
            Zr = Zsp*dRds + Ze*dRde
            Zp = (R*Ze/Lve)
            Zz = Zsp*dZds + Ze*dZde

            # return helical vectors #
            R_vec = np.stack((Rr, Rp, Rz), axis=self.ndim)
            Z_vec = np.stack((Zr, Zp, Zz), axis=self.ndim)
            return R_vec, Z_vec

        elif coord == 'vmec':
            # compute vmec components of helical basis vectors #
            Rs = Rsp + (Re/Ls)
            Ru = Re/Lu
            Rv = Re/Lve
            Zs = Zsp + (Ze/Ls)
            Zu = Ze/Lu
            Zv = Ze/Lve

            # return helical vectors #
            R_vec = np.stack((Rs, Ru, Rv), axis=self.ndim)
            Z_vec = np.stack((Zs, Zu, Zv), axis=self.ndim)
            return R_vec, Z_vec

    def compute_average_toroidal_helical_frame(self, R_vec, Z_vec, u_dom):
        """ Calculate the poloidally averaged helical basis vectors.

        Parameters
        ----------
        R_vec : 3D arr
            The helical basis vector in the radial-like direction.
        Z_vec : 3D arr
            The helical basis vector in the vertical-like direction.
        u_dom : 1D arr
            Polidal angles over which to perform integration
        """
        # compute average of basis vector components #
        g_uu_sqrt = np.sqrt((self.wout.invFourAmps['dR_du']**2) + (self.wout.invFourAmps['dZ_du']**2))
        theta_norm_inv = 1./np.trapz(g_uu_sqrt, u_dom, axis=1)
        R1 = np.trapz(R_vec[:,:,0]*g_uu_sqrt, u_dom, axis=1)*theta_norm_inv
        R2 = np.trapz(R_vec[:,:,1]*g_uu_sqrt, u_dom, axis=1)*theta_norm_inv
        Z1 = np.trapz(Z_vec[:,:,0]*g_uu_sqrt, u_dom, axis=1)*theta_norm_inv
        Z2 = np.trapz(Z_vec[:,:,1]*g_uu_sqrt, u_dom, axis=1)*theta_norm_inv

        # normalize basis vector components #
        R_norm_inv = 1./np.sqrt(R1*R1+R2*R2)
        Z_norm_inv = 1./np.sqrt(Z1*Z1+Z2*Z2)
        R1_norm = R1*R_norm_inv
        R2_norm = R2*R_norm_inv
        Z1_norm = Z1*Z_norm_inv
        Z2_norm = Z2*Z_norm_inv

        # return averaged basis vectors #
        R_avg_vec = np.stack((R1_norm, R2_norm), axis=1)
        Z_avg_vec = np.stack((Z1_norm, Z2_norm), axis=1)
        return R_avg_vec, Z_avg_vec

    def compute_average_xi_helical_frame(self, R_vec, Z_vec, eta_dom, N=4.):
        """ Calculate the poloidally averaged helical basis vectors.

        Parameters
        ----------
        R_vec : 3D arr
            The helical basis vector in the radial-like direction.
        Z_vec : 3D arr
            The helical basis vector in the vertical-like direction.
        eta_dom : 1D arr
            Eta angles over which to perform integration
        """
        # compute metric components #
        g_ss, g_uu, g_vv, g_su, g_uv, g_vs = cc.compute_metric(self.wout, basis='covariant')

        # compute inverse \xi and \eta partial derivative components #
        vmec = self.wout.invFourAmps
        Ls = (1./vmec['dL_ds'])
        Lu = (1./(1+vmec['dL_du']))
        Lv = (1./(vmec['dL_dv'] - N))
        g_ee = Ls*Ls*g_ss + Lu*Lu*g_uu + Lv*Lv*g_vv + 2*Ls*Lu*g_su + 2*Ls*Lv*g_vs + 2*Lu*Lv*g_uv
        g_ee_sqrt = np.sqrt(g_ee)

        # compute average of basis vector components #
        eta_norm_inv = 1./np.trapz(g_ee_sqrt, eta_dom)
        R1 = np.trapz(R_vec[:,0]*g_ee_sqrt, eta_dom)*eta_norm_inv
        R2 = np.trapz(R_vec[:,1]*g_ee_sqrt, eta_dom)*eta_norm_inv
        R3 = np.trapz(R_vec[:,2]*g_ee_sqrt, eta_dom)*eta_norm_inv
        Z1 = np.trapz(Z_vec[:,0]*g_ee_sqrt, eta_dom)*eta_norm_inv
        Z2 = np.trapz(Z_vec[:,1]*g_ee_sqrt, eta_dom)*eta_norm_inv
        Z3 = np.trapz(Z_vec[:,2]*g_ee_sqrt, eta_dom)*eta_norm_inv

        # normalize basis vector components #
        R_norm_inv = 1./np.sqrt(R1*R1+R2*R2+R3*R3)
        Z_norm_inv = 1./np.sqrt(Z1*Z1+Z2*Z2+Z3*Z3)
        R1_norm = R1*R_norm_inv
        R2_norm = R2*R_norm_inv
        R3_norm = R3*R_norm_inv
        Z1_norm = Z1*Z_norm_inv
        Z2_norm = Z2*Z_norm_inv
        Z3_norm = Z3*Z_norm_inv

        # return averaged basis vectors #
        R_avg_vec = np.array([R1, R2, R3])
        Z_avg_vec = np.array([Z1, Z2, Z3])
        return R_avg_vec, Z_avg_vec

    def compute_shaping_parameters(self, X_ma, X_surf, R_vec, Z_vec, p_set=[2,3,4]):
        """ Compute the shaping parameters in the provided cross sections. Shaping parameters
        are returned across all provided cross sections.

        Parameters
        ----------
        X_ma : 2D arr
            Magnetic axis is cylindrical coordinates. The first axis is over toroidal angle.
            The second axis provide R, Z points.
        X_surf : 3D arr
            Flux surface in each toroidal cross section. The first axis is over toroidal angle.
            The second axis if over the poloidal angle. The third axis provides R, Z points.
        R_vec : 2D arr
            Helical radial-like basis vector. The first axis is over toroidal angle. The second
            axis provides R, Z points.
        Z_vec : 2D arr
            Helical vertical-like basis vector. The first axis is over toroidal angle. The second
            axis provides R, Z points.
        p_set : list, optional
            A list of shaping parameters to evaluate. Default is [2, 3, 4], which corresponds to
            elongation, triangularity, and squareness, respectively.
        """
        # define minor radius of flux surfaces #
        X_ma = np.repeat(X_ma, X_surf.shape[1], axis=0).reshape(X_ma.shape[0], X_surf.shape[1], 2)
        X = X_surf-X_ma
        X_norm = np.linalg.norm(X, axis=2)

        # compute theta mapping #
        R_vec = np.repeat(R_vec, X_surf.shape[1], axis=0).reshape(R_vec.shape[0], X_surf.shape[1], 2)
        Z_vec = np.repeat(Z_vec, X_surf.shape[1], axis=0).reshape(Z_vec.shape[0], X_surf.shape[1], 2)
        X_dot_Z = np.sum(X*Z_vec, axis=2)
        X_dot_R = np.sum(X*R_vec, axis=2)
        theta = np.arctan2(X_dot_Z, X_dot_R)
        shaping_parameters = np.empty((theta.shape[0], len(p_set)))
        for i, the in enumerate(theta):
            the_map = np.stack((the, X_norm[i]), axis=1)
            the_sort = np.array(sorted(the_map, key=lambda x: x[0]))
            the = the_sort[:,0]
            X2_the = the_sort[:,1]**2
            X2_norm_inv = 1./np.trapz(X2_the, the)
            for j, p in enumerate(p_set):
                shape = ((-1)**(p-1))*np.trapz(X2_the*np.cos(p*the), the)*X2_norm_inv
                shaping_parameters[i,j] = shape

        return shaping_parameters

    def interpMagAxis(self, r, z):
        """ Interpolates magnetic axis using r, z grid data.

        Parameters
        ----------
        r : array
            3D array of r values in B_val grid
        z : array
            3D array of z values in B_val grid

        Raises
        ------
        ValueError
            Given the shape of B_val, the magnetic axis is undetermined.
        """
        if (self.v_num!=1) and (self.s_num!=1) and (self.u_num!=1):
            for v_idx in range(self.v_num):
                Bmap = self.B_val[0::,v_idx,0::]

                r_grd = r[0::,v_idx,0::]
                z_grd = z[0::,v_idx,0::]

                z_grd_mask = z_grd[1::,0::].flatten()
                r_grd_mask = r_grd[1::,0::].flatten()

                Bmap_mask = Bmap[1::,0::].flatten()

                Binterp = griddata((r_grd_mask, z_grd_mask), Bmap_mask, (r_grd[0,0], z_grd[0,0]))

                self.B_val[0,v_idx,0::] = Binterp

        elif (self.v_num==1) and (self.s_num!=1) and (self.u_num!=1):
            Bmap = self.B_val[0::,0::]

            z_grd_mask = z[1::,0::].flatten()
            r_grd_mask = r[1::,0::].flatten()

            Bmap_mask = Bmap[1::,0::].flatten()

            Binterp = griddata((r_grd_mask, z_grd_mask), Bmap_mask, (r[0,0], z[0,0]))

            self.B_val[0,0::] = Binterp

        else:
            raise ValueError('Cannont interperate magnetic axis from shape {}.'.format(self.B_val.shape))

    def paraPlot_B(self, fig, ax, rEff=1):
        """ Make Parametric plot of the magnetic field along a flux surface.

        Parameters
        ----------
        fig : object
            figure on which to place colorbar
        ax : object
            axis on which to plot parametric field
        rEff : float
            Effective radius (r/a) of flux surface to plot (default is 1)

        Raises
        ------
        ValueError
            The shape of B_val does not allow for a parametric plot.
        """
        B_min = np.min(self.B_val[np.nonzero(self.B_val)])
        B_max = np.max(self.B_val)

        if (self.s_num == 1) and (self.v_num != 1) and (self.u_num != 1):
            B_show = self.B_val.T

        elif (self.s_num != 1) and (self.v_num != 1) and (self.u_num != 1):
            s_idx = np.argmin(np.abs(self.s_dom - rEff))
            B_show = self.B_val[s_idx, 0::, 0::].T

        else:
            raise ValueError('Cannont make parametric plot with B_val shape {}.'.format(self.B_val.shape))

        v_dom_norm = self.v_dom/np.pi - 1
        u_dom_norm = self.u_dom/np.pi - 1
        s_map = ax.pcolormesh(v_dom_norm, u_dom_norm, B_show, vmin=B_min, vmax=B_max, cmap=plt.get_cmap('jet'))

        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'$|\mathbf{B}|$')

        ax.set_xlabel(r'tor [$\pi$]')
        ax.set_ylabel(r'pol [$\pi$]')

        ax.set_title(r'$\Psi_N = $'+'{0:0.2f}'.format(rEff))
        # ax.set_title(r'$r/a = $'+'{0:0.1f}'.format(np.sqrt(rEff)))

    def plot_B(self, fig, ax, r, z, vLst=[0], contMan=None):
        """ Plots poloidal cross sections of the magnetic field.

        Parameters
        ----------
        fig : object
            figure object on which the colorbar is constructed
        ax : object
            axis object on which to construct color map
        r : array
            3D array of r values that maps to B_val
        z : array
            3D array of z values that maps to B_val
        vLst : list
            Toroidal angles of cross section (default is [0])
        contMan : list
            List of contour levels to plot manually (default is None)

        Raises
        ------
        ValueError
            The shape of B_val does not allow for a poloidal cross section.
        """
        B_min = np.min(self.B_val[np.nonzero(self.B_val)])
        B_max = np.max(self.B_val)

        if (self.v_num != 1) and (self.s_num != 1) and (self.u_num != 1):
            for v in vLst:
                v_ind = np.argmin(np.abs(self.v_dom - v))

                rMA = r[0, v_ind, 0]
                zMA = z[0, v_ind, 0]

                r_show = r[0::, v_ind, 0::]
                z_show = z[0::, v_ind, 0::]
                B_show = self.B_val[0::, v_ind, 0::]

                lvls = [self.B_val[0, v_ind, 0], self.B_val[30, v_ind, 0], self.B_val[30, v_ind,int(.5*self.u_num)]]

                if contMan:
                    lvls.extend(contMan)
                lvls = sorted(lvls)

                s_map = ax.pcolormesh(r_show, z_show, B_show, vmin=B_min, vmax=B_max, cmap=plt.get_cmap('jet'))
                ax.plot(r_show[-1, 0::], z_show[-1, 0::], linewidth=3, c='k')
                ax.scatter([rMA], [zMA], c='k', marker='X', s=100, zorder=10)

                CS = ax.contour(r_show, z_show, B_show, lvls, colors='k', linewidths=1)
                ax.clabel(CS, inline=5, fontsize=16)

                ax.plot(r_show[-1, 0::], z_show[-1, 0::], c='k')

            cbar = fig.colorbar(s_map, ax=ax)
            cbar.ax.set_ylabel(r'$|\mathbf{B}|$')

        elif (self.v_num == 1) and (self.s_num != 1) and (self.u_num != 1):
            rMA = r[0, 0]
            zMA = z[0, 0]

            lvls = [self.B_val[0, 0], self.B_val[30, 0], self.B_val[30, int(.5*self.u_num)]]

            if contMan:
                lvls.extend(contMan)
            lvls = sorted(lvls)

            s_map = ax.pcolormesh(r, z, self.B_val[0::, 0::], vmin=B_min, vmax=B_max, cmap=plt.get_cmap('jet'))
            ax.plot(r[-1, 0::], z[-1, 0::], linewidth=3, c='k')
            ax.scatter([rMA], [zMA], c='k', marker='X', s=100, zorder=10)

            # cbar = fig.colorbar(s_map, ax=ax)
            # cbar.ax.set_ylabel(r'$B$ (T)')

            # CS = ax.contour(r, z, self.B_val[0::, 0::], lvls, colors='k', linewidths=1)
            # ax.clabel(CS, inline=5, fontsize=16)

            ax.plot(r[-1, 0::], z[-1, 0::], c='k')

        else:
            raise ValueError('Cannont plot poloidal cross section with B_val shape {}.'.format(self.B_val.shape))


if __name__ == "__main__":
    # equilibrium grids #
    u_dom = np.linspace(-np.pi, np.pi, 101)
    v_dom = np.linspace(-np.pi, np.pi, 4*u_dom.shape[0])
    # amp_keys = ['R', 'Z', 'Bmod', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'dBmod_ds', 'dBmod_du', 'dBmod_dv']
    amp_keys = ['R', 'Lambda']

    # compute equilibrium grids #
    # wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
    wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)
    wout.transForm_2D_sSec(0.5, u_dom, v_dom, amp_keys)
    vecB = BvecTools(wout)
    u_fit, v_fit = vecB.invert_symmetry_coordinates()

    # construct cross sections #
    xi_dom = np.linspace(-np.pi, np.pi, 101)
    eta_dom = np.linspace(-np.pi, np.pi, 101)
    eta_mesh, xi_mesh = np.meshgrid(eta_dom, xi_dom, indexing='ij')

    u_dom = u_fit(eta_mesh, xi_mesh)
    v_dom = v_fit(eta_mesh, xi_mesh)
    s_dom = np.full(u_dom.shape[0], 0.5)
    vmec_points = np.stack((s_dom, u_dom[:,0], v_dom[:,0]), axis=1)
    wout.transForm_listPoints(vmec_points, ['R', 'Z'])
    
    R = wout.invFourAmps['R']
    phi = v_dom[:,0]
    Z = wout.invFourAmps['Z']

    print(phi)
    print(np.mean(phi))

    if False:
        # plotting parameters #
        plot = pd.plot_define(fontSize=14, labelSize=16)
        plt = plot.plt
        fig, axs = plt.subplots(1, 2, tight_layout=True, figsize=(16, 6))
        ax1, ax2 = axs[0], axs[1]

        # plot data #
        cmap = 'magma'
        ax1.contour(v_dom/np.pi, u_dom/np.pi, u_fit(eta, xi).T, levels=20, cmap='jet')
        smap1 = ax1.contour(v_dom/np.pi, u_dom/np.pi, eta.T/np.pi, levels=20, cmap=cmap)
        ax2.contour(v_dom/np.pi, u_dom/np.pi, v_fit(eta, xi).T, levels=20, cmap='jet')
        smap2 = ax2.contour(v_dom/np.pi, u_dom/np.pi, xi.T/np.pi, levels=20, cmap=cmap)

        cbar1 = fig.colorbar(smap1, ax=ax1)
        cbar1.ax.set_ylabel(r'$\eta \ /\ \pi$')

        cbar2 = fig.colorbar(smap2, ax=ax2)
        cbar2.ax.set_ylabel(r'$\xi \ / \ \pi$')

        for ax in axs.flat:
            ax.set_xlabel(r'$\zeta \ / \ \pi$')
            ax.set_ylabel(r'$\theta \ / \ \pi$')
            ax.set_xlim(-1, 1)
            ax.set_ylim(-1, 1)

        plt.show()
