# -*- coding: utf-8 -*-
"""
Created on Mon Sep 21 23:04:48 2020

@author: micha
"""

import numpy as np

import wout_read as wr
import curveB_tools as cBT
import coord_convert as cc

import os, sys
path = os.path.dirname(os.path.dirname(os.path.abspath(os.getcwd())))
sys.path.append(path)

import plot_define as pd
import directory_dict as dd


def plot_Bk_overlap(conIDs, plot, plotDict, uPts=11, basePath=dd.exp_ext_base):
    cnt = 0
    counts = {}
    Bk_O = {}
    for key in conIDs:
        cnt = len(conIDs[key])
        counts[key] = cnt
        if key == 'hsx':
            Bk_O[key] = np.empty((cnt+1, 128))
            Bk_O[key][0] = np.linspace(0,1,128) 
        elif key == 'w7x':
            Bk_O[key] = np.empty((cnt+1, 99))
            Bk_O[key][0] = np.linspace(0,1,99)
        elif key == 'itos':
            Bk_O[key] = np.empty((cnt+1, 99))
            Bk_O[key][0] = np.linsapce(0,1,99)
        else:
            raise KeyError(key+' is not currently an available experimental geometry.')
    
    keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs', 'Bu', 'Bv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']
    
    for key in conIDs:
        if key == 'hsx':
            print('HSX')
            u_dom = np.linspace(0,2*np.pi,uPts)
            v_dom = np.linspace(0,2*np.pi,4*uPts)
            for idx in range(counts[key]):
                conID = conIDs[key][idx]
                print('   '+conID)
                IDs = conID.split('-')
                
                mainID = 'main_coil_{}'.format(IDs[0])
                setID = 'set_{}'.format(IDs[1])
                jobID = 'job_{}'.format(IDs[2])
                
                path = os.path.join(basePath,mainID,setID,jobID)
                if os.path.isfile(os.path.join(path,'wout_HSX_main_opt0.nc')):
                    name = 'wout_HSX_main_opt0.nc'
                elif os.path.isfile(os.path.join(path,'wout_HSX_aux_opt0.nc')):
                    name = 'wout_HSX_aux_opt0.nc'
                else:
                    raise IOError('No usable wout file in '+path)
                            
                wout = wr.readWout(path, name=name, diffAmps=True, curvAmps=True)
                wout.transForm_3D(u_dom, v_dom, keys)
                
                curB = cBT.BcurveTools(wout)
                curB.calc_curvature()
                curB.calc_Bk_overlap()
                
                Bk_O[key][idx+1] = curB.Bk_overlap
        
        if key == 'w7x':
            print('W7X')
            u_dom = np.linspace(0,2*np.pi,uPts)
            v_dom = np.linspace(0,2*np.pi,5*uPts)
            for idx in range(counts[key]):
                path = os.path.join(dd.exp_local['W7X'],'coil_configs')
                conID = conIDs[key][idx]
                print('   ref_'+conID)
                
                if conID=='168':
                    name = 'w7x_ref_168_wout_beta_0.0.nc'
                elif conID=='13':
                    name = 'W7X_wout_ref_13_high_beta.nc'
                else:
                    raise NameError('ref_'+conID+' is not currently available for W7-x geomertires.')
                
                wout = wr.readWout(path, name=name, diffAmps=True, curvAmps=True)
                wout.transForm_3D(u_dom, v_dom, keys)
                
                curB = cBT.BcurveTools(wout)
                curB.calc_curvature()
                curB.calc_Bk_overlap()
                
                Bk_O[key][idx+1] = curB.Bk_overlap
    
    for key in Bk_O:
        for i in range(len(Bk_O[key][1::])):
            pltD = {}
            for plot_key in plotDict[key]:
                pltD[plot_key] = plotDict[key][plot_key][i]
                
            plot.plt.plot(Bk_O[key][0][5:-1], Bk_O[key][i+1][5:-1]*1e4, **pltD)
    
    plot.plt.xlabel(r'$\Psi_n$')
    plot.plt.ylabel(r'$\mathcal{O}$ [cm$^{-2}$]')
    
    plot.plt.ylim(0, 14)
    #plot.plt.yscale('log')
    
    for key in plotDict:
        if 'label' in plotDict[key]:
            box = plot.ax.get_position()
            plot.ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
        
            plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1), fontsize=16)
            break
    
    plot.plt.grid()
    plot.plt.tight_layout()  
    
def plot_drift(conIDs, plot, plotDict, uPts=11, basePath=dd.exp_ext_base):   
    cnt = 0
    counts = {}
    drift = {}
    for key in conIDs:
        cnt = len(conIDs[key])
        counts[key] = cnt
        if key == 'hsx':
            drift[key] = np.empty((cnt+1, 128))
            drift[key][0] = np.linspace(0,1,128) 
        elif key == 'w7x':
            drift[key] = np.empty((cnt+1, 99))
            drift[key][0] = np.linspace(0,1,99)
        elif key == 'itos':
            drift[key] = np.empty((cnt+1, 99))
            drift[key][0] = np.linsapce(0,1,99)
        else:
            raise KeyError(key+' is not currently an available experimental geometry.')
    
    keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs', 'Bu', 'Bv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']
    
    for key in conIDs:
        if key == 'hsx':
            print('HSX')
            u_dom = np.linspace(0,2*np.pi,uPts)    
            v_dom = np.linspace(0,2*np.pi,4*uPts)
            for idx in range(counts[key]):
                conID = conIDs[key][idx]
                print('   '+conID)
                IDs = conID.split('-')
                
                mainID = 'main_coil_{}'.format(IDs[0])
                setID = 'set_{}'.format(IDs[1])
                jobID = 'job_{}'.format(IDs[2])
                
                path = os.path.join(basePath,mainID,setID,jobID)
                if os.path.isfile(os.path.join(path,'wout_HSX_main_opt0.nc')):
                    name = 'wout_HSX_main_opt0.nc'
                elif os.path.isfile(os.path.join(path,'wout_HSX_aux_opt0.nc')):
                    name = 'wout_HSX_aux_opt0.nc'
                else:
                    raise IOError('No usable wout file in '+path)
                            
                wout = wr.readWout(path, name=name, diffAmps=True, curvAmps=True)
                wout.transForm_3D(u_dom, v_dom, keys)
                
                curB = cBT.BcurveTools(wout)
                curB.calc_drift()
                
                drift[key][idx+1] = curB.drift
                
        if key == 'w7x':
            print('W7X')
            u_dom = np.linspace(0,2*np.pi,uPts)
            v_dom = np.linspace(0,2*np.pi,5*uPts)
            for idx in range(counts[key]):
                path = os.path.join(dd.exp_local['W7X'],'coil_configs')
                conID = conIDs[key][idx]
                print('   ref_'+conID)
                
                if conID=='168':
                    name = 'w7x_ref_168_wout_beta_0.0.nc'
                elif conID=='13':
                    name = 'W7X_wout_ref_13_high_beta.nc'
                else:
                    raise NameError('ref_'+conID+' is not currently available for W7-x geomertires.')
                
                wout = wr.readWout(path, name=name, diffAmps=True, curvAmps=True)
                wout.transForm_3D(u_dom, v_dom, keys)
                
                curB = cBT.BcurveTools(wout)
                curB.calc_drift()
                
                drift[key][idx+1] = curB.drift
                
        if key == 'itos':
            print('ITOS')
            u_dom = np.linspace(0,2*np.pi,uPts)
            v_dom = np.linspace(0,2*np.pi,5*uPts)
            for idx in range(counts[key]):
                path = os.path.join(dd.exp_local['ITOS'],'configurations','BaseCase')
                conID = conIDs[key][idx]
                
                wout = wr.readWout(path, name='wout_ITOS.nc', diffAmps=True, curvAmps=True)
                wout.transForm_3D(u_dom, v_dom, keys)
                
                curB = cBT.BcurveTools(wout)
                curB.calc_drift()

                drift[key][idx+1] = curB.drift
                
    for key in drift:
        for i in range(len(drift[key][1::])):
            pltD = {}
            for plot_key in plotDict[key]:
                pltD[plot_key] = plotDict[key][plot_key][i]
            
            plot.plt.plot(drift[key][0][5:-1], drift[key][i+1][5:-1], **pltD)
    
    plot.plt.xlabel(r'$\Psi_n$')
    plot.plt.ylabel(r'$\langle\mathcal{D}\rangle$')
    
    #plot.plt.yscale('log')
    
    for key in plotDict:
        if 'label' in plotDict[key]:
            box = plot.ax.get_position()
            plot.ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
        
            plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1), fontsize=16)
            break
    
    plot.plt.grid()
    plot.plt.tight_layout()              
                 
    
def plot_Gss_metric(conIDs, plot, plotDict, uPts=11, basePath=dd.exp_ext_base):
    cnt = 0
    counts = {}
    gss = {}
    for key in conIDs:
        cnt = len(conIDs[key])
        counts[key] = cnt
        if key == 'hsx':
            gss[key] = np.empty((cnt+1, 128))
            gss[key][0] = np.linspace(0,1,128) 
        elif key == 'w7x':
            gss[key] = np.empty((cnt+1, 99))
            gss[key][0] = np.linspace(0,1,99)
        elif key == 'itos':
            gss[key] = np.empty((cnt+1, 99))
            gss[key][0] = np.linsapce(0,1,99)
        else:
            raise KeyError(key+' is not currently an available experimental geometry.')
    
    keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs', 'Bu', 'Bv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']
    
    for key in conIDs:
        if key == 'hsx':
            print('HSX')
            u_dom = np.linspace(0,2*np.pi,uPts)    
            v_dom = np.linspace(0,2*np.pi,4*uPts)
            for idx in range(counts[key]):
                conID = conIDs[key][idx]
                print('   '+conID)
                IDs = conID.split('-')
                
                mainID = 'main_coil_{}'.format(IDs[0])
                setID = 'set_{}'.format(IDs[1])
                jobID = 'job_{}'.format(IDs[2])
                
                path = os.path.join(basePath,mainID,setID,jobID)
                if os.path.isfile(os.path.join(path,'wout_HSX_main_opt0.nc')):
                    name = 'wout_HSX_main_opt0.nc'
                elif os.path.isfile(os.path.join(path,'wout_HSX_aux_opt0.nc')):
                    name = 'wout_HSX_aux_opt0.nc'
                else:
                    raise IOError('No usable wout file in '+path)
                            
                wout = wr.readWout(path, name=name, diffAmps=True, curvAmps=True)
                wout.transForm_3D(u_dom, v_dom, keys)
                
                gss[key][idx+1] = cc.calc_Gss_metric(wout) 
                
        if key == 'w7x':
            print('W7X')
            u_dom = np.linspace(0,2*np.pi,uPts)
            v_dom = np.linspace(0,2*np.pi,5*uPts)
            for idx in range(counts[key]):
                path = os.path.join(dd.exp_local['W7X'],'coil_configs')
                conID = conIDs[key][idx]
                print('   ref_'+conID)
                
                if conID=='168':
                    name = 'w7x_ref_168_wout_beta_0.0.nc'
                elif conID=='13':
                    name = 'W7X_wout_ref_13_high_beta.nc'
                else:
                    raise NameError('ref_'+conID+' is not currently available for W7-x geomertires.')
                
                wout = wr.readWout(path, name=name, diffAmps=True, curvAmps=True)
                wout.transForm_3D(u_dom, v_dom, keys)
                
                gss[key][idx+1] = cc.calc_Gss_metric(wout) 
                
        if key == 'itos':
            print('ITOS')
            u_dom = np.linspace(0,2*np.pi,uPts)
            v_dom = np.linspace(0,2*np.pi,5*uPts)
            for idx in range(counts[key]):
                path = os.path.join(dd.exp_local['ITOS'],'configurations','BaseCase')
                conID = conIDs[key][idx]
                
                wout = wr.readWout(path, name='wout_ITOS.nc', diffAmps=True, curvAmps=True)
                wout.transForm_3D(u_dom, v_dom, keys)

                gss[key][idx+1] = cc.calc_Gss_metric(wout) 
                
    for key in gss:
        for i in range(len(gss[key][1::])):
            pltD = {}
            for plot_key in plotDict[key]:
                pltD[plot_key] = plotDict[key][plot_key][i]
            
            plot.plt.plot(gss[key][0][1:-1], gss[key][i+1][1:-1], **pltD)
    
    plot.plt.xlabel(r'$\Psi_n$')
    plot.plt.ylabel(r'$\langle\mathcal{G}_{ss}\rangle$')
    
    plot.plt.yscale('log')
    #plot.plt.ylim(0.3e-2, 0.4)
    
    for key in plotDict:
        if 'label' in plotDict[key]:
            box = plot.ax.get_position()
            plot.ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
        
            plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1), fontsize=16)
            break
    
    plot.plt.grid()
    plot.plt.tight_layout()              
    
    
def plot_iota_profile(conIDs, plot, plotDict, basePath=dd.exp_ext_base):
    iota_pros = np.empty((len(conIDs), 128))
    for conIdx, conID in enumerate(conIDs):
        IDs = conID.split('-')
        
        mainID = 'main_coil_{}'.format(IDs[0])
        setID = 'set_{}'.format(IDs[1])
        jobID = 'job_{}'.format(IDs[2])
        
        path = os.path.join(basePath,mainID,setID,jobID)
        if os.path.isfile(os.path.join(path,'wout_HSX_main_opt0.nc')):
            name = 'wout_HSX_main_opt0.nc'
        elif os.path.isfile(os.path.join(path,'wout_HSX_aux_opt0.nc')):
            name = 'wout_HSX_aux_opt0.nc'
        else:
            raise IOError('No usable wout file in '+path)
    
        wout = wr.readWout(path, name=name, iotaPro=True)
        iota_pros[conIdx] = wout.iota
        
    for i, iota in enumerate(iota_pros):
        pltD = {}
        for key in plotDict:
            pltD[key] = plotDict[key][i]
        plot.ax.plot(wout.s_dom, iota, **pltD)
    
    plot.ax.annotate('4/4', (1, 1), fontsize=14)
    plot.ax.annotate('8/7', (1, 8./7), fontsize=14)
    plot.ax.annotate('8/9', (1, 8./9), fontsize=14)
    plot.ax.annotate('12/13', (1, 12./13), fontsize=14)
    plot.ax.annotate('12/11', (1, 12./11), fontsize=14)    
    
    plot.ax.plot([0,1], [1, 1], c='k', ls='-', linewidth=1)
    plot.ax.plot([0,1], [8./7, 8./7], c='k', ls='--', linewidth=1)
    plot.ax.plot([0,1], [8./9, 8./9], c='k', ls='--', linewidth=1)
    plot.ax.plot([0,1], [12./13, 12./13], c='k', ls='-.', linewidth=1)
    plot.ax.plot([0,1], [12./11, 12./11], c='k', ls='-.', linewidth=1)
    
    plot.plt.xlim(0, 1)
    
    plot.plt.xlabel(r'$\Psi_n$')
    plot.plt.ylabel(r'$\iota$')
    
    box = plot.ax.get_position()
    plot.ax.set_position([box.x0, box.y0, box.width * .95, box.height])
        
    plot.plt.grid()
    plot.plt.legend(loc='upper left', bbox_to_anchor=(1.15, 1))
    plot.plt.tight_layout()  
    
def plot_boozer_spectrum(conID, plot, title='QHS', basePath=dd.exp_ext_base):
    IDs = conID.split('-')
    mainID = 'main_coil_{}'.format(IDs[0])
    setID = 'set_{}'.format(IDs[1])
    jobID = 'job_{}'.format(IDs[2])
    
    path = os.path.join(basePath, mainID, setID, jobID)
    if os.path.isfile(os.path.join(path,'wout_HSX_main_opt0.nc')):
        name = 'wout_HSX_main_opt0.nc'
    elif os.path.isfile(os.path.join(path,'wout_HSX_aux_opt0.nc')):
        name = 'wout_HSX_aux_opt0.nc'
    else:
        raise IOError('No usable wout file in '+path)
    
    path = dd.pathQHS
    wout = wr.readWout(path, name=name)
    wout.boozerSpectra(plot.ax, 'Bmod')
    
    plot.ax.set_xlim(0, 1)
    plot.ax.set_xlabel(r'$\Psi_n$')
    plot.ax.set_ylabel('Amplitude')
    
    box = plot.ax.get_position()
    plot.ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    
    plot.plt.title(title)
    plot.plt.legend(loc='upper left', bbox_to_anchor=(1.05, 1))
    

def plot_flf_curvature(plot, conID, r_init, v_chg):
    if conID == 'qhs':
        mainID = 'main_coil_0'
        setID = 'set_1'
        jobID = 'job_0'
    else:
        IDs = conID.split('-')
        mainID = 'main_coil_{}'.format(IDs[0])
        setID = 'set_{}'.format(IDs[1])
        jobID = 'job_{}'.format(IDs[2])
                
    keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs', 'Bu', 'Bv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']    
        
    path = os.path.join('D:\\','HSX_Configs',mainID,setID,jobID)
    wout = wr.readWout(path, diffAmps=True, curvAmps=True)
    points = wout.field_line_follower(r_init, v_chg)
    wout.transForm_listPoints(points, keys)
    
    return points
    '''
    curB = cBT.BcurveTools(wout)
    curB.calc_curvature()
    k_norm = curB.k_norm
    
    v_dom = points[0::,2]
    
    ax1 = plot.ax
    ax2 = ax1.twinx()
    
    plt1, = ax1.plot(v_dom, wout.invFourAmps['Bmod'], c='k')
    plt2, = ax2.plot(v_dom, k_norm, c='tab:red')
    ax2.plot(v_dom, [0]*len(v_dom), c=plt2.get_color(), ls='--', zorder=0)
    
    #ax1.set_ylim(0.7, 1.1)
    ax1.set_xlabel(r'$\theta$', c=plt1.get_color())
    ax1.set_ylabel(r'$|\mathbf{B}|$', c=plt1.get_color())
    
    ax2.set_ylim(-0.15, 0.15)
    ax2.set_ylabel(r'$\kappa_n$', c=plt2.get_color())
    ax2.tick_params(axis='y', colors=plt2.get_color())
    
    plot.plt.tight_layout()
    '''

def plot_flf_GssKn(plot, conID, r_init, v_chg):
    if conID == 'qhs':
        mainID = 'main_coil_0'
        setID = 'set_1'
        jobID = 'job_0'
    else:
        IDs = conID.split('-')
        mainID = 'main_coil_{}'.format(IDs[0])
        setID = 'set_{}'.format(IDs[1])
        jobID = 'job_{}'.format(IDs[2])
                
    keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs', 'Bu', 'Bv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']    
        
    path = os.path.join('D:\\','HSX_Configs',mainID,setID,jobID)
    wout = wr.readWout(path, diffAmps=True, curvAmps=True)
    points = wout.field_line_follower(r_init, v_chg)
    wout.transForm_listPoints(points, keys)
    
    curB = cBT.BcurveTools(wout)
    curB.calc_curvature()
    k_norm = curB.k_norm
    gss = curB.gss
    
    v_dom = points[0::,2]
    
    ax1 = plot.ax
    ax2 = ax1.twinx()
    
    plt1, = ax1.plot(v_dom, k_norm, c='tab:red')
    ax1.plot(v_dom, [0]*len(v_dom), c=plt1.get_color(), ls='--', zorder=0)
    plt2, = ax2.plot(v_dom, gss, c='tab:blue')
    
    ax1.set_ylim(-0.15, 0.15)
    ax1.set_xlabel(r'$\theta$', c='k')
    ax1.set_ylabel(r'$\kappa_n$', c=plt1.get_color())
    ax1.tick_params(axis='y', colors=plt1.get_color())
    
    ax2.set_ylim(0, 0.05)
    ax2.set_ylabel(r'$g_{ss}$', c=plt2.get_color())
    ax2.tick_params(axis='y', colors=plt2.get_color())
    
    plot.plt.tight_layout()
    
def plot_movingAvg_trapRes(dataFile, dataKey, saveFig):
    import h5py as hf
    
    with hf.File(dataFile, 'r') as hf_file:
        om_drift = hf_file[dataKey][:]
    
    npts = om_drift.shape[0]
    om_drift_avg = np.empty(npts)
    for i in range(npts):
        idx = i + 1
        om_drift_avg[i] = np.mean(om_drift[0:idx])
    
    om_std = np.std(om_drift)
    om_mean = om_drift_avg[-1]
        
    plot = pd.plot_define()
    plt = plot.plt
    
    plt.plot(1 + np.arange(npts), om_drift_avg, c='k')
    plt.plot([1, npts], [om_mean]*2, c='k', ls='--', label=r'$\langle \omega_{de} \rangle =$'+'{0:0.2f}'.format( om_mean ) )
    
    plt.xlabel('particles simulated')
    plt.ylabel(r'$\langle \omega_{de} \rangle$')
    
    plt.grid()
    plt.legend()
    plt.tight_layout()
    # plot.plt.show()
    
    plt.savefig(saveFig)

if __name__ == '__main__':    
    if False:
        conIDs = {'hsx' : ['1-0-1','76-1-728','105-1-684','72-1-0']}
        
        plotDict = {'hsx' : {'label' : ['QHS','76-1-728','105-1-684','72-1-0'],
                             'ls' : ['-','dashed','dashdot',':','--'],
                             'c' : ['k','tab:red','tab:blue','tab:green','tab:cyan']},
                    'w7x' : {'label' : [r'W7X $\beta=0$',r'W7X $\beta\gg0$'],
                             'ls' : ['-','--'],
                             'c' : ['tab:red','tab:red']}}
        
        plot = pd.plot_define(lineWidth=2)    
        #plot_drift(conIDs, plot, plotDict)
        #plot_Bk_overlap(conIDs, plot, plotDict)
        plot_Gss_metric(conIDs, plot, plotDict)
        
        plot.plt.savefig('Gss_metric.png')
        #plot.plt.show()
    
    if False:
        dataFile = os.path.join( dd.pathW7X, 'trapRes_psiN_0p25.h5' )
        dataKey = 'beta_5p62'
        saveFig = os.path.join( dd.pathW7X, 'trapRes_'+dataKey+'.png' )
        
        plot_movingAvg_trapRes(dataFile, dataKey, saveFig)
    
    if True:
        conID_list = []
        fileName = os.path.join('D:\\','GENE','eps_valley','configuration_data.txt')
        with open(fileName, 'r') as myFile:
            lines = myFile.readlines()
            for line in lines:
                line = line.strip().split(':')
                conID_list.append( line[1].strip() )
                
        for conID in conID_list:
            conIDs = ['0-1-0', conID]
            
            plot = pd.plot_define(fontSize=14, labelSize=18, lineWidth=2)
            plotDict = {'label' : ['QHS', conID],
                        'c' : ['k', 'tab:red']}
            
            plot_iota_profile(conIDs, plot, plotDict)
            
            saveName = os.path.join('D:\\','GENE','eps_valley','figures','iota_profiles',conID+'.png')
            plot.plt.savefig(saveName)
        