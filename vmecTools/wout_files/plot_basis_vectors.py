import os, sys
import numpy as np
import matplotlib as mpl

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization')
sys.path.append(ModDir)
import plot_define as pd
import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.vecB_tools as vb
import vmecTools.wout_files.coord_convert as cc

# define VMEC grid #
s_val = 0.5
pol_dom = np.linspace(-np.pi, np.pi, 51, endpoint=True)
tor_dom = np.linspace(-np.pi, np.pi, pol_dom.shape[0]*4, endpoint=True)
amp_keys = ['R', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 
        'dL_ds', 'dL_du', 'dL_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv']

# import VMEC data #
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)

# generate helical reference frame
wout.transForm_2D_sSec(s_val, pol_dom, tor_dom, amp_keys)
vecB = vb.BvecTools(wout)
R_vec, Z_vec = vecB.compute_xi_helical_frame()
Rs = R_vec[:,:,0]
Ru = R_vec[:,:,1]
Rv = R_vec[:,:,2]
Zs = Z_vec[:,:,0]
Zu = Z_vec[:,:,1]
Zv = Z_vec[:,:,2]

# map basis vectors onto flux surface #
vmec = wout.invFourAmps
g_uu = (vmec['dR_du']**2) + (vmec['dZ_du']**2)
g_vv = (vmec['dR_dv']**2) + (vmec['R']**2) + (vmec['dZ_dv']**2)
g_uv = vmec['dR_du']*vmec['dR_dv'] + vmec['dZ_du']*vmec['dZ_dv']
Su = Ru+Zu
Sv = Rv+Zv
vec_norm = np.sqrt(Su*Su*g_uu + 2*Su*Sv*g_uv + Sv*Sv*g_vv)
vmec_norm_inv = 1./vec_norm
vec = (0.1) * np.stack((Sv*vec_norm_inv, Su*vec_norm_inv), axis=2)

plot = pd.plot_define(fontSize=14, labelSize=16)
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True)

cmap = 'magma'
# smap = ax.pcolormesh(tor_dom/np.pi, pol_dom/np.pi, vmec['Bmod'].T, cmap=cmap)
smap = ax.pcolormesh(tor_dom/np.pi, pol_dom/np.pi, (1./vec_norm_inv).T, cmap=cmap, vmin=0, vmax=2)
ax.contour(tor_dom/np.pi, pol_dom/np.pi, vmec['Bmod'].T, cmap='jet')

tor_idx = np.linspace(0, tor_dom.shape[0], 7, dtype=int, endpoint=False)
pol_idx = np.linspace(0, pol_dom.shape[0], 7, dtype=int, endpoint=False)
for i in tor_idx:
    for j in pol_idx:
        ax.arrow(tor_dom[i]/np.pi, pol_dom[j]/np.pi, vec[i,j,0], vec[i,j,1], color='w', head_width=0.025, lw=1)
        # ax.arrow(tor_dom[i]/np.pi, pol_dom[j]/np.pi, arr_norm*eta_contra[i,j,0], arr_norm*eta_contra[i,j,1], color='tab:blue', head_width=0.025, lw=1)
        # ax.arrow(tor_dom[i]/np.pi, pol_dom[j]/np.pi, arr_norm*xi_contra[i,j,0], arr_norm*xi_contra[i,j,1], color='tab:orange', head_width=0.025, lw=1)

cbar = fig.colorbar(smap, ax=ax)
cbar.ax.set_ylabel(r'$B$')

ax.set_xlabel(r'$\zeta \ / \ \pi$')
ax.set_ylabel(r'$\theta \ / \ \pi$')
ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)

plt.show()
