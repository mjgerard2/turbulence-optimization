import numpy as np
import h5py as hf

import read_boozmn as rb

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd

### Declare Configuration ID ###
conID = '0-3-701'
ampKey = 'Bmod'

s_val = 0.5
upts = 101

### Define Paths ###
mainID = conID.split('-')[0]
setID = conID.split('-')[1]
jobID = conID.split('-')[2]

# base_path = os.path.join('/mnt','HSX_Database','HSX_Configs','main_coil_'+mainID,'set_'+setID,'job_'+jobID)
# booz_path = os.path.join(base_path, 'boozmn_wout_HSX_main_opt0.nc')
# vmec_path = os.path.join(base_path, 'wout_HSX_main_opt0.nc')
booz_path = os.path.join('/home', 'michael', 'Desktop', 'boozmn_wout_preciseQH.nc')
vmec_path = os.path.join('/home', 'michael', 'Desktop', 'wout_20221002-01-009_QH_nfp4_A6p5_n0_3_T0_15_avoidIota1_MercierSMin0p1.nc')

booz = rb.readBooz(booz_path, vmec_path)

### Get Field Lines ###
alpha_set = np.array([0])  # np.linspace(-np.pi, np.pi, 5)
s_idx = np.argmin(np.abs(booz.s_booz - s_val))
pol_dom = np.linspace(-np.pi, np.pi, 100)
tor_dom_set = np.empty((alpha_set.shape[0], pol_dom.shape[0]))
for idx, alpha in enumerate(alpha_set):
    # tor_dom_set[idx] = (1. / booz.iota(s_val)) * (pol_dom - alpha/np.sqrt(s_val))
    tor_dom_set[idx] = (pol_dom - alpha) / booz.iota(s_val)

### Get Toroidal and Poloidal B_field Magnitude ###
u_dom = np.linspace(-np.pi, np.pi, upts)
v_dom = np.linspace(-np.pi, np.pi, 4*upts)

booz.boozCoord_2D_sSec(s_val, u_dom, v_dom, [ampKey])

### Generate Flat Torus Plot ###
plot = pd.plot_define(lineWidth=2, fontSize=20, labelSize=24)
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True)

s_map = ax.pcolormesh(v_dom/np.pi, u_dom/np.pi, booz.invFourAmps['Bmod'].T, cmap='jet')
for tor_dom in tor_dom_set:
    pol_dom_plot = pol_dom[(tor_dom > -np.pi) & (tor_dom < np.pi)]
    tor_dom_plot = tor_dom[(tor_dom > -np.pi) & (tor_dom < np.pi)]

    ax.plot(tor_dom_plot/np.pi, pol_dom_plot/np.pi, c='k', ls='--')

cbar = fig.colorbar(s_map, ax=ax)
cbar.ax.set_ylabel(r'$B$ (T)')

ax.set_title(r'$\psi_N = $'+'{0:0.2f} '.format(s_val)+r'($r/a = $'+'{0:0.2f})'.format(np.sqrt(s_val)))
ax.set_xlabel(r'$\zeta/\pi$')
ax.set_ylabel(r'$\theta/\pi$')

plt.show()
savePath = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'figures', 'flat_torus', 'Mirror_psiN_0p25.png')
# plt.savefig(savePath)
