import h5py as hf
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.booz_read as br
import vmecTools.wout_files.read_boozmn as rb
import gistTools.gist_reader as gr


conID_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(conID_file, 'r') as hf_:
    met_data = hf_['metric data'][()]
met_data = np.array(sorted(met_data, key=lambda x: x[9]))

conIDs = []
for met in met_data:
    conIDs.append('-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]]))

conIDs = [conIDs[0], conIDs[-1]]
for kdx, conID in enumerate(conIDs):
    print(conID+' ({0:0.0f}|{1:0.0f})'.format(kdx+1, len(conIDs)))

    # Data Files #
    base_dirc = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir')
    booz_file = os.path.join(base_dirc, 'booz_nc', 'booz_16-16', 'boozmn_wout_HSX_'+conID+'.nc')
    wout_file = os.path.join(base_dirc, 'wout_nc', 'wout_HSX_'+conID+'.nc')
    gist_file = os.path.join(base_dirc, 'gist_files', 'gist_'+conID+'_s0p5_alpha0_npol2_nz2048.dat')

    # tran = rb.readBooz(booz_file, wout_file)
    booz = br.readBooz(booz_file)
    iota = booz.iota(0.5)

    # Flux Surface Grid #
    pol_pnts = 101
    tor_pnts = 4*pol_pnts

    pol_norm = np.linspace(-1, 1, pol_pnts)
    tor_norm = np.linspace(-1, 1, tor_pnts)

    # Boozer Mode Fields #
    idx_xn = np.where(booz.xn == 4)[0]
    idx_xm = np.where(booz.xm == 1)[0]
    idx = np.intersect1d(idx_xn, idx_xm)[0]
    B_norm = 1. / np.abs(booz.fourierAmps['Bmod'](0.5)[idx])

    B_41 = B_norm * booz.plot_mode(0.5, [[4, 1, 1]], pol_pnts=pol_pnts, tor_pnts=tor_pnts)
    B_42 = B_norm * booz.plot_mode(0.5, [[4, 2, 10]], pol_pnts=pol_pnts, tor_pnts=tor_pnts)
    B_01 = B_norm * booz.plot_mode(0.5, [[0, 1, 10]], pol_pnts=pol_pnts, tor_pnts=tor_pnts)
    B_super = booz.plot_mode(0.5, [[4, 1, 1], [4, 2, 1]], pol_pnts=pol_pnts, tor_pnts=tor_pnts)

    # tran.transForm_2D_sSec(0.5, pol_norm*np.pi, tor_norm*np.pi, ['Bmod'])
    # B_super = tran.invFourAmps['Bmod']

    # Gist Data #
    gist = gr.read_gist(gist_file)
    gist.partition_wells()
    gist.calc_shear()

    well_idx = np.argmin(np.abs(gist.well_indices))
    pol_dom = np.linspace(gist.pol_max[well_idx], gist.pol_max[well_idx+1], 151)
    # pol_dom = np.pi * np.linspace(-0.5, 0.5, 151)

    B = gist.model['modB'](pol_dom)
    gradPsi = gist.model['gxx'](pol_dom)
    Kn = gist.model['Kn'](pol_dom)
    Kg = gist.model['Kg'](pol_dom)
    D = gist.model['D'](pol_dom)
    curve_drive = (Kn / gradPsi) + D*Kg*(gradPsi / B)

    # Plotting Parameters #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 18}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2

    # Plotting Axis #
    fig = plt.figure(tight_layout=True, figsize=(16, 10))
    gs = mpl.gridspec.GridSpec(2, 3)

    ax1 = fig.add_subplot(gs[0, 0])
    ax2 = fig.add_subplot(gs[0, 1])
    ax3 = fig.add_subplot(gs[0, 2])
    ax4 = fig.add_subplot(gs[1, 0])
    ax5 = fig.add_subplot(gs[1, 1:3])
    ax6 = ax5.twinx()

    ax1.axes.xaxis.set_ticklabels([])
    ax2.axes.xaxis.set_ticklabels([])
    ax2.axes.yaxis.set_ticklabels([])
    ax3.axes.xaxis.set_ticklabels([])
    ax3.axes.yaxis.set_ticklabels([])

    # Plot B_41 #
    ax1.pcolormesh(tor_norm, pol_norm, B_41.T, vmin=-1, vmax=1, cmap='viridis')
    ax2.pcolormesh(tor_norm, pol_norm, B_42.T, vmin=-1, vmax=1, cmap='viridis')
    ax3.pcolormesh(tor_norm, pol_norm, B_01.T, vmin=-1, vmax=1, cmap='viridis')
    ax4.contour(tor_norm, pol_norm, B_super.T, cmap='viridis')

    ax5.plot(pol_dom/np.pi, B, c='tab:blue')
    ax6.plot(pol_dom/np.pi, curve_drive, c='tab:orange')

    # Field Line #
    pol_bnd = np.array([pol_dom[0], pol_dom[-1]])
    tor_bnd = gist.q0 * pol_bnd - gist.alpha0

    ax1.plot(tor_bnd/np.pi, pol_bnd/np.pi, c='w', marker='o', ms=10)
    ax2.plot(tor_bnd/np.pi, pol_bnd/np.pi, c='w', marker='o', ms=10)
    ax3.plot(tor_bnd/np.pi, pol_bnd/np.pi, c='w', marker='o', ms=10)
    ax4.plot(tor_bnd/np.pi, pol_bnd/np.pi, c='k', marker='o', ms=10)

    # Highlight Negative Curvature Drive Region #
    neg_idx = np.where(curve_drive < 0)[0]
    neg_bool = ((neg_idx - np.roll(neg_idx, 1)) != 1)
    negDX = np.where(neg_bool)[0]

    alpha_clr = 0.2
    for i, idx in enumerate(negDX[0:-1]):
        pol_beg = neg_idx[idx]
        pol_end = neg_idx[negDX[i+1] - 1] + 1
        ax6.axvspan(pol_dom[pol_beg]/np.pi, pol_dom[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')

    if len(negDX) > 0:
        pol_beg = neg_idx[negDX[-1]]
        pol_end = neg_idx[-1]
        ax6.axvspan(pol_dom[pol_beg]/np.pi, pol_dom[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')

    # Axis Limits #
    # ax5.set_ylim(0.78, 1.25)
    # ax6.set_ylim(-2, 4)
    ax5.set_xlim(pol_dom[0]/np.pi, pol_dom[-1]/np.pi)

    # Axis Grid #
    ax6.grid()

    # Axis Labels #
    ax1.set_ylabel(r'$\theta/\pi$')
    ax4.set_ylabel(r'$\theta/\pi$')
    ax4.set_xlabel(r'$\zeta/\pi$')

    ax1.set_title(r'$B_{41}$')
    ax2.set_title(r'$10 \times B_{42}$')
    ax3.set_title(r'$10 \times B_{01}$')
    ax4.set_title(r'$B_{41}+B_{42}+B_{01}$')

    ax5.set_ylabel(r'$B$')
    ax6.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D \kappa_g \frac{|\nabla \psi|}{B}$')

    # Save or Show #
    save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'linear_tem_in_qhs', 'figures', 'boozer_scan_HSX', 'kappa_{0:0.0f}.png'.format(kdx))
    # plt.savefig(save_path)
    plt.show()
