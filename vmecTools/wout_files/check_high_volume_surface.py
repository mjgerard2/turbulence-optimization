import os, sys
import h5py as hf
import numpy as np

from matplotlib.ticker import AutoMinorLocator, MultipleLocator

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import vmecTools.wout_files.wout_read as wr
import plot_define as pd

# import volume list #
vol_path = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools', 'vmecTools', 'wout_files', 'volume_data.h5')
with hf.File(vol_path, 'r') as hf_:
    vol_data = hf_['data'][()]

# get QHS flux surface #
psi_dom = np.array([1])
pol_dom = np.linspace(-np.pi, np.pi, 501)
tor_val = 40.5 * (np.pi/180)

qhs_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
wout = wr.readWout(qhs_path)
wout.transForm_2D_vSec(psi_dom, pol_dom, tor_val, ['R', 'Z'])

R_qhs = wout.invFourAmps['R'][0]
Z_qhs = wout.invFourAmps['Z'][0]

# plotting parameters #
plot = pd.plot_define(fontSize=14, labelSize=16)
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True)
ax.set_aspect('equal')

# plot data #
ax.plot(R_qhs, Z_qhs, label='QHS')

# get comparative flux surface #
for i in range(8,9):
    main_id = 'main_coil_{0:0.0f}'.format(vol_data[i,0])
    set_id = 'set_{0:0.0f}'.format(vol_data[i,1])
    job_id = 'job_{0:0.0f}'.format(vol_data[i,2])
    chk_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_HSX_main_opt0.nc')
    wout = wr.readWout(chk_path)
    wout.transForm_2D_vSec(psi_dom, pol_dom, tor_val, ['R', 'Z'])

    R_chk = wout.invFourAmps['R'][0]
    Z_chk = wout.invFourAmps['Z'][0]
    
    config_id = '-'.join(['{0:0.0f}'.format(x) for x in vol_data[i,0:3]])
    ax.plot(R_chk, Z_chk, label=config_id)

# axis labels #
ax.set_xlabel(r'$R \ / \ \mathrm{m}$')
ax.set_ylabel(r'$Z \ / \ \mathrm{m}$')
ax.set_title(r'$\varphi = {0:0.2f} \pi$'.format(tor_val/np.pi))

# axis ticks #
ax.tick_params(axis='both', which='both', direction='in')
ax.xaxis.set_ticks_position('default')
ax.yaxis.set_ticks_position('default')

ax.xaxis.set_minor_locator(MultipleLocator(0.01))
ax.yaxis.set_minor_locator(MultipleLocator(0.01))

# axis legend #
ax.legend(frameon=False)

# save/show #
# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'high_volume_configuration.pdf')
plt.savefig(save_path, format='pdf')
