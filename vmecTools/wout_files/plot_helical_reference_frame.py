import os, sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.vecB_tools as vb
import plot_define as pd

# equilibrium grids #
s_dom = np.linspace(0.1, 0.9, 50)
u_dom = np.linspace(-np.pi, np.pi, 151)
v_dom = np.array([0, 0.125, 0.25])*np.pi
amp_keys = ['R', 'Z', 'Bmod', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'dBmod_ds', 'dBmod_du', 'dBmod_dv']

# magnetic axis grid #
u_ma = np.array([0])
v_ma = v_dom

# compute equilibrium grids #
# wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
# wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_60', 'set_1', 'job_84', 'wout_60-1-84_mn1824_ns101.nc')
# wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_152', 'set_1', 'job_485', 'wout_152-1-485_mn1824_ns101.nc')
wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)

# compute magnetic axis #
wout.transForm_2D_sSec(0, u_ma, v_ma, ['R', 'Z'])
R_ma = wout.invFourAmps['R'][:,0]
Z_ma = wout.invFourAmps['Z'][:,0]

# compute flux surface quantities #
wout.transForm_3D(s_dom, u_dom, v_dom, amp_keys)
# wout.transForm_2D_sSec(0.5, u_dom, v_dom, amp_keys)
R_grid = wout.invFourAmps['R']
Z_grid = wout.invFourAmps['Z']
B_grid = wout.invFourAmps['Bmod']

# compute helical basis vectors #
vecB = vb.BvecTools(wout)
R_vec, Z_vec = vecB.compute_toroidal_helical_frame()

# compute poloidally averaged basis vectors #
X_ma = np.stack((R_ma, Z_ma), axis=1)
X_surf = np.stack((R_grid[-1], Z_grid[-1]), axis=2)
g_uu = (wout.invFourAmps['dR_du'][-1]**2) + (wout.invFourAmps['dZ_du'][-1]**2)
R_vec_avg, Z_vec_avg = vecB.compute_average_toroidal_helical_frame(X_ma, X_surf, R_vec[-1], Z_vec[-1], g_uu, u_dom)

R_prime = 0.15*wout.a_minor*R_vec
Z_prime = 0.15*wout.a_minor*Z_vec

R_prime_avg = 0.25*wout.a_minor*R_vec_avg
Z_prime_avg = 0.25*wout.a_minor*Z_vec_avg

# compute shaping parameters #
shapes = vecB.compute_shaping_parameters(X_ma, X_surf, R_vec_avg, Z_vec_avg)
print(shapes)

# plotting parameters #
plot = pd.plot_define()
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True)
ax.set_aspect('equal')

# plot contours #
cmap = 'jet'
triang = mpl.tri.Triangulation(R_grid[1:,0,:].flatten(), Z_grid[1:,0,:].flatten())
# cont0 = ax.tricontourf(triang, B_grid[1:,0,:].flatten(), 11, cmap=cmap)
cont0 = ax.tricontourf(triang, B_grid[1:,0,:].flatten(), 20, cmap=cmap)
clippath = Path(np.c_[R_grid[-1,0,:], Z_grid[-1,0,:]])
patch = PathPatch(clippath, facecolor='none')
ax.add_patch(patch)
for c in cont0.collections:
    c.set_clip_path(patch)

triang = mpl.tri.Triangulation(R_grid[1:,1,:].flatten(), Z_grid[1:,1,:].flatten())
cont1 = ax.tricontourf(triang, B_grid[1:,1,:].flatten(), levels=cont0.levels, cmap=cmap)
clippath = Path(np.c_[R_grid[-1,1,:], Z_grid[-1,1,:]])
patch = PathPatch(clippath, facecolor='none')
ax.add_patch(patch)
for c in cont1.collections:
    c.set_clip_path(patch)

triang = mpl.tri.Triangulation(R_grid[1:,2,:].flatten(), Z_grid[1:,2,:].flatten())
cont2 = ax.tricontourf(triang, B_grid[1:,2,:].flatten(), levels=cont0.levels, cmap=cmap)
clippath = Path(np.c_[R_grid[-1,2,:], Z_grid[-1,2,:]])
patch = PathPatch(clippath, facecolor='none')
ax.add_patch(patch)
for c in cont2.collections:
    c.set_clip_path(patch)

# plot LCFS #
ax.plot(R_grid[-1,0,:], Z_grid[-1,0,:], c='k', lw=2)
ax.plot(R_grid[-1,1,:], Z_grid[-1,1,:], c='k', lw=2)
ax.plot(R_grid[-1,2,:], Z_grid[-1,2,:], c='k', lw=2)

# basis vectors #
pol_plot = np.linspace(-np.pi, np.pi, 50, endpoint=False)
for pol in pol_plot:
    i = np.argmin(np.abs(u_dom-pol))
    ax.arrow(R_grid[-1,0,i], Z_grid[-1,0,i], R_prime[-1,0,i,0], R_prime[-1,0,i,1], head_width=0.005, color='tab:blue', zorder=10, lw=1)
    ax.arrow(R_grid[-1,0,i], Z_grid[-1,0,i], Z_prime[-1,0,i,0], Z_prime[-1,0,i,1], head_width=0.005, color='tab:red', zorder=10, lw=1)
    ax.arrow(R_grid[-1,1,i], Z_grid[-1,1,i], R_prime[-1,1,i,0], R_prime[-1,1,i,1], head_width=0.005, color='tab:blue', zorder=10, lw=1)
    ax.arrow(R_grid[-1,1,i], Z_grid[-1,1,i], Z_prime[-1,1,i,0], Z_prime[-1,1,i,1], head_width=0.005, color='tab:red', zorder=10, lw=1)
    ax.arrow(R_grid[-1,2,i], Z_grid[-1,2,i], R_prime[-1,2,i,0], R_prime[-1,2,i,1], head_width=0.005, color='tab:blue', zorder=10, lw=1)
    ax.arrow(R_grid[-1,2,i], Z_grid[-1,2,i], Z_prime[-1,2,i,0], Z_prime[-1,2,i,1], head_width=0.005, color='tab:red', zorder=10, lw=1)

# average basis vectors #
ax.arrow(R_ma[0], Z_ma[0], R_prime_avg[0,0], R_prime_avg[0,1], head_width=0.008, color='tab:blue', zorder=10, lw=1)
ax.arrow(R_ma[0], Z_ma[0], Z_prime_avg[0,0], Z_prime_avg[0,1], head_width=0.008, color='tab:red', zorder=10, lw=1)
ax.arrow(R_ma[1], Z_ma[1], R_prime_avg[1,0], R_prime_avg[1,1], head_width=0.008, color='tab:blue', zorder=10, lw=1)
ax.arrow(R_ma[1], Z_ma[1], Z_prime_avg[1,0], Z_prime_avg[1,1], head_width=0.008, color='tab:red', zorder=10, lw=1)
ax.arrow(R_ma[2], Z_ma[2], R_prime_avg[2,0], R_prime_avg[2,1], head_width=0.008, color='tab:blue', zorder=10, lw=1)
ax.arrow(R_ma[2], Z_ma[2], Z_prime_avg[2,0], Z_prime_avg[2,1], head_width=0.008, color='tab:red', zorder=10, lw=1)

# axis labels #
ax.set_xlabel(r'$R$')
ax.set_ylabel(r'$Z$')

# axis ticks #
ax.tick_params(axis='both', direction='in')
ax.xaxis.set_ticks_position('default')
ax.yaxis.set_ticks_position('default')

# save/show #
# plt.show()
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'personal_meetings', 'Joey_Duff', '20240305', 'figures', 'helical_reference_frame.pdf')
plt.savefig(save_path, format='pdf')
