import os, sys
import numpy as np

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import plot_define as pd
import vmecTools.wout_files.wout_read as wr

def calc_kappa(wout):
    pol_dom = np.linspace(-np.pi, np.pi, 11, endpoint=False)
    tor_dom = np.linspace(-np.pi, np.pi, pol_dom.shape[0]*4, endpoint=False)
        
    wout.transForm_3D(wout.s_grid, pol_dom, tor_dom, ['R', 'Z'])
    R_grid = wout.invFourAmps['R']
    Z_grid = wout.invFourAmps['Z']

    cos_tor = np.cos(tor_dom)
    sin_tor = np.sin(tor_dom)
    vec_phat = np.stack((-sin_tor, cos_tor, np.zeros(tor_dom.shape)), axis=1)

    vec_ma = np.stack((R_grid[0,:,0]*cos_tor, R_grid[0,:,0]*sin_tor, Z_grid[0,:,0]), axis=1)
    vec_ma_norm = np.linalg.norm(vec_ma, axis=1)

    vec_rhat = np.empty(vec_ma.shape)
    vec_rhat[:,0] = vec_ma[:,0] / vec_ma_norm
    vec_rhat[:,1] = vec_ma[:,1] / vec_ma_norm
    vec_rhat[:,2] = vec_ma[:,2] / vec_ma_norm

    kappa_psi = np.empty(wout.ns-1)
    for i in range(1, wout.ns):
        R_psi = R_grid[i,:,:]
        Z_psi = Z_grid[i,:,:]
        kappa_tor = np.empty(tor_dom.shape)
        for j in range(tor_dom.shape[0]):
            r_ma = vec_ma_norm[j]
            rhat = vec_rhat[j]
            phat = vec_phat[j]
            zhat = np.cross(rhat, phat)

            cos = cos_tor[j]
            sin = sin_tor[j]

            numer = np.empty(pol_dom.shape)
            denom = np.empty(pol_dom.shape)
            for k in range(pol_dom.shape[0]):
                vec = np.array([R_psi[j,k]*cos, R_psi[j,k]*sin, Z_psi[j,k]])
                numer[k] = np.abs(np.dot(zhat, vec))
                denom[k] = np.abs(np.dot(rhat, vec) - r_ma)
            kappa_tor[j] = np.trapz(numer, pol_dom)/np.trapz(denom, pol_dom)
        kappa_psi[i-1] = np.trapz(kappa_tor, tor_dom)/(2*np.pi)
    kappa = np.trapz(kappa_psi, wout.phi_dom[1:])/(wout.phi_dom[-1]-wout.phi_dom[1])
    return kappa        

wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
# wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
wout = wr.readWout(wout_path)
kappa_qhs = calc_kappa(wout)

wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'wout_well8p0_mn1824_ns101.nc')
wout = wr.readWout(wout_path)
kappa_well = calc_kappa(wout)

print(kappa_well/kappa_qhs)
