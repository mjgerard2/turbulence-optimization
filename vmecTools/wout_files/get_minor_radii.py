import os, sys
import pandas as pd
import h5py as hf
import numpy as np

from netCDF4 import Dataset

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import databaseTools.functions as fun

# define coil current arrays #
cc_dict = {}
cc_dict['QHS'] = [np.ones(6), np.zeros(6)]
cc_dict['Mirror'] = [np.ones(6), 0.1*np.array([1,1,1,-1,-1,-1])]
cc_dict['AntiMirror'] = [np.ones(6), 0.1*np.array([-1,-1,-1,1,1,1])]
cc_dict['Flip14'] = [np.ones(6), 0.1*np.array([-1,1,1,1,-1,-1])]
# cc_dict['Hill'] = [np.ones(6), 0.1*np.ones(1)]
cc_dict['Well'] = [np.ones(6), -0.1*np.ones(1)]

minor_path = os.path.join('/home', 'michael', 'Desktop', 'HSX_minor_radii.txt')
with open(minor_path, 'w') as f:
    f.write('Configuration: minor radius (m)\n')
    for key, crnt in cc_dict.items():
        config_id = fun.findPathFromCrnt(crnt[0], crnt[1])
        print('%s: %s' % (key, config_id))

        main_id = 'main_coil_%s' % config_id.split('-')[0]
        set_id = 'set_%s' % config_id.split('-')[1]
        job_id = 'job_%s' % config_id.split('-')[2]
        if key == 'QHS':
            wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_%s_mn1824_ns101.nc' % key)
        else:
            wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_HSX_%s_mn1824_ns101.nc' % key)
        
        rootgrp = Dataset(wout_path, 'r')
        minor = rootgrp['/Aminor_p'][()]
        rootgrp.close()
        f.write('{}: {}\n'.format(key, minor))
"""
# get full array #
mn_tag = 'mn88'
file_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'HSX_coil_current_data_{}.h5'.format(mn_tag))
df = pd.read_hdf(file_path, key='DataFrame')
config_list = df.index.values

vol_arr = np.zeros((len(config_list), 4))
for i, config_id in enumerate(config_list):
    if (i+1) % 100000 == 0:
        print('({}|{})'.format(i+1, vol_arr.shape[0]))
    config_split = config_id.split('-')
    vol_arr[i,0:3] = [int(x) for x in config_split]
    main_id = 'main_coil_%s' % config_split[0]
    set_id = 'set_%s' % config_split[1]
    job_id = 'job_%s' % config_split[2]
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_HSX_main_opt0.nc')

    rootgrp = Dataset(wout_path, 'r')
    vol_arr[i,3] = rootgrp['/volume_p'][()]
    rootgrp.close()

vol_arr = np.array(sorted(vol_arr, key=lambda x: x[3], reverse=True))
print(vol_arr[0:10])

hf_path = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools', 'vmecTools', 'wout_files', 'volume_data.h5')
with hf.File(hf_path, 'w') as hf_:
    hf_.create_dataset('data', data=vol_arr)
"""
