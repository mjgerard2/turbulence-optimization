import os
import h5py as hf
import read_boozmn as rb


config_id = '897-1-0'

config_split = config_id.split('-')
main_id = 'main_coil_%s' % config_split[0]
set_id = 'set_%s' % config_split[1]
job_id = 'job_%s' % config_split[2]

equil_base = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
booz_path = os.path.join(equil_base, 'boozmn_wout_HSX_main_opt0.nc')
vmec_path = os.path.join(equil_base, 'wout_HSX_main_opt0.nc')

gene_base = os.path.join('/mnt', 'GENE', 'nonlinear_data', config_id, 'omn_2p0_omti_0p0_omte_2p0')
pram_path = os.path.join(gene_base, 'parameters_1')
tube_path = os.path.join(gene_base, 'flux_tube_data_interp3_1.h5')

ampKeys = ['R', 'Z', 'Phi', 'Bmod']

# Construct Boozer and VMEC Objects #
booz = rb.readBooz(booz_path, vmec_path)
booz.flux_tube_geometry(0.5, 0.0, pram_path, ampKeys, Ninterp=3)

with hf.File(tube_path, 'w') as hf_:
    hf_.create_dataset('GENE x domain', data=booz.gene_x)
    hf_.create_dataset('GENE y domain', data=booz.gene_y)
    hf_.create_dataset('poloidal domain', data=booz.pol_dom)
    hf_.create_dataset('cartesian coordinates', data=booz.cart_points)
    hf_.create_dataset('q0', data=booz.q0)
    hf_.create_dataset('s0', data=booz.s0)
    hf_.create_dataset('alpha0', data=booz.alpha0)
    hf_.create_dataset('shat', data=booz.shat)
    hf_.create_dataset('Bref', data=booz.Bref)
    hf_.create_dataset('Lref', data=booz.Lref)
    hf_.create_dataset('rho ref', data=booz.rho_ref)
