import h5py as hf
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.booz_read as br
import gistTools.gist_reader as gr


conID_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(conID_file, 'r') as hf_:
    met_data = hf_['metric data'][()]
met_data = np.array(sorted(met_data, key=lambda x: x[9]))

conIDs = []
for met in met_data:
    conIDs.append('-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]]))

conIDs = ['897-1-0']
for kdx, conID in enumerate(conIDs):
    print(conID+' ({0:0.0f}|{1:0.0f})'.format(kdx+1, len(conIDs)))

    # Data Files #
    base_dirc = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir')
    booz_file = os.path.join(base_dirc, 'booz_nc', 'booz_16-16', 'boozmn_wout_HSX_'+conID+'.nc')
    gist_file = os.path.join(base_dirc, 'gist_files', 'gist_'+conID+'_s0p5_alpha0_npol2_nz2048.dat')

    # Gist Data #
    gist = gr.read_gist(gist_file)
    gist.partition_wells()
    well_idx = np.argmin(np.abs(gist.well_indices))
    # pol_dom = np.linspace(gist.pol_max[well_idx], gist.pol_max[well_idx+1], 151)
    pol_dom = np.pi * np.linspace(-1, 1, 151)

    # Boozer Data #
    booz = br.readBooz(booz_file)
    iota = booz.iota(0.5)

    s_val = 0.5
    q0 = gist.q0
    alpha0 = gist.alpha0

    # Flux Surface Grid #
    pol_pnts = 101
    tor_pnts = 4*pol_pnts

    pol_norm = np.linspace(-1, 1, pol_pnts)
    tor_norm = np.linspace(-1, 1, tor_pnts)

    # Boozer Mode Fields #
    B_41 = booz.plot_mode(0.5, [[4, 1, 1]], pol_pnts=pol_pnts, tor_pnts=tor_pnts)
    B_42 = booz.plot_mode(0.5, [[4, 2, 1]], pol_pnts=pol_pnts, tor_pnts=tor_pnts)
    B_84 = booz.plot_mode(0.5, [[8, 4, 1]], pol_pnts=pol_pnts, tor_pnts=tor_pnts)
    B_01 = booz.plot_mode(0.5, [[0, 1, 1]], pol_pnts=pol_pnts, tor_pnts=tor_pnts)

    idx_xn = np.where(booz.xn == 4)[0]
    idx_xm = np.where(booz.xm == 1)[0]
    idx_41 = np.intersect1d(idx_xn, idx_xm)[0]
    amp_41 = booz.fourierAmps['Bmod'](s_val)[idx_41]
    amp_41 = amp_41

    idx_xn = np.where(booz.xn == 4)[0]
    idx_xm = np.where(booz.xm == 2)[0]
    idx_42 = np.intersect1d(idx_xn, idx_xm)[0]
    amp_42 = 10 * booz.fourierAmps['Bmod'](s_val)[idx_42]

    idx_xn = np.where(booz.xn == 8)[0]
    idx_xm = np.where(booz.xm == 4)[0]
    idx_84 = np.intersect1d(idx_xn, idx_xm)[0]
    amp_84 = 10 * booz.fourierAmps['Bmod'](s_val)[idx_84]

    idx_xn = np.where(booz.xn == 16)[0]
    idx_xm = np.where(booz.xm == 8)[0]
    idx_168 = np.intersect1d(idx_xn, idx_xm)[0]
    amp_168 = 10 * booz.fourierAmps['Bmod'](s_val)[idx_168]

    idx_xn = np.where(booz.xn == 0)[0]
    idx_xn = np.where(booz.xn == 0)[0]
    idx_xn = np.where(booz.xn == 0)[0]
    idx_xm = np.where(booz.xm == 1)[0]
    idx_01 = np.intersect1d(idx_xn, idx_xm)[0]
    amp_01 = 0 * booz.fourierAmps['Bmod'](s_val)[idx_01]

    B_well_41 = amp_41 * np.cos((4*q0 - 1)*pol_dom - 4*alpha0)
    B_well_41_42 = B_well_41 + amp_42 * np.cos((4*q0 - 2)*pol_dom - 4*alpha0)
    B_well_41_42_84 = B_well_41_42 + amp_84 * np.cos((8*q0 - 4)*pol_dom - 8*alpha0)
    B_well_41_42_84_168 = B_well_41_42_84 + amp_168 * np.cos((16*q0 - 8)*pol_dom - 16*alpha0)
    B_well_41_42_01 = B_well_41_42 + amp_01 * np.cos(-pol_dom)

    # Plotting Parameters #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 18}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2

    # Plotting Axis #
    fig = plt.figure(tight_layout=True, figsize=(16, 10))
    gs = mpl.gridspec.GridSpec(2, 3)

    ax1 = fig.add_subplot(gs[0, 0])
    ax2 = fig.add_subplot(gs[0, 1])
    ax3 = fig.add_subplot(gs[0, 2])
    ax4 = fig.add_subplot(gs[1, 0:3])

    ax2.axes.yaxis.set_ticklabels([])
    ax3.axes.yaxis.set_ticklabels([])

    # Plot B_41 #
    ax1.pcolormesh(tor_norm, pol_norm, B_41.T, cmap='viridis')
    ax2.pcolormesh(tor_norm, pol_norm, B_42.T, cmap='viridis')
    ax3.pcolormesh(tor_norm, pol_norm, B_84.T, cmap='viridis')

    ax4.plot(pol_dom/np.pi, B_well_41, c='k', ls=':', label=r'$B_{41}$')
    # ax4.plot(pol_dom/np.pi, B_well_41_42, c='k', ls='-.', label=r'$B_{41} + B_{42}$')
    ax4.plot(pol_dom/np.pi, B_well_41_42_84_168, c='k', ls='-', label=r'$B_{41} + B_{42} + B_{84}$')

    ax4.legend()

    # Field Line #
    pol_bnd = np.array([pol_dom[0], pol_dom[-1]])
    # pol_bnd = np.pi * np.array([-1, 1])
    tor_bnd = q0 * pol_bnd - alpha0

    ax1.plot(tor_bnd/np.pi, pol_bnd/np.pi, c='w', marker='o', ms=10)
    ax2.plot(tor_bnd/np.pi, pol_bnd/np.pi, c='w', marker='o', ms=10)
    ax3.plot(tor_bnd/np.pi, pol_bnd/np.pi, c='w', marker='o', ms=10)

    # Axis Limits #
    ax4.set_xlim(pol_dom[0]/np.pi, pol_dom[-1]/np.pi)

    # Axis Grid #
    ax4.grid()

    # Axis Labels #
    ax1.set_ylabel(r'$\theta/\pi$')
    ax2.set_ylabel(r'$\theta/\pi$')
    ax3.set_ylabel(r'$\theta/\pi$')
    ax4.set_ylabel(r'$B \ (T)$')
    ax4.set_xlabel(r'$\theta/\pi$')

    ax1.set_title(r'${0:0.4f} \times B_{{41}}$'.format(amp_41))
    ax2.set_title(r'${0:0.4f} \times B_{{42}}$'.format(amp_42))
    ax3.set_title(r'${0:0.4f} \times B_{{84}}$'.format(amp_84))

    # Save or Show #
    save_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'linear_tem_in_qhs', 'figures', 'boozer_scan_HSX', 'kappa_{0:0.0f}.png'.format(kdx))
    # plt.savefig(save_path)
    plt.show()
