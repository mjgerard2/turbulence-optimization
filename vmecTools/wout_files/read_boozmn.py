import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import numpy as np
import scipy.interpolate as spi
from netCDF4 import Dataset

class readBooz:
    """
    A class to read an ./xbooz_xform netCDF output

    ...

    Attributes
    ----------
    path : str
        path to the directory in which the xbooz file is located
    name : str, optional
        name of the wout file being read.  Default is 'wout_HSX_main_opt0.nc'
    space_derivs : bool, optional
        read differential flux coordinates and include in fourier amplitudes.
        Default is False
    field_derivs : bool, optional
        read B field differentials necessary to perform B field curvature
        calculations and include in fourier amplitudes. Default is False

    Methods
    -------
    transForm_3D(u_dom, v_dom, ampKeys, errKeys=False)
        Performs 3D Fourier transform on specified keys

    transForm_2D_sSec(s_val, u_dom, v_dom, ampKeys, errKeys=False)
        Performs 2D Fourier transform along one flux surface on specified keys

    transForm_1D(s_val, u, v, ampKeys, errKeys=False)
        Performs 1D Fourier transform at a particular flux coordinate on
        specified keys

    transForm_listPoints(points, ampKeys, errKeys=False):
        Performs 1D Fourier transform at listed flux coordinates on
        specified keys

    boozerSpectra(ax, ampKey, nSpec=10, excFirst=True)
        Plots highest amplitude Fourier modes on specified keys

    Raises
    ------
    IOError
        Boozer and/or VMEC files do not exist.
    """

    def __init__(self, booz_path, vmec_path, space_derivs=False, field_derivs=False):
        try:
            booz_file = Dataset(booz_path, 'r')
        except IOError:
            raise IOError('Boozer file does not exists: ' + booz_path)

        try:
            vmec_file = Dataset(vmec_path, 'r')
        except IOError:
            raise IOError('VMEC file does not exists: ' + vmec_path)

        #for key in booz_file.variables.keys():
        #    print(key)

        self.booz_path = booz_path
        self.vmec_path = vmec_path

        self.space_derivs = space_derivs
        self.field_derivs = field_derivs

        # Read in VMEC Data #
        self.ns_vmec = vmec_file['/ns'][0]
        self.s_vmec = np.linspace(0, 1, self.ns_vmec)

        self.xm_vmec = vmec_file['/xm'][:]
        self.xn_vmec = vmec_file['/xn'][:]
        self.md_vmec = self.xm_vmec.shape[0]

        self.iota = spi.interp1d(self.s_vmec, vmec_file['/iotaf'][:])
        self.lambda_amps = spi.interp1d(self.s_vmec, vmec_file['/lmns'][:,:], axis=0)

        # Read in Boozer Data #
        ns = booz_file['/ns_b'][0] - 1
        nsurf = booz_file['/rmnc_b'][()].shape[0]

        self.ns_booz = booz_file['/ns_b'][0] - (1 + ns - nsurf)
        self.ds = self.s_vmec[1] - self.s_vmec[0]

        s_booz_beg = .5 * self.ds
        s_booz_end = 1 - (0.5 + ns - nsurf) * self.ds
        self.s_booz = np.linspace(s_booz_beg, s_booz_end, self.ns_booz)

        self.xm_booz = booz_file['/ixm_b'][:]
        self.xn_booz = booz_file['/ixn_b'][:]
        self.md_booz = self.xm_booz.shape[0]

        self.fourierAmps = {'R': spi.interp1d(self.s_booz, booz_file['/rmnc_b'][:, :], axis=0),
                            'Z': spi.interp1d(self.s_booz, booz_file['/zmns_b'][:, :], axis=0),
                            'P': spi.interp1d(self.s_booz, booz_file['/pmns_b'][:, :], axis=0),
                            'Jacobian': spi.interp1d(self.s_booz, booz_file['/gmn_b'][:, :], axis=0),
                            'Bmod': spi.interp1d(self.s_booz, booz_file['/bmnc_b'][:, :], axis=0)}

        self.cosine_keys = ['R', 'Jacobian', 'Bmod']
        self.sine_keys = ['Z', 'P']

        if space_derivs:
            self.fourierAmps['dR_ds'] = spi.interp1d(self.s_booz, np.gradient(booz_file['/rmnc_b'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dR_du'] = spi.interp1d(self.s_booz, -booz_file['/rmnc_b'][:, :]*self.xm_booz, axis=0)
            self.fourierAmps['dR_dv'] = spi.interp1d(self.s_booz, booz_file['/rmnc_b'][:, :]*self.xn_booz, axis=0)

            self.fourierAmps['dZ_ds'] = spi.interp1d(self.s_booz, np.gradient(booz_file['/zmns_b'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dZ_du'] = spi.interp1d(self.s_booz, booz_file['/zmns_b'][:, :]*self.xm_booz, axis=0)
            self.fourierAmps['dZ_dv'] = spi.interp1d(self.s_booz, -booz_file['/zmns_b'][:, :]*self.xn_booz, axis=0)

            self.fourierAmps['dP_ds'] = spi.interp1d(self.s_booz, np.gradient(booz_file['/pmns_b'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dP_du'] = spi.interp1d(self.s_booz, booz_file['/pmns_b'][:, :]*self.xm_booz, axis=0)
            self.fourierAmps['dP_dv'] = spi.interp1d(self.s_booz, -booz_file['/pmns_b'][:, :]*self.xn_booz, axis=0)

            self.fourierAmps['dL_du'] = spi.interp1d(self.s_vmec, vmec_file['/lmns'][:, :]*self.xm_vmec, axis=0)
            self.fourierAmps['dL_dv'] = spi.interp1d(self.s_vmec, -vmec_file['/lmns'][:, :]*self.xn_vmec, axis=0)

            self.cosine_keys.extend(['dR_ds', 'dZ_du', 'dZ_dv', 'dP_du', 'dP_dv', 'dL_du', 'dL_dv'])
            self.sine_keys.extend(['dR_du', 'dR_dv', 'dZ_ds', 'dP_ds'])

        if field_derivs:
            self.fourierAmps['dBmod_ds'] = spi.interp1d(self.s_booz, np.gradient(booz_file['/bmnc_b'][:, :], self.ds, axis=0), axis=0)
            self.fourierAmps['dBmod_du'] = spi.interp1d(self.s_booz, -booz_file['/bmnc_b'][:, :] * self.xm_booz, axis=0)
            self.fourierAmps['dBmod_dv'] = spi.interp1d(self.s_booz, booz_file['/bmnc_b'][:, :] * self.xn_booz, axis=0)

            self.cosine_keys.extend(['dBmod_ds'])
            self.sine_keys.extend(['dBmod_du', 'dBmod_dv'])

        vmec_file.close()
        booz_file.close()

    def vmec2booz_spectral_transform(self, vmec_key):
        """ Transform a VMEC spectrum to a Boozer spectrum.

        Parameters
        ----------
        vmec_key : str
            Scalar quantity whose spectrum will be transformed
        """
        # Construct VMEC Coodinate Flux Surface Mesh #
        s_num, u_num, v_num = self.ns_booz, 150, 600
        uv_num = u_num * v_num

        u_dom = np.linspace(-np.pi, np.pi, u_num)
        v_dom = np.linspace(-np.pi, np.pi, v_num)

        pol, tor = np.meshgrid(u_dom, v_dom)

        pol_xm_vmec = np.dot(self.xm_vmec.reshape(self.md_vmec, 1), pol.reshape(1, v_num * u_num))
        tor_xn_vmec = np.dot(self.xn_vmec.reshape(self.md_vmec, 1), tor.reshape(1, v_num * u_num))

        pol_xm_booz = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, v_num * u_num))
        tor_xn_booz = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, v_num * u_num))

        sin_mu_nv_vmec = np.sin(pol_xm_vmec - tor_xn_vmec)
        cos_mu_nv_vmec = np.cos(pol_xm_vmec - tor_xn_vmec)

        sin_mu_nv_booz = np.sin(pol_xm_booz - tor_xn_booz)
        cos_mu_nv_booz = np.cos(pol_xm_booz - tor_xn_booz)

        P = np.dot(self.fourierAmps['P'](self.s_booz), sin_mu_nv_booz).reshape(s_num, v_num, u_num)
        dP_du = np.dot(self.dP_du(self.s_booz), cos_mu_nv_booz).reshape(s_num, v_num, u_num)
        dP_dv = np.dot(self.dP_dv(self.s_booz), cos_mu_nv_booz).reshape(s_num, v_num, u_num)

        Lambda = np.dot(self.lambda_amps(self.s_booz), sin_mu_nv_vmec).reshape(s_num, v_num, u_num)
        dLambda_du = np.dot(self.dLambda_du(self.s_booz), cos_mu_nv_vmec).reshape(s_num, v_num, u_num)
        dLambda_dv = np.dot(self.dLambda_dv(self.s_booz), cos_mu_nv_vmec).reshape(s_num, v_num, u_num)

        iota_grid = np.tile(self.iota(self.s_booz), v_num).reshape(v_num, s_num).T
        iota_grid = np.repeat(iota_grid, u_num).reshape(s_num, v_num, u_num)

        jacob_vmec = (1 + dLambda_du) * (1 - dP_dv) - dP_du * (iota_grid - dLambda_dv)

        # Get Scalar Amplitudes From VMEC Wout File #
        vmec_path, vmec_name = os.path.split(self.vmec_path)

        space_keys = ['dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv']
        field_keys = ['Bu_contra', 'Bv_contra', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv',
                      'dBv_ds', 'dBv_du', 'dBmod_ds', 'dBmod_du', 'dBmod_dv']

        if any(ikey == vmec_key for ikey in space_keys):
            space_key = True
        else:
            space_key = False

        if any(ikey == vmec_key for ikey in field_keys):
            field_key = True
        else:
            field_key = False

        wout = wr.readWout(vmec_path, vmec_name, space_derivs=space_key, field_derivs=field_key)
        wout.transForm_3D(self.s_booz, u_dom, v_dom, [vmec_key])
        omega = wout.invFourAmps[vmec_key]

        # Transform to Boozer Coordinates #
        pol = np.repeat(pol.reshape(1, v_num, u_num), s_num, axis=0)
        tor = np.repeat(tor.reshape(1, v_num, u_num), s_num, axis=0)

        iota_grid = np.tile(self.iota(self.s_booz), v_num).reshape(v_num, s_num).T
        iota_grid = np.repeat(iota_grid, u_num).reshape(s_num, v_num, u_num)

        pol = pol + Lambda - iota_grid * P
        tor = tor - P

        # Perform Integration #
        if wout.nyq_limit:
            cosine_keys = wout.cosine_nyq_keys
            sine_keys = wout.sine_nyq_keys
        else:
            cosine_keys = wout.cosine_keys
            sine_keys = wout.sine_keys

        norm = 1. / (2*np.pi)**2

        if any(ikey == vmec_key for ikey in cosine_keys):
            self.cosine_keys.append(vmec_key)
            omega_booz = np.zeros((self.ns_booz, self.md_booz))

            for sdx in range(self.ns_booz):
                print('{}-{}'.format(sdx+1, self.ns_booz))
                pol_xm = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol[sdx].reshape(1, v_num * u_num))
                tor_xn = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor[sdx].reshape(1, v_num * u_num))

                for i in range(self.md_booz):
                    cos_mu_nv = np.cos(pol_xm - tor_xn)
                    print(jacob_vmec.shape)
                    print(omega.shape)
                    print(cos_mu_nv.shape)
                    # cos_mu_nv = np.cos(self.xn_booz[i] * tor - self.xm_booz[i] * pol)
                    integrand = jacob_vmec[sdx] * omega[sdx] * cos_mu_nv
                    omega_booz[sdx, i] = norm * np.trapz(np.trapz(integrand, v_dom, axis=1), u_dom, axis=1)

        elif any(ikey == vmec_key for ikey in sine_keys):
            self.sine_keys.append(vmec_key)
            omega_booz = np.zeros((self.ns_booz, self.md_booz))

            for sdx in range(self.ns_booz):
                pol_xm = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol[sdx].reshape(1, v_num * u_num))
                tor_xn = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor[sdx].reshape(1, v_num * u_num))

                for i in range(self.md_booz):
                    sin_mu_nv = np.sin(pol_xm - tor_xn).reshape(v_num, u_num)
                    # sin_mu_nv = np.sin(self.xn_booz[i] * tor[0] - self.xm_booz[i] * pol[0])
                    integrand = jacob_vmec[sdx] * omega[sdx] * sin_mu_nv
                    omega_booz[sdx, i] = norm * np.trapz(np.trapz(integrand, v_dom, axis=1), u_dom, axis=1)

        else:
            raise NameError('key = {} : is not available'.format(key))

        self.fourierAmps[vmec_key+'_trans'] = omega_booz


    def transForm_3D(self, s_dom, u_dom, v_dom, ampKeys):
        """ Performs 3D Fourier transform on specified keys

        Parameters
        ----------
        s_dom : array
            radial domain on which to perform Fourier transform
        u_dom : array
            poloidal domain on which to perform Fourier transform
        v_dom : array
            toroidal domain on which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        # Construct VMEC Coodinate Flux Surface Mesh #
        s_num = s_dom.shape[0]
        u_num = u_dom.shape[0]
        v_num = v_dom.shape[0]

        uv_num = u_num * v_num

        pol, tor = np.meshgrid(u_dom, v_dom)

        # print('xm: VMEC={}, BOOZ={}'.format(self.xm_vmec.shape, self.xm_booz.shape))
        # print('xn: VMEC={}, BOOZ={}'.format(self.xn_vmec.shape, self.xn_booz.shape))

        pol_xm_vmec = np.dot(self.xm_vmec.reshape(self.md_vmec, 1), pol.reshape(1, v_num * u_num))
        tor_xn_vmec = np.dot(self.xn_vmec.reshape(self.md_vmec, 1), tor.reshape(1, v_num * u_num))

        pol_xm_booz = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, v_num * u_num))
        tor_xn_booz = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, v_num * u_num))

        # Calculate Inverse VMEC Fourier Transforms #
        sin_mu_nv_vmec = np.sin(pol_xm_vmec - tor_xn_vmec)
        sin_mu_nv_booz = np.sin(pol_xm_booz - tor_xn_booz)

        Lambda = np.dot(self.lambda_amps(s_dom), sin_mu_nv_vmec).reshape(s_num, v_num, u_num)
        P = np.dot(self.fourierAmps['P'](s_dom), sin_mu_nv_booz).reshape(s_num, v_num, u_num)

        # Transform to Boozer Coordinates #
        pol = np.repeat(pol.reshape(1, v_num, u_num), s_num, axis=0)
        tor = np.repeat(tor.reshape(1, v_num, u_num), s_num, axis=0)

        iota_grid = np.tile(self.iota(s_dom), v_num).reshape(v_num, s_num).T
        iota_grid = np.repeat(iota_grid, u_num).reshape(s_num, v_num, u_num)

        pol = pol + Lambda - iota_grid * P
        tor = tor - P

        pol_xm = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, s_num * v_num * u_num)).reshape(self.md_booz, s_num, uv_num)
        tor_xn = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, s_num * v_num * u_num)).reshape(self.md_booz, s_num, uv_num)

        pol_xm = np.swapaxes(pol_xm, 0, 1)
        tor_xn = np.swapaxes(tor_xn, 0, 1)

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_dom)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.empty((s_num, v_num, u_num))
                for idx, s_val in enumerate(s_dom):
                    self.invFourAmps[key][idx] = np.dot(fourAmps[idx], cos_mu_nv[idx]).reshape(1, v_num, u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.empty((self.ns_booz, v_num, u_num))
                for idx, s_val in enumerate(s_dom):
                    self.invFourAmps[key][idx] = np.dot(fourAmps[idx], sin_mu_nv[idx]).reshape(1, v_num, u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_2D_sSec(self, s_val, u_dom, v_dom, ampKeys):
        """ Performs 2D Fourier transform along one flux surface on specified keys

        Parameters
        ----------
        s_val : float
            effective radius of flux surface on which Fourier tronsfrom will
            be performed
        u_dom : array
            poloidal domain on which to perform Fourier transform
        v_dom : array
            toroidal domain on which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        # Construct VMEC Coordinate Flux Surface Mesh #
        self.s_num = 1
        self.u_num = u_dom.shape[0]
        self.v_num = v_dom.shape[0]

        self.u_dom = u_dom
        self.v_dom = v_dom

        pol, tor = np.meshgrid(u_dom, v_dom)

        pol_xm_vmec = np.dot(self.xm_vmec.reshape(self.md_vmec, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn_vmec = np.dot(self.xn_vmec.reshape(self.md_vmec, 1), tor.reshape(1, self.v_num * self.u_num))

        pol_xm_booz = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn_booz = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, self.v_num * self.u_num))

        # Calculate Inverse VMEC Fourier Transforms #
        sin_mu_nv_vmec = np.sin(pol_xm_vmec - tor_xn_vmec)
        sin_mu_nv_booz = np.sin(pol_xm_booz - tor_xn_booz)

        Lambda = np.dot(self.lambda_amps(s_val), sin_mu_nv_vmec).reshape(1, self.v_num, self.u_num)
        P = np.dot(self.fourierAmps['P'](s_val), sin_mu_nv_booz).reshape(1, self.v_num, self.u_num)

        # Transform to Boozer Coordinates #
        pol = pol + Lambda - self.iota(s_val) * P
        tor = tor - P

        pol_xm = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, self.v_num * self.u_num))

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        self.pol_dom = pol
        self.tor_dom = tor

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.v_num, self.u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.v_num, self.u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def boozCoord_2D_sSec(self, s_val, u_dom, v_dom, ampKeys):
        """ Performs 2D Fourier transform along one flux surface on specified keys

        Parameters
        ----------
        s_val : float
            effective radius of flux surface on which Fourier tronsfrom will
            be performed
        u_dom : array
            poloidal domain on which to perform Fourier transform
        v_dom : array
            toroidal domain on which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        # Construct VMEC Coordinate Flux Surface Mesh #
        self.s_num = 1
        self.u_num = u_dom.shape[0]
        self.v_num = v_dom.shape[0]

        pol, tor = np.meshgrid(u_dom, v_dom)
        pol_xm = np.dot(self.xm_booz.reshape(self.md_booz, 1), pol.reshape(1, self.v_num * self.u_num))
        tor_xn = np.dot(self.xn_booz.reshape(self.md_booz, 1), tor.reshape(1, self.v_num * self.u_num))

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        self.pol_dom = pol
        self.tor_dom = tor

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.v_num, self.u_num)

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.v_num, self.u_num)

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_1D(self, s_val, u_val, v_val, ampKeys):
        """ Performs 1D Fourier transform at specified flux coordinates on
        specified keys

        Parameters
        ----------
        s_val : float
            effective radius at which to perform Fourier tronsfrom
        u_val : float
            poloidal coordinate at which to perform Fourier transform
        v_val : float
            toroidal coordinate at which to perform Fourier transform
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        self.s_num = 1
        self.u_num = 1
        self.v_num = 1

        # Construct VMEC Coordinate Mode Mesh #
        pol_xm_vmec = self.xm_vmec.reshape(self.md_vmec, 1) * u_val
        tor_xn_vmec = self.xn_vmec.reshape(self.md_vmec, 1) * v_val

        pol_xm_booz = self.xm_booz.reshape(self.md_booz, 1) * u_val
        tor_xn_booz = self.xn_booz.reshape(self.md_booz, 1) * v_val

        # Calculate Inverse VMEC Fourier Transforms #
        sin_mu_nv_vmec = np.sin(pol_xm_vmec - tor_xn_vmec)
        sin_mu_nv_booz = np.sin(pol_xm_booz - tor_xn_booz)

        Lambda = np.dot(self.lambda_amps(s_val), sin_mu_nv_vmec)[0]
        P = np.dot(self.fourierAmps['P'](s_val), sin_mu_nv_booz)[0]

        # Transform to Boozer Coordinates #
        u_val = u_val + Lambda - self.iota(s_val) * P
        v_val = v_val - P

        pol_xm = self.xm_booz.reshape(self.md_booz, 1) * u_val
        tor_xn = self.xn_booz.reshape(self.md_booz, 1) * v_val

        cos_mu_nv = np.cos(pol_xm - tor_xn)
        sin_mu_nv = np.sin(pol_xm - tor_xn)

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        for key in ampKeys:

            fourAmps = self.fourierAmps[key](s_val)

            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv)[0]

            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv)[0]

            else:
                raise NameError('key = {} : is not available'.format(key))

    def transForm_listPoints(self, points, ampKeys):
        """ Performs 1D Fourier transform at specified flux coordinates on
        specified keys

        Parameters
        ----------
        points : array
            List of flux coordinates to perform 1D transform on, ordered as
            {s,u,v}
        ampKeys : list
            keys specifying Fourier amplitudes to be transformed

        Raises
        ------
        NameError
            at least on key in ampKeys is not an available Fourier amplitude.
        """
        self.invFourAmps = {}
        for key in ampKeys:
            self.invFourAmps[key] = np.empty(points.shape[0])

        self.s_dom = points[:, 0]
        self.u_dom = np.empty(points.shape[0])
        self.v_dom = np.empty(points.shape[0])

        for p, point in enumerate(points):
            s, u, v = point[0], point[1], point[2]

            # Construct VMEC Coordinate Mode Mesh #
            pol_xm_vmec = self.xm_vmec.reshape(self.md_vmec, 1) * u
            tor_xn_vmec = self.xn_vmec.reshape(self.md_vmec, 1) * v

            pol_xm_booz = self.xm_booz.reshape(self.md_booz, 1) * u
            tor_xn_booz = self.xn_booz.reshape(self.md_booz, 1) * v

            # Calculate Inverse VMEC Fourier Transforms #
            sin_mu_nv_vmec = np.sin(pol_xm_vmec - tor_xn_vmec)
            sin_mu_nv_booz = np.sin(pol_xm_booz - tor_xn_booz)

            Lambda = np.dot(self.lambda_amps(s), sin_mu_nv_vmec)[0]
            P = np.dot(self.fourierAmps['P'](s), sin_mu_nv_booz)[0]

            # Transform to Boozer Coordinates #
            u = u + Lambda - self.iota(s) * P
            v = v - P

            self.u_dom[p], self.v_dom[p] = u, v

            pol_xm = self.xm_booz.reshape(self.md_booz, 1) * u
            tor_xn = self.xn_booz.reshape(self.md_booz, 1) * v

            cos_mu_nv = np.cos(pol_xm - tor_xn)
            sin_mu_nv = np.sin(pol_xm - tor_xn)

            # Calculate Inverse Boozer Fourier Transforms #
            for key in ampKeys:

                fourAmps = self.fourierAmps[key](s)

                if any(ikey == key for ikey in self.cosine_keys):
                    self.invFourAmps[key][p] = np.dot(fourAmps, cos_mu_nv)[0]

                elif any(ikey == key for ikey in self.sine_keys):
                    self.invFourAmps[key][p] = np.dot(fourAmps, sin_mu_nv)[0]

                else:
                    raise NameError('key = {} : is not available'.format(key))

    def metric_tensor_2D_sSec(self, s_vmec, u_vmec, v_vmec):
        """ Calculate the metric tensor components.
        """
        if not self.space_derivs:
            raise KeyError("space_derivs flag must be set to True")

        # Construct VMEC Coordinate Flux Surface Mesh #
        self.s_num = 1
        self.u_num = u_vmec.shape[0]
        self.v_num = v_vmec.shape[0]

        self.u_vmec = u_vmec
        self.v_vmec = v_vmec

        u_vmec_grid, v_vmec_grid = np.meshgrid(u_vmec, v_vmec)
        u_vmec_grid = u_vmec_grid.reshape(1, self.v_num * self.u_num)
        v_vmec_grid = v_vmec_grid.reshape(1, self.v_num * self.u_num)

        u_xm_vmec = np.dot(self.xm_vmec.reshape(self.md_vmec, 1), u_vmec_grid)
        v_xn_vmec = np.dot(self.xn_vmec.reshape(self.md_vmec, 1), v_vmec_grid)

        u_xm_booz = np.dot(self.xm_booz.reshape(self.md_booz, 1), u_vmec_grid)
        v_xn_booz = np.dot(self.xn_booz.reshape(self.md_booz, 1), v_vmec_grid)

        # Calculate Inverse VMEC Fourier Transforms #
        sin_mu_nv_vmec = np.sin(u_xm_vmec - v_xn_vmec)
        cos_mu_nv_vmec = np.cos(u_xm_vmec - v_xn_vmec)

        sin_mu_nv_booz = np.sin(u_xm_booz - v_xn_booz)
        cos_mu_nv_booz = np.cos(u_xm_booz - v_xn_booz)

        L = np.dot(self.lambda_amps(s_vmec), sin_mu_nv_vmec).reshape(1, self.v_num, self.u_num)
        dL_du = np.dot(self.fourierAmps['dL_du'](s_vmec), cos_mu_nv_vmec).reshape(self.v_num, self.u_num)
        dL_dv = np.dot(self.fourierAmps['dL_dv'](s_vmec), cos_mu_nv_vmec).reshape(self.v_num, self.u_num)

        P = np.dot(self.fourierAmps['P'](s_vmec), sin_mu_nv_booz).reshape(1, self.v_num, self.u_num)
        dP_ds = np.dot(self.fourierAmps['dP_ds'](s_vmec), sin_mu_nv_booz).reshape(self.v_num, self.u_num)
        dP_du = np.dot(self.fourierAmps['dP_du'](s_vmec), cos_mu_nv_booz).reshape(self.v_num, self.u_num)
        dP_dv = np.dot(self.fourierAmps['dP_dv'](s_vmec), cos_mu_nv_booz).reshape(self.v_num, self.u_num)

        # Transform to Boozer Coordinates #
        L = L.reshape(1, self.v_num * self.u_num)
        P = P.reshape(1, self.v_num * self.u_num)

        u_booz_grid = u_vmec_grid + L - self.iota(s_vmec) * P
        v_booz_grid = v_vmec_grid - P

        u_xm_booz = np.dot(self.xm_booz.reshape(self.md_booz, 1), u_booz_grid)
        v_xn_booz = np.dot(self.xn_booz.reshape(self.md_booz, 1), v_booz_grid)

        cos_mu_nv = np.cos(u_xm_booz - v_xn_booz)
        sin_mu_nv = np.sin(u_xm_booz - v_xn_booz)

        # Calculate Inverse Boozer Fourier Transforms #
        self.invFourAmps = {}
        ampKeys = ['R', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv']
        for key in ampKeys:
            fourAmps = self.fourierAmps[key](s_vmec)
            if any(ikey == key for ikey in self.cosine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, cos_mu_nv).reshape(self.v_num, self.u_num)
            elif any(ikey == key for ikey in self.sine_keys):
                self.invFourAmps[key] = np.dot(fourAmps, sin_mu_nv).reshape(self.v_num, self.u_num)
            else:
                raise NameError('key = {} : is not available'.format(key))

        # Retrieve Coordinates and Derivatives #
        R = self.invFourAmps['R']
        R2 = R**2

        dR_ds = self.invFourAmps['dR_ds']
        dR_du = self.invFourAmps['dR_du']
        dR_dv = self.invFourAmps['dR_dv']

        dZ_ds = self.invFourAmps['dZ_ds']
        dZ_du = self.invFourAmps['dZ_du']
        dZ_dv = self.invFourAmps['dZ_dv']

        iota = self.iota(s_vmec)
        iota_inv = 1./iota

        dPhi_ds = dP_ds
        dPhi_dv = (1 + dL_du) / ((1 + dL_du)*(1 - dP_ds) - dP_du*(iota - dL_dv))
        dPhi_du = iota_inv * dPhi_dv

        # Calculate Covariant Metric Components #
        self.gss = dR_ds**2 + dZ_ds**2 #+ R2 * (dPhi_ds**2)
        self.gsu = dR_ds * dR_du + dZ_ds * dZ_du #+ R2 * dPhi_ds * dPhi_du
        self.gsv = dR_ds * dR_dv + dZ_ds * dZ_dv #+ R2 * dPhi_ds * dPhi_dv
        self.guu = dR_du**2 + dZ_du**2 #+ R2 * (dPhi_du**2)
        self.guv = dR_du * dR_dv + dZ_du * dZ_dv #+ R2 * dPhi_du * dPhi_dv
        self.gvv = dR_dv**2 + dZ_dv**2 + R2 #* (dPhi_dv**2)

        # Calculate Contravariant Metric Components #
        """
        jacob_inv = 1. / (R * R * ((dR_du * dZ_ds - dR_ds * dZ_du)**2))
        self.g_ss = jacob_inv * ((dR_du*dZ_dv - dR_dv*dZ_du)**2
                                 + (R**2) * (dR_du**2 + dZ_du**2))
        self.g_uu = jacob_inv * ((dR_ds*dZ_dv - dR_dv*dZ_ds)**2
                                 + (R**2) * (dZ_ds**2 + dR_ds**2))
        self.g_vv = jacob_inv * ((dR_ds*dZ_du - dR_du*dZ_ds)**2)
        self.g_su = jacob_inv * ((dR_ds*dZ_dv - dR_dv*dZ_ds)
                                 * (dR_dv*dZ_du - dR_du*dZ_dv)
                                 - (R**2) * (dZ_ds*dZ_du + dR_ds*dR_du))
        self.g_sv = jacob_inv * ((dR_du*dZ_dv - dR_dv*dZ_du)
                                 * (dR_ds*dZ_du - dR_du*dZ_ds))
        self.g_uv = jacob_inv * ((dR_ds*dZ_dv - dR_dv*dZ_ds)
                                 * (dR_du*dZ_ds - dR_ds*dZ_du))
        """
        self.g_ss = np.empty(self.gss.shape)
        self.g_su = np.empty(self.gsu.shape)
        self.g_sv = np.empty(self.gsv.shape)
        self.g_uu = np.empty(self.guu.shape)
        self.g_uv = np.empty(self.guv.shape)
        self.g_vv = np.empty(self.gvv.shape)

        for i in range(self.v_num):
            for j in range(self.u_num):
                gss = self.gss[i, j]
                gsu = self.gsu[i, j]
                gsv = self.gsv[i, j]
                guu = self.guu[i, j]
                guv = self.guv[i, j]
                gvv = self.gvv[i, j]

                mat = np.array([[gss, gsu, gsv],
                                [gsu, guu, guv],
                                [gsv, guv, gvv]])
                inv_mat = np.linalg.inv(mat)

                self.g_ss[i, j] = inv_mat[0, 0]
                self.g_su[i, j] = inv_mat[0, 1]
                self.g_sv[i, j] = inv_mat[0, 2]
                self.g_uu[i, j] = inv_mat[1, 1]
                self.g_uv[i, j] = inv_mat[1, 2]
                self.g_vv[i, j] = inv_mat[2, 2]

    def follow_field_line(self, s, alpha, nz, n_pol, ampKeys):
        """ Follow a magnetic field line following the GIST specification parameters.

        Parameters
        ----------
        s : float
            normalized toroidal flux magnetic surface label.  Mathces VMEC "s".
        alpha : float
            Clebsch representation field line label. Goes from 0 to 2pi.
        nz : int
            Number of grid points along field line.  GIST "z" coordinate.
        n_pol : float
            Number of poloidal rotations for field line following.
        """
        self.pol_dom = np.pi * np.linspace(-n_pol, n_pol, nz)
        # self.tor_dom = (1. / self.iota(s)) * (self.pol_dom - alpha/np.sqrt(s))
        self.tor_dom = (self.pol_dom - alpha) / self.iota(s)

        self.invFourAmps = {}
        for key in ampKeys:
            self.invFourAmps[key] = np.empty(nz)

        for i in range(nz):
            pol_xm = self.xm_booz.reshape(self.md_booz, 1) * self.pol_dom[i]
            tor_xn = self.xn_booz.reshape(self.md_booz, 1) * self.tor_dom[i]

            cos_mu_nv = np.cos(pol_xm - tor_xn)
            sin_mu_nv = np.sin(pol_xm - tor_xn)

            for key in ampKeys:

                fourAmps = self.fourierAmps[key](s)

                if any(ikey == key for ikey in self.cosine_keys):
                    self.invFourAmps[key][i] = np.dot(fourAmps, cos_mu_nv)[0]

                elif any(ikey == key for ikey in self.sine_keys):
                    self.invFourAmps[key][i] = np.dot(fourAmps, sin_mu_nv)[0]

                else:
                    raise NameError('key = {} : is not available'.format(key))

    def flux_tube_geometry(self, s0, a0, pram_file, ampKeys, Ninterp=0):
        """ Calculate quantities in a GENE flux-tube geometry.

        Parameters
        ----------
        s0 : float
            Normalized flux-surface label that defines central field-line.
        a0 : float
            Field-line label that defines the central field_line.
        pram_file : str
            Absolute path to parameters file output by GENE.
        ampKeys : list
            List of quantities to be calculated.
        """
        import geneAnalysis.read_parameters as rp
        import scipy.constants as const

        import matplotlib as mpl
        import matplotlib.pyplot as plt

        # Plotting Parameters #
        plt.close('all')

        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': 18}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = 22
        mpl.rcParams['lines.linewidth'] = 2
        #######################

        pram = rp.read_file(pram_file)

        lx = pram.data_dict['lx']
        ly = pram.data_dict['ly']
        nx0 = int(pram.data_dict['nx0'])
        nky0 = int(pram.data_dict['nky0'])
        nz0 = int(pram.data_dict['nz0'])

        Nx = nx0
        Ny = 2*nky0
        Nz = nz0 * (1 + Ninterp) - Ninterp

        q0 = pram.data_dict['q0']
        shat = pram.data_dict['shat']
        n_pol = pram.data_dict['n_pol']
        mref = const.m_p * pram.data_dict['mref']
        Tref = 1e3 * const.e * pram.data_dict['Tref']
        Lref = pram.data_dict['Lref']
        Bref = pram.data_dict['Bref']
        rho_ref = np.sqrt(mref*Tref)/(const.e*Bref)

        hlf_stp = np.pi * n_pol / (nz0 - 1)
        pol_dom = np.pi * np.linspace(-n_pol, n_pol, Nz)
        pol_dom = pol_dom - hlf_stp

        # x_dom = np.linspace(0, lx, nx0+1) - 0.5*lx
        # y_dom = np.linspace(0, ly, 2*nky0+1) - 0.5*ly

        x_dom = 0.5*lx*np.linspace(-1., (Nx-3)/(Nx-1), Nx)
        y_dom = 0.5*ly*np.linspace(-1, (Ny-3)/(Ny-1), Ny)

        x_scale = (2*np.sqrt(s0)*rho_ref)/(q0*Lref)
        y_scale = (q0*rho_ref)/(np.sqrt(s0)*Lref)

        s_dom = x_scale * x_dom + s0
        s_dom = s_dom[(s_dom >= self.s_booz[0]) & (s_dom <= self.s_booz[-1])]
        x_dom = s_dom[(s_dom >= self.s_booz[0]) & (s_dom <= self.s_booz[-1])]
        q_rng = q0*(1 + (s_dom - s0)*(shat/(2*s0)))

        ypts = y_dom.shape[0]
        xpts = x_dom.shape[0]

        cart_points = np.empty((pol_dom.shape[0], ypts, xpts, 4))
        cart_points[:, :, :, :] = np.nan
        for pol_idx, pol in enumerate(pol_dom):
            print('theta = {0:0.4f} ({1:0.0f}|{2:0.0f})'.format(pol, pol_idx+1, Nz))

            self.invFourAmps = {}
            for key in ampKeys:
                self.invFourAmps[key] = np.empty((ypts, xpts))
                self.invFourAmps[key][:, :] = np.nan

            for ydx, y in enumerate(y_dom):
                for sdx, s in enumerate(s_dom):
                    tor = y_scale * y + q_rng[sdx]*pol - a0
                    pol_xm = self.xm_booz.reshape(self.md_booz, 1) * pol
                    tor_xn = self.xn_booz.reshape(self.md_booz, 1) * tor

                    cos_mu_nv = np.cos(pol_xm - tor_xn)
                    sin_mu_nv = np.sin(pol_xm - tor_xn)

                    for key in ampKeys:

                        if key == 'Phi':
                            P = np.dot(self.fourierAmps['P'](s), sin_mu_nv)[0]
                            self.invFourAmps[key][ydx, sdx] = tor + P

                        else:
                            fourAmps = self.fourierAmps[key](s)

                            if any(ikey == key for ikey in self.cosine_keys):
                                self.invFourAmps[key][ydx, sdx] = np.dot(fourAmps, cos_mu_nv)[0]

                            elif any(ikey == key for ikey in self.sine_keys):
                                self.invFourAmps[key][ydx, sdx] = np.dot(fourAmps, sin_mu_nv)[0]

                            else:
                                raise NameError('key = {} : is not available'.format(key))

            X = self.invFourAmps['R'] * np.cos(self.invFourAmps['Phi'])
            Y = self.invFourAmps['R'] * np.sin(self.invFourAmps['Phi'])

            cart_points[pol_idx, :, :, 0] = X
            cart_points[pol_idx, :, :, 1] = Y
            cart_points[pol_idx, :, :, 2] = self.invFourAmps['Z']
            cart_points[pol_idx, :, :, 3] = self.invFourAmps['Bmod']

        self.cart_points = cart_points

        self.gene_x = x_dom
        self.gene_y = y_dom
        self.pol_dom = pol_dom

        self.q0 = q0
        self.s0 = s0
        self.alpha0 = a0
        self.shat = shat
        self.Lref = Lref
        self.Bref = Bref
        self.rho_ref = rho_ref

        if False:
            fig, axs = plt.subplots(2, 2, sharex=True, sharey=True, figsize=(16, 12))

            ax1 = axs[0, 0]
            ax2 = axs[0, 1]
            ax3 = axs[1, 0]
            ax4 = axs[1, 1]

            pmap1 = ax1.pcolormesh(x_dom, y_dom, cart_points[0, :, :, 0], cmap='jet')
            pmap2 = ax2.pcolormesh(x_dom, y_dom, cart_points[0, :, :, 1], cmap='jet')
            pmap3 = ax3.pcolormesh(x_dom, y_dom, cart_points[0, :, :, 2], cmap='jet')
            pmap4 = ax4.pcolormesh(x_dom, y_dom, cart_points[0, :, :, 3], cmap='jet')

            cmap1 = fig.colorbar(pmap1, ax=ax1)
            cmap2 = fig.colorbar(pmap2, ax=ax2)
            cmap3 = fig.colorbar(pmap3, ax=ax3)
            cmap4 = fig.colorbar(pmap4, ax=ax4)

            cmap1.ax.set_ylabel(r'$X \ (m)$')
            cmap2.ax.set_ylabel(r'$Y \ (m)$')
            cmap3.ax.set_ylabel(r'$Z \ (m)$')
            cmap4.ax.set_ylabel(r'$B \ (T)$')

            ax1.set_ylabel(r'$y/\rho_s$')
            ax3.set_ylabel(r'$y/\rho_s$')
            ax3.set_xlabel(r'$x/\rho_s$')
            ax4.set_xlabel(r'$x/\rho_s$')

            plt.show()

    def plot_spectrum(self, plot, s_dom, nSpec=10, excFirst=True, savePath=None):
        """ Plots the most prominant modes according to the Fourier amplitude
        specified.  The first mode is excluded by default.

        Parameters
        ----------
        ax : object
            axis on which to plot the spectrum
        s_dom : arr
            1D array of s values along which the spectrum will be calculated
        ampKey : str
            key specifying Fourier amplitude to be plotted
        nSpec : int
            number of modes to plot (default is 10)
        excFirst : boolean
            defaults to exclude the first mode (default is True)
        """
        if excFirst:
            io = 1
        else:
            io = 0

        specData = {}
        specOrder = []

        full_spectrum = self.fourierAmps['Bmod'](s_dom)
        if self.md_booz > io+nSpec:
            for i in range(io+nSpec):
                key = '({0}, {1})'.format(int(self.xn_booz[i]), int(self.xm_booz[i]))
                spec = full_spectrum[:, i]
                specMax = np.abs(np.trapz(spec, s_dom))

                specData[key] = spec
                specOrder.append([specMax, key])

            specOrder = np.array(specOrder, dtype=object)
            for i in range(io+nSpec, self.md_booz):
                key = '({0}, {1})'.format(int(self.xn_booz[i]), int(self.xm_booz[i]))
                spec = full_spectrum[:, i]
                specMax = np.abs(np.trapz(spec, s_dom))

                if specMax > np.min(specOrder[:, 0]):
                    idx = np.argmin(specOrder[:, 0])
                    rm_key = specOrder[idx, 1]
                    del specData[rm_key]

                    specData[key] = spec
                    specOrder[idx] = np.array([specMax, key], dtype=object)

        else:
            for i in range(0, self.md_booz):
                key = '({0}, {1})'.format(int(self.xn_booz[i]), int(self.xm_booz[i]))
                spec = self.fourierAmps['Bmod'](s_dom)
                specMax = np.abs(np.trapz(spec, s_dom))

                specData[key] = spec
                specOrder.append([specMax, key])

        specOrder = sorted(specOrder, key=lambda x: x[0], reverse=True)

        fig, ax = plot.construct_axis(figsize=(10, 8))

        spec_min = np.empty(nSpec)
        spec_max = np.empty(nSpec)
        for i in range(nSpec):
            key = specOrder[io+i][1]
            spec = specData[key]

            spec_min[i] = np.min(spec)
            spec_max[i] = np.max(spec)

            ax.plot(s_dom, spec, label=key)

        spec_min = 1.1 * np.min(spec_min)
        spec_max = 1.1 * np.max(spec_max)

        ax.set_xlabel(r'$\hat{\psi}$')
        ax.set_ylabel(r'$B_{nm}$')

        ax.set_xlim(s_dom[0], s_dom[-1])
        ax.set_ylim(spec_min, spec_max)

        box = plot.ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 1, box.height])

        ax.legend(loc='upper left', title=r'$(nN_{fp},\, m)$', bbox_to_anchor=(1.01, 1))

        ax.grid()
        plot.plt.tight_layout()

        if savePath is None:
            plot.plt.show()
        else:
            plot.plt.savefig(savePath)


if __name__ == "__main__":
    import os, sys
    WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
    sys.path.append(WORKDIR)

    import matplotlib as mpl
    import matplotlib.pyplot as plt

    import plot_define as pd
    import gistTools.gist_reader as gr

    import vtkTools.vtk_grids as vtkG
    import vmecTools.wout_files.wout_read as wr
    import coord_convert as cc

    import h5py as hf
    # import matplotlib as mpl
    # import matplotlib.pyplot as plt

    if False:
        # 3D Field Line VTK #
        path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0')
        gist_path = os.path.join(path, 'gist_s0p5_alpha0_npol4_nz8192.dat')
        booz_path = os.path.join(path, 'boozmn_wout_HSX_main_opt0.nc')
        vmec_path = os.path.join(path, 'wout_HSX_main_opt0.nc')

        booz = readBooz(booz_path, vmec_path)
        gist = gr.read_gist(gist_path)

        u_dom = np.linspace(-np.pi, np.pi, 151)
        v_dom = np.linspace(-np.pi, np.pi, 501)

        booz.transForm_2D_sSec(gist.s0, u_dom, v_dom, ['R', 'Z', 'Bmod'])
        R_2D = booz.invFourAmps['R']
        Z_2D = booz.invFourAmps['Z']
        Bmod_2D = booz.invFourAmps['Bmod']
        cart_coord = cc.cartCoord(booz)

        pol_dom = gist.pol_dom
        tor_dom = pol_dom * gist.q0
        s_vals = np.full(pol_dom.shape, gist.s0)
        points = np.stack((s_vals, pol_dom, tor_dom), axis=1)
        booz.transForm_listPoints(points, ['R', 'Z'])

        R_1D = booz.invFourAmps['R']
        Z_1D = booz.invFourAmps['Z']
        X_1D = R_1D * np.cos(tor_dom)
        Y_1D = R_1D * np.sin(tor_dom)
        cart_line = np.stack((X_1D, Y_1D, Z_1D), axis=1)

        path = os.path.join('/home', 'michael', 'onedrive', 'Figures', 'vtk_files', 'HSX', 'QHS')
        name = 'booz_modB_psiN_0p5.vtk'
        vtkG.scalar_mesh(path, name, cart_coord, Bmod_2D)

        name = 'booz_line_psiN_0p5.vtk'
        vtkG.scalar_line_mesh(path, name, cart_line, np.ones(cart_line.shape[0]))

    if True:
        # Plot Spectrum #
        config_id = '1020-1-1'
        main_id = 'main_coil_{}'.format(config_id.split('-')[0])
        set_id = 'set_{}'.format(config_id.split('-')[1])
        job_id = 'job_{}'.format(config_id.split('-')[2])

        booz_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'boozmn_wout_%s_mn1824_ns101.nc' % config_id)
        vmec_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_%s_mn1824_ns101.nc' % config_id)

        booz = readBooz(booz_path, vmec_path)

        plot = pd.plot_define(fontSize=20, labelSize=24, lineWidth=2)
        # fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(10, 8))

        save_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'booz_spec_mn1824.png')
        s_dom = booz.s_booz[(booz.s_booz >= 0.645) & (booz.s_booz <= 0.655)]
        booz.plot_spectrum(plot, s_dom)  # , savePath=save_path)
        # booz.plot_spectrum(plot)

    if False:
        # Plot 2D Flux Surface #
        path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data')
        booz_path = os.path.join(path, 'boozmn_wout_QHS_best.nc')
        vmec_path = os.path.join(path, 'wout_QHS_best.nc')

        booz = readBooz(booz_path, vmec_path)

        pol_dom = np.linspace(-np.pi, np.pi, 121)
        tor_dom = np.linspace(-np.pi, -np.pi/2, pol_dom.shape[0])
        booz.boozCoord_2D_sSec(0.2, pol_dom, tor_dom, ['Bmod'])
        B_show = booz.invFourAmps['Bmod']

        plot = pd.plot_define(fontSize=20, labelSize=24, lineWidth=2, figSize=(10, 8))
        fig, ax = plot.fig, plot.ax

        s_map = ax.pcolormesh(tor_dom/np.pi, pol_dom/np.pi, B_show.T, vmin=np.min(B_show), vmax=np.max(B_show), cmap=plt.get_cmap('jet'))

        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'$B \ (T)$')

        ax.set_xlabel(r'$\zeta / \pi$')
        ax.set_ylabel(r'$\theta / \pi$')

        ax.set_title(r'$\Psi_N = 0.2$')
        # ax.set_title(r'$r/a = $'+'{0:0.1f}'.format(np.sqrt(rEff)))

        plt.tight_layout()
        plt.show()

    if False:
        # Plot Particle Trajectory #
        traj_path = os.path.join('/home','michael','Desktop','trajData.h5')
        with hf.File(traj_path, 'r') as hf_:
            traj_data = hf_['trajectory'][()]

        s_val = np.mean(traj_data[:,0])

        # Define Paths #
        base_path = os.path.join('/mnt','HSX_Database','HSX_Configs','main_coil_0','set_1','job_0')
        booz_path = os.path.join(base_path, 'boozmn_wout_HSX_main_opt0.nc')
        vmec_path = os.path.join(base_path, 'wout_HSX_main_opt0.nc')

        # Construct Boozer and VMEC Objects #
        booz = readBooz(booz_path, vmec_path)

        booz.transForm_listPoints(traj_data, ['Bmod'])
        pol_traj = booz.u_dom
        tor_traj = booz.v_dom
        Bmod_traj = booz.invFourAmps['Bmod']

        pol_norm = np.empty(pol_traj.shape)
        tor_norm = np.empty(tor_traj.shape)
        for idx, pol in enumerate(pol_traj):
            pol = pol / np.piinvFourAmps
            if pol <= 1:
                pol_norm[idx] = pol
            else:
                pol_norm[idx] = pol - 2

            tor = tor_traj[idx] / np.pi
            if tor <= 1:
                tor_norm[idx] = tor
            else:
                tor_norm[idx] = tor - 2

            tor = tor_traj[idx] / np.pi

        # Get Toroidal and Poloidal B_field Magnitude #
        upts = 151
        u_dom = np.linspace(-np.pi, np.pi, upts)
        v_dom = np.linspace(-np.pi, np.pi, 4*upts)

        booz.transForm_2D_sSec(s_val, u_dom, v_dom, ['Bmod'])

        # Generate Flat Torus Plot #
        plot = pd.plot_define(figSize=(12,8), lineWidth=2)
        plt, fig, ax = plot.plt, plot.fig, plot.ax
        """
        s_map = ax.pcolormesh(v_dom/np.pi, u_dom/np.pi, booz.invFourAmps['Bmod'].T, cmap='jet')
        ax.plot(tor_norm, pol_norm, c='k')

        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'$|B| \, (T)$')
        """
        plt.plot(tor_traj, Bmod_traj)

        ax.set_xlabel(r'$\zeta/\pi$')
        ax.set_ylabel(r'$\theta/\pi$')

        #plt.show()
        savePath = os.path.join('/home','michael','onedrive','traj_plot.png')
        plt.savefig(savePath)

    if False:
        # conIDs = ['5-1-485','779-1-9','897-1-0']
        # conIDs = ['1020-1-85', '61-1-26']
        conIDs = ['0-1-0']

        for conID in conIDs:
            mainID = conID.split('-')[0]
            setID = conID.split('-')[1]
            jobID = conID.split('-')[2]

            # Define Paths #
            # base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_'+mainID, 'set_'+setID, 'job_'+jobID)
            # booz_path = os.path.join(base_path, 'boozmn_wout_HSX_main_opt0_54_72.nc')
            # vmec_path = os.path.join(base_path, 'wout_HSX_main_opt0.nc')

            base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data')
            booz_path = os.path.join(base_path, 'boozmn_wout_QHS_best.nc')
            vmec_path = os.path.join(base_path, 'wout_QHS_best.nc')

            # Construct Boozer and VMEC Objects #
            booz = readBooz(booz_path, vmec_path)
            vmec = wr.readWout(base_path, name='wout_QHS_best.nc')

            npts = 251  # booz.ns_booz
            s_dom = np.full(npts, booz.s_booz[-1])
            u_dom = np.linspace(-np.pi, np.pi, npts)
            v_dom = np.full(npts, 0.*np.pi)
            points = np.stack((s_dom, u_dom, v_dom), axis=1)

            booz.transForm_listPoints(points, ['R', 'Z'])
            vmec.transForm_listPoints(points, ['R', 'Z'])

            R_booz = booz.invFourAmps['R']
            Z_booz = booz.invFourAmps['Z']

            R_vmec = vmec.invFourAmps['R']
            Z_vmec = vmec.invFourAmps['Z']

            # Plotting Parameters #
            plt.close('all')

            font = {'family': 'sans-serif',
                    'weight': 'normal',
                    'size': 18}

            mpl.rc('font', **font)

            mpl.rcParams['axes.labelsize'] = 22
            mpl.rcParams['lines.linewidth'] = 2
            #########################

            fig, axs = plt.subplots(2, 1, sharex=True, sharey=False, tight_layout=True, figsize=(8, 6))

            axs[0].plot(u_dom/np.pi, booz.v_dom/np.pi, c='k')
            axs[1].plot(u_dom/np.pi, booz.u_dom/np.pi, c='k')

            axs[0].set_xlim(-1, 1)

            axs[1].set_xlabel(r'$\theta_V/\pi$')
            axs[0].set_ylabel(r'$\zeta_B/\pi$')
            axs[1].set_ylabel(r'$\theta_B/\pi$')

            axs[0].set_title(r'$\zeta_V = \pi/4$')
            axs[0].grid()
            axs[1].grid()

            plt.show()

    if False:
        # conIDs = ['5-1-485','779-1-9','897-1-0']
        # conIDs = ['1020-1-85', '61-1-26']
        conIDs = ['0-1-0']

        for conID in conIDs:
            mainID = conID.split('-')[0]
            setID = conID.split('-')[1]
            jobID = conID.split('-')[2]

            # Define Paths #
            # base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_'+mainID, 'set_'+setID, 'job_'+jobID)
            # booz_path = os.path.join(base_path, 'boozmn_wout_HSX_main_opt0_54_72.nc')
            # vmec_path = os.path.join(base_path, 'wout_HSX_main_opt0.nc')

            base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data')
            booz_path = os.path.join(base_path, 'boozmn_wout_QHS_best.nc')
            vmec_path = os.path.join(base_path, 'wout_QHS_best.nc')

            # Construct Boozer and VMEC Objects #
            booz = readBooz(booz_path, vmec_path)
            vmec = wr.readWout(base_path, name='wout_QHS_best.nc')

            npts = booz.ns_booz
            s_dom = booz.s_booz
            u_dom = np.full(npts, 0.375*np.pi)
            v_dom = np.full(npts, 0.125*np.pi)
            points = np.stack((s_dom, u_dom, v_dom), axis=1)

            booz.transForm_listPoints(points, ['R', 'Z'])
            vmec.transForm_listPoints(points, ['R', 'Z'])

            R_booz = booz.invFourAmps['R']
            Z_booz = booz.invFourAmps['Z']

            R_vmec = vmec.invFourAmps['R']
            Z_vmec = vmec.invFourAmps['Z']

            # Plotting Parameters #
            plt.close('all')

            font = {'family': 'sans-serif',
                    'weight': 'normal',
                    'size': 18}

            mpl.rc('font', **font)

            mpl.rcParams['axes.labelsize'] = 22
            mpl.rcParams['lines.linewidth'] = 2
            #########################

            fig, axs = plt.subplots(2, 1, sharex=True, sharey=False, tight_layout=True, figsize=(8, 6))

            axs[0].plot(s_dom, R_vmec[:], c='k', label='VMEC')
            axs[0].plot(s_dom, R_booz[:], c='tab:red', label='Boozer')

            axs[1].plot(s_dom, Z_vmec[:], c='k', label='VMEC')
            axs[1].plot(s_dom, Z_booz[:], c='tab:red', label='Boozer')

            axs[0].set_xlim(0, 1)

            axs[0].set_ylabel(r'R (m)')
            axs[1].set_ylabel(r'Z (m)')
            axs[1].set_xlabel(r'$\psi_N$')

            axs[0].legend()

            axs[0].grid()
            axs[1].grid()

            plt.show()

    if False:
        # conIDs = ['5-1-485','779-1-9','897-1-0']
        # conIDs = ['1020-1-85', '61-1-26']
        conIDs = ['0-1-0']

        for conID in conIDs:
            mainID = conID.split('-')[0]
            setID = conID.split('-')[1]
            jobID = conID.split('-')[2]

            # Define Paths #
            base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_'+mainID, 'set_'+setID, 'job_'+jobID)
            booz_path = os.path.join(base_path, 'boozmn_wout_HSX_main_opt0_16_16.nc')
            vmec_path = os.path.join(base_path, 'wout_HSX_main_opt0.nc')

            # Construct Boozer and VMEC Objects #
            booz = readBooz(booz_path, vmec_path)
            vmec = wr.readWout(base_path)

            u_dom = np.linspace(-np.pi, np.pi, 251)
            v_dom = np.array([0.25]) * np.pi

            booz.transForm_3D(booz.s_booz, u_dom, v_dom, ['R', 'Z'])
            vmec.transForm_3D(booz.s_vmec, u_dom, v_dom, ['R', 'Z'])

            R_booz = booz.invFourAmps['R']
            Z_booz = booz.invFourAmps['Z']

            R_vmec = vmec.invFourAmps['R']
            Z_vmec = vmec.invFourAmps['Z']

            plot = pd.plot_define(eqAsp=True, lineWidth=2)
            plt, fig, ax = plot.plt, plot.fig, plot.ax

            for i in range(booz.ns_booz):
                if i == booz.ns_booz-1:
                    plt.plot(R_booz[i, 0, :], Z_booz[i, 0, :], c='tab:red', label='Boozer')
                    plt.plot(R_vmec[i, 0, :], Z_vmec[i, 0, :], c='k', label='VMEC')

                else:
                    plt.plot(R_booz[i, 0, :], Z_booz[i, 0, :], c='tab:red')
                    plt.plot(R_vmec[i, 0, :], Z_vmec[i, 0, :], c='k')

            for j in range(booz.ns_booz, booz.ns_vmec):
                plt.plot(R_vmec[j, 0, :], Z_vmec[j, 0, :], c='k')

            plt.legend()
            plt.grid()
            plt.show()

    if False:
        base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_897', 'set_1', 'job_0')
        booz_path = os.path.join(base_path, 'boozmn_wout_HSX_main_opt0.nc')
        vmec_path = os.path.join(base_path, 'wout_HSX_main_opt0.nc')
        pram_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '897-1-0', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_1')

        ampKeys = ['R', 'Z', 'Phi', 'Bmod']

        # Construct Boozer and VMEC Objects #
        booz = readBooz(booz_path, vmec_path)
        booz.flux_tube_geometry(0.5, 0.0, pram_path, ampKeys)

        if True:
            hf_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '897-1-0', 'flux_tube_data_1.h5')
            with hf.File(hf_path, 'w') as hf_:
                hf_.create_dataset('GENE x domain', data=booz.gene_x)
                hf_.create_dataset('GENE y domain', data=booz.gene_y)
                hf_.create_dataset('cartesian coordinates', data=booz.cart_points)
                hf_.create_dataset('poloidal domain', data=booz.pol_dom)
                hf_.create_dataset('safety factor', data=booz.safty_factor)
                hf_.create_dataset('central s', data=booz.s0)
                hf_.create_dataset('central alpha', data=booz.alpha0)
                hf_.create_dataset('Bref', data=booz.Bref)
                hf_.create_dataset('Lref', data=booz.Lref)
                hf_.create_dataset('rho ref', data=booz.rho_ref)
