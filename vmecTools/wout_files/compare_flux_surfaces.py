import os
import numpy as np
import wout_read as wr
from matplotlib.ticker import MultipleLocator

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization')
sys.path.append(ModDir)

import plot_define as pd


# Import wout Files #
config_id = '60-1-84'
sq = 0.5
s1 = 0.49
# s2 = 0.5
# s3 = 0.65

main_id = 'main_coil_{}'.format(config_id.split('-')[0])
set_id = 'set_{}'.format(config_id.split('-')[1])
job_id = 'job_{}'.format(config_id.split('-')[2])

wout_path_q = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0')
wout_name_q = 'wout_QHS_mn1824_ns101.nc'

wout_path_1 = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
wout_name_1 = 'wout_60-1-84_mn1824_ns101.nc'

woutq = wr.readWout(os.path.join(wout_path_q, wout_name_q))
wout1 = wr.readWout(os.path.join(wout_path_1, wout_name_1))

# Generate Surfaces #
upts = 501

pol_dom = np.linspace(-np.pi, np.pi, upts)
tor_dom = np.array([0, 0.125, 0.25]) * np.pi

woutq.transForm_2D_sSec(sq, pol_dom, tor_dom, ['R', 'Z'])
Rq = woutq.invFourAmps['R']
Zq = woutq.invFourAmps['Z']

wout1.transForm_2D_sSec(s1, pol_dom, tor_dom, ['R', 'Z'])
R1 = wout1.invFourAmps['R']
Z1 = wout1.invFourAmps['Z']

# Plot Surfaces #
plot = pd.plot_define(fontSize=16, labelSize=20, lineWidth=2)
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True)
ax.set_aspect('equal')

pltq, = ax.plot(Rq[0, :], Zq[0, :], label=r'QHS: $\psi/\psi_\mathrm{{edge}}={0:0.2f}$'.format(sq))
plt1, = ax.plot(R1[0, :], Z1[0, :], label=r'HE-QHS: $\psi/\psi_\mathrm{{edge}}={0:0.2f}$'.format(s1))
for i in range(1,3):
    ax.plot(Rq[i, :], Zq[i, :], c=pltq.get_color())
    ax.plot(R1[i, :], Z1[i, :], c=plt1.get_color())

# Axis Labels #
ax.set_xlabel('R / m')
ax.set_ylabel('Z / m')

# axis ticks #
ax.tick_params(axis='both', which='major', direction='in', length=5)
ax.tick_params(axis='both', which='minor', direction='in', length=2.5)
ax.xaxis.set_ticks_position('default')
ax.yaxis.set_ticks_position('default')
ax.xaxis.set_minor_locator(MultipleLocator(0.025))
ax.yaxis.set_minor_locator(MultipleLocator(0.025))

# Axis Legends #
ax.legend(frameon=False)

# Save/Show
# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'APS_2024', 'figures', 'flux_surface_comparison.pdf')
plt.savefig(save_path, format='pdf')
