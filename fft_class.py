# -*- coding: utf-8 -*-
"""
Created on Sun May 10 01:24:29 2020

@author: micha
"""


import numpy as np

import scipy.fftpack as fft
from scipy.signal import find_peaks
from scipy.integrate import trapz


class fftTools():
    """ A class to perform and analyze the fourier transform of the specified
    data set

    ...

    Attributes
    ----------
    data : array
        data set to be transformed
    time : array
        time domain of data set

    Methods
    -------
    ifft()
        performs inverse fourier transform

    freq_filter(low, high)
        re-defines fourier data, amplitude and phase to exclude specified
        range

    peak_finder(height)
        retreirve fourier peak data for peaks with amplitudes ogreater than the
        specified height

    plot_amplitude(ax, withPeaks=False, norm=False, color='tab:red')
        plots amplitude of fourier transform

    plot_phase(ax, withPeaks=False)
        plots phase of fourier transform

    plot_ifft(ax)
        plots inverser fourier transform
    """

    def __init__(self, data, time, norm=False):
        self.npts = data.shape[0]
        self.hlf = int(.5 * self.npts)

        self.dt = time[1] - time[0]

        self.data = data
        self.time = time

        self.fft_data = fft.fft(data)
        self.fft_freq = fft.fftfreq(data.size, d=self.dt)

        self.fft_phase = np.arctan2(self.fft_data.real, self.fft_data.imag)
        self.fft_amp = np.abs(self.fft_data[1:])

        if norm:
            self.norm = True
            self.fft_scale = np.max(self.fft_amp)
            self.fft_amp = self.fft_amp / self.fft_scale
        else:
            self.norm = False


    def ifft(self, return_ifft=False, renormalize=False):
        self.ifft_data = fft.ifft(self.fft_data)
        if renormalize:
            data_len = self.data.shape[0]
            quart_idx = int(.25*data_len)

            argmax = quart_idx + np.argmax(np.abs(self.data[quart_idx:-quart_idx]))
            coeff = np.polyfit(np.arange(6), self.data[int(argmax-3):int(argmax+3)], 2)
            data_max = coeff[2] - .5 * (coeff[1]**2 - coeff[0])

            argmax =  quart_idx + np.argmax(np.abs(self.ifft_data.real[quart_idx:-quart_idx]))
            coeff = np.polyfit(np.arange(6), self.ifft_data.real[int(argmax-3):int(argmax+3)], 2)
            ifft_max = coeff[2] - .5 * (coeff[1]**2 - coeff[0])

            self.ifft_data = np.abs(data_max / ifft_max) * self.ifft_data

        if return_ifft:
            return self.ifft_data


    def freq_filter(self, fLow, fHigh):
        self.fft_data[np.where(self.fft_freq < -fHigh)] = 0+0j
        self.fft_data[np.where((self.fft_freq > -fLow) & (self.fft_freq < fLow))] = 0+0j
        self.fft_data[np.where(self.fft_freq > fHigh)] = 0+0j

        self.fft_amp = np.hypot(self.fft_data.real, self.fft_data.imag)


    def peak_selector(self, num_of_peaks, height=0):
        self.peak_finder(height)

        peak_data = np.stack((self.peaks[1]['peak_heights'], self.peaks[0]), axis=1)
        peak_data = np.array( sorted( peak_data, key=lambda x: x[0], reverse=True) )

        fft_data = np.zeros(self.fft_data.shape, dtype=np.complex64)
        if peak_data.shape[0] < num_of_peaks:
            indices = peak_data[0::, 1].astype('int32')
        else:
            indices = peak_data[0:num_of_peaks, 1].astype('int32')

        for idx in indices:
            fft_data[idx] = self.fft_data[idx]

        self.fft_data = fft_data


    def peak_finder(self, height, return_peak_data=False):
        self.peaks = find_peaks(self.fft_amp[0:self.hlf], height=height)

        if return_peak_data:
            return self.peaks


    def plot_amplitude(self, ax, withPeaks=False, norm=False, color='tab:red'):
        hlf = int(.5 * len(self.fft_freq))

        if norm:
            div = 1. / np.max(self.fft_amp[hlf::])
        else:
            div = 1.

        ax.plot(self.fft_freq[0:hlf], self.fft_amp[0:hlf]*div, c=color, zorder=0)

        if withPeaks:
            for idx in self.peakIdx:
                freq = self.fft_freq[idx]
                amp = self.fft_amp[idx]

                ax.plot([freq, freq], [0, amp], ls='--', label='%.2f steps' % (1./freq))


    def plot_phase(self, ax, color='tab:red', withPeaks=False):
        hlf = int(.5 * len(self.fft_freq))
        ax.plot(self.fft_freq[0:hlf], self.fft_phase[0:hlf] / np.pi, c=color)

        if withPeaks:
            for idx in self.peakIdx:
                freq = self.fft_freq[idx]
                phase = self.fft_phase[idx]

                ax.plot([freq, freq], [-1, 1], ls='--', label=r'%.3f $\pi$' % phase)


    def plot_ifft(self, ax):
        ax.plot(self.time, self.ifft_data.real, c='tab:red', ls='--')

