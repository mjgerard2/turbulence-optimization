import os, sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

ModDir = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'python_scripts', 'BAD_scripts')
sys.path.append(ModDir)
import BAD

kx_idx = 1
ky_tag = 'ky0p1'
config_id = '0-1-0'

pol_beg = -4*np.pi
pol_end = 4*np.pi

# import data #
bad_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'BAD_mn1824_res1024_npol16.h5')
bad = BAD.BAD_class(bad_path, config_ids=[config_id])

bad.mod_list([config_id])
bad.import_gist()
bad.import_normalizations(ky_tag)
bad.import_tem_data(ky_tag)

# read normalizations #
aLn = bad.res_norms[config_id]['grad_n']
aLT = bad.res_norms[config_id]['grad_T']
kx_base = bad.res_norms[config_id]['kx base']
nx0 = bad.res_norms[config_id]['nx0']
nz0 = bad.res_norms[config_id]['nz0']
q0 = bad.res_norms[config_id]['q0']
cref = bad.res_norms[config_id]['cref']
rho_ref = bad.res_norms[config_id]['rho_ref']
Lref = bad.res_norms[config_id]['Lref']
zeta = bad.res_norms[config_id]['zeta']
norm = (cref*rho_ref)/(zeta*Lref*Lref)

# get frequency and wavenumber data #
kx = kx_base * kx_idx
ky = float(ky_tag.split('y')[1].replace('p', '.'))
omg = bad.tem_df.loc[config_id, 'omega_%s' % ky_tag]

# read define data arrays #
B_field = bad.B_field[config_id]
pol = bad.bad_dict[config_id][0]
wgt = bad.bad_dict[config_id][1]
oma = ky*bad.bad_dict[config_id][2]
omp = q0*q0*kx*bad.bad_dict[config_id][3]
idx = np.where((pol >= pol_beg) & (pol <= pol_end))[0]

omp_oma = omp/oma
omg_ky_oma = norm*(omg/(ky*oma))

ibeg, iend = int(kx_idx+.5*(nx0-1))*nz0, int(1+kx_idx+.5*(nx0-1))*nz0
pol_phi = bad.phi_dict[config_id][ky_tag][0]
pol_shft = pol_phi[ibeg:iend]-pol_phi[ibeg]+pol_phi[int(.5*(nx0-1))*nz0-1] 
phi = bad.phi_dict[config_id][ky_tag][1][ibeg:iend]
phi_idx = np.where((pol_shft >= pol_beg) & (pol_shft <= pol_end))[0]

# calculate over ballooning space #
kx = kx_base*kx_idx
freq_inv = 1./(1 + q0*q0*(kx/ky)*omp_oma)
z_prime = norm*(omg/(ky*oma))*freq_inv
R = 1 + (1./norm)*(ky/omg)*(aLn+aLT*(z_prime-1.5))
heavy_side = np.heaviside(z_prime, 0)
z_prime = z_prime * heavy_side
R = omg*omg*R*heavy_side

# plotting parameters #
bad.plotting()
fig, axs = plt.subplots(5, 1, sharex=False, tight_layout=False, figsize=(12, 10))

# plot data #
axs[0].plot(pol[idx]/np.pi, B_field[idx], c='k')
axs[1].plot(pol[idx]/np.pi, ky*oma[idx], c='tab:blue', label=r'$\hat{\overline{\omega}}_\mathrm{y}$')
axs[1].plot(pol[idx]/np.pi, kx*omp[idx], c='tab:red', label=r'$\hat{\overline{\omega}}_\mathrm{x}$')
axs[2].plot(pol[idx]/np.pi, wgt[idx], c='k')
axs[3].plot(pol[idx]/np.pi, -R[idx], c='k')
axs[4].plot(pol_shft[phi_idx]/np.pi, phi[phi_idx], c='k')

# axis limits #
drift_max = np.max(np.abs(axs[1].get_ylim()))
axs[1].set_ylim(-drift_max, drift_max)
axs[2].set_ylim(0, axs[2].get_ylim()[1])
axs[3].set_yscale('log')
axs[3].set_ylim(1e-3, axs[3].get_ylim()[1])
axs[4].set_ylim(0, axs[4].get_ylim()[1])

# axis labels #
axs[4].set_xlabel(r'$\theta_\mathrm{b} \ / \ \pi$')
axs[0].set_ylabel(r'$B \ / \ \mathrm{T}$')
axs[1].legend(ncol=2, loc='upper left')
axs[2].set_ylabel(r'$p(\theta_\mathrm{b})$')
axs[3].set_ylabel(r'$-R(\theta_\mathrm{b})$')
axs[4].set_ylabel(r'$|\overline{\phi}|^2$')

# axis title #
axs[0].set_title(r'$(k_\mathrm{{x}}\rho_\mathrm{{s}},\, k_\mathrm{{y}}\rho_\mathrm{{s}}) = ({0:0.2f},\, {1:0.2f})$'.format(kx, ky))

# axis legend #
axs[1].legend(ncol=2, frameon=False)

# axis ticks #
for i, ax in enumerate(axs.flat):
    ax.grid()
    ax.set_xlim(pol_beg/np.pi, pol_end/np.pi)
    ax.tick_params(axis='both', which='both', direction='in')
    ax.xaxis.set_ticks_position('default')
    ax.yaxis.set_ticks_position('default')
    # ax.xaxis.set_minor_locator(MultipleLocator(0.1))
    if i != len(axs.flat)-1:
        ax.set_xticklabels([])

# save/show #
fig.subplots_adjust(hspace=0.1)
plt.show()
