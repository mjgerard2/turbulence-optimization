import os, sys
import warnings
import numpy as np
import h5py as hf

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import gistTools.gist_reader as gr
import geneAnalysis.read_eigenvector_IV as re

# import configuration list #
config_check = ['0-1-0']
config_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'linear_data', 'config_list.txt')
with open(config_path, 'r') as f:
    lines = f.readlines()
    for line in lines:
        config_check.append(line.strip())

# check for existing BAD data #
config_list = []
config_skip = ['308-1-664', '366-1-612', '77-1-621', '1302-1-1', '1017-1-704', '1088-1-0', '1136-1-561', '1277-1-27', '616-1-243', '77-1-621', '1038-1-27']
hf_name = "BAD_mn1824_res256_npol16"
hf_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', hf_name+'.h5')
if os.path.isfile(hf_path):
    with hf.File(hf_path, 'r') as hf_:
        for config_id in config_check:
            if any([icon == config_id for icon in config_skip]):
                continue
            else:
                if not config_id in hf_:
                    config_list.append(config_id)
else:
    config_list = config_check
nfigs = len(config_list)

# parse naming tags #
name_split = hf_name.split('_')
mn_tag = name_split[1]
res_tag = name_split[2]
npol_tag = name_split[3]
end_tag = '%s_%s.dat' % (res_tag, npol_tag)

# loop over configurations #
for (idx, config_id) in enumerate(config_list):
    # identify gist file path #
    print('\n{}: ({}|{})'.format(config_id, idx+1, nfigs))
    main_id = 'main_coil_{}'.format(config_id.split('-')[0])
    set_id = 'set_{}'.format(config_id.split('-')[1])
    job_id = 'job_{}'.format(config_id.split('-')[2])
    gist_dir = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
    gist_files = [f.name for f in os.scandir(gist_dir) if f.name.endswith(end_tag)]
    gist_names = [name for name in gist_files if name.split('_')[2] == 'mn1824']
    if not len(gist_names) > 0:
        continue

    s_tag = gist_names[0].split('_')[4]
    if config_id == "0-1-0":
        gist_file = os.path.join(gist_dir, 'gist_QHS_%s_alf0_%s_%s_%s.dat' % (mn_tag, s_tag, res_tag, npol_tag))
    else:
        gist_file = os.path.join(gist_dir, 'gist_%s_%s_alf0_%s_%s_%s.dat' % (config_id, mn_tag, s_tag, res_tag, npol_tag))

    # import gist data and calculate drifts and weighting function #
    gist = gr.read_gist(gist_file)
    print('    calculate drifts')
    gist.calculate_omega_de()

    print('    calculate weights')
    gist.calculate_p_weight(exp_order=4, base_scale=0.15)

    # get eigenfunction #
    eigen_dict = {}
    ky_dirs = ['ky0p1', 'ky0p4', 'ky0p7', 'ky1p0']
    gene_dir = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'linear_data', config_id)
    for ky_dir in ky_dirs:
        print('    calculate phi for ky={}'.format(ky_dir.split('y')[1].replace('p','.')))
        ball_name = [f.path for f in os.scandir(os.path.join(gene_dir, ky_dir)) if f.name.split('_')[0] == 'balle']
        if len(ball_name) == 0:
            continue
        elif len(ball_name) == 1:
            runID = ball_name[0].split('.')[0].split('_')[-1]
            param_path = os.path.join(gene_dir, ky_dir, 'parameters_%s' % runID)
            eigen_dict[ky_dir] = re.read_eigendata(ball_name[0], param_path)
            eigen_dict[ky_dir].average_sqrd_bar(gist)
        else:
            print(ball_name)
            raise ValueError('Too many ballooning diagnostic files!')

    if len(ball_name) == 0:
        continue
    
    # save BAD data for configuration #
    # BAD_data = np.stack((pol_dom, p_weight, omega_alpha, omega_psi, jacob), axis=0)
    with hf.File(hf_path, 'a') as hf_:
        hf_.create_dataset(config_id+'/BAD data/poloidal angle', data=gist.pol_dom)
        hf_.create_dataset(config_id+'/BAD data/density weighting function', data=gist.data['p-weight'])
        hf_.create_dataset(config_id+'/BAD data/omega alpha', data=gist.data['omega_alpha'])
        hf_.create_dataset(config_id+'/BAD data/omega psi', data=gist.data['omega_psi'])
        for key, eigen in eigen_dict.items():
            hf_.create_dataset(config_id+'/phi data/'+key+'/bounce averaged phi2', data=eigen.avg_phi_sqrd)
            hf_.create_dataset(config_id+'/phi data/'+key+'/poloidal angle', data=eigen.pol_dom)
