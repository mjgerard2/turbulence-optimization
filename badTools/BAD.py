import os
import h5py as hf
import numpy as np
import pandas as pds

import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d
from scipy import constants as const
from sklearn.linear_model import LinearRegression

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import gistTools.gist_reader as gr
import geneAnalysis.read_parameters as rp


class BAD_class():

    def __init__(self, bad_path, config_ids='all'):
        # parse data file name #
        self.bad_path = bad_path
        self.bad_name = bad_path.split('/')[-1]

        bad_split = self.bad_name.split('.')[0].split('_')
        self.mn_tag = bad_split[1]
        self.res_tag = bad_split[2]
        self.npol_tag = bad_split[3]

        # import bad data into dicitonary #
        self.bad_dict = {}
        self.phi_dict = {}
        with hf.File(bad_path, 'r') as hf_:
            if config_ids == 'all':
                self.config_list = []
                for config_id in hf_:
                    self.config_list.append(config_id)
                    pol = hf_[config_id]['BAD data']['poloidal angle'][()]
                    wgt = hf_[config_id]['BAD data']['density weighting function'][()]
                    oma = hf_[config_id]['BAD data']['omega alpha'][()]
                    omp = hf_[config_id]['BAD data']['omega psi'][()]
                    self.bad_dict[config_id] = np.stack((pol, wgt, oma, omp), axis=0)
                    self.phi_dict[config_id] = {}
                    for ky_tag in hf_[config_id]['phi data']:
                        pol = hf_[config_id]['phi data'][ky_tag]['poloidal angle'][()]
                        phi = hf_[config_id]['phi data'][ky_tag]['bounce averaged phi2'][()]
                        self.phi_dict[config_id][ky_tag] = np.stack((pol, phi), axis=0)
            else:
                self.config_list = config_ids
                for config_id in config_ids:
                    pol = hf_[config_id]['BAD data']['poloidal angle'][()]
                    wgt = hf_[config_id]['BAD data']['density weighting function'][()]
                    oma = hf_[config_id]['BAD data']['omega alpha'][()]
                    omp = hf_[config_id]['BAD data']['omega psi'][()]
                    self.bad_dict[config_id] = np.stack((pol, wgt, oma, omp), axis=0)
                    self.phi_dict[config_id] = {}
                    for ky_tag in hf_[config_id]['phi data']:
                        pol = hf_[config_id]['phi data'][ky_tag]['poloidal angle'][()]
                        phi = hf_[config_id]['phi data'][ky_tag]['bounce averaged phi2'][()]
                        self.phi_dict[config_id][ky_tag] = np.stack((pol, phi), axis=0)
    
    def mod_list(self, new_list):
        self.config_list = new_list

    def import_gist(self):
        self.B_field = {}
        for config_id in self.config_list:
            # import corresponding gist data #
            main_id = 'main_coil_{}'.format(config_id.split('-')[0])
            set_id = 'set_{}'.format(config_id.split('-')[1])
            job_id = 'job_{}'.format(config_id.split('-')[2])
            gist_dir = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
            gist_files = [f.name for f in os.scandir(gist_dir) if f.name.split('_')[0] == 'gist']
            gist_files = [f for f in gist_files if f.split('_')[2] == self.mn_tag]
            s_tag = gist_files[0].split('_')[4]
            if config_id == '0-1-0':
                gist_name = 'gist_QHS_%s_alf0_%s_%s_%s.dat' % (self.mn_tag, s_tag, self.res_tag, self.npol_tag)
            else:
                gist_name = 'gist_%s_%s_alf0_%s_%s_%s.dat' % (config_id, self.mn_tag, s_tag, self.res_tag, self.npol_tag)
            gist_path = os.path.join(gist_dir, gist_name)
            gist = gr.read_gist(gist_path, nfp=4)
            gist.balloon_data(self.bad_dict[config_id][0])
            self.B_field[config_id] = gist.model['modB'](self.bad_dict[config_id][0])*gist.Bref

    def import_normalizations(self, ky_tag):
        self.ky_tag = ky_tag
        self.ky_val = float(ky_tag.split('y')[1].replace('p', '.'))
        self.res_norms = {}
        self.phys_norms = {}
        for config_id in self.config_list:
            param_dir = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'linear_data', config_id, ky_tag)
            if os.path.isdir(param_dir):
                param_files = sorted([f.name for f in os.scandir(param_dir) if f.name.split('_')[0] == 'parameters'])
            else:
                continue
            if len(param_files) > 0:
                # import GENE normalizations #
                param_path = os.path.join(param_dir, param_files[-1])
                param = rp.read_file(param_path)
                nx0 = param.data_dict['nx0']
                nz0 = param.data_dict['nz0']
                q0 = param.data_dict['q0'] # unitless
                mref = const.m_p * param.data_dict['mref'] # kilograms
                Tref = 1e3 * const.e * param.data_dict['Tref'] # joules
                Lref = param.data_dict['Lref'] # meters
                lx = param.data_dict['lx'] # ion gyroradius
                Bref = param.data_dict['Bref'] # tesla
                aLn = param.data_dict['omne'] # unitless
                aLT = param.data_dict['omte'] # unitless

                Ti = Tref * param.data_dict['tempi']
                Te = Tref * param.data_dict['tempe']
                cref = np.sqrt(Tref/mref)
                Oref = const.e*Bref/mref
                rho_ref = cref/Oref
                kx_base = (2*np.pi)/lx

                # import flux normalizations #
                main_id = 'main_coil_{}'.format(config_id.split('-')[0])
                set_id = 'set_{}'.format(config_id.split('-')[1])
                job_id = 'job_{}'.format(config_id.split('-')[2])
                gist_dir = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
                gist_files = [f.name for f in os.scandir(gist_dir) if f.name.split('_')[0] == 'gist']
                gist_files = [f for f in gist_files if f.split('_')[2] == self.mn_tag]
                s_tag = gist_files[0].split('_')[4]
                if config_id == '0-1-0':
                    gist_name = 'gist_QHS_%s_alf0_%s_%s_%s.dat' % (self.mn_tag, s_tag, self.res_tag, self.npol_tag)
                else:
                    gist_name = 'gist_%s_%s_alf0_%s_%s_%s.dat' % (config_id, self.mn_tag, s_tag, self.res_tag, self.npol_tag)
                gist_path = os.path.join(gist_dir, gist_name)
                with open(gist_path) as f:
                    lines = f.readlines()
                    for line in lines:
                        line_split = line.split()
                        if line_split[0] == '!s0,':
                            psiN = float(line_split[3])
                        elif line_split[0] == '!major,':
                            a = float(line_split[5])
                        elif line_split[0] == '!Bref':
                            Bhat = float(line_split[2])
                            break

                psi_edge = 0.5*a*a*Bhat
                zeta = Te/(2*const.e*q0*psi_edge)

                self.res_norms[config_id] = {'grad_n': aLn,
                                             'grad_T': aLT,
                                             'nx0': int(nx0),
                                             'nz0': int(nz0),
                                             'kx base': kx_base,
                                             'q0': q0,
                                             'cref': cref,
                                             'rho_ref': rho_ref,
                                             'Lref': Lref,
                                             'zeta': zeta}

                self.phys_norms[config_id] = {'omega': -cref/Lref,
                                              'omega_alpha': -zeta*(cref/Lref),
                                              'grad_n': aLn*zeta*(cref/Lref),
                                              'grad_T': aLT*zeta*(cref/Lref)}

    def import_tem_data(self, ky_tag):
        gene_colms = ['omega', 'gamma', 'phi_x_n', 'phi_x_Tpe']
        self.gene_colms = ['%s_%s' % (colm, ky_tag) for colm in gene_colms]

        df_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'HSX_coil_current_data_mn1824.h5')
        df = pds.read_hdf(df_path, key='DataFrame')
        df = df.loc[self.config_list, self.gene_colms].dropna()
        self.tem_df = df.loc[~((df[self.gene_colms[0]] == 0.0) & (df[self.gene_colms[1]] == 0.0))]

    def calculate_resonance(self, ky_tag, norms='gene'):
        self.import_normalizations(ky_tag)
        self.import_tem_data(ky_tag)

        self.resonance = {}
        # self.bad_mean = {}
        for config_id in self.config_list:
            if not (self.tem_df.index == config_id).any():
                continue
        
            # read normalizations #
            aLn = self.res_norms[config_id]['grad_n']
            aLT = self.res_norms[config_id]['grad_T']
            nx0 = self.res_norms[config_id]['nx0']
            nz0 = self.res_norms[config_id]['nz0']
            kx_base = self.res_norms[config_id]['kx base']
            q0 = self.res_norms[config_id]['q0']
            cref = self.res_norms[config_id]['cref']
            rho_ref = self.res_norms[config_id]['rho_ref']
            Lref = self.res_norms[config_id]['Lref']
            zeta = self.res_norms[config_id]['zeta']
            norm = (cref*rho_ref)/(zeta*Lref*Lref)

            # get frequency and wavenumber data #
            ky = float(ky_tag.split('y')[1].replace('p', '.'))
            omg = self.tem_df.loc[config_id, 'omega_%s' % ky_tag]
            norm_ky_omg = (1./norm)*(ky/omg)

            # redefine poloidal data #
            pol = self.bad_dict[config_id][0]
            wgt = self.bad_dict[config_id][1]
            oma = self.bad_dict[config_id][2]
            omp = self.bad_dict[config_id][3]
            omp_oma = omp/oma
            omg_ky_oma = norm*(omg/(ky*oma))

            phi_pol = self.phi_dict[config_id][ky_tag][0]
            dpol = phi_pol[1]-phi_pol[0]
            phi_pol_base = phi_pol[int(.5*(nx0-1))*nz0-1]
            phi = self.phi_dict[config_id][ky_tag][1]
            
            # calculate over ballooning space #
            numerator = 0
            denominator = 0
            for i in range(nx0):
                j = int(i-.5*(nx0-1))
                if np.abs(j) < np.inf:
                    if i == 0:
                        iend = (i+1)*nz0+1
                        pol_kx = phi_pol[0:iend]
                        phi_kx = phi[0:iend]
                        pol_kx = np.insert(pol_kx, 0, pol_kx[0]-dpol)
                        phi_kx = np.insert(phi_kx, 0, 0)
                    elif i == nx0-1:
                        ibeg = i*nz0-1
                        pol_kx = phi_pol[ibeg:]
                        phi_kx = phi[ibeg:]
                        pol_kx = np.append(pol_kx, pol_kx[-1]+dpol)
                        phi_kx = np.append(phi_kx, 0)
                    else:
                        ibeg, iend = i*nz0-1, (i+1)*nz0+1
                        pol_kx = phi_pol[ibeg:iend]
                        phi_kx = phi[ibeg:iend]
                    pol_kx_shft = pol_kx-pol_kx[0]+phi_pol_base
                    phi_kx_model = interp1d(pol_kx_shft, phi_kx)
                    phi_kx = phi_kx_model(pol)

                    kx = kx_base*j
                    freq_inv = 1./(1 + q0*q0*(kx/ky)*omp_oma)
                    z_prime = omg_ky_oma*freq_inv
                    R = 1 + norm_ky_omg*(aLn+aLT*(z_prime-1.5))
                    heavy_side = np.heaviside(z_prime, 0)
                    z_prime = z_prime * heavy_side
                    R = omg*omg*R*heavy_side

                    base_integ = np.sqrt(z_prime)*np.exp(-z_prime)*phi_kx*wgt
                    numerator += np.trapz(R*base_integ, pol)
                    denominator += np.trapz(base_integ, pol)
            
            # integrate resonance and particle drifts #
            res = numerator/denominator
            oma_avg = ky*(1.5/norm)*np.trapz(oma*wgt, pol)/np.trapz(wgt, pol)
            omp_avg = kx_base*q0*q0*(1.5/norm)*np.trapz(omp*wgt, pol)/np.trapz(wgt, pol)
            self.resonance[config_id] = [res, oma_avg, omp_avg]
            # self.bad_mean[config_id] = (1.5/norm)*np.trapz(oma*wgt, pol)/np.trapz(wgt, pol)
    
    def calculate_well_resonance(self, ky_tag):
        self.import_normalizations(ky_tag)
        self.import_tem_data(ky_tag)

        self.resonance_partition = {}
        for i, config_id in enumerate(self.config_list):
            # get resonance data #
            # print(config_id+': ({}|{})'.format(i+1, len(self.config_list)))
            if not (self.tem_df.index == config_id).any():
                # print(config_id)
                continue

            # read normalizations #
            aLn = self.res_norms[config_id]['grad_n']
            aLT = self.res_norms[config_id]['grad_T']
            kx_base = self.res_norms[config_id]['kx base']
            nx0 = self.res_norms[config_id]['nx0']
            nz0 = self.res_norms[config_id]['nz0']
            q0 = self.res_norms[config_id]['q0']
            cref = self.res_norms[config_id]['cref']
            rho_ref = self.res_norms[config_id]['rho_ref']
            Lref = self.res_norms[config_id]['Lref']
            zeta = self.res_norms[config_id]['zeta']
            norm = (cref*rho_ref)/(zeta*Lref*Lref)

            # get frequency and wavenumber data #
            ky = float(ky_tag.split('y')[1].replace('p', '.'))
            omg = self.tem_df.loc[config_id, 'omega_%s' % ky_tag]
            norm_ky_omg = (1./norm)*(ky/omg)

            # redefine poloidal data #
            pol = self.bad_dict[config_id][0]
            wgt = self.bad_dict[config_id][1]
            oma = self.bad_dict[config_id][2]
            omp = self.bad_dict[config_id][3]

            phi_pol = self.phi_dict[config_id][ky_tag][0]
            dpol = phi_pol[1]-phi_pol[0]
            phi_pol_base = phi_pol[int(.5*(nx0-1))*nz0-1]
            phi = self.phi_dict[config_id][ky_tag][1]
            
            # import gist data #
            main_id = 'main_coil_{}'.format(config_id.split('-')[0])
            set_id = 'set_{}'.format(config_id.split('-')[1])
            job_id = 'job_{}'.format(config_id.split('-')[2])
            gist_dir = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
            gist_files = [f.name for f in os.scandir(gist_dir) if f.name.split('_')[0] == 'gist']
            gist_files = [f for f in gist_files if f.split('_')[2] == self.mn_tag]
            s_tag = gist_files[0].split('_')[4]
            if config_id == '0-1-0':
                gist_name = 'gist_QHS_%s_alf0_%s_%s_%s.dat' % (self.mn_tag, s_tag, self.res_tag, self.npol_tag)
            else:
                gist_name = 'gist_%s_%s_alf0_%s_%s_%s.dat' % (config_id, self.mn_tag, s_tag, self.res_tag, self.npol_tag)
            gist_path = os.path.join(gist_dir, gist_name)
            gist = gr.read_gist(gist_path, nfp=4)
            gist.partition_wells()

            # calculate resonance in partitioned wells #
            self.resonance_partition[config_id] = {}
            for well_num in gist.well_indices[1:-2]:
                well_key = 'well {0:0.0f}'.format(well_num)
                pol_lim = gist.partition[well_key]['pol']
                idx_well = np.where((pol >= pol_lim[0]) & (pol <= pol_lim[2]))[0]
                pol_well = pol[idx_well]
                wgt_well = wgt[idx_well]
                oma_well = oma[idx_well]
                omp_well = omp[idx_well]
                omp_oma = omp_well/oma_well
                omg_ky_oma = norm*(omg/(ky*oma_well))

                # calculate over ballooning space #
                numerator = 0
                denominator = 0
                for i in range(nx0):
                    j = int(i-.5*(nx0-1))
                    if np.abs(j) < np.inf:
                        if i == 0:
                            iend = (i+1)*nz0+1
                            pol_kx = phi_pol[0:iend]
                            phi_kx = phi[0:iend]
                            pol_kx = np.insert(pol_kx, 0, pol_kx[0]-dpol)
                            phi_kx = np.insert(phi_kx, 0, 0)
                        elif i == nx0-1:
                            ibeg = i*nz0-1
                            pol_kx = phi_pol[ibeg:]
                            phi_kx = phi[ibeg:]
                            pol_kx = np.append(pol_kx, pol_kx[-1]+dpol)
                            phi_kx = np.append(phi_kx, 0)
                        else:
                            ibeg, iend = i*nz0-1, (i+1)*nz0+1
                            pol_kx = phi_pol[ibeg:iend]
                            phi_kx = phi[ibeg:iend]
                        pol_kx_shft = pol_kx-pol_kx[0]+phi_pol_base
                        phi_kx_model = interp1d(pol_kx_shft, phi_kx)
                        phi_kx = phi_kx_model(pol_well)

                        kx = kx_base*j
                        freq_inv = 1./(1 + q0*q0*(kx/ky)*omp_oma)
                        z_prime = omg_ky_oma*freq_inv
                        R = 1 + norm_ky_omg*(aLn+aLT*(z_prime-1.5))
                        heavy_side = np.heaviside(z_prime, 0)
                        z_prime = z_prime * heavy_side
                        R = omg*omg*R*heavy_side

                        base_integ = np.sqrt(z_prime)*np.exp(-z_prime)*phi_kx*wgt_well
                        numerator += np.trapz(R*base_integ, pol_well)
                        denominator += np.trapz(base_integ, pol_well)
                
                # broadcast resonance data #
                res_well = numerator / denominator
                oma_avg = ky*(1.5/norm)*np.trapz(oma_well*wgt_well, pol_well)/np.trapz(wgt_well, pol_well)
                omp_avg = kx_base*q0*q0*(1.5/norm)*np.trapz(omp_well*wgt_well, pol_well)/np.trapz(wgt_well, pol_well)
                self.resonance_partition[config_id][well_key] = [res_well, oma_avg, omp_avg]

    def calculate_resonance_metric(self, ky_tag, M=10):
        self.import_normalizations(ky_tag)
        self.import_tem_data(ky_tag)

        self.resonance = {}
        for config_id in self.config_list:
            if not (self.tem_df.index == config_id).any():
                continue

            M_inv = 1./M
            omn = -self.gene_norms[config_id]['grad_n']
            omt = -self.gene_norms[config_id]['grad_T']

            idx_dom = np.where(self.bad_dict[config_id][0] <= 0)[0]
            pol = self.bad_dict[config_id][0, idx_dom]
            wgt = self.bad_dict[config_id][1, idx_dom]
            oma = self.bad_dict[config_id][2, idx_dom] * self.gene_norms[config_id]['omega_alpha']
            # omg = 0.5*(np.trapz(oma*wgt, pol) / np.trapz(wgt, pol))
            # omg = 0.0772*(0.5*(np.trapz(oma*wgt, pol) / np.trapz(wgt, pol)))-0.1039

            # oma_inv = 1./oma
            # omg_oma = omg*oma_inv
            # pos_idx = np.where(oma < .5*omg*M_inv)[0]
            # omg_oma = omg_oma[pos_idx]
            # oma_inv = oma_inv[pos_idx]
            beta=10
            h_dom = np.linspace(1-np.sqrt((beta-1)/beta), 1+np.sqrt((beta-1)/beta), 100)
            R_set = np.zeros((h_dom.shape[0], pol.shape[0]))
            for i, h in enumerate(h_dom):
                xi = (1.5*omt*h-omn*h)/(h*omt+2*oma)
                pos_idx = np.where(xi >= 0)[0]
                xi = xi[pos_idx]
                R_set[i,pos_idx] = -h*((2+2*(xi-1.5))**2)*np.sqrt(xi)*np.exp(-xi)

            # R = np.zeros(pol.shape)
            # R[pos_idx] = omg*omg_oma*np.sqrt(omg_oma)*np.sqrt(1-.5*M_inv)*(1 + (((omn-1.5*omt)/omg) + (omt*oma_inv)*(1-.5*M_inv)))*np.exp(-omg_oma*(1-.25*M_inv*omg_oma))
            # R[pos_idx] = -((2+2*(xi-1.5))**2)*np.sqrt(xi)*np.exp(-xi)
            # self.resonance[config_id] = np.trapz(R*wgt, pol) / np.trapz(wgt, pol)
            self.resonance[config_id] = np.trapz(np.trapz(R_set, h_dom, axis=0)*wgt, pol)/np.trapz(wgt,pol)

    def calculate_resonance_metric_numerical(self, ky_tag, M, zres, zmult):
        self.import_normalizations(ky_tag)
        self.import_tem_data(ky_tag)

        zres_inv = 1./zres
        M_inv = 1./M

        self.resonance = {}
        for cdx, config_id in enumerate(self.config_list):
            print('{}: ({}|{})'.format(config_id, cdx+1, len(self.config_list)))
            if not (self.tem_df.index == config_id).any():
                continue

            omn = self.gene_norms[config_id]['grad_n']
            omt = self.gene_norms[config_id]['grad_T']

            idx_dom = np.where(self.bad_dict[config_id][0] <= 0)[0]
            pol = self.bad_dict[config_id][0, idx_dom]
            wgt = self.bad_dict[config_id][1, idx_dom]
            oma = self.bad_dict[config_id][2, idx_dom] * self.gene_norms[config_id]['omega_alpha']
            omg = 0.5*(np.trapz(oma*wgt, pol) / np.trapz(wgt, pol))
            # omg = self.tem_df.loc[config_id, 'omega_%s' % ky_tag]

            oma_inv = 1./oma
            omg_oma = omg*oma_inv
            oma_omg = 1./omg_oma
            pos_idx = np.where(oma < .5*omg*M_inv)[0]
            omg_oma = omg_oma[pos_idx]
            oma_omg = oma_omg[pos_idx]
            oma_inv = oma_inv[pos_idx]

            R = np.zeros(pol.shape)
            z1_max = zmult*(.5*omg_oma*((1-.5*M_inv*omg_oma) + np.sqrt(M_inv + (1 - .5*M_inv*omg_oma)**2)))
            z3_max = zmult*(.5*omg_oma*((1-.5*M_inv*omg_oma) + np.sqrt(3*M_inv + (1 - .5*M_inv*omg_oma)**2)))
            for i, idx in enumerate(pos_idx):
                z1_dom = np.linspace(0, z1_max[i], int(z1_max[i]*zres_inv)+1)
                z3_dom = np.linspace(0, z3_max[i], int(z3_max[i]*zres_inv)+1)

                I1_integ = np.sqrt(z1_dom)*np.exp(M*(-1.+2*(oma_omg[i]-.5*M_inv)*z1_dom-(oma_omg[i]*z1_dom)**2))
                I1 = np.trapz(I1_integ, z1_dom)

                I3_integ = z3_dom*np.sqrt(z3_dom)*np.exp(M*(-1.+2*(oma_omg[i]-.5*M_inv)*z3_dom-(oma_omg[i]*z3_dom)**2))
                I3 = np.trapz(I3_integ, z3_dom)
                
                R[idx] = (omg)*((1.+((omn-1.5*omt)/omg))*I1 + (omt/omg)*I3)
            self.resonance[config_id] = np.trapz(R*wgt, pol) / np.trapz(wgt, pol)

    def calculate_resonance_spectrum(self, ky_tag, npts=1001):
        self.import_normalizations(ky_tag)

        self.resonance_spectrum = {}
        self.resonance_omega = np.linspace(-20, 0, npts, endpoint=False)
        for config_id in self.config_list:
            omn = self.gene_norms[config_id]['grad_n']
            omt = self.gene_norms[config_id]['grad_T']

            idx_dom = np.where(self.bad_dict[config_id][0] <= 0)[0]
            pol = self.bad_dict[config_id][0, idx_dom]
            wgt = self.bad_dict[config_id][1, idx_dom]
            oma = self.bad_dict[config_id][2, idx_dom] * self.gene_norms[config_id]['omega_alpha']
            oma_inv = 1./oma

            res_spec = np.empty(npts)
            for i, omg in enumerate(self.resonance_omega):
                omg_oma = omg*oma_inv
                pos_idx = np.where(omg_oma > 0)[0]
                omg_oma = omg_oma[pos_idx]
                oma_inv = oma_inv[pos_idx]
                R = np.zeros(pol.shape)
                R[pos_idx] = -omg*omg*np.sqrt(omg_oma)*(1 + (((1.5*omt-omn)/omg) - (omt*oma_inv)))*np.exp(-omg_oma)
                res_spec[i] = np.trapz(R*wgt, pol) / np.trapz(wgt, pol)
            self.resonance_spectrum[config_id] = res_spec

    def plotting(self, fontsize=14, labelsize=18, linewidth=1):
        plt.close('all')

        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': fontsize}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = labelsize
        mpl.rcParams['lines.linewidth'] = linewidth

    def plot_gamma_vs_resonance(self, ky_tag, save_path=None):
        # import metric data #
        met_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'HSX_coil_current_data_mn1824.h5')
        df = pds.read_hdf(met_path, key='DataFrame')

        color_data = np.empty(self.tem_df.shape[0])
        gene_data = np.empty(self.tem_df.shape[0])
        res_data = np.empty(self.tem_df.shape[0])
        for i, (config_id, data) in enumerate(self.tem_df.iterrows()):
            gene_data[i] = data[1]
            res_data[i] = self.resonance[config_id][0]
            color_data[i] = df.loc[config_id, 'phi_x_n_'+ky_tag]
            # color_data[i] = df.loc[config_id, 'zeta']
        
        # idx = np.where(color_data <= 0)[0]

        # plot data #
        self.plotting()
        fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))
        smap = ax.scatter(res_data, gene_data, c=color_data, edgecolor='k', s=25, marker='o', vmin=-1, vmax=1, cmap='Spectral')
        # smap = ax.scatter(res_data, gene_data, c=color_data, edgecolor='k', s=25, marker='o', cmap='Spectral')

        cbar = fig.colorbar(smap, ax=ax)
        cbar.ax.set_ylabel(r'$M_{\phi\times n}$')

        # axis limits #
        ax.set_ylim(0, ax.get_ylim()[1])
        # ax.set_xlim(ax.get_xlim()[0], 0.1)

        # axis labels #
        ax.set_xlabel(r'$\langle \omega(\omega-\omega_*^T) \rangle \ / \ (c_s/a)^2$')
        ax.set_ylabel(r'$\gamma \ / \ (c_s/a)$')
        ky = self.ky_tag.split('y')[1].replace('p', '.')
        ax.set_title(r'$k_y/\rho_s = %s$' % ky)

        # grid #
        ax.grid()

        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

    def plot_gamma_vs_resonance_across_wells(self, well_IDs=np.flip(np.arange(-5, 1)), save_path=None):
        # organize data into arrays #
        well_dict = {}
        for well_ID in well_IDs:
            well_key = 'well {}'.format(well_ID)
            well_arr = np.empty((self.tem_df.shape[0], 3))
            for i, (config_id, data) in enumerate(self.tem_df.iterrows()):
                res, oma = self.resonance_parition[config_id][well_key]
                tem = data[1]
                well_arr[i] = [oma, tem, res]
            
            is_nan = np.isnan(well_arr[:,0])
            not_nan = ~is_nan
            well_arr = well_arr[not_nan]
            
            is_nan = np.isnan(well_arr[:,2])
            not_nan = ~is_nan
            well_arr = well_arr[not_nan]

            well_dict[well_key] = well_arr

        # get maximum <\omega_\alpha> #
        oma_max = 0
        for key, data in well_dict.items():
            oma_chk = np.max(np.abs(data[:, 2]))
            if oma_chk > oma_max:
                oma_max = oma_chk

        # plotting axes #
        self.plotting()
        fig, axs = plt.subplots(2, 3, tight_layout=True, figsize=(18, 8))

        # define plotting axis dictionary #
        ax_dict = {}
        for i, well_ID in enumerate(well_IDs):
            i_mod = i % 3
            i_div = int((i-i_mod)/3)
            well_key = 'well {}'.format(well_ID)
            ax_dict[well_key] = axs[i_div, i_mod]

        for key, ax in ax_dict.items():
            # plot data #
            data = well_dict[key]
            smap = ax.scatter(data[:,0], data[:,1], c=data[:,2], edgecolor='k', s=25, marker='o', vmin=-1, vmax=0, cmap='jet')

            # axis limits #
            xmin, xmax = ax.get_xlim()
            if xmax < 0:
                ax.set_xlim(xmin, 0)
            else:
                ax.set_xlim(xmin, xmax)
            ax.set_ylim(0, ax.get_ylim()[1])

            # axis labels #
            cbar = fig.colorbar(smap, ax=ax)
            # cbar.ax.set_ylabel(r'$\langle \hat{\overline{\omega}}_{\alpha} \rangle$')
            cbar.ax.set_ylabel(r'$\langle R \rangle$')

            # grid #
            ax.grid()

        # ax_dict['well -3'].set_xlabel(r'$\langle \hat{R} \rangle$')
        # ax_dict['well -4'].set_xlabel(r'$\langle \hat{R} \rangle$')
        # ax_dict['well -5'].set_xlabel(r'$\langle \hat{R} \rangle$')
        ax_dict['well -3'].set_xlabel(r'$\langle \hat{\overline{\omega}}_{\alpha} \rangle$')
        ax_dict['well -4'].set_xlabel(r'$\langle \hat{\overline{\omega}}_{\alpha} \rangle$')
        ax_dict['well -5'].set_xlabel(r'$\langle \hat{\overline{\omega}}_{\alpha} \rangle$')
        
        ax_dict['well 0'].set_ylabel(r'$\gamma / (c_s/a)$')
        ax_dict['well -3'].set_ylabel(r'$\gamma / (c_s/a)$')
            
        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

    def plot_resonance_with_cross_phase(self, save_path=None):
        # organize gene and resonance data #
        gene_dict = {}
        colms = self.gene_colms
        for colm in colms:
            gene_dict[colm] = np.empty(self.tem_df.shape[0])

        res_data = np.empty(self.tem_df.shape[0])
        for i, (config_id, data) in enumerate(self.tem_df.iterrows()):
            res_data[i] = self.resonance[config_id][0]
            for j, colm in enumerate(colms):
                gene_dict[colm][i] = data[j]

        # plot data #
        self.plotting(fontsize=16, labelsize=18)
        fig, axs = plt.subplots(1, 2, sharex=True, sharey=True, tight_layout=True, figsize=(13, 6))

        smap0 = axs[0].scatter(10*res_data, gene_dict[colms[1]], c=gene_dict[colms[2]], edgecolor='k', s=25, marker='o', vmin=-1, vmax=1, cmap='seismic')
        smap1 = axs[1].scatter(10*res_data, gene_dict[colms[1]], c=gene_dict[colms[3]], edgecolor='k', s=25, marker='o', vmin=-1, vmax=1, cmap='seismic')

        # axis limits #
        axs[0].set_ylim(0, axs[0].get_ylim()[1])
        if all(res_data < 0):
            axs[0].set_xlim(axs[0].get_xlim()[0], 0.)
            # axs[0].set_xlim(-1.1, 0.)
        else:
            axs[0].set_xlim(axs[0].get_xlim())

        # axis labels #
        axs[0].set_xlabel(r'$10 \times \langle \hat{R} \rangle$')
        axs[1].set_xlabel(r'$10 \times \langle \hat{R} \rangle$')
        axs[0].set_ylabel(r'$\gamma / (c_s/a)$')

        ky = self.ky_tag.split('y')[1].replace('p', '.')
        fig.suptitle(r'$k_y/\rho_s = %s$' % ky)

        cbar0 = fig.colorbar(smap0, ax=axs[0])
        cbar0.ax.set_ylabel(r'$M_{n}$')
        cbar1 = fig.colorbar(smap1, ax=axs[1])
        cbar1.ax.set_ylabel(r'$M_{T_{\perp}}$')

        # grid #
        axs[0].grid()
        axs[1].grid()

        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

    def plot_resonance_spectrum(self, config_ids, ky_tag, save_path=None):
        # import data #
        if isinstance(config_ids, list):
            self.mod_list(config_ids)
        else:
            self.mod_list([config_ids])
        self.calculate_resonance_spectrum(ky_tag)

        # plotting parameters #
        self.plotting()
        fig, ax = plt.subplots(1, 1, tight_layout=True)

        # plot data #
        res_omg = -self.resonance_omega
        for config_id in self.config_list:
            res_spec = -self.resonance_spectrum[config_id]
            if config_id == '0-1-0':
                ax.plot(res_omg, res_spec, label='QHS')
            else:
                ax.plot(res_omg, res_spec, label=config_id)

        # axis labels #
        ax.set_xlabel(r'$-\hat{\omega}$')
        ax.set_ylabel(r'$-\langle \hat{R}(\hat{\omega}) \rangle_{\theta_b}$')

        # axis limits #
        ax.set_xlim(0, -self.resonance_omega[0])
        ax.set_ylim(0, ax.get_ylim()[1])

        # axis gris/legend #
        ax.grid()
        ax.legend(fontsize=16)

        # save/show #
        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

    def plot_resonance_along_field_line(self, config_id, ky_tag, norms='gene', save_path=None):
        # import data #
        ky_val = float(ky_tag.split('y')[1].replace('p','.'))
        self.mod_list([config_id])
        self.import_gist()
        self.import_normalizations(ky_tag)
        self.import_tem_data(ky_tag)

        # normalize and redfine varaibles #
        zeta = self.res_norms[config_id]['zeta']
        aLn = self.res_norms[config_id]['grad_n']
        aLT = self.res_norms[config_id]['grad_T']
        omg = self.tem_df.loc[config_id, 'omega_%s' % ky_tag]

        pol_full = self.bad_dict[config_id][0]
        idx_dom = np.where((pol_full >= -4*np.pi) & (pol_full <= 4*np.pi))[0]
        B_field = self.B_field[config_id][idx_dom]
        pol = self.bad_dict[config_id][0, idx_dom] / np.pi
        wgt = self.bad_dict[config_id][1, idx_dom]
        oma = self.bad_dict[config_id][2, idx_dom]
        
        if ky_tag == 'ky0p1':
            phi = self.bad_dict[config_id][4, idx_dom]
        elif ky_tag == 'ky0p4':
            phi = self.bad_dict[config_id][5, idx_dom]
        elif ky_tag == 'ky0p7':
            phi = self.bad_dict[config_id][6, idx_dom]
        elif ky_tag == 'ky1p0':
            phi = self.bad_dict[config_id][7, idx_dom]
        
        # calculate resonance #
        z = np.zeros(pol.shape)
        if omg < 0:
            pos_idx = np.where(oma < 0)[0]
        else:
            pos_idx = np.where(oma > 0)[0]
        z[pos_idx] = omg/(zeta*oma[pos_idx])

        # calculate average drift #
        oma_avg = 0.5*zeta*(np.trapz(oma*wgt, pol)/np.trapz(wgt, pol))
        oma_dag = -zeta*aLn

        R = np.zeros(pol.shape)
        PDF = np.sqrt(z) * np.exp(-z)
        # R[pos_idx] = omg*(omg+zeta*aLT*z[pos_idx]+zeta*(aLn-1.5*aLT))
        R[pos_idx] = omg*(omg+zeta*aLT*z[pos_idx]+zeta*(aLn-1.5*aLT))
        res = (R*PDF*phi*wgt) / (np.trapz(PDF*phi*wgt, pol))

        # set plotting parameters #
        self.plotting()
        fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(12, 6))
        axs_twn = axs[0].twinx()

        # plot data #
        plt1, = axs[0].plot(pol, B_field, c='tab:blue')
        plt2, = axs_twn.plot(pol, zeta*oma/oma_dag, c='tab:orange')
        axs[1].plot(pol, -res, c='k')

        # axis labels and tickmarks #
        axs[0].spines['right'].set_color(plt1.get_color())
        axs[0].tick_params(axis='y', colors=plt1.get_color())

        axs_twn.spines['right'].set_color(plt2.get_color())
        axs_twn.tick_params(axis='y', colors=plt2.get_color())

        axs[0].set_ylabel(r'B/T', color=plt1.get_color())
        # axs[1].set_ylabel(r'$\hat{R}\left(\theta_b,\, \hat{\omega}\right)$')
        axs[1].set_ylabel(r'$\omega(\omega-\omega_*^{T\prime})$')
        axs[1].set_xlabel(r'$\theta/\pi$')
        axs[1].set_yscale('log')

        axs_twn.set_ylabel(r'$\omega_{\alpha}/\omega_*$', color=plt2.get_color())
        # axs_twn.plot(pol, [omg]*pol.shape[0], c=plt2.get_color(), ls='--', label=r'$\hat{{\omega}} = {0:0.3f}$'.format(omg))
        axs_twn.plot(pol, [oma_avg/oma_dag]*pol.shape[0], c=plt2.get_color(), ls='-.', label=r'$\{{ \omega_{{\alpha}} \}}/\omega_* = {0:0.3f}$'.format(oma_avg/oma_dag))
        # axs_twn.plot(pol, [oma_dag]*pol.shape[0], c=plt2.get_color(), ls=':', label=r'$\langle \hat{{\omega}}_*^T \rangle = {0:0.3f}$'.format(oma_dag))

        if config_id == '0-1-0':
            axs[0].set_title(r'QHS $(k_y={})$'.format(ky_val))
        else:
            # axs[0].set_title(config_id)
            axs[0].set_title(r'{} $(k_y={})$'.format(config_id, ky_val))

        axs[0].set_title(r'$\omega/(c_s/a) = {0:0.4f}, \ \omega_*/(c_s/a)={1:0.4f}$'.format(omg, -zeta*aLn))

        # axis limits #
        ymax = max(np.abs(axs_twn.get_ylim()))
        axs_twn.set_ylim(-ymax, ymax)
        axs[1].set_xlim(pol[0], pol[-1])
        axs[1].set_xticks(np.linspace(-4, 4, 9))
        axs[1].set_ylim(1e-15, 1)

        # axis grids #
        axs[0].grid(True, axis='x')
        axs_twn.grid()
        axs[1].grid()

        axs_twn.legend(loc='upper left')

        # save/show #
        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

    def plot_omega_vs_avg_bad(self, ky_tag, norms='gene', save_path=None):
        # import data #
        self.import_normalizations(ky_tag)
        self.import_tem_data(ky_tag)

        # calculate velocity-space-averaged BADs #
        omg = []
        oma_avg = []
        for config_id in self.config_list:
            if not (self.tem_df.index == config_id).any():
                continue

            idx_dom = np.where(self.bad_dict[config_id][0] <= 0)[0]
            pol = self.bad_dict[config_id][0, idx_dom]
            wgt = self.bad_dict[config_id][1, idx_dom]
            oma = self.bad_dict[config_id][2, idx_dom]
            oma_avg_id = 0.5*(np.trapz(oma*wgt, pol) / np.trapz(wgt, pol))
            omg_id = self.tem_df.loc[config_id, 'omega_%s' % ky_tag]
            
            if norms == 'gene':
                oma_avg.append(oma_avg_id*self.gene_norms[config_id]['omega_alpha'])
                omg.append(omg_id)
            elif norms == 'physical':
                oma_avg.append(1e-3*oma_avg_id*self.phys_norms[config_id]['omega_alpha'])
                omg.append(1e-3*omg_id*self.phys_norms[config_id]['omega'])

        omg = np.array(omg)
        oma_avg = np.array(oma_avg)

        # linear fit #
        model = LinearRegression().fit(oma_avg.reshape((-1, 1)), omg)
        m = model.coef_[0]
        b = model.intercept_

        # set plotting parameters #
        self.plotting()
        fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

        # plot data #
        ax.scatter(oma_avg, omg, facecolor='None', edgecolor='k', s=15, marker='o')

        # axis labels #
        if norms == 'gene':
            ax.set_xlabel(r'$\langle \overline{\omega}_{\alpha} \rangle \ / \ (c_{\mathrm{ref}}/L_{\mathrm{ref}})$')
            ax.set_ylabel(r'$\omega \ / \ (c_{\mathrm{ref}}/L_{\mathrm{ref}})$')
        elif norms == 'physical':
            ax.set_xlabel(r'$\langle \overline{\omega} \rangle \ / \ (\mathrm{kHz})$')
            ax.set_ylabel(r'$\omega \ / \ (\mathrm{kHz})$')
        ax.set_title(r'$k_y\rho_{{\mathrm{{s}}}} = {}$'.format(ky_tag.split('y')[1].replace('p','.')))

        # axis limits #
        if norms == 'gene':
            ax.set_xlim(ax.get_xlim()[0], 0)
            ax.set_ylim(ax.get_ylim()[0], 0)
        elif norms == 'physical':
            ax.set_xlim(0, ax.get_xlim()[1])
            ax.set_ylim(0, ax.get_ylim()[1])
        
        # x_dom = np.array(ax.get_xlim())
        # y_dom = b + m*x_dom
        # omg_dia = self.gene_norms['1006-1-9']['grad_n']
        # ax.plot(ax.get_xlim(), [omg_dia]*2, ls='--', c='k', label=r'(m, b) = ({0:0.4f}, {1:0.4f})'.format(m,b))

        # grid/legend #
        ax.grid()
        # ax.legend()

        # save/show #
        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

    def plot_bad_along_field_line(self, config_id, ky_tag, norms='physical', save_path=None):
        # import data #
        self.mod_list([config_id])
        self.import_gist()
        self.import_tem_data(ky_tag)
        self.import_normalizations(ky_tag)

        # normalize data #
        B_field = self.B_field[config_id]
        pol_norm = self.bad_dict[config_id][0]/np.pi
        p_weight = self.bad_dict[config_id][1]
        oma = self.bad_dict[config_id][2]
        omg = self.tem_df.loc[config_id, 'omega_%s' % ky_tag]

        oma_hlf = oma[pol_norm <= 0]
        wgt_hlf = p_weight[pol_norm <= 0]
        pol_hlf = pol_norm[pol_norm <= 0]
        oma_avg = .5*np.trapz(oma_hlf*wgt_hlf, pol_hlf)/np.trapz(wgt_hlf, pol_hlf)

        if norms == 'physical':
            omg = 1e-3*omg*self.phys_norms[config_id]['omega']
            oma = 1e-3*oma*self.phys_norms[config_id]['omega_alpha']
            oma_avg = 1e-3*oma_avg*self.phys_norms[config_id]['omega_alpha']
            omg_diag = 1e-3*self.phys_norms[config_id]['omega_diag']

        elif norms == 'gene':
            oma = oma*self.gene_norms[config_id]['omega_alpha']
            omg = omg*self.gene_norms[config_id]['omega']
            oma_avg = oma_avg*self.gene_norms[config_id]['omega_alpha']

        else:
            raise KeyError(norms+" is not a valid normalization key.")

        # set plotting parameters #
        self.plotting()
        fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(12, 6))
        axs_twn = axs[0].twinx()

        # plot data #
        plt1, = axs[0].plot(pol_norm, B_field, c='tab:blue')
        plt2, = axs_twn.plot(pol_norm, oma, c='tab:orange')
        axs[1].plot(pol_norm, p_weight, c='k')

        # axis labels and tickmarks #
        axs[0].spines['right'].set_color(plt1.get_color())
        axs_twn.spines['right'].set_color(plt2.get_color())
        axs_twn.tick_params(axis='y', colors=plt2.get_color())

        axs[0].set_ylabel(r'B/T', color=plt1.get_color())
        axs[1].set_ylabel(r'$p\left(\theta_b\right)$')
        axs[1].set_xlabel(r'$\theta/\pi$')
        if norms == 'physical':
            axs_twn.set_ylabel(r'$\overline{\omega}_{\alpha}/z \ (\mathrm{kHz})$', color=plt2.get_color())
            axs_twn.plot(pol_norm, [omg]*pol_norm.shape[0], c=plt2.get_color(), ls='--', label=r'$\omega = {0:0.0f} \ (\mathrm{{kHz}})$'.format(omg))
            axs_twn.plot(pol_norm, [oma_avg]*pol_norm.shape[0], c=plt2.get_color(), ls=':', label=r'$\langle \overline{{\omega}}_{{\alpha}} \rangle = {0:0.0f} \ (\mathrm{{kHz}})$'.format(oma_avg))
            axs_twn.plot(pol_norm, [omg_diag]*pol_norm.shape[0], c=plt2.get_color(), ls=':', label=r'$\omega_{{\mathrm{{e}}*}} = {0:0.0f} \ (\mathrm{{kHz}})$'.format(omg_diag))

        elif norms == 'gene':
            axs_twn.set_ylabel(r'$\hat{\overline{\omega}}_{\alpha}$', color=plt2.get_color())
            axs_twn.plot(pol_norm, [omg]*pol_norm.shape[0], c=plt2.get_color(), ls='--', label=r'$\hat{{\omega}} = {0:0.3f}$'.format(omg))
            axs_twn.plot(pol_norm, [oma_avg]*pol_norm.shape[0], c=plt2.get_color(), ls=':', label=r'$\langle \hat{{\overline{{\omega}}}}_{{\alpha}} \rangle = {0:0.3f}$'.format(oma_avg))

        if config_id == '0-1-0':
            axs[0].set_title('QHS')
        else:
            axs[0].set_title(config_id)

        # axis limits #
        oma_max = .85*np.max(np.abs(oma))
        axs_twn.set_ylim(-oma_max, oma_max)
        axs[1].set_ylim(0, axs[1].get_ylim()[1])
        axs[1].set_xlim(pol_norm[0], pol_norm[-1])

        # axis grids #
        axs[0].grid(True, axis='x')
        axs_twn.grid()
        axs[1].grid()

        axs_twn.legend(loc='upper left', fontsize=14)

        # save/show #
        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

if __name__ == '__main__':
    bad_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'BAD_mn1824_res1024_npol16.h5')
    bad = BAD_class(bad_path)
    ky_tag = 'ky0p1'

    if True:
        bad.import_tem_data(ky_tag)
        bad.import_normalizations(ky_tag)
        bad.calculate_resonance(ky_tag)
        # save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'personal_meetings', 'Joey_Duff', '20231204', 'figures', 'omega_omega_star_hyp.png')
        bad.plot_gamma_vs_resonance(ky_tag)  # save_path=save_path)

    if False:
        config_id = '0-1-0'
        bad.mod_list([config_id])
        bad.import_gist()
        bad.import_normalizations(ky_tag)
        bad.import_tem_data(ky_tag)
        
        omg = bad.tem_df.loc[config_id, 'omega_%s' % ky_tag]
        omn = bad.gene_norms[config_id]['grad_n']
        omt = bad.gene_norms[config_id]['grad_T']

        idx_dom = np.where(bad.bad_dict[config_id][0] <= 0)[0]
        pol = bad.bad_dict[config_id][0, idx_dom]
        wgt = bad.bad_dict[config_id][1, idx_dom]
        oma = bad.bad_dict[config_id][2, idx_dom] * bad.gene_norms[config_id]['omega_alpha']

        omg_oma = np.zeros(oma.shape)
        pos_idx = np.where(oma < 0)[0]
        omg_oma[pos_idx] = omg/oma[pos_idx]

        oma_avg = np.trapz(omg_oma*wgt, pol) / np.trapz(wgt, pol)  # 0.5*(np.trapz(oma*wgt, pol)/np.trapz(wgt, pol))

        print('omg = {}'.format(omg))
        print('omn = {}'.format(omn))
        print('oma_avg = {}'.format(oma_avg))
