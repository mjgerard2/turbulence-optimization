import numpy as np


def v_par(M, params):
    theta_lim = 2 * np.arcsin(M)

    npts = int(theta_lim / params['Deta'])
    theta_dom = np.linspace(params['Deta'], theta_lim, npts, endpoint=False)

    Ro = params['Ro']
    rho = params['rho']

    v = np.sqrt(2 * (rho / Ro)) * np.sqrt(M**2 - np.sin(theta_dom / 2)**2)

    return v, theta_dom


def Kn(theta, params):
    Ro = params['Ro']
    rho = params['rho']
    kappa = params['kappa']
    iota = params['iota']

    Kn_coef = -(kappa / Ro)
    Kn_numer = np.cos(theta) + (rho/Ro) * (iota**2 - np.cos(theta)**2)
    Kn_denom = np.sqrt(1 + (kappa**2 - 1) * np.cos(theta)**2)
    Kn = Kn_coef * (Kn_numer / Kn_denom)

    return Kn


def Kg(theta, params):
    Ro = params['Ro']
    rho = params['rho']
    kappa = params['kappa']
    iota = params['iota']

    Kg_coef = 1. / Ro
    Kg_numer = np.sin(theta) - 0.5*(rho/Ro)*(1 + iota**2 * (kappa**2 - 1))*np.sin(2*theta)
    Kg_denom = np.sqrt(1 + (kappa**2 - 1) * np.cos(theta)**2)
    Kg = Kg_coef * (Kg_numer / Kg_denom)

    return Kg


def Grad_Psi(theta, params):
    Ro = params['Ro']
    rho = params['rho']
    kappa = params['kappa']
    norm = params['normalization']

    GradPsi = ((2*np.pi)**2 / norm) * Ro * rho
    GradPsi = GradPsi * (1 - (rho/Ro)*np.cos(theta))
    GradPsi = GradPsi * np.sqrt(1 + (kappa**2 - 1) * np.cos(theta)**2)

    return GradPsi


def B(theta, params):
    Ro = params['Ro']
    rho = params['rho']
    norm = params['normalization']

    modB = ((2*np.pi)**2 / norm) * Ro
    modB = modB * (1 - (rho / Ro) * np.cos(theta))

    return modB


def jacob(theta, params):
    rho = params['rho']
    Ro = params['Ro']
    norm = params['normalization']

    sqrG = (norm / (2*np.pi)**2) * (1 + 2*(rho/Ro)*np.cos(theta))

    return sqrG


def D(theta, params):
    Ro = params['Ro']
    rho = params['rho']
    kappa = params['kappa']
    norm = params['normalization']
    iota = params['iota']
    sigma = params['sigma']

    if kappa == 1:
        coefD = 1. / (rho * Ro)
        coefD = coefD * (norm / (4*np.pi*np.pi))
        coefD = coefD * ((2*sigma/iota) - (1./Ro))

        D1 = np.sin(theta)
        D2 = 0.
        D3 = 0.

    else:
        coefD = (norm / (2*np.pi)**2)

        theta_low = theta[theta < (-0.5 * np.pi)]
        theta_mid = theta[(theta >= (-0.5 * np.pi)) & (theta <= (0.5 * np.pi))]
        theta_hgh = theta[theta > (0.5 * np.pi)]

        coefD1 = 1. / (kappa*rho*rho*Ro)
        coefD1 = coefD1 * (((kappa*kappa + 1)/kappa) - (Ro/iota)*sigma)

        D1_low = (theta_low - np.arctan(np.tan(theta_low) / kappa) + np.pi)
        D1_mid = (theta_mid - np.arctan(np.tan(theta_mid) / kappa))
        D1_hgh = (theta_hgh - np.arctan(np.tan(theta_hgh) / kappa) - np.pi)
        D1 = coefD1 * np.append(np.append(D1_low, D1_mid), D1_hgh)

        coefD2 = (2./(kappa*rho*Ro))
        coefD2 = coefD2 * ((sigma/(iota*kappa)) - ((2*kappa*kappa-1)/(Ro*kappa*kappa)))
        if kappa > 1:
            D2 = kappa / np.sqrt(kappa**2 - 1)
            D2 = coefD2 * D2 * np.arctanh((np.sqrt(kappa**2 - 1) / kappa) * np.sin(theta))

        elif kappa < 1:
            coef1 = - 1j * (kappa / np.sqrt(1 - kappa**2))
            coef2 = 1j * (np.sqrt(1 - kappa**2) / kappa)

            D2 = coefD2 * coef1 * np.arctanh(coef2 * np.sin(theta))
            D2 = D2.real

        D3_numer = -2 * rho * np.sin(theta) + Ro * (kappa**2 - 1) * np.sin(2*theta)
        D3_denom = ((rho*Ro)**2) * kappa * (1 + kappa**2 + (kappa**2 - 1) * np.cos(2*theta))
        D3 = D3_numer / D3_denom

    shearD = coefD * (D1 + D2 + D3)
    return shearD


def shear(params):
    Ro = params['Ro']
    rho = params['rho']
    iota = params['iota']
    kappa = params['kappa']
    norm = params['normalization']
    sigma = params['sigma']

    iotaP = (1./(rho**2)) * (norm / (2*np.pi)**2)
    iotaP = iotaP * ((sigma/kappa) - (iota/Ro) * ((kappa**2 + 1) / kappa**2))

    return iotaP
