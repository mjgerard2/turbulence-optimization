import numpy as np
import field_functions as ff

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs

import os


# Plot Settings #
font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Field Parameters #
params = {'Deta': 1e-3,
          'Ro': 1.5,
          'minor': 0.05,
          'Delta': 0.1,
          'iota': 1.05,
          'normalization': 250}#4*np.pi**2}

path = os.path.join('/home', 'michael', 'Desktop', '3D_chrisModel_kScan')

# scan_dircs = ['scan_4', 'scan_5', 'scan_6']
scan_dircs = ['scan_4']
nfps = [8, 10, 12]

for sdx, scan_dirc in enumerate(scan_dircs):
    print(scan_dirc)
    scan_path = os.path.join(path, scan_dirc)
    params['N'] = nfps[sdx]

    k_dircs = [f.name for f in os.scandir(scan_path) if os.path.isdir(f)]
    k_vals = sorted([float(k[1::]) for k in k_dircs])
    for k_val in k_vals:
        k_dirc = 'k{0:0.2f}'.format(k_val)
        k_path = os.path.join(scan_path, k_dirc)

        params['kappa'] = k_val
        params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['minor']

        # Calculate Fields #
        eta_dom = np.linspace(-np.pi, np.pi, 501)

        Bmod = ff.B(eta_dom, params)
        Grad_Psi = ff.Grad_Psi(eta_dom, params)
        Kn = ff.Kn(eta_dom, params)
        Kg = ff.Kg(eta_dom, params)
        D = ff.D(eta_dom, params)

        # Generate Figure Layout #
        fig = plt.figure(tight_layout=True, figsize=(8, 12))
        spec = gs.GridSpec(5, 1, figure=fig)

        ax11 = fig.add_subplot(spec[0])
        ax12 = ax11.twinx()

        ax2 = fig.add_subplot(spec[1], sharex=ax11)
        ax3 = fig.add_subplot(spec[2], sharex=ax11)
        ax4 = fig.add_subplot(spec[3], sharex=ax11)

        ax51 = fig.add_subplot(spec[4], sharex=ax11)
        ax52 = ax51.twinx()

        plt.setp(ax11.get_yticklabels(), visible=True, color='tab:red')
        plt.setp(ax11.get_xticklabels(), visible=False)
        plt.setp(ax12.get_yticklabels(), visible=True, color='tab:blue')
        plt.setp(ax2.get_xticklabels(), visible=False)
        plt.setp(ax3.get_xticklabels(), visible=False)
        plt.setp(ax4.get_xticklabels(), visible=False)
        plt.setp(ax52.get_yticklabels(), visible=True, color='tab:red')

        # Plot Data #
        eta_norm = eta_dom / np.pi

        ax11.plot(eta_norm, Kn, c='tab:red')
        ax12.plot(eta_norm, Kg, c='tab:blue')
        ax2.plot(eta_norm, D, c='k')
        ax3.plot(eta_norm, Grad_Psi, c='k')
        ax4.plot(eta_norm, Bmod, c='k')

        curve_drive = Kn/Grad_Psi + D*Kg*(Grad_Psi / Bmod)
        ax51.plot(eta_norm, curve_drive, c='k')
        ax52.plot(eta_norm, Kn / Grad_Psi, c='tab:red', ls='--')
        ax51.plot(eta_norm, np.zeros(eta_norm.shape), c='k', ls='--', linewidth=1)

        # Plot Labels #
        ax51.set_xlabel(r'$\eta (\pi)$')
        ax51.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D\kappa_g \frac{|\nabla \psi|}{B}$')
        ax52.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|}$', c='tab:red')

        ax11.set_ylabel(r'$\kappa_n$', c='tab:red')
        ax12.set_ylabel(r'$\kappa_g$', c='tab:blue')
        ax2.set_ylabel(r'$D$')
        ax3.set_ylabel(r'$|\nabla \psi|$')
        ax4.set_ylabel(r'$|B|$')

        # Plot Limits #
        ax11.set_xlim(eta_dom[0] / np.pi, eta_dom[-1] / np.pi)

        Kn_max = 1.1 * np.max(np.abs(Kn))
        ax11.set_ylim(-Kn_max, Kn_max)

        Kg_max = 1.1 * np.max(np.abs(Kg))
        ax12.set_ylim(-Kg_max, Kg_max)

        KD_max = np.max(np.array([np.max(np.abs(curve_drive)), np.max(np.abs(Kn/Grad_Psi))]))
        ax51.set_ylim(-KD_max, KD_max)
        ax52.set_ylim(-KD_max, KD_max)

        # Plot Styles #
        ax11.grid()
        ax2.grid()
        ax3.grid()
        ax4.grid()
        ax51.grid()

        ax11.set_title('Model Fields')

        saveName = os.path.join(k_path, 'fields_model_2nd_order.png')
        # plt.savefig(saveName)
        # plt.close('all')
        plt.show()
        break
