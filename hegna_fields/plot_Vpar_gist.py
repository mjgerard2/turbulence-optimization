import numpy as np

from scipy.interpolate import interp1d

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs

import sys, os
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import gistTools as gt


"""
# Specify ConIDs #
conIDs = ['5-1-485', 'QHS', '897-1-0']
base_path = os.path.join('/mnt', 'HSX_Database', 'GENE')
for conID in conIDs:
    if conID != 'QHS':
        gist_path = os.path.join(base_path, 'eps_valley', 'geomdir', 'gist_files', 'gist_genet_wout_HSX_{}_s0500_npol2_nz512_alpha0_bean'.format(conID))
    else:
        gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_s0p5_alpha0_npol2_nz2048.dat')

    nfp = 4
"""
# Specify Scan Directory #
path = os.path.join('/home', 'michael', 'Desktop', '3D_model_kScan')

nfps = [10, 12]
scan_dircs = ['scan_2']
# scan_dircs = ['scan_4', 'scan_5', 'scan_6']

for sdx, scan_dirc in enumerate(scan_dircs):
    print(scan_dirc)
    scan_path = os.path.join(path, scan_dirc)
    k_dircs = [f.name for f in os.scandir(scan_path) if os.path.isdir(f)]

    for k_dirc in ['k2.00']:
        print('   '+k_dirc)
        # Import Gist File #
        k_path = os.path.join(scan_path, k_dirc)
        gist_path = os.path.join(k_path, 'gist_s0p98_alpha0_npol1_nz1024.dat')

        nfp = nfps[sdx]
        gist = gt.read_gist(gist_path)
        gist.calc_shear(nfp)

        #####################
        #    Slecting Well  #
        #####################
        eta_lim = 1.25 * np.pi
        eta_stp = (nfp * gist.q0 - 1) * gist.hlf_stp
        eta_full = (nfp * gist.q0 - 1) * gist.pol_dom
        eta_dom = eta_full[(eta_full >= -eta_lim) & (eta_full <= eta_lim)]

        B = gist.data['modB'][(eta_full >= -eta_lim) & (eta_full <= eta_lim)]

        eta_lft = eta_dom[eta_dom < -.5*np.pi]
        eta_mid = eta_dom[(eta_dom >= -.5*np.pi) & (eta_dom <= .5*np.pi)]
        eta_rgt = eta_dom[eta_dom > .5*np.pi]

        B_lft = B[eta_dom < -.5*np.pi]
        B_mid = B[(eta_dom >= -.5*np.pi) & (eta_dom <= .5*np.pi)]
        B_rgt = B[eta_dom > .5*np.pi]

        idx_max_lft = np.argmax(B_lft)
        idx_min = np.argmin(B_mid)
        idx_max_rgt = np.argmax(B_rgt)

        eta_max_lft, Bmax_lft = gt.get_extrema(B_lft, eta_lft, idx_max_lft)
        eta_min, Bmin = gt.get_extrema(B_mid, eta_mid, idx_min)
        eta_max_rgt, Bmax_rgt = gt.get_extrema(B_rgt, eta_rgt, idx_max_rgt)

        Bmax = np.max(np.array([Bmax_lft, Bmax_rgt]))
        B_extrema = np.array([Bmin, Bmax])

        npts = 2 * int((eta_max_rgt - eta_max_lft) / eta_stp) + 1
        eta_well = np.linspace(eta_max_lft, eta_max_rgt, npts)
        pol_well = eta_well / (nfp * gist.q0 - 1)

        B_well = gist.model['modB'](pol_well)
        #####################
        #        End        #
        #####################

        # Calculate Curvature Drive Term #
        pol_set, Vpar_set = gist.calc_Vpar_test(B_well, B_extrema, pol_well, 1, 4)
        #pol_set, Vpar_set = gist.calc_Vpar(1, nfp)
        pol_dom = pol_well #pol_set[0]

        modB = gist.model['modB'](pol_dom)
        gradPsi = gist.model['gxx'](pol_dom)
        Kn = gist.model['Kn'](pol_dom)
        Kg = gist.model['Kg'](pol_dom)
        D = gist.model['D'](pol_dom)

        curve_drive = Kn / gradPsi
        curve_drive = curve_drive + D * Kg * gradPsi / modB

        curve_max = 1.1 * np.max(np.abs(curve_drive))

        M_dom = np.linspace(0.1, 1, 10)
        pol_dom_set = []
        v_par_set = []
        for M in M_dom:
            # pol_set, Vpar_set = gist.calc_Vpar(M, nfp)
            pol_set, Vpar_set = gist.calc_Vpar_test(B_well, B_extrema, pol_well, M, 4)
            pol_dom_set.append(pol_set)
            v_par_set.append(Vpar_set)

        # Define Plotting Parameters #
        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': 18}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = 22
        mpl.rcParams['lines.linewidth'] = 2

        # Construct Figure Layout #
        fig = plt.figure(constrained_layout=True, figsize=(8, 6))
        spec = gs.GridSpec(3, 1, figure=fig)

        ax1 = fig.add_subplot(spec[1:3, :])
        ax2 = fig.add_subplot(spec[0, :], sharex=ax1)
        plt.setp(ax2.get_xticklabels(), visible=False)
        ax3 = ax2.twinx()

        ax3.spines['right'].set_color('tab:red')
        ax3.tick_params(axis='y', colors='tab:red')

        pol_norm = pol_dom / np.pi
        cmap = plt.get_cmap('tab20b', 10)

        for Mdx, M in enumerate(np.flip(M_dom)):
            mdx = M_dom.shape[0] - 1 - Mdx
            for i in range(len(pol_dom_set[mdx])):
                if pol_dom_set[mdx][i] is None:
                    continue
                else:
                    pol_norm = pol_dom_set[mdx][i] / np.pi
                    v_par = v_par_set[mdx][i]
                    ax1.fill_between(pol_norm, np.zeros(pol_norm.shape), v_par, color=cmap(0.999*M))

        pos1 = ax1.get_position()
        ax1c = fig.add_axes([0.85, pos1.y0+.025, pos1.width/20, pos1.height])
        norm = mpl.colors.Normalize(vmin=0, vmax=1)
        cb1 = mpl.colorbar.ColorbarBase(ax1c, cmap=cmap, norm=norm, orientation='vertical')
        cb1.ax.set_ylabel(r'$M$')

        pol_norm = pol_dom / np.pi
        ax2.plot(pol_norm, modB, c='k', linewidth=4)
        ax3.plot(pol_norm, curve_drive, c='tab:red', linewidth=4)
        # ax3.plot(pol_norm, Kn / gradPsi, c='tab:blue', linewidth=4, ls='--', label=r'$\kappa_n / |\nabla \psi|$')
        ax3.plot(pol_norm, np.zeros(pol_norm.shape), ls='--', c='tab:red')

        neg_idx = np.where(curve_drive < 0)[0]
        neg_bool = ((neg_idx - np.roll(neg_idx, 1)) != 1)
        negDX = np.where(neg_bool)[0]

        alpha_clr = 0.3
        for i, idx in enumerate(negDX[0:-1]):
            pol_beg = neg_idx[idx]
            pol_end = neg_idx[negDX[i+1] - 1] + 1

            ax1.axvspan(pol_norm[pol_beg], pol_norm[pol_end], alpha=alpha_clr, color='tab:red')
            ax2.axvspan(pol_norm[pol_beg], pol_norm[pol_end], alpha=alpha_clr, color='tab:red')

        if len(negDX) > 0:
            pol_beg = neg_idx[negDX[-1]]
            pol_end = neg_idx[-1]
            ax1.axvspan(pol_norm[pol_beg], pol_norm[pol_end], alpha=alpha_clr, color='tab:red')
            ax2.axvspan(pol_norm[pol_beg], pol_norm[pol_end], alpha=alpha_clr, color='tab:red')

        ax1.set_xlabel(r'$\theta/\pi$')
        ax1.set_ylabel(r'$\frac{ v_{\parallel} }{v}$')

        ax2.set_ylabel(r'$B$')
        ax3.set_ylabel(r'$\frac{ \kappa_n }{ |\nabla \psi| } + D \kappa_g \frac{ |\nabla \psi| }{B}$', c='tab:red')

        ax1.set_xlim(pol_norm[0], pol_norm[-1])
        ax1.set_ylim(0, 1)
        ax2.set_ylim(0.99*np.min(modB), 1.01*np.max(modB))
        ax3.set_ylim(-curve_max, curve_max)
        ax2.grid()

        # ax3.legend(loc='upper center')

        plt.show()
        # saveName = os.path.join(k_path, 'Vpar_gist.png')
        saveName = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'HSX_Group', '2022', '2022-02-11', 'Vpar_{}.png'.format(conID))
        # plt.savefig(saveName)
