import numpy as np
import os

tempPath = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'input.3D_model')
with open(tempPath, 'r') as f:
    tempLines = f.readlines()

Ro = 1.
a = 0.1
Delta = 0.1
kappa_set = np.linspace(1, 10, 10)
rho_set = np.sqrt(2 / (5*kappa_set**2 - 4*kappa_set**2 + 1)) * a

for kdx, kappa in enumerate(kappa_set):
    rho = rho_set[kdx]

    mode = [[Ro, 0],
            [Delta, Delta],
            [0.5*rho*(3*kappa - 1), 0.5*rho*(3*kappa - 1)],
            [-0.5*rho*(kappa - 1), -0.5*rho*(kappa - 1)]]

    newLines = tempLines
    for i, idx in enumerate(range(34, 38)):
        line = newLines[idx].split()
        line[2] = str(mode[i][0])
        line[5] = str(mode[i][1])

        newLines[idx] = '  ' + ' '.join(line) + '\n'

    newPath = os.path.join('/home', 'michael', 'Desktop', '3D_model_kScan', 'input.3D_model_k{0:0.0f}'.format(kappa))
    with open(newPath, 'w') as f:
        for line in newLines:

            f.write(line)
