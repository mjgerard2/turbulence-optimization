import numpy as np
import h5py as hf
import field_functions as ff

from scipy.special import ellipk
from matplotlib import pylab

import sys, os
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd


def omega_drift(M, params):
    params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['minor']

    P = params['rho'] / params['Delta']
    P2 = P**2
    P_inv = 1. / P

    iotaP = ff.iota_prime(params)
    shear = iotaP / (params['N'] - params['iota'])

    rho_prime = params['N'] * params['rho'] / params['Ro']
    Delta_prime = params['N'] * params['Delta'] / params['Ro']

    kappa_low = 0.25 * P_inv * (np.sqrt(1 + 16*P2) - 1)
    kappa_hgh = 0.25 * P_inv * (np.sqrt(1 + 16*P2) + 1)

    if params['kappa'] < kappa_low:
        nu, eta_dom, index, second_well = ff.low_kappa_nu(M, params)
        nu_sqrt = np.sqrt(nu)

    elif params['kappa'] >= kappa_low and kappa <= kappa_hgh:
        nu, eta_dom = ff.mid_kappa_nu(M, params)
        nu_sqrt = np.sqrt(nu)
        second_well = False

    else:
        nu, eta_dom = ff.hgh_kappa_nu(M, params)
        nu_sqrt = np.sqrt(nu)
        second_well = False

    Kn = ff.Kn(eta_dom, params)
    Kg = ff.Kg(eta_dom, params)
    B = ff.B(eta_dom, params)
    Grad_Psi = ff.Grad_Psi(eta_dom, params)
    jacob = ff.jacob(eta_dom, params)
    D = ff.D(eta_dom, params)

    integ_scale = jacob * B / nu_sqrt

    dJ_dPsi = 2 * rho_prime * Delta_prime * shear * nu
    dJ_dPsi = dJ_dPsi - Kn / Grad_Psi
    dJ_dPsi = dJ_dPsi - D * Kg * Grad_Psi / B
    dJ_dPsi = integ_scale * dJ_dPsi

    if second_well:
        dJ_dPsi_1well = np.trapz(dJ_dPsi[index[0]:index[1]], eta_dom[index[0]:index[1]])
        dJ_dK_1well = np.trapz(integ_scale[index[0]:index[1]], eta_dom[index[0]:index[1]])
        omega_D = dJ_dPsi_1well / dJ_dK_1well

        dJ_dPsi_2well = np.trapz(dJ_dPsi[index[1]:], eta_dom[index[1]:])
        dJ_dK_2well = np.trapz(integ_scale[index[1]::], eta_dom[index[1]::])
        omega_D_2well = dJ_dPsi_2well / dJ_dK_2well

    else:
        dJ_dPsi = np.trapz(dJ_dPsi, eta_dom)
        dJ_dK = np.trapz(integ_scale, eta_dom)
        omega_D = dJ_dPsi / dJ_dK

        omega_D_2well = np.nan

    return omega_D, omega_D_2well


params = {'Deta': 1e-3,
          'Ro': 1.5,
          'Delta': 0.1,
          'N': 10,
          'iota': 1.05,
          'kappa': 1.,
          'minor': 0.05,
          'normalization': 4*np.pi**2}

Mpts = 250
Mstp = 1 / Mpts
M_dom = np.linspace(Mstp, 1, Mpts, endpoint=False)

kappa_dom = np.linspace(1, 2, 5)
#kappa_dom = np.linspace(0.5, 1, 6)
omega_set = np.empty((kappa_dom.shape[0], M_dom.shape[0]))

omega_scnd = []
kappa_scnd = []
M_scnd = []

for idx, kappa in enumerate(kappa_dom):
    params['kappa'] = kappa
    params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['minor']
    for Mdx, M in enumerate(M_dom):
        omega_D, omega_D_2well = omega_drift(M, params)
        omega_set[idx, Mdx] = omega_D
        if not np.isnan(omega_D_2well):
            if 'M_temp' in globals():
                M_temp.append(M)
                omega_temp.append(omega_D_2well)

            else:
                M_temp = [M]
                omega_temp = [omega_D_2well]

        else:
            omega_set[idx, Mdx] = omega_D

    if 'M_temp' in globals():
        M_scnd.append(M_temp)
        omega_scnd.append(omega_temp)
        kappa_scnd.append(kappa)

        del M_temp
        del omega_temp

omega_scnd = np.array(omega_scnd)
M_scnd = np.array(M_scnd)
kappa_scnd = np.array(kappa_scnd)

if True:
    plot = pd.plot_define(lineWidth=2, figSize=(10, 6))
    plt = plot.plt

    ls = ['solid', 'dashed', 'dashdot', (0, (3, 1, 1, 1, 1, 1)), (0, (1, 1))]
    colors = pylab.cm.copper(np.linspace(0,1, 5))
    plt.plot([0, 1], [0, 0], c='k', ls='--', linewidth=1)
    for kdx, kappa in enumerate(kappa_dom):
        plt1, = plt.plot(M_dom, omega_set[kdx], c=colors[kdx], ls=ls[kdx], label=r'$\kappa = $'+'{0:0.3f}'.format(kappa))

        if len(kappa_scnd) > 0:
            idx = np.argmin(np.abs(kappa_scnd - kappa))
            if kappa_scnd[idx] == kappa:
                plt.plot(M_scnd[idx], omega_scnd[idx], c=plt1.get_color(), ls='--')

    idx_spec = np.argmin(np.abs(M_dom - 0.93))
    plt.scatter(M_dom[idx_spec], omega_set[0][idx_spec], c='tab:red', s=150, marker='o', zorder=10)

    plt.xlabel(r'$M$')
    plt.ylabel(r'$\frac{e}{k_{\alpha} K} \langle\omega_{D}\rangle$')

    plt.xlim(0, 1)

    box = plot.ax.get_position()
    plot.ax.set_position([box.x0, box.y0, box.width * 1, box.height])
    plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1), fontsize=14)

    plt.tight_layout()
    plt.grid()

    # plt.show()
    saveName = os.path.join('/home','michael','Desktop', '3D_chrisModel_kScan', 'scan_5', 'omegaD_model.png')
    plt.savefig(saveName)
