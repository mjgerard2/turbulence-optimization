import numpy as np
import field_functions_tok as ff

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs

import os


# Plot Settings #
font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

path = os.path.join('/home', 'michael', 'Desktop', 'Tokamak_model_kScan')
scan_dircs = ['scan_1']

for scan_dirc in scan_dircs:
    scan_path = os.path.join(path, scan_dirc)
    k_dircs = [f.name for f in os.scandir(scan_path) if os.path.isdir(f)]

    for k_dirc in k_dircs:
        k_path = os.path.join(scan_path, k_dirc)
        kappa = float(k_dirc[1::])

        # Field Parameters #
        params = {'Deta': 1e-3,
                  'Ro': 1.5,
                  'iota': 1./3,
                  'kappa': kappa,
                  'minor': 0.15,
                  'normalization': 4*np.pi**2,
                  'sigma': .75,
                  'alpha': 1}

        params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['minor']

        # Calculate Fields #
        theta_dom = np.linspace(-np.pi, np.pi, 501)

        Bmod = ff.B(theta_dom, params)
        Grad_Psi = ff.Grad_Psi(theta_dom, params)
        Kn = ff.Kn(theta_dom, params)
        Kg = ff.Kg(theta_dom, params)
        D = ff.D(theta_dom, params)

        # Generate Plots #
        fig = plt.figure(tight_layout=True, figsize=(8, 12))
        spec = gs.GridSpec(5, 1, figure=fig)

        ax11 = fig.add_subplot(spec[0])
        ax12 = ax11.twinx()

        ax2 = fig.add_subplot(spec[1], sharex=ax11)
        ax3 = fig.add_subplot(spec[2], sharex=ax11)
        ax4 = fig.add_subplot(spec[3], sharex=ax11)
        ax5 = fig.add_subplot(spec[4], sharex=ax11)

        plt.setp(ax11.get_yticklabels(), visible=True, color='tab:red')
        plt.setp(ax11.get_xticklabels(), visible=False)
        plt.setp(ax12.get_yticklabels(), visible=True, color='tab:blue')
        plt.setp(ax2.get_xticklabels(), visible=False)
        plt.setp(ax3.get_xticklabels(), visible=False)
        plt.setp(ax4.get_xticklabels(), visible=False)

        # Plot Data #
        theta_norm = theta_dom / np.pi

        ax11.plot(theta_norm, Kn, c='tab:red')
        ax12.plot(theta_norm, Kg, c='tab:blue')
        ax2.plot(theta_norm, D, c='k')
        ax3.plot(theta_norm, Grad_Psi, c='k')
        ax4.plot(theta_norm, Bmod, c='k')

        curve_drive = Kn/Grad_Psi + D*Kg*(Grad_Psi / Bmod)
        ax5.plot(theta_norm, curve_drive, c='k')
        ax5.plot(theta_norm, np.zeros(theta_norm.shape), c='k', ls='--', linewidth=1)

        # Plot Labels #
        ax5.set_xlabel(r'$\theta (\pi)$')

        ax11.set_ylabel(r'$\kappa_n$', c='tab:red')
        ax12.set_ylabel(r'$\kappa_g$', c='tab:blue')
        ax2.set_ylabel(r'$D$')
        ax3.set_ylabel(r'$|\nabla \psi|$')
        ax4.set_ylabel(r'$|B|$')
        ax5.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D\kappa_g \frac{|\nabla \psi|}{B}$')

        # Plot Limits #
        ax11.set_xlim(theta_dom[0] / np.pi, theta_dom[-1] / np.pi)

        Kn_max = 1.1 * np.max(np.abs(Kn))
        ax11.set_ylim(-Kn_max, Kn_max)

        Kg_max = 1.1 * np.max(np.abs(Kg))
        ax12.set_ylim(-Kg_max, Kg_max)

        # Plot Styles #
        ax11.grid()
        ax2.grid()
        ax3.grid()
        ax4.grid()
        ax5.grid()

        ax11.set_title('Model Fields')

        # plt.show()
        saveName = os.path.join(k_path, 'fields_model.png')
        plt.savefig(saveName)
        plt.close('all')
