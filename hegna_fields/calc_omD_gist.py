import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import gistTools.gist_reader as gr

wellID = 1

# Calculate Omega Data for M_{1/2} #
base_dirc = os.path.join('/home', 'michael', 'Desktop', '3D_model_kScan', 'scan_12')
k_dircs = [f.name for f in os.scandir(base_dirc) if os.path.isdir(f)]

m0p5 = np.empty((len(k_dircs), 2))
for kdx, k_dirc in enumerate(k_dircs):
    gist_path = os.path.join(base_dirc, k_dirc, 'gist_genet_wout_'+k_dirc+'_res1024_s0p95_npol1.dat')
    gist = gr.read_gist(gist_path, nfp=10)
    gist.calc_shear()
    gist.calculate_Omega_singleWell(wellID)

    m0p5[kdx, 0] = float(k_dirc[1::].replace('p', '.'))
    m0p5[kdx, 1] = gist.Omega

m0p5 = np.array(sorted(m0p5, key=lambda x: x[0]))

# Calculate Omega Data for M_{0} #
base_dirc = os.path.join('/home', 'michael', 'Desktop', '3D_chrisModel_kScan', 'scan_7')
k_dircs = [f.name for f in os.scandir(base_dirc) if os.path.isdir(f)]

m0 = np.empty((len(k_dircs), 2))
for kdx, k_dirc in enumerate(k_dircs):
    gist_path = os.path.join(base_dirc, k_dirc, 'gist_genet_wout_'+k_dirc+'_res1024_s0p95_npol1.dat')
    gist = gr.read_gist(gist_path, nfp=10)
    gist.calc_shear()
    gist.calculate_Omega_singleWell(wellID)

    m0[kdx, 0] = float(k_dirc[1::].replace('p', '.'))
    m0[kdx, 1] = gist.Omega

m0 = np.array(sorted(m0, key=lambda x: x[0]))

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['lines.linewidth'] = 2

# Plot Axis #
fig, ax = plt.subplots(1, 1, tight_layout=True)

ax.plot(m0[:, 0], m0[:, 1], ls='--', c='k', marker='o', markerfacecolor='tab:red', markersize=15, label=r'$M_{0}$')
ax.plot(m0p5[:, 0], m0p5[:, 1], ls='--', c='k', marker='s', markerfacecolor='tab:blue', markersize=15, label=r'$M_{1/2}$')

ax.set_xlabel(r'$\kappa$')
ax.set_ylabel(r'$\overline{\langle \omega_D \rangle}$')

ax.grid()
ax.legend(fontsize=16)
plt.show()
