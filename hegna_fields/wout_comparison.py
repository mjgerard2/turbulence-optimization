#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 10 15:29:47 2021

@author: michael
"""

import numpy as np

import os, sys
path = os.path.dirname(os.path.abspath(os.getcwd()))
sys.path.append(path)

import plot_define as pd
import vmecTools.wout_files.wout_read as wr

upts = 161
s_dom = np.linspace(0, 1, 128)
u_dom = np.linspace(0, 2*np.pi, upts)
v_dom = np.linspace(0, 2*np.pi, upts * 4)
v_dom_sub = np.linspace(0, 0.375*np.pi, 4)

s_idx_1 = np.argmin(np.abs(s_dom - 0.0625))
s_idx_2 = np.argmin(np.abs(s_dom - 0.25))
s_idx_3 = np.argmin(np.abs(s_dom - 0.5625))

plot = pd.plot_define(eqAsp=True, lineWidth=2)

path = os.path.join('/home','michael','Desktop','Research','stellarators','HSX','Epsilon_Valley','wout_files')
names = ['wout_41-1-421.nc', 'wout_89-1-3.nc']
colors = ['k', 'tab:red']
for ndx, name in enumerate(names):
    conID = name.split('_')[1].split('.')[0]
    
    wout = wr.readWout(path, name=name)
    wout.transForm_3D(u_dom, v_dom_sub, ['R','Z'])
    
    R_major = wout.R_major
    plot.plt.scatter([R_major], [0], s=250, marker='*', c=colors[ndx])
    
    R = wout.invFourAmps['R']
    Z = wout.invFourAmps['Z']
    
    for i in range(v_dom_sub.shape[0]):
        plot.plt.scatter(R[0,i,0], Z[0,i,0], c=colors[ndx], s=15)
        plot.plt.plot(R[s_idx_1,i,:], Z[s_idx_1,i,:], c=colors[ndx])
        plot.plt.plot(R[s_idx_2,i,:], Z[s_idx_2,i,:], c=colors[ndx])
        if i+1 == v_dom_sub.shape[0]:
            plot.plt.plot(R[s_idx_3,i,:], Z[s_idx_3,i,:], c=colors[ndx], label=conID)
        else:
            plot.plt.plot(R[s_idx_3,i,:], Z[s_idx_3,i,:], c=colors[ndx])
            
    wout.transForm_2D_sSec(s_dom[0], u_dom, v_dom, ['R','Z'])
    
    R = wout.invFourAmps['R']
    Z = wout.invFourAmps['Z']
        
    plot.plt.plot(R[:,0], Z[:,0], ls=':', c=colors[ndx], linewidth=1, zorder=0)

plot.plt.xlabel('R [m]')
plot.plt.ylabel('Z [m]')

box = plot.ax.get_position()
plot.ax.set_position([box.x0, box.y0, box.width * 1, box.height])
    
plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1))

plot.plt.grid()
plot.plt.tight_layout()
plot.plt.show()
#plot.plt.savefig('elogation.png')