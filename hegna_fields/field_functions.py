import numpy as np


def low_kappa_nu(M, params):
    P = params['rho'] / params['Delta']
    M2 = M**2
    chi = 0.5 * (params['kappa'] / (params['kappa']**2 - 1)) * (params['Delta'] / params['rho'])

    eta_prime_plus = np.arccos(chi + (1 - chi) * np.sqrt(1 - M2))

    npts = int(eta_prime_plus / params['Deta'])
    eta_dom = np.linspace(0, eta_prime_plus, npts, endpoint=False)

    if ((-4 * chi) / (1 - chi)**2) < M2:
        eta_prime_mins = np.arccos(chi - (1 - chi) * np.sqrt(1 - M2))
        npts = int((np.pi - eta_prime_mins) / params['Deta'])
        eta_dom_2well = np.linspace(eta_prime_mins + params['Deta'], np.pi, npts)

        index = np.array([0, eta_dom.shape[0]], dtype=int)
        eta_dom = np.append(eta_dom, eta_dom_2well)
        second_well = True

    else:
        index = np.array([0, eta_dom.shape[0]], dtype=int)
        second_well = False

    nu = P * (params['kappa']**2 - 1) * (np.sin(eta_dom)**2 - M2)
    nu = nu + params['kappa'] * np.cos(eta_dom)
    nu = nu - params['kappa']
    nu = nu + params['kappa'] * M2 * (1 - 0.5*chi)

    return nu, eta_dom, index, second_well


def mid_kappa_nu(M, params):
    M2 = M**2

    if params['kappa'] == 1:
        eta_prime = np.arccos(1 - 2*M2)
        npts = int(eta_prime / params['Deta'])
        eta_dom = np.linspace(0, eta_prime, npts, endpoint=False)

        nu = np.cos(eta_dom) - (1 - 2*M2)

    elif (params['kappa']**2 < 1):
        P = params['rho'] / params['Delta']
        P_inv = 1. / P
        chi = 0.5 * (params['kappa'] / (params['kappa']**2 - 1)) * P_inv

        eta_prime = np.arccos(chi + np.sqrt((1 - chi)**2 + 4*chi*M2))

        npts = int(eta_prime / params['Deta'])
        eta_dom = np.linspace(0, eta_prime, npts, endpoint=False)

        nu = P * (params['kappa']**2 - 1) * (np.sin(eta_dom)**2)
        nu = nu + params['kappa'] * np.cos(eta_dom)
        nu = nu - params['kappa'] * (1 - 2*M2)

    elif (params['kappa']**2 > 1):
        P = params['rho'] / params['Delta']
        P_inv = 1. / P
        chi = 0.5 * (params['kappa'] / (params['kappa']**2 - 1)) * P_inv

        arccos_arg = round(chi - np.sqrt((1 - chi)**2 + 4*chi*M2), 15)
        eta_prime = np.arccos(arccos_arg)
        npts = int(eta_prime / params['Deta'])
        eta_dom = np.linspace(0, eta_prime, npts, endpoint=False)

        nu = P * (params['kappa']**2 - 1) * (np.sin(eta_dom)**2)
        nu = nu + params['kappa'] * np.cos(eta_dom)
        nu = nu - params['kappa'] * (1 - 2*M2)

    return nu, eta_dom


def hgh_kappa_nu(M, params):
    P = params['rho'] / params['Delta']
    P_inv = 1. / P

    M2 = M**2
    chi = 0.5 * (params['kappa'] / (params['kappa']**2 - 1)) * P_inv

    eta_prime_mins = np.arccos(chi - (1 + chi) * M)

    if M < ((1 - chi) / (1 + chi)):
        eta_prime_plus = np.arccos(chi + (1 + chi) * M)
        npts = int((eta_prime_mins - eta_prime_plus) / params['Deta'])
        eta_dom = np.linspace(eta_prime_plus + params['Deta'], eta_prime_mins, npts-1, endpoint=False)

    else:
        npts = int(eta_prime_mins / params['Deta'])
        eta_dom = np.linspace(0, eta_prime_mins, npts, endpoint=False)

    nu = P * (params['kappa']**2 - 1) * (M2 - np.cos(eta_dom)**2)
    nu = nu + params['kappa'] * np.cos(eta_dom)
    nu = nu + params['kappa'] * M2
    nu = nu - 0.5 * params['kappa'] * chi * (1 - M2)

    return nu, eta_dom


def Kn(eta, params):
    Ro = params['Ro']
    rho = params['rho']
    Delta = params['Delta']
    N = params['N']
    kappa = params['kappa']

    return (N / Ro)**2 * (-kappa * Delta * np.cos(eta) + rho * (kappa - 1)**2 +\
        rho * (kappa**2 - 1) * np.cos(2 * eta)) / np.sqrt(1 + (kappa**2 - 1)*np.cos(eta)**2)


def Kg(eta, params):
    Ro = params['Ro']
    rho = params['rho']
    Delta = params['Delta']
    N = params['N']
    kappa = params['kappa']

    return (N / Ro)**2 * (-Delta * np.sin(eta) + rho * (kappa**2 - 1) *\
        np.sin(2 * eta)) / np.sqrt(1 + (kappa**2 - 1)*np.cos(eta)**2)


def Grad_Psi(eta, params):
    N = params['N']
    Ro = params['Ro']
    rho = params['rho']
    Delta = params['Delta']
    kappa = params['kappa']
    norm = params['normalization']

    DovR = Delta / rho
    rhoP2 = (N * rho / Ro)**2

    #return (Ro * rho * np.sqrt(1 + (kappa**2 - 1) * np.cos(eta)**2)) * ((4*np.pi*np.pi) / norm)
    scale = (4*np.pi*np.pi*Ro*rho) / (norm*np.sqrt(1 + (kappa**2 - 1) * np.cos(eta)**2))
    Ord_0 = 1 + (kappa**2 - 1) * np.cos(eta)**2

    Ord_2 = (DovR*np.sin(eta) - .5*(kappa**2 - 1)*np.sin(2*eta))**2
    Ord_2 = Ord_2 - (1 + (kappa**2 - 1)*np.cos(eta)**2) * (DovR*(2 - kappa)*np.cos(eta) - .5*(kappa**2 - 1)*np.cos(2*eta))

    gradPsi = scale * (Ord_0 + .5*rhoP2 * Ord_2)
    return gradPsi


def B(eta, params):
    # B_arr = [params['Ro'] * ((4*np.pi*np.pi) / params['normalization'])] * len(eta)
    # return np.array(B_arr)

    rho_prime = params['N'] * params['rho'] / params['Ro']
    Delta_prime = params['N'] * params['Delta'] / params['Ro']

    B_coeff = (params['normalization'] / (4*np.pi**2)) * params['Ro']
    B_mod = 1 - rho_prime * Delta_prime * params['kappa'] * np.cos(eta)
    B_mod = B_mod + 0.5 * (rho_prime**2) * (params['kappa']**2 - 1) * np.cos(2*eta)

    return B_coeff * B_mod


def jacob(eta, params):
    """
    return params['normalization'] / (4*np.pi*np.pi)
    """
    rho = params['rho']
    Delta = params['Delta']
    kappa = params['kappa']
    N = params['N']
    Ro = params['Ro']
    norm = params['normalization']

    rho_prime = N * rho / Ro
    Delta_prime = N * Delta / Ro

    # jacob_coeff = params['normalization'] / (4*np.pi*np.pi)
    # jacob_vals = 1 + rho_prime * Delta_prime * np.cos(eta)
    # jacob_vals = jacob_vals - 0.5 * rho_prime**2 * (kappa**2 - 1) * np.cos(2*eta)
    # return jacob_coeff * jacob_vals
    scale = norm / (4*np.pi*np.pi)
    Ord_0 = 1
    Ord_2_1 = (2 - kappa) * np.cos(eta)
    Ord_2_2 = .5 * (kappa**2 - 1) * np.cos(2*eta)

    sqrtG = scale * (Ord_0 + rho_prime*Delta_prime*Ord_2_1 - rho_prime*rho_prime*Ord_2_2)
    return sqrtG


def D(eta, params):
    Ro = params['Ro']
    rho = params['rho']
    N = params['N']
    iota = params['iota']
    kappa = params['kappa']
    norm = params['normalization']

    eta_low = eta[eta < (-0.5 * np.pi)]
    eta_mid = eta[(eta >= (-0.5 * np.pi)) & (eta <= (0.5 * np.pi))]
    eta_hgh = eta[eta > (0.5 * np.pi)]

    D_coeff = -(N / (Ro * rho * rho * (N - iota))) * (norm / (4*np.pi*np.pi))

    D_left_low = ((kappa - 1) / kappa)**2 * (eta_low - np.arctan(np.tan(eta_low) / kappa) + np.pi)
    D_left_mid = ((kappa - 1) / kappa)**2 * (eta_mid - np.arctan(np.tan(eta_mid) / kappa))
    D_left_hgh = ((kappa - 1) / kappa)**2 * (eta_hgh - np.arctan(np.tan(eta_hgh) / kappa) - np.pi)
    D_left = np.append(np.append(D_left_low, D_left_mid), D_left_hgh)


    D_right = ((kappa**2 - 1) / kappa) * (np.sin(2*eta) / ((kappa**2 + 1) + (kappa**2 - 1) * np.cos(2*eta)))

    return D_coeff * (D_left + D_right)


def iota_prime(params):
    return (params['normalization'] / (4*np.pi*np.pi)) * (params['N'] / (params['Ro'] * params['rho']**2)) * ((params['kappa'] - 1) / params['kappa'])**2


def phase_space_weight(eta, params):
    B_well = B(eta, params)
    dB_well_dEta = np.abs(np.gradient(B_well, eta))

    p_wght = np.abs(dB_well_dEta[1::]) / (np.sqrt(1 - B_well[0] / B_well[1::]) * B_well[1::]**2)
    return p_wght


if __name__ == '__main__':
    import os
    import matplotlib as mpl
    import matplotlib.pyplot as plt
    from scipy.interpolate import interp1d

    params = {'Deta': 1e-5,
              'Ro': 1.5,
              'Delta': 0.1,
              'N': 10,
              'iota': 1.05,
              'minor': 0.05,
              'normalization': 4*np.pi**2}

    M = 0.93
    M2 = M**2
    params['kappa'] = 1
    params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['minor']

    P = params['rho'] / params['Delta']
    P2 = P**2
    P_inv = 1. / P

    rho_prime = params['N'] * params['rho'] / params['Ro']
    Delta_prime = params['N'] * params['Delta'] / params['Ro']

    kappa_low = 0.25 * P_inv * (np.sqrt(1 + 16*P2) - 1)
    kappa_hgh = 0.25 * P_inv * (np.sqrt(1 + 16*P2) + 1)

    if params['kappa'] < kappa_low:
        nu, eta_dom, index, second_well = low_kappa_nu(M, params)
        nu_sqrt = np.sqrt(nu)

    elif params['kappa'] >= kappa_low and params['kappa'] <= kappa_hgh:
        nu, eta_dom = mid_kappa_nu(M, params)
        nu_sqrt = np.sqrt(nu)
        second_well = False

    else:
        nu, eta_dom = hgh_kappa_nu(M, params)
        nu_sqrt = np.sqrt(nu)
        second_well = False

    Vpar = np.sqrt(rho_prime * Delta_prime * nu)
    eta_full = np.linspace(0, np.pi, 250)

    B_full = B(eta_full, params)
    gradPsi = Grad_Psi(eta_full, params)
    kn = Kn(eta_full, params)
    kg = Kg(eta_full, params)
    D_full = D(eta_full, params)

    KD_full = (kn / gradPsi) + D_full * kg * (gradPsi / B_full)

    dB_full_dEta = np.abs(np.gradient(B_full, eta_full))
    p_wght = np.abs(dB_full_dEta[1::]) / (np.sqrt(1 - B_full[0] / B_full[1::]) * B_full[1::]**2)

    B_well = B(eta_dom, params)
    gradPsi = Grad_Psi(eta_dom, params)
    kn = Kn(eta_dom, params)
    kg = Kg(eta_dom, params)
    D_well = D(eta_dom, params)

    KD = (kn / gradPsi) + D_well * kg * (gradPsi / B_well)
    bounce = np.arcsin(np.sqrt(B_well[0] / (B_well[-1]*M2 + B_well[0]*(1 - M2))))

    eta_fit = np.r_[-np.flip(eta_full[1::]), eta_full[1::]]
    p_wght_full = np.r_[np.flip(p_wght), p_wght]
    p_wght_model = interp1d(eta_fit, p_wght_full)

    eta_full = np.r_[-np.flip(eta_full[1::]), eta_full]
    eta_dom = np.r_[-np.flip(eta_dom[1::]), eta_dom]

    B_full = np.r_[np.flip(B_full[1::]), B_full]
    KD_full = np.r_[np.flip(KD_full[1::]), KD_full]

    B_well = np.r_[np.flip(B_well[1::]), B_well]
    KD = np.r_[np.flip(KD[1::]), KD]
    Vpar = np.r_[np.flip(Vpar[1::]), Vpar]

    p_wght_full = p_wght_model(eta_full)
    p_wght_well = p_wght_model(eta_dom)

    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 18}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2

    if False:
        fig, axs = plt.subplots(2, 1, tight_layout=True, sharex=True, figsize=(8, 8))
        ax1 = axs[0].twinx()
        ax2 = axs[1].twinx()

        ax1.spines['right'].set_color('tab:red')
        ax1.tick_params(axis='y', colors='tab:red')

        ax2.spines['right'].set_color('tab:blue')
        ax2.tick_params(axis='y', colors='tab:blue')

        eta_norm = eta_dom / np.pi

        axs[0].plot(eta_full/np.pi, B_full, c='k', ls='--')
        axs[0].plot(eta_norm, B_well, c='k')
        axs[0].scatter([eta_norm[0], eta_norm[-1]], [B_well[0], B_well[-1]], c='k', s=150, marker='o')

        ax1.plot(eta_full/np.pi, KD_full, c='tab:red', ls='--')
        ax1.plot(eta_norm, KD, c='tab:red')
        ax1.plot(eta_full/np.pi, np.zeros(eta_full.shape), ls='dotted', c='tab:red', linewidth=2)
        ax1.scatter([eta_norm[0], eta_norm[-1]], [KD[0], KD[-1]], s=150, marker='o', c='tab:red')

        axs[1].plot(eta_full/np.pi, p_wght_full, c='k', ls='--')
        axs[1].plot(eta_norm, p_wght_well, c='k')
        axs[1].scatter([eta_norm[0], eta_norm[-1]], [p_wght_well[0], p_wght_well[-1]], c='k', s=150)

        ax2.plot(eta_norm, Vpar, c='tab:blue')

        axs[1].set_xlim(-1, 1)
        axs[1].set_ylim(0, 1.1*np.max(p_wght_full))
        ax2.set_ylim(0, 1)

        axs[0].set_ylabel(r'$B$')
        ax1.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D\kappa_g \frac{|\nabla \psi|}{B}$', c='tab:red')

        axs[1].set_xlabel(r'$\eta / \pi$')
        axs[1].set_ylabel(r'$p(\phi_b)$')
        ax2.set_ylabel(r'$v_{\parallel} / v$', c='tab:blue')

        # axs[0].set_title(r'$M = $'+'{0:0.2f} '.format(M))
        axs[0].set_title(r'$M = $'+'{0:0.2f} '.format(M)+r'$\rightarrow \ \phi_b = $'+'{0:0.2f}'.format(bounce/np.pi)+r'$\, \pi$')

        axs[0].grid()
        axs[1].grid()

        # plt.show()
        saveName = os.path.join('/home', 'michael', 'Desktop', '3D_chrisModel_kScan', 'scan_5', 'k1.00', 'phaseSpace_k1p0_M0p93.png')
        plt.savefig(saveName)

    if True:
        fig, ax0 = plt.subplots(1, 1, tight_layout=True)
        ax1 = ax0.twinx()

        ax1.spines['right'].set_color('tab:red')
        ax1.tick_params(axis='y', colors='tab:red')

        eta_norm = eta_dom / np.pi

        ax0.plot(eta_full/np.pi, B_full, c='k', ls='--')
        ax0.plot(eta_norm, B_well, c='k')
        ax0.scatter([eta_norm[0], eta_norm[-1]], [B_well[0], B_well[-1]], c='k', s=150, marker='o')

        ax1.plot(eta_full/np.pi, KD_full, c='tab:red', ls='--')
        ax1.plot(eta_norm, KD, c='tab:red')
        ax1.plot(eta_full/np.pi, np.zeros(eta_full.shape), ls='dotted', c='tab:red', linewidth=2)
        ax1.scatter([eta_norm[0], eta_norm[-1]], [KD[0], KD[-1]], s=150, marker='o', c='tab:red')

        ax0.set_xlabel(r'$\eta\pi$')
        ax0.set_ylabel(r'$B$')
        ax1.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D\kappa_g \frac{|\nabla \psi|}{B}$', c='tab:red')

        ax0.set_title(r'$M = $'+'{0:0.2f} '.format(M)+r'$\rightarrow \ \phi_b = $'+'{0:0.2f}'.format(bounce/np.pi)+r'$\, \pi$')
        ax0.grid()

        ax0.set_xlim(-1, 1)
        kD_max = 1.1 * np.max(np.abs(KD_full))
        ax1.set_ylim(-kD_max, kD_max)

        # plt.show()
        saveName = os.path.join('/home', 'michael', 'Desktop', '3D_chrisModel_kScan', 'scan_5', 'k1.00', 'curveDrive_k1p0_M0p93.png')
        plt.savefig(saveName)
