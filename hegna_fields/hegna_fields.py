#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 16 09:22:08 2021

@author: michael
"""

import numpy as np
from scipy.special import ellipe
from matplotlib.colors import LogNorm

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import vtkTools.vtk_grids as vtkG

class hegna_field():

    def __init__(self, N, Ro, rho, delta, kappa):
        self.params = {'N' : N,
                       'Ro' : Ro,
                       'rho' : rho,
                       'delta' : delta,
                       'kappa' : kappa}

    def calculate_field(self, features, zeta_dom, theta_dom):
        ### Define Constants ###
        N = self.params['N']
        Ro = self.params['Ro']
        rho = self.params['rho']
        delta = self.params['delta']
        kappa = self.params['kappa']

        ### Construct Grid ###
        self.zeta_dom = zeta_dom; self.theta_dom = theta_dom
        theta_grid, zeta_grid = np.meshgrid(theta_dom, zeta_dom)
        eta_grid = 0.5*N * zeta_grid - theta_grid

        ### Calculate Additional Constants ###
        if any(['Bmod' == feat_chk or 'jacobian' == feat_chk or 'kappa_n' == feat_chk for feat_chk in features]):
            eta2_grid = 2 * eta_grid
        if any(['Bmod' == feat_chk or 'jacobian' == feat_chk for feat_chk in features]):
            eps_h = ( N * N * delta * rho * (2-kappa) ) / ( Ro**2 + (N * delta)**2 + 0.5 * (N * rho * (kappa-1))**2 )
            eps_2h = ( -0.5 * (N * rho)**2 * (kappa**2 - 1) ) / ( Ro**2 + (N * delta)**2 + 0.5 * (N * rho * (kappa-1))**2 )

        self.features = {}
        ### Calculate Features ###
        if any(['R' == feat_chk for feat_chk in features]):
            self.features['R_grid'] = Ro + delta * np.cos(N*zeta_grid) + rho * ( np.cos(theta_grid) + (kappa-1) * \
                                              np.sin(eta_grid) * np.sin(0.5*N*zeta_grid) )

        if any(['Z' == feat_chk for feat_chk in features]):
            self.features['Z_grid'] = delta * np.sin(N*zeta_grid) + rho * (np.sin(theta_grid) - (kappa-1) * \
                                         np.sin(eta_grid) * np.cos(0.5*N*zeta_grid) )

        if any(['phi' == feat_chk for feat_chk in features]):
            self.features['phi_grid'] = -zeta_grid

        if any(['jacobian' == feat_chk for feat_chk in features]):
            self.features['jacobian'] = 1 + eps_h * np.cos(eta_grid) + eps_2h * np.cos(eta2_grid)

        if any(['Bmod' == feat_chk for feat_chk in features]):
            self.features['Bmod'] = np.sqrt( Ro**2 + (N * delta)**2 + (N * rho * (kappa-1))**2 - 2 * N * N * delta * rho * \
                         (kappa-1) * np.cos(eta_grid) ) / ( 1 + eps_h * np.cos(eta_grid) + eps_2h * np.cos(eta2_grid) )

        if any(['Grad_Psi' == feat_chk for feat_chk in features]):
            Ld = delta * np.sin(eta_grid) - 0.5 * rho * (kappa**2 - 1) * np.sin(2*eta_grid)

            self.features['Grad_Psi'] = np.sqrt((Ro*rho)**2 * (1 + (kappa**2 - 1) * np.cos(eta_grid)**2) + (N*rho*Ld)**2) \
                        / (1 + eps_h * np.cos(eta_grid) + eps_2h * np.cos(eta2_grid))

        if any(['kappa_n' == feat_chk for feat_chk in features]):
            Ld = delta * np.sin(eta_grid) - 0.5 * rho * (kappa**2 - 1) * np.sin(2*eta_grid)
            Ln = np.sqrt( Ro**2 * ( 1 + (kappa**2 - 1) * (np.cos(eta_grid)**2) ) + (N * Ld)**2 )
            Lb = np.sqrt( Ro**2 + (N * delta)**2 + (N * rho * (kappa-1))**2 - 2 * N * N * delta * rho * (kappa-1) * \
                         np.cos(eta_grid) )

            self.features['kappa_n'] = ( N * N * Ro * ( -delta * kappa * np.cos(eta_grid) + rho * (kappa**2 - 1) * \
                         np.cos(eta2_grid) + rho * (kappa-1)**2 ) ) / (Ln * (Lb**2))

        if any(['kappa_g' == feat_chk for feat_chk in features]):
            Ln = np.sqrt( Ro**2 * ( 1 + (kappa**2 - 1) * (np.cos(eta_grid)**2) ) + (N * Ld)**2 )
            Lb = np.sqrt( Ro**2 + (N * delta)**2 + (N * rho * (kappa-1))**2 - 2 * N * N * delta * rho * (kappa-1) * \
                         np.cos(eta_grid) )
            Gg = kappa * delta * np.cos(eta_grid) - 0.5 * N * rho * (kappa - 1)**2 - 0.5 * N * rho * (kappa**2 - 1) * np.cos(2*eta_grid)

            self.features['kappa_g'] = (N**2 / (Ln*Lb)) * (-delta * np.sin(eta_grid) + rho * (kappa**2 - 1) * np.sin(2*eta_grid)
                        - (N / Lb)**2 * (delta * rho * (kappa - 1)) * np.sin(eta_grid) * Gg)

        if any(['torsion' == feat_chk for feat_chk in features]):
            """
            Ln = np.sqrt( Ro**2 * ( 1 + (kappa**2 - 1) * (np.cos(eta_grid)**2) ) + (N * Ld)**2 )
            Lb = np.sqrt( Ro**2 + (N * delta)**2 + (N * rho * (kappa-1))**2 - 2 * N * N * delta * rho * (kappa-1) * \
                         np.cos(eta_grid) )
            Cq = -0.5 * (kappa**2 - 1)**2 * np.cos(2*eta_grid)**2 - 0.5 * (kappa - 1)**2 * (kappa**2 - 1) * np.cos(2*eta_grid)
            Cr = (0.5 * (kappa - 1)**2 + 0.75 * kappa * (kappa**2 - 1)) * np.cos(eta_grid) + 0.25 * (kappa + 2) * (kappa**2 - 1) * np.cos(3*eta_grid)

            self.features['torsion'] = ((N * Ro) / (Ln*Lb)**2) * (0.5 * Ro**2 * (kappa - 1)**2 + 0.5 * Ro**2 * (kappa**2 - 1) * np.cos(2*eta_grid) \
                        + (N * delta)**2 * (np.sin(eta_grid)**2 - kappa) + (N*rho)**2 * Cq + N*N*delta*rho*Cr)
            """
            self.features['torsion'] = (N / (2 * Ro)) * (((kappa - 1)**2 + (kappa**2 - 1) * np.cos(2*eta_grid)) / (1 + (kappa**2 - 1) * np.cos(eta_grid)**2))

    def calculate_F_kappa(self, zeta_dom, theta_dom):
        ### Calculate F_kappa Numerically ###
        self.calculate_field(['Bmod','jacobian','kappa_n'], zeta_dom, theta_dom)

        B = self.features['Bmod']
        jacob = self.features['jacobian']
        k_n = self.features['kappa_n']

        avg_B = np.mean(B * jacob)
        B_shft = (B - avg_B) / avg_B
#        B_shft = B_shft * np.heaviside(-B_shft, 1)

#        avg_Kn = np.mean(k_n * jacob)
#        Kn_shft = (k_n - avg_Kn) / avg_Kn

        F_kappa = np.mean(B_shft * k_n * jacob)
#        F_kappa = np.mean(B_shft * Kn_shft * jacob)
        return F_kappa

    def make_surfaceVTK(self, path, name, feature, zeta_dom, theta_dom):
        ### Calculate F_kappa Numerically ###
        self.calculate_field(['R','Z','phi',feature], zeta_dom, theta_dom)

        R_grid = self.features['R_grid']
        Z_grid = self.features['Z_grid']
        phi_grid = self.features['phi_grid']

        X_grid = R_grid * np.cos(-phi_grid)
        Y_grid = R_grid * np.sin(-phi_grid)

        featGrid = self.features[feature]

        cartGrid = np.stack((X_grid, Y_grid, Z_grid), axis=2)
        vtkG.scalar_mesh(path, name, cartGrid, featGrid)

    def parametric_plot(self, feature, plot):
        ### Define Plotting Feature ###
        plt, fig, ax = plot.plt, plot.fig, plot.ax
        try:
            feat = self.features[feature]
        except KeyError:
            raise KeyError("The " + feature + " feature has not been generated.  Have you run calculate_field for this value yet?")

        ### Define Constants ###
        Ro = self.params['Ro']
        rho = self.params['rho']
        delta = self.params['delta']
        kappa = self.params['kappa']

        ### Determine Scaling ###
        feat_sym = ['Z_grid','kappa_n']
        feat_abs = ['R_grid','Bmod','jacobian']

        feat_min = np.min(feat); feat_max = np.max(feat)
        if any([feature == feat_chk for feat_chk in feat_sym]):
            color_map = 'seismic'
            if feat_max >= abs(feat_min):
                feat_min = -feat_max
            else:
                feat_max = abs(feat_min)

        if any([feature == feat_chk for feat_chk in feat_abs]):
            color_map = 'jet'

        ### Determine Labels ###
        if feature == 'R_grid':
            cLab = r'$R$'
        elif feature == 'Z_grid':
            cLab = r'$Z$'
        elif feature == 'Bmod':
            cLab = r'$\|B\|$'
        elif feature == 'jacobian':
            cLab = r'$\sqrt{g}$'
        elif feature == 'kappa_n':
            cLab = r'$\kappa_n$'

        ### Make Parametric Plot ###
        s_map = ax.pcolormesh(self.zeta_dom, self.theta_dom, feat.T, vmin=feat_min, vmax=feat_max, cmap=color_map, shading='auto')
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(cLab)

        plt.xlabel(r'$\zeta$')
        plt.ylabel(r'$\theta$')

#        title = r'$R_o=$'+'{}'.format(Ro)+r', $\rho_o=$'+'{}'.format(rho)+r', $\Delta=$'+'{}'.format(delta)+r', $\kappa=$'+'{}'.format(kappa)
        title = r'$\rho_o=$'+'{}'.format(rho)+r', $\Delta=$'+'{}'.format(delta)+r', $\kappa=$'+'{}'.format(kappa)
        plt.title(title)
        plt.tight_layout()

    def plot_toriodal_crossSec(self, zeta, plt, legend=False):
        theta_dom = np.linspace(0, 2*np.pi, 101)
        self.calculate_field(['R','Z'], zeta, theta_dom)

        R_grid = self.features['R_grid']
        Z_grid = self.features['Z_grid']

        grid = np.stack((R_grid[0], Z_grid[0]), axis=1)

        if legend:
            plt.plot(grid[:,0], grid[:,1], c='k', label='3D Model')
        else:
            plt.plot(grid[:,0], grid[:,1], c='k')

if __name__ == '__main__':
    N = 4
    Ro = 1.22
    delta = 0.17
    kappa = 1
    a_minor = .1
    rho = np.sqrt(2 / (kappa**2 + 1)) * a_minor

    theta_pnts = 251
    zeta_pnts = 8 * theta_pnts

    theta_dom = np.linspace(-np.pi, np.pi, theta_pnts)
    zeta_dom = np.linspace(-np.pi, np.pi, zeta_pnts)

    if True:
        # Compare Mapping with HSX Equilibrium #
        import vmecTools.wout_files.wout_read as wr
        from scipy.optimize import curve_fit

        u_dom = np.linspace(0, 2*np.pi, 251)
        v_dom = np.array([0, 0.125, 0.25]) * np.pi
        #v_dom = np.linspace(0, 2*np.pi, 251)

        wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs',
                                 'main_coil_0', 'set_1', 'job_0')
        wout = wr.readWout(wout_path)
        wout.transForm_2D_sSec(0.5, u_dom, v_dom, ['R', 'Z'])

        R_QHS = wout.invFourAmps['R']
        Z_QHS = wout.invFourAmps['Z']

        # Construct Plot #
        plot = pd.plot_define(lineWidth=2, eqAsp=True)

        heg = hegna_field(N, Ro, rho, delta, kappa)
        for vdx, v in enumerate(v_dom):
            if vdx+1 == len(v_dom):
                heg.plot_toriodal_crossSec(v, plot.plt, legend=True)
                plot.plt.plot(R_QHS[vdx,:], Z_QHS[vdx,:], c='tab:red', label='HSX')
            else:
                heg.plot_toriodal_crossSec(v, plot.plt)
                plot.plt.plot(R_QHS[vdx,:], Z_QHS[vdx,:], c='tab:red')

            R_ma = Ro + delta * np.cos(N * v)
            Z_ma = delta * np.sin(N * v)
            #plot.plt.scatter([R_ma], [Z_ma], c='k', marker='*', s=150)

        plot.plt.title(r'$\kappa = $'+'{0:0.1f}'.format(kappa))

        plot.plt.xlabel('R [m]')
        plot.plt.ylabel('Z [m]')
        #plot.plt.title(r'$\zeta = $ '+'{0:0.3f}'.format(v_dom[0] / np.pi) + r'$\pi$')

        plot.plt.grid()
        plot.plt.legend(loc='lower center')
        plot.plt.tight_layout()
        plot.plt.show()

        # saveName = os.path.join('/home', 'michael', 'onedrive', 'Figures',\
        #                        '3D_Drift_Model', 'HSX_Model_Comp_3.png')
        saveName = os.path.join('/home', 'michael', 'Desktop', '3D_model_kScan', 'HSX_comp_k1p0.png')
        plot.plt.savefig(saveName)

    if False:
        # Fit Magnetic Axis #
        import vmecTools.wout_files.wout_read as wr
        from scipy.optimize import curve_fit

        u_dom = np.linspace(0, 2*np.pi, 251)
        v_dom = np.linspace(0, 2*np.pi, 251)

        wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs',
                                 'main_coil_0', 'set_1', 'job_0')
        wout = wr.readWout(wout_path)
        wout.transForm_2D_sSec(0, u_dom, v_dom, ['R', 'Z'])

        R_QHS = wout.invFourAmps['R']
        Z_QHS = wout.invFourAmps['Z']

        def R_func(v, delta, Ro):
            return Ro + delta * np.cos(N * v)

        def Z_func(v, delta):
            return delta * np.sin(N * v)

        R_popt, R_pcov = curve_fit(R_func, v_dom, R_QHS[:, 0], p0=np.array([delta, Ro]))
        Z_popt, Z_pcov = curve_fit(Z_func, v_dom, Z_QHS[:, 0], p0=delta)

        delta_rng = np.linspace(Z_popt[0], R_popt[0], 101)
        error_rng = np.empty(delta_rng.shape)
        for dx, delta in enumerate(delta_rng):
            R_model = R_func(v_dom, delta, R_popt[1])
            Z_model = Z_func(v_dom, delta)

            R_err = np.abs(R_model - R_QHS) / R_QHS
            Z_err = np.abs(Z_model - Z_QHS) / (Z_QHS + R_popt[1])
            error_rng[dx] = np.mean(np.hypot(R_err, Z_err))

        idx = np.argmin(error_rng)
        delta = delta_rng[idx]

        R_model = R_func(v_dom, delta, R_popt[1])
        Z_model = Z_func(v_dom, delta)

        # Construct Plot #
        plot = pd.plot_define(lineWidth=2, eqAsp=True)

        plot.plt.plot(v_dom / np.pi, R_QHS[:,0], c='k', label='R-QHS')
        plot.plt.plot(v_dom / np.pi, Z_QHS[:,0], c='tab:red', label='Z-QHS')

        plot.plt.plot(v_dom / np.pi, R_model, c='k', ls='--', label='R-Model')
        plot.plt.plot(v_dom / np.pi, Z_model, c='tab:red', ls='--', label='Z-Model')

        plot.plt.xlabel(r'$\zeta \, [\pi]$')
        plot.plt.title(r'$(R_o,\, \Delta) = $' + '({0:0.4f}, {1:0.4f})'.format(R_popt[1], delta))

        plot.plt.grid()
        plot.plt.legend()
        plot.plt.tight_layout()
        plot.plt.show()

    if False:
        ### Make VTK files ###
        saveDir = os.path.join('/home','michael','Desktop')
        heg = hegna_field(N, Ro, rho, delta, kappa)
        heg.make_surfaceVTK(saveDir, 'Bmod_examp.vtk', 'Bmod', zeta_dom, theta_dom)
        #heg.make_surfaceVTK(saveDir, 'Kn_fake.vtk', 'kappa_n', zeta_dom, theta_dom)

    if False:
        ### Make Parametric Plots ###
        rho = 0.1
        delta = 0.15
        kappa = 1.0

        heg = hegna_field(N, Ro, rho, delta, kappa)
        heg.calculate_field(['Bmod','kappa_n'], zeta_dom, theta_dom)

        if True:
            ### Parametric Plot of |B| ###
            plot = pd.plot_define(lineWidth=2)
            heg.parametric_plot('Bmod', plot)
            plot.plt.plot([0,2*np.pi], [0,2*np.pi], c='k', ls='--')
            plot.plt.show()
#            plot.plt.savefig('Bmod_delta_0p15_kappa_1p1.png')

        if False:
            ### Parametric Plot of \sqrt{g} ###
            plot = pd.plot_define()
            heg.parametric_plot('jacobian', plot)
            plot.plt.show()

        if True:
            ### Parametric Plot of \kappa_n ###
            plot = pd.plot_define(lineWidth=2)
            heg.parametric_plot('kappa_n', plot)
            plot.plt.plot([0, 2*np.pi], [0, 2*np.pi], c='k', ls='--')
            plot.plt.show()
#            plot.plt.savefig('Kn_delta_0p15_kappa_1p1.png')

        npts = 100
        zeta_pnts = np.linspace(0, 2*np.pi, npts)
        theta_pnts = np.linspace(0, 2*np.pi, npts)

        Bmod = np.empty(npts)
        Kn = np.empty(npts)
        for i in range(npts):
            heg.calculate_field(['Bmod','kappa_n'], [zeta_pnts[i]], [theta_pnts[i]])
            Bmod[i] = heg.features['Bmod']
            Kn[i] = heg.features['kappa_n']

        plot = pd.plot_define(lineWidth=2)
        plt, ax1 = plot.plt, plot.ax
        ax2 = ax1.twinx()

        ax1.plot(zeta_pnts, Bmod, c='k')
        ax2.plot(zeta_pnts, Kn, c='tab:red')

        ax1.set_xlabel(r'$\zeta$')
        ax1.set_ylabel(r'$|B|$')
        ax2.set_ylabel(r'$\kappa_n$', c='tab:red')
        plt.title(r'$\Delta = $'+'{0:0.03f}'.format(delta))

        ax2.yaxis.label.set_color('tab:red')
        ax2.tick_params(axis='y', labelcolor='tab:red')
        ax2.spines['right'].set_color('tab:red')

        plt.grid()
        plt.tight_layout()
        plt.show()

    if False:
        heg = hegna_field(N, Ro, rho, delta, kappa)
        heg.calculate_field(['jacobian', 'Bmod', 'Grad_Psi', 'torsion'], zeta_dom, theta_dom)

        jacob = heg.features['jacobian']
        Bmod = heg.features['Bmod']
        Grad_Psi = heg.features['Grad_Psi']
        torsion = heg.features['torsion']

        shear = (Bmod**2 / Grad_Psi **2) * torsion * jacob
        avg_shear = np.trapz(np.trapz(shear, theta_dom, axis=1), zeta_dom)
        # print(- avg_shear / (2 * np.pi**2))
        # print((N / (Ro*rho*rho)) * ((kappa - 1) / kappa)**2)

        plot = pd.plot_define(eqAsp=True, lineWidth=2)
        plt, fig, ax = plot.plt, plot.fig, plot.ax

        s_map = ax.pcolormesh(zeta_dom / np.pi, theta_dom / np.pi, torsion.T, cmap='jet', shading='auto')
        cbar = fig.colorbar(s_map, ax=ax)
        cbar.ax.set_ylabel(r'$\sqrt{g} \frac{B^2}{|\nabla \psi|^2} \tau_n$')

        plt.xlabel(r'$\zeta (\pi)$')
        plt.ylabel(r'$\theta (\pi)$')

        plt.tight_layout()
        plt.show()
