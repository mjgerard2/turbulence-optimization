import numpy as np
from scipy.interpolate import interp1d


def get_extrema(Yarr, Xarr, idx):
    X1, X2, X3 = Xarr[idx-1], Xarr[idx], Xarr[idx+1]
    Y1, Y2, Y3 = Yarr[idx-1], Yarr[idx], Yarr[idx+1]

    Xmat = np.array([[X1**2, X1, 1],
                     [X2**2, X2, 1],
                     [X3**2, X3, 1]])
    Xmat_inv = np.linalg.inv(Xmat)

    Yvec = np.array([Y1, Y2, Y3])
    coef = np.dot(Xmat_inv, Yvec)

    x_ext = -0.5 * (coef[1] / coef[0])
    y_ext = coef[2] - 0.25 * coef[1] * coef[1] / coef[0]

    return x_ext, y_ext


def V_parallel(gist, M, nfp, v_thresh=1e-6):
    # Read in Field Data #
    modB = gist.data['modB']
    eta_dom = (nfp * gist.q0 - 1) * gist.pol_dom
    eta_spc = 2 * ((nfp * gist.q0 - 1) * gist.hlf_stp)

    modB_interp = interp1d(eta_dom, modB, kind='quadratic')

    # Partition grid with mod-B extrema #
    eta_sub = eta_dom[(eta_dom >= -1.25*np.pi) & (eta_dom <= 1.25*np.pi)]
    modB_sub = modB[(eta_dom >= -1.25*np.pi) & (eta_dom <= 1.25*np.pi)]

    eta_sub_lft = eta_dom[(eta_dom >= -1.25*np.pi) & (eta_dom <= 0)]
    modB_sub_lft = modB[(eta_dom >= -1.25*np.pi) & (eta_dom <= 0)]

    eta_sub_rgt = eta_dom[(eta_dom > 0) & (eta_dom <= 1.25*np.pi)]
    modB_sub_rgt = modB[(eta_dom > 0) & (eta_dom <= 1.25*np.pi)]

    modB_max_idx_lft = np.argmax(modB_sub_lft)
    modB_min_idx = np.argmin(modB_sub)
    modB_max_idx_rgt = np.argmax(modB_sub_rgt)

    eta_max_lft, modB_max_lft = get_extrema(modB_sub_lft, eta_sub_lft, modB_max_idx_lft)
    eta_min, modB_min = get_extrema(modB_sub, eta_sub, modB_min_idx)
    eta_max_rgt, modB_max_rgt = get_extrema(modB_sub_rgt, eta_sub_rgt, modB_max_idx_rgt)
    modB_max = max([modB_max_lft, modB_max_rgt])

    npts = int((eta_max_rgt - eta_max_lft) / eta_spc)
    eta_dom = np.linspace(eta_max_lft, eta_max_rgt, npts)
    # eta_dom_ext = np.insert(eta_dom, [0, npts], [eta_max_lft-eta_spc, eta_max_rgt+eta_spc])
    modB = modB_interp(eta_dom)

    # Calculate (v_{\parallel} / v)^2 #
    v_sqrd_ratio = 1 - modB / (modB_max * M**2 + modB_min * (1 - M**2))
    v_sqrd_interp = interp1d(eta_dom, v_sqrd_ratio, kind='quadratic')
    Vpar_exists = True

    if M == 1:
        eta_beg = np.array([eta_dom[0]])
        eta_end = np.array([eta_dom[-1]])

    else:
        # Find indices where V_par goes from pos -> neg #
        neg_idx = np.where(v_sqrd_ratio < 0)[0]
        neg_bool = ((neg_idx - np.roll(neg_idx, 1)) != 1)
        negDX = np.where(neg_bool)[0]
        if len(negDX) > 0:
            negIDX = np.empty(negDX.shape[0]-1, dtype=int)
            for i, idx in enumerate(negDX[1::]):
                if neg_idx[idx] - neg_idx[idx-1] > 3:
                    negIDX[i] = neg_idx[idx] - 1
                else:
                    Vpar_exists = False

        # Approximate (pos -> neg) zero crossing
        if len(negDX) > 0 and Vpar_exists:
            eta_end = np.empty(negDX.shape[0]-1)
            for i, idx in enumerate(negIDX):
                eta = eta_dom[idx]
                v_sqrd = v_sqrd_interp(eta)
                # print(v_sqrd)
                while v_sqrd > v_thresh and v_sqrd > 0:
                    eta_prev = eta

                    eta_lft = eta - eta_spc
                    eta_rgt = eta + eta_spc
                    if eta_rgt > eta_dom[-1]:
                        eta_rgt = eta_dom[-1]
                    if eta_lft < eta_dom[0]:
                        eta_lft = eta_dom[0]

                    numer = eta_spc * v_sqrd_interp(eta)
                    denom = v_sqrd_interp(eta_rgt) - v_sqrd_interp(eta_lft)
                    eta = eta - (numer / denom)
                    v_sqrd = v_sqrd_interp(eta)
                    # print('pos -> neg: ({}, {})'.format(eta, v_sqrd))
                if v_sqrd < 0:
                    eta = eta_prev
                eta_end[i] = eta
        else:
            eta_end = np.array([eta_dom[-1]])

        # Find indices where V_par goes from neg -> pos #
        pos_idx = np.where(v_sqrd_ratio > 0)[0]
        pos_bool = ((pos_idx - np.roll(pos_idx, 1)) != 1)
        posDX = np.where(pos_bool)[0]
        # print(posDX)
        if len(posDX) > 0 and len(negDX) > 0 and Vpar_exists:
            posIDX = np.empty(posDX.shape[0], dtype=int)
            for i, idx in enumerate(posDX):
                posIDX[i] = pos_idx[idx]
        elif len(negDX) > 0 and Vpar_exists:
            Vpar_exists = False

        # Approximate (neg -> pos) zero crossing
        if len(posDX) > 0 and len(negDX) > 0 and Vpar_exists:
            eta_beg = np.empty(posIDX.shape)
            for i, idx in enumerate(posIDX):
                eta = eta_dom[idx]
                v_sqrd = v_sqrd_interp(eta)
                while v_sqrd > v_thresh and v_sqrd > 0:
                    eta_prev = eta

                    eta_lft = eta - eta_spc
                    eta_rgt = eta + eta_spc
                    if eta_rgt > eta_dom[-1]:
                        eta_rgt = eta_dom[-1]
                    if eta_lft < eta_dom[0]:
                        eta_lft = eta_dom[0]

                    numer = eta_spc * v_sqrd
                    denom = v_sqrd_interp(eta_rgt) - v_sqrd_interp(eta_lft)
                    eta = eta - (numer / denom)
                    v_sqrd = v_sqrd_interp(eta)
                    # print('neg -> pos: {}'.format(v_sqrd))
                if v_sqrd < 0:
                    eta = eta_prev
                eta_beg[i] = eta
        else:
            eta_beg = np.array([eta_dom[0]])

    # Define Bounds #
    eta_vals = []
    Vpar_vals = []
    if Vpar_exists:
        for i, etaBeg in enumerate(eta_beg):
            etaEnd = eta_end[i]
            npts = int(1. + (etaEnd - etaBeg) / eta_spc)
            eta = np.linspace(etaBeg, etaEnd, npts)
            vpar = np.sqrt(v_sqrd_interp(eta))

            eta_vals.append(eta)
            Vpar_vals.append(vpar)
    else:
        eta_vals.append(None)
        Vpar_vals.append(None)

    return eta_vals, Vpar_vals
