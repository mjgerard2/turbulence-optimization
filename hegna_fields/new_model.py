import numpy as np
import sys, os
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd

def calc_jacobian(eta, xi, params):
    d = params['Ro']**2 + (params['N'] * params['Delta'])**2
    d = d - 0.25 * (params['iota'] * params['N'] * params['rho'])**2 \
        * (3*params['kappa'] - 1) * (params['kappa'] - 1) * np.cos(xi)

    e = 0.5 * params['iota'] * (3*params['kappa'] - 1) * params['Delta'] * params['rho'] * params['N']**2

    b = 0.25 * (params['iota'] * params['rho'])**2 * params['N'] * (params['N'] + 1)
    b = b * (3*params['kappa'] - 1) * (params['kappa'] - 1) * np.sin(xi)

    eta_2 = 0.5 * eta
    eta_2_low = eta_2[eta_2 < (-0.5 * np.pi)]
    eta_2_mid = eta_2[(eta_2 >= (-0.5 * np.pi)) & (eta_2 <= (0.5 * np.pi))]
    eta_2_hgh = eta_2[eta_2 > (0.5 * np.pi)]

    tan_coef = ((d - e) / np.sqrt(d**2 - e**2))
    tan_coef_low = tan_coef[0:eta_2_low.shape[0]]
    tan_coef_mid = tan_coef[eta_2_low.shape[0]:eta_2_low.shape[0]+eta_2_mid.shape[0]]
    tan_coef_hgh = tan_coef[eta_2_low.shape[0]+eta_2_mid.shape[0]::]

    atan_low = np.arctan(tan_coef_low * np.tan(eta_2_low)) - np.pi
    atan_mid = np.arctan(tan_coef_mid * np.tan(eta_2_mid))
    atan_hgh = np.arctan(tan_coef_hgh * np.tan(eta_2_hgh)) + np.pi
    atan = np.append(np.append(atan_low, atan_mid), atan_hgh)

    exp_coeff = (2 * b) / np.sqrt(d**2 - e**2)
    jacob_exp = np.exp(exp_coeff * atan)
    #jacob_exp_mid = np.exp(exp_coeff_mid * atan_mid)
    #jacob_exp_hgh_ = np.exp(exp_coeff_hgh * atan_hgh)

    jacob_coef = d + e * np.cos(eta)
    jacob = jacob_coef * jacob_exp
    #jacob_mid = jacob_coef * jacob_exp_mid
    #jacob_hgh = jacob_coef * jacob_exp_hgh

    # jacob = jacob_mid
    #jacob = np.append(np.append(jacob_low, jacob_mid), jacob_hgh)
    return jacob


def calc_modB(eta, xi, params):
    jacob = calc_jacobian(eta, xi, params)
    return np.sqrt(params['Ro']**2 + (params['N'] * params['Delta'])**2) / jacob


params = {'Ro': 1.5,
          'N': 4,
          'a': 0.15,
          'Delta': 0.1,
          'iota': 1,
          'kappa': 5}

alpha = 0
pol_dom = np.pi * np.linspace(-1, 1, 500)
eta_dom = ((params['N'] - params['iota']) / params['iota']) * pol_dom - (params['N'] / params['iota']) * alpha
xi_dom = ((params['N'] - 2*params['iota']) / params['iota']) * pol_dom - ((params['N'] - params['iota']) / params['iota']) * alpha

plot = pd.plot_define()
plt, fig, ax = plot.plt, plot.fig, plot.ax

kappa_dom = np.linspace(1, 1.5, 5)
for kappa in kappa_dom:
    params['kappa'] = kappa
    params['rho'] = np.sqrt(2 / (5*params['kappa']**2 - 4*params['kappa'] + 1)) * params['a']

    jacob = calc_jacobian(eta_dom, xi_dom, params)

    ax.plot(pol_dom, jacob, label=r'$\kappa = $'+'{0:0.1f}'.format(kappa))

ax.set_xlim(-np.pi, np.pi)

ax.set_xlabel(r'$\theta$')
ax.set_ylabel(r'$\sqrt{g}$')

box = ax.get_position()
ax.set_position([box.x0, box.y0, box.width * 1, box.height])

plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1))

plt.tight_layout()
plt.grid()
plt.show()
