#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jul 10 16:21:28 2021

@author: michael
"""

import numpy as np
import h5py as hf

import os, sys
path = os.path.dirname(os.path.abspath(os.getcwd()))
sys.path.append(path)

import plot_define as pd
import vmecTools.wout_files.wout_read as wr

def calc_Kappa(wout, s_dom, upts=101, vpts=51):
    """ Claculate the Kappa parameter as a function of toroidal angle and its
    toroidal average.
    
    Parameters
    ----------
    wout : obj
        Wout file object.
    s_dom : arr
        1D array of psiN surfaces on which to calculate the Kappa parameter.
    upts : int, optional
        Number of poloidal points in discretization. Default is 101.
    vpts : int, optional
        Number of toroidal points in discretization. Default is 51.
    """    
    ### Grid Domain ###
    u_dom = np.linspace(0, 2*np.pi, upts)
    v_dom = np.linspace(0, 0.25*np.pi, vpts)
    
    ### Get Magnetic Axis ###
    wout.transForm_2D_sSec(0, u_dom, v_dom, ['R','Z'])
    
    Rma = wout.invFourAmps['R']
    Zma = wout.invFourAmps['Z']
    ma_vec = np.stack((Rma[:,0], Zma[:,0], np.zeros(vpts)), axis=1)
    
    ### Calculate Center Line ###
    R_major = wout.R_major
    cLine = np.array([R_major,0,0])
    
    ### Calculate Delta ###
    delta = np.linalg.norm(ma_vec - cLine, axis=1)
    delta_avg = np.trapz(delta, v_dom) / v_dom[-1]
    
    ### Calculate Parallel and Perpendiculat Directions to Shafranov Shift ###
    k_par = ma_vec - cLine; k_par = k_par / np.repeat(np.linalg.norm(k_par, axis=1), 3).reshape(k_par.shape)
    k_perp = -np.cross(k_par, np.array([0,0,1]))
    
    ### Calculate Kappa Across Flux Surface ###
    kappa = np.empty((s_dom.shape[0], vpts))
    kappa_avg = np.empty(s_dom.shape)
    for sdx, s_val in enumerate(s_dom):
        wout.transForm_2D_sSec(s_val, u_dom, v_dom, ['R','Z'])
        
        R = wout.invFourAmps['R']
        Z = wout.invFourAmps['Z']
        
        ### Transform Coordinates to Magnetic Axis ###
        R_vec = np.stack((R, Z, np.zeros((vpts, upts))), axis=2)
        r_vec = R_vec - np.tile(ma_vec, upts).reshape(vpts, upts, 3)
        
        ### Dot Product of Flux Surface Vector in MA Reference Frame With Shafranov Shift ###
        k_perp_tiled = np.tile(k_perp, upts).reshape(vpts, upts, 3)
        k_par_tiled = np.tile(k_par, upts).reshape(vpts, upts, 3)
        
        kappa_perp = np.abs( np.sum(r_vec * k_perp_tiled, axis=2) )
        kappa_par = np.abs( np.sum(r_vec * k_par_tiled, axis=2) )
        
        ### Calculate Toroidal Averages ###
        kappa_perp_avg = np.trapz(kappa_perp, u_dom, axis=1)
        kappa_par_avg = np.trapz(kappa_par, u_dom, axis=1)
        
        ### Calculate Kappa Ratio ###
        kappa[sdx] = kappa_perp_avg / kappa_par_avg
        kappa_avg[sdx] = np.trapz(kappa[sdx], v_dom) / v_dom[-1]
    
    return kappa, delta, kappa_avg, delta_avg


### psiN Surface ###
roa_dom = np.array([0.25, 0.5, 0.75])
s_dom = roa_dom * roa_dom

### Number of Grid Points ###
upts = 101
vpts = int(0.5 * upts) + 1

path = os.path.join('/home','michael','Desktop','Research','stellarators','HSX','Epsilon_Valley','valley_100','wout_files')
names = [f.name for f in os.scandir(path) if f.name.endswith('.nc')]
npts = len(names)

kappa_avg = np.empty((npts, 3+s_dom.shape[0]))
delta_avg = np.empty((npts, 4))

for ndx, name in enumerate(names): 
    conID = name.split('_')[2].split('.')[0]
    conArr = np.array([float(ID) for ID in conID.split('-')])
    
    print('Working : '+conID+' : {0} of {1}'.format(ndx+1, npts))
    
    wout = wr.readWout(path, name=name)
    kap, delt, kap_avg, delt_avg = calc_Kappa(wout, s_dom, upts=upts, vpts=vpts)
    
    kappa_avg[conID] = np.r_[conArr, kap_avg]
    delta_avg[conID] = np.r_[conArr, delt_avg]
    

hf_path = os.path.join('/home','michael','Desktop','Research','stellarators','HSX','Epsilon_Valley','valley_100','Fk_params.h5')
with hf.File(hf_path, 'w') as hf_file:
    hf_file.create_dataset('kappa', data=kappa_avg)
    hf_file.create_dataset('delta', data=delta_avg)
        
colors = ['k', 'tab:red']
if False:
    ### Plot Kappa Calculation ###
    sdx = 2
    v_dom = np.linspace(0, .25*np.pi, vpts)
    
    plot = pd.plot_define(lineWidth=2)
    
    for ndx, name in enumerate(names):
        conID = name.split('_')[1].split('.')[0]
        
        plot.plt.plot(v_dom/np.pi, kappa[ndx, sdx], c=colors[ndx], label=conID + r' : $\langle\kappa\rangle = $'+'{0:0.3f}'.format(kappa_avg[ndx, sdx]))
        plot.plt.plot(v_dom/np.pi, [kappa_avg[ndx, sdx]]*vpts, c=colors[ndx], ls='--')

    plot.plt.xlabel(r'$\zeta \, [\pi]$')
    plot.plt.ylabel(r'$\kappa$')
    plot.plt.title(r'$r/a = $'+'{}'.format(roa_dom[sdx]))
    
    plot.plt.grid()
    plot.plt.legend()
    plot.plt.show()

if False:
    ## Delta Plotting ###
    v_dom = np.linspace(0, .25*np.pi, vpts)
    plot = pd.plot_define(lineWidth=2)

    for ndx, name in enumerate(names):
        conID = name.split('_')[1].split('.')[0]
        
        plot.plt.plot(v_dom/np.pi, delta[ndx], c=colors[ndx], label=conID + r' : $\langle\Delta\rangle = $'+'{0:0.3f}'.format(delta_avg[ndx]))
        plot.plt.plot(v_dom/np.pi, [delta_avg[ndx]]*v_dom.shape[0], c=colors[ndx], ls='--')
        
    plot.plt.xlabel(r'$\zeta\, [\pi]$')
    plot.plt.ylabel(r'$\Delta$')
    
    plot.plt.grid()
    plot.plt.legend()
    plot.plt.tight_layout()
    plot.plt.show()
