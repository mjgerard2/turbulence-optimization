import numpy as np
import h5py as hf
import field_functions as ff

from scipy.interpolate import interp1d

import matplotlib as mpl
import matplotlib.pylab as plb
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
from matplotlib.animation import FuncAnimation

import sys, os
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd


def v_parallel(M, eta_dom, params):
    P = params['rho'] / params['Delta']
    P2 = P**2
    P_inv = 1. / P

    kappa = params['kappa']
    kappa_low = 0.25 * P_inv * (np.sqrt(1 + 16*P2) - 1)
    kappa_hgh = 0.25 * P_inv * (np.sqrt(1 + 16*P2) + 1)

    rho_prime = params['N'] * params['rho'] / params['Ro']
    Delta_prime = params['N'] * params['Delta'] / params['Ro']

    if kappa < kappa_low:
        nu, eta_grid, index, second_well = ff.low_kappa_nu(M, params)
    elif kappa >= kappa_low and kappa <= kappa_hgh:
        nu, eta_grid = ff.mid_kappa_nu(M, params)
        second_well = False
    elif kappa > kappa_hgh:
        nu, eta_grid = ff.hgh_kappa_nu(M, params)
        second_well = False

    if second_well:
        eta_1well = np.r_[-np.flip(eta_grid[1:index[1]]), eta_grid[0:index[1]]]
        eta_2well_rgt = eta_grid[index[1]:]
        eta_2well_lft = -np.flip(eta_2well_rgt)

        nu_1well = np.r_[np.flip(nu[1:index[1]]), nu[0:index[1]]]
        nu_2well_rgt = nu[index[1]:]
        nu_2well_lft = np.flip(nu_2well_rgt)

        Vpar_div_V_1well = np.sqrt(rho_prime * Delta_prime * nu_1well)
        Vpar_div_V_2well_rgt = np.sqrt(rho_prime * Delta_prime * nu_2well_rgt)
        Vpar_div_V_2well_lft = np.sqrt(rho_prime * Delta_prime * nu_2well_lft)

        Vpar_div_V_1well_fit = interp1d(eta_1well, Vpar_div_V_1well)
        Vpar_div_V_2well_rgt_fit = interp1d(eta_2well_rgt, Vpar_div_V_2well_rgt)
        Vpar_div_V_2well_lft_fit = interp1d(eta_2well_lft, Vpar_div_V_2well_lft)

        num_lft = eta_dom[(eta_dom > eta_2well_lft[-1]) & (eta_dom < eta_1well[0])].shape[0]
        num_rgt = eta_dom[(eta_dom > eta_1well[-1]) & (eta_dom < eta_2well_rgt[0])].shape[0]

        zero_lft = np.zeros(num_lft)
        zero_rgt = np.zeros(num_rgt)

        Vpar_lft = Vpar_div_V_2well_lft_fit(eta_dom[eta_dom <= eta_2well_lft[-1]])
        Vpar_mid = Vpar_div_V_1well_fit(eta_dom[(eta_dom >= eta_1well[0]) & (eta_dom <= eta_1well[-1])])
        Vpar_rgt = Vpar_div_V_2well_rgt_fit(eta_dom[eta_dom >= eta_2well_rgt[0]])

        return np.r_[Vpar_lft, zero_lft, Vpar_mid, zero_rgt, Vpar_rgt]

    else:
        eta_grid = np.r_[-np.flip(eta_grid[1::]), eta_grid]
        nu_grid = np.r_[np.flip(nu[1::]), nu]

        Vpar_div_V = np.sqrt(rho_prime * Delta_prime * nu_grid)
        Vpar_dir_V_fit = interp1d(eta_grid, Vpar_div_V)

        num_beg = eta_dom[eta_dom < eta_grid[0]].shape[0]
        num_end = eta_dom[eta_dom > eta_grid[-1]].shape[0]

        zero_beg = np.zeros(num_beg)
        zero_end = np.zeros(num_end)
        eta_samp = eta_dom[num_beg:(eta_dom.shape[0] - num_end)]

        Vpar_fit = np.r_[zero_beg, Vpar_dir_V_fit(eta_samp), zero_end]
        return Vpar_fit


params = {'Deta': 1e-3,
          'Ro': 1.5,
          'Delta': 0.1,
          'iota': 1.05,
          'minor': 0.05,
          'normalization': 4*np.pi**2}

path = os.path.join('/home', 'michael', 'Desktop', '3D_chrisModel_kScan')
scan_dircs = ['scan_5']#, 'scan_5', 'scan_6']
nfps = [10, 12]

for sdx, scan_dirc in enumerate(scan_dircs):
    print(scan_dirc)

    params['N'] = nfps[sdx]
    scan_path = os.path.join(path, scan_dirc)

    k_dircs = [f.name for f in os.scandir(scan_path) if os.path.isdir(f)]
    k_vals = sorted([float(k[1::]) for k in k_dircs])
    for k_val in k_vals:
        k_dirc = 'k{0:0.2f}'.format(k_val)
        k_path = os.path.join(scan_path, k_dirc)

        params['kappa'] = k_val
        params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['minor']

        Mpts = 10
        M_dom = np.linspace(0.1, 1, Mpts)

        eta_pnts = 501
        eta_dom = np.linspace(-np.pi, np.pi, eta_pnts)

        v_par_set = np.empty((M_dom.shape[0], eta_pnts))
        for Mdx, M in enumerate(M_dom):
            v_par_set[Mdx] = v_parallel(M, eta_dom, params)

        # Single Plot #
        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': 18}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = 22
        mpl.rcParams['lines.linewidth'] = 2

        fig = plt.figure(constrained_layout=True, figsize=(8, 6))
        spec = gs.GridSpec(3, 1, figure=fig)

        ax1 = fig.add_subplot(spec[1:3, :])
        ax2 = fig.add_subplot(spec[0, :], sharex=ax1)
        plt.setp(ax2.get_xticklabels(), visible=False)
        ax3 = ax2.twinx()

        ax3.spines['right'].set_color('tab:red')
        ax3.tick_params(axis='y', colors='tab:red')

        B_mod = ff.B(eta_dom, params)
        K_norm = ff.Kn(eta_dom, params)
        Grad_Psi = ff.Grad_Psi(eta_dom, params)
        D = ff.D(eta_dom, params)
        K_geod = ff.Kg(eta_dom, params)
        shear = ff.iota_prime(params)

        rho_prime = params['N'] * params['rho'] / params['Ro']
        Delta_prime = params['N'] * params['Delta'] / params['Ro']

        curve_drive = K_norm / Grad_Psi
        curve_drive = curve_drive + D * K_geod * Grad_Psi / B_mod

        curve_absMax = np.max(np.abs(curve_drive))

        eta_norm = eta_dom / np.pi
        cmap = plt.get_cmap('tab20b', 10)

        for Mdx, M in enumerate(np.flip(M_dom)[0:-1]):
            key = 'line '+str(Mdx)
            mdx = M_dom.shape[0] - 1 - Mdx
            ax1.fill_between(eta_norm, np.zeros(eta_norm.shape), v_par_set[mdx, :], color=cmap(0.999*M))
        ax1.fill_between(eta_norm, 0, v_par_set[0, :], color=cmap(0.999*M_dom[0]))

        pos1 = ax1.get_position()
        ax1c = fig.add_axes([0.85, pos1.y0+.025, pos1.width/20, pos1.height])
        norm = mpl.colors.Normalize(vmin=0, vmax=1)
        cb1 = mpl.colorbar.ColorbarBase(ax1c, cmap=cmap, norm=norm, orientation='vertical')
        cb1.ax.set_ylabel(r'$M$')

        ax2.plot(eta_norm, B_mod, c='k', linewidth=4)
        ax3.plot(eta_norm, curve_drive, c='tab:red', linewidth=4)
        ax3.plot(eta_norm, K_norm / Grad_Psi, ls='--', c='tab:red', linewidth=4, label=r'$\kappa_n / |\nabla \psi|$')
        ax3.plot(eta_norm, np.zeros(eta_norm.shape), ls='dotted', c='tab:red', linewidth=2)

        neg_idx = np.where(curve_drive < 0)[0]
        neg_bool = ((neg_idx - np.roll(neg_idx, 1)) != 1)
        negDX = np.where(neg_bool)[0]

        alpha_clr = 0.3
        for i, idx in enumerate(negDX[0:-1]):
            eta_beg = neg_idx[idx]
            eta_end = neg_idx[negDX[i+1] - 1] + 1

            ax1.axvspan(eta_norm[eta_beg], eta_norm[eta_end], alpha=alpha_clr, color='tab:red')
            ax2.axvspan(eta_norm[eta_beg], eta_norm[eta_end], alpha=alpha_clr, color='tab:red')

        if len(negDX) > 0:
            eta_beg = neg_idx[negDX[-1]]
            eta_end = neg_idx[-1]
            ax1.axvspan(eta_norm[eta_beg], eta_norm[eta_end], alpha=alpha_clr, color='tab:red')
            ax2.axvspan(eta_norm[eta_beg], eta_norm[eta_end], alpha=alpha_clr, color='tab:red')

        ax1.set_xlabel(r'$\eta\, (\pi)$')
        ax1.set_ylabel(r'$\frac{ v_{\parallel} }{v}$')

        ax2.set_ylabel(r'$B$')
        ax3.set_ylabel(r'$\frac{ \kappa_n }{ |\nabla \psi| } + D \kappa_g \frac{ |\nabla \psi| }{B}$', c='tab:red')

        ax1.set_xlim(-1, 1)
        ax1.set_ylim(0, 1)
        ax2.set_ylim(0.99*np.min(B_mod), 1.01*np.max(B_mod))
        ax3.set_ylim(-curve_absMax, curve_absMax)
        ax2.grid()

        ax3.legend()

        saveName = os.path.join(k_path, 'Vpar_model.png')
        #plt.savefig(saveName)
        #plt.close('all')
        plt.show()

if False:
    # Make Animation #
    plot = pd.plot_define(lineWidth=2)
    plt, fig, ax1 = plot.plt, plot.fig, plot.ax
    ax2 = ax1.twinx()

    ax2.spines['right'].set_color('tab:red')
    ax2.tick_params(axis='y', colors='tab:red')

    ax2.set_ylabel(r'$\kappa_n$', c='tab:red')
    ax1.set_xlabel(r'$\eta \ [\pi]$')
    ax1.set_ylabel(r'$|B|$')

    ax1.set_xlim(-1, 1)

    plt.tight_layout()

    eta_norm = eta_dom / np.pi

    fill_dict = {}
    fill_clrs = {}
    clrs = ['#4e79a7', '#f28e2b', '#e15759', '#76b7b2', '#59a14f', '#edc948',
            '#b07aa1', '#ff9da7', '#9c755f', '#bab0ac']

    params['kappa'] = kappa_dom[0]
    B_mod = B(eta_dom, params)
    K_norm = Kn(eta_dom, params)

    v_par_set[0] = (v_par_set[0] / np.max(v_par_set[0])) * np.max(B_mod)
    for Mdx, M in enumerate(np.flip(M_dom)[0:-1]):
        key = 'fill '+str(Mdx)
        mdx = M_dom.shape[0] - 1 - Mdx
        fill_dict[key] = ax1.fill_between(eta_norm, v_par_set[0, mdx-1], v_par_set[0, mdx])
        fill_clrs[key] = clrs[Mdx]
    ax1.fill_between(eta_norm, 0, v_par_set[0, 0, :], color=clrs[9])
    fill_clrs['fill 9'] = clrs[Mdx]

    line_B, = ax1.plot([], [], c='k', lw=4)
    line_Kn, = ax2.plot([], [], c='tab:red', lw=4)

    title = ax1.text(0.7,0.85, "", bbox={'facecolor':'k', 'alpha':0.25, 'pad':5},
                    transform=ax1.transAxes, ha="center")

    def init():
        line_B.set_data([], [])
        line_Kn.set_data([], [])

        title.set_text("")

        return line_B, line_Kn, title,

    def animate(i):
        params['kappa'] = kappa_dom[i]
        B_mod = B(eta_dom, params)
        K_norm = Kn(eta_dom, params)

        K_max = np.max(K_norm)
        K_min = np.min(K_norm)
        if K_max >= np.abs(K_min):
            K_absMax = K_max
        else:
            K_absMax = np.abs(K_min)

        ax1.set_ylim(0, 1.5*np.max(B_mod))
        ax2.set_ylim(-K_absMax, K_absMax)

        line_B.set_data(eta_norm, B_mod)
        line_Kn.set_data(eta_norm, K_norm)

        v_par_set[i] = (v_par_set[i] / np.max(v_par_set[i])) * np.max(B_mod)
        for Mdx, M in enumerate(np.flip(M_dom)[0:-1]):
            key = 'fill '+str(Mdx)
            fill_dict[key].remove()
            mdx = M_dom.shape[0] - 1 - Mdx
            fill_dict[key] = ax1.fill_between(eta_norm, v_par_set[i, mdx-1], v_par_set[i, mdx], color=fill_clrs[key])
        ax1.fill_between(eta_norm, 0, v_par_set[i, 0, :], color=fill_clrs['fill 9'])

        title.set_text(r'$\kappa = $'+'{0:0.3f}'.format(kappa_dom[i]))

        return line_B, line_Kn, title

    anim = FuncAnimation(fig, animate, init_func=init, frames=100, interval=50, blit=True)
    anim.save('/home/michael/onedrive/full_kappa_range.gif', writer='imagemagick')
