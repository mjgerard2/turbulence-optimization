import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.wout_read as wr


class Local_Boundary():


    def __init__(self, params):
        self.params = params


    def R_boundary(self, theta, zeta):
        R_val = self.params['Ro'] + self.params['rho'] * np.cos(theta)
        return R_val


    def Z_boundary(self, theta, zeta):
        Z_val = self.params['rho'] * self.params['kappa'] * np.sin(theta)
        return Z_val


    def save_model_data(self, metaFile, rho_dom, kappa_dom):
        with open(metaFile, 'w') as file:
            file.write('N = {}\n'.format(self.params['N']))
            file.write('Ro = {}\n'.format(self.params['Ro']))
            file.write('a = {}\n'.format(self.params['a']))
            file.write('Delta = {}\n'.format(self.params['Delta']))
            file.write('iota = {}\n'.format(self.params['iota']))

            rho_str = 'rho = ('+', '.join(['{0}'.format(rho) for rho in rho_dom])+')\n'
            kappa_str = 'kappa = ('+', '.join(['{0}'.format(kappa) for kappa in kappa_dom])+')\n'
            file.write(rho_str)
            file.write(kappa_str)


    def make_boundary_VTK(self, savePath, saveName, theta_dom, zeta_dom):
        import vtkTools.vtk_grids as vtkG

        theta_grid, zeta_grid = np.meshgrid(theta_dom, zeta_dom)

        R_grid = self.R_boundary(theta_grid, zeta_grid)
        Z_grid = self.Z_boundary(theta_grid, zeta_grid)

        X_grid = R_grid * np.cos(zeta_grid)
        Y_grid = R_grid * np.sin(zeta_grid)
        cartCoord = np.stack((X_grid, Y_grid, Z_grid), axis=2)

        vtkG.scalar_mesh(savePath, saveName, cartCoord, np.ones(X_grid.shape))


    def descur_input(self, savePath, theta_pnts, zeta_pnts, plotCheck=False):
        nfp = int(self.params['N'])
        if nfp == 0:
            nfp = 1

        if zeta_pnts % nfp != 0 and theta_pnts % nfp != 0:
            raise ValueError('zeta_pnts and theta_pnts must be multiples of the number of field periods ({0:0.0f}).'.format(nfp))

        # Grid parameters #
        theta_descPnts = int(theta_pnts * nfp)
        zeta_descPnts = int(zeta_pnts / nfp)
        npts = int(theta_descPnts * zeta_descPnts)

        # Construct theta grid #
        theta_dom = np.linspace(0, 2*np.pi, theta_pnts, endpoint=False)
        theta_grid = np.repeat(theta_dom, nfp)
        theta_grid = np.tile(theta_grid, zeta_descPnts)

        # Construct zeta grid #
        zeta_spc = np.linspace(0, 2*np.pi/nfp, zeta_descPnts, endpoint=False)
        zeta_dom = np.linspace(0, 2*np.pi, nfp, endpoint=False)
        zeta_grid = np.tile(zeta_dom, theta_pnts*zeta_descPnts)
        zeta_grid = zeta_grid + np.repeat(zeta_spc, theta_descPnts)

        # Calculate coordinate grid #
        R_grid = self.R_boundary(theta_grid, zeta_grid)
        Z_grid = self.Z_boundary(theta_grid, zeta_grid)
        ordered_surface = np.stack((R_grid, -zeta_grid, Z_grid), axis=1)

        # Save DESCUR input #
        with open(savePath, 'w') as file:
            file.write('{0} {1} {2}\n'.format(theta_descPnts, zeta_descPnts, nfp))
            for n in range(npts):
                file.write('{0:0.6f} {1:0.6f} {2:0.6f} \n'.format(ordered_surface[n,0], ordered_surface[n,1], ordered_surface[n,2]))

        # Visually check ordered surface for errors #
        if plotCheck:
            import plot_define as pd

            plot = pd.plot_define(proj3D=True)
            plt, fig, ax = plot.plt, plot.fig, plot.ax

            X_surf = ordered_surface[:,0] * np.cos(ordered_surface[:,1])
            Y_surf = ordered_surface[:,0] * np.sin(ordered_surface[:,1])
            Z_surf = ordered_surface[:,2]

            for i in range(zeta_dom.shape[0]-1):
                begIdx = int(i * zeta_pnts)
                endIdx = int((i + 1) * zeta_pnts)

                X_plot = X_surf[begIdx:endIdx]
                Y_plot = Y_surf[begIdx:endIdx]
                Z_plot = Z_surf[begIdx:endIdx]

                ax.scatter(X_plot, Y_plot, Z_plot)

            ax.set_zlim(np.min(X_surf), np.max(X_surf))
            plt.show()

    def input_VMEC(self, template, descur, output, params, phiEdge=0.039418):
        """ Generate VMEC input from template, with the boundary provided by
        the specified DESCUR file.

        Parameters
        ----------
        template : str
            Absolute path to the template VMEC input.
        descur : str
            Absolute path to the DESCUR output to pull boundary data from.
        output : str
            Absolute path to the VMEC input that will be generated.
        psiEdge : float, Optional
            Toroidal magnetic flux at plasma boundary. Default is 4pi^2
        """
        read_segment = False

        mode_num = []
        BC_amps = []
        AX_amps = []

        nfp = self.params['N']
        if nfp == 0:
            nfp = 1

        with open(descur, 'r') as my_file:
            lines = my_file.readlines()
            for line in lines:
                line = line.split()
                line_len = len(line)
                if line_len > 0:
                    if read_segment:
                        try:
                            int(line[0])

                            mode_num.append([line[0], line[1]])
                            BC_amps.append([line[2], line[5]])
                            if line_len > 6:
                                AX_amps.append([line[6], line[7]])
                        except ValueError:
                            read_segment = False
                    if line[0] == 'MB' and line_len > 3:
                        read_segment = True

        press_0 = (params['iota']*params['alpha']/params['rho']) * ((2*np.pi)**2 / params['normalization'])
        press_2 = - 0.5 * (params['iota']*params['alpha']/params['rho']) * ((2*np.pi)**2 / params['normalization'])

        iota_0 = 0.5
        iota_2 = params['iota'] - iota_0

        with open(template, 'r') as my_file:
            lines = my_file.readlines()

            new_lines = []
            for line in lines:
                line_split = line.split()

                if line_split[0] == 'NFP':
                    new_lines.append('  NFP = {}\n'.format(nfp))
                elif line_split[0] == 'PHIEDGE':
                    new_lines.append('  PHIEDGE = {}\n'.format(phiEdge))
                elif line_split[0] == 'AM':
                    new_lines.append('  AM = {} 0.00000000E+00 {}\n'.format(press_0, press_2))
                elif line_split[0] == 'AI':
                    new_lines.append('  AI = {} 0.00000000E+00 {}\n'.format(iota_0, iota_2))
                elif line_split[0] == 'RAXIS':
                    new_lines.append('  RAXIS = ' + ' '.join([Ax[0] for Ax in AX_amps]) + '\n')
                elif line_split[0] == 'ZAXIS':
                    new_lines.append('  ZAXIS = ' + ' '.join([Ax[1] for Ax in AX_amps]) + '\n')
                elif line_split[0] == 'RBC(00,00)':
                    for idx, BC in enumerate(BC_amps):
                        RBC, ZBS = BC[0], BC[1]
                        pol_mode, tor_mode = mode_num[idx][1], mode_num[idx][0]
                        new_lines.append('  RBC( {0},{1}) = {2}  ZBS( {0},{1}) = {3}\n'.format(pol_mode, tor_mode, RBC, ZBS))
                    break
                else:
                    new_lines.append(line)

        with open(output, 'w') as my_file:
            for line in new_lines:
                my_file.write(line)
            my_file.write('/\n&END')


params = {'N': 0,
          'Ro': 1.5,
          'a': 0.15,
          'Delta': 0.,
          'iota': 1./3,
          'alpha': 1,
          'normalization': 4*np.pi*np.pi}

if False:
    path = os.path.join('/home', 'michael', 'Desktop', 'Tokamak_model_kScan', 'scan_1')
    if not os.path.isdir(path):
        os.mkdir(path)

    kappa_dom = np.linspace(0.5, 2, 7)
    rho_dom = np.empty(kappa_dom.shape)

    for kdx, kappa in enumerate(kappa_dom):
        kPath = os.path.join(path, 'k{0:0.2f}'.format(kappa))
        if not os.path.isdir(kPath):
            os.mkdir(kPath)

        params['kappa'] = kappa
        params['rho'] = params['a'] / np.sqrt(1 + 0.5*(params['kappa']**2 - 1))  # Tokamak Model
        rho_dom[kdx] = params['rho']

        model = Local_Boundary(params)

        savePath = os.path.join(kPath, 'lcfs_k{0:0.0f}.txt'.format(kappa))
        if params['N'] == 0:
            model.descur_input(savePath, 20, 20, plotCheck=False)
        else:
            model.descur_input(savePath, 20, int(params['N']*20), plotCheck=False)

    metaFile = os.path.join(path, 'parameter_list.txt')
    model.save_model_data(metaFile, rho_dom, kappa_dom)

if True:
    path = os.path.join('/home', 'michael', 'Desktop', 'Tokamak_model_kScan', 'scan_1')

    model = Local_Boundary(params)

    template = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'input.tok_model')
    dircs = [f.name for f in os.scandir(path) if os.path.isdir(f)]
    for dirc in dircs:
        params['kappa'] = float(dirc[1::])
        params['rho'] = params['a'] / np.sqrt(1 + 0.5*(params['kappa']**2 - 1))

        descur = os.path.join(path, dirc, 'outcurve')
        output = os.path.join(path, dirc, 'input.tok_model_'+dirc)
        model.input_VMEC(template, descur, output, params, phiEdge=0.05)
