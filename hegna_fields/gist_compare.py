import numpy as np
import os, sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs

import field_functions as ff

WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import gistTools as gt


# Field Keys Being Plotted #
keys = ['modB', 'gxx', 'Kn', 'Kg', 'D', 'shear', 'Jacob']

path = os.path.join('/home', 'michael', 'Desktop', '3D_chrisModel_kScan')
# scan_dircs = ['scan_25']
scan_dircs = [f.name for f in os.scandir(path) if os.path.isdir(f)]
for scan_dirc in scan_dircs:
    print(scan_dirc)
    path_scan = os.path.join(path, scan_dirc)

    param_file = os.path.join(path_scan, 'parameter_list.txt')
    with open(param_file, 'r') as f:
        lines = f.readlines()
        nfp = int(lines[0].strip().split()[2])

    ky_dircs = [f.name for f in os.scandir(path_scan) if os.path.isdir(f)]
    for ky_dirc in ky_dircs:
        print('   '+ky_dirc)
        ky_path = os.path.join(path_scan, ky_dirc)

        eta_lim_gist = np.pi
        eta_lim_model = np.pi

        # Read Gist File #
        # gistPath = os.path.join('/home',
        #                        'michael',
        #                        'Desktop',
        #                        '3D_chrisModel_kScan',
        #                        'scan_5',
        #                        'k1.50',
        #                        'gist_s1p0_alpha0_pol4_nz1024.dat'
        #                        )
        gistPath = os.path.join(ky_path, 'gist_s0p98_alpha0_pol1_nz1024.dat')

        gist = gt.read_gist(gistPath)
        gist.calc_D(nfp)

        iota = 1. / gist.q0
        eta_dom = (nfp * gist.q0 - 1) * gist.pol_dom + gist.alpha0

        eta_gist = eta_dom[(eta_dom >= -eta_lim_gist) & (eta_dom <= eta_lim_gist)]
        gist_data = {}
        for key in keys:
            gist_data[key] = gist.data[key][(eta_dom >= -eta_lim_gist) & (eta_dom <= eta_lim_gist)]

        # Field Parameters #
        params = {'Deta': 1e-3,
                  'Ro': gist.major,
                  'minor': gist.minor,
                  'iota': iota,
                  'N': nfp,
                  'Delta': 0.1,
                  'kappa': float(ky_dirc[1::]),
                  'normalization': 4*np.pi**2
                  }
        params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['minor']

        # Calculate Fields #
        eta_model = np.linspace(-eta_lim_model, eta_lim_model, 500)

        model_data = {'modB': ff.B(eta_model, params),
                      'gxx': ff.Grad_Psi(eta_model, params),
                      'Jacob': ff.jacob(eta_model, params),
                      'Kn': ff.Kn(eta_model, params),
                      'Kg': ff.Kg(eta_model, params),
                      'D': ff.D(eta_model, params),
                      'iotaP': ff.iota_prime(params)
                      }

        shear_coef = model_data['gxx']**2 / (model_data['Jacob'] * model_data['modB']**2)
        D_coef = (nfp * gist.q0 - 1) / gist.q0
        dD_dEta = np.gradient(model_data['D'], eta_model)
        model_data['shear'] = shear_coef * (model_data['iotaP'] + D_coef * dD_dEta)

        # Plot Settings #
        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': 18}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = 22
        mpl.rcParams['lines.linewidth'] = 2

        # Generate Figure Layout #
        fig = plt.figure(constrained_layout=True, figsize=(18, 12))
        spec = gs.GridSpec(4, 2, figure=fig)

        ax11 = fig.add_subplot(spec[0, 0])
        ax12 = fig.add_subplot(spec[0, 1])

        plt.setp(ax11.get_xticklabels(), visible=False)
        plt.setp(ax12.get_xticklabels(), visible=False)

        ax21 = fig.add_subplot(spec[1, 0], sharex=ax11)
        ax22 = fig.add_subplot(spec[1, 1], sharex=ax12)

        plt.setp(ax21.get_xticklabels(), visible=False)
        plt.setp(ax22.get_xticklabels(), visible=False)

        ax31 = fig.add_subplot(spec[2, 0], sharex=ax11)
        ax32 = fig.add_subplot(spec[2, 1], sharex=ax12)

        plt.setp(ax31.get_xticklabels(), visible=False)
        plt.setp(ax32.get_xticklabels(), visible=False)

        ax41 = fig.add_subplot(spec[3, 0], sharex=ax11)
        ax42 = fig.add_subplot(spec[3, 1], sharex=ax12)

        # Plot Data #
        eta_gist_norm = eta_gist / np.pi
        eta_model_norm = eta_model / np.pi

        ax11.plot(eta_gist_norm, gist_data['modB'], c='k')
        ax12.plot(eta_model_norm, model_data['modB'], c='k')

        ax21.plot(eta_gist_norm, gist_data['gxx'], c='k')
        ax22.plot(eta_model_norm, model_data['gxx'], c='k')

        ax31.plot(eta_gist_norm, gist_data['Kn'], c='tab:red', label=r'$\kappa_n$')
        ax32.plot(eta_model_norm, model_data['Kn'], c='tab:red', label=r'$\kappa_n$')

        ax31.plot(eta_gist_norm, gist_data['Kg'], c='tab:blue', label=r'$\kappa_g$')
        ax32.plot(eta_model_norm, model_data['Kg'], c='tab:blue', label=r'$\kappa_g$')

        ax41.plot(eta_gist_norm, gist_data['D'], c='k')
        # ax41.plot(eta_gist_norm, gist_data['shear'], c='k')
        # ax41.plot(eta_gist_norm, [gist.shat]*eta_gist_norm.shape[0], c='k', ls='--')
        ax42.plot(eta_model_norm, model_data['D'], c='k')
        # ax42.plot(eta_model_norm, model_data['shear'], c='k')
        # ax42.plot(eta_model_norm, shear_coef * model_data['iotaP'], c='k', ls='--')

        # Plot Labels #
        ax41.set_xlabel(r'$\eta (\pi)$')
        ax42.set_xlabel(r'$\eta (\pi)$')

        # ax41.set_ylabel(r'$s_{loc}$')
        # ax41.set_ylabel(r'$\frac{dD}{d\theta}$')
        ax41.set_ylabel(r'$D$')
        # ax42.set_ylabel(r'$s_{loc}$')
        ax42.set_ylabel(r'$D$')
        ax31.set_ylabel(r'$\kappa$')
        ax21.set_ylabel(r'$|\nabla \psi|$')
        ax11.set_ylabel(r'$|B|$')

        # Plot Limits #
        ax11.set_xlim(-eta_lim_gist / np.pi, eta_lim_gist / np.pi)
        ax12.set_xlim(-eta_lim_model / np.pi, eta_lim_model / np.pi)

        # Plot Styles #
        ax31.legend(loc='lower left')
        ax32.legend(loc='lower left')

        ax11.grid()
        ax12.grid()

        ax21.grid()
        ax22.grid()

        ax31.grid()
        ax32.grid()

        ax41.grid()
        ax42.grid()

        ax11.set_title(r'$\kappa = $' + '{0:0.2f} (GIST)'.format(params['kappa']))
        ax12.set_title(r'$\kappa = $' + '{0:0.2f} (3D Model)'.format(params['kappa']))

        # plt.show()
        saveName = os.path.join(ky_path, 'field_comp.png')
        plt.savefig(saveName)
        plt.close('all')
