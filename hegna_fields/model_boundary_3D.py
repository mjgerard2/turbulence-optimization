import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.wout_read as wr
import vtkTools.vtk_grids as vtkG


class Local_Boundary():

    def __init__(self, params, model):
        self.params = params
        self.model = model

    def R_boundary(self, theta, zeta):
        if self.model == 'tokModel':
            R_val = self.params['Ro'] + self.params['rho'] * np.cos(theta)

        elif self.model == 'chrisModel':
            eta = self.params['N'] * zeta - theta
            R_val = self.params['Ro'] + self.params['Delta'] * np.cos(self.params['N']*zeta)
            R_val = R_val + self.params['rho'] * np.cos(theta)
            R_val = R_val + self.params['rho'] * (self.params['kappa'] - 1) * np.sin(eta) * np.sin(self.params['N'] * zeta)

        elif self.model == 'negRotModel':
            eta = self.params['N'] * zeta - theta
            R_val = self.params['Ro'] + self.params['Delta'] * np.cos(self.params['N']*zeta)
            R_val = R_val - self.params['rho'] * np.cos(theta)
            R_val = R_val - self.params['rho'] * (self.params['kappa'] - 1) * np.sin(eta) * np.sin(self.params['N'] * zeta)

        elif self.model == 'novelModel':
            eta = (self.params['N'] / 2) * zeta - theta
            R_val = self.params['Ro'] + self.params['Delta'] * np.cos(self.params['N']*zeta)
            R_val = R_val + self.params['rho'] * np.sin(theta)
            R_val = R_val + self.params['rho'] * (self.params['kappa'] - 1) * np.sin(eta) * np.cos((self.params['N'] / 2) * zeta)

        elif self.model == 'model':
            eta = self.params['N'] * zeta - theta
            R_val = self.params['Ro'] + self.params['Delta'] * np.cos(self.params['N']*zeta)
            R_val = R_val + 0.5 * self.params['rho'] * (self.params['kappa'] + 1) * np.cos(theta)
            R_val = R_val - 0.5 * self.params['rho'] * (self.params['kappa'] - 1) * np.cos(eta)

        else:
            raise KeyError('Either no model has been specified or the one requested is not available.')

        return R_val

    def Z_boundary(self, theta, zeta):
        if self.model == 'tokModel':
            Z_val = self.params['rho'] * self.params['kappa'] * np.sin(theta)

        elif self.model == 'chrisModel':
            eta = self.params['N'] * zeta - theta

            Z_val = self.params['Delta'] * np.sin(self.params['N']*zeta)
            Z_val = Z_val + self.params['rho'] * np.sin(theta)
            Z_val = Z_val - self.params['rho'] * (self.params['kappa'] - 1) * np.sin(eta) * np.cos(self.params['N'] * zeta)

        elif self.model == 'negRotModel':
            eta = self.params['N'] * zeta - theta

            Z_val = self.params['Delta'] * np.sin(self.params['N']*zeta)
            Z_val = Z_val - self.params['rho'] * np.sin(theta)
            Z_val = Z_val + self.params['rho'] * (self.params['kappa'] - 1) * np.sin(eta) * np.cos(self.params['N'] * zeta)

        elif self.model == 'novelModel':
            eta = (self.params['N'] / 2) * zeta - theta
            Z_val = self.params['Delta'] * np.sin(self.params['N']*zeta)
            Z_val = Z_val + self.params['rho'] * np.sin(theta)
            Z_val = Z_val - self.params['rho'] * (self.params['kappa'] - 1) * np.sin(eta) * np.cos((self.params['N'] / 2) * zeta)

        elif self.model == 'model':
            eta = self.params['N'] * zeta - theta
            Z_val = self.params['Delta'] * np.sin(self.params['N']*zeta)
            Z_val = Z_val + 0.5 * self.params['rho'] * (self.params['kappa'] + 1) * np.sin(theta)
            Z_val = Z_val - 0.5 * self.params['rho'] * (self.params['kappa'] - 1) * np.sin(eta)

        else:
            raise KeyError('Either no model has been specified or the one requested is not available.')

        return Z_val

    def modB_boundary(self, theta, zeta):
        Ro = self.params['Ro']
        N = self.params['N']
        rho = self.params['rho']
        Delta = self.params['Delta']
        kappa = self.params['kappa']

        if self.model == 'chrisModel':
            eps_denom = Ro**2 + (Delta*N)**2 + (rho*N*(kappa - 1))**2
            eps_h = (rho * Delta * N * N * (2 - kappa)) / eps_denom
            eps_2h = (-.5*(kappa**2 - 1)*(rho*N))
            jacob = 1 + eps_h * np.cos(N*zeta -theta) + eps_2h * np.cos(2*N*zeta - 2*theta)

            B_numer = Ro**2 + (Delta*N)**2 + (rho*N*(kappa - 1))**2 - 2*rho*Delta*N*N*(kappa - 1)*np.cos(N*zeta - theta)
            B = np.sqrt(B_numer) / jacob

        elif self.model == 'negRotModel':
            eps_denom = Ro**2 + (Delta*N)**2 + (rho*N*(kappa - 1))**2
            eps_h = (rho * Delta * N * N * (2 - kappa)) / eps_denom
            eps_2h = (-.5*(kappa**2 - 1)*(rho*N))
            jacob = 1 - eps_h * np.cos(N*zeta -theta) + eps_2h * np.cos(2*N*zeta - 2*theta)

            B_numer = Ro**2 + (Delta*N)**2 + (rho*N*(kappa - 1))**2 + 2*rho*Delta*N*N*(kappa - 1)*np.cos(N*zeta - theta)
            B = np.sqrt(B_numer) / jacob

        else:
            raise KeyError('Either no model has been specified or the one requested is not available.')

        return B

    def get_Bvec(self, theta, zeta):
        Ro = self.params['Ro']
        N = self.params['N']
        rho = self.params['rho']
        Delta = self.params['Delta']
        kappa = self.params['kappa']
        eta = N*zeta - theta

        if self.model == 'chrisModel':
            eps_denom = Ro**2 + (Delta*N)**2 + (rho*N*(kappa - 1))**2
            eps_h = (rho * Delta * N * N * (2 - kappa)) / eps_denom
            eps_2h = (-.5*(kappa**2 - 1)*(rho*N))
            jacob = 1 + eps_h * np.cos(eta) + eps_2h * np.cos(2*eta)

            B_theta = (-.5*rho*rho*N*(kappa-1)**2 - .5*rho*rho*N*(kappa**2-1)*np.cos(2*eta) + rho*Delta*N*kappa*np.cos(eta)) / jacob
            B_zeta = (Ro**2 + (Delta*N)**2 + (rho*N*(kappa-1))**2 - 2*rho*Delta*N*N*(kappa-1)*np.cos(eta)) / jacob

        elif self.model == 'negRotModel':
            eps_denom = Ro**2 + (Delta*N)**2 + (rho*N*(kappa - 1))**2
            eps_h = (rho * Delta * N * N * (2 - kappa)) / eps_denom
            eps_2h = (-.5*(kappa**2 - 1)*(rho*N))
            jacob = 1 - eps_h * np.cos(eta) + eps_2h * np.cos(2*eta)

            B_theta = (-.5*rho*rho*N*(kappa-1)**2 - .5*rho*rho*N*(kappa**2-1)*np.cos(2*eta) - rho*Delta*N*kappa*np.cos(eta)) / jacob
            B_zeta = (Ro**2 + (Delta*N)**2 + (rho*N*(kappa-1))**2 + 2*rho*Delta*N*N*(kappa-1)*np.cos(eta)) / jacob

        else:
            raise KeyError('Either no model has been specified or the one requested is not available.')

        return np.array([B_theta, B_zeta])

    def normCurve_boundary(self, theta, zeta):
        Ro, rho, kappa, iota = self.params['Ro'], self.params['rho'], self.params['kappa'], self.params['iota']
        R = Ro + rho * np.cos(theta)

        kn_numer = - kappa * R * np.cos(theta) - iota * rho * kappa
        kn_denom = np.sqrt(1 + (kappa**2 - 1) * np.cos(theta)**2)
        kn_denom = kn_denom * (R**2 + (iota*rho)**2 * (1 + (kappa**2 - 1) * np.cos(theta)**2))
        kn = kn_numer / kn_denom

        return kn

    def geodCurve_boundary(self, theta, zeta):
        Ro, rho, kappa, iota = self.params['Ro'], self.params['rho'], self.params['kappa'], self.params['iota']
        R = Ro + rho * np.cos(theta)

        kg_numer = -R**2 * np.sin(theta) - 0.5 * iota * R * rho * (kappa**2 - 1) * np.sin(2*theta)
        kg_denom = np.sqrt(1 + (kappa**2 - 1) * np.cos(theta)**2)
        kg_denom = kg_denom * (R**2 + (iota*rho)**2 * (1 + (kappa**2 - 1) * np.cos(theta)**2))**1.5
        kg = kg_numer / kg_denom

        return kg

    def save_model_data(self, metaFile, rho_dom, kappa_dom):
        with open(metaFile, 'w') as file:
            file.write('N = {}\n'.format(self.params['N']))
            file.write('Ro = {}\n'.format(self.params['Ro']))
            file.write('a = {}\n'.format(self.params['a']))
            file.write('Delta = {}\n'.format(self.params['Delta']))
            file.write('iota = {}\n'.format(self.params['iota']))

            rho_str = 'rho = ('+', '.join(['{0}'.format(rho) for rho in rho_dom])+')\n'
            kappa_str = 'kappa = ('+', '.join(['{0}'.format(kappa) for kappa in kappa_dom])+')\n'
            file.write(rho_str)
            file.write(kappa_str)

    def make_boundary_VTK(self, savePath, saveName, theta_dom, zeta_dom):
        import vtkTools.vtk_grids as vtkG

        theta_grid, zeta_grid = np.meshgrid(theta_dom, zeta_dom)

        R_grid = self.R_boundary(theta_grid, zeta_grid)
        Z_grid = self.Z_boundary(theta_grid, zeta_grid)

        modB = self.modB_boundary(theta_grid, zeta_grid)
        # kapN = self.normCurve_boundary(theta_grid, zeta_grid)
        # kapG = self.geodCurve_boundary(theta_grid, zeta_grid)

        X_grid = R_grid * np.cos(zeta_grid)
        Y_grid = R_grid * np.sin(zeta_grid)
        cartCoord = np.stack((X_grid, Y_grid, Z_grid), axis=2)

        save_modB = saveName+'_modB.vtk'
        # save_kapN = saveName+'_kapN.vtk'
        # save_kapG = saveName+'_kapG.vtk'

        vtkG.scalar_mesh(savePath, save_modB, cartCoord, modB)
        # vtkG.scalar_mesh(savePath, save_kapN, cartCoord, kapN)
        # vtkG.scalar_mesh(savePath, save_kapG, cartCoord, kapG)

    def descur_input(self, savePath, theta_pnts, zeta_pnts, plotCheck=False):
        nfp = int(self.params['N'])
        if nfp == 0:
            nfp = 1

        if zeta_pnts % nfp != 0 and theta_pnts % nfp != 0:
            raise ValueError('zeta_pnts and theta_pnts must be multiples of the number of field periods ({0:0.0f}).'.format(nfp))

        # Grid parameters #
        theta_descPnts = int(theta_pnts * nfp)
        zeta_descPnts = int(zeta_pnts / nfp)
        npts = int(theta_descPnts * zeta_descPnts)

        # Construct theta grid #
        theta_dom = np.linspace(0, 2*np.pi, theta_pnts, endpoint=False)
        theta_grid = np.repeat(theta_dom, nfp)
        theta_grid = np.tile(theta_grid, zeta_descPnts)

        # Construct zeta grid #
        zeta_spc = np.linspace(0, 2*np.pi/nfp, zeta_descPnts, endpoint=False)
        zeta_dom = np.linspace(0, 2*np.pi, nfp, endpoint=False)
        zeta_grid = np.tile(zeta_dom, theta_pnts*zeta_descPnts)
        zeta_grid = zeta_grid + np.repeat(zeta_spc, theta_descPnts)

        # Calculate coordinate grid #
        R_grid = self.R_boundary(theta_grid, zeta_grid)
        Z_grid = self.Z_boundary(theta_grid, zeta_grid)
        ordered_surface = np.stack((R_grid, -zeta_grid, Z_grid), axis=1)

        # Save DESCUR input #
        with open(savePath, 'w') as file:
            file.write('{0} {1} {2}\n'.format(theta_descPnts, zeta_descPnts, nfp))
            for n in range(npts):
                file.write('{0:0.6f} {1:0.6f} {2:0.6f} \n'.format(ordered_surface[n,0], ordered_surface[n,1], ordered_surface[n,2]))

        # Visually check ordered surface for errors #
        if plotCheck:
            import plot_define as pd

            plot = pd.plot_define(proj3D=True)
            plt, fig, ax = plot.plt, plot.fig, plot.ax

            X_surf = ordered_surface[:,0] * np.cos(ordered_surface[:,1])
            Y_surf = ordered_surface[:,0] * np.sin(ordered_surface[:,1])
            Z_surf = ordered_surface[:,2]

            for i in range(zeta_dom.shape[0]-1):
                begIdx = int(i * zeta_pnts)
                endIdx = int((i + 1) * zeta_pnts)

                X_plot = X_surf[begIdx:endIdx]
                Y_plot = Y_surf[begIdx:endIdx]
                Z_plot = Z_surf[begIdx:endIdx]

                ax.scatter(X_plot, Y_plot, Z_plot)

            ax.set_zlim(np.min(X_surf), np.max(X_surf))
            plt.show()

    def input_VMEC(self, template, descur, output, phiEdge=0.039418):
        """ Generate VMEC input from template, with the boundary provided by
        the specified DESCUR file.

        Parameters
        ----------
        template : str
            Absolute path to the template VMEC input.
        descur : str
            Absolute path to the DESCUR output to pull boundary data from.
        output : str
            Absolute path to the VMEC input that will be generated.
        psiEdge : float, Optional
            Toroidal magnetic flux at plasma boundary. Default is 4pi^2
        """
        read_segment = False

        mode_num = []
        BC_amps = []
        AX_amps = []

        nfp = self.params['N']
        if nfp == 0:
            nfp = 1

        with open(descur, 'r') as my_file:
            lines = my_file.readlines()
            for line in lines:
                line = line.split()
                line_len = len(line)
                if line_len > 0:
                    if read_segment:
                        try:
                            int(line[0])

                            mode_num.append([line[0], line[1]])
                            BC_amps.append([line[2], line[5]])
                            if line_len > 6:
                                AX_amps.append([line[6], line[7]])
                        except ValueError:
                            read_segment = False
                    if line[0] == 'MB' and line_len > 3:
                        read_segment = True

        with open(template, 'r') as my_file:
            lines = my_file.readlines()

            new_lines = []
            for line in lines:
                line_split = line.split()

                if line_split[0] == 'NFP':
                    new_lines.append('  NFP = {}\n'.format(nfp))
                elif line_split[0] == 'PHIEDGE':
                    new_lines.append('  PHIEDGE = {}\n'.format(phiEdge))
                elif line_split[0] == 'RAXIS':
                    new_lines.append('  RAXIS = ' + ' '.join([Ax[0] for Ax in AX_amps]) + '\n')
                elif line_split[0] == 'ZAXIS':
                    new_lines.append('  ZAXIS = ' + ' '.join([Ax[1] for Ax in AX_amps]) + '\n')
                elif line_split[0] == 'RBC(00,00)':
                    for idx, BC in enumerate(BC_amps):
                        RBC, ZBS = BC[0], BC[1]
                        pol_mode, tor_mode = mode_num[idx][1], mode_num[idx][0]
                        new_lines.append('  RBC( {0},{1}) = {2}  ZBS( {0},{1}) = {3}\n'.format(pol_mode, tor_mode, RBC, ZBS))
                    break
                else:
                    new_lines.append(line)

        with open(output, 'w') as my_file:
            for line in new_lines:
                my_file.write(line)
            my_file.write('/\n&END')

    def boundary_comparison(self, theta_dom, zeta_dom, compFile, saveName=None):
        wout = wr.readWout(compFile)
        wout.transForm_2D_sSec(0.5, theta_dom, zeta_dom, ['R', 'Z'])

        R_QHS = wout.invFourAmps['R']
        Z_QHS = wout.invFourAmps['Z']

        # Construct Plot #
        plt.close('all')

        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': 18}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = 22
        mpl.rcParams['lines.linewidth'] = 2

        fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(10, 8))
        ax.set_aspect('equal')

        for vdx, v in enumerate(zeta_dom):
            if vdx+1 == len(zeta_dom):
                R_vals = self.R_boundary(theta_dom, v)
                Z_vals = self.Z_boundary(theta_dom, v)

                ax.plot(R_vals, Z_vals, c='k', label='Model')
                ax.plot(R_QHS[vdx, :], Z_QHS[vdx, :], c='tab:red', label='HSX')
            else:
                R_vals = self.R_boundary(theta_dom, v)
                Z_vals = self.Z_boundary(theta_dom, v)

                ax.plot(R_vals, Z_vals, c='k')
                ax.plot(R_QHS[vdx, :], Z_QHS[vdx, :], c='tab:red')

        ax.set_title(r'$\kappa = $'+'{0:0.1f}'.format(self.params['kappa']))

        ax.set_xlabel('R [m]')
        ax.set_ylabel('Z [m]')

        plt.grid()
        plt.legend(loc='lower center', fontsize=16)

        if saveName is None:
            plt.show()
        else:
            plt.savefig(saveName)

    def fieldline_following(self, theta_init, zeta_init, dl, nstep):
        '''
        use the a leap-frog routine for field-line tracing
        inputs: xyz_start   - start position in XYZ
                dl          - step size
                nstep       - number of steps
                coil_pos    - 3d vector containing the XYZ points of each coil.
                currents    - 1d vector with the currents per coil
        '''
        # define arrays to store the outputs #
        point_arr = np.zeros((nstep+1, 2))
        Babs_arr = np.zeros(nstep+1)

        # get B and babs for the start-position #
        B = self.get_Bvec(theta_init, zeta_init)
        babs = self.modB_boundary(theta_init, zeta_init)

        point_arr[0] = np.array([theta_init, zeta_init])
        Babs_arr[0] = babs
        for i in range(nstep):
            # ----------------------------------------------------
            # get the magnetic field vector at the "half way position"
            # ----------------------------------------------------
            half_stp = point_arr[i] + B/Babs_arr[i] * 0.5 * dl
            B = self.get_Bvec(half_stp[0], half_stp[1])
            babs = self.modB_boundary(theta_init, zeta_init)
            point_arr[i+1] = point_arr[i] + (B/babs*dl)
            Babs_arr[i+1] = babs

        return(point_arr)


if __name__ == '__main__':
    params = {'N': 10,
              'Ro': 1.2,
              'a': 0.06,
              'Delta': 0.06,
              'iota': 1.0}

    if False:
        # Plot Boundary Comparison to HSX #
        params['kappa'] = 1.5
        params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['a']

        model = Local_Boundary(params)

        theta_dom = np.linspace(0, 2*np.pi, 150, endpoint=True)
        zeta_dom = np.pi * np.linspace(0, 2/params['N'], 5, endpoint=True)

        compFile = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0')
        saveName = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'PT_Turbo', '2022', '2022_01', '2022_01_21', 'HSX_comp_chrisModel.png')
        model.boundary_comparison(theta_dom, zeta_dom, compFile)

    if True:
        # Generate VTK File of ModB Surface #
        model = 'negRotModel'
        dirc = '3D_negRotModel_kScan'
        scan = 'scan_1'

        params = {}
        param_file = os.path.join('/home', 'michael', 'Desktop', dirc, scan, 'parameter_list.txt')
        with open(param_file, 'r') as f:
            lines = f.readlines()
            for line in lines:
                line_split = line.strip().split()
                if line_split[0] != 'rho' and line_split[0] != 'kappa':
                    params[line_split[0]] = float(line_split[2])

                elif line_split[0] == 'rho':
                    cnt = len(line_split[2::])
                    rho = np.empty(cnt)
                    for i, item in enumerate(line_split[2::]):
                        if i == 0:
                            rho[i] = float(item[1:-1])
                        else:
                            rho[i] = float(item[0:-1])

                elif line_split[0] == 'kappa':
                    cnt = len(line_split[2::])
                    kappa = np.empty(cnt)
                    for i, item in enumerate(line_split[2::]):
                        if i == 0:
                            kappa[i] = float(item[1:-1])
                        else:
                            kappa[i] = float(item[0:-1])

        for i in range(cnt):
            params['rho'] = rho[i]
            params['kappa'] = kappa[i]

            # Elongation Label #
            k_int = int(kappa[i] * 1e2)
            if k_int == 200:
                k_tag = 'k2p00'
            elif k_int > 100:
                k_tag = 'k1p{}'.format(k_int - 100)
            elif k_int == 100:
                k_tag = 'k1p00'
            else:
                k_tag = 'k0p{}'.format(k_int)

            savePath = os.path.join('/home', 'michael', 'Desktop', dirc, scan, k_tag)
            saveName = 'modB_'+k_tag+'.vtk'
            saveFLF = 'flf_theta_pi_zeta_0.vtk'

            npts = 101
            theta_dom = np.linspace(-np.pi, np.pi, npts)
            zeta_dom = np.linspace(-np.pi, np.pi, int(npts*params['N']))

            md = Local_Boundary(params, model)
            # md.make_boundary_VTK(savePath, saveName, theta_dom, zeta_dom)

            if False:
                # Plot Field Line #
                flf_points = md.fieldline_following(np.pi, 0, 0.01, 10)
                R_pnts = md.R_boundary(flf_points[:, 0], flf_points[:, 1])
                Z_pnts = md.Z_boundary(flf_points[:, 0], flf_points[:, 1])

                X_pnts = R_pnts * np.cos(flf_points[:, 1])
                Y_pnts = R_pnts * np.sin(flf_points[:, 1])

                cart_pnts = np.stack((X_pnts, Y_pnts, Z_pnts), axis=1)
                scal_pnts = np.ones(X_pnts.shape)

                #vtkG.scalar_line_mesh(savePath, saveFLF, cart_pnts, scal_pnts)

    if False:
        path = os.path.join('/home', 'michael', 'Desktop', '3D_negRotModel')

        params['kappa'] = 1.5
        # params['rho'] = params['a'] / np.sqrt(1 + 0.5*(params['kappa']**2 - 1))
        # params['rho'] = np.sqrt(2 / (5*params['kappa']**2 - 4*params['kappa'] +1)) * params['a']
        params['rho'] = 0.15  # np.sqrt(2 / (params['kappa']**2 + 1)) * params['a']

        model = Local_Boundary(params)

        savePath = path
        saveName = 'boundary_k1p50_N8.vtk'

        theta_dom = np.linspace(-np.pi, np.pi, 251)
        zeta_dom = np.linspace(-np.pi, np.pi, 251)
        model.make_boundary_VTK(savePath, saveName, theta_dom, zeta_dom)

    if False:
        path = os.path.join('/home', 'michael', 'Desktop', '3D_negRotModel_kScan', 'scan_1')
        if not os.path.isdir(path):
            os.mkdir(path)

        kappa_dom = np.linspace(0.5, 2, 7)
        rho_dom = np.empty(kappa_dom.shape)

        # Elongation Label #
        k_tags = []
        for k_val in kappa_dom:
            k_int = int(k_val * 1e2)
            if k_int == 200:
                k_tags.append('k2p00')
            elif k_int > 100:
                k_tags.append('k1p{}'.format(k_int - 100))
            elif k_int == 100:
                k_tags.append('k1p00')
            else:
                k_tags.append('k0p{}'.format(k_int))

        for kdx, kappa in enumerate(kappa_dom):
            kPath = os.path.join(path, k_tags[kdx])
            if not os.path.isdir(kPath):
                os.mkdir(kPath)

            params['kappa'] = kappa
            # params['rho'] = params['a'] / np.sqrt(1 + 0.5*(params['kappa']**2 - 1))  # Tokamak Model
            params['rho'] = np.sqrt(2 / (kappa**2 + 1)) * params['a']  # Chris' Model and HSX Model
            rho_dom[kdx] = params['rho']

            model = Local_Boundary(params)

            savePath = os.path.join(kPath, 'lcfs_'+k_tags[kdx]+'.txt')
            if params['N'] == 0:
                model.descur_input(savePath, 20, 20, plotCheck=False)
            else:
                model.descur_input(savePath, 20, int(params['N']*20), plotCheck=False)

        metaFile = os.path.join(path, 'parameter_list.txt')
        model.save_model_data(metaFile, rho_dom, kappa_dom)

    if False:
        path = os.path.join('/home', 'michael', 'Desktop', '3D_negRotModel_kScan', 'scan_1')

        model = Local_Boundary(params)

        template = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'input.3D_model')
        dircs = [f.name for f in os.scandir(path) if os.path.isdir(f)]
        for dirc in dircs:
            descur = os.path.join(path, dirc, 'outcurve')
            output = os.path.join(path, dirc, 'input.3D_model_'+dirc)
            model.input_VMEC(template, descur, output, phiEdge=0.05)
