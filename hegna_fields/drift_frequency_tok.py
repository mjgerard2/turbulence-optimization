import numpy as np
import field_functions_tok as ff

from scipy.special import ellipk
from scipy.special import ellipe

import sys, os
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd


def omega_drift(M, params, analytic=True):
    if analytic:
        Vprime = params['normalization']
        rho = params['rho']
        Ro = params['Ro']
        q = 1. / params['iota']
        k = params['kappa']
        sigma = params['sigma']
        alpha = params['alpha']

        M2 = M**2

        omD_cf = Vprime / (2*np.pi*np.pi*rho*Ro*Ro)

        omD_cf1 = (4./q) * (Vprime / (4*np.pi*np.pi)) * (((k**2 + 1) / k**2) - ((q*Ro)/k) * sigma)
        omD_1 = (ellipe(M2)/ellipk(M2)) + M2 - 1

        omD_cf2 = (4*k/q) * ((5 - k*k) / (3 + k*k))
        omD_2 = (ellipe(M2)/ellipk(M2)) - 0.5

        omD_cf3 = -(rho*sigma/9.) * ((k*k+1)/(k*k)) * (5 - k*k) * (k*k + 3)
        omD_3 = (ellipe(M2)/ellipk(M2)) * (2*M2 - 1) + 1 - M2

        omD_4 = -alpha/(q*q)

        omD = omD_cf * (omD_cf1*omD_1 + omD_cf2*omD_2 + omD_cf3*omD_3 + omD_4)
        return omD

    else:
        iota = params['iota']
        v_par, theta_dom = ff.v_par(M, params)

        Kn = ff.Kn(theta_dom, params)
        Kg = ff.Kg(theta_dom, params)
        B = ff.B(theta_dom, params)
        Grad_Psi = ff.Grad_Psi(theta_dom, params)
        jacob = ff.jacob(theta_dom, params)
        D = ff.D(theta_dom, params)
        shear = ff.shear(params)

        integ_scale = jacob * B / v_par

        dJ_dPsi = v_par**2 * (shear / iota)
        dJ_dPsi = dJ_dPsi + (Kn / Grad_Psi)
        dJ_dPsi = dJ_dPsi + (D * Kg * Grad_Psi / B)
        dJ_dPsi = integ_scale * dJ_dPsi
        dJ_dPsi = np.trapz(dJ_dPsi, theta_dom)

        dJ_dK = np.trapz(integ_scale, theta_dom)
        if dJ_dK == 0:
            omega_D = np.nan
        else:
            omega_D = - dJ_dPsi / dJ_dK
        #    print('{0:0.2f} : {1}'.format(params['kappa'], M))
        #    print('   '+str(dJ_dK))
        #    dJ_dK = 1e-6
        # integ_chk = np.trapz(jacob, theta_dom)
        # print('({}, {}, {})'.format(M, dJ_dPsi, dJ_dK))

        return omega_D


params = {'Deta': 1e-4,
          'Ro': 1.5,
          'iota': 0.5,
          'minor': 0.15,
          'normalization': 4*np.pi**2,
          'sigma': 0.5,
          'alpha': 1}

Mpts = 250
Mstp = 1 / Mpts
M_dom = np.linspace(Mstp, 1, Mpts, endpoint=False)

kappa_dom = np.linspace(0.8, 1.4, 4)
omega_set = np.empty((kappa_dom.shape[0], M_dom.shape[0]))

for idx, kappa in enumerate(kappa_dom):
    print(kappa)
    params['kappa'] = kappa
    params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['minor']
    for Mdx, M in enumerate(M_dom):
        omega_D = omega_drift(M, params, analytic=False)
        omega_set[idx, Mdx] = omega_D

if True:
    plot = pd.plot_define(lineWidth=2, figSize=(10, 6))
    plt = plot.plt

    plt.plot([0, 1], [0, 0], c='k', ls='--')
    for kdx, kappa in enumerate(kappa_dom):
        plt1, = plt.plot(M_dom, omega_set[kdx], label=r'$\kappa = $'+'{0:0.3f}'.format(kappa))

    plt.xlabel(r'$M$')
    plt.ylabel(r'$\frac{1}{k_{\alpha}} \frac{|e|}{K} \langle\omega_{De}\rangle$')

    plt.xlim(0, 1)

    box = plot.ax.get_position()
    plot.ax.set_position([box.x0, box.y0, box.width * 1, box.height])
    plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1), fontsize=14)

    plt.tight_layout()
    plt.grid()

    plt.show()
    saveName = os.path.join('/home','michael','onedrive','Figures','3D_Drift_Model','omega_drift_tok_exact.png')
    # plt.savefig(saveName)
