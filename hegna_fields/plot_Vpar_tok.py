import numpy as np
import h5py as hf
import field_functions_tok as ff

from scipy.interpolate import interp1d

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs
from matplotlib.animation import FuncAnimation

import sys, os
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd


def v_parallel(M, theta_dom, params):
    nu, theta_grid = ff.v_par(M, params)

    theta_grid = np.r_[-np.flip(theta_grid[1::]), theta_grid]
    nu_grid = np.r_[np.flip(nu[1::]), nu]

    Vpar_dir_V_fit = interp1d(theta_grid, nu_grid)

    num_beg = theta_dom[theta_dom < theta_grid[0]].shape[0]
    num_end = theta_dom[theta_dom > theta_grid[-1]].shape[0]

    zero_beg = np.zeros(num_beg)
    zero_end = np.zeros(num_end)
    theta_samp = theta_dom[num_beg:(theta_dom.shape[0] - num_end)]

    Vpar_fit = np.r_[zero_beg, Vpar_dir_V_fit(theta_samp), zero_end]
    return Vpar_fit


params = {'Deta': 1e-3,
          'Ro': 1.5,
          'iota': 1./3,
          'minor': 0.15,
          'alpha': 1,
          'sigma': 0.5,
          'normalization': 4*np.pi**2}

Mpts = 10
M_dom = np.linspace(0.1, 1, Mpts)
kappa_dom = np.array([0.5, 1, 2])

theta_pnts = 501
theta_dom = np.linspace(-np.pi, np.pi, theta_pnts)

v_par_set = np.empty((kappa_dom.shape[0], M_dom.shape[0], theta_pnts))
for idx, kappa in enumerate(kappa_dom):
    params['kappa'] = kappa
    params['rho'] = np.sqrt(2 / (params['kappa']**2 + 1)) * params['minor']
    for Mdx, M in enumerate(M_dom):
        v_par_set[idx, Mdx] = v_parallel(M, theta_dom, params)

if True:
    # Single Plot #
    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 18}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2

    names = ['tok_vPar_kLow.png', 'tok_vPar_kMid.png', 'tok_vPar_kHgh.png']
    for kdx, kappa in enumerate(kappa_dom):
        fig = plt.figure(constrained_layout=True, figsize=(8, 6))
        spec = gs.GridSpec(3, 1, figure=fig)

        ax1 = fig.add_subplot(spec[1:3, :])
        ax2 = fig.add_subplot(spec[0, :], sharex=ax1)
        plt.setp(ax2.get_xticklabels(), visible=False)
        ax3 = ax2.twinx()

        ax3.spines['right'].set_color('tab:red')
        ax3.tick_params(axis='y', colors='tab:red')

        params['kappa'] = kappa_dom[kdx]
        B_mod = ff.B(theta_dom, params)
        K_norm = ff.Kn(theta_dom, params)
        Grad_Psi = ff.Grad_Psi(theta_dom, params)
        D = ff.D(theta_dom, params)
        K_geod = ff.Kg(theta_dom, params)
        shear = ff.shear(params)

        integ_value = K_norm / Grad_Psi
        integ_value = integ_value + D * K_geod * Grad_Psi / B_mod

        integ_max = np.max(integ_value)
        integ_min = np.min(integ_value)
        if integ_max >= np.abs(integ_min):
            integ_absMax = 1.1 * integ_max
        else:
            integ_absMax = 1.1 * np.abs(integ_min)

        theta_norm = theta_dom / np.pi
        clrs = ['#4e79a7', '#f28e2b', '#e15759', '#76b7b2', '#59a14f', '#edc948', '#b07aa1', '#ff9da7', '#9c755f', '#bab0ac']

        for Mdx, M in enumerate(np.flip(M_dom)[0:-1]):
            key = 'line '+str(Mdx)
            mdx = M_dom.shape[0] - 1 - Mdx
            ax1.fill_between(theta_norm, v_par_set[kdx, mdx-1, :], v_par_set[kdx, mdx, :], color=clrs[Mdx])
        ax1.fill_between(theta_norm, 0, v_par_set[kdx, 0, :], color=clrs[Mdx+1])

        ax2.plot(theta_norm, B_mod, c='k', linewidth=4)
        ax3.plot(theta_norm, integ_value, c='tab:red', linewidth=4)
        ax3.plot(theta_norm, np.zeros(theta_norm.shape), ls='--', c='tab:red')

        neg_idx = np.where(integ_value < 0)[0]
        neg_bool = ((neg_idx - np.roll(neg_idx, 1)) != 1)
        negDX = np.where(neg_bool)[0]

        alpha_clr = 0.3
        for i, idx in enumerate(negDX[0:-1]):
            theta_beg = neg_idx[idx]
            theta_end = neg_idx[negDX[i+1] - 1] + 1

            ax1.axvspan(theta_norm[theta_beg], theta_norm[theta_end], alpha=alpha_clr, color='tab:red')
            ax2.axvspan(theta_norm[theta_beg], theta_norm[theta_end], alpha=alpha_clr, color='tab:red')

        if len(negDX) > 0:
            theta_beg = neg_idx[negDX[-1]]
            theta_end = neg_idx[-1]
            ax1.axvspan(theta_norm[theta_beg], theta_norm[theta_end], alpha=alpha_clr, color='tab:red')
            ax2.axvspan(theta_norm[theta_beg], theta_norm[theta_end], alpha=alpha_clr, color='tab:red')

        ax1.set_xlabel(r'$\theta\, (\pi)$')
        # ax1.set_ylabel(r'$v_{\parallel} \, (a.u.)$')
        ax1.set_ylabel(r'$v_{\parallel} / v$')

        ax2.set_ylabel(r'$|B|$')
        # ax3.set_ylabel(r'$\kappa_n$', c='tab:red')
        # ax3.set_ylabel(r'$\frac{ \kappa_n }{ |\nabla \psi| } + D \kappa_g \frac{ |\nabla \psi| }{B}$', c='tab:red')
        ax3.set_ylabel(r'$\kappa_D$', c='tab:red')

        ax1.set_xlim(-1, 1)
        ax1.set_ylim(0, 1.1 * np.max(v_par_set[kdx]))
        ax2.set_ylim(0.99*np.min(B_mod), 1.01*np.max(B_mod))
        ax3.set_ylim(-integ_absMax, integ_absMax)
        ax2.grid()

        plt.title(r'$\kappa = $'+'{0:0.3f}'.format(kappa_dom[kdx]))

        saveName = os.path.join('/home', 'michael', 'onedrive', 'Figures', '3D_Drift_Model', names[kdx])
        # plt.savefig(saveName)
        plt.show()

if False:
    # Make Animation #
    plot = pd.plot_define(lineWidth=2)
    plt, fig, ax1 = plot.plt, plot.fig, plot.ax
    ax2 = ax1.twinx()

    ax2.spines['right'].set_color('tab:red')
    ax2.tick_params(axis='y', colors='tab:red')

    ax2.set_ylabel(r'$\kappa_n$', c='tab:red')
    ax1.set_xlabel(r'$\eta \ [\pi]$')
    ax1.set_ylabel(r'$|B|$')

    ax1.set_xlim(-1, 1)

    plt.tight_layout()

    eta_norm = eta_dom / np.pi

    fill_dict = {}
    fill_clrs = {}
    clrs = ['#4e79a7', '#f28e2b', '#e15759', '#76b7b2', '#59a14f', '#edc948',
            '#b07aa1', '#ff9da7', '#9c755f', '#bab0ac']

    params['kappa'] = kappa_dom[0]
    B_mod = B(eta_dom, params)
    K_norm = Kn(eta_dom, params)

    v_par_set[0] = (v_par_set[0] / np.max(v_par_set[0])) * np.max(B_mod)
    for Mdx, M in enumerate(np.flip(M_dom)[0:-1]):
        key = 'fill '+str(Mdx)
        mdx = M_dom.shape[0] - 1 - Mdx
        fill_dict[key] = ax1.fill_between(eta_norm, v_par_set[0, mdx-1], v_par_set[0, mdx])
        fill_clrs[key] = clrs[Mdx]
    ax1.fill_between(eta_norm, 0, v_par_set[0, 0, :], color=clrs[9])
    fill_clrs['fill 9'] = clrs[Mdx]

    line_B, = ax1.plot([], [], c='k', lw=4)
    line_Kn, = ax2.plot([], [], c='tab:red', lw=4)

    title = ax1.text(0.7,0.85, "", bbox={'facecolor':'k', 'alpha':0.25, 'pad':5},
                    transform=ax1.transAxes, ha="center")

    def init():
        line_B.set_data([], [])
        line_Kn.set_data([], [])

        title.set_text("")

        return line_B, line_Kn, title,

    def animate(i):
        params['kappa'] = kappa_dom[i]
        B_mod = B(eta_dom, params)
        K_norm = Kn(eta_dom, params)

        K_max = np.max(K_norm)
        K_min = np.min(K_norm)
        if K_max >= np.abs(K_min):
            K_absMax = K_max
        else:
            K_absMax = np.abs(K_min)

        ax1.set_ylim(0, 1.5*np.max(B_mod))
        ax2.set_ylim(-K_absMax, K_absMax)

        line_B.set_data(eta_norm, B_mod)
        line_Kn.set_data(eta_norm, K_norm)

        v_par_set[i] = (v_par_set[i] / np.max(v_par_set[i])) * np.max(B_mod)
        for Mdx, M in enumerate(np.flip(M_dom)[0:-1]):
            key = 'fill '+str(Mdx)
            fill_dict[key].remove()
            mdx = M_dom.shape[0] - 1 - Mdx
            fill_dict[key] = ax1.fill_between(eta_norm, v_par_set[i, mdx-1], v_par_set[i, mdx], color=fill_clrs[key])
        ax1.fill_between(eta_norm, 0, v_par_set[i, 0, :], color=fill_clrs['fill 9'])

        title.set_text(r'$\kappa = $'+'{0:0.3f}'.format(kappa_dom[i]))

        return line_B, line_Kn, title

    anim = FuncAnimation(fig, animate, init_func=init, frames=100, interval=50, blit=True)
    anim.save('/home/michael/onedrive/full_kappa_range.gif', writer='imagemagick')
