import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

from vmecTools.wout_files import booz_read

nfp = 4
scan = 'scan_13'
dirc = '3D_model_kScan'

k_vals = np.linspace(0.5, 2.0, 7)
booz_dirc = os.path.join('/home', 'michael', 'Desktop', dirc, scan)

# Elongation Label #
k_tags = []
for k_val in k_vals:
    k_int = int(k_val * 1e2)
    if k_int == 200:
        k_tags.append('k2p00')
    elif k_int > 100:
        k_tags.append('k1p{}'.format(k_int - 100))
    elif k_int == 100:
        k_tags.append('k1p00')
    else:
        k_tags.append('k0p{}'.format(k_int))

# Get Boozer Modes #
mode_brk = [[nfp, 2], [nfp, 0], [0, 1]]
mode_prs = [[nfp, 1], [2*nfp, 2], [3*nfp, 3]]
modes_brk = np.empty((len(k_tags), len(mode_brk)))
modes_prs = np.empty((len(k_tags), len(mode_prs)))
for idx, k_tag in enumerate(k_tags):
    booz_path = os.path.join(booz_dirc, k_tag, 'boozmn_wout_3D_model_'+k_tag+'.nc')
    booz = booz_read.readBooz(booz_path)

    for mdx, mode in enumerate(mode_brk):
        modes_brk[idx, mdx] = booz.sym_break_modes(booz.s_dom[-1], mode[0], mode[1], symNum=nfp)
        modes_prs[idx, mdx] = booz.sym_preserve_modes(booz.s_dom[-1], mode_prs[mdx][0], mode_prs[mdx][1], symNum=nfp)

# Construct Breaking Mode Plot #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 16}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['lines.linewidth'] = 2

gam_idx = 1
ms = ['s', 'o', '^']
fc = ['tab:blue', 'tab:orange', 'tab:red']
for mdx, mode in enumerate(mode_brk):
    mode_key = ', '.join(['{0:0.0f}'.format(m) for m in mode])
    plt.plot(k_vals, modes_brk[:, mdx], c='k', ls='--', markerfacecolor=fc[mdx], marker=ms[mdx], markersize=15, label=mode_key)

plt.xlabel(r'$\kappa$')
plt.ylabel(r'$B_{nm} \left( \sum_{n \neq 4m} B_{nm}^2 \right)^{-1/2}$')

# plt.xlim(0.96, 1.076)
plt.ylim(-1, 1)

plt.legend()
plt.grid()
plt.tight_layout()

save_path = os.path.join('/home', 'michael', 'Desktop', dirc, scan, 'kappa_scan_NoSym.png')
plt.savefig(save_path)
# plt.show()

# Construct Preserving Mode Plot #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 16}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['lines.linewidth'] = 2

gam_idx = 1
ms = ['s', 'o', '^']
fc = ['tab:blue', 'tab:orange', 'tab:red']
for mdx, mode in enumerate(mode_prs):
    mode_key = ', '.join(['{0:0.0f}'.format(m) for m in mode])
    plt.plot(k_vals, modes_prs[:, mdx], c='k', ls='--', markerfacecolor=fc[mdx], marker=ms[mdx], markersize=15, label=mode_key)

plt.xlabel(r'$\kappa$')
plt.ylabel(r'$B_{nm} \left( \sum_{n = 4m} B_{nm}^2 \right)^{-1/2}$')

# plt.xlim(0.96, 1.076)
plt.ylim(-1, 1)

plt.legend()
plt.grid()
plt.tight_layout()

save_path = os.path.join('/home', 'michael', 'Desktop', dirc, scan, 'kappa_scan_Sym.png')
plt.savefig(save_path)
# plt.show()
