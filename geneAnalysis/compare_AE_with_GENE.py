import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Read in Growth Rates #
hf_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')
with hf.File(hf_path, 'r') as hf_file:
    tem_data = np.full((100, 7, 3), np.nan)
    for m, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        tem_data[m] = hf_file[conID][()]

# Read in Available Energy Data #
omn = 2
omt = 2
ae_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'AE_data.h5')
with hf.File(ae_path, 'r') as hf_:
    omn_vals = hf_['omn_vals'][()]
    omt_vals = hf_['omt_vals'][()]

    omn_idx = np.argmin(np.abs(omn_vals - omn))
    omt_idx = np.argmin(np.abs(omt_vals - omt))

    ae_data = np.empty(100)
    as_data = np.empty(100)
    for m, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        ae_data[m] = hf_[conID+'/Available Energy'][omn_idx, omt_idx]
        as_data[m] = hf_[conID+'/Available SINK'][omn_idx, omt_idx]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22

# Plotting Axis #
fig, axs = plt.subplots(1, 3, sharey=True, tight_layout=True, figsize=(16, 6))

axs[0].scatter(ae_data, tem_data[:, 0, 1], facecolor='None', edgecolor='tab:blue', marker='v', s=150, label='Aval. Energy')
axs[0].scatter(ae_data, tem_data[:, 1, 1], facecolor='None', edgecolor='tab:orange', marker='o', s=150, label='Aval. Energy')
axs[0].scatter(ae_data, tem_data[:, 2, 1], facecolor='None', edgecolor='tab:green', marker='s', s=150, label='Aval. Energy')
axs[0].scatter(ae_data, tem_data[:, 3, 1], facecolor='None', edgecolor='tab:red', marker='^', s=150, label='Aval. Energy')

axs[1].scatter(as_data, tem_data[:, 0, 1], facecolor='None', edgecolor='tab:blue', marker='s', s=150, label='Avail. Sink')
axs[1].scatter(as_data, tem_data[:, 1, 1], facecolor='None', edgecolor='tab:orange', marker='o', s=150, label='Avail. Sink')
axs[1].scatter(as_data, tem_data[:, 2, 1], facecolor='None', edgecolor='tab:green', marker='s', s=150, label='Avail. Sink')
axs[1].scatter(as_data, tem_data[:, 3, 1], facecolor='None', edgecolor='tab:red', marker='^', s=150, label='Avail. Sink')

axs[2].scatter(ae_data-as_data, tem_data[:, 0, 1], facecolor='None', edgecolor='tab:blue', marker='v', s=150, label='Aval. Energy')
axs[2].scatter(ae_data-as_data, tem_data[:, 1, 1], facecolor='None', edgecolor='tab:orange', marker='o', s=150, label='Aval. Energy')
axs[2].scatter(ae_data-as_data, tem_data[:, 2, 1], facecolor='None', edgecolor='tab:green', marker='s', s=150, label='Aval. Energy')
axs[2].scatter(ae_data-as_data, tem_data[:, 3, 1], facecolor='None', edgecolor='tab:red', marker='^', s=150, label='Aval. Energy')

axs[0].set_ylabel(r'$\gamma \ (c_s/a)$')
axs[0].set_xlabel(r'$\hat{\mathrm{AE}}$')
axs[1].set_xlabel(r'$\hat{\mathrm{AS}}$')
axs[2].set_xlabel(r'$\hat{\mathrm{AE}} - \hat{\mathrm{AS}}$')

axs[0].set_ylim(0, 1.05*np.nanmax(tem_data[:, 0:4, 1]))

axs[0].grid()
axs[1].grid()
axs[2].grid()

# plt.legend()
plt.show()
save_path = os.path.join('available_energy_linear.png')
# plt.savefig(save_path)
