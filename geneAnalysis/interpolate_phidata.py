import os
import h5py as hf
import numpy as np

import scipy.interpolate as spi


Ninterp = 3
base_dirc = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0')
phi_path = os.path.join(base_dirc, 'phidata_00_10.h5')
new_phi_path = os.path.join(base_dirc, 'phidata_interp{}_00_10.h5'.format(Ninterp))

with hf.File(phi_path, 'r') as hf_:
    print('initializing with \"time 0\"')
    t_slice = hf_['time 0'][()]

    Nz = t_slice.shape[0] * (Ninterp + 1) - Ninterp
    Ny = t_slice.shape[1]
    Nx = t_slice.shape[2]

    z_dom = np.linspace(-1, 1, t_slice.shape[0])
    z_interp = np.linspace(-1, 1, Nz)

    t_slice_new = np.empty((Nz, Ny, Nx))
    for i in range(Ny):
        for j in range(Nx):
            phi_model = spi.interp1d(z_dom, t_slice[:, i, j])
            t_slice_new[:, i, j] = phi_model(z_interp)

    with hf.File(new_phi_path, 'a') as hf_new:
        hf_new.create_dataset('time 0', data=t_slice_new)

    tpts = len(hf_)
    for i in range(1, tpts):
        print('({}|{})'.format(i+1, tpts))
        time_key = 'time {}'.format(i)
        t_slice = hf_[time_key][()]

        t_slice_new = np.empty((Nz, Ny, Nx))
        for i in range(Ny):
            for j in range(Nx):
                phi_model = spi.interp1d(z_dom, t_slice[:, i, j])
                t_slice_new[:, i, j] = phi_model(z_interp)

        with hf.File(new_phi_path, 'a') as hf_new:
            hf_new.create_dataset(time_key, data=t_slice_new)
