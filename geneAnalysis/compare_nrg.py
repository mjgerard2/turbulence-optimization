import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

import read_nrg_summary as rns


# User Input #
nrg_path_1 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_1_3.h5')
nrg_path_2 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_21_22.h5')
# nrg_path_3 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'nrgsummarye_26_44.h5')

# save_name = None
save_name = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'figures', 'v2_vs_v3_nrg.pdf')
tbeg = [150, 150]
lbs = [r'Gene 2.0 (CPU)', r'Gene 3.0 (GPU)']

nrg_path_list = [nrg_path_1, nrg_path_2]
###############################################################################

nrg_data_list = []
nrg_data_avg = []
nrg_data_std = []

t_end = []
for ndx, nrg_path in enumerate(nrg_path_list):
    nrg = rns.nrg_summary(nrg_path)
    if ndx == 1:
        nrg.nrg_average(tbeg[ndx], end_time=t_end[0])
    else:
        nrg.nrg_average(tbeg[ndx])
    time_series = np.stack((nrg.time_domain, nrg.time_series['Q_tot']), axis=0)

    nrg_data_list.append(time_series)
    nrg_data_avg.append(nrg.time_average['Q_tot'])
    nrg_data_std.append(nrg.time_std['Q_tot'])
    t_end.append(nrg.time_domain[-1])

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24

# Plotting Axis #
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

tmax = []
Qmax = []

for i, nrg_data in enumerate(nrg_data_list):
    time = nrg_data[0]
    flux = nrg_data[1]

    avg = nrg_data_avg[i]
    std = nrg_data_std[i]

    lab = lbs[i]+r'$\ \left( \langle Q_{{\mathrm{{es}}}} \rangle_{{t \geq {0:0.0f}}} = {1:0.2f} \right)$'.format(tbeg[i], avg)

    plot, = ax.plot(time[time < tbeg[i]], flux[time < tbeg[i]], alpha=0.35)
    ax.plot(time[time >= tbeg[i]], flux[time >= tbeg[i]], label=lab, c=plot.get_color())
    idx = np.argmin(np.abs(time - tbeg[i]))
    # ax.scatter(time[idx], flux[idx], marker='o', s=100, c=plot.get_color(), edgecolor='k', zorder=10)

    tmax.append(np.max(nrg_data[0]))
    Qmax.append(np.max(nrg_data[1]))

tmax = tmax[0]# max(tmax)
Qmax = 1.5 * max(Qmax)

# Axis Limits #
ax.set_xlim(0, tmax)
ax.set_ylim(0, ax.get_ylim()[1])

# Axis Labels #
ax.set_xlabel(r'$t \ / \ (a/c_s)$')
ax.set_ylabel(r'$\langle Q_{\mathrm{es}} \rangle$')

# axis ticks #
ax.tick_params(axis='both', which='major', direction='in', length=5)
ax.tick_params(axis='both', which='minor', direction='in', length=2.5)
ax.xaxis.set_ticks_position('default')
ax.yaxis.set_ticks_position('default')
ax.xaxis.set_minor_locator(MultipleLocator(25))
ax.yaxis.set_minor_locator(MultipleLocator(0.1))

# Axis Legend #
ax.legend(fontsize=14, frameon=False)

# Axis Grid #
# ax.grid()

# Save or Show Plot #
if save_name:
    plt.savefig(save_name, format='pdf')
else:
    plt.show()
