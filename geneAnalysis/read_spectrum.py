import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as ani


class spectrum:
    """ A class for reading GENE *.dat spectrum data files.

    ...

    Attributes
    ----------
    spec_path : str
        Global path to spectrum file.

    pram_path : str
        Global path to parameters file.

    Methods
    -------
    spectrum_average(start_time, end_time=-1)
        Calculate time average of spectral data.
    """
    def __init__(self, spec_path, pram_path):
        # Check if input files exsit #
        if not os.path.isfile(spec_path):
            raise IOError(spec_path+" does not exist!")

        if not os.path.isfile(pram_path):
            raise IOError(pram_path+' does not exist.')

        # Read in resolutions #
        with open(pram_path, 'r') as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip().split()
                if len(line) > 0:
                    if line[0] == 'nx0':
                        nx0 = int(line[2])
                    elif line[0] == 'nky0':
                        nky0 = int(line[2])
                    elif line[0] == 'kymin':
                        kymin = float(line[2])
                    elif line[0] == 'lx':
                        lx = float(line[2])
                        break

        self.nkx0 = int(0.5*nx0)+1
        self.nky0 = nky0

        self.kx_max = (2*np.pi/lx)*(self.nkx0-1)
        self.ky_max = kymin*(nky0 - 1)

        self.kx_domain = np.linspace(0, self.kx_max, self.nkx0)
        self.ky_domain = np.linspace(0, self.ky_max, self.nky0)

        # Read in spectrum file #
        with open(spec_path, 'r') as f:
            # Read in time domain data #
            lines = f.readlines()
            split_1 = lines[1].strip().split('(')
            self.nsteps = int(split_1[2].split(')')[0].split()[0])

            split_2 = split_1[1].split()[-1]
            time_0 = float(split_2.split('-')[0])
            time_f = float(split_2.split('-')[1])
            self.time_domain = np.linspace(time_0, time_f, self.nsteps)

            # Read in spectral variables #
            names = lines[7].strip().split()[2::]
            self.var_keys = [name.split(',')[0] for name in names]
            self.nvar = len(self.var_keys)
            
            # Read in spectra data #
            self.spec_data = {'kx': {},
                              'ky': {}}

            for var in self.var_keys:
                self.spec_data['kx'][var] = np.empty((self.nsteps, self.nkx0))
                self.spec_data['ky'][var] = np.empty((self.nsteps, self.nky0))
            
            for nstep in range(self.nsteps):
                for ikey, key in enumerate(['kx', 'ky']):
                    beg_idx = 8 + nstep*(self.nkx0+self.nky0+8) + ikey*(self.nkx0+3)
                    end_idx = beg_idx + (1 - ikey)*(self.nkx0) + ikey*self.nky0
                    for ldx, line in enumerate(lines[beg_idx:end_idx]):
                        line = line.strip().split()[1::]
                        data = [float(val) for val in line]
                        for vdx, var in enumerate(self.var_keys):
                            self.spec_data[key][var][nstep, ldx] = data[vdx]

    def spectrum_average(self, start_time, end_time=-1):
        """ Calculate the time average spectrum.

        Parameters
        ----------
        start_time : float
            beginning of time window over which average is calculated
        end_time : float (optional)
            end of time window over which average is calculated. Default is
            -1, which averages out to final time slice.
        """
        start_idx = np.argmin(np.abs(self.time_domain - start_time))
        if end_time == -1:
            end_idx = self.time_domain.shape[0]
        else:
            end_idx = np.argmin(np.abs(self.time_domain - end_time))+1

        time_domain = self.time_domain[start_idx:end_idx]
        t_norm = 1./(time_domain[-1] - time_domain[0])

        self.spec_data_avg = {'kx': {},
                              'ky': {}}

        for var in self.var_keys:
            self.spec_data_avg['kx'][var] = np.empty(self.nkx0)
            self.spec_data_avg['ky'][var] = np.empty(self.nky0)

        kx_spec = self.spec_data['kx']
        for var in self.var_keys:
            for i, kx in enumerate(self.kx_domain):
                time_avg = np.trapz(kx_spec[var][start_idx:end_idx, i], time_domain)*t_norm
                self.spec_data_avg['kx'][var][i] = time_avg

        ky_spec = self.spec_data['ky']
        for var in self.var_keys:
            for i, ky in enumerate(self.ky_domain):
                time_avg = np.trapz(ky_spec[var][start_idx:end_idx, i], time_domain)*t_norm
                self.spec_data_avg['ky'][var][i] = time_avg

    def spectrum_normalization(self):
        """ Calculate the normalization factor so that the normalized spectrum
        integrates to one.
        """
        self.spec_norms = {'kx': {},
                           'ky': {}}

        for var in self.var_keys:
            kx_spec = self.spec_data_avg['kx'][var]
            ky_spec = self.spec_data_avg['ky'][var]

            kx_norm = np.trapz(kx_spec, self.kx_domain)
            ky_norm = np.trapz(ky_spec, self.ky_domain)

            self.spec_norms['kx'][var] = kx_norm
            self.spec_norms['ky'][var] = ky_norm

    def spectrum_average_plot(self, ax1, ax2):
        """ Plot time averaged spectral data.

        Parameters
        ----------
        ax1 : obj
            matplotlib plotting axis. Axis on which ky spectrum is plotted.
        ax2 : obj
            matplotlib plotting axis. Axis on which kx spectrum is plotted.
        """
        ky_spec = self.spec_data_avg['ky']
        y_peak = np.empty(self.nvar)
        for i, var in enumerate(self.var_keys):
            ax1.plot(self.ky_domain, ky_spec[var], label=var)
            y_peak[i] = np.max(ky_spec[var])

        kx_spec = self.spec_data_avg['kx']
        x_peak = np.empty(self.nvar)
        for i, var in enumerate(self.var_keys):
            ax2.plot(self.kx_domain, kx_spec[var], label=var)
            x_peak[i] = np.max(kx_spec[var])

        # Axis limits #
        ax1.set_xlim(0, self.ky_max)
        ax1.set_ylim(0, ax1.get_ylim()[1])

        ax2.set_xlim(0, self.kx_max)
        ax2.set_ylim(0, ax2.get_ylim()[1])

        # Axis labels #
        ax1.set_xlabel(r'$k_y \rho_s$')
        ax2.set_xlabel(r'$k_x \rho_s$')

        # Axis legend #
        ax1.legend()
        ax2.legend()

        # Axis grid #
        ax1.grid()
        ax2.grid()

    def spectrum_average_save(self, file_path):
        """ Generate a .txt file with the kx and ky spectral data
        over the averaged time domain.

        Parameters
        ----------
        file_path : str
            Global path to where the .txt file will be saved.
        """
        with open(file_path, 'w') as f:
            # write out kx spectral data #
            f.write('kx '+' '.join(self.var_keys)+'\n')
            for i in range(self.kx_domain.shape[0]):
                kx_values = np.array([self.kx_domain[i]])
                for key in self.var_keys:
                    kx_values = np.append(kx_values, self.spec_data_avg['kx'][key][i])
                f.write(' '.join(['{}'.format(x) for x in kx_values])+'\n')

            # write out ky spectral data #
            f.write('\nky '+' '.join(self.var_keys)+'\n')
            for i in range(self.ky_domain.shape[0]):
                ky_values = np.array([self.ky_domain[i]])
                for key in self.var_keys:
                    ky_values = np.append(ky_values, self.spec_data_avg['ky'][key][i])
                f.write(' '.join(['{}'.format(x) for x in ky_values])+'\n')


if __name__ == "__main__":
    spec_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'fluxspectrae_4_14.dat')
    pram_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_4')

    spec = spectrum(spec_path, pram_path)
    spec.spectrum_average(350, end_time=1500)
    # spec.spectrum_average(2400)
    print(np.trapz(spec.spec_data_avg['kx']['Q_es'], spec.kx_domain)/(spec.kx_domain[-1]-spec.kx_domain[0]))
    """
    # Define Plotting Parameters #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 16}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 18
    mpl.rcParams['lines.linewidth'] = 3

    # Construct Plotting Axis #
    fig, axs = plt.subplots(1, 2, sharex=False, sharey=True, tight_layout=True, figsize=(12, 6))
    spec.spectrum_average_plot(axs[0], axs[1])

    plt.show()
    """
