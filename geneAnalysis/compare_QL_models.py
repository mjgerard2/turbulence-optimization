import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import geneAnalysis.read_spectrum as rSpec
import geneAnalysis.read_nrg_summary as rNrg
import geneAnalysis.QL_model as QLm
    

ky_dom = np.linspace(0.4, 2.4, 6)
base_config = 'QHS'
comp_config = 'HE-QHS'

# define global paths to  nonlinear heat flux and spectral data #
if base_config == 'QHS':
    time_dom_base = [600, -1]
    base_path_base = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0')
    nrg_path_base = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_act.h5')
    spec_path_base = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'fluxspectrae_1_act.dat')
    pram_path_base = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'parameters_1')

    # time_dom_comp = [250, -1]
    # base_path_comp = os.path.join('/mnt', 'GENE', 'linear_data', '60-1-84', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0')
    # nrg_path_comp = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_2.h5')
    # spec_path_comp = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'fluxspectrae_1_2.dat')
    # pram_path_comp = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'parameters_1')

    time_dom_comp = [2400, -1]
    base_path_comp = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_2p0_omti_0p0_omte_2p0')
    nrg_path_comp = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_00_06.h5')
    spec_path_comp = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'fluxspectrae_00_06.dat')
    pram_path_comp = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_00')

elif base_config == 'HE-QHS':
    time_dom_base = [250, -1]
    base_path_base = os.path.join('/mnt', 'GENE', 'linear_data', '60-1-84', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0')
    nrg_path_base = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_2.h5')
    spec_path_base = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'fluxspectrae_1_2.dat')
    pram_path_base = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'parameters_1')

    time_dom_comp = [600, -1]
    base_path_comp = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0')
    nrg_path_comp = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_act.h5')
    spec_path_comp = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'fluxspectrae_1_act.dat')
    pram_path_comp = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'parameters_1')

# import nonlinear nrg data # 
nrg_base = rNrg.nrg_summary(nrg_path_base)
nrg_base.nrg_average(time_dom_base[0], end_time=time_dom_base[1])
nl_heat_flux_base = nrg_base.time_average['Q_tot']

nrg_comp = rNrg.nrg_summary(nrg_path_comp)
nrg_comp.nrg_average(time_dom_comp[0], end_time=time_dom_comp[1])
nl_heat_flux_comp = nrg_comp.time_average['Q_tot']

# import nonlinear spectral data #
spec_base = rSpec.spectrum(spec_path_base, pram_path_base)
spec_base.spectrum_average(time_dom_base[0], end_time=time_dom_base[1])
ky_nl_base = spec_base.ky_domain
spec_nl_base = spec_base.spec_data_avg['ky']['Q_es']

spec_comp = rSpec.spectrum(spec_path_comp, pram_path_comp)
spec_comp.spectrum_average(time_dom_comp[0], end_time=time_dom_comp[1])
ky_nl_comp = spec_comp.ky_domain
spec_nl_comp = spec_comp.spec_data_avg['ky']['Q_es']

# construct base-case QL model #
QL1_base = QLm.QL_Data(ky_dom, eigen_base_paths, kperp1_base_paths)
QL1_base.ky_sums_wNL(nrg_path_base, spec_path_base, pram_path_base, time_dom_base, with_weights=False)

ql1_heat_flux_base = QL1_base.ql_heat_flux
ky1_wgt_sums_base = QL1_base.ky_wgt_sums
Sk1_base = QL1_base.Sk
C1_base = QL1_base.C

QL10_base = QLm.QL_Data(ky_dom, eigen_base_paths, kperp10_base_paths)
QL10_base.ky_sums_wNL(nrg_path_base, spec_path_base, pram_path_base, time_dom_base, with_weights=False)

ql10_heat_flux_base = QL10_base.ql_heat_flux
ky10_wgt_sums_base = QL10_base.ky_wgt_sums
Sk10_base = QL10_base.Sk
C10_base = QL10_base.C

# compare QL model with base-case parameters #
QL1_comp = QLm.QL_Data(ky_dom, eigen_comp_paths, kperp1_comp_paths)
QL1_comp.ky_sums_woNL(Sk1_base, C1_base, with_weights=False)
ql1_heat_flux_comp = QL1_comp.ql_heat_flux
ky1_wgt_sums_comp = QL1_comp.ky_wgt_sums

QL10_comp = QLm.QL_Data(ky_dom, eigen_comp_paths, kperp10_comp_paths)
QL10_comp.ky_sums_woNL(Sk10_base, C10_base, with_weights=False)
ql10_heat_flux_comp = QL10_comp.ql_heat_flux
ky10_wgt_sums_comp = QL10_comp.ky_wgt_sums

# plotting parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 16}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['lines.linewidth'] = 2

# plotting axes #
fig, axs = plt.subplots(1, 2, sharey=True, tight_layout=True, figsize=(10, 6))

# plot nonlinear spectral data #
axs[0].plot(ky_nl_base, spec_nl_base, c='k', label=r'$\langle \hat{Q}_{\mathrm{nl}} \rangle$')
axs[1].plot(ky_nl_comp, spec_nl_comp, c='k', label=r'$\langle \hat{Q}_{\mathrm{nl}} \rangle$')

# plot quasilinear spectral data #
axs[0].plot(ky_dom, Sk1_base*ky1_wgt_sums_base, c='k', ls='--', marker='s', mfc='None', mec='k', ms=10, label=r'$\langle \hat{Q}_{\mathrm{ql}} \rangle$')
# axs[0].plot(ky_dom, Sk10_base*ky10_wgt_sums_base, c='tab:red', ls=':', marker='o', mfc='None', mec='tab:red', ms=10, label=r'$\langle \hat{Q}_{\mathrm{ql}} \rangle$ (n_conn=10)')
axs[1].plot(ky_dom, Sk1_base*ky1_wgt_sums_comp, c='k', ls='--', marker='s', mfc='None', mec='k', ms=10, label=r'$\langle \hat{Q}_{\mathrm{ql}} \rangle$')
# axs[1].plot(ky_dom, Sk10_base*ky10_wgt_sums_comp, c='tab:red', ls=':', marker='o', mfc='None', mec='tab:red', ms=10, label=r'$\langle \hat{Q}_{\mathrm{ql}} \rangle$ (n_conn=10)')
# axs[0].plot(ky_dom, Sk1_base*ky1_wgt_sums_base, c='k', ls='--', marker='s', mfc='None', mec='k', ms=10, label=r'$S_k\sum_{j} \frac{\gamma_{kj}}{\langle k_{\perp}^2 \rangle_{kj}}$')
# axs[1].plot(ky_dom, Sk1_base*ky1_wgt_sums_comp, c='k', ls='--', marker='s', mfc='None', mec='k', ms=10, label=r'$S_k\sum_{j} \frac{\gamma_{kj}}{\langle k_{\perp}^2 \rangle_{kj}}$')

# axis labels #
axs[0].set_xlabel(r'$k_y\rho_s$')
axs[1].set_xlabel(r'$k_y\rho_s$')

# axis limits #
axs[0].set_xlim(0, ky_nl_base[-1])
axs[0].set_ylim(0, axs[0].get_ylim()[1])

axs[1].set_xlim(0, ky_nl_comp[-1])
axs[1].set_ylim(0, axs[1].get_ylim()[1])

axs[0].set_title(base_config+r' $(Q_{{\mathrm{{nl}}}},\, Q_{{\mathrm{{ql}}}}) = ({0:0.2f},\, {1:0.2f})$'.format(nl_heat_flux_base, ql1_heat_flux_base))
axs[1].set_title(comp_config+r' $(Q_{{\mathrm{{nl}}}},\, Q_{{\mathrm{{ql}}}}) = ({0:0.2f},\, {1:0.2f})$'.format(nl_heat_flux_comp, ql1_heat_flux_comp))

# axis text #
xtxt, ytxt = 0.02, 0.98
axs[0].text(xtxt, ytxt, '(a)', transform=axs[0].transAxes, va='top', ha='left')
axs[1].text(xtxt, ytxt, '(b)', transform=axs[1].transAxes, va='top', ha='left')

# grid and legend #
axs[0].grid()
axs[1].grid()

axs[0].legend()
axs[1].legend()

# show/save #
plt.show()
# save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'personal_meetings', 'Paul_Terry_group', '20230824', 'figures', 'QL_comp_base_qhs_noWght.png')
save_path = os.path.join('/home', 'michael', 'onedrive', 'Proposals', 'ACCESS', '2023_Accelerate', 'QL_model.png')
# plt.savefig(save_path)
