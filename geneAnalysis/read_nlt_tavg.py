import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import colors

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)
import plot_define as pd


class read_nlt:
    """ A class for reading GENE nlt diagnostic data, output in ASCII format.

    ...

    Attributes
    ----------
    nlt_path : str
        Global path to nlt summary file.

    Methods
    -------
    """
    def __init__(self, nlt_path, pram_path):
        # Check if input files exsit #
        if not os.path.exists(nlt_path):
            raise NameError('Path %s does not exist.' % nlt_path)
        if not os.path.exists(pram_path):
            raise NameError('Path %s does not exist.' % pram_path)

        # Read in resolutions #
        with open(pram_path, 'r') as f:
            lines = f.readlines()
            for line in lines:
                line = line.strip().split()
                if len(line) > 0:
                    if line[0] == 'nx0':
                        nx0 = int(line[2])
                    elif line[0] == 'nky0':
                        nky0 = int(line[2])
                    elif line[0] == 'lx':
                        lx = float(line[2])
                    elif line[0] == 'ly':
                        ly = float(line[2])
                        break
        self.nkx = nx0+1
        self.nky = 2*nky0-1
        self.kx = ((2*np.pi)/lx)*np.linspace(-nx0/2, nx0/2, self.nkx)
        self.ky = ((2*np.pi)/ly)*np.linspace(-nky0+1, nky0-1, self.nky)

        # read in nlt data #
        self.nlt_keys = []
        with open(nlt_path, 'r') as f:
            lines = f.readlines()
            num_of_lines = len(lines)
            
            # find beginning of data arrays #
            for l, line in enumerate(lines):
                line_split = line.strip().split()
                if line_split[0] != '#':
                    lbeg = l
                    break
            
            # import additionanl wavenumber arrays #
            nlt_dict = {}
            while lbeg < num_of_lines:
                line1 = lines[lbeg-1].split('k_')
                line_kx, line_ky = line1[1], line1[2].split(')(')[0]
                line_key = 'kx=%s, ky=%s' % (line_kx.split()[1], line_ky.split()[1])
                self.nlt_keys.append(line_key)
                nlt_array = np.zeros((self.nkx, self.nky))
                for l, line in enumerate(lines[lbeg:lbeg+self.nkx-1]):
                    nlt_array[l+1] = [float(x) for x in line.strip().split()]
                nlt_dict[line_key] = nlt_array
                lbeg += self.nkx + 2

        self.nlt_dict = nlt_dict

    def plot_nlt(self, fig, ax, key, args={}, SymLog=False):
        nlt_max = np.max(np.abs(self.nlt_dict[key]))
        if SymLog:
            nlt_scl = 1
            nlt_base = round(np.log10(nlt_max)-2)
            smap = ax.pcolormesh(self.kx, self.ky, self.nlt_dict[key].T, norm=colors.SymLogNorm(linthresh=10**nlt_base, linscale=0.01, vmin=-nlt_max, vmax=nlt_max), **args)
        else:
            if nlt_max < 1e-2:
                nlt_scl = 10**round(np.log10(nlt_max)/-2)
            else:
                nlt_scl = 1
            args['vmin'] = -nlt_max*nlt_scl
            args['vmax'] = nlt_max*nlt_scl
            smap = ax.pcolormesh(self.kx, self.ky, nlt_scl*self.nlt_dict[key].T, **args)
        ax.plot([0, 0], [self.ky[0], self.ky[-1]], c='k', lw=0.5)
        ax.plot([self.kx[0], self.kx[-1]], [0,0], c='k', lw=0.5)

        # mode marker #
        key_split = key.split(', ')
        kx = float(key_split[0].split('=')[1])
        ky = float(key_split[1].split('=')[1])
        kx_idx = np.argmin(np.abs(self.kx-kx))
        ky_idx = np.argmin(np.abs(self.ky-ky))
        ax.scatter(self.kx[kx_idx], self.ky[ky_idx], marker='x', c='k', s=100, label=r'$({0:0.2f},\, {1:0.2f})$'.format(kx, ky))

        # axis labels #
        # ax.set_xlabel(r'$k_\mathrm{x}\rho_\mathrm{s}$')
        # ax.set_ylabel(r'$k_\mathrm{y}\rho_\mathrm{s}$')

        # axis legend #
        ax.legend(loc='upper right', handletextpad=0.1)

        # colorbar #
        cbar = fig.colorbar(smap, ax=ax)
        if nlt_scl == 1:
            cbar.ax.set_ylabel(r'$\langle T_{\chi}^{k,k^{\prime}} \rangle$')
        else:
            cbar.ax.set_ylabel(r'$10^{0:0.0f} \times \langle T_{{\chi}}^{{k,k^{{\prime}}}} \rangle$'.format(np.log10(nlt_scl)))
        

if __name__ == '__main__':
    nlt_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nltcont_tavg_nlt_14.dat')
    pram_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_4')
    nlt = read_nlt(nlt_path, pram_path)
    
    plot = pd.plot_define()
    plt = plot.plt
    fig, ax = plt.subplots(1, 1, tight_layout=True)

    key = nlt.nlt_keys[3]
    nlt_max = np.max(np.abs(nlt.nlt_dict[key]))
    ax.pcolormesh(nlt.kx, nlt.ky, nlt.nlt_dict[key].T, vmin=-nlt_max, vmax=nlt_max, cmap='jet', shading='gouraud')
    ax.plot([0, 0], [nlt.ky[0], nlt.ky[-1]], c='k')
    ax.plot([nlt.kx[0], nlt.kx[-1]], [0,0], c='k')

    # mode marker #
    key_split = key.split(', ')
    kx = float(key_split[0].split('=')[1])
    ky = float(key_split[1].split('=')[1])
    kx_idx = np.argmin(np.abs(nlt.kx-kx))
    ky_idx = np.argmin(np.abs(nlt.ky-ky))
    ax.scatter(nlt.kx[kx_idx], nlt.ky[ky_idx], marker='x', c='k', s=150)

    # axis labels #
    ax.set_title(key)
    ax.set_xlabel(r'$k_\mathrm{x}\rho_\mathrm{s}$')
    ax.set_ylabel(r'$k_\mathrm{y}\rho_\mathrm{s}$')
    
    ax.set_xlim(-0.4, 0.4)
    ax.set_ylim(-0.4, 0.4)

    plt.show()
