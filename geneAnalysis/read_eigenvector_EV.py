import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import geneAnalysis.read_eigenvalues as re
import gistTools.gist_reader as gr

class read_eigendata:
    """ A class for reading GENE hdf5 eigendata data files.

    ...

    Attributes
    ----------
    eigen_path : str
        Global path to eigendata summary file.

    Methods
    -------
    nrg_average(start_time, end_time=-1)
        Calculate time average of nonlinear quantities.
    """
    def __init__(self, eigen_path):
        # read in eigendata #
        with hf.File(eigen_path, 'r') as hf_:
            ball_data = hf_['ball data'][()]

        # define ballooning grid points #
        self.num_of_modes = ball_data.shape[0]
        self.num_of_ball_pnts = ball_data.shape[1]

        # define ballooning data arrays #
        self.ball_dom = ball_data[:,:,0]
        self.real_phi = ball_data[:,:,1]
        self.imag_phi = ball_data[:,:,2]
        self.amp_phi = np.sqrt(self.real_phi**2 + self.imag_phi**2)
        self.ball_conn = ball_data[:,:,3]

    def read_in_projections(self, proj_path):
        """ Read in projection data for eigenvalues 

        Parameters
        ----------
        proj_path : str
            Global to projection data.
        """
        with open(proj_path, 'r') as f:
            lines = f.readlines()
            self.proj_data = np.empty(self.num_of_modes)
            for l, line in enumerate(lines):
                self.proj_data[l] = float(line.strip())

    def read_in_frequencies(self, freq_path):
        """ Read in complex frequency data for eigenvalues 

        Parameters
        ----------
        freq_path : str
            Global to eigenvalue frequency data.
        """
        freq = re.eigenvalues(freq_path)
        self.omega = freq.omega

    def separate_modes(self):
        """ Separate data arrays into radial wavenumbers.
        """
        # find array partition points #
        chng_idx = np.where(self.ball_conn[0]-np.roll(self.ball_conn[0],1) != 0)[0]
        idx_range = np.arange(self.ball_conn.shape[1])[chng_idx]

        # define grid points #
        self.num_of_kx = chng_idx.shape[0]
        self.num_of_zpts = int(self.num_of_ball_pnts/chng_idx.shape[0])

        # define radial wavenumbers and poloidal domain #
        nkx0 = int(.5*(self.num_of_kx-1))
        self.kx_modes = np.linspace(-nkx0, nkx0, self.num_of_kx)
        # self.pol_dom = self.ball_dom[0, idx_range[nkx0]:idx_range[nkx0+1]]

        # separate arrays into radial wavenumbers #
        self.pol_dom = np.empty((self.num_of_modes, self.num_of_kx, self.num_of_zpts))
        self.real_modes = np.empty((self.num_of_modes, self.num_of_kx, self.num_of_zpts))
        self.imag_modes = np.empty((self.num_of_modes, self.num_of_kx, self.num_of_zpts))
        self.amp_modes = np.empty((self.num_of_modes, self.num_of_kx, self.num_of_zpts))
        for i in range(self.num_of_modes):
            for j in range(self.num_of_kx-1):
                self.pol_dom[i,j] = self.ball_dom[i,idx_range[j]:idx_range[j+1]]
                self.real_modes[i,j] = self.real_phi[i,idx_range[j]:idx_range[j+1]]
                self.imag_modes[i,j] = self.imag_phi[i,idx_range[j]:idx_range[j+1]]
                self.amp_modes[i,j] = np.hypot(self.real_phi[i,idx_range[j]:idx_range[j+1]], self.imag_phi[i,idx_range[j]:idx_range[j+1]])
            self.pol_dom[i,j+1] = self.ball_dom[i,idx_range[j+1]:]
            self.real_modes[i,j+1] = self.real_phi[i,idx_range[j+1]:]
            self.imag_modes[i,j+1] = self.imag_phi[i,idx_range[j+1]:]
            self.amp_modes[i,j+1] = np.hypot(self.real_phi[i,idx_range[j+1]:], self.imag_phi[i,idx_range[j+1]:])

    def transForm_xy(self, x_dom, y_dom, mode_idx, pol_ang):
        try:
            pol_idx = np.argmin(np.abs(self.pol_dom-pol_ang))
        except AttributeError:
            self.seperate_modes()
            pol_idx = np.argmin(np.abs(self.pol_dom-pol_ang))

        real_phi = self.real_modes[mode_idx, :, pol_idx]
        imag_phi = self.imag_modes[mode_idx, :, pol_idx]
        cmplx_phi = real_phi + 1j*imag_phi

        ky = 0.1
        kx_arr = np.linspace(0, 6, self.num_of_kx)

        phi_grid = np.empty((x_dom.shape[0], y_dom.shape[0]), dtype=complex)
        for i, x in enumerate(x_dom):
            for j, y in enumerate(y_dom):
                phi_grid[i,j] = np.sum(np.exp(1j*(kx_arr*x + ky*y))*cmplx_phi)

        return phi_grid

    def plotting(self, fontsize=14, labelsize=16, linewidth=1, figsize=(15,4)):
        # Plotting Parameters #
        plt.close('all')

        font = {'family': 'sans-serif',
                'serif': 'Times New Roman',
                'weight': 'normal',
                'size': fontsize}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = labelsize
        mpl.rcParams['lines.linewidth'] = linewidth
        mpl.rcParams['figure.figsize'] = figsize

        fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=figsize)
        return plt, fig, ax

    def plot_eigenfunctions(self, freq_path, proj_path, save_dir=None, runID=None):
        """ Plot the eigenfunction data for every eigenmode.

        Parameters
        ----------
        freq_path : str
            Global path to frequency data.
        proj_path : str
            Global path to projection data.
        save_dir : str, optional
            Global path to the directory where the figures will be saved. If None,
            the figures will be shown instead of saved. Default is None.
        runID : int, optional
            Numerical identifier for the simulation run. This only needs to be
            defined if the figures are being saved. Default is None.
        """
        # read in frequency and projection data #
        self.read_in_frequencies(freq_path)
        self.read_in_projections(proj_path)
        proj = self.proj_data
        for i in range(self.ball_dom.shape[0]):
            plt, fig, ax = self.plotting()

            # plot data #
            ax.plot(self.ball_dom[i], self.real_phi[i], c='tab:red', label=r'$\mathcal{Re}(\phi)$')
            ax.plot(self.ball_dom[i], self.imag_phi[i], c='tab:blue', label=r'$\mathcal{Im}(\phi)$')
            ax.plot(self.ball_dom[i], self.amp_phi[i], c='k')

            # axis labels #
            ax.set_xlabel(r'$\theta / \pi$')
            ax.set_ylabel(r'$|\phi|$')
            title = r'$(\gamma, \ \omega, \ p_j)$'+\
                    ' = ({0:0.3f}, {1:0.3f}, {2:0.2f})'.format(self.omega[i,0], self.omega[i,1], proj[i])
            ax.set_title(title)
            
            # axis limits #
            ax.set_xlim(self.ball_dom[i,0], self.ball_dom[i,-1])

            # axis legend/grid #
            ax.legend(frameon=False, loc='lower left', fontsize=16)
            ax.grid()

            # save/show #
            if save_path is None:
                plt.show()
            else:
                save_path = os.path.join(save_dir, 'balle_{}_ev{}.png'.format(runID, i))
                plt.savefig(save_path)

    def plot_eigenfunction_over_field_geometry(self, gist_path, mode_idx, kx_idx, save_path=None):
        """ Plot a single kx mode over the field-line field strength and curvature.

        Parameters
        ----------
        gist_path : str
            Global path to the gist file where field geometry is stored.
        """
        # define poloidal domains #
        kx0_idx = np.argmin(np.abs(self.kx_modes))
        pol_kx0 = self.pol_dom[0,kx0_idx]
        pol_dom = self.pol_dom[mode_idx, kx_idx]

        # compute field-line geometry variables #
        gist = gr.read_gist(gist_path)
        modB = gist.model['modB'](pol_kx0*np.pi)
        L2 = gist.model['L2'](pol_kx0*np.pi)

        # define axes #
        plt, fig, ax1 = self.plotting()
        ax2 = ax1.twinx()
        ax3 = ax1.twinx()
        ax2.spines['right'].set_color('tab:blue')
        ax2.tick_params(axis='y', colors='tab:blue')
        ax3.spines['right'].set_position(('axes', 1.1))
        ax3.spines['right'].set_color('tab:orange')
        ax3.tick_params(axis='y', colors='tab:orange')

        # plot data #
        ax1.plot(pol_dom, self.amp_modes[mode_idx, kx_idx], c='k')
        ax2.plot(pol_dom, modB, c='tab:blue', ls='--')
        ax3.plot(pol_dom, L2, c='tab:orange', ls='-.')

        # axis limits #
        ax1.set_xlim(pol_dom[0], pol_dom[-1])
        ax1.set_ylim(0, ax1.get_ylim()[1])

        # axis labels #
        ax1.set_ylabel(r'$|\phi|$')
        ax1.set_xlabel(r'$\theta \ / \ \pi$')
        ax2.set_ylabel(r'$B \ / \ \mathrm{T}$', c='tab:blue')
        ax3.set_ylabel(r'$\mathcal{L}_2$', c='tab:orange')

        # save/show #
        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

if __name__ == "__main__":
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    # define global paths #
    ky_tag = 'ky0p8'
    runID = '1'
    eigen_path = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_2p0_omti_0p0_omte_2p0', ky_tag, 'balle_{}.h5'.format(runID))
    freq_path = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_2p0_omti_0p0_omte_2p0', ky_tag, 'eigenvalues_{}'.format(runID))
    # proj_path = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0', ky_tag, 'stell_NLproj_{}.dat'.format(runID))
    # proj03_path = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_2p0_omti_0p0_omte_2p0', ky_tag, 'stell_NLproj_{}_NL03.dat'.format(runID))
    proj10_path = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_2p0_omti_0p0_omte_2p0', ky_tag, 'stell_NLproj_{}_NL10.dat'.format(runID))
    gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_QHS_mn1824_alf0_s0p50_res64_npol4.dat')

    # import data #
    eig = read_eigendata(eigen_path)
    eig.separate_modes()

    mode_idx = 46
    kx_val = 0
    kx_idx = np.argmin(np.abs(eig.kx_modes-kx_val))
    save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'personal_meetings', 'Joey_Duff', '20240514', 'figures', 'QHS_ev46_ky0p8_kx0.png')
    eig.plot_eigenfunction_over_field_geometry(gist_path, mode_idx, kx_idx, save_path=save_path)
