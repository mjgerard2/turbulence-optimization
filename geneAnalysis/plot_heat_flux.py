import os
import h5py as hf
import numpy as np

import matplotlib.colors as mcolors

import sys
WORKDIR = os.path.dirname(os.getcwd())
sys.path.append(WORKDIR)

import plot_define as pd
from gistTools import gist_reader as gr
from vmecTools.wout_files import booz_read as br


config_ids = ['QHS', '60-1-84', '897-1-0']
elongation = np.empty(len(config_ids))
gamma = np.empty((len(config_ids), 4))
shear = np.empty(len(config_ids))

# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Read in Growth Rates #
hf_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')
with hf.File(hf_path, 'r') as hf_:
    tem_data = {}
    for key in hf_:
        tem_data[key] = hf_[key][()]

# Read in QHS growth rate #
metric_norm = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations.h5')
with hf.File(metric_norm, 'r') as hf_:
    qhs_tem = hf_['TEM'][()]

# Loop over configurations #
for i, config_id in enumerate(config_ids):
    if config_id == 'QHS':
        elongation[i] = 1
        gamma[i] = qhs_tem[0:4, 1]

        gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'gist_QHS_best_alf0_s0p5_res128_npol4.dat')
        gist = gr.read_gist(gist_file)
        shear[i] = gist.shat


    else:
        config_arr = np.array([int(x) for x in config_id.split('-')])
        met_idx = np.argmin(np.sum(np.abs(met_data[:, 0:3] - config_arr), axis=1))
        elongation[i] = met_data[met_idx, 9]

        gamma[i] = tem_data[config_id][0:4, 1]

        main_id = 'main_coil_{}'.format(config_id.split('-')[0])
        set_id = 'set_{}'.format(config_id.split('-')[1])
        job_id = 'job_{}'.format(config_id.split('-')[2])

        gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'gist_HSX_'+config_id+'_s0p5_res1024_npol1_alpha0p00.dat')
        gist = gr.read_gist(gist_file)
        shear[i] = gist.shat

# Plot heat flux #
heat_flux = np.array([1.49, 2.18, 3.09])

plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(3, 1, sharex=True, sharey=False, tight_layout=True, figsize=(7, 12))

ax1 = axs[0]
ax2 = axs[1]
ax3 = axs[2]

color_list = ['tab:blue', 'tab:orange', 'tab:green']

ax1.plot(elongation, heat_flux, c='k', ls='--')
ax1.scatter(elongation, heat_flux, facecolor=color_list, edgecolor='k', marker='o', s=300, zorder=10)

ax2.plot(elongation, gamma[:, 0], c='k', ls='--')
ax2.scatter(elongation, gamma[:, 0], facecolor=color_list, edgecolor='k', marker='v', s=300, zorder=10, label='0.1')
ax2.plot(elongation, gamma[:, 1], c='k', ls='--')
ax2.scatter(elongation, gamma[:, 1], facecolor=color_list, edgecolor='k', marker='o', s=300, zorder=10, label='0.4')
ax2.plot(elongation, gamma[:, 2], c='k', ls='--')
ax2.scatter(elongation, gamma[:, 2], facecolor=color_list, edgecolor='k', marker='s', s=300, zorder=10, label='0.7')
ax2.plot(elongation, gamma[:, 3], c='k', ls='--')
ax2.scatter(elongation, gamma[:, 3], facecolor=color_list, edgecolor='k', marker='^', s=300, zorder=10, label='1.0')

ax3.plot(elongation, shear, c='k', ls='--')
ax3.scatter(elongation, shear, facecolor=color_list, edgecolor='k', marker='o', s=300, zorder=10)

ax3.set_xlabel(r'$\mathcal{K}/\mathcal{K}^*$')
ax1.set_ylabel(r'$\langle Q_{\mathrm{e}}^{es} \rangle \ \left( c_sn_{\mathrm{e}} T_{\mathrm{e}} (\rho_s/a)^2 \right)$')
ax2.set_ylabel(r'$\gamma \ (c_s/a)$')
ax3.set_ylabel(r'$\hat{s}$')

ax1.set_ylim(0, 1.05*ax1.get_ylim()[1])
ax2.set_ylim(0, 1.75*ax2.get_ylim()[1])
ax3.set_ylim(1.05*ax3.get_ylim()[0], 0)

# Axis Legends #
ax2.legend(loc='upper center', frameon=False, ncol=4, columnspacing=0.5, handletextpad=0., title=r'$k_y$', fontsize=20)

# Axis Grid #
ax1.grid()
ax2.grid()
ax3.grid()

# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'Prelim_Defense', 'figures', 'heat_flux_comparison.png')
plt.savefig(save_path)
