import os


class read_file():

    def __init__(self, path):
        if not os.path.exists(path):
            raise IOError("File does not exist! "+path)

        self.data_dict = {}
        with open(path, 'r') as f:
            data_keys = ['nx0', 'nky0', 'nz0', 'nv0', 'nw0', 'kymin', 'lx',
                         'ly', 'lv', 'lw', 'n_pol', 'nexec', 'hyp_z', 'hyp_v',
                         'q0', 'shat', 'minor_r', 'major_R', 'rhostar',
                         'n_spec', 'name', 'Bref', 'Tref', 'nref', 'Lref',
                         'mref']

            lines = f.readlines()
            for ldx, line in enumerate(lines):
                line_split = line.strip().split()
                if len(line_split) >= 3:
                    if any(key == line_split[0] for key in data_keys):
                        if line_split[0] == 'name':
                            if line_split[2] == "\'i\'":
                                line_1 = lines[ldx+1].strip().split()
                                line_2 = lines[ldx+2].strip().split()
                                line_5 = lines[ldx+5].strip().split()
                                self.data_dict['omni'] = float(line_1[2])
                                self.data_dict['omti'] = float(line_2[2])
                                self.data_dict['tempi'] = float(line_5[2])

                            elif line_split[2] == "\'e\'":
                                line_1 = lines[ldx+1].strip().split()
                                line_2 = lines[ldx+2].strip().split()
                                line_5 = lines[ldx+5].strip().split()
                                self.data_dict['omne'] = float(line_1[2])
                                self.data_dict['omte'] = float(line_2[2])
                                self.data_dict['tempe'] = float(line_5[2])

                        else:
                            self.data_dict[line_split[0]] = float(line_split[2])


if __name__ == '__main__':
    param_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'linear_data', '0-1-111', 'ky_0p1', 'parameters_1_1')
    pram = read_file(param_path)
    for key in pram.data_dict:
        print(key+' = {}'.format(pram.data_dict[key]))
