import os
import h5py as hf
import numpy as np

import matplotlib.colors as mcolors

import sys
WORKDIR = os.path.dirname(os.getcwd())
sys.path.append(WORKDIR)

import plot_define as pd
from gistTools import gist_reader as gr
from vmecTools.wout_files import booz_read as br


config_ids = ['QHS', '60-1-84', '897-1-0']
elongation = np.empty(len(config_ids))

# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Loop over configurations #
for i, config_id in enumerate(config_ids):
    if config_id == 'QHS':
        elongation[i] = 1

    else:
        config_arr = np.array([int(x) for x in config_id.split('-')])
        met_idx = np.argmin(np.sum(np.abs(met_data[:, 0:3] - config_arr), axis=1))
        elongation[i] = met_data[met_idx, 9]

# Plot heat flux #
den_arr = np.array([2, 2.5, 3])
heat_flux = np.array([[1.49, 2.18, 3.09],
                      [4.85, 3.7, 4.23],
                      [np.nan, 7.18, 32.84]])

plot = pd.plot_define()
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

ax.plot(den_arr, heat_flux[:, 0], c='k', ls='--')
ax.scatter(den_arr[0], heat_flux[0, 0], marker='o', facecolor='tab:blue', edgecolor='k', s=150, label='QHS')
ax.scatter(den_arr[1:3], heat_flux[1:3, 0], marker='o', facecolor='None', edgecolor='k', s=150)

ax.plot(den_arr, heat_flux[:, 1], c='k', ls='--')
ax.scatter(den_arr[0], heat_flux[0, 1], marker='s', facecolor='tab:orange', edgecolor='k', s=150, label='60-1-84')
ax.scatter(den_arr[1:3], heat_flux[1:3, 1], marker='s', facecolor='None', edgecolor='k', s=150)

ax.plot(den_arr, heat_flux[:, 2], c='k', ls='--')
ax.scatter(den_arr[0], heat_flux[0, 2], marker='v', facecolor='tab:green', edgecolor='k', s=150, label='897-1-0')
ax.scatter(den_arr[1:3], heat_flux[1:3, 2], marker='v', facecolor='None', edgecolor='k', s=150)

# Axis Labels #
ax.set_xlabel(r'$a/L_n$')
ax.set_ylabel(r'$\langle Q_{\mathrm{e}}^{es} \rangle \ \left( c_sn_{\mathrm{e}} T_{\mathrm{e}} (\rho_s/a)^2 \right)$')

# Axis Grid #
ax.grid()
ax.legend(loc='upper left')

# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'Prelim_Defense', 'figures', 'heat_flux_scan.png')
plt.savefig(save_path)
