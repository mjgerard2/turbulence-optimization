import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


# Configuration IDs #
conID_list = ['0-1-0', '60-1-84', '897-1-0']

heat_flux = np.array([1.49, 2.18, 3.09])

# Read in Available Energy Data #
omn = 3
omt = 2
ae_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'AE_data.h5')
with hf.File(ae_path, 'r') as hf_:
    omn_vals = hf_['omn_vals'][()]
    omt_vals = hf_['omt_vals'][()]

    omn_idx = np.argmin(np.abs(omn_vals - omn))
    omt_idx = np.argmin(np.abs(omt_vals - omt))

    ae_data = np.empty((len(conID_list), 2))
    as_data = np.empty((len(conID_list), 2))
    ad_data = np.empty((len(conID_list), 2))
    for c, conID in enumerate(conID_list):
        ae_data[c] = [hf_[conID+'/Available Energy'][omn_idx, omt_idx], heat_flux[c]]
        as_data[c] = [hf_[conID+'/Available SINK'][omn_idx, omt_idx], heat_flux[c]]
        ad_data[c] = [ae_data[c, 0] - as_data[c, 0], heat_flux[c]]

ae_data = np.array(sorted(ae_data, key=lambda x: x[0]))
as_data = np.array(sorted(as_data, key=lambda x: x[0]))
ad_data = np.array(sorted(ad_data, key=lambda x: x[0]))

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axis #
fig, axs = plt.subplots(1, 3, sharex=False, sharey=True, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = axs[1]
ax3 = axs[2]

# Plot data #
ax1.plot(ae_data[:, 0], ae_data[:, 1], c='tab:blue', marker='s', mfc='None', mec='tab:blue', markersize=15, ls='--')
ax2.plot(as_data[:, 0], as_data[:, 1], c='tab:orange', marker='s', mfc='None', mec='tab:orange', markersize=15, ls='--')
ax3.plot(ad_data[:, 0], ad_data[:, 1], c='tab:green', marker='s', mfc='None', mec='tab:green', markersize=15, ls='--')

# Axis labels #
ax1.set_ylabel(r'$\langle Q_{\mathrm{es}} \rangle$')
ax1.set_xlabel(r'$\hat{\mathrm{AE}}$')
ax2.set_xlabel(r'$\hat{\mathrm{AS}}$')
ax3.set_xlabel(r'$\hat{\mathrm{AE}} - \hat{\mathrm{AS}}$')

# Axis limits #
ax1.set_ylim(0, 1.05*np.max(heat_flux))
# ax2.set_ylim(0, 1.05*np.max(as_data))

ax1.grid()
ax2.grid()
ax3.grid()

plt.show()
