import numpy as np
import h5py as hf
import os

import read_parameters as rp


job_tag = '1'
ky_vals = np.round(np.linspace(.1, 1.9, 7), 1)
save_file = os.path.join('/mnt', 'GENE', 'linear_data', 'omega_data_'+job_tag+'_x.h5')

# Configuration IDs #
con_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'config_list.txt')
with open(con_path, 'r') as f:
    lines = f.readlines()
    conIDs = []
    for line in lines:
        conIDs.append(line.strip())

# Read in Data #
prof_dict = {}
data_dict = {}

data_path = os.path.join('/mnt', 'GENE', 'linear_data')
for cdx, conID in enumerate(conIDs):
    print(conID+' ({0:0.0f}|{1:0.0f})'.format(cdx+1, len(conIDs)))
    omega_kys = np.empty((ky_vals.shape[0], 3))
    for kdx, ky in enumerate(ky_vals):
        ky_tag = 'ky_{0:0.1f}'.format(ky).replace('.', 'p')
        # print('    '+ky_tag)
        ky_conID = os.path.join(data_path, conID, ky_tag)

        # Read Frequency Data #
        omega_runs = []
        for f in os.scandir(ky_conID):
            f_name = f.name.split('_')
            if f_name[0] == 'omega' and f_name[1] == job_tag:
                omega_runs.append(int(f_name[2]))
        omega_run = np.max(omega_runs)

        run_tag = job_tag+'_{0:0.0f}'.format(omega_run)
        omega_file = 'omega_'+run_tag

        omega_path = os.path.join(ky_conID, omega_file)
        with open(omega_path, 'r') as f:
            lines = f.readlines()
            if len(lines) > 0:
                omega_data = [float(x) for x in lines[0].split()]
                if omega_data[1] == 0.0 and omega_data[2] == 0.0:
                    omega_kys[kdx] = np.array([omega_data[0], np.nan, np.nan])
                else:
                    omega_kys[kdx] = omega_data
            else:
                omega_kys[kdx] = np.array([ky, np.nan, np.nan])

        # Read in Profile Data #
        param_path = os.path.join(ky_conID, 'parameters_'+run_tag)
        pram = rp.read_file(param_path)

        if pram.data_dict['n_spec'] == 1:
            prof_dict[conID+'/'+ky_tag+'/n_spec'] = 1
            prof_dict[conID+'/'+ky_tag+'/omni'] = pram.data_dict['omni']
            prof_dict[conID+'/'+ky_tag+'/omti'] = pram.data_dict['omti']
            prof_dict[conID+'/'+ky_tag+'/tempi'] = pram.data_dict['tempi']

        elif pram.data_dict['n_spec'] == 2:
            prof_dict[conID+'/'+ky_tag+'/n_spec'] = 2
            prof_dict[conID+'/'+ky_tag+'/omni'] = pram.data_dict['omni']
            prof_dict[conID+'/'+ky_tag+'/omti'] = pram.data_dict['omti']
            prof_dict[conID+'/'+ky_tag+'/tempi'] = pram.data_dict['tempi']

            prof_dict[conID+'/'+ky_tag+'/omne'] = pram.data_dict['omne']
            prof_dict[conID+'/'+ky_tag+'/omte'] = pram.data_dict['omte']
            prof_dict[conID+'/'+ky_tag+'/tempe'] = pram.data_dict['tempe']

        else:
            prof_dict[conID+'/n_spec'] = pram.data_dict['n_spec']

    data_dict[conID] = omega_kys

# Check for Consistency in Profile Data #
prof_dict_chk = {}
for cdx, conID in enumerate(data_dict):
    if cdx == 0:
        ky_tag = 'ky_{0:0.1f}'.format(ky_vals[0]).replace('.', 'p')
        n_spec = prof_dict[conID+'/'+ky_tag+'/n_spec']
        if n_spec == 1:
            prof_dict_chk['n_spec'] = 1
            prof_dict_chk['omni'] = prof_dict[conID+'/'+ky_tag+'/omni']
            prof_dict_chk['omti'] = prof_dict[conID+'/'+ky_tag+'/omti']
            prof_dict_chk['tempi'] = prof_dict[conID+'/'+ky_tag+'/tempi']

        elif n_spec == 2:
            prof_dict_chk['n_spec'] = 1
            prof_dict_chk['omni'] = prof_dict[conID+'/'+ky_tag+'/omni']
            prof_dict_chk['omti'] = prof_dict[conID+'/'+ky_tag+'/omti']
            prof_dict_chk['tempi'] = prof_dict[conID+'/'+ky_tag+'/tempi']

            prof_dict_chk['n_spec'] = 2
            prof_dict_chk['omne'] = prof_dict[conID+'/'+ky_tag+'/omne']
            prof_dict_chk['omte'] = prof_dict[conID+'/'+ky_tag+'/omte']
            prof_dict_chk['tempe'] = prof_dict[conID+'/'+ky_tag+'/tempe']

        else:
            raise ValueError('Code not currently equiped to handle {} number of species\n'.format(prof_dict[conID+'/'+ky_tag+'/n_spec']))

    else:
        for key in prof_dict_chk:
            for ky in ky_vals:
                ky_tag = 'ky_{0:0.1f}'.format(ky_vals[0]).replace('.', 'p')
                if prof_dict[conID+'/'+ky_tag+'/'+key] != prof_dict_chk[key]:
                    raise IOError(conID+' has inconsistent key: '+key+'\n')

# Save Data #
with hf.File(save_file, 'w') as hf_:
    for key in data_dict:
        hf_.create_dataset(key, data=data_dict[key])
    for key in prof_dict_chk:
        hf_.create_dataset(key, data=prof_dict_chk[key])
