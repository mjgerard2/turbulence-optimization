import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import h5py as hf
import numpy as np
import geneAnalysis.read_parameters as rp


base_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0')
pram_path = os.path.join(base_path, 'parameters_00')
phi_path = os.path.join(base_path, 'phidata_00_10.dat')
phi_hf_path = os.path.join(base_path, 'phidata_00_10.h5')

pram = rp.read_file(pram_path)

Nx = int(pram.data_dict['nx0'])
Ny = 2*int(pram.data_dict['nky0'])
Nz = int(pram.data_dict['nz0'])

itr = 0
Nxy = Nx*Ny
Nxyz = Nxy*Nz

with open(phi_path, 'r') as f:
    while True:
        if itr == 10:
            break
        print('iteration: {}'.format(itr))
        try:
            with hf.File(phi_hf_path, 'a') as hf_:
                tkey = 'time {}'.format(itr)
                if not tkey in hf_:
                    line_set = [next(f) for x in range(Nxyz*itr, Nxyz*(itr+1))]
                    # line_set = [next(f) for x in range(Nxyz)]
                    phi = np.empty((Nz, Ny, Nx))
                    for i in range(Nz):
                        for j in range(Ny):
                            for k in range(Nx):
                                ldx = i*Nxy + j*Nx + k
                                phi[i, j, k] = float(line_set[ldx].strip())

                    # with hf.File(phi_hf_path, 'a') as hf_:
                    #    hf_.create_dataset('time {}'.format(itr), data=phi)
                    hf_.create_dataset(tkey, data=phi)
                itr += 1

        except StopIteration:
            break
