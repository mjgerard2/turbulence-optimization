import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


heat_flux = np.array([150, 1.42, 1.78, 2.85, 2.7, 3.09])
conIDs = ['5-1-485', '0-1-385', '0-1-0', '3-1-138', '60-1-84', '897-1-0']

# Read in Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Order Metric Data #
eps_data = np.empty(len(conIDs))
qhs_data = np.empty(len(conIDs))
kap_data = np.empty(len(conIDs))
for cdx, conID in enumerate(conIDs):
    if conID == '0-1-0':
        eps_data[cdx] = 1.
        qhs_data[cdx] = 1.
        kap_data[cdx] = 1.

    else:
        conArr = np.array([float(iD) for iD in conID.split('-')])
        idx = np.argmin(np.sum(np.abs(met_data[:, 0:3] - conArr), axis=1))

        eps_data[cdx] = met_data[idx, 3]
        qhs_data[cdx] = met_data[idx, 4]
        kap_data[cdx] = met_data[idx, 9]

# Plot Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['lines.linewidth'] = 2

# Construct Axex #
fig, axs = plt.subplots(3, 1, sharex=True, tight_layout=True, figsize=(8, 10))

ax1 = axs[0]
ax2 = axs[1]
ax3 = axs[2]

# Plot Data #
ax1.plot(kap_data, heat_flux, c='k', ls='--', marker='o', mfc='tab:red', ms=15)
ax2.plot(kap_data, qhs_data, c='k', ls='--', marker='s', mfc='tab:green', ms=15)
ax3.plot(kap_data, eps_data, c='k', ls='--', marker='v', mfc='tab:blue', ms=15)

# Axis Labels #
ax3.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax1.set_ylabel(r'$\langle Q_{es} \rangle$')
ax2.set_ylabel(r'$\mathcal{Q} / \mathcal{Q}^*$')
ax3.set_ylabel(r'$\mathcal{E} / \mathcal{E}^*$')

# Axis Grid #
ax1.grid()
ax2.grid()
ax3.grid()

# Axis Limits #
ax1.set_ylim(0, 3.5)

# Axis Show/Save #
plt.show()
