import os
import numpy as np

import read_nrg_summary as rns
import read_spectrum as rs
import read_contours as rc

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as ani


pram_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_00')
nrg_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_00_10.h5')
spec_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'fluxspectrae_00_10.dat')
cont_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'conte_00_10.dat')

save_name = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'figures', 'animation_00_10.mp4')
frames_per_second = 10

# Instantiate Data File Objects #
nrg = rns.nrg_summary(nrg_path)
spec = rs.spectrum(spec_path, pram_path)
cont = rc.contours(cont_path, pram_path)

# Define Start Time and Time Domains #
t_start = 0
t_spec_avg = 3000
nrg_time = nrg.time_domain
spec_time = spec.time_domain
cont_time = cont.time_domain

# Define Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 16}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['lines.linewidth'] = 2

# Construct Plotting Axis #
fig, axs = plt.subplots(2, 2, sharex=False, sharey=False, tight_layout=True, figsize=(14, 12))

ax1 = axs[0, 0]
ax2 = axs[0, 1]
ax3 = axs[1, 0]
ax4 = axs[1, 1]

# Plot contours #
x_dom = cont.x_domain
y_dom = cont.y_domain

cont_phi = cont.cont_data['phi']
cont_n = cont.cont_data['n']

phi_sprd = 3*np.std(cont_phi)
n_sprd = 4*np.std(cont_n)

cont_idx = np.argmin(np.abs(cont_time - t_start))

cont_map1 = ax1.pcolormesh(x_dom, y_dom, cont_phi[cont_idx].T, vmin=-phi_sprd, vmax=phi_sprd, cmap='jet', shading='gouraud')
cont_map3 = ax3.pcolormesh(x_dom, y_dom, cont_n[cont_idx].T, vmin=-n_sprd, vmax=n_sprd, cmap='jet', shading='gouraud')

# Contour labels #
ax1.set_ylabel(r'$y / \rho_s$')
ax1.set_xlabel(r'$x / \rho_s$')

ax3.set_ylabel(r'$y / \rho_s$')
ax3.set_xlabel(r'$x / \rho_s$')

cax1 = fig.colorbar(cont_map1, ax=ax1)
cax1.ax.set_ylabel(r'$\tilde{\phi}$')

cax3 = fig.colorbar(cont_map3, ax=ax3)
cax3.ax.set_ylabel(r'$\tilde{n}$')

# Contour limits #
ax1.set_xlim(x_dom[0], x_dom[-1])
ax1.set_ylim(y_dom[0], y_dom[-1])

ax3.set_xlim(x_dom[0], x_dom[-1])
ax3.set_ylim(y_dom[0], y_dom[-1])

# Plot nrg time trace #
nrg_heat = nrg.time_series['Q_tot']
nrg_time_now = nrg_time[nrg_time <= t_start]
nrg_heat_now = nrg_heat[nrg_time <= t_start]

ax2.plot(nrg_time, nrg_heat, c='k', alpha=0.2)
nrg_trace, = ax2.plot([], [], c='k')
nrg_spot, = ax2.plot([], [], marker='o', markersize=15, mfc='tab:red', zorder=10)

# nrg labels #
ax2.set_xlabel(r'$t \ (a/c_s)$')
ax2.set_ylabel(r'$\langle Q_{\mathrm{es}} \rangle$')

# nrg limits #
ax2.set_xlim(0, nrg_time[-1])
ax2.set_ylim(0, 1.05*np.max(nrg_heat))

# Plot spectrum #
spec.spectrum_average(t_spec_avg)
spec.spectrum_normalization()

ky = spec.ky_domain
ky_norm = 1./spec.spec_norms['ky']['Q_es']
ky_amp = spec.spec_data_avg['ky']['Q_es']*ky_norm

spec_idx = np.argmin(np.abs(spec.time_domain - t_start))
ky_amp_now = spec.spec_data['ky']['Q_es'][spec_idx]*ky_norm

ax4.plot(ky, ky_amp, c='k')
spec_now, = ax4.plot([], [], c='k', ls='--', lw=2)

# Spectrum labels #
ax4.set_xlabel(r'$k_y \rho_s$')
ax4.set_ylabel(r'$\langle \hat{Q}_{\mathrm{es}} \rangle$ (a.u.)')

# Spectrum limits #
ax4.set_xlim(0, ky[-1])
# ax4.set_ylim(0, np.max(spec.spec_data['ky']['Q_es'][spec_idx::])*ky_norm)
ax4.set_ylim(0, 1.25*np.max(ky_amp))

animation_time = cont_time[cont_idx::]
def animate(i):
    time = animation_time[i]

    # Contour #
    cont_idx = np.argmin(np.abs(cont_time - time))
    cont_map1.set_array(cont_phi[cont_idx].T.ravel())
    cont_map3.set_array(cont_n[cont_idx].T.ravel())

    # nrg #
    time_now = nrg_time[nrg_time <= time]
    heat_now = nrg_heat[nrg_time <= time]
    nrg_trace.set_data(time_now, heat_now)
    nrg_spot.set_data(time_now[-1], heat_now[-1])

    # Spectrum #
    spec_idx = np.argmin(np.abs(spec_time - time))
    amp_now = spec.spec_data['ky']['Q_es'][spec_idx]*ky_norm
    spec_now.set_data(ky, amp_now)

    return cont_map1, cont_map3, nrg_trace, nrg_spot, spec_now


# Save or show #
anim = ani.FuncAnimation(fig, animate, frames=animation_time.shape[0])
anim.save(save_name, writer='ffmpeg', fps=frames_per_second)
