import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

from scipy.interpolate import interp1d
from scipy.optimize import curve_fit

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import plot_define as pd


class read_phases:
    """ A class for reading GENE phase data from *.dat files.

    ...

    Attributes
    ----------
    file_path : str
        Global path to phase file.

    Methods
    -------
    plot_cross_phase(ax, phase_key, norm_phase=True, norm_amp=True)
        Plot cross phase data for the specified phase key.

    get_average_phase(phase_key)
        Calculate the average cross phase for the specified phase key.
    """
    def __init__(self, file_path):
        # Check if input file exists #
        if not os.path.isfile(file_path):
            raise IOError(file_path+" does not exist.")

        # Read in phase data #
        self.data = {}
        with open(file_path, 'r') as f:
            lines = f.readlines()
            num_of_lines = len(lines)

            phase = []
            for ldx, line in enumerate(lines):
                line = line.strip().split()
                if len(line) > 0:
                    if line[0] != '#':
                        phase.append(float(line[0]))
                else:
                    last_ldx = ldx
                    self.num_of_phases = len(phase)
                    self.data['phases'] = np.array(phase)
                    break

            while last_ldx+1 < num_of_lines:
                for ldx, line in enumerate(lines[last_ldx::]):
                    line = line.strip().split()
                    if len(line) > 0:
                        break

                last_ldx += ldx

                phase = []
                for ldx, line in enumerate(lines[last_ldx::]):
                    line = line.strip().split()
                    if len(line) > 0:
                        if line [0] != '#':
                            phase.append(float(line[0]))
                    else:
                        key = lines[last_ldx+ldx-1-len(phase)].strip().split()[1]
                        if key == 'phi_x_Tpa' or key == 'phi_x_Tpe':
                            if key+',p' in self.data:
                                key = key+',t'
                                self.data[key] = np.array(phase)
                            else:
                                key = key+',p'
                                self.data[key] = np.array(phase)
                        else:
                            self.data[key] = np.array(phase)
                        break

                last_ldx += ldx

            key = lines[last_ldx-len(phase)].strip().split()[1]
            if key == 'phi_x_Tpa' or key == 'phi_x_Tpe':
                if key+',p' in self.data:
                    key = key+',t'
                    self.data[key] = np.array(phase)
                else:
                    key = key+',p'
                    self.data[key] = np.array(phase)
            self.data[key] = np.array(phase)

        # make spline fits #
        self.data_model = {}
        for key, data in self.data.items():
            if key != 'phases' and key != 'ky':
                self.data_model[key] = interp1d(self.data['phases'], data, kind='quadratic')

    def normalize_amplitudes(self):
        """ Calculate normlization required to make the 
        cross-phase integral equal to unity.
        """
        self.norms = {}
        for key, data in self.data.items():
            if key != 'phases' and key!= 'ky':
                self.norms[key] = 1./np.trapz(data, self.data['phases'])

    def get_average_phase(self, phase_key):
        """ Calculate the average cross phase for the specified phase key.

        Parameters
        ----------
        phase_key : str
           Specified phase key

        Returns
        -------
        phase_avg : float
            Integral averaged phase
        """
        phases = self.data['phases']
        amp = self.data[phase_key]
        idx_max = np.argmax(amp)
        phs_max = phases[idx_max]
        if phs_max >= 0.:
            idx_min = np.argmin(amp[0:idx_max])
            if idx_min > 0:
                amp = np.r_[amp[idx_min::], amp[0:idx_min]]
                phases = np.r_[phases[idx_min::], 2*np.pi+phases[0:idx_min]]
        else:
            idx_min = idx_max + np.argmin(amp[idx_max::])
            if idx_min < amp.shape[0]-1:
                amp = np.r_[amp[idx_min::], amp[0:idx_min]]
                phases = np.r_[-2*np.pi+phases[idx_min::], phases[0:idx_min]]

        norm_inv = 1./np.trapz(amp, phases)
        phase_avg = norm_inv * np.trapz(phases*amp, phases)
        phase_amp = amp[np.argmin(np.abs(phases - phase_avg))]

        return phase_avg, phase_amp

    def calc_mode_metric(self, exp_n=2):
        """ Calculate the mode metric.
        """
        phase = self.data['phases']
        sig2 = ((.5*np.pi)**2)/exp_n
        phase_wght = np.exp(-((.5*np.pi-phase)**2)/sig2)
        phase_norm = 1./np.trapz(phase_wght, phase)

        integ_dict = {}
        key_tag = [',p', ',t']
        key_base = ['phi_x_n', 'phi_x_Tpa', 'phi_x_Tpe']
        for i, ikey in enumerate(key_base):
            if not ikey in integ_dict:
                integ_dict[ikey] = np.empty(2)
            for j, jkey in enumerate(key_tag):
                phase_key = ikey+jkey
                phase_amp = self.data[phase_key]
                integ = np.trapz(phase_amp*phase_wght, phase)*phase_norm
                if jkey == ',t':
                    integ_dict[ikey][0] = integ
                elif jkey == ',p':
                    integ_dict[ikey][1] = integ

        metric_dict = {}
        for key, data in integ_dict.items():
            metric_dict[key] = (data[0]-data[1])/(data[0]+data[1])

        return metric_dict

    def plotting_parameters(self, fontsize=16, labelsize=18, linewidth=2):
        """ Define plotting parameters.

        Parameters
        ----------
        fontsize: int, optional
            Font size in figure. Default is 16.
        labelsize: int, optional
            Axis label size in figure. Default is 18.
        linewidth: int, optional
            Width of lines in figure. Default is 2.
        """
        plt.close('all')

        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': fontsize}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = labelsize
        mpl.rcParams['lines.linewidth'] = linewidth

    def plot_cross_phase(self, ax, phase_key):
        """ Plot cross phase data for the specified phase key.

        Parameters
        ----------
        ax : obj
            Matplotlib axis object.
        phase_key : str
            Dictionary key of specified cross phase data.
        """
        # Check norms #
        try:
            norm = self.norms[phase_key]
        except:
            norm = 1e10

        # Get maximum phase #
        idx = np.argmax(self.data[phase_key])
        max_phase = self.data['phases'][idx]/np.pi
        max_amp = norm*self.data[phase_key][idx]

        # Plot phase data #
        ax.plot(self.data['phases']/np.pi, norm*self.data[phase_key], c='k')
        # ax.plot([max_phase, max_phase], [0, max_amp], c='tab:red', ls=':')

        ax.set_xlim(self.data['phases'][0]/np.pi, self.data['phases'][-1]/np.pi)
        # ax.set_ylim(0, ax.get_ylim()[1])

    def fit_lorentzian(self, phase_key, ax=None):
        # Check norms #
        try:
            norm = self.norms[phase_key]
            norm_inv = 1./norm
        except:
            norm_inv = np.trapz(self.data[phase_key], self.data['phases'])
            norm = 1./norm_inv

        # identify grid maximum #
        phase_data = self.data[phase_key]*norm
        phase = self.data['phases']
        imax = np.argmax(phase_data)
        
        # get initial guess for Lorentz distribution #
        ph_avg, ph_amp = self.get_average_phase(phase_key)
        x0_init = ph_avg
        Gamma_init = 2./(np.pi*ph_amp*norm)
        
        # perform curve fit optimization #
        lorentz = lambda x, x0, Gamma: .5*Gamma/(np.pi*(((x-x0)**2) + ((.5*Gamma)**2)))
        popt, pcov = curve_fit(lorentz, phase, phase_data, p0=[x0_init, Gamma_init])
        x0, Gamma = popt
        L0 = (2.*norm_inv)/(np.pi*Gamma)

        if not ax is None:
            lorentz = lambda x: .5*Gamma/(np.pi*(((x-x0)**2) + ((.5*Gamma)**2)))
            x_dom = np.linspace(phase[0], phase[-1], 101)
            ax.plot(x_dom/np.pi, norm_inv*lorentz(x_dom), c='k', ls='--')

        return x0, L0

if __name__ == "__main__":
    file_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'linear_data', '1289-1-359', 'ky0p1', 'phasese_3.dat')
    phs = read_phases(file_path)
    phs.normalize_amplitudes()
    
    key_base = ['phi_x_n', 'phi_x_Tpa', 'phi_x_Tpe']
    key_tag = [',p', ',t']
    
    phs.plotting_parameters(fontsize=16, labelsize=18, linewidth=2)
    fig, axs = plt.subplots(3, 2, sharex=True, sharey=False, tight_layout=True, figsize=(12, 12))

    label_dict = {'phi_x_n,p': r'$\left(\tilde{\phi} \times \tilde{n}\right)_{\mathrm{p}}$',
                  'phi_x_n,t': r'$\left(\tilde{\phi} \times \tilde{n}\right)_{\mathrm{t}}$',
                  'phi_x_Tpa,p': r'$\left(\tilde{\phi} \times \tilde{T}_{\parallel}\right)_{\mathrm{p}}$',
                  'phi_x_Tpa,t': r'$\left(\tilde{\phi} \times \tilde{T}_{\parallel}\right)_{\mathrm{t}}$',
                  'phi_x_Tpe,p': r'$\left(\tilde{\phi} \times \tilde{T}_{\perp}\right)_{\mathrm{p}}$',
                  'phi_x_Tpe,t': r'$\left(\tilde{\phi} \times \tilde{T}_{\perp}\right)_{\mathrm{t}}$'}

    for i, base in enumerate(key_base):
        for j, tag in enumerate(key_tag):
            key = base+tag
            # phase_metric = phs.calc_mode_metric(key)

            ax = axs[i,j]
            phs.plot_cross_phase(ax, key)
            # phs.fit_lorentzian(key, ax=ax)

            # ph_avg = phs.get_average_phase(key)/np.pi
            # ax.plot([ph_avg, ph_avg], [0, ax.get_ylim()[1]], c='tab:red', ls=':')

            ax.set_ylabel(label_dict[key])
            # ax.set_title(r'$A(\overline{{\alpha}}) \sin(\overline{{\alpha}}) = {0:0.2f}$'.format(phase_metric))
            # ax.set_title(r'$\sin(\overline{{\alpha}}) = {0:0.2f}$'.format(phase_metric))

            ax.grid()

    axs[2,0].set_xlabel(r'$\alpha/\pi$')
    axs[2,1].set_xlabel(r'$\alpha/\pi$')

    fig.suptitle(r'QHS $(k_y \rho_{\mathrm{s}} = 0.4)$')
    plt.show()
