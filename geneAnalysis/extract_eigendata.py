import os
import numpy as np

def gen_diag(diag_path, data_path, runID):
    with open(diag_path, 'w') as f:
        f.write('startTime = 0.0\n'
                'endTime = 50000\n'
                'dataPath = '+data_path+'\n'
                'runIDs = '+runID+'\n'
                'plotType = pyplot\n'
                'diags = triplet')


# Get Configuration List #
con_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'config_list.txt')
with open(con_path, 'r') as f:
    conIDs = []
    for line in f.readlines():
        conIDs.append(line.strip())

run_tags = ['4', '5', '6', '7', '8']
for run_tag in run_tags:
    ky_vals = np.linspace(0.1, 1., 4)
    ky_tags = ['ky_{0:0.1f}'.format(ky).replace('.', 'p') for ky in ky_vals]

    diag_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'diag_alt.in')
    base_dirc = os.path.join('/mnt', 'GENE', 'linear_data')

    for cdx, conID in enumerate(conIDs):
        print(conID+' ({0:0.0f}|{1:0.0f})'.format(cdx+1, len(conIDs)))
        for ky_tag in ky_tags:
            print('    '+ky_tag)
            data_path = os.path.join(base_dirc, conID, ky_tag)
            runs = [f.name.split('_')[2] for f in os.scandir(data_path) if f.name.split('_')[0] == 'parameters' and f.name.split('_')[1] == run_tag]
            run = np.max(np.array(runs, dtype=int))
            runID = run_tag+'_{0:0.0f}'.format(run)
            eigen_path = os.path.join(data_path, 'eigendata_'+runID+'.h5')
            if not os.path.isfile(eigen_path):
                gen_diag(diag_path, data_path, runID)
                os.system("julia /home/michael/Desktop/julia_tools/genetools.jl/gen_eigendata_single.jl")
