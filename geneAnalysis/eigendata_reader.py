import os, sys
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import gistTools.gist_reader as gr


conID = '85-1-147'
ky_tag = 'ky_1p0'
runID = '1_1'

# Import Eigendata #
eigen_path = os.path.join('/mnt', 'GENE', 'linear_data', conID, ky_tag, 'eigendata_'+runID+'.h5')
with hf.File(eigen_path, 'r') as hf_:
    time = hf_['time'][()]
    bloon = hf_['ballooning angle'][()]
    eigen = hf_['eigenfunction'][0]

# Import GIST Data #
gist_path = os.path.join('/mnt', 'GENE', 'linear_data', conID, ky_tag, 'gist_'+runID)
# gist_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir', 'gist_files', 'gist_genet_wout_HSX_56-1-728_s0500_npol4_nz512_alpha0_bean')
gist = gr.read_gist(gist_path)

pol_dom = gist.pol_dom
modB = gist.data['modB']

# Import Simulation Parameters #
pram_path = os.path.join('/mnt', 'GENE', 'linear_data', conID, ky_tag, 'parameters_'+runID)
with open(pram_path, 'r') as f:
    lines = f.readlines()
    for line in lines:
        line = line.strip().split()
        if len(line) > 0 and line[0] == 'n_pol':
            npol = int(line[2])
        if len(line) > 0 and line[0] == 'nx0':
            nx0 = int(line[2])
        if len(line) > 0 and line[0] == 'nz0':
            nz0 = int(line[2])
        if len(line) > 0 and line[0] == 'lx':
            lx = float(line[2])

# Ballooning Partitions and Radial Wavenumbers #
if nx0 % 2 == 1:
    n_dom = np.linspace(-0.5*(nx0-1), 0.5*(nx0-1), nx0)
else:
    n_dom = np.linspace(-.5*(nx0-2), .5*(nx0-2), nx0)

balloon_angles = np.stack((2*n_dom-1, 2*n_dom+1), axis=1) * (npol*np.pi)
kx_dom = n_dom * (2*np.pi/lx)

eigen_dom = np.zeros(nz0)
theta_dom = np.linspace(-npol, npol, nz0)

for idx, balloon_range in enumerate(balloon_angles):
    z_dom = bloon[(bloon >= balloon_range[0]) & (bloon < balloon_range[1])]
    eigen_sub = eigen[(bloon >= balloon_range[0]) & (bloon < balloon_range[1])]
    eigen_dom = eigen_dom + (eigen_sub * np.conj(eigen_sub)).real

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22

fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(10, 6))
ax1 = ax.twinx()

ax.plot(theta_dom, eigen_dom, c='k')
ax1.plot(pol_dom, modB, c='tab:blue')

ax.set_xlabel(r'$\theta/\pi$')
ax.set_ylabel(r'$|\phi|^2$')

ax.set_xlim(theta_dom[0], theta_dom[-1])
ax.set_ylim(0, 1.05*np.max(eigen_dom))

plt.show()
