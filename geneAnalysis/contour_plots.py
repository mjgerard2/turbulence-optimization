import os
import string
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization')
sys.path.append(ModDir)
import plot_define as pd
import geneAnalysis.read_spectrum as rSpec
import geneAnalysis.read_nrg_summary as rNrg
import geneAnalysis.QL_model as QLm
import geneAnalysis.read_contours3 as rc

ModDir = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'Gerard_2025_turbulence', 'source')
sys.path.append(ModDir)
import data_dict as dd


# import contour data #
contour_key_qhs = 'QHS (aLn=2.0)'
time_qhs = [679, 4798]
cont_path = dd.cont_paths[contour_key_qhs]
pram_path = dd.pram_paths[contour_key_qhs]
cont = rc.contours(cont_path)
x_dom_qhs = cont.x_domain
y_dom_qhs = cont.y_domain
cont_qhs = np.empty((len(time_qhs), cont.nx, cont.ny))
for i, t in enumerate(time_qhs):
    t_idx = np.argmin(np.abs(cont.time_domain-t))
    cont_qhs[i] = cont.cont_data['phi'][t_idx]

# import contour data #
contour_key_he = 'HE-QHS (aLn=2.0)'
time_he = [747, 3944]
cont_path = dd.cont_paths[contour_key_he]
pram_path = dd.pram_paths[contour_key_he]
cont = rc.contours(cont_path)
x_dom_he = cont.x_domain
y_dom_he = cont.y_domain
cont_he = np.empty((len(time_he), cont.nx, cont.ny))
for i, t in enumerate(time_he):
    t_idx = np.argmin(np.abs(cont.time_domain-t))
    cont_he[i] = cont.cont_data['phi'][t_idx]

# plotting parameters #
plot = pd.plot_define(lineWidth=1.5)
plt = plot.plt
# fig, axs = plt.subplots(2, 2, figsize=(11, 8))
fig, axs = plt.subplots(1, 4, figsize=(24, 5))
ax1, ax2, ax3, ax4 = axs[0], axs[1], axs[2], axs[3]

# define colorbar boundaries #
ncolors = 21
bnd1 = np.linspace(-np.abs(cont_qhs[0]).max(), np.abs(cont_qhs[0]).max(), ncolors+1)
bnd2 = np.linspace(-np.abs(cont_he[0]).max(), np.abs(cont_he[0]).max(), ncolors+1)
bnd3 = np.linspace(-np.abs(cont_qhs[1]).max(), np.abs(cont_qhs[1]).max(), ncolors+1)
bnd4 = np.linspace(-np.abs(cont_he[1]).max(), np.abs(cont_he[1]).max(), ncolors+1)

# define keyword arguments #
phi_map = 'coolwarm'
cmap = plt.get_cmap(phi_map)
args1 = dict(cmap=phi_map, norm=mpl.colors.BoundaryNorm(bnd1, ncolors=cmap.N), extent=[x_dom_qhs.min(), x_dom_qhs.max(), y_dom_qhs.min(), y_dom_qhs.max()], origin='lower', aspect='auto')
args2 = dict(cmap=phi_map, norm=mpl.colors.BoundaryNorm(bnd2, ncolors=cmap.N), extent=[x_dom_he.min(), x_dom_he.max(), y_dom_he.min(), y_dom_he.max()], origin='lower', aspect='auto')
args3 = dict(cmap=phi_map, norm=mpl.colors.BoundaryNorm(bnd3, ncolors=cmap.N), extent=[x_dom_qhs.min(), x_dom_qhs.max(), y_dom_qhs.min(), y_dom_qhs.max()], origin='lower', aspect='auto')
args4 = dict(cmap=phi_map, norm=mpl.colors.BoundaryNorm(bnd4, ncolors=cmap.N), extent=[x_dom_he.min(), x_dom_he.max(), y_dom_he.min(), y_dom_he.max()], origin='lower', aspect='auto')

# plot contours #
smap1 = ax1.imshow(cont_qhs[0].T, **args1)
smap2 = ax2.imshow(cont_he[0].T, **args2)
smap3 = ax3.imshow(cont_qhs[1].T, **args3)
smap4 = ax4.imshow(cont_he[1].T, **args4)

# axis labels #
ax1.set_ylabel(r'$y \ / \ \rho_\mathrm{s}$')
ax1.set_xlabel(r'$x \ / \ \rho_\mathrm{s}$')
ax2.set_xlabel(r'$x \ / \ \rho_\mathrm{s}$')
ax3.set_xlabel(r'$x \ / \ \rho_\mathrm{s}$')
ax4.set_xlabel(r'$x \ / \ \rho_\mathrm{s}$')

# axis ticks #
maj_len = 10
min_len = 5
for ax in axs.flat:
    ax.tick_params(axis='both', which='major', direction='out', length=maj_len)
    ax.tick_params(axis='both', which='minor', direction='out', length=min_len)
    ax.xaxis.set_ticks_position('default')
    ax.yaxis.set_ticks_position('default')
    ax.xaxis.set_minor_locator(MultipleLocator(5))
    ax.yaxis.set_minor_locator(MultipleLocator(5))

# axis text #
x_txt, y_txt = 0.95, 0.95
props = dict(boxstyle='round', facecolor='w', alpha=0.75)
text1 = r'QHS: $t \ / \ (a/c_\mathrm{{s}})={}$'.format(time_qhs[0])
text2 = r'HE-QHS: $t \ / \ (a/c_\mathrm{{s}})={}$'.format(time_he[0])
text3 = r'QHS: $t \ / \ (a/c_\mathrm{{s}})={}$'.format(time_qhs[1])
text4 = r'HE-QHS: $t \ / \ (a/c_\mathrm{{s}})={}$'.format(time_he[1])
ax1.text(x_txt, y_txt, text1, transform=ax1.transAxes, va='top', ha='right', bbox=props)
ax2.text(x_txt, y_txt, text2, transform=ax2.transAxes, va='top', ha='right', bbox=props)
ax3.text(x_txt, y_txt, text3, transform=ax3.transAxes, va='top', ha='right', bbox=props)
ax4.text(x_txt, y_txt, text4, transform=ax4.transAxes, va='top', ha='right', bbox=props)

# colorbars #
cbar1 = fig.colorbar(smap1, ax=ax1)
cbar2 = fig.colorbar(smap2, ax=ax2)
cbar3 = fig.colorbar(smap3, ax=ax3)
cbar4 = fig.colorbar(smap4, ax=ax4)
cbar1.ax.set_ylabel(r'$\Phi_1$')
cbar2.ax.set_ylabel(r'$\Phi_1$')
cbar3.ax.set_ylabel(r'$\Phi_1$')
cbar4.ax.set_ylabel(r'$\Phi_1$')

# colorbar tick labels #
shft1 = 0.5*(bnd1[1]-bnd1[0])
tick1 = np.linspace(bnd1[0]+shft1, bnd1[-1]-shft1, int(.5*(ncolors-1)+1))
cbar1.set_ticks(tick1)
cbar1.set_ticklabels(['{0:0.2g}'.format(x) for x in tick1])
cbar1.ax.minorticks_off()
shft2 = 0.5*(bnd2[1]-bnd2[0])
tick2 = np.linspace(bnd2[0]+shft2, bnd2[-1]-shft2, int(.5*(ncolors-1)+1))
cbar2.set_ticks(tick2)
cbar2.set_ticklabels(['{0:0.2g}'.format(x) for x in tick2])
cbar2.ax.minorticks_off()
shft3 = 0.5*(bnd3[1]-bnd3[0])
tick3 = np.linspace(bnd3[0]+shft3, bnd3[-1]-shft3, int(.5*(ncolors-1)+1))
cbar3.set_ticks(tick3)
cbar3.set_ticklabels(['{0:0.2g}'.format(x) for x in tick3])
cbar3.ax.minorticks_off()
shft4 = 0.5*(bnd4[1]-bnd4[0])
tick4 = np.linspace(bnd4[0]+shft4, bnd4[-1]-shft4, int(.5*(ncolors-1)+1))
cbar4.set_ticks(tick4)
cbar4.set_ticklabels(['{0:0.2g}'.format(x) for x in tick4])
cbar4.ax.minorticks_off()

# subplots adjust #
# plt.subplots_adjust(left=0.085, right=0.975, bottom=0.084, top=0.98, wspace=0.2, hspace=0.2)
plt.subplots_adjust(left=0.04, right=0.985, bottom=0.14, top=0.96, wspace=0.2)

# show/save #
# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'APS_2024', 'figures', 'contours.pdf')
plt.savefig(save_path, format='pdf')
