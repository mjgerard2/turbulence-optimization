import os
import h5py as hf
import numpy as np

from scipy.signal import savgol_filter

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import plot_define as pd


class frequency_reader:
    """ A class for reading GENE frequency data from an NL simulation.

    ...

    Attributes
    ----------
    freq_path : str
        Global path to frequency file.

    Methods
    -------
    """
    def __init__(self, freq_path):
        with hf.File(freq_path, 'r') as hf_:
            # import spectral and frequency domians #
            self.ky_dom = hf_['ky'][()]
            self.omg_dom = hf_['omega'][()]
            self.num_of_ky = self.ky_dom.shape[0]
            self.num_of_omg = self.omg_dom.shape[0]

            # import electrostatic intensities #
            self.ky_amp = np.empty((self.num_of_ky, self.num_of_omg))
            ky0_tag = 'ky={0:0.2f}'.format(self.ky_dom[0]).replace('.',',')
            self.ky_amp[0] = hf_[ky0_tag][()]
            for key in hf_:
                if key.endswith(ky0_tag) and key != ky0_tag:
                    ky_key = key.split(','+ky0_tag)[0].split('ky=')[1]
                    ky_idx = round(float(ky_key.replace(',','.'))/self.ky_dom[0])-1
                    # self.ky_amp[ky_idx] = savgol_filter(hf_[key][()], 5, 2)
                    self.ky_amp[ky_idx] = hf_[key][()]

    def plot_ky(self, ky_val, filt=False):
        # get ky data #
        ky_idx = np.argmin(np.abs(self.ky_dom - ky_val))
        ky_freq = self.ky_amp[ky_idx]

        # plotting parameters #
        plot = pd.plot_define()
        plt = plot.plt
        fig, ax = plt.subplots(1, 1, tight_layout=True)

        # plot data #
        ax.plot(self.omg_dom, ky_freq, c='k')

        # axis limits #
        # ax.set_xlim(self.omg_dom[0], self.omg_dom[-1])
        ax.set_xlim(-1, 1)
        ax.set_ylim(0, ax.get_ylim()[1])
        ax.plot([-0.0]*2, ax.get_ylim(), c='k', ls='--')

        # axis labels #
        ax.set_xlabel(r'$\omega \ / \ (c_\mathrm{s}/a)$')
        ax.set_ylabel(r'$\langle |\phi|^2 \rangle_\mathrm{t}$')

        plt.show()

    def plot_ky_set(self, ky_vals, filt=False):
        # get ky data #
        ky_freq = np.empty((ky_vals.shape[0], self.num_of_omg))
        for i, ky_val in enumerate(ky_vals):
            ky_idx = np.argmin(np.abs(self.ky_dom - ky_val))
            ky_freq[i] = self.ky_amp[ky_idx]

        # plotting parameters #
        plot = pd.plot_define()
        plt = plot.plt
        fig, ax = plt.subplots(1, 1, tight_layout=True)

        # plot data #
        for i, ky_val in enumerate(ky_vals):
            ax.plot(self.omg_dom, ky_freq[i], label=r'$k_\mathrm{{y}}\rho_\mathrm{{s}} = {}$'.format(np.format_float_positional(ky_val)))

        # axis limits #
        # ax.set_xlim(self.omg_dom[0], self.omg_dom[-1])
        ax.set_xlim(-1, 1)
        ax.set_ylim(0, ax.get_ylim()[1])

        # axis labels #
        ax.set_xlabel(r'$\omega \ / \ (c_\mathrm{s}/a)$')
        ax.set_ylabel(r'$\langle |\phi|^2 \rangle_\mathrm{t}$')

        ax.legend()
        plt.show()


    def plot_frequency_spectrum(self, show=True):
        # plotting parameters #
        plot = pd.plot_define()
        plt = plot.plt
        fig, ax = plt.subplots(1, 1, tight_layout=True)

        # plot data #
        smap = ax.pcolormesh(self.ky_dom, self.omg_dom, self.ky_amp.T, cmap='magma', shading='gouraud')
        ax.plot(self.ky_dom, np.zeros(self.num_of_ky), c='w')

        # axial scales #
        # ax.set_xscale('log')
        ax.set_xlim(self.ky_dom[0], 1.4)
        ax.set_ylim(-0.75, 0.75)

        # label axes #
        ax.set_xlabel(r'$k_\mathrm{y} \rho_\mathrm{s}$')
        ax.set_ylabel(r'$\omega \ / \ (c_\mathrm{s}/a)$')
        cbar = fig.colorbar(smap, ax=ax)
        cbar.ax.set_ylabel(r'$\langle|\phi|^2\rangle_\mathrm{t}$')

        # save/show #
        if show:
            plt.show()
        else:
            return plt, fig, ax

if __name__ == '__main__':
    # freq_path = os.path.join('/mnt', 'GENE', 'nonlinear_data','60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'frequency_03_dfout.h5')
    freq_path = os.path.join('/mnt', 'GENE', 'nonlinear_data','60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'frequency_11.h5')
    freq = frequency_reader(freq_path)
    plt, fig, ax = freq.plot_frequency_spectrum(show=False)
    save_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'figures', 'frequency_11_lin-ky.pdf')
    plt.savefig(save_path, format='pdf')
