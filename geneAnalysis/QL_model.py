import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import geneAnalysis.read_spectrum as rSpec
import geneAnalysis.read_nrg_summary as rNrg


class QL_Data():

    def __init__(self, ky_dom, eigen_paths, kperp_paths):
        self.ky_vals = ky_dom
        self.ky_tags = []
        self.QL_dict = {}
        for ky in ky_dom:
            # define data paths #
            ky_tag = 'ky{0:0.1f}'.format(ky).replace('.','p')
            self.ky_tags.append(ky_tag)
            eigen_path = eigen_paths[ky_tag]  # os.path.join(base_dir, ky_tag, 'eigenvalues_{}'.format(run_id))
            kperp_path = kperp_paths[ky_tag]  # os.path.join(base_dir, ky_tag, 'kperpe_nconn{}_{}.dat'.format(nconn, run_id))

            # import eigendata #
            eigen_data = []
            with open(eigen_path, 'r') as f:
                lines = f.readlines()
                for line in lines[1::]:
                    line_strip = line.strip().split()
                    line_data = [float(x) for x in line_strip]
                    if line_data[0] > 0:
                        eigen_data.append(line_data[0])
                    else:
                        break
            eigen_data = np.array(eigen_data).reshape(len(eigen_data), 1)

            # import kperp data #
            ql_data = np.empty((eigen_data.shape[0], 3))
            with open(kperp_path, 'r') as f:
                lines = f.readlines()
                for ldx, line in enumerate(lines):
                    line_strip = line.strip().split()
                    if line_strip[0] != '#':
                        break
                for i, line in enumerate(lines[ldx:ldx+ql_data.shape[0]]):
                    line_strip = line.strip().split()
                    line_data = [float(x) for x in line_strip]
                    ql_data[i] = np.append(eigen_data[i], line_data[1:3])

            self.QL_dict[ky_tag] =  ql_data

    def plotting_parameters(self, fontsize=16, labelsize=18, linewidth=2):
        plt.close('all')

        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': fontsize}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = labelsize
        mpl.rcParams['lines.linewidth'] = linewidth

    def ky_sums_wNL(self, nrg_path, spec_path, pram_path, time_dom, ax=None, with_weights=True):
        # import nonlinear nrg data # 
        nrg = rNrg.nrg_summary(nrg_path)
        nrg.nrg_average(time_dom[0], end_time=time_dom[1])
        nl_heat_flux = nrg.time_average['Q_tot']

        # import nonlinear spectral data #
        spec = rSpec.spectrum(spec_path, pram_path)
        spec.spectrum_average(time_dom[0], end_time=time_dom[1])
        ky_nl = spec.ky_domain
        spec_nl = spec.spec_data_avg['ky']['Q_es']

        # sum over unstable modes #
        self.Sk = np.empty(len(self.ky_vals))
        self.ky_wgt_sums = np.empty(len(self.ky_vals))
        for i, ky_tag in enumerate(self.ky_tags):
            ky_val = self.ky_vals[i]
            ky_nl_idx = np.argmin(np.abs(ky_nl - ky_val))
            if np.min(np.abs(ky_nl-ky_val)) > 1e-7:
                raise ValueError('Mismatch in wavenumber resolution between nonlinear and linear simulations. Mismatch identified at ky={}.'.format(ky_val))
            data = self.QL_dict[ky_tag]
            if with_weights:
                self.ky_wgt_sums[i] = np.sum(data[:,0]*data[:,2]/data[:,1])
            else:
                self.ky_wgt_sums[i] = np.sum(data[:,0]/data[:,1])
            self.Sk[i] = spec_nl[ky_nl_idx]/self.ky_wgt_sums[i]

        # sum over wavenumbers #
        ql_heat_flux = np.sum(self.Sk*self.ky_wgt_sums)
        self.C = nl_heat_flux/ql_heat_flux
        self.ql_heat_flux = self.C * ql_heat_flux

        # plot spectrum #
        if not ax is None:
            ax.plot(self.ky_vals, self.Sk*self.ky_wgt_sums, c='k', ls='--', marker='s', mfc='None', mec='k', ms=10, label=r'$S_k\sum_{j} (w_{kj}\gamma_{kj})/\langle k_{\perp}^2 \rangle_{kj}$')
            ax.plot(ky_nl, spec_nl, c='k', label=r'$\langle \hat{Q}_{\mathrm{es}} \rangle$')

    def ky_sums_woNL(self, Sk, C, ax=None, with_weights=True):
        self.ky_wgt_sums = np.empty(len(self.ky_vals))
        for i, ky_tag in enumerate(self.ky_tags):
            data = self.QL_dict[ky_tag]
            if with_weights:
                self.ky_wgt_sums[i] = np.sum(data[:,0]*data[:,2]/data[:,1])
            else:
                self.ky_wgt_sums[i] = np.sum(data[:,0]/data[:,1])
        self.ql_heat_flux = np.sum(C*Sk*self.ky_wgt_sums)


if __name__ == "__main__":
    ky_dom = np.linspace(0.4, 2.4, 6)

    time_dom = [250, -1]
    base_path = os.path.join('/mnt', 'GENE', 'linear_data', '60-1-84', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0')
    nrg_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_2.h5')
    spec_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'fluxspectrae_1_2.dat')
    pram_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'parameters_1')

    QL_qhs = QL_Data(base_path, ky_dom)
    QL_qhs.plotting_parameters()
    fig, ax = plt.subplots(1, 1, tight_layout=True)
    QL_qhs.ky_sums_wNL(nrg_path, spec_path, pram_path, time_dom, ax=ax)

    """
    ky_wgt_sums_qhs = QL_qhs.ky_wgt_sums
    Sk_qhs = QL_qhs.Sk
    C_qhs = QL_qhs.C

    base_path = os.path.join('/mnt', 'GENE', 'linear_data', '60-1-84', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0')
    QL_he = QL_Data(base_path, ky_dom)
    QL_he.ky_sums_woNL(Sk_qhs, C_qhs)

    """ 
    ax.set_xlabel(r'$k_y\rho_s$')

    ax.set_xlim(0, ax.get_xlim()[1])
    ax.set_ylim(0, ax.get_ylim()[1])
    ax.set_title('HE-QHS')

    ax.grid()
    ax.legend()
    plt.show()
    save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'personal_meetings', 'MJ', '20230816', 'figures', 'QL_spectrum_he-qhs.png')
    # plt.savefig(save_path)
