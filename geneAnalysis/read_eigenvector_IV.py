import os, sys
import h5py as hf
import numpy as np

from scipy.interpolate import interp1d
from scipy.integrate import quadrature

import matplotlib as mpl
import matplotlib.pyplot as plt

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import geneAnalysis.read_parameters as rp
import gistTools.gist_reader as gr

class read_eigendata:
    """ A class for reading GENE hdf5 eigendata data files.

    ...

    Attributes
    ----------
    eigen_path : str
        Global path to eigendata summary file.

    Methods
    -------
    nrg_average(start_time, end_time=-1)
        Calculate time average of nonlinear quantities.
    """
    def __init__(self, eigen_path, param_path):

        if eigen_path.endswith('h5'):
            self.time_series = {}
            with hf.File(eigen_path, 'r') as hf_:
                self.time = hf_['time'][()]
                self.pol_dom = hf_['ballooning angle'][()]
                self.complex_phi = hf_['eigenfunction'][0]

        elif eigen_path.endswith('dat'):
            with open(eigen_path, 'r') as f:
                lines = f.readlines()
                for l, line in enumerate(lines):
                    line_split = line.strip().split()
                    if line_split[0] == 'theta_M':
                        l += 1
                        break
                
                npts = len(lines)-l
                self.time = -1
                self.pol_dom = np.empty(npts)
                self.complex_phi = np.empty(npts, dtype=complex)
                for i, line in enumerate(lines[l::]):
                    line_arr = [float(x) for x in line.strip().split()]
                    self.pol_dom[i] = line_arr[0]*np.pi
                    self.complex_phi[i] = complex(line_arr[1], line_arr[2])

        else:
            raise KeyError(eigen_path+' is not a valid file type.')

        param = rp.read_file(param_path)
        self.nx0 = int(param.data_dict['nx0'])
        self.nz0 = int(param.data_dict['nz0'])

        self.amplitude = np.sqrt((self.complex_phi*np.conj(self.complex_phi)).real)

        # add buffer points for interpolation #
        dpol = self.pol_dom[1]-self.pol_dom[0]
        pol_buff = np.insert(self.pol_dom, 0, self.pol_dom[0]-dpol)
        pol_buff = np.append(pol_buff, self.pol_dom[-1]+dpol)

        amp_fit = np.insert(self.amplitude, 0, 0)
        amp_fit = np.append(amp_fit, 0)

        phi_real_fit = np.insert(self.complex_phi.real, 0, 0)
        phi_real_fit = np.append(phi_real_fit, 0)

        phi_imag_fit = np.insert(self.complex_phi.imag, 0, 0)
        phi_imag_fit = np.append(phi_imag_fit, 0)

        self.amplitude_model = interp1d(pol_buff, amp_fit)
        self.real_phi_model = interp1d(pol_buff, phi_real_fit)
        self.imag_phi_model = interp1d(pol_buff, phi_imag_fit)

    def normalize_phi(self, pol_dom, method='integral'):
        """ Normalize the eigenfunction amplitude, with the normalization
        factor calculated over the specified poloidal domain.

        Parameters
        ----------
        pol_dom : arr
            Poloidal domain over which normalization is performed.
        """
        if method == 'integral':
            eigen_amp = self.amplitude_model(pol_dom)
            self.norm_inv = 1/np.trapz(eigen_amp, x=pol_dom)
        elif method == 'maximum':
            eigen_amp = self.amplitude_model(pol_dom)
            print(np.max(eigen_amp))
            self.norm_inv = 1./np.max(eigen_amp)

        self.amplitude = self.amplitude * self.norm_inv
        self.amplitude_model = interp1d(self.pol_dom, self.amplitude)

        self.complex_phi = self.complex_phi * self.norm_inv

        self.real_phi_model = interp1d(self.pol_dom, self.complex_phi.real)
        self.imag_phi_model = interp1d(self.pol_dom, self.complex_phi.imag)

    def average_bar_sqrd(self, pol_dom):
        """" Calculate $overline{|phi|^2}$ over the specified poloidal domain.

        Parameters
        ----------
        pol_dom : arr
            Poloidal domain over which average is performed.
        """
        eigen_amp = self.amplitude_model(pol_dom)
        return np.trapz(eigen_amp, pol_dom) / (pol_dom[-1] - pol_dom[0])

    def average_sqrd_bar(self, gist, method='gtrapz'):
        """" Calculate $|overline{phi}|^2$ over the specified poloidal domain.

        Parameters
        ----------
        gist : obj
            Data object generated from the read_gist method from the gistTools
            module.
        method : str, optional
            Integration method for bounce averaging. Options are the general
            trapezoidal rule (gtrapz) or quadrature integration (quad). 
            Default is gtrapz.
        """
        # identify buffer domain on left and right boundaries #
        ilft, irgt = int(.5*(self.nx0-1))*self.nz0, int(.5*(self.nx0-1)+1)*self.nz0
        pol_cent = self.pol_dom[ilft:irgt]
        try:
            pol_gist = gist.pol_dom_buff
        except AttributeError:
            gist.partition_wells()
            pol_gist = gist.pol_dom_buff

        Dpol = pol_cent[1] - pol_cent[0]
        npol_buff_lft = int(np.ceil((pol_cent[0]-pol_gist[0])/Dpol))
        npol_buff_rgt = int(np.ceil((pol_gist[-1]-pol_cent[-1])/Dpol))
        
        pol_buff_lft = np.flip(pol_cent[0] - Dpol*(1+np.arange(npol_buff_lft)))
        pol_buff_rgt = pol_cent[-1] + Dpol*(1+np.arange(npol_buff_rgt))

        # calculate trapping regions of k #
        k2_sets = []
        B_dom = gist.model['modB'](pol_gist)
        B_extrema = np.array([np.min(B_dom), np.max(B_dom)])
        k2_dom = np.linspace(0, 1, 101, endpoint=False)
        for k2 in k2_dom:
            k2_set = gist.calc_Vpar(pol_gist, B_dom, k2, B_extrema)
            pol_set, Vpar_set = [], []
            for pdx, pol in enumerate(k2_set[0]):
                if pol is not None:
                    pol_set.append(pol)
                    Vpar_set.append(k2_set[1][pdx])
            k2_sets.append([pol_set, Vpar_set])

        # loop over ballooning angle domains #
        self.avg_phi_sqrd = np.zeros(self.pol_dom.shape)
        for i in range(self.nx0):
            # shift ballooning domain to poloidal domain in gist file #
            ibeg, iend = i*self.nz0, (i+1)*self.nz0, 
            pol_phi = self.pol_dom[ibeg:iend]
            pol_shft = pol_phi-pol_phi[0] + self.pol_dom[ilft]
            if i == 0:
                pol_buff = np.append(pol_shft, pol_buff_rgt)
                pol_interp = pol_buff+pol_phi[0]-self.pol_dom[ilft]
                real_phi = self.real_phi_model(pol_interp)
                imag_phi = self.imag_phi_model(pol_interp)
                pol_buff = np.insert(pol_buff, 0, pol_buff_lft)

                zero_buff = np.zeros(pol_buff_lft.shape)
                real_phi = np.insert(real_phi, 0, zero_buff)
                imag_phi = np.insert(imag_phi, 0, zero_buff)

            elif i == self.nx0-1:
                pol_buff = np.insert(pol_shft, 0, pol_buff_lft)
                pol_interp = pol_buff+pol_phi[0]-self.pol_dom[ilft]
                real_phi = self.real_phi_model(pol_interp)
                imag_phi = self.imag_phi_model(pol_interp)
                pol_buff = np.append(pol_buff, pol_buff_rgt)

                zero_buff = np.zeros(pol_buff_rgt.shape)
                real_phi = np.append(real_phi, zero_buff)
                imag_phi = np.append(imag_phi, zero_buff)

            else:
                pol_buff = np.insert(pol_shft, 0, pol_buff_lft)
                pol_buff = np.append(pol_buff, pol_buff_rgt)
                pol_interp = pol_buff+pol_phi[0]-self.pol_dom[ilft]
                real_phi = self.real_phi_model(pol_interp)
                imag_phi = self.imag_phi_model(pol_interp)

            real_phi_model = interp1d(pol_buff, real_phi)
            imag_phi_model = interp1d(pol_buff, imag_phi)

            # calculate bounce average #
            well = []
            for k2_set in k2_sets:
                pol_set, Vpar_set = k2_set
                for pdx, pol in enumerate(pol_set):
                    v_par = Vpar_set[pdx]
                    B = gist.model['modB'](pol)
                    jacob = gist.model['Jacob'](pol)
                            
                    real_phi = real_phi_model(pol)
                    imag_phi = imag_phi_model(pol)
                    if method == 'quad':
                        tau_integ = B*jacob/v_par
                        real_integ = tau_integ*real_phi
                        imag_integ = tau_integ*imag_phi

                        tau_integ_model = interp1d(pol, tau_integ, kind='quadratic')
                        real_integ_model = interp1d(pol, real_integ, kind='quadratic')
                        imag_integ_model = interp1d(pol, imag_integ, kind='quadratic')

                        tau = quadrature(tau_integ_model, pol[0], pol[-1], maxiter=5000)[0]
                        real = quadrature(real_integ_model, pol[0], pol[-1], maxiter=5000)[0]
                        imag = quadrature(imag_integ_model, pol[0], pol[-1], maxiter=5000)[0]

                        real_avg = real/tau
                        imag_avg = imag/tau
                    
                    elif method == 'gtrapz':
                        tau_integ = B*jacob
                        real_integ = tau_integ*real_phi
                        imag_integ = tau_integ*imag_phi

                        dpol = pol[1:]-pol[0:-1]
                        dv2 = v_par[1:]**2-v_par[0:-1]**2
                        dv3 = v_par[1:]**3-v_par[0:-1]**3

                        dh_tau = tau_integ[1:]-tau_integ[0:-1]
                        dh_real = real_integ[1:]-real_integ[0:-1]
                        dh_imag = imag_integ[1:]-imag_integ[0:-1]
                        
                        dhv_tau = tau_integ[1:]*v_par[1:]-tau_integ[0:-1]*v_par[0:-1]
                        dhv_real = real_integ[1:]*v_par[1:]-real_integ[0:-1]*v_par[0:-1]
                        dhv_imag = imag_integ[1:]*v_par[1:]-imag_integ[0:-1]*v_par[0:-1]

                        tau = np.empty(dpol.shape)
                        real = np.empty(dpol.shape)
                        imag = np.empty(dpol.shape)

                        v_thresh = 1e-10
                        s_idx = np.where((np.abs(dv2) < v_thresh) & (np.abs(dv3) < v_thresh))[0]
                        nos_idx = np.where((np.abs(dv2) >= v_thresh) & (np.abs(dv3) >= v_thresh))[0]

                        tau[nos_idx] = (2*dpol[nos_idx]*dhv_tau[nos_idx]/dv2[nos_idx]) - (4./3)*(dpol[nos_idx]*dh_tau[nos_idx]*dv3[nos_idx]/(dv2[nos_idx]**2))
                        real[nos_idx] = (2*dpol[nos_idx]*dhv_real[nos_idx]/dv2[nos_idx]) - (4./3)*(dpol[nos_idx]*dh_real[nos_idx]*dv3[nos_idx]/(dv2[nos_idx]**2))
                        imag[nos_idx] = (2*dpol[nos_idx]*dhv_imag[nos_idx]/dv2[nos_idx]) - (4./3)*(dpol[nos_idx]*dh_imag[nos_idx]*dv3[nos_idx]/(dv2[nos_idx]**2))

                        tau[s_idx] = .5*dpol[s_idx]*(tau_integ[0:-1]/v_par[0:-1]+tau_integ[1:]/v_par[1:])[s_idx]
                        real[s_idx] = .5*dpol[s_idx]*(real_integ[0:-1]/v_par[0:-1]+real_integ[1:]/v_par[1:])[s_idx]
                        imag[s_idx] = .5*dpol[s_idx]*(imag_integ[0:-1]/v_par[0:-1]+imag_integ[1:]/v_par[1:])[s_idx]

                        tau = np.sum(tau)
                        real_avg = np.sum(real)/tau
                        imag_avg = np.sum(imag)/tau

                    well.append(np.array([pol[0], real_avg, imag_avg]))
                    well.append(np.array([pol[-1], real_avg, imag_avg]))
            
            # order bounce average are broadcast to object variable #
            well = np.array(sorted(well, key=lambda x: x[0]))
            pol_fit, uniq_idx = np.unique(well[:,0], return_index=True)
            pol_fit = np.insert(pol_fit, 0, pol_shft[0]-Dpol)
            pol_fit = np.append(pol_fit, pol_shft[-1]+Dpol)
            avg_phi_sqrd = well[uniq_idx,1]**2 + well[uniq_idx,2]**2
            if i == 0:
                avg_phi_sqrd = np.insert(avg_phi_sqrd, 0, 0)
                phi_ext = (avg_phi_sqrd[-1]*(pol_fit[-1]-pol_fit[-3])-avg_phi_sqrd[-2]*(pol_fit[-1]-pol_fit[-2]))/(pol_fit[-2]-pol_fit[-3])
                avg_phi_sqrd = np.append(avg_phi_sqrd, phi_ext)
            elif i == self.nx0-1:
                avg_phi_sqrd = np.insert(avg_phi_sqrd, 0, self.avg_phi_sqrd[ibeg-1])
                avg_phi_sqrd = np.append(avg_phi_sqrd, 0)
            else:
                avg_phi_sqrd = np.insert(avg_phi_sqrd, 0, self.avg_phi_sqrd[ibeg-1])
                phi_ext = (avg_phi_sqrd[-1]*(pol_fit[-1]-pol_fit[-3])-avg_phi_sqrd[-2]*(pol_fit[-1]-pol_fit[-2]))/(pol_fit[-2]-pol_fit[-3])
                avg_phi_sqrd = np.append(avg_phi_sqrd, phi_ext)

            avg_phi_sqrd_model = interp1d(pol_fit, avg_phi_sqrd)
            self.avg_phi_sqrd[ibeg:iend] = avg_phi_sqrd_model(pol_shft)

        # generate model fit #
        self.avg_phi_sqrd = self.avg_phi_sqrd/np.nanmax(self.avg_phi_sqrd)
        self.avg_phi_sqrd_model = interp1d(self.pol_dom, self.avg_phi_sqrd)

    def plotting(self, fontsize=14, labelsize=18, linewidth=1):
        """ Define plotting parameters 

        Parameters
        ----------
        fontsize : int, optional
            Figure font size. Default is 14.

        labelsize : int, optional
            Figure label size. Default is 18.

        linewidth : int, optional
            Figure line width. Degault is 1.
        """
        plt.close('all')

        font = {'family': 'sans-serif',
                'serif': 'Times New Roman',
                'weight': 'normal',
                'size': fontsize}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = labelsize
        mpl.rcParams['lines.linewidth'] = linewidth


if __name__ == "__main__":
    config_id = '0-1-0'
    eigen_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'linear_data', config_id, 'ky0p1', 'balle_12.dat')
    params_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'linear_data', config_id, 'ky0p1', 'parameters_12')
    eigen = read_eigendata(eigen_path, params_path)
    
    main_id = 'main_coil_%s' % config_id.split('-')[0]
    set_id = 'set_%s' % config_id.split('-')[1]
    job_id = 'job_%s' % config_id.split('-')[2]
    gist_dir = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
    gist_files = [f.name for f in os.scandir(gist_dir) if f.name.endswith('res512_npol16.dat')]
    gist_names = [name for name in gist_files if name.split('_')[2] == 'mn1824']
    s_tag = gist_names[0].split('_')[4]
    if config_id == '0-1-0':
        gist_path = os.path.join(gist_dir, 'gist_QHS_mn1824_alf0_%s_res512_npol16.dat' % (s_tag))
    else:
        gist_path = os.path.join(gist_dir, 'gist_%s_mn1824_alf0_%s_res512_npol16.dat' % (config_id, s_tag))
    gist = gr.read_gist(gist_path)
    # gist.partition_wells()
    eigen.average_sqrd_bar(gist)

    eigen.plotting()
    fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(14, 6))
    ax1 = axs[0]
    ax2 = axs[1]

    amp_sqrd = eigen.amplitude**2
    ax1.plot(eigen.pol_dom/np.pi, amp_sqrd/np.max(amp_sqrd), c='k')
    ax2.plot(eigen.pol_dom/np.pi, eigen.avg_phi_sqrd, c='k')

    ax1.set_ylabel(r'$|\phi|^2$')
    ax2.set_xlabel(r'$\theta_\mathrm{b} \ / \ \pi$')
    ax2.set_ylabel(r'$|\overline{\phi}|^2$')

    ax1.set_xlim(eigen.pol_dom[0]/np.pi, eigen.pol_dom[-1]/np.pi)
    ax1.set_ylim(0, ax1.set_ylim()[1])
    ax2.set_ylim(0, ax2.set_ylim()[1])

    plt.show()
