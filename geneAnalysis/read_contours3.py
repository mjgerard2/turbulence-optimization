import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as ani

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd


class contours:
    """ A class for reading GENE dat contout data files.

    ...

    Attributes
    ----------
    cont_path : str
        Global path to contour file.

    Methods
    -------
    mean_nrg(start_time, end_time=-1)
        Calculate time average of nonlinear quantities.
    """
    def __init__(self, cont_path):
        # Check if input files exsit #
        if not os.path.isfile(cont_path):
            raise IOError(cont_path+' does not exist.')

        with open(cont_path, 'r') as f:
            lines = f.readlines()

            # identify beginning line index #
            for l, line in enumerate(lines):
                line_split = line.strip().split()
                if line_split[0] != '#':
                    lbeg = l
                    break
            
            # Read in x grid #
            x_dom = []
            for l, line in enumerate(lines[lbeg:]):
                line_split = line.strip().split()
                if len(line_split) == 0:
                    lbeg += l+3
                    break
                else:
                    x_dom.append(float(line_split[0]))
            self.x_domain = np.array(x_dom)
            self.nx = self.x_domain.shape[0]

            # Read in y grid #
            y_dom = []
            for l, line in enumerate(lines[lbeg:]):
                line_split = line.strip().split()
                if len(line_split) == 0:
                    lbeg += l+3
                    break
                else:
                    y_dom.append(float(line_split[0]))
            self.y_domain = np.array(y_dom)
            self.ny = self.y_domain.shape[0]

            # Read in time array #
            t_dom = []
            for l, line in enumerate(lines[lbeg:]):
                line_split = line.strip().split()
                if len(line_split) == 0:
                    lbeg += l+4
                    break
                else:
                    t_dom.append(float(line_split[0]))
            self.time_domain = np.array(t_dom)
            self.nt = self.time_domain.shape[0]

            # read in contour data #
            cont_phi = np.empty((self.nt, self.nx, self.ny))
            cont_den = np.empty((self.nt, self.nx, self.ny))
            for i in range(2, self.time_domain.shape[0]):
                lphi = lbeg + i*(2*self.nx+8)
                lden = lphi + self.nx + 4
                phi = np.empty((self.nx, self.ny))
                for l, line in enumerate(lines[lphi:lphi+self.nx]):
                    phi_l = [float(x) for x in line.strip().split()]
                    phi[l] = phi_l
                den = np.empty((self.nx, self.ny))
                for l, line in enumerate(lines[lden:lden+self.nx]):
                    den[l] = [float(x) for x in line.strip().split()]
                cont_phi[i] = phi
                cont_den[i] = den
            self.cont_data = {'phi': cont_phi, 'den': cont_den}

    def plot_all_time_steps(self, save_dirc):
        phi_sprd = 3*np.std(self.cont_data['den'])
        for t_idx, t_val in enumerate(self.time_domain):
            plot = pd.plot_define()

            plt = plot.plt
            fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

            smap = ax.pcolormesh(self.x_domain, self.y_domain, self.cont_data['den'][t_idx].T, vmin=-phi_sprd, vmax=phi_sprd, cmap='PiYG', shading='gouraud')

            # Axis Labels #
            cax = fig.colorbar(smap, ax=ax)
            cax.ax.set_ylabel(r'$n$')

            ax.set_xlabel(r'$x/\rho_s$')
            ax.set_ylabel(r'$y/\rho_s$')

            # ax.set_title(r'$\beta = 5\times 10^{-5}$')
            ax.set_title(r'QHS: $a/L_{\mathrm{n}} = 2.0$')
            # ax.set_title(r'LE2: $t={0:0.0f} \ (a/c_s)$'.format(self.time_domain[t_idx]))
            
            props = dict(boxstyle='round', facecolor='w', alpha=0.75)
            #ax.text(.9*ax.get_xlim()[0], .87*ax.get_ylim()[0],
            #        r'$t = {0:0.0f} \ (a/c_s)$'.format(t_val),
            #        bbox=props)
            ax.text(0.96, 0.04, r'$t = {0:0.0f} \ (a/c_s)$'.format(t_val), transform=ax.transAxes, va='bottom', ha='right', bbox=props)
            
            save_path = os.path.join(save_dirc, 'time_index_{0:0.0f}.png'.format(t_idx))
            plt.savefig(save_path)


if __name__ == '__main__':
    cont_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'conte_3_11.dat')
    cont = contours(cont_path)
    save_dir = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'figures', 'density_contours')
    cont.plot_all_time_steps(save_dir)
    """
    cont_phi = cont.cont_data['phi']
    cont_n = cont.cont_data['n']

    phi_sprd = 3*np.std(cont_phi)
    n_sprd = 4*np.std(cont_n)

    x_dom = cont.x_domain
    y_dom = cont.y_domain

    # Define Plotting Parameters #
    plot = pd.plot_define()
    plt = plot.plt
    """
    if False:
        # Plot Phi Fluctuations #
        fig, ax = plt.subplots(1, 1, sharex=True, sharey=True, tight_layout=True, figsize=(8, 6))

        # idx = int(.75*cont_phi.shape[0])-1
        idx = int(.8*cont_phi.shape[0])-1
        map = ax.pcolormesh(x_dom, y_dom, cont_phi[idx].T, vmin=-phi_sprd, vmax=phi_sprd, cmap='seismic', shading='gouraud')

        # Axis Labels #
        cax = fig.colorbar(map, ax=ax)
        cax.ax.set_ylabel(r'$\phi$')

        ax.set_xlabel(r'$x / \rho_s$')
        ax.set_ylabel(r'$y / \rho_s$')

        ax.set_title(r'LE2:  $t = {0:0.0f} \ (c_s/a)$'.format(cont.time_domain[idx]))

        save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Prelim_Defense', 'figures', '5-1-485_contour.png')
        # plt.show()
        plt.savefig(save_path)

    if False:
        # Construct Plotting Axis #
        fig, axs = plt.subplots(2, 1, sharex=True, sharey=True, tight_layout=True, figsize=(8, 12))

        ax1 = axs[0]
        ax2 = axs[1]

        map1 = ax1.pcolormesh(x_dom, y_dom, cont_phi[-1].T, vmin=-phi_sprd, vmax=phi_sprd, cmap='jet', shading='gouraud')
        map2 = ax2.pcolormesh(x_dom, y_dom, cont_n[-1].T, vmin=-n_sprd, vmax=n_sprd, cmap='jet', shading='gouraud')

        # Axis Labels #
        cax1 = fig.colorbar(map1, ax=ax1)
        cax1.ax.set_ylabel(r'$\tilde{\phi}$')

        cax2 = fig.colorbar(map2, ax=ax2)
        cax2.ax.set_ylabel(r'$\tilde{n}$')

        ax1.set_ylabel(r'$y / \rho_s$')
        ax2.set_ylabel(r'$y / \rho_s$')
        ax2.set_xlabel(r'$x / \rho_s$')

        plt.show()

    if False:
        fig, axs = plt.subplots(2, 1, sharex=True, sharey=True, tight_layout=True, figsize=(8, 12))

        ax1 = axs[0]
        ax2 = axs[1]

        map1 = ax1.pcolormesh(x_dom, y_dom, cont_phi[0].T, vmin=-phi_sprd, vmax=phi_sprd, cmap='jet', shading='gouraud')
        map2 = ax2.pcolormesh(x_dom, y_dom, cont_n[0].T, vmin=-n_sprd, vmax=n_sprd, cmap='jet', shading='gouraud')

        # Axis Labels #
        cax1 = fig.colorbar(map1, ax=ax1)
        cax1.ax.set_ylabel(r'$\tilde{\phi}$')

        cax2 = fig.colorbar(map2, ax=ax2)
        cax2.ax.set_ylabel(r'$\tilde{n}$')

        ax1.set_ylabel(r'$y / \rho_s$')
        ax2.set_ylabel(r'$y / \rho_s$')
        ax2.set_xlabel(r'$x / \rho_s$')

        def animate(i):
            map1.set_array(cont_phi[i].T.ravel())
            map2.set_array(cont_n[i].T.ravel())
            return map1, map2

        anim = ani.FuncAnimation(fig, animate, frames=cont.nsteps)
        anim.save('contour.mp4', writer='ffmpeg', fps=4)
