import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import read_eigenvalues as re


# User Input #
eig_path_1 = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_2p0_omti_0p0_omte_2p0', 'ky0p1', 'eigenvalues_7')
eig_path_2 = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_2p0_omti_0p0_omte_2p0', 'ky0p1', 'eigenvalues_9')

# save_name = None
save_name = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'personal_meetings', 'MJ', 'EV_npol_ky0p1.png')
lbs = ['npol=4', 'npol=100']

ec = ['tab:blue', 'tab:orange']
marker = ['o', 's', 'v']
title = 'QHS'

eig_path_list = [eig_path_1, eig_path_2]  # , eig_path_3]
###############################################################################

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24

# Plotting Axis #
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

for i, eig_path in enumerate(eig_path_list):
    eig = re.eigenvalues(eig_path)
    # omegas = eig.omega[eig.omega[:,0] > -0.05, 0]
    # gammas = eig.omega[eig.omega[:,0] > -0.05, 1]
    omegas = eig.omega[:, 0]
    gammas = eig.omega[:, 1]
    ax.scatter(omegas, gammas, s=150, facecolors='None', edgecolors=ec[i], marker=marker[i], label=lbs[i])

# Set Axis Limits #
if ax.get_xlim()[0] > 0:
    ax.set_xlim(0, ax.get_xlim()[1])
else:
    ax.set_xlim(ax.get_xlim())

if ax.get_ylim()[1] < 0:
    ax.set_ylim(ax.get_ylim()[0], 0)
elif ax.get_ylim()[0] > 0:
    ax.set_ylim(0, ax.get_ylim()[1])
else:
    ax.set_ylim(ax.get_ylim())

ax.plot(ax.get_xlim(), [0,0], c='k', ls='--')
ax.plot([0,0], ax.get_ylim(), c='k', ls='--')

# Label Axes #
ax.set_xlabel(r'$\gamma \ / \ (c_s/a)$')
ax.set_ylabel(r'$\omega \ / \ (c_s/a)$')
ax.set_title(title)

# Axis Legend #
ax.legend(fontsize=16, loc='upper right')

# Axis Grid #
ax.grid()

# Save or Show Plot #
if save_name:
    plt.savefig(save_name)
else:
    plt.show()
