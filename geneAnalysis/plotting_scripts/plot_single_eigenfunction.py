import os
import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.pyplot as plt


t_idx = 0
ky_key = 'ky_0.7'
ky_tag = 'ky_0p7'

conID = '897-1-0'
save_dirc = os.path.join('/home', 'michael', 'Documents')

# Read in eigen data #
if conID == 'QHS':
    eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'QHS_eigendata.h5')
else:
    eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'eigendata.h5')

with hf.File(eigen_path, 'r') as hf_:
    if conID == 'QHS':
        eigen_key = conID+'/'+ky_key + '/data'
        if eigen_key in hf_:
            eigen_data = hf_[eigen_key][t_idx]

            eigen_real = eigen_data.real
            eigen_imag = eigen_data.imag
            eigen_mag = np.absolute(eigen_data)

            eigen_real = eigen_real / np.max(eigen_mag)
            eigen_imag = eigen_imag / np.max(eigen_mag)
            eigen_mag = eigen_mag / np.max(eigen_mag)

            z_data = hf_[ky_key+'/z coords'][t_idx]
            check = True

        else:
            check = False

    else:
        eigen_key = conID+'/'+ky_key+'/data'
        if eigen_key in hf_:
            eigen_data = hf_[eigen_key][0]

            eigen_real = eigen_data.real
            eigen_imag = eigen_data.imag
            eigen_mag = np.absolute(eigen_data)

            eigen_real = eigen_real / np.max(eigen_mag)
            eigen_imag = eigen_imag / np.max(eigen_mag)
            eigen_mag = eigen_mag / np.max(eigen_mag)

            z_data = hf_[conID+'/'+ky_key+'/z coords'][0]
            check = True

        else:
            check = False

if check:
    z_dom = np.linspace(-4, 4, z_data.shape[0])

    # Plot data #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 18}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2

    # Define axes #
    fig, axs = plt.subplots(3, 1, sharex=True, figsize=(12, 10))

    ax1 = axs[0]
    ax2 = axs[1]
    ax3 = axs[2]

    # Plot data #
    ax1.plot(z_dom, eigen_mag, c='k')  # , label=r'$|\phi|$')
    ax2.plot(z_dom, eigen_real, c='tab:red')  # , label=r'$\mathcal{Re}(phi)$')
    ax3.plot(z_dom, eigen_imag, c='tab:blue')  # , label=r'$\mathcal{Im}(\phi)$')

    # Set limits #
    ax1.set_xlim(-4, 4)
    ax1.set_ylim(0, 1.)
    ax2.set_ylim(-1., 1.)
    ax3.set_ylim(-1., 1.)

    # Define labesl #
    ax1.set_ylabel(r'$|\phi|$')
    ax2.set_ylabel(r'$\mathcal{Re}(\phi)$')
    ax3.set_ylabel(r'$\mathcal{Im}(\phi)$')
    ax3.set_xlabel(r'$\theta / \pi$')

    ky_split = ky_key.split('_')
    ax1.set_title('%s (%s = %s)' % (conID, ky_split[0], ky_split[1]))

    # Axis grid #
    ax1.grid()
    ax2.grid()
    ax3.grid()

    plt.tight_layout()
    plt.show()

    save_path = os.path.join(save_dirc, '5-1-485_ky0p7.png')
    # plt.savefig(save_path)

else:
    print('Eigendata not found!')
