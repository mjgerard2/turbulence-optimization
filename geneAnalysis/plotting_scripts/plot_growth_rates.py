import os
import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.pyplot as plt


tem_list = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'configuration_data.txt')
tem_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'gene_data', 'TEM_omega.h5')
qhs_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations.h5')
met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data.h5')

conIDs = ['5-1-485', 'QHS', '897-1-0']

tem_dict = {}
tem_data = np.empty((len(conIDs), 4))

with open(tem_list, 'r') as f:
    lines = f.readlines()
    for l, line in enumerate(lines):
        arr = line.strip().split(' : ')
        tem_dict[arr[1]] = arr[0]

with hf.File(tem_path, 'r') as hf_:
    for cdx, conID in enumerate(conIDs):
        if conID == 'QHS':
            with hf.File(qhs_path, 'r') as hf2_:
                tem_data[cdx] = hf2_['TEM'][:, 1]
        else:
            tem_key = tem_dict[conID]
            tem_data[cdx] = hf_[tem_key][:, 1]

with hf.File(met_path, 'r') as hf_:
    met_arr = hf_['metric data'][()]
    met_data = np.empty((len(conIDs), met_arr.shape[1]))
    for cdx, conID in enumerate(conIDs):
        if conID == 'QHS':
            met_data[cdx] = np.r_[0, 1, 0, np.ones(met_arr.shape[1]-3)]
        else:
            conArr = np.array([float(i) for i in conID.split('-')])
            sum_abs = np.sum(np.abs(met_arr[:, 0:3] - conArr), axis=1)
            idx = np.argmin(sum_abs)
            if sum_abs[idx] == 0:
                met_data[cdx] = met_arr[idx]

plt.close('all')

font = {'family' : 'sans-serif',
        'weight' : 'normal',
        'size'   : 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

fig, axs = plt.subplots(3, 1, sharex=True, figsize=(8, 14))

axs[0].plot(met_data[:, 9], tem_data[:, 0], c='k', ls='--', marker='s', markerfacecolor='None', markersize=15)
axs[1].plot(met_data[:, 9], met_data[:, 3], c='k', ls='--', marker='o', markerfacecolor='None', markersize=15)

plt.show()
