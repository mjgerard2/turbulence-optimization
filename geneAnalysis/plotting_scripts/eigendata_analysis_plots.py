import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import gistTools.gist_reader as gr


t_idx = 0
ky_keys = ['ky_0.1', 'ky_0.4', 'ky_0.7', 'ky_1.0']
ky_tags = ['ky_0p1', 'ky_0p4', 'ky_0p7', 'ky_1p0']

for kdx, ky_key in enumerate(ky_keys):
    ky_tag = ky_tags[kdx]

    save_dirc = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'figures', ky_tag)
    read_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'gene_data', 'tem_order_'+ky_tag+'.txt')

    with open(read_file, 'r') as f:
        lines = f.readlines()

    print('\n##############################')
    print('#      Working : %s      #' % ky_key)
    print('##############################\n')

    for ldx, line in enumerate(lines):
        line = line.strip().split(' : ')
        conID = line[0]
        gamma = float(line[1])

        print(conID+' ({0:0.0f}|{1:0.0f})'.format(ldx+1, len(lines)))
        if conID == '0-1-0':
            conID = 'QHS'

        # Read in gist data #
        if conID == "QHS":
            gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_s0p5_alpha0_npol4_nz8192.dat')
        else:
            gist_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir', 'gist_files', 'gist_{}_s0p5_alpha0_npol4_nz8192.dat'.format(conID))

        gist = gr.read_gist(gist_path)
        gist.calc_shear()
        gist.partition_wells()

        num_of_wells = gist.B_min.shape[0]
        well_IDs = np.arange(num_of_wells, dtype=int) - np.floor(0.5*num_of_wells)
        pol_max = gist.pol_max

        gist.calculate_Omega_singleWell(well_IDs[0])
        theta_bnc = gist.theta_bnc
        omegaD = gist.omegaD
        p_wght = gist.p_wght
        Omega = np.array([gist.Omega])

        for well_ID in well_IDs[1::]:
            gist.calculate_Omega_singleWell(well_ID)
            theta_bnc = np.append(theta_bnc, gist.theta_bnc)
            omegaD = np.append(omegaD, gist.omegaD)
            p_wght = np.append(p_wght, gist.p_wght)
            Omega = np.append(Omega, gist.Omega)

        pol_norm = gist.pol_dom / np.pi
        modB = gist.data['modB']

        # Calculate curvature drive #
        gradPsi = gist.data['gxx']
        Kn = gist.data['Kn']
        Kg = gist.data['Kg']
        D = gist.data['D']
        curve_drive = (Kn / gradPsi) + D*Kg*(gradPsi / modB)
        CD_max = np.max(np.abs(curve_drive))

        # Read in eigen data #
        if conID == 'QHS':
            eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'QHS_eigendata.h5')
        else:
            eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'eigendata.h5')

        with hf.File(eigen_path, 'r') as hf_:
            if conID == 'QHS':
                eigen_key = ky_key + '/data'
                if eigen_key in hf_:
                    eigen_data = hf_[eigen_key][()]
                    eigen_data = np.absolute(eigen_data)[t_idx]
                    eigen_data = eigen_data / np.max(eigen_data)
                    z_data = hf_[ky_key+'/z coords'][t_idx]
                    check = True
                else:
                    check = False

            else:
                eigen_key = conID+'/'+ky_key+'/data'
                if eigen_key in hf_:
                    eigen_data = hf_[eigen_key][()]
                    eigen_data = np.absolute(eigen_data)[t_idx]
                    eigen_data = eigen_data / np.max(eigen_data)
                    z_data = hf_[conID+'/'+ky_key+'/z coords'][t_idx]
                    check = True
                else:
                    check = False

        if check:
            z_dom = np.linspace(pol_norm[0], pol_norm[-1], z_data.shape[0])

            # Plot data #
            plt.close('all')

            font = {'family': 'sans-serif',
                    'weight': 'normal',
                    'size': 18}

            mpl.rc('font', **font)

            mpl.rcParams['axes.labelsize'] = 22
            mpl.rcParams['lines.linewidth'] = 2

            # Define axes #
            fig, axs = plt.subplots(3, 1, sharex=True, figsize=(16, 10))

            ax1 = axs[0]
            ax2 = axs[1]
            ax3 = ax2.twinx()
            ax4 = axs[2]
            ax5 = ax4.twinx()

            # Color axes #
            ax3.spines['right'].set_color('tab:red')
            ax3.tick_params(axis='y', colors='tab:red')

            # Plot data #
            ax1.plot(z_dom, eigen_data, c='k')
            ax2.plot(pol_norm, modB, c='k')
            ax3.plot(pol_norm, curve_drive, c='tab:red')
            # ax3.plot(pol_norm, np.zeros(pol_norm.shape[0]), c='tab:red', ls='--')
            ax4.plot(theta_bnc/np.pi, omegaD, c='k')
            ax5.plot(theta_bnc/np.pi, p_wght, c='tab:blue')

            for i in range(num_of_wells+1):
                pol_ext = [pol_max[i]/np.pi, pol_max[i]/np.pi]
                ax1.plot(pol_ext, [0, 1.2], c='k', ls='--')
                ax3.plot(pol_ext, [-CD_max, CD_max], c='k', ls='--')
                ax5.plot(pol_ext, [0, 1.1*np.max(p_wght)], c='k', ls='--')
                if i < num_of_wells:
                    wgt = 0.9
                    pol_text = (wgt*pol_max[i] + (1-wgt)*pol_max[i+1]) / np.pi
                    ax1.text(pol_text, 1.1, r'$\Omega = {0:0.2f}$'.format(Omega[i]), fontsize=14)

            # Set limits #
            ax1.set_ylim(0, 1.2)
            ax2.set_xlim(-4, 4)
            ax3.set_ylim(-CD_max, CD_max)
            ax5.set_ylim(0, 1.1*np.max(p_wght))

            # Define labesl #
            ax1.set_ylabel(r'$|\phi|^2 / |\phi|^2_{max}$')
            ax4.set_xlabel(r'$\theta / \pi$')
            ax2.set_ylabel(r'$B$ (T)')
            ax3.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D \kappa_g \frac{|\nabla \psi|}{B}$', c='tab:red')
            ax4.set_ylabel(r'$\langle \omega_D \rangle$')
            ax5.set_ylabel(r'$p(\theta_b)$')

            ky_num = float(ky_key.split('_')[1])
            ax1.set_title(r'$\gamma = {0:0.3f} \ (c_s/a) \quad [k_y = {1:0.1f}]$'.format(gamma, ky_num))

            # Axis grid #
            ax1.grid()
            ax3.grid()
            ax4.grid()

            plt.tight_layout()
            # plt.show()

            save_path = os.path.join(save_dirc, 'gam{0:0.0f}_{1}.png'.format(ldx, conID))
            plt.savefig(save_path)
