import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import gistTools.gist_reader as gr


t_idx = 0
ky_keys = ['ky_0.1', 'ky_0.4', 'ky_0.7', 'ky_1.0']
ky_tags = ['ky_0p1', 'ky_0p4', 'ky_0p7', 'ky_1p0']

save_dirc = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'figures', 'eigenfunctions')
read_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'gene_data', 'tem_order_ky_0p4.txt')
with open(read_file, 'r') as f:
    lines = f.readlines()

for ldx, line in enumerate(lines):
    line = line.strip().split(' : ')
    conID = line[0]
    gamma = float(line[1])

    print(conID+' ({0:0.0f}|{1:0.0f})'.format(ldx+1, len(lines)))
    if conID == '0-1-0':
        conID = 'QHS'

    # Read in gist data #
    if conID == "QHS":
        gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_s0p5_alpha0_npol4_nz8192.dat')
    else:
        gist_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir', 'gist_files', 'gist_{}_s0p5_alpha0_npol4_nz8192.dat'.format(conID))

    gist = gr.read_gist(gist_path)
    gist.calc_shear()
    gist.partition_wells()

    num_of_wells = gist.B_min.shape[0]
    well_IDs = np.arange(num_of_wells, dtype=int) - np.floor(0.5*num_of_wells)
    pol_max = gist.pol_max

    gist.calculate_Omega_singleWell(well_IDs[0])
    theta_bnc = gist.theta_bnc
    omegaD = gist.omegaD
    p_wght = gist.p_wght
    Omega = np.array([gist.Omega])

    for well_ID in well_IDs[1::]:
        gist.calculate_Omega_singleWell(well_ID)
        theta_bnc = np.append(theta_bnc, gist.theta_bnc)
        omegaD = np.append(omegaD, gist.omegaD)
        p_wght = np.append(p_wght, gist.p_wght)
        Omega = np.append(Omega, gist.Omega)

    pol_norm = gist.pol_dom / np.pi
    om_wght = omegaD * p_wght
    om_integ = np.trapz(om_wght, theta_bnc) / np.trapz(p_wght, theta_bnc)
    om_wght_max = np.max(np.abs(om_wght))

    # Plot data #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 18}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2

    # Define axes #
    fig, axs = plt.subplots(len(ky_keys)+1, 1, sharex=True, figsize=(18, 14))

    axs[0].plot(theta_bnc/np.pi, om_wght, c='k')
    for i in range(num_of_wells+1):
        y_lim = [-om_wght_max, om_wght_max]
        pol_ext = [pol_max[i]/np.pi, pol_max[i]/np.pi]
        axs[0].plot(pol_ext, y_lim, c='k', ls='--')

    # Read in eigen data #
    if conID == 'QHS':
        eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'QHS_eigendata.h5')
    else:
        eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'eigendata.h5')

    # Data Analysis #
    with hf.File(eigen_path, 'r') as hf_:
        for kdx, ky_key in enumerate(ky_keys):
            ky_tag = ky_tags[kdx]

            if conID == 'QHS':
                eigen_key = ky_key + '/data'
                if eigen_key in hf_:
                    eigen_data = hf_[eigen_key][t_idx]

                    eigen_real = eigen_data.real
                    eigen_imag = eigen_data.imag
                    eigen_mag = np.absolute(eigen_data)

                    eigen_real = eigen_real / np.max(eigen_mag)
                    eigen_imag = eigen_imag / np.max(eigen_mag)
                    eigen_mag = eigen_mag / np.max(eigen_mag)

                    z_data = hf_[ky_key+'/z coords'][t_idx]
                    check = True
                else:
                    check = False

            else:
                eigen_key = conID+'/'+ky_key+'/data'
                if eigen_key in hf_:
                    eigen_data = hf_[eigen_key][t_idx]

                    eigen_real = eigen_data.real
                    eigen_imag = eigen_data.imag
                    eigen_mag = np.absolute(eigen_data)

                    eigen_real = eigen_real / np.max(eigen_mag)
                    eigen_imag = eigen_imag / np.max(eigen_mag)
                    eigen_mag = eigen_mag / np.max(eigen_mag)

                    z_data = hf_[conID+'/'+ky_key+'/z coords'][t_idx]
                    check = True
                else:
                    check = False

            if check:
                z_dom = np.linspace(pol_norm[0], pol_norm[-1], z_data.shape[0])

                # Plot data #
                axs[kdx+1].plot(z_dom, eigen_mag, c='k', label=r'$k_y \rho_s = $ %s' % ky_key.split('_')[1])
                axs[kdx+1].plot(z_dom, eigen_real, c='tab:red')  # , label=r'$\mathcal{Re}(\phi) / |\phi_{max}|$')
                axs[kdx+1].plot(z_dom, eigen_imag, c='tab:blue')  # , label=r'$\mathcal{Im}(\phi) / |\phi|_{max}$')
                #axs[kdx+1].legend()

                for i in range(num_of_wells+1):
                    y_lim = [-1, 1]
                    pol_ext = [pol_max[i]/np.pi, pol_max[i]/np.pi]
                    axs[kdx+1].plot(pol_ext, y_lim, c='k', ls='--')
                    if i < num_of_wells:
                        wgt = 0.9
                        pol_text = (wgt*pol_max[i] + (1-wgt)*pol_max[i+1]) / np.pi
                        axs[kdx+1].text(pol_text, 1.1, r'$\Omega = {0:0.2f}$'.format(Omega[i]), fontsize=14)

    # Set limits #
    axs[0].set_ylim(-om_wght_max, om_wght_max)
    axs[1].set_ylim(-1, 1.)
    axs[2].set_ylim(-1, 1.)
    axs[3].set_ylim(-1, 1.)
    axs[4].set_ylim(-1, 1.)

    axs[0].set_xlim(-4, 4)

    # Define labesl #
    axs[0].set_ylabel(r'$p(\theta_b) \cdot \langle \omega_D \rangle$')
    axs[1].set_ylabel(r'$|\phi| / |\phi|_{max}$')
    axs[2].set_ylabel(r'$|\phi| / |\phi|_{max}$')
    axs[3].set_ylabel(r'$|\phi| / |\phi|_{max}$')
    axs[4].set_ylabel(r'$|\phi| / |\phi|_{max}$')
    axs[4].set_xlabel(r'$\theta / \pi$')

    axs[0].set_title(r'$\Omega_{{full}} = {0:0.3f}$'.format(om_integ))

    # Axis grid #
    axs[0].grid()
    axs[1].grid()
    axs[2].grid()
    axs[3].grid()
    axs[4].grid()

    plt.tight_layout()
    # plt.show()

    save_path = os.path.join(save_dirc, 'gam{0:0.0f}_'.format(ldx)+conID+'.png')
    plt.savefig(save_path)
