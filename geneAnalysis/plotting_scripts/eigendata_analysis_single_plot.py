import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.pyplot as plt

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import gistTools.gist_reader as gr


t_idx = 0
ky_key = 'ky_0.4'
ky_tag = 'ky_0p4'

conID = '0-1-111'
save_dirc = os.path.join('/home', 'michael', 'Documents')

# Read in gist data #
if conID == "QHS":
    gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_s0p5_alpha0_npol4_nz8192.dat')
else:
    gist_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir', 'gist_files', 'gist_{}_s0p5_alpha0_npol4_nz8192.dat'.format(conID))

gist = gr.read_gist(gist_path)
gist.calc_shear()
gist.partition_wells()

num_of_wells = gist.B_min.shape[0]
well_IDs = np.arange(num_of_wells, dtype=int) - np.floor(0.5*num_of_wells)
pol_max = gist.pol_max

gist.calculate_Omega_singleWell(well_IDs[0])
theta_bnc = gist.theta_bnc
omegaD = gist.omegaD
p_wght = gist.p_wght
Omega = np.array([gist.Omega])

for well_ID in well_IDs[1::]:
    gist.calculate_Omega_singleWell(well_ID)
    theta_bnc = np.append(theta_bnc, gist.theta_bnc)
    omegaD = np.append(omegaD, gist.omegaD)
    p_wght = np.append(p_wght, gist.p_wght)
    Omega = np.append(Omega, gist.Omega)

pol_norm = gist.pol_dom / np.pi
modB = gist.data['modB']
omegaD_wght = omegaD * p_wght
OmWght_max = 1.1 * np.max(np.abs(omegaD_wght))

# Calculate curvature drive #
gradPsi = gist.data['gxx']
Kn = gist.data['Kn']
Kg = gist.data['Kg']
D = gist.data['D']
curve_drive = (Kn / gradPsi) + D*Kg*(gradPsi / modB)
CD_max = np.max(np.abs(curve_drive))

# Read in eigen data #
if conID == 'QHS':
    eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'QHS_eigendata.h5')
else:
    eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'eigendata.h5')
    # eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', '5-1-485_eigendata.h5')

with hf.File(eigen_path, 'r') as hf_:
    if conID == 'QHS':
        eigen_key = conID+'/'+ky_key + '/data'
        if eigen_key in hf_:
            eigen_data = hf_[eigen_key][()]
            eigen_data = np.absolute(eigen_data)[t_idx]
            eigen_data = eigen_data / np.max(eigen_data)
            z_data = hf_[ky_key+'/z coords'][t_idx]
            check = True
        else:
            check = False

    else:
        eigen_key = conID+'/'+ky_key+'/data'
        if eigen_key in hf_:
            eigen_data = hf_[eigen_key][0]
            eigen_real = eigen_data.real
            eigen_imag = eigen_data.imag
            eigen_data = np.absolute(eigen_data)
            eigen_data = eigen_data / np.max(eigen_data)
            z_data = hf_[conID+'/'+ky_key+'/z coords'][0]
            check = True
        else:
            check = False

if check:
    z_dom = np.linspace(gist.pol_dom[0], gist.pol_dom[-1], z_data.shape[0]) / np.pi

    # Plot data #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 18}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2

    # Define axes #
    # fig, ax1 = plt.subplots(1, 1, sharex=True, figsize=(12, 6))
    fig, axs = plt.subplots(3, 1, sharex=True, figsize=(16, 10))

    ax1 = axs[0]
    ax2 = axs[1]
    ax3 = ax2.twinx()
    ax4 = axs[2]
    # ax5 = ax4.twinx()

    # Color axes #
    ax3.spines['right'].set_color('tab:red')
    ax3.tick_params(axis='y', colors='tab:red')

    # Plot data #
    ax1.plot(z_dom, eigen_data, c='k', label=r'$|\phi|$')
    ax2.plot(pol_norm, modB, c='k')
    ax3.plot(pol_norm, curve_drive, c='tab:red')
    # ax3.plot(pol_norm, np.zeros(pol_norm.shape[0]), c='tab:red', ls='--')
    ax4.plot(theta_bnc/np.pi, omegaD_wght, c='k')
    # ax4.plot(theta_bnc/np.pi, omegaD, c='k')
    # ax5.plot(theta_bnc/np.pi, p_wght, c='tab:blue')

    for i in range(num_of_wells+1):
        pol_ext = [pol_max[i]/np.pi, pol_max[i]/np.pi]
        ax1.plot(pol_ext, [0, 1.2], c='k', ls='--')
        ax3.plot(pol_ext, [-CD_max, CD_max], c='k', ls='--')
        ax4.plot(pol_ext, [-OmWght_max, OmWght_max], c='k', ls='--')
        # ax5.plot(pol_ext, [0, 1.1*np.max(p_wght)], c='k', ls='--')
        if i < num_of_wells:
            wgt = 0.9
            pol_text = (wgt*pol_max[i] + (1-wgt)*pol_max[i+1]) / np.pi
            ax1.text(pol_text, 1.1, r'$\Omega = {0:0.2f}$'.format(Omega[i]), fontsize=14)

    # Set limits #
    ax1.set_ylim(0, 1.2)
    ax2.set_xlim(-4, 4)
    ax3.set_ylim(-CD_max, CD_max)
    ax4.set_ylim(-OmWght_max, OmWght_max)
    # ax5.set_ylim(0, 1.1*np.max(p_wght))

    # Define labesl #
    ax1.set_ylabel(r'$|\phi| / |\phi|_{max}$')
    ax4.set_xlabel(r'$\theta / \pi$')
    ax2.set_ylabel(r'$B$ (T)')
    ax3.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D \kappa_g \frac{|\nabla \psi|}{B}$', c='tab:red')
    ax4.set_ylabel(r'$\langle \omega_D \rangle \cdot p(\theta_b)$')
    # ax5.set_ylabel(r'$p(\theta_b)$')

    # Axis grid #
    ax1.grid()
    ax3.grid()
    ax4.grid()

    plt.tight_layout()
    plt.show()

    save_path = os.path.join(save_dirc, '5-1-485_ky0p7.png')
    # plt.savefig(save_path)
