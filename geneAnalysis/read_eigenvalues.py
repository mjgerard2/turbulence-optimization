import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import plot_define as pd


class eigenvalues:
    """ A class for reading GENE eigenvalues from an eigenvalue data file.

    ...

    Attributes
    ----------
    eigen_path : str
        Global path to eigenvalue file.

    Methods
    -------
    """
    def __init__(self, eigen_path, proj_path=None):
        if not os.path.isfile(eigen_path):
            raise IOError('Oops! '+eigen_path+' does not exist :(')

        with open(eigen_path, 'r') as f:
            lines = f.readlines()
            self.omega = np.full((len(lines)-1, 2), np.nan)
            for i, line in enumerate(lines[1::]):
                self.omega[i] = [float(x) for x in line.strip().split()]

        if not proj_path is None:
            with open(proj_path, 'r') as f:
                lines = f.readlines()
                self.proj = np.empty(len(lines))
                for i, line in enumerate(lines):
                    self.proj[i] = float(line.strip())

    def plot_omegas(self, ky_val=None, save_path=None, show_proj=False):
        plot = pd.plot_define(fontSize=16, labelSize=20)
        fig, ax = plt.subplots(1, 1, tight_layout=True)

        # Plot data #
        idx = np.where(np.abs(self.omega[:,1]) < 1)[0]
        # self.omega = self.omega[np.abs(self.omega[:,1]) < 1]
        self.omega = self.omega[idx]
        if show_proj:
            self.proj = self.proj[idx]
            smap = ax.scatter(self.omega[:, 0], self.omega[:, 1], s=75, c=self.proj, edgecolors='k', cmap='magma', vmin=0, vmax=.3)  # np.max(self.proj))
        else:
            ax.scatter(self.omega[:, 0], self.omega[:, 1], s=75, facecolors='None', edgecolors='k')

        # Label Axes #
        ax.set_xlabel(r'$\gamma \ (c_s/a)$')
        ax.set_ylabel(r'$\omega \ (c_s/a)$')
        if not ky_val is None:
            ax.set_title(r'QHS $(k_y\rho_s = {})$'.format(ky_val))

        if show_proj:
            cmap = fig.colorbar(smap, ax=ax)
            cmap.ax.set_ylabel(r'$p_j$')

        # Set Axis Limits #
        if all(self.omega[:,0] > 0):
            ax.set_xlim(0, ax.get_xlim()[1])
        else:
            ax.set_xlim(ax.get_xlim())

        if all(self.omega[:,1] < 0):
            ax.set_ylim(ax.get_ylim()[0], 0)
        elif all(self.omega[:,1] > 0):
            ax.set_ylim(0, ax.get_ylim()[1])
        else:
            ax.set_ylim(ax.get_ylim())
        
        # plot zero lines #
        x_min, x_max = ax.get_xlim()
        y_min, y_max = ax.get_ylim()
        ax.plot([x_min, x_max], [0, 0], c='k')
        ax.plot([0, 0], [y_min, y_max], c='k')

        # axis grid #
        ax.grid()

        # save/show #
        if save_path == None:
            plt.show()
        else:
            plt.savefig(save_path)


if __name__ == "__main__":
    if False:
        ky_val = 0.2
        ky_tag = 'ky_{0:0.1f}'.format(ky_val).replace('.', 'p')
        eigen_path = os.path.join('/mnt', 'GENE', 'linear_data', '60-1-84', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0', ky_tag, 'eigenvalues_3')
        # proj_path = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_2p0_omti_0p0_omte_2p0', ky_tag, 'stell_NLproj_2.dat')
        eig = eigenvalues(eigen_path) #, proj_path=proj_path)

        save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'APS', '2023', 'Figures', 'QHS_2p0_ky0p4_proj.png')
        eig.plot_omegas(ky_val=ky_val)  # , show_proj=True, save_path=save_path)

    if True:
        eigen_path_1 = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0', 'ky0p1', 'eigenvalues_8')
        proj_path_1 = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0', 'ky0p1', 'stell_NLproj_8.dat')
        eigen_path_2 = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0', 'ky0p2', 'eigenvalues_8')
        proj_path_2 = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0', 'ky0p2', 'stell_NLproj_8.dat')

        eig_1 = eigenvalues(eigen_path_1, proj_path=proj_path_1)
        eig_2 = eigenvalues(eigen_path_2, proj_path=proj_path_2)

        plot = pd.plot_define()
        fig, ax = plt.subplots(1, 1, sharex=True, sharey=True, tight_layout=True, figsize=(8, 6))

        # eig_1.omega = eig_1.omega[np.where(np.abs(eig_1.omega[:,1]) < 1)]
        # eig_2.omega = eig_2.omega[np.where(np.abs(eig_2.omega[:,1]) < 1)]
        ax.scatter(eig_1.omega[:, 0], eig_1.omega[:, 1], s=75, marker='s', facecolors='None', edgecolors='tab:blue', label=r'$a/L_{\mathrm{n}} = 1.5$')
        ax.scatter(eig_2.omega[:, 0], eig_2.omega[:, 1], s=75, marker='o', facecolors='None', edgecolors='tab:orange', label=r'$a/L_{\mathrm{n}} = 2.0$')

        ax.set_xlim(ax.get_xlim())
        ax.set_ylim(ax.get_ylim())

        ax.plot(ax.get_xlim(), [0, 0], c='k', ls='--', linewidth=2)
        ax.plot([0, 0], ax.get_ylim(), c='k', ls='--', linewidth=2)

        ax.set_ylabel(r'$\omega \ (c_s/a)$')
        ax.set_xlabel(r'$\gamma \ (c_s/a)$')

        ax.grid()
        ax.legend()

        plt.show()
        save_path = os.path.join('home', 'michael', 'onedrive', 'Presentations', 'personal_meetings', 'MJ_20230712', 'figures', 'harmonic_taumfn_comparison_0-1-0_jason_aLn2p0_ky0p2_7_10.png')
        # plt.savefig(save_path)
