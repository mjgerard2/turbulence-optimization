import os
import h5py as hf
import numpy as np

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import plot_define as pd


# import metric data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# metric normalization #
metric_norm = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations_OLD_2023-04-04.h5')
with hf.File(metric_norm, 'r') as hf_:
    qhs_tems = hf_['TEM'][()]

# read in growth rates #
hf_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')
with hf.File(hf_path, 'r') as hf_file:
    tem_data = np.full((100, 7, 3), np.nan)
    for m, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        tem_data[m] = hf_file[conID][()]

# plotting axis #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(1, 3, sharex=False, sharey=True, tight_layout=True, figsize=(16, 6))

ax1 = axs[0]
ax2 = axs[1]
ax3 = axs[2]

# plot growth rates against Bk #
ax1.scatter(met_data[:, 6], tem_data[:, 0, 1], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label='0.1')
ax1.scatter(met_data[:, 6], tem_data[:, 1, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label='0.4')
ax1.scatter(met_data[:, 6], tem_data[:, 2, 1], marker='s', s=150, facecolor='None', edgecolor='tab:green', label='0.7')
ax1.scatter(met_data[:, 6], tem_data[:, 3, 1], marker='^', s=150, facecolor='None', edgecolor='tab:red', label='1.0')

ax1.scatter([-1.], qhs_tems[0, 1], marker='*', s=750, c='tab:blue', edgecolor='k')
ax1.scatter([-1.], qhs_tems[1, 1], marker='*', s=750, c='tab:orange', edgecolor='k')
ax1.scatter([-1.], qhs_tems[2, 1], marker='*', s=750, c='tab:green', edgecolor='k')
ax1.scatter([-1.], qhs_tems[3, 1], marker='*', s=750, c='tab:red', edgecolor='k')

# plot growth rates against Cn #
ax2.scatter(met_data[:, 7], tem_data[:, 0, 1], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label='0.1')
ax2.scatter(met_data[:, 7], tem_data[:, 1, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label='0.4')
ax2.scatter(met_data[:, 7], tem_data[:, 2, 1], marker='s', s=150, facecolor='None', edgecolor='tab:green', label='0.7')
ax2.scatter(met_data[:, 7], tem_data[:, 3, 1], marker='^', s=150, facecolor='None', edgecolor='tab:red', label='1.0')

ax2.scatter([-1.], qhs_tems[0, 1], marker='*', s=750, c='tab:blue', edgecolor='k')
ax2.scatter([-1.], qhs_tems[1, 1], marker='*', s=750, c='tab:orange', edgecolor='k')
ax2.scatter([-1.], qhs_tems[2, 1], marker='*', s=750, c='tab:green', edgecolor='k')
ax2.scatter([-1.], qhs_tems[3, 1], marker='*', s=750, c='tab:red', edgecolor='k')

# plot growth rates against K #
ax3.scatter(met_data[:, 9], tem_data[:, 0, 1], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label='0.1')
ax3.scatter(met_data[:, 9], tem_data[:, 1, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label='0.4')
ax3.scatter(met_data[:, 9], tem_data[:, 2, 1], marker='s', s=150, facecolor='None', edgecolor='tab:green', label='0.7')
ax3.scatter(met_data[:, 9], tem_data[:, 3, 1], marker='^', s=150, facecolor='None', edgecolor='tab:red', label='1.0')

ax3.scatter([1.], qhs_tems[0, 1], marker='*', s=750, c='tab:blue', edgecolor='k')
ax3.scatter([1.], qhs_tems[1, 1], marker='*', s=750, c='tab:orange', edgecolor='k')
ax3.scatter([1.], qhs_tems[2, 1], marker='*', s=750, c='tab:green', edgecolor='k')
ax3.scatter([1.], qhs_tems[3, 1], marker='*', s=750, c='tab:red', edgecolor='k')

# Axis Limits #
ax1.set_ylim(0, ax1.get_ylim()[1])
ax3.set_xlim(0.955, 1.08)

# Axis Labels #
ax1.set_ylabel(r'$\gamma / (c_{\mathrm{s}}/a)$')
ax1.set_xlabel(r'$\mathcal{B}_{\kappa} / |\mathcal{B}_{\kappa}^*|$')
ax2.set_xlabel(r'$\mathcal{C}_{\mathrm{n}} / |\mathcal{C}_{\mathrm{n}}^*|$')
ax3.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# ax2.yaxis.set_tick_params(labelleft=False)
# ax3.yaxis.set_tick_params(labelleft=False)

# Axis Grids #
ax1.grid()
ax2.grid()
ax3.grid()

# Axis Legends #
# ax2.legend(loc='upper center', frameon=False, ncol=4, columnspacing=0.5, handletextpad=-0.2, title=r'$k_y \rho_s$', fontsize=20)

# Plot/Save Figures #
# plt.show()
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'TTF_2023', 'figures', 'growth_rates.png')
plt.savefig(save_path)
