import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt


base_dirc = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0')

beta_vals = np.empty(4)
ky_vals = np.linspace(0.1, 1.9, 7)
ky_tags = ['ky_{0:0.1f}'.format(ky).replace('.', 'p') for ky in ky_vals]

omega_data = np.empty((4, 7, 3))
for ky_idx, ky_tag in enumerate(ky_tags):
    omega_file = os.path.join(base_dirc, ky_tag, 'omega_1_1')
    with open(omega_file, 'r') as f:
        lines = f.readlines()
        omega_data[0, ky_idx] = np.array([float(elem) for elem in lines[0].strip().split()])

prams_file = os.path.join(base_dirc, 'ky_0p1', 'parameters_1_1')
with open(prams_file, 'r') as f:
    lines = f.readlines()
    for line in lines:
        line = line.strip().split()
        if len(line) > 0:
            if line[0] == 'beta':
                beta_vals[0] = float(line[2])
                break

base_dirc = os.path.join(base_dirc, 'hsx_beta_scan')
for i in np.arange(ky_vals.shape[0], dtype=int):
    for j in np.arange(1, 4, dtype=int):
        run_dirc = 'run_'+str(i*3+j).zfill(2)
        omega_name = 'omega_'+str(i*3+j).zfill(4)
        omega_file = os.path.join(base_dirc, run_dirc, omega_name)
        with open(omega_file, 'r') as f:
            lines = f.readlines()
            omega_data[j, i] = np.array([float(elem) for elem in lines[0].strip().split()])

for i in np.arange(1, 4, dtype=int):
    run_dirc = 'run_'+str(i).zfill(2)
    prams_name = 'parameters_'+str(i).zfill(4)
    prams_file = os.path.join(base_dirc, run_dirc, prams_name)
    with open(prams_file, 'r') as f:
        lines = f.readlines()
        for line in lines:
            line = line.strip().split()
            if len(line) > 0:
                if line[0] == 'beta':
                    beta_vals[i] = float(line[2])
                    break

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24

# Plotting Axis #
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))

ax.plot(omega_data[0, :, 0], omega_data[0, :, 1], c='k', ls='--', marker='^', mfc='None', mec='k', markersize=10, label=r'$\beta = {}$'.format(beta_vals[0]))
ax.plot(omega_data[1, :, 0], omega_data[1, :, 1], c='tab:red', ls='--', marker='o', mfc='None', mec='tab:red', markersize=10, label=r'$\beta = {}$'.format(beta_vals[1]))
ax.plot(omega_data[2, :, 0], omega_data[2, :, 1], c='tab:blue', ls='--', marker='s', mfc='None', mec='tab:blue', markersize=10, label=r'$\beta = {}$'.format(beta_vals[2]))
ax.plot(omega_data[3, :, 0], omega_data[3, :, 1], c='tab:green', ls='--', marker='v', mfc='None', mec='tab:green', markersize=10, label=r'$\beta = {}$'.format(beta_vals[3]))

ax.set_ylim(0, np.max(omega_data[:, :, 1])+np.min(omega_data[:, :, 1]))
ax.set_xlim(0, np.max(omega_data[:, :, 0])+0.1)

ax.set_xlabel(r'$k_y$')
ax.set_ylabel(r'$\gamma$')

ax.legend()
ax.grid()

# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'beta_scan.png')
plt.savefig(save_path)
