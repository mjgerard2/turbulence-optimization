import os
import numpy as np
import h5py as hf


# Import Metric Data #
metric_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(metric_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Import GENE Data #
omega_path = os.path.join('/mnt', 'GENE', 'linear_data', 'omega_data_1_x.h5')
with hf.File(omega_path, 'r') as hf_:
    tem_data = np.empty((met_data.shape[0], 7, 3))
    for idx, met in enumerate(met_data):
        conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
        tem_data = hf_[conID][()]
        nan_idx = np.isnan(tem_data)
        if np.any(nan_idx == True):
            print(conID)
            ky_val = tem_data[nan_idx[:, 1], 0][0]
            print('\tky_{0:0.1f}'.format(ky_val).replace('.', 'p'))
