import numpy as np
import h5py as hf

from scipy.optimize import curve_fit
from scipy.signal import argrelextrema

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import gistTools as gt


t_idx = 0
ky_vals = np.array([0.1, 0.4, 0.7, 1.0])


conID_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'conID_list.txt')
with open(conID_file, 'r') as f:
    lines = f.readlines()
    conIDs = []
    for line in lines:
        line = line.strip().split()
        conIDs.append(line[0])

savePath = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'figures', 'omD_exact', 'eigen_well_maximum.h5')
for cdx, conID in enumerate(conIDs):
    print(conID+' ({0:0.0f}|{1:0.0f})'.format(cdx+1, len(conIDs)))
    for ky in ky_vals:
        ky_min = 'ky_{0:0.1f}'.format(ky)

        # Read in gist file #
        if conID == 'QHS':
            gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_s0p5_alpha0_npol4_nz8192.dat')
        else:
            gist_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir', 'gist_files', 'gist_0-1-111_s0p5_alpha0_npol4_nz8192.dat')

        gist = gt.read_gist(gist_path)

        pol_dom = gist.pol_dom
        modB = gist.data['modB']

        Bmax = np.max(modB)
        Bmin = np.min(modB)

        # Partition wells #
        order = int(1. / ((gist.nfp * gist.q0 - 1.) * gist.hlf_stp))
        cos_model = lambda x, a1, a2: a1 + a2 * np.cos(x * 2 * (gist.nfp * gist.q0 - 1))
        p_guess = np.array([np.mean(modB), 0.5*(np.max(modB) - np.min(modB))])
        popt, pcov = curve_fit(cos_model, pol_dom, modB, p0=p_guess)
        B_chk = cos_model(pol_dom, popt[0], popt[1])
        Bmin_idx = argrelextrema(B_chk, np.less, order=order)[0]
        Bmax_idx = argrelextrema(B_chk, np.greater, order=order)[0]

        if Bmin_idx.shape[0] == Bmax_idx.shape[0]:
            if Bmin_idx[0] < Bmax_idx[0]:
                Bmin_idx = Bmin_idx[1::]
            elif Bmin_idx[-1] > Bmax_idx[-1]:
                Bmin_idx = Bmin_idx[0:-1]
            else:
                raise ValueError("Bmin_idx.shape[0] == Bmax_idx.shape[0] and which side to drop is ill-determined!")
        elif Bmin_idx.shape[0] == Bmax_idx.shape[0] + 1:
            Bmin_idx = Bmin_idx[1:-1]
        elif Bmin_idx.shape[0] != Bmax_idx.shape[0] - 1:
            raise ValueError("Bmin_idx.shape[0] = {} | Bmax_idx.shape[0] = {} | not sure what to make of that.".format(Bmin_idx.shape[0], Bmax_idx.shape[0]))

        idx_lim = -np.floor(0.5*Bmin_idx.shape[0])
        well_idx = np.arange(idx_lim, -idx_lim+1, dtype=int)
        Bmin_idx = np.append(np.insert(Bmin_idx, 0, 0), pol_dom.shape[0])

        max_idx = []
        for i, min_idx in enumerate(Bmin_idx[1::]):
            idx = np.argmax(modB[Bmin_idx[i]:min_idx])
            max_idx.append(Bmin_idx[i] + idx)
        max_idx = np.array(max_idx, dtype=int)
        num_of_wells = int(max_idx.shape[0] - 1)

        # Read in eigendata #
        found = False
        eigen_path = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'genetools.jl', 'eigendata.h5')
        with hf.File(eigen_path, 'r') as hf_:
            data_key = conID+'/'+ky_min
            if data_key in hf_:
                found = True
                z_data = hf_[data_key+'/z coords'][()] / np.pi
                eigen_data = hf_[data_key+'/data'][()]

        if found:
            eigen_data = np.absolute(eigen_data)[t_idx]
            eigen_z = z_data[t_idx] * np.pi
            eigen_max = np.max(eigen_data)

            # Integrate Eigenfunction #
            pol_lft, pol_rgt = pol_dom[max_idx[0]], pol_dom[max_idx[-1]]
            lft_idx = np.argmin(np.abs(eigen_z - pol_lft))
            rgt_idx = np.argmin(np.abs(eigen_z - pol_rgt))

            eigen = eigen_data[lft_idx:rgt_idx+1]
            pol = eigen_z[lft_idx:rgt_idx+1]
            # eigen_integ = np.trapz(eigen, pol)
            eigen_maximum = np.max(eigen)

            eigen_integ_well = np.empty((num_of_wells, 3))
            for i in range(num_of_wells):
                pol_lft, pol_rgt = pol_dom[max_idx[i]], pol_dom[max_idx[i+1]]
                lft_idx = np.argmin(np.abs(eigen_z - pol_lft))
                rgt_idx = np.argmin(np.abs(eigen_z - pol_rgt))

                eigen = eigen_data[lft_idx:rgt_idx+1]
                pol = eigen_z[lft_idx:rgt_idx+1]

                eigen_integ_well[i, 0] = well_idx[i]
                eigen_integ_well[i, 1] = .5*(pol_lft + pol_rgt)
                # eigen_integ_well[i, 2] = np.trapz(eigen, pol) / eigen_integ
                eigen_integ_well[i, 2] = np.max(eigen) / eigen_maximum

            # Save Data #
            with hf.File(savePath, 'a') as hf_:
                hf_.create_dataset('{}/{}'.format(conID, ky_min), data=eigen_integ_well)

"""
# Plot data #
plot = pd.plot_define()
fig, ax1, plt = plot.fig, plot.ax, plot.plt
ax2 = ax1.twinx()

#ax1.plot(eigen_z/np.pi, eigen_data, c='k')
ax1.plot(pol_dom[Bmin_idx[1:-1]]/np.pi, eigen_integ_well, c='k')
ax2.plot(pol_dom/np.pi, modB, c='tab:blue')

for idx in max_idx:
    #ax1.plot([pol_dom[idx]/np.pi]*2, [0, 1.1*eigen_max], c='k', ls='--')
    ax1.plot([pol_dom[idx]/np.pi]*2, [0, 1], c='k', ls='--')

#ax1.set_xlim(np.min(z_data), np.max1(z_data))
ax1.set_xlim(-2, 2)
# ax1.set_ylim(0, 1.1*eigen_max)
ax1.set_ylim(0, 1)

ax1.set_xlabel(r'$\theta / \pi$')
ax2.set_ylabel(r'$B$ (T)')
ax2.set_ylabel(r'$\phi$')

plt.tight_layout()
plt.show()
"""
