import os
import numpy as np

from pandas_ods_reader import read_ods

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import ImageGrid


runBC = 10
conID = '60-1-84'

base_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', 'TEM_conChk.ods')
df = read_ods(base_path, 1, headers=False)

found_ID = False
for i, elem in enumerate(df['column.0']):
    if elem == conID:
        found_ID = True
        idx = i
    if found_ID:
        if elem == 'Heat Flux':
            jdx = i+1
            break

if not found_ID:
    raise KeyError('Configuration ID %s has not been found' % conID)

con_head = {}
for i in range(jdx-idx):
    for key in df:
        if key == 'column.0':
            con_key = df[key][idx+i].strip().split()[0]
            con_head[con_key] = []
        else:
            con_head[con_key].append(df[key][idx+i])

for idx, run in enumerate(con_head[conID]):
    if run == None:
        run_cnt = int(con_head[conID][idx-1].strip().split('_')[0])
        break
    elif run.strip().split('_')[0][-1] == 'f':
        run_cnt = int(con_head[conID][idx-1].strip().split('_')[0])
        break

input_data = {}
base_case = {}
for key in con_head:
    if key == 'Heat':
        heat_flux = np.empty(run_cnt)
        for i in range(run_cnt):
            heat_flux[i] = float(con_head['Heat'][i].strip().split()[0])
        base_case['heat flux'] = heat_flux[runBC-1]
    elif key != conID:
        input_data[key] = np.array(con_head[key][0:run_cnt])
        base_case[key] = con_head[key][runBC-1]

heat_lower = (19./21)*base_case['heat flux']
heat_upper = (21./19)*base_case['heat flux']

# Plot Axis #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axis #
fig, axs = plt.subplots(2, 5, sharex=False, sharey=True, tight_layout=True, figsize=(20, 8))

ax1 = axs[0, 0]
ax2 = axs[1, 0]
ax3 = axs[0, 1]
ax4 = axs[1, 1]
ax5 = axs[0, 2]
ax6 = axs[1, 2]
ax7 = axs[0, 3]
ax8 = axs[1, 3]
ax9 = axs[0, 4]
ax10 = axs[1, 4]

# x-Radial Runs #
lx = (-1*input_data['nexc'])/(input_data['kymin']*input_data['npol'])
lx_base = (-1*base_case['nexc'])/(base_case['kymin']*base_case['npol'])
lx_runs = np.array([6, 8, 9], dtype=int)-1

xres = lx / input_data['nx0']
xres_base = lx_base / base_case['nx0']
xres_runs = np.array([1, 2, 3])-1

x_stack = np.stack((lx, xres, input_data['nx0'], heat_flux), axis=1)

lx_chk = np.array(sorted(x_stack[lx_runs], key=lambda x: x[0]))
xres_chk = np.array(sorted(x_stack[xres_runs], key=lambda x: x[1]))

ax1.plot(lx_chk[:, 0], lx_chk[:, 3], c='k', ls='--')
ax1.scatter(lx_chk[:, 0], lx_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax1.scatter(lx_base, base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax1.plot(lx_chk[:, 0], [heat_lower]*lx_chk.shape[0], c='tab:red', ls='-')
ax1.plot(lx_chk[:, 0], [heat_upper]*lx_chk.shape[0], c='tab:red', ls='-')

ax1.set_xscale('log', base=2)
ax1.set_xticks(lx_chk[:, 0])
tick_labels = ['({0:0.1f}, {1:0.0f})'.format(lx[0], lx[2]) for lx in lx_chk[:, 0:3]]
ax1.set_xticklabels(tick_labels, rotation=-45)
ax1.set_ylim(0, 1.1*np.max(heat_flux))

ax2.plot(xres_chk[:, 1], xres_chk[:, 3], c='k', ls='--')
ax2.scatter(xres_chk[:, 1], xres_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax2.scatter(xres_base, base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax2.plot(xres_chk[:, 1], [heat_lower]*xres_chk.shape[0], c='tab:red', ls='-')
ax2.plot(xres_chk[:, 1], [heat_upper]*xres_chk.shape[0], c='tab:red', ls='-')

ax2.set_xscale('log', base=2)
ax2.set_xticks(xres_chk[:, 1])
tick_labels = ['({0:0.1f}, {1:0.0f})'.format(x[0], x[2]) for x in xres_chk[:, 0:3]]
ax2.set_xticklabels(tick_labels, rotation=-45)

ax2.set_xlabel(r'$\left(\hat{s}L_x,\, N_x\right)$')
ax2.set_ylabel(r'$\langle Q_{\mathrm{es}} \rangle$')
ax1.set_ylabel(r'$\langle Q_{\mathrm{es}} \rangle$')

# ky-Binormal Runs #
kymin_runs = np.array([8, 10, 11])-1
nky_runs = np.array([2, 4, 5])-1

ky_stack = np.stack((input_data['kymin'], input_data['nky0']*input_data['kymin'], input_data['nky0'], heat_flux), axis=1)
kymin_chk = np.array(sorted(ky_stack[kymin_runs], key=lambda x: x[0]))
nky_chk = np.array(sorted(ky_stack[nky_runs], key=lambda x: x[1]))

ax3.plot(kymin_chk[:, 0], kymin_chk[:, 3], c='k', ls='--')
ax3.scatter(kymin_chk[:, 0], kymin_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax3.scatter(base_case['kymin'], base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax3.plot(kymin_chk[:, 0], [heat_lower]*kymin_chk.shape[0], c='tab:red', ls='-')
ax3.plot(kymin_chk[:, 0], [heat_upper]*kymin_chk.shape[0], c='tab:red', ls='-')

ax3.set_xscale('log', base=2)
ax3.set_xticks(kymin_chk[:, 0])
tick_labels = ['({0:0.0f}, {1:0.1f})'.format(ky[2], ky[0]) for ky in kymin_chk[:, 0:3]]
ax3.set_xticklabels(tick_labels, rotation=-45)

ax4.plot(nky_chk[:, 1], nky_chk[:, 3], c='k', ls='--')
ax4.scatter(nky_chk[:, 1], nky_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax4.scatter(base_case['nky0']*base_case['kymin'], base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax4.plot(nky_chk[:, 1], [heat_lower]*nky_chk.shape[0], c='tab:red', ls='-')
ax4.plot(nky_chk[:, 1], [heat_upper]*nky_chk.shape[0], c='tab:red', ls='-')

ax4.set_xscale('log', base=2)
ax4.set_xticks(nky_chk[:, 1])
tick_labels = ['({0:0.0f}, {1:0.1f})'.format(nky[2], nky[0]) for nky in nky_chk[:, 0:3]]
ax4.set_xticklabels(tick_labels, rotation=-45)

ax4.set_xlabel(r'$\left(N_{ky},\, k_{y,\mathrm{min}}\right)$')

# z-Parallel Runs #
npol_runs = np.array([10, 12, 13])-1
nz_runs = np.array([4, 6, 7])-1

z_sorted = np.stack((input_data['npol'], input_data['npol']/input_data['nz0'], input_data['nz0'], heat_flux), axis=1)
npol_chk = np.array(sorted(z_sorted[npol_runs], key=lambda x: x[0]))
nz_chk = np.array(sorted(z_sorted[nz_runs], key=lambda x: x[1]))

ax5.plot(npol_chk[:, 0], npol_chk[:, 3], c='k', ls='--')
ax5.scatter(npol_chk[:, 0], npol_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax5.scatter(base_case['npol'], base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax5.plot(npol_chk[:, 0], [heat_lower]*npol_chk.shape[0], c='tab:red', ls='-')
ax5.plot(npol_chk[:, 0], [heat_upper]*npol_chk.shape[0], c='tab:red', ls='-')

ax5.set_xscale('log', base=2)
ax5.set_xticks(npol_chk[:, 0])
tick_labels = ['({0:0.0f}, {1:0.0f})'.format(npol[0], npol[2]) for npol in npol_chk[:, 0:3]]
ax5.set_xticklabels(tick_labels, rotation=-45)

ax6.plot(nz_chk[:, 1], nz_chk[:, 3], c='k', ls='--')
ax6.scatter(nz_chk[:, 1], nz_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax6.scatter(base_case['npol']/base_case['nz0'], base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax6.plot(nz_chk[:, 1], [heat_lower]*nz_chk.shape[0], c='tab:red', ls='-')
ax6.plot(nz_chk[:, 1], [heat_upper]*nz_chk.shape[0], c='tab:red', ls='-')

ax6.set_xscale('log', base=2)
ax6.set_xticks(nz_chk[:, 1])
tick_labels = ['({0:0.0f}, {1:0.0f})'.format(nz[0], nz[2]) for nz in nz_chk[:, 0:3]]
ax6.set_xticklabels(tick_labels, rotation=-45)

ax6.set_xlabel(r'$\left(N_{\mathrm{pol}},\, N_{z}\right)$')

# v-Velocity Runs #
lv_runs = np.array([14, 18])-1
nv_runs = np.array([10, 14, 15])-1

v_stack = np.stack((input_data['lv'], input_data['lv']/input_data['nv0'], input_data['nv0'], heat_flux), axis=1)
lv_chk = np.array(sorted(v_stack[lv_runs], key=lambda x: x[0]))
nv_chk = np.array(sorted(v_stack[nv_runs], key=lambda x: x[1]))

ax7.plot(lv_chk[:, 0], lv_chk[:, 3], c='k', ls='--')
ax7.scatter(lv_chk[:, 0], lv_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax7.scatter(base_case['lv'], base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax7.plot(lv_chk[:, 0], [heat_lower]*lv_chk.shape[0], c='tab:red', ls='-')
ax7.plot(lv_chk[:, 0], [heat_upper]*lv_chk.shape[0], c='tab:red', ls='-')

ax7.set_xscale('log', base=2)
ax7.set_xticks(lv_chk[:, 0])
tick_labels = ['({0:0.0f}, {1:0.0f})'.format(lv[0], lv[2]) for lv in lv_chk[:, 0:3]]
ax7.set_xticklabels(tick_labels, rotation=-45)

ax8.plot(nv_chk[:, 1], nv_chk[:, 3], c='k', ls='--')
ax8.scatter(nv_chk[:, 1], nv_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax8.scatter(base_case['lv']/base_case['nv0'], base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax8.plot(nv_chk[:, 1], [heat_lower]*nv_chk.shape[0], c='tab:red', ls='-')
ax8.plot(nv_chk[:, 1], [heat_upper]*nv_chk.shape[0], c='tab:red', ls='-')

ax8.set_xscale('log', base=2)
ax8.set_xticks(nv_chk[:, 1])
tick_labels = ['({0:0.0f}, {1:0.0f})'.format(nv[0], nv[2]) for nv in nv_chk[:, 0:3]]
ax8.set_xticklabels(tick_labels, rotation=-45)

ax8.set_xlabel(r'$\left(L_v,\, N_v\right)$')

# mu-Velocity Runs #
lw_runs = np.array([14, 19], dtype=int)-1
wres_runs = np.array([14, 16, 17])-1
wres = input_data['lw'] / input_data['nw0']
wres_base = base_case['lw'] / base_case['nw0']

w_stack = np.stack((input_data['lw'], wres, input_data['nw0'], heat_flux), axis=1)

lw_chk = np.array(sorted(w_stack[lw_runs], key=lambda x: x[0]))
wres_chk = np.array(sorted(w_stack[wres_runs], key=lambda x: x[1]))

ax9.plot(lw_chk[:, 0], lw_chk[:, 3], c='k', ls='--')
ax9.scatter(lw_chk[:, 0], lw_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax9.scatter(base_case['lw'], base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax9.plot(lw_chk[:, 0], [heat_lower]*lw_chk.shape[0], c='tab:red', ls='-')
ax9.plot(lw_chk[:, 0], [heat_upper]*lw_chk.shape[0], c='tab:red', ls='-')

ax9.set_xscale('log', base=2)
ax9.set_xticks(lw_chk[:, 0])
tick_labels = ['({0:0.0f}, {1:0.0f})'.format(lw[0], lw[2]) for lw in lw_chk[:, 0:3]]
ax9.set_xticklabels(tick_labels, rotation=-45)
ax9.set_ylim(0, 1.1*np.max(heat_flux))

ax10.plot(wres_chk[:, 1], wres_chk[:, 3], c='k', ls='--')
ax10.scatter(wres_chk[:, 1], wres_chk[:, 3], s=100, marker='s', facecolor='None', edgecolor='k')
ax10.scatter(wres_base, base_case['heat flux'], s=300, marker='*', facecolor='None', edgecolor='tab:red')
ax10.plot(wres_chk[:, 1], [heat_lower]*wres_chk.shape[0], c='tab:red', ls='-')
ax10.plot(wres_chk[:, 1], [heat_upper]*wres_chk.shape[0], c='tab:red', ls='-')

ax10.set_xscale('log', base=2)
ax10.set_xticks(wres_chk[:, 1])
tick_labels = ['({0:0.0f}, {1:0.0f})'.format(x[0], x[2]) for x in wres_chk[:, 0:3]]
ax10.set_xticklabels(tick_labels, rotation=-45)

ax10.set_xlabel(r'$\left(L_w,\, N_w\right)$')

# Save/Show #
# plt.show()
save_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', 'convergence_60-1-84.png')
plt.savefig(save_path)
