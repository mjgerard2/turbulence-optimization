import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import geneAnalysis.read_eigenvalues as re

class read_spec:
    """ A class for reading GENE fluctuation spectra data files.

    ...

    Attributes
    ----------
    spec_path : str
        Global path to spectral data file.

    Methods
    -------
    """
    def __init__(self, spec_path):
        if spec_path.endswith('.h5'):
            with hf.File(spec_path, 'r') as hf_:
                self.keys = []
                for key in hf_:
                    if key.endswith(',kx'):
                        self.keys.append(key.split(',')[0])
                self.spec_dom = {'kx': hf_['kx'][()],
                                 'ky': hf_['ky'][()]}
                self.spec = {}
                for key in self.keys:
                    self.spec['%s,kx' % key] = hf_['%s,kx' % key][()]
                    self.spec['%s,ky' % key] = hf_['%s,ky' % key][()]
                self.nkx = self.spec_dom['kx'].shape[0]
                self.nky = self.spec_dom['ky'].shape[0]
                self.nt = 1
        
        elif spec_path.endswith('.dat'):
            with open(spec_path, 'r') as f:
                # get number of time steps #
                lines = f.readlines()
                for l, line in enumerate(lines):
                    line_split = line.strip().split()
                    if line_split[0] == '#':
                        if len(line_split) > 1:
                            if line_split[1] == 'diag:':
                                self.nt = int(line_split[7].split('(')[1])
                    else:
                        lbeg = l
                        break

                # define spectral dictionary #
                spec = {}
                self.spec_dom = {}
                self.keys = lines[lbeg-1].strip().split()[2:]
                nkeys = len(self.keys)
                time_domain = np.empty(self.nt)
                time_domain[0] = float(lines[lbeg-(nkeys+2)].strip().split()[8])

                # get ky spectral grid #
                ky_init = []
                for l, line in enumerate(lines[lbeg:]):
                    line_split = line.strip().split()
                    if len(line_split) == 0:
                        lbeg += l+4+nkeys
                        break
                    else:
                        ky_init.append([float(x) for x in line_split])
                ky_init = np.array(ky_init)
                self.spec_dom['ky'] = ky_init[:,0]
                self.nky = ky_init.shape[0]
                for k, key in enumerate(self.keys):
                    spec['%s,ky' % key] = np.empty((self.nt, self.nky))
                    spec['%s,ky' % key][0] = ky_init[:,k+1]

                # read in remaining ky data #
                for i in range(self.nt-1):
                    l0 = lbeg + i*(self.nky+4+nkeys)
                    time_domain[i+1] = float(lines[l0-(nkeys+2)].strip().split()[8])
                    ky_array = np.empty((self.nky, len(self.keys)))
                    for l, line in enumerate(lines[l0:l0+self.nky]):
                        ky_array[l] = [float(x) for x in line.strip().split()[1:]]
                    for k, key in enumerate(self.keys):
                        spec['%s,ky' % key][i+1] = ky_array[:,k]
                lbeg += (self.nt-1)*(self.nky+4+nkeys)

                # get kx spectral grid #
                kx_init = []
                for l, line in enumerate(lines[lbeg:]):
                    line_split = line.strip().split()
                    if len(line_split) == 0:
                        lbeg += l+4+nkeys
                        break
                    else:
                        kx_init.append([float(x) for x in line_split])
                kx_init = np.array(kx_init)
                self.spec_dom['kx'] = kx_init[:,0]
                self.nkx = kx_init.shape[0]
                for k, key in enumerate(self.keys):
                    spec['%s,kx' % key] = np.empty((self.nt, self.nkx))
                    spec['%s,kx' % key][0] = kx_init[:,k+1]
           
                # read in remaining kx data #
                for i in range(self.nt-1):
                    l0 = lbeg + i*(self.nkx+4+nkeys)
                    kx_array = np.empty((self.nkx, len(self.keys)))
                    for l, line in enumerate(lines[l0:l0+self.nkx]):
                        kx_array[l] = [float(x) for x in line.strip().split()[1:]]
                    for k, key in enumerate(self.keys):
                        spec['%s,kx' % key][i+1] = kx_array[:,k]

                self.time_domain = time_domain
                self.spec = spec
        else:
            raise NameError('File type not supported: %s' % spec_path)

    def spectral_limit_ratio(self, lim, kdim='ky'):
        """ Compute the ratio of spectral content below a particular
        wavenumberto the total integrated spectrum.

        Parameters
        ----------
        lim : float
            Compute contribution from wavenumbers <= lim.
        kdim : str, optional
            Specify the ky or kx wavenumber. Default is ky
        """
        k_dom = self.spec_dom[kdim]
        idx = np.where(k_dom <= lim[1])[0]
        idx = np.where(k_dom[idx] >= lim[0])[0]
        if self.nt == 1:
            spec_ratio = {}
            for key in self.keys:
                spec_key = '%s,%s' % (key, kdim)
                spec = self.spec[spec_key]
                spec_tot = np.trapz(spec, k_dom)
                spec_ratio[spec_key] = np.trapz(spec[idx], k_dom[idx])/spec_tot

        else:
            spec_ratio = {}
            for key in self.keys:
                spec_key = '%s,%s' % (key, kdim)
                spec_ratio[spec_key] = np.empty(self.nt)
                spec_tfull = self.spec[','.join([key, kdim])]
                for s, spec in enumerate(spec_tfull):
                    spec_tot = np.trapz(spec, k_dom)
                    spec_ratio[spec_key][s] = np.trapz(spec[idx], k_dom[idx])/spec_tot

        return spec_ratio

    def plotting(self, fontsize=14, labelsize=16, linewidth=1, figsize=(8,6)):
        # Plotting Parameters #
        plt.close('all')

        font = {'family': 'sans-serif',
                'serif': 'Times New Roman',
                'weight': 'normal',
                'size': fontsize}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = labelsize
        mpl.rcParams['lines.linewidth'] = linewidth
        mpl.rcParams['figure.figsize'] = figsize

        fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=figsize)
        return plt, fig, ax

if __name__ == '__main__':
    import geneAnalysis.read_zonal_diag as rz

    # spec_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'spectrae_4_14.dat')
    # zonal_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'zonale_4_14.dat')
    # spec_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'spectrae_4_7.dat')
    # zonal_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'zonale_4_7.dat')
    # spec_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'spectrae_3_11.dat')
    # zonal_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'zonale_3_11.dat')
    spec_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p5_omti_0p0_omte_2p0', 'spectrae_6_15.dat')
    zonal_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p5_omti_0p0_omte_2p0', 'zonale_6_15.dat')
    sp = read_spec(spec_path)
    zn = rz.read_zonal(zonal_path)

    lim = [0.05, .15]
    kdim = 'ky'
    spec_ratio = sp.spectral_limit_ratio(lim, kdim=kdim)
    plt, fig, ax1 = sp.plotting()
    ax2 = ax1.twinx()

    ax1.plot(sp.time_domain, spec_ratio['phi,'+kdim], c='tab:blue', label=r'$\langle|\phi|\rangle_{{k_\mathrm{{y}} \leq {0:0.2f}}} \ / \ \langle|\phi|\rangle$'.format(lim[1]))
    ax1.plot(sp.time_domain, spec_ratio['Apar,'+kdim], c='tab:red', label=r'$\langle|A_{{\parallel}}|\rangle_{{k_\mathrm{{y}} \leq {0:0.2f}}} \ / \ \langle|A_{{\parallel}}|\rangle$'.format(lim[1]))
    ax1.plot(sp.time_domain, spec_ratio['n,'+kdim], c='tab:green', label=r'$\langle|n|\rangle_{{k_\mathrm{{y}} \leq {0:0.2f}}} \ / \ \langle|n|\rangle$'.format(lim[1]))
    ax2.plot(zn.zonal_data[:,0], zn.zonal_data[:,1], c='k')

    ax1.set_xlim(0, sp.time_domain[-1])
    ax1.set_ylim(0, 1)
    ax2.set_ylim(0, ax2.get_ylim()[1])

    ax1.set_xlabel(r'$t \ / \ (a/c_\mathrm{s})$')
    ax2.set_ylabel(r'$\phi_\mathrm{zonal}$')
    ax1.legend(frameon=False)

    plt.show()
