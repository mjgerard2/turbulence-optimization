import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization')
sys.path.append(WORKDIR)
import plot_define as pd


class nrg_summary:
    """ A class for reading GENE hdf5 nrg summary data files.

    ...

    Attributes
    ----------
    nrg_path : str
        Global path to nrg summary file.

    Methods
    -------
    nrg_average(start_time, end_time=-1)
        Calculate time average of nonlinear quantities.
    """
    def __init__(self, nrg_path):

        self.time_series = {}
        self.var_names = []
        with hf.File(nrg_path, 'r') as hf_:
            for key in hf_:
                key_split = key.split(',')
                if len(key_split) > 1:
                    self.var_names.append(key_split[0])
                    self.time_series[key_split[0]] = hf_[key][()]
                elif key == 't':
                    self.time_domain = hf_[key][()]

    def nrg_average(self, start_time, end_time=-1):
        """ Calculate time average of nonlinear quantities.

        Parameters
        ----------
        start_time : float
            beginning of time window over which average is calculated
        end_time : float (optional)
            end of time window over which average is calculated. Default is
            -1, which averages out to final time slice.
        """
        start_idx = np.argmin(np.abs(self.time_domain - start_time))
        if end_time == -1:
            end_idx = self.time_domain.shape[0]
        else:
            end_idx = np.argmin(np.abs(self.time_domain - end_time))+1

        time_domain = self.time_domain[start_idx:end_idx]
        t_norm = 1./(time_domain[-1] - time_domain[0])

        self.time_average = {}
        self.time_std = {}
        for key in self.time_series:
            if key != 't':
                t_data = self.time_series[key][start_idx:end_idx]
                t_avg = np.trapz(t_data, time_domain)*t_norm
                t_std = np.sqrt(np.trapz((t_data - t_avg)**2, time_domain)*t_norm)

                self.time_average[key] = t_avg
                self.time_std[key] = t_std


if __name__ == "__main__":
    var = 'P_tot'
    scl = 1.392265193

    nrg_path_e = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_1_3.h5')
    nrg_e = nrg_summary(nrg_path_e)
    nrg_e.nrg_average(250)
    avg_e = scl*nrg_e.time_average[var]

    nrg_path_i = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummaryi_1_3.h5')
    nrg_i = nrg_summary(nrg_path_i)
    nrg_i.nrg_average(250)
    avg_i = scl*nrg_i.time_average[var]

    plot = pd.plot_define()
    fig, ax = plt.subplots(1, 1, tight_layout=True)

    ax.plot(nrg_e.time_domain, scl*nrg_e.time_series[var], c='tab:blue', label=r'electrons: $\langle \Pi_{{\mathrm{{es}}}} \rangle_{{t \geq 250}} = {0:0.2g} \ (\mathrm{{N}}/\mathrm{{cm}})$'.format(avg_e))
    ax.plot(nrg_i.time_domain, scl*nrg_i.time_series[var], c='tab:orange', label=r'ions: $\langle \Pi_{{\mathrm{{es}}}} \rangle_{{t \geq 250}} = {0:0.2g} \ (\mathrm{{N}}/\mathrm{{cm}})$'.format(avg_i))

    ax.set_xlabel(r'$t \ (a/c_s)$')
    ax.set_ylabel(r'$\langle \Pi_{\mathrm{es}} \rangle \ (\mathrm{N}/\mathrm{cm})$')

    max_e = np.max(np.abs(nrg_e.time_series[var]))
    max_i = np.max(np.abs(nrg_i.time_series[var]))
    max_var = np.max(np.array([max_e, max_i]))

    ax.set_xlim(0, nrg_e.time_domain[-1])
    ax.set_ylim(-max_var, max_var)

    ax.grid()
    ax.legend()

    plt.show()
