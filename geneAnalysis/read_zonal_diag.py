import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import geneAnalysis.read_eigenvalues as re


class read_zonal:
    """ A class for reading GENE zonal flow data files.

    ...

    Attributes
    ----------
    data_path : str
        Global path to zonal flow data file.

    Methods
    -------
    """
    def __init__(self, data_path):
        with open(data_path, 'r') as f:
            lines = f.readlines()
            for l, line in enumerate(lines):
                line_split = line.strip().split()
                if len(line_split) > 1:
                    if line_split[1] == 'diag:':
                        self.nt = int(line_split[7].split('(')[1])
                else:
                    lbeg = l+5
                    break

            zonal_data = np.empty((self.nt, 3))
            for l, line in enumerate(lines[lbeg:lbeg+self.nt]):
                line_split = line.strip().split()
                zonal_data[l] = [float(x) for x in line_split]
            self.zonal_data = zonal_data

if __name__ == '__main__':
    data_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'zonale_4_14.dat')
    zn = read_zonal(data_path)
    print(zn.zonal_data[:,2])
