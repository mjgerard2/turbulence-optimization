import os
import numpy as np

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd
import geneAnalysis.read_nrg_summary as rnrg
import geneAnalysis.read_spectrum as rspec
import geneAnalysis.read_contours as rcont


# spectrum paths #
qhs_spec_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'fluxspectrae_1_act.dat')
qhs_spec_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'fluxspectrae_00_06.dat')
qhs_spec_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'fluxspectrae_26_44.dat')
qhs_spec_paths = [qhs_spec_1p5, qhs_spec_2p0, qhs_spec_2p5]

# contour paths #
qhs_cont_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'conte_1_act.dat')
qhs_cont_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'conte_00_06.dat')
qhs_cont_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'conte_26_44.dat')

# parameters paths #
qhs_pram_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'parameters_1')
qhs_pram_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_00')
qhs_pram_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'parameters_26')
qhs_pram_paths = [qhs_pram_1p5, qhs_pram_2p0, qhs_pram_2p5]

# define time average domain #
qhs_t_doms = [250, 2000, 850]
qhs_cont_t = [1085, 1747]

# read in spectral data #
qhs_ky_spec = []
for i, (spec_path, pram_path, t0) in enumerate(zip(qhs_spec_paths, qhs_pram_paths, qhs_t_doms)):
    spec = rspec.spectrum(spec_path, pram_path)
    spec.spectrum_average(t0)
    spec.spectrum_normalization()
    ky = spec.ky_domain
    ky_norm = 1./spec.spec_norms['ky']['Q_es']
    ky_amp = spec.spec_data_avg['ky']['Q_es']*ky_norm
    qhs_ky_spec.append(np.stack((ky, ky_amp), axis=0))

# plotting axes #
plot = pd.plot_define(fontSize=18, labelSize=22, lineWidth=2)
plt = plot.plt

fig = plt.figure(figsize=(10, 8))
gs = fig.add_gridspec(4, 4)

ax1 = fig.add_subplot(gs[0:2, 0:4])
ax2 = fig.add_subplot(gs[2:4, 0:2])
ax3 = fig.add_subplot(gs[2:4, 2:4])

ls = ['-', '--', '-.']
eta_dom = np.array([1.5, 2.0, 2.5])

# plot spectral data #
for i, eta in enumerate((eta_dom)):
    ax1.plot(qhs_ky_spec[i][0], qhs_ky_spec[i][1], c='tab:blue', ls=ls[i], label=r'$a/L_{{\mathrm{{n}}}} = {}$'.format(eta))

# plot contour data #
props = dict(boxstyle='round', facecolor='w', alpha=0.75)

cont = rcont.contours(qhs_cont_1p5, qhs_pram_1p5)
cdx = np.argmin(np.abs(cont.time_domain - qhs_cont_t[0]))
cmap2 = ax2.pcolormesh(cont.x_domain, cont.y_domain, cont.cont_data['phi'][cdx].T, cmap='coolwarm', shading='gouraud')
ax2.text(.9*ax2.get_xlim()[0], .87*ax2.get_ylim()[0], r'$a/L_{\mathrm{n}} = 1.5$', bbox=props, fontsize=16)

cont = rcont.contours(qhs_cont_2p5, qhs_pram_2p5)
cdx = np.argmin(np.abs(cont.time_domain - qhs_cont_t[1]))
cmap3 = ax3.pcolormesh(cont.x_domain, cont.y_domain, cont.cont_data['phi'][cdx].T, cmap='coolwarm', shading='gouraud')
ax3.text(.9*ax3.get_xlim()[0], .87*ax3.get_ylim()[0], r'$a/L_{\mathrm{n}} = 2.5$', bbox=props, fontsize=16)

# axis labels #
ax1.set_xlabel(r'$k_{\mathrm{y}}\rho_{\mathrm{s}}$')
ax1.set_ylabel(r'$Q^{es}_{\mathrm{e}} \ (\mathrm{a.u.})$')

ax2.set_xlabel(r'$x/\rho_{\mathrm{s}}$')
ax2.set_ylabel(r'$y/\rho_{\mathrm{s}}$')
cax2 = fig.colorbar(cmap2, ax=ax2)
cax2.ax.set_ylabel(r'$\phi$')

ax3.set_xlabel(r'$x/\rho_{\mathrm{s}}$')
cax3 = fig.colorbar(cmap3, ax=ax3)
cax3.ax.set_ylabel(r'$\phi$')

# axis limits #
ax1.set_xlim(0, 3.5)
ax1.set_ylim(0, ax1.get_ylim()[1])

# axis gird/legends #
ax1.grid()
ax1.legend(frameon=False)

# save/show #
plt.tight_layout()
# plt.show()
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'TTF_2023', 'figures', 'qhs_heat_flux.png')
plt.savefig(save_path)
