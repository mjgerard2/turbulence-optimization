import os
import numpy as np

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd
import geneAnalysis.read_nrg_summary as rnrg
import geneAnalysis.read_spectrum as rspec
import geneAnalysis.read_contours as rcont


# nrg summary paths #
qhs_nrg_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_act.h5')
qhs_nrg_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_00_06.h5')
qhs_nrg_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'nrgsummarye_26_44.h5')
qhs_nrg_paths = [qhs_nrg_1p5, qhs_nrg_2p0, qhs_nrg_2p5]

he_nrg_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_2.h5')
he_nrg_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_2.h5')
he_nrg_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p5_omti_0p0_omte_2p0', 'nrgsummarye_6_12.h5')
he_nrg_paths = [he_nrg_1p5, he_nrg_2p0, he_nrg_2p5]

# spectrum paths #
qhs_spec_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'fluxspectrae_1_act.dat')
qhs_spec_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'fluxspectrae_00_06.dat')
qhs_spec_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'fluxspectrae_26_44.dat')
qhs_spec_paths = [qhs_spec_1p5, qhs_spec_2p0, qhs_spec_2p5]

he_spec_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'fluxspectrae_1_2.dat')
he_spec_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'fluxspectrae_2.dat')
he_spec_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p5_omti_0p0_omte_2p0', 'fluxspectrae_6_12.dat')
he_spec_paths = [he_spec_1p5, he_spec_2p0, he_spec_2p5]

# contour paths #
qhs_cont_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'conte_1_act.dat')
qhs_cont_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'conte_00_06.dat')
qhs_cont_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'conte_26_44.dat')

he_cont_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'conte_1_2.dat')
he_cont_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'conte_2.dat')
he_cont_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p5_omti_0p0_omte_2p0', 'conte_6_12.dat')

# parameters paths #
qhs_pram_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'parameters_1')
qhs_pram_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_00')
qhs_pram_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'parameters_26')
qhs_pram_paths = [qhs_pram_1p5, qhs_pram_2p0, qhs_pram_2p5]

he_pram_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'parameters_1')
he_pram_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_2')
he_pram_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p5_omti_0p0_omte_2p0', 'parameters_6')
he_pram_paths = [he_pram_1p5, he_pram_2p0, he_pram_2p5]

# define time average domain #
qhs_t_doms = [250, 2000, 850]
he_t_doms = [250, 250, 800]
qhs_cont_t = [1085, 1747]
he_cont_t = [1165, 1450]

# read in nrg data #
qhs_nrg = np.empty(len(qhs_nrg_paths))
for i, nrg_path in enumerate(qhs_nrg_paths):
    nrg = rnrg.nrg_summary(nrg_path)
    nrg.nrg_average(qhs_t_doms[i])
    qhs_nrg[i] = nrg.time_average['Q_tot']

he_nrg = np.empty(len(he_nrg_paths))
for i, nrg_path in enumerate(he_nrg_paths):
    nrg = rnrg.nrg_summary(nrg_path)
    nrg.nrg_average(he_t_doms[i])
    he_nrg[i] = nrg.time_average['Q_tot']

# read in spectral data #
qhs_ky_spec = []
for i, (spec_path, pram_path, t0) in enumerate(zip(qhs_spec_paths, qhs_pram_paths, he_t_doms)):
    spec = rspec.spectrum(spec_path, pram_path)
    spec.spectrum_average(t0)
    spec.spectrum_normalization()
    ky = spec.ky_domain
    ky_norm = 1./spec.spec_norms['ky']['Q_es']
    ky_amp = spec.spec_data_avg['ky']['Q_es']*ky_norm
    qhs_ky_spec.append(np.stack((ky, ky_amp), axis=0))

he_ky_spec = []
for i, (spec_path, pram_path, t0) in enumerate(zip(he_spec_paths, he_pram_paths, he_t_doms)):
    spec = rspec.spectrum(spec_path, pram_path)
    spec.spectrum_average(t0)
    spec.spectrum_normalization()
    ky = spec.ky_domain
    ky_norm = 1./spec.spec_norms['ky']['Q_es']
    ky_amp = spec.spec_data_avg['ky']['Q_es']*ky_norm
    he_ky_spec.append(np.stack((ky, ky_amp), axis=0))

# plotting axes #
plot = pd.plot_define(fontSize=18, labelSize=22)
plt = plot.plt

fig = plt.figure(figsize=(22, 6))
gs = fig.add_gridspec(2, 6)

ax1 = fig.add_subplot(gs[0:2, 0:2])
ax2 = fig.add_subplot(gs[0, 2:4])
ax3 = fig.add_subplot(gs[1, 2:4])
ax4 = fig.add_subplot(gs[0, 4])
ax5 = fig.add_subplot(gs[0, 5])
ax6 = fig.add_subplot(gs[1, 4])
ax7 = fig.add_subplot(gs[1, 5])

ls = ['-', '--', '-.']
eta_dom = np.array([1.5, 2.0, 2.5])

# plot nrg data #
plt1, = ax1.plot(eta_dom, qhs_nrg, marker='o', ms=15, mfc='None', mec='tab:blue', c='tab:blue', ls='--', label='QHS')
plt2, = ax1.plot(eta_dom, he_nrg, marker='s', ms=15, mfc='None', mec='tab:orange', c='tab:orange', ls='--', label='HE-QHS')

# plot spectral data #
for i, eta in enumerate((eta_dom)):
    ax2.plot(qhs_ky_spec[i][0], qhs_ky_spec[i][1], c=plt1.get_color(), ls=ls[i], label=r'$a/L_{{\mathrm{{n}}}} = {}$'.format(eta))
    ax3.plot(he_ky_spec[i][0], he_ky_spec[i][1], c=plt2.get_color(), ls=ls[i], label=r'$a/L_{{\mathrm{{n}}}} = {}$'.format(eta))

# plot contour data #
props = dict(boxstyle='round', facecolor='w', alpha=0.75)

cont = rcont.contours(qhs_cont_1p5, qhs_pram_1p5)
cdx = np.argmin(np.abs(cont.time_domain - qhs_cont_t[0]))
cmap4 = ax4.pcolormesh(cont.x_domain, cont.y_domain, cont.cont_data['phi'][cdx].T, cmap='coolwarm', shading='gouraud')
ax4.text(.9*ax4.get_xlim()[0], .87*ax4.get_ylim()[0], r'$a/L_{\mathrm{n}} = 1.5$', bbox=props, fontsize=16)

cont = rcont.contours(qhs_cont_2p5, qhs_pram_2p5)
cdx = np.argmin(np.abs(cont.time_domain - qhs_cont_t[1]))
cmap5 = ax5.pcolormesh(cont.x_domain, cont.y_domain, cont.cont_data['phi'][cdx].T, cmap='coolwarm', shading='gouraud')
ax5.text(.9*ax5.get_xlim()[0], .87*ax5.get_ylim()[0], r'$a/L_{\mathrm{n}} = 2.5$', bbox=props, fontsize=16)

cont = rcont.contours(he_cont_1p5, he_pram_1p5)
cdx = np.argmin(np.abs(cont.time_domain - he_cont_t[0]))
cmap6 = ax6.pcolormesh(cont.x_domain, cont.y_domain, cont.cont_data['phi'][cdx].T, cmap='coolwarm', shading='gouraud')
ax6.text(.9*ax6.get_xlim()[0], .87*ax6.get_ylim()[0], r'$a/L_{\mathrm{n}} = 1.5$', bbox=props, fontsize=16)

cont = rcont.contours(he_cont_2p5, he_pram_2p5)
cdx = np.argmin(np.abs(cont.time_domain - he_cont_t[1]))
cmap7 = ax7.pcolormesh(cont.x_domain, cont.y_domain, cont.cont_data['phi'][cdx].T, cmap='coolwarm', shading='gouraud')
ax7.text(.9*ax7.get_xlim()[0], .87*ax7.get_ylim()[0], r'$a/L_{\mathrm{n}} = 2.5$', bbox=props, fontsize=16)

# axis labels #
ax1.set_xticks(eta_dom)
ax1.set_xlabel(r'$a/L_{\mathrm{n}}$')
ax1.set_ylabel(r'$Q^{es}_{\mathrm{e}} / (c_sn_{\mathrm{e}}T_{\mathrm{e}}(\rho_s/a)^2)$')

ax2.set_xlabel(r'$k_{\mathrm{y}}\rho_{\mathrm{s}}$')
ax2.set_ylabel(r'$Q^{es}_{\mathrm{e}} \ (\mathrm{a.u.})$')

ax3.set_xlabel(r'$k_{\mathrm{y}}\rho_{\mathrm{s}}$')
ax3.set_ylabel(r'$Q^{es}_{\mathrm{e}} \ (\mathrm{a.u.})$')

ax4.set_xlabel(r'$x/\rho_{\mathrm{s}}$')
ax4.set_ylabel(r'$y/\rho_{\mathrm{s}}$')
cax4 = fig.colorbar(cmap4, ax=ax4)
cax4.ax.set_ylabel(r'$\phi$')

ax5.set_xlabel(r'$x/\rho_{\mathrm{s}}$')
ax5.set_ylabel(r'$y/\rho_{\mathrm{s}}$')
cax5 = fig.colorbar(cmap5, ax=ax5)
cax5.ax.set_ylabel(r'$\phi$')

ax6.set_xlabel(r'$x/\rho_{\mathrm{s}}$')
ax6.set_ylabel(r'$y/\rho_{\mathrm{s}}$')
cax6 = fig.colorbar(cmap6, ax=ax6)
cax6.ax.set_ylabel(r'$\phi$')

ax7.set_xlabel(r'$x/\rho_{\mathrm{s}}$')
ax7.set_ylabel(r'$y/\rho_{\mathrm{s}}$')
cax7 = fig.colorbar(cmap7, ax=ax7)
cax7.ax.set_ylabel(r'$\phi$')

# axis limits #
ax1.set_ylim(0, ax1.get_ylim()[1])

ax2.set_xlim(0, ax2.get_xlim()[1])
ax2.set_ylim(0, ax2.get_ylim()[1])

ax3.set_xlim(0, ax3.get_xlim()[1])
ax3.set_ylim(0, ax3.get_ylim()[1])

# axis gird/legends #
ax1.grid()
ax2.grid()
ax3.grid()

ax1.legend(frameon=False)
ax2.legend(frameon=False, title='QHS')
ax3.legend(frameon=False, title='HE-QHS')

# save/show #
plt.tight_layout()
# plt.show()
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'TTF_2023', 'figures', 'nonlinear_heat_flux.png')
plt.savefig(save_path)
