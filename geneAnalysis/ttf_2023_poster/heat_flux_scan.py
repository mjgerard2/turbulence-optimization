import os
import numpy as np

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd
import geneAnalysis.read_nrg_summary as rnrg


# nrg summary paths #
qhs_nrg_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_act.h5')
qhs_nrg_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_00_06.h5')
qhs_nrg_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'nrgsummarye_26_44.h5')
qhs_nrg_paths = [qhs_nrg_1p5, qhs_nrg_2p0, qhs_nrg_2p5]

he_nrg_1p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_2.h5')
he_nrg_2p0 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_2.h5')
he_nrg_2p5 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p5_omti_0p0_omte_2p0', 'nrgsummarye_6_12.h5')
he_nrg_paths = [he_nrg_1p5, he_nrg_2p0, he_nrg_2p5]

# define time average domain #
qhs_t_doms = [250, 2000, 850]
he_t_doms = [250, 250, 800]
eta_dom = np.array([1.5, 2.0, 2.5])

# read in nrg data #
qhs_nrg = np.empty(len(qhs_nrg_paths))
for i, nrg_path in enumerate(qhs_nrg_paths):
    nrg = rnrg.nrg_summary(nrg_path)
    nrg.nrg_average(qhs_t_doms[i])
    qhs_nrg[i] = nrg.time_average['Q_tot']

he_nrg = np.empty(len(he_nrg_paths))
for i, nrg_path in enumerate(he_nrg_paths):
    nrg = rnrg.nrg_summary(nrg_path)
    nrg.nrg_average(he_t_doms[i])
    he_nrg[i] = nrg.time_average['Q_tot']

# plotting axes #
plot = pd.plot_define(fontSize=20, labelSize=24, lineWidth=2)
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(5.5, 5))

# plot nrg data #
ax.plot(eta_dom, qhs_nrg, marker='o', ms=15, mfc='None', mec='tab:blue', c='tab:blue', ls='--', label='QHS')
ax.plot(eta_dom, he_nrg, marker='s', ms=15, mfc='None', mec='tab:orange', c='tab:orange', ls='--', label='HE-QHS')

# axis labels #
ax.set_xticks(eta_dom)
ax.set_xlabel(r'$a/L_{\mathrm{n}}$')
ax.set_ylabel(r'$Q^{es}_{\mathrm{e}} / (c_sn_{\mathrm{e}}T_{\mathrm{e}}(\rho_s/a)^2)$')

# axis limits #
ax.set_ylim(0, ax.get_ylim()[1])

# axis gird/legends #
ax.grid()
ax.legend(frameon=False)

# save/show #
plt.tight_layout()
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'TTF_2023', 'figures', 'heat_flux.png')
plt.savefig(save_path)
