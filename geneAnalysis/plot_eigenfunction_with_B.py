import os
import numpy as np
import read_eigendata as re

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)
import gistTools.gist_reader as gr

import matplotlib as mpl
import matplotlib.pyplot as plt


# Read in gist data #
gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_s0p5_alpha0_npol4_nz8192.dat')
gist = gr.read_gist(gist_path)

B = gist.data['modB']

# Read in eigen data #
eigen_path = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0', 'ky_0p1', 'eigendata_1_1.h5')
eigen = re.read_eigendata(eigen_path)

n_pol = gist.n_pol
pol_dom = eigen.pol_dom[(eigen.pol_dom > -n_pol*np.pi) & (eigen.pol_dom < n_pol*np.pi)]

eigen.normalize_phi(pol_dom)
eigen_amp = eigen.amplitude[(eigen.pol_dom > -n_pol*np.pi) & (eigen.pol_dom < n_pol*np.pi)]

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24

# Plotting Axis #
fig, ax1 = plt.subplots(1, 1, tight_layout=True)
ax2 = ax1.twinx()

ax1.plot(gist.pol_dom/np.pi, B, c='k')
ax2.plot(pol_dom/np.pi, eigen_amp, c='tab:red')

ax1.set_xlabel(r'$\theta / \pi$')
ax1.set_ylabel(r'$B$')
ax2.set_ylabel(r'$|\phi|$')

ax1.grid()
plt.show()
