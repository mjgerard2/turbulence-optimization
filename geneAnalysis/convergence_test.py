import os
import numpy as np

from pandas_ods_reader import read_ods

import matplotlib as mpl
import matplotlib.pyplot as plt


runBC = 19
conID = '897-1-0'

base_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', 'TEM_conChk.ods')
df = read_ods(base_path, 1, headers=False)

found_ID = False
for i, elem in enumerate(df['column.0']):
    if elem == conID:
        found_ID = True
        idx = i
    if found_ID:
        if elem == 'Heat Flux':
            jdx = i+1
            break

if not found_ID:
    raise KeyError('Configuration ID %s has not been found' % conID)

con_head = {}
for i in range(jdx-idx):
    for key in df:
        if key == 'column.0':
            con_key = df[key][idx+i].strip().split()[0]
            con_head[con_key] = []
        else:
            con_head[con_key].append(df[key][idx+i])

for idx, run in enumerate(con_head[conID]):
    if run == None:
        run_cnt = int(con_head[conID][idx-1].strip().split('_')[0])
        break
    elif run.strip().split('_')[0][-1] == 'f':
        run_cnt = int(con_head[conID][idx-1].strip().split('_')[0])
        break

input_data = {}
base_case = {}
for key in con_head:
    if key == 'Heat':
        heat_flux = np.empty(run_cnt)
        for i in range(run_cnt):
            heat_flux[i] = float(con_head['Heat'][i].strip().split()[0])
        base_case['heat flux'] = heat_flux[runBC-1]
    elif key != conID:
        input_data[key] = np.array(con_head[key][0:run_cnt])
        base_case[key] = con_head[key][runBC-1]

heat_lower = (19./21)*base_case['heat flux']
heat_upper = (21./19)*base_case['heat flux']

check_data = {}
base_data = {}
keys = ['lx', 'lv', 'lw', 'npol', 'nky0', 'nx0', 'nv0', 'nw0', 'nz0', 'kymin']

index_check = {}
for key in input_data:
    if key == 'nexc':
        lx = (-1*input_data[key]) / (input_data['kymin']*input_data['npol'])
        lx_diff = lx - np.roll(lx, 1)
        lx_diff[0] = 0
        lx_chg = np.where(lx_diff != 0)[0]

        lx_res = lx / input_data['nx0']
        lx_res_diff = lx_res - np.roll(lx_res, 1)
        lx_res_diff[0] = 0
        lx_res_no_chg = np.where(lx_res_diff == 0)[0]

        check_indices = np.intersect1d(lx_chg, lx_res_no_chg)
        base_params = {}
        for sub_key in input_data:
            if all([ikey != sub_key for ikey in ['nx0', 'nexc', 'npol', 'kymin']]):
                base_i = check_indices[0]
                base_params[sub_key] = np.empty(check_indices[0], dtype=bool)
                for i in np.arange(check_indices[0]):
                    if input_data[sub_key][i] == input_data[sub_key][base_i]:
                        base_params[sub_key][i] = True
                    else:
                        base_params[sub_key][i] = False

        for i in np.arange(1, check_indices[0]+1):
            base_chk = np.empty(len(base_params), dtype=bool)
            for j, base_key in enumerate(base_params):
                base_chk[j] = base_params[base_key][check_indices[0]-i]
            if all(base_chk):
                check_indices = np.insert(check_indices, 0, check_indices[0]-i)
                break

        index_check['lx'] = check_indices 
    else:
        chg = input_data[key] - np.roll(input_data[key], 1)
        chg[0] = 0
        index_check[key] = np.where(chg != 0)[0]
        # base_change[key] = np.where(input_data[key] != base_case[key])[0]

#for key in base_change:
print('lx: '+' '.join(['{}'.format(i) for i in index_check['lx']]))
"""
for key in keys:
    if key == 'lx':
        lx = np.empty((run_cnt, 2))
        for i in range(run_cnt):
            lx[i, 0] = (-1*con_head['nexc'][i]) / (con_head['kymin'][i]*con_head['npol'][i])
            lx[i, 1] = heat_flux[i]
        lx_base = lx[runBC-1]
        lx_chk = np.append(lx_base, lx[np.where(lx[:, 0] != lx_base[0])])
        lx_chk = lx_chk.reshape(int(lx_chk.shape[0]/2), 2)
        base_data[key] = lx_base
        check_data[key] = np.array(sorted(lx_chk, key=lambda x: x[0]))

    elif key == 'nx0':
        nx = np.empty((run_cnt, 4))
        for i in range(run_cnt):
            nx[i, 0] = con_head['nx0'][i]
            nx[i, 1] = (-1*con_head['nexc'][i]) / (con_head['kymin'][i]*con_head['npol'][i])
            nx[i, 2] = round(nx[i, 1] / con_head['nx0'][i], 6)
            nx[i, 3] = heat_flux[i]
        nx_base = nx[runBC-1]
        nx_chk = np.append(nx_base, nx[np.where(nx[:, 2] != nx_base[2])])
        nx_chk = nx_chk.reshape(int(nx_chk.shape[0]/4), 4)
        base_data[key] = nx_base
        check_data[key] = np.array(sorted(nx_chk, key=lambda x: x[2]))

    else:
        check_data[key] = con_head[key][0:run_cnt]

plt.plot(check_data['lx'][:, 0], check_data['lx'][:, 1], c='k', ls=':')
idx_below = np.where(check_data['lx'][:, 0] < base_data['lx'][0])
idx_above = np.where(check_data['lx'][:, 0] > base_data['lx'][0])
plt.scatter(check_data['lx'][idx_below, 0], check_data['lx'][idx_below, 1], marker='s', s=100, facecolor='None', edgecolor='k')
plt.scatter(check_data['lx'][idx_above, 0], check_data['lx'][idx_above, 1], marker='s', s=100, facecolor='None', edgecolor='k')
plt.scatter(base_data['lx'][0], base_data['lx'][1], marker='*', s=300, facecolor='None', edgecolor='tab:red')
plt.plot(check_data['lx'][:, 0], [heat_lower]*check_data['lx'].shape[0], c='tab:red', ls='-')
plt.plot(check_data['lx'][:, 0], [heat_upper]*check_data['lx'].shape[0], c='tab:red', ls='-')
"""
"""

plt.plot(check_data['nx0'][:, 2], check_data['nx0'][:, 3], c='k', ls=':')
idx_below = np.where(check_data['nx0'][:, 2] < base_data['nx0'][2])
idx_above = np.where(check_data['nx0'][:, 2] > base_data['nx0'][2])
plt.scatter(check_data['nx0'][idx_below, 2], check_data['nx0'][idx_below, 3], marker='s', s=100, facecolor='None', edgecolor='k')
plt.scatter(check_data['nx0'][idx_above, 2], check_data['nx0'][idx_above, 3], marker='s', s=100, facecolor='None', edgecolor='k')
plt.scatter(base_data['nx0'][2], base_data['nx0'][3], marker='*', s=300, facecolor='None', edgecolor='tab:red')
plt.plot(check_data['nx0'][:, 2], [heat_lower]*check_data['nx0'].shape[0], c='tab:red', ls='-')
plt.plot(check_data['nx0'][:, 2], [heat_upper]*check_data['nx0'].shape[0], c='tab:red', ls='-')

"""
# plt.show()
