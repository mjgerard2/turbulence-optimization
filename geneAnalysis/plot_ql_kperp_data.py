import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import geneAnalysis.QL_model as QLm


ky_dom = np.linspace(0.4, 2.4, 6)
base_path = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'omn_1p5_omti_0p0_omte_2p0')

# plotting parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 16}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 18
mpl.rcParams['lines.linewidth'] = 2

fig, axs = plt.subplots(2, 3, sharex=False, sharey=False, tight_layout=True, figsize=(12, 8))

xmax = 0
for i in range(1,2):
    QL = QLm.QL_Data(base_path, ky_dom, nconn=i)
    for j, ky in enumerate(ky_dom):
        ky_tag = 'ky{0:0.1f}'.format(ky).replace('.', 'p')
        data = QL.QL_dict[ky_tag][:, 2]
        xcnt = np.arange(1, data.shape[0]+1)
        if data.shape[0] > xmax:
            xmax = data.shape[0]

        c = j % 3
        r = int((j-c)/3)
        if c == 0 and r == 0:
            plt1, = axs[r,c].plot(xcnt, data, label='n_conn = {}'.format(i), c='k')
        else:
            axs[r,c].plot(xcnt, data, c=plt1.get_color())

        if i == 1:
            axs[r,c].set_title(r'$k_y\rho_{{\mathrm{{s}}}} = {0:0.1f}$'.format(ky))

# axis labels #
axs[1, 0].set_xlabel(r'$i_{\mathrm{ev}}$')
axs[1, 1].set_xlabel(r'$i_{\mathrm{ev}}$')
axs[1, 2].set_xlabel(r'$i_{\mathrm{ev}}$')

# axs[0, 0].set_ylabel(r'$\langle k_{\perp} \rangle$')
# axs[1, 0].set_ylabel(r'$\langle k_{\perp} \rangle$')
axs[0, 0].set_ylabel(r'$w$')
axs[1, 0].set_ylabel(r'$w$')

# axis limits #
axs[0,0].set_xlim(0, axs[0,0].get_xlim()[1])
axs[0,1].set_xlim(0, axs[0,1].get_xlim()[1])
axs[0,2].set_xlim(0, axs[0,2].get_xlim()[1])
axs[1,0].set_xlim(0, axs[1,0].get_xlim()[1])
axs[1,1].set_xlim(0, axs[1,1].get_xlim()[1])
axs[1,2].set_xlim(0, axs[1,2].get_xlim()[1])

axs[0,0].set_ylim(0, axs[0,0].get_ylim()[1])
axs[0,1].set_ylim(0, axs[0,1].get_ylim()[1])
axs[0,2].set_ylim(0, axs[0,2].get_ylim()[1])
axs[1,0].set_ylim(0, axs[1,0].get_ylim()[1])
axs[1,1].set_ylim(0, axs[1,1].get_ylim()[1])
axs[1,2].set_ylim(0, axs[1,2].get_ylim()[1])

# grids and legends #
axs[0,0].grid()
axs[0,1].grid()
axs[0,2].grid()
axs[1,0].grid()
axs[1,1].grid()
axs[1,2].grid()

# axis legend #
# axs[0,0].legend(fontsize=12)

# figure title #
fig.suptitle('QHS')

# save/show figure #
# plt.show()
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'personal_meetings', 'MJ', '20230816', 'figures', 'weight_qhs.png')
plt.savefig(save_path)
