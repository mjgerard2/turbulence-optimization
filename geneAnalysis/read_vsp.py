import os, sys
import numpy as np
import h5py as hf
import matplotlib as mpl

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import plot_define as pd
import gistTools.gist_reader as rgist

class vsp_reader:
    """ A class for reading GENE vsp data from an hdf5 file.

    ...

    Attributes
    ----------
    vsp_path : str
        Global path to hdf5 vsp file.

    Methods
    -------
    """
    def __init__(self, vsp_path):
        # check if path exists #
        if not os.path.isfile(vsp_path):
            raise IOError('Oops! '+vsp_path+' does not exist :(')

        # read in data #
        with hf.File(vsp_path, 'r') as hf_:
            # read in space and time coordinates #
            self.z = hf_['z'][()]
            self.time = hf_['time'][()]
            self.nz = self.z.shape[0]
            self.nt = self.time.shape[0]
            
            # read in velocity-space coordinates #
            self.vpar = hf_['vpar'][()]
            self.mu = hf_['mu'][()]
            self.vpar_num = self.vpar.shape[0]
            self.mu_num = self.mu.shape[0]

            # read in velocity-space distributions and trapping boundaries #
            self.trap_dict, self.vel_dist = {}, {}
            for key in hf_['velocity-space distributions']:
                self.trap_dict[key] = hf_['trapping boundaries/'+key][()]
                self.vel_dist[key] = hf_['velocity-space distributions/'+key][()]

    def time_average_distribution(self, z_values, time_interval):
        """ Compute the time-averaged distribution over the
        specified z coordinates and over the specified time
        domain.

        Parameters
        ----------
        z_values : array/list
            Array of z coordinates where averaging is performed.
        time_interval : tuple
            Initial and final time over which averaging is performed.
        """
        # extract time-domain array #
        time_index = np.where(self.time <= time_interval[1])[0]
        time_index = np.where(self.time[time_index] >= time_interval[0])[0]
        time_domain = self.time[time_index]
        Dt_inv = 1./(time_domain[-1]-time_domain[0])

        # compute averages #
        self.vel_dist_avg = np.empty((len(z_values), self.vpar_num, self.mu_num))
        for z_idx, z_val in enumerate(z_values):
            z_key = 'z={}'.format(self.z[np.argmin(np.abs(self.z-z_val))])
            dist = self.vel_dist[z_key][time_index]
            self.vel_dist_avg[z_idx] = Dt_inv * np.trapz(dist, time_domain, axis=0)

        # return data #
        return self.vel_dist_avg


if __name__ == '__main__':
    from matplotlib.ticker import MultipleLocator

    # define vsp paths #
    vsp_path_qhs = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'vsp1e_4_14.h5')
    vsp_path_he = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'vsp1e_3_11.h5')

    # define gist paths #
    gist_path_qhs = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'gist_4')
    gist_path_he = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'gist_3')

    # read in data #
    vsp_qhs = vsp_reader(vsp_path_qhs)
    vsp_he = vsp_reader(vsp_path_he)

    # compute time averages #
    z_qhs = [0]
    z_he = [0]
    dist_avg1_qhs = vsp_qhs.time_average_distribution(z_qhs, [350, 1500])
    vsp_qhs.normalize_distribution(gist_path_qhs)
    dist_avg2_qhs = vsp_qhs.time_average_distribution(z_qhs, [2400, vsp_qhs.time[-1]])
    vsp_qhs.normalize_distribution(gist_path_qhs)
    dist_avg1_he = vsp_he.time_average_distribution(z_he, [350, 1500])
    vsp_he.normalize_distribution(gist_path_he)
    dist_avg2_he = vsp_he.time_average_distribution(z_he, [2400, vsp_he.time[-1]])
    vsp_he.normalize_distribution(gist_path_he)
