import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


config_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'config_list.txt')
met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data.h5')
scan_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files')

scan_name_list = ['omega_data_{}_x.h5'.format(int(i)) for i in [4, 7, 8]]
num_of_grads = len(scan_name_list)

# save_path = None
save_path = os.path.join('/mnt', 'GENE', 'linear_data', 'figures', 'omte_scan.png')

# Read in Cofiguration List #
with open(config_path, 'r') as f:
    lines = f.readlines()
    config_list = []
    for line in lines:
        config_list.append(line.strip())

num_of_configs = len(config_list)

# Read in Metric Data #
with hf.File(met_path, 'r') as hf_:
    met_arr = hf_['metric data'][()]
    met_data = np.empty((num_of_configs, met_arr.shape[1]))
    for cdx, config in enumerate(config_list):
        config_arr = np.array([float(i) for i in config.split('-')])
        sum_abs = np.sum(np.abs(met_arr[:, 0:3] - config_arr), axis=1)
        idx = np.argmin(sum_abs)
        if sum_abs[idx] == 0:
            met_data[cdx] = met_arr[idx]

# Read in GENE Data #
gene_data = np.empty((num_of_configs, num_of_grads, 4, 3))
for sdx, scan_name in enumerate(scan_name_list):
    scan_file = os.path.join(scan_path, scan_name)
    with hf.File(scan_file, 'r') as hf_:
        for cdx, config in enumerate(config_list):
            gene_data[cdx, sdx] = hf_[config][0:4, :]

plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

fig, axs = plt.subplots(2, 3, sharex=True, sharey=False, tight_layout=True, figsize=(20, 12))

ax1 = axs[0, 0]
ax2 = axs[0, 1]
ax3 = axs[0, 2]

ax4 = axs[1, 0]
ax5 = axs[1, 1]
ax6 = axs[1, 2]

# First Gradient #
ax1.scatter(met_data[:, 9], gene_data[:, 0, 0, 1], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label='0.1')
ax1.scatter(met_data[:, 9], gene_data[:, 0, 1, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label='0.4')
ax1.scatter(met_data[:, 9], gene_data[:, 0, 2, 1], marker='s', s=150, facecolor='None', edgecolor='tab:green', label='0.7')
ax1.scatter(met_data[:, 9], gene_data[:, 0, 3, 1], marker='^', s=150, facecolor='None', edgecolor='tab:red', label='1.0')

ax4.scatter(met_data[:, 9], gene_data[:, 0, 0, 2], marker='v', s=150, facecolor='None', edgecolor='tab:blue')
ax4.scatter(met_data[:, 9], gene_data[:, 0, 1, 2], marker='o', s=150, facecolor='None', edgecolor='tab:orange')
ax4.scatter(met_data[:, 9], gene_data[:, 0, 2, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green')
ax4.scatter(met_data[:, 9], gene_data[:, 0, 3, 2], marker='^', s=150, facecolor='None', edgecolor='tab:red')

# Second Gradient #
ax2.scatter(met_data[:, 9], gene_data[:, 1, 0, 1], marker='v', s=150, facecolor='None', edgecolor='tab:blue')
ax2.scatter(met_data[:, 9], gene_data[:, 1, 1, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange')
ax2.scatter(met_data[:, 9], gene_data[:, 1, 2, 1], marker='s', s=150, facecolor='None', edgecolor='tab:green')
ax2.scatter(met_data[:, 9], gene_data[:, 1, 3, 1], marker='^', s=150, facecolor='None', edgecolor='tab:red')

ax5.scatter(met_data[:, 9], gene_data[:, 1, 0, 2], marker='v', s=150, facecolor='None', edgecolor='tab:blue')
ax5.scatter(met_data[:, 9], gene_data[:, 1, 1, 2], marker='o', s=150, facecolor='None', edgecolor='tab:orange')
ax5.scatter(met_data[:, 9], gene_data[:, 1, 2, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green')
ax5.scatter(met_data[:, 9], gene_data[:, 1, 3, 2], marker='^', s=150, facecolor='None', edgecolor='tab:red')

# Third Gradient #
ax3.scatter(met_data[:, 9], gene_data[:, 2, 0, 1], marker='v', s=150, facecolor='None', edgecolor='tab:blue')
ax3.scatter(met_data[:, 9], gene_data[:, 2, 1, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange')
ax3.scatter(met_data[:, 9], gene_data[:, 2, 2, 1], marker='s', s=150, facecolor='None', edgecolor='tab:green')
ax3.scatter(met_data[:, 9], gene_data[:, 2, 3, 1], marker='^', s=150, facecolor='None', edgecolor='tab:red')

ax6.scatter(met_data[:, 9], gene_data[:, 2, 0, 2], marker='v', s=150, facecolor='None', edgecolor='tab:blue')
ax6.scatter(met_data[:, 9], gene_data[:, 2, 1, 2], marker='o', s=150, facecolor='None', edgecolor='tab:orange')
ax6.scatter(met_data[:, 9], gene_data[:, 2, 2, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green')
ax6.scatter(met_data[:, 9], gene_data[:, 2, 3, 2], marker='^', s=150, facecolor='None', edgecolor='tab:red')

# Axis Limits #
ax1.set_xlim(0.955, 1.08)

ax1.set_ylim(0, 1.05 * np.max(gene_data[:, :, 0:4, 1]))
ax2.set_ylim(0, 1.05 * np.max(gene_data[:, :, 0:4, 1]))
ax3.set_ylim(0, 1.05 * np.max(gene_data[:, :, 0:4, 1]))

ax4.set_ylim(1.05 * np.min(gene_data[:, :, 0:4, 2]), 0)
ax5.set_ylim(1.05 * np.min(gene_data[:, :, 0:4, 2]), 0)
ax6.set_ylim(1.05 * np.min(gene_data[:, :, 0:4, 2]), 0)

# Axis Labels #
ax4.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax5.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax6.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

ax1.set_ylabel(r'$\gamma \ (c_s/a)$')
ax4.set_ylabel(r'$\omega \ (c_s/a)$')

# Axis Title #
ax1.set_title(r'$\left( a/L_n,\, a/L_{Te},\, a/L_{Ti} \right) = \left( 2,\, 2,\, 1 \right)$')
ax2.set_title(r'$\left( a/L_n,\, a/L_{Te},\, a/L_{Ti} \right) = \left( 2,\, 3,\, 1 \right)$')
ax3.set_title(r'$\left( a/L_n,\, a/L_{Te},\, a/L_{Ti} \right) = \left( 2,\, 4,\, 1 \right)$')

# Axis Legend #
ax1.legend(loc='upper center', frameon=False, ncol=4, columnspacing=0.5, handletextpad=0., title=r'$k_y\rho_s$')

# Axis Grids #
ax1.grid()
ax2.grid()
ax3.grid()

ax4.grid()
ax5.grid()
ax6.grid()

# Save/Show Figure #
if save_path is None:
    plt.show()
else:
    plt.savefig(save_path)
