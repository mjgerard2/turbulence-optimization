import os
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator

import read_spectrum as rs

# User Input #
spec_path_1 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'fluxspectrae_1_3.dat')
spec_path_2 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'fluxspectrae_16.dat')
# spec_path_3 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'fluxspectrae_26_44.dat')

pram_path_1 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_1')
pram_path_2 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'parameters_16')
# pram_path_3 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'parameters_26')

# save_name = None
save_name = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'figures', 'JvM_spectrum.pdf')
tbeg = [200, 3000]
lbs = ['Jason', 'Michael']

spec_path_list = [spec_path_1, spec_path_2]
pram_path_list = [pram_path_1, pram_path_2]
###############################################################################

kx_spec_list = []
ky_spec_list = []

kx_max = []
ky_max = []

kx_amp_max = []
ky_amp_max = []

for i, (spec_path, pram_path, t0) in enumerate(zip(spec_path_list, pram_path_list, tbeg)):
    spec = rs.spectrum(spec_path, pram_path)
    if i == 0:
        t_end = spec.time_domain[-1]
        spec.spectrum_average(t0)
    else:
        spec.spectrum_average(t0)
    spec.spectrum_normalization()

    kx = spec.kx_domain
    ky = spec.ky_domain

    kx_norm = 1./spec.spec_norms['kx']['Q_es']
    ky_norm = 1./spec.spec_norms['ky']['Q_es']

    kx_amp = spec.spec_data_avg['kx']['Q_es']# *kx_norm
    ky_amp = spec.spec_data_avg['ky']['Q_es']# *ky_norm

    kx_max.append(spec.kx_max)
    ky_max.append(spec.ky_max)

    kx_amp_max.append(np.max(kx_amp))
    ky_amp_max.append(np.max(ky_amp))

    kx_spec_list.append(np.stack((kx, kx_amp), axis=0))
    ky_spec_list.append(np.stack((ky, ky_amp), axis=0))

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axis #
fig, axs = plt.subplots(1, 2, sharex=False, sharey=False, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = axs[1]

# Plot data #
for i, ky_spec in enumerate(ky_spec_list):
    ax1.plot(ky_spec[0], ky_spec[1], label=lbs[i])

for i, kx_spec in enumerate(kx_spec_list):
    ax2.plot(kx_spec[0], kx_spec[1], label=lbs[i])

# Axis labels #
ax1.set_ylabel(r'$\langle \hat{Q}_{\mathrm{es}} \rangle$')
ax1.set_xlabel(r'$k_y \rho_s$')
ax2.set_xlabel(r'$k_x \rho_s$')

# Axis limits #
ax1.set_xlim(0, max(ky_max))
ax1.set_ylim(0, 1.05*max(ky_amp_max))

ax2.set_xlim(0, max(kx_max))
ax2.set_ylim(0, 1.05*max(kx_amp_max))

# Axis legends #
ax1.legend(frameon=False)
ax2.legend(frameon=False)

# Axis grid #
# ax1.grid()
# ax2.grid()

for ax in axs.flat:
    # axis ticks #
    ax.tick_params(axis='both', which='both', direction='in')
    ax.xaxis.set_ticks_position('default')
    ax.yaxis.set_ticks_position('default')
ax1.xaxis.set_minor_locator(MultipleLocator(0.1))
ax1.yaxis.set_minor_locator(MultipleLocator(0.005))
ax2.xaxis.set_minor_locator(MultipleLocator(0.25))
ax2.yaxis.set_minor_locator(MultipleLocator(0.01))

# Save or Show Plot #
if save_name:
    plt.savefig(save_name, format='pdf')
else:
    plt.show()
