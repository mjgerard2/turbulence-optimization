import os
import numpy as np

import sys
WORKDIR = os.path.dirname(os.getcwd())
sys.path.append(WORKDIR)

import gistTools.gist_reader as gr


# Import gist data #
path_to_gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_60', 'set_1', 'job_84', 'gist_60-1-84_mn1824_alf0_s0p49_res64_npol4.dat')
gist = gr.read_gist(path_to_gist_file)

jacob = gist.data['Jacob']
jacob_str = '  z_jacobian = [%s]\n' % ','.join(['{}'.format(x) for x in jacob])

# Read stell_NLproj.pro.bak #
temp_path = os.path.join(os.getcwd(), 'stell_scalpr.pro.bak')
with open(temp_path, 'r') as f:
    lines = f.readlines()
    lines[83] = jacob_str

# Write stell_NLproj.pro #
new_path = os.path.join(os.getcwd(), 'stell_scalpr.pro')
with open(new_path, 'w') as f:
    for line in lines:
        f.write(line)

