import os
import h5py as hf
import numpy as np

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd
import geneAnalysis.read_nrg_summary as rnrg


# import AE data #
ae_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', 'ae-data.h5')
with hf.File(ae_path, 'r') as hf_:
    ae_data = hf_['ae data'][()]
    grad_scan = hf_['omn values (axis=2)'][()]

# import qhs gradient scan #
hf_qhs = np.empty(3)

nrg_path_1 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_act.h5')
nrg_path_2 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_00_06.h5')
nrg_path_3 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p5_omti_0p0_omte_2p0', 'nrgsummarye_26_44.h5')
    
nrg1 = rnrg.nrg_summary(nrg_path_1)
nrg2 = rnrg.nrg_summary(nrg_path_2)
nrg3 = rnrg.nrg_summary(nrg_path_3)

nrg1.nrg_average(600)
nrg2.nrg_average(2400)
nrg3.nrg_average(850)

hf_qhs[0] = nrg1.time_average['Q_tot']
hf_qhs[1] = nrg2.time_average['Q_tot']
hf_qhs[2] = nrg3.time_average['Q_tot']

# import 60-1-84 gradient scan #
hf_60 = np.empty(3)

nrg_path_1 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_1p5_omti_0p0_omte_2p0', 'nrgsummarye_1_2.h5')
nrg_path_2 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_2.h5')
nrg_path_3 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '60-1-84', 'omn_2p5_omti_0p0_omte_2p0', 'nrgsummarye_6_12.h5')
    
nrg1 = rnrg.nrg_summary(nrg_path_1)
nrg2 = rnrg.nrg_summary(nrg_path_2)
nrg3 = rnrg.nrg_summary(nrg_path_3)

nrg1.nrg_average(250)
nrg2.nrg_average(250)
nrg3.nrg_average(800)

hf_60[0] = nrg1.time_average['Q_tot']
hf_60[1] = nrg2.time_average['Q_tot']
hf_60[2] = nrg3.time_average['Q_tot']

# import 897-1-0 gradient scan #
# hf_897 = np.empty(3)
# 
# nrg_path_2 = os.path.join('/mnt', 'GENE', 'nonlinear_data', '897-1-0', 'omn_2p0_omti_0p0_omte_2p0', 'nrgsummarye_4_7.h5')
# nrg2 = rnrg.nrg_summary(nrg_path_2)
# nrg2.nrg_average(600)
# 
# hf_897[0] = np.nan
# hf_897[1] = nrg2.time_average['Q_tot']
# hf_897[2] = np.nan

# plot data #
plot = pd.plot_define(fontSize=14, labelSize=16, lineWidth=2)
plt = plot.plt
fig, axs = plt.subplots(1, 1, sharex=True, sharey=False, tight_layout=True, figsize=(6, 5))

ax1 = axs
# ax1 = axs[0]
# ax2 = axs[1]

plt1, = ax1.plot(grad_scan, hf_qhs, ls='--', marker='o', ms=15, mfc='None', label='QHS')
plt2, = ax1.plot(grad_scan, hf_60, ls='--', marker='s', ms=15, mfc='None', label='HE-QHS')
# plt3, = ax1.plot(grad_scan, hf_897, ls='--', marker='v', ms=15, mfc='None', label='HE-QHS')

# ax2.plot(grad_scan, ae_data[0], c=plt1.get_color(), ls='--', marker='o', ms=15, mfc='None')
# ax2.plot(grad_scan, ae_data[1], c=plt2.get_color(), ls='--', marker='s', ms=15, mfc='None')
# ax2.plot(grad_scan, ae_data[2], c=plt3.get_color(), ls='--', marker='v', ms=15, mfc='None')

ax1.set_ylabel(r'$Q_{\mathrm{e}}^{\mathrm{es}} \ / \ (c_{\mathrm{s}}n_{\mathrm{e}}T_{\mathrm{e}}(\rho_{\mathrm{s}}/a)^2)$')
ax1.set_xlabel(r'$a/L_{\mathrm{n}}$')
# ax2.set_ylabel(r'$\hat{A}$')
# ax2.set_xlabel(r'$a/L_{\mathrm{n}}$')

ax1.set_ylim(0, ax1.get_ylim()[1])
# ax2.set_ylim(0, ax2.get_ylim()[1])
ax1.set_xticks([1.5, 1.75, 2.0, 2.25, 2.5])

ax1.grid()
# ax2.grid()
ax1.legend(frameon=False)

# plt.show()
# save_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', 'figures', 'heat_flux_aLn_scan.png')
save_path = os.path.join('/home', 'michael', 'onedrive', 'Proposals', 'ACCESS', '2023_Explore', 'heat_flux.png')
plt.savefig(save_path)
