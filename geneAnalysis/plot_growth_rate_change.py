import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


config_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'config_list.txt')
met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data.h5')
scan_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files')

scan_name_list = ['omega_data_{}_x.h5'.format(int(i)) for i in [4, 7, 8]]
num_of_chngs = len(scan_name_list)-1

# save_path = None
save_path = os.path.join('/mnt', 'GENE', 'linear_data', 'figures', 'omte_change.png')

# Read in Cofiguration List #
with open(config_path, 'r') as f:
    lines = f.readlines()
    config_list = []
    for line in lines:
        config_list.append(line.strip())

num_of_configs = len(config_list)

# Read in Metric Data #
with hf.File(met_path, 'r') as hf_:
    met_arr = hf_['metric data'][()]
    met_data = np.empty((num_of_configs, met_arr.shape[1]))
    for cdx, config in enumerate(config_list):
        config_arr = np.array([float(i) for i in config.split('-')])
        sum_abs = np.sum(np.abs(met_arr[:, 0:3] - config_arr), axis=1)
        idx = np.argmin(sum_abs)
        if sum_abs[idx] == 0:
            met_data[cdx] = met_arr[idx]

# Read in GENE Data #
gamma_base = np.empty((num_of_configs, 4))
gamma_chng = np.empty((num_of_configs, num_of_chngs, 4))
for sdx, scan_name in enumerate(scan_name_list):
    scan_file = os.path.join(scan_path, scan_name)
    if sdx == 0:
        with hf.File(scan_file, 'r') as hf_:
            for cdx, config in enumerate(config_list):
                gamma_base[cdx] = hf_[config][0:4, 1]
    else:
        with hf.File(scan_file, 'r') as hf_:
            for cdx, config in enumerate(config_list):
                gamma_chng[cdx, sdx-1] = hf_[config][0:4, 1] - gamma_base[cdx]

# Define Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Define Plotting Axes #
fig, axs = plt.subplots(1, 2, sharex=True, sharey=True, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = axs[1]

# First Change #
ax1.scatter(met_data[:, 9], gamma_chng[:, 0, 0], marker='v', s=150, facecolor='None', edgecolor='tab:blue', label='0.1')
ax1.scatter(met_data[:, 9], gamma_chng[:, 0, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange', label='0.4')
ax1.scatter(met_data[:, 9], gamma_chng[:, 0, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green', label='0.7')
ax1.scatter(met_data[:, 9], gamma_chng[:, 0, 3], marker='^', s=150, facecolor='None', edgecolor='tab:red', label='1.0')

# Second Change #
ax2.scatter(met_data[:, 9], gamma_chng[:, 1, 0], marker='v', s=150, facecolor='None', edgecolor='tab:blue')
ax2.scatter(met_data[:, 9], gamma_chng[:, 1, 1], marker='o', s=150, facecolor='None', edgecolor='tab:orange')
ax2.scatter(met_data[:, 9], gamma_chng[:, 1, 2], marker='s', s=150, facecolor='None', edgecolor='tab:green')
ax2.scatter(met_data[:, 9], gamma_chng[:, 1, 3], marker='^', s=150, facecolor='None', edgecolor='tab:red')

# Axis Limits #
ax1.set_xlim(0.955, 1.08)
ax1.set_ylim(1.05 * np.min(gamma_chng), 1.05 * np.max(gamma_chng))

# Axis Labels #
ax1.set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
ax1.set_ylabel(r'$\Delta \gamma \ (c_s/a)$')

# Axis Title #
ax1.set_title(r'$\Delta a/L_{Te} = 1$')
ax2.set_title(r'$\Delta a/L_{Te} = 2$')

# Axis Legend #
ax1.legend(loc='upper center', frameon=False, ncol=4, columnspacing=0.5, handletextpad=0., title=r'$k_y\rho_s$')

# Axis Grids #
ax1.grid()
ax2.grid()

# Save/Show Figure #
if save_path is None:
    plt.show()
else:
    plt.savefig(save_path)
