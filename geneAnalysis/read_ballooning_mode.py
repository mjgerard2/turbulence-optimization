import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as ani

import sys
WORKDIR = os.path.dirname(os.getcwd())
sys.path.append(WORKDIR)

import plot_define as pd
from gistTools import gist_reader as gr


def isfloat(var):
    try:
        float(var)
        is_float = True
    except ValueError:
        is_float = False

    return is_float


class ballooning:
    """ A class for reading GENE ballooning modes.

    ...

    Attributes
    ----------
    ball_path : str
        Global path to contour file.

    Methods
    -------
    mean_nrg(start_time, end_time=-1)
        Calculate time average of nonlinear quantities.
    """
    def __init__(self, ball_path):
        # Check if input files exsit #
        if not os.path.isfile(ball_path):
            raise IOError(ball_path+' does not exist.')

        # Read in ballooning data #
        with open(ball_path, 'r') as f:
            lines = f.readlines()
            for ldx, line in enumerate(lines):
                check = isfloat(line.strip().split()[0])
                if check:
                    break

            self.ball_data = np.empty((len(lines[ldx::]), 4))
            for i, line in enumerate(lines[ldx::]):
                value_list = np.array([float(x) for x in line.strip().split()])
                self.ball_data[i] = value_list

if __name__ == '__main__':
    if True:
        ball_path = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'ky_0p2', 'balle_10_1.dat')
        ball = ballooning(ball_path)

        plot = pd.plot_define(lineWidth=2)
        plt = plot.plt
        fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(12, 6))

        ball_amp = np.hypot(ball.ball_data[:, 1], ball.ball_data[:, 2])
        ax.plot(ball.ball_data[:, 0], ball_amp, c='tab:blue')

        # Axis Limits #
        ax.set_xlim(ball.ball_data[0, 0], ball.ball_data[-1, 0])
        ax.set_ylim(0, 1.05*np.max(ball_amp))

        # Axis Labels #
        ax.set_xlabel(r'$\theta/\pi$')
        ax.set_ylabel(r'$|\phi|$')

        # plt.show()
        save_path = os.path.join('/home', 'michael', 'Desktop', 'Prelim_Defense', 'figures', 'eigenvector_ion.png')
        plt.savefig(save_path)

    if False:
        # Import Shear #
        config_ids = ['QHS', '60-1-84', '897-1-0']

        shear = np.empty(len(config_ids))
        for i, config_id in enumerate(config_ids):
            if config_id == 'QHS':
                gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'gist_QHS_best_alf0_s0p5_res128_npol4.dat')
                gist = gr.read_gist(gist_file)
                shear[i] = gist.shat

            else:
                main_id = 'main_coil_{}'.format(config_id.split('-')[0])
                set_id = 'set_{}'.format(config_id.split('-')[1])
                job_id = 'job_{}'.format(config_id.split('-')[2])

                gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'gist_HSX_'+config_id+'_s0p5_res1024_npol1_alpha0p00.dat')
                gist = gr.read_gist(gist_file)
                shear[i] = gist.shat


        # Read in Ballooning Data #
        ball_path_qhs = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0_jason', 'eigenvalue', 'ky_0p2', 'balle_1_1.dat')
        ball_qhs = ballooning(ball_path_qhs)

        ball_path_60 = os.path.join('/mnt', 'GENE', 'linear_data', '60-1-84', 'eigenvalue', 'ky_0p2', 'balle_1_2.dat')
        ball_60 = ballooning(ball_path_60)

        ball_path_897 = os.path.join('/mnt', 'GENE', 'linear_data', '897-1-0', 'eigenvalue', 'ky_0p2', 'balle_1_1.dat')
        ball_897 = ballooning(ball_path_897)

        plot = pd.plot_define(lineWidth=2)
        plt = plot.plt
        fig, axs = plt.subplots(3, 1, sharex=True, tight_layout=True, figsize=(10, 10))

        ax1 = axs[0]
        ax2 = axs[1]
        ax3 = axs[2]

        ball_amp_qhs = np.hypot(ball_qhs.ball_data[:, 1], ball_qhs.ball_data[:, 2])
        ax1.plot(ball_qhs.ball_data[:, 0], ball_amp_qhs, c='tab:blue')

        ball_amp_60 = np.hypot(ball_60.ball_data[:, 1], ball_60.ball_data[:, 2])
        ax2.plot(ball_60.ball_data[:, 0], ball_amp_60, c='tab:orange')

        ball_amp_897 = np.hypot(ball_897.ball_data[:, 1], ball_897.ball_data[:, 2])
        ax3.plot(ball_897.ball_data[:, 0], ball_amp_897, c='tab:green')

        # Axis Limits #
        # ax1.set_xlim(ball_qhs.ball_data[0, 0], ball_qhs.ball_data[-1, 0])
        ax1.set_ylim(0, 1.05*np.max(ball_amp_qhs))

        ax2.set_xlim(ball_60.ball_data[0, 0], ball_60.ball_data[-1, 0])
        ax2.set_ylim(0, 1.05*np.max(ball_amp_60))

        # ax3.set_xlim(ball_897.ball_data[0, 0], ball_897.ball_data[-1, 0])
        ax3.set_ylim(0, 1.05*np.max(ball_amp_897))

        # Axis Labels #
        ax3.set_xlabel(r'$\theta/\pi$')
        ax3.set_ylabel(r'$|\phi|$')
        ax2.set_ylabel(r'$|\phi|$')
        ax1.set_ylabel(r'$|\phi|$')

        # Axis Titles #
        ax1.set_title('QHS $(\hat{{s}} = {0:0.2f})$'.format(shear[0]))
        ax2.set_title('60-1-84 $(\hat{{s}} = {0:0.2f})$'.format(shear[1]))
        ax3.set_title('897-1-0 $(\hat{{s}} = {0:0.2f})$'.format(shear[2]))

        # plt.show()
        save_path = os.path.join('/home', 'michael', 'Desktop', 'Prelim_Defense', 'figures', 'eigenvectors_shear.png')
        plt.savefig(save_path)
