import os
import h5py as hf
import numpy as np

import flf_class
import databaseTools.functions as fun

import sys
WORKDIR = os.path.dirname(os.getcwd())
sys.path.append(WORKDIR)

import plot_define as pd
import vmecTools.wout_files.wout_read as wr


config_id = '897-1-0'

# Import wout File #
main_id = 'main_coil_{}'.format(config_id.split('-')[0])
set_id = 'set_{}'.format(config_id.split('-')[1])
job_id = 'job_{}'.format(config_id.split('-')[2])

wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
wout_name_1 = 'wout_HSX_'+config_id+'.nc'
wout_name_2 = 'wout_HSX_main_opt0.nc'

wout1 = wr.readWout(wout_path, name=wout_name_1)
wout2 = wr.readWout(wout_path, name=wout_name_2)

# Define Coil Currents #
main_crnt, aux_crnt = fun.readCrntConfig(config_id)

crnt = -10722. * np.r_[main_crnt, 14*aux_crnt]
crnt_str = ' '.join(['{}'.format(c) for c in crnt])

# Calculate Surfaces #
nsurf = 10
upnts = 500

psi_dom = np.linspace(wout1.s_grid[0], wout1.s_grid[-1], nsurf)
pol_dom = np.linspace(-np.pi, np.pi, upnts)
tor_ang = .25*np.pi

wout1.transForm_2D_vSec(psi_dom, pol_dom, tor_ang, ['R', 'Z'])
wout2.transForm_2D_vSec(psi_dom, pol_dom, tor_ang, ['R', 'Z'])

R_surf1 = wout1.invFourAmps['R']
Z_surf1 = wout1.invFourAmps['Z']

R_surf2 = wout2.invFourAmps['R']
Z_surf2 = wout2.invFourAmps['Z']

# Get FLF Data #
rots = 500
dphi = 0.125*np.pi
stps = int(2*np.pi/dphi)
npts = int(rots*stps)

mod_dict = {'points_dphi': dphi,
            'n_iter': npts,
            'mgrid_currents': crnt_str}

flf = flf_class.flf_wrapper('HSX')
flf.change_params(mod_dict)

flf_pnts1 = np.empty((nsurf, rots-1, 4))
flf_pnts2 = np.empty((nsurf, rots-1, 4))
for i in range(nsurf):
    print('\nFLF: ({}|{})'.format(i+1, nsurf))
    init_point = np.array([R_surf1[i, 0], Z_surf1[i, 0], tor_ang])
    flf_pnts1[i] = flf.read_out_point(init_point, return_poin_data=True)

    init_point = np.array([R_surf2[i, 0], Z_surf2[i, 0], tor_ang])
    flf_pnts2[i] = flf.read_out_point(init_point, return_poin_data=True)

# Plot Surfaces #
plot = pd.plot_define(lineWidth=1)

plt = plot.plt
fig, axs = plt.subplots(1, 2, sharex=True, sharey=True, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = axs[1]

ax1.set_aspect('equal')
ax2.set_aspect('equal')

ax1.scatter(R_surf1[0, 0], Z_surf1[0, 0], marker='+', s=50, c='k')
ax2.scatter(R_surf2[0, 0], Z_surf2[0, 0], marker='+', s=50, c='k')

for i in range(1, nsurf):
    ax1.plot(R_surf1[i, :], Z_surf1[i, :], c='tab:red', ls='-')
    ax1.scatter(flf_pnts1[i, :, 0], flf_pnts1[i, :, 1], c='k', s=1)

    ax2.plot(R_surf2[i, :], Z_surf2[i, :], c='tab:red', ls='-')
    ax2.scatter(flf_pnts2[i, :, 0], flf_pnts2[i, :, 1], c='k', s=1)

    if i == nsurf-1:
        ax1.scatter(flf_pnts1[i, :, 0], flf_pnts1[i, :, 1], c='k', s=1, label='FLF')
        ax1.plot(R_surf1[i, :], Z_surf1[i, :], c='tab:red', ls='-', label='VMEC')

        ax2.scatter(flf_pnts2[i, :, 0], flf_pnts2[i, :, 1], c='k', s=1, label='FLF')
        ax2.plot(R_surf2[i, :], Z_surf2[i, :], c='tab:red', ls='-', label='VMEC')

# Axis Labels #
ax1.set_xlabel(r'R (m)')
ax2.set_xlabel(r'R (m)')
ax1.set_ylabel(r'Z (m)')

# Axis Titles #
ax1.set_title('(n, m) = (18, 24)')
ax2.set_title('(n, m) = (8, 8)')

# Axis Legends #
ax1.legend(frameon=False, loc='lower right', fontsize=18)
ax2.legend(frameon=False, loc='lower right', fontsize=18)

# Save/Show #
plt.show()
save_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'figures', 'flf_wout_comparison.png')
plt.savefig(save_path)
