# -*- coding: utf-8 -*-
"""
Created on Mon Jun 22 23:29:14 2020

@author: micha
"""

import numpy as np
import flf_class

import os, sys
path = os.path.dirname(os.path.abspath(os.getcwd()))
sys.path.append(path)

import plot_define

### Current profile ###
mod_crnt = 0.
crnt = 23522 * np.array([1, -.25, -.08, -.08, -.08, -.08, mod_crnt, mod_crnt, mod_crnt, mod_crnt])
crnt_str = ', '.join(['{}'.format(c) for c in crnt])

### flf Parameters ###
tag = 'BaseCase'
init_point = np.array([.3, 0, 0.25*np.pi])

dstp=60
rots=100

dphi = dstp * (np.pi/180)
stps = int(2 * np.pi / dphi)
npts = int(rots*stps)

chg_dict = {'points_dphi' : dphi,
            'n_iter' : npts,
            'mgrid_file' : 'itos_coils/tolerance_test/'+tag+'/mgrid_itos_'+tag+'.nc',
            'mgrid_currents' : crnt_str}

### Instantiate flf object and find MagAxis and LCFS ###
flf = flf_class.flf_wrapper('ITOS')
flf.change_params(chg_dict)

ma_pnt, lcfs_pnt = flf.find_boundaries(init_point, 4)
init_point = ma_pnt + 0.7 * (lcfs_pnt - ma_pnt)

### Calculate error of Fourier fit for set of toroidal coil currents ###
npts = 101

err = np.empty(npts)
mod_crnts = np.linspace(-0.01, 0.01, npts)
for m, mod in enumerate(mod_crnts):
    print('\nWorking : {} of {}'.format(m+1, npts))
    crnt = 23522 * np.array([1, -.25, -.08, -.08, -.08, -.08, mod, mod, mod, mod])
    crnt_str = ', '.join(['{}'.format(c) for c in crnt])
    
    chg_dict = {'mgrid_currents' : crnt_str}
    
    flf.change_params(chg_dict)
    fit_pnts, err[m] = flf.fit_surf(init_point, r_modes=5, z_modes=5)

### Plot errof of fits over currents ###
plot = plot_define.plot_define(fontSize=20, labelSize=24, lineWidth=2)
plt = plot.plt

plt.plot(mod_crnts, err, c='k')

plt.yscale('log')

plt.xlabel('Toroidal Currents (23.522 kA)')
plt.ylabel(r'$\chi^2$ of Fourier fit')

plt.grid()
plt.show()
plt.close()
