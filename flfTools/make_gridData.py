# -*- coding: utf-8 -*-
"""
Created on Thu May 21 09:40:44 2020

@author: micha
"""

import h5py as h5
import numpy as np

import flf_class

import os, sys
path = os.path.dirname(os.path.abspath(os.getcwd()))
sys.path.append(path)

from vmecTools.wout_files import wout_read

### Define Current ###
#main_crnt = np.full(6, 1.)
#aux_crnt = np.zeros(6)
#crnt = -10722 * np.r_[main_crnt, 14*aux_crnt]
crnt = np.array([1000, -250, -80, -80, -80, -80, 50, 50, 50, 50])
exp = 'ITOS'

### Grid Domain ###
rlim = [0, .5]
plim = [0, 2*np.pi]
tlim = np.pi * np.array([-.25, .25])

rspc = 0.001 # meters
pspc = .5 * (np.pi / 180) # degrees
tspc = 22.5 * (np.pi / 180) # degrees

rpts = int( (rlim[1]-rlim[0]) / rspc ) + 1
ppts = int( (plim[1]-plim[0]) / pspc ) + 1
tpts = int( (tlim[1]-tlim[0]) / tspc ) + 1
Npts = rpts*ppts*tpts

r_dom = np.linspace(rlim[0], rlim[1], rpts)
p_dom = np.linspace(plim[0], plim[1], ppts)
t_dom = np.linspace(tlim[0], tlim[1], tpts)

### Import magnetic axis ###
'''
path = os.path.join('C:\\', 'Users', 'micha', 'Documents', 'Research', 'Turbulence_Optimization', 'dataVMEC', 'HSX', 'coil_configs', 'main_coil_0', 'set_1', 'job_0')
wout = wout_read.readWout(path)
wout.transForm_2D_sSec(0, np.array([0]), t_dom, ['R', 'Z'])

R_ma = wout.invFourAmps['R']
Z_ma = wout.invFourAmps['Z']

vec_ma = np.stack((R_ma[0,0::,0], Z_ma[0,0::,0]), axis=1)
'''
vec_ma = np.empty((t_dom.shape[0], 2))
vec_ma[:] = np.array([0.625, 0.])

### Make initial points ###
init_points = np.empty((Npts, 3))
coord_grid = np.empty((rpts, ppts, tpts, 3))
for i, r in enumerate(r_dom):
    for j, p in enumerate(p_dom):
        R_ext = r * np.cos(p)
        Z_ext = r * np.sin(p)
        for k, t in enumerate(t_dom):
            idx = i * ppts * tpts + j * tpts + k       
            R = R_ext + vec_ma[k][0]
            Z = Z_ext + vec_ma[k][1]
            
            init_points[idx] = [R,Z,t]
            coord_grid[i,j,k] = [R,Z,t]
            
### Instantiate/Execute flf ###
flf = flf_class.flf_wrapper(crnt, exp=exp)

chg_params = {'general_option' : 2,
              'points_number' : Npts}
flf.change_params(chg_params)
points = flf.execute_flf(init_points)

### Orgainze flf data into Grid ###
B_grid = np.empty((rpts, ppts, tpts, 4))
for i, r in enumerate(r_dom):
    for j, p in enumerate(p_dom):
        for k, t in enumerate(t_dom):
            idx = i * ppts * tpts + j * tpts + k            
            B_grid[i,j,k] = points[idx]
            
### Save Grid Data ###
hf = h5.File(exp+'_GridData.h5', 'w')
hf.create_dataset('B Grid', data=B_grid)
hf.create_dataset('Coord Grid', data=coord_grid)
hf.close()