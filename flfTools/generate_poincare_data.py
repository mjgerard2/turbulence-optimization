import os
import numpy as np

import flf_class
import databaseTools.functions as fun

import sys
WORKDIR = os.path.dirname(os.getcwd())
sys.path.append(WORKDIR)

import plot_define as pd


# Define Coil Currents #
config_id = '897-1-0'
main_crnt, aux_crnt = fun.readCrntConfig(config_id)

crnt = -10722. * np.r_[main_crnt, 14*aux_crnt]
crnt_str = ' '.join(['{}'.format(c) for c in crnt])

# Construct FLF Object #
rots = 500
dphi = 0.125*np.pi
stps = int(2*np.pi/dphi)
npts = int(rots*stps)

mod_dict = {'points_dphi': dphi,
            'n_iter': npts,
            'mgrid_currents': crnt_str}

flf = flf_class.flf_wrapper('HSX')
flf.change_params(mod_dict)

init_pnt = np.array([1.51, 0, 0])
ma_pnt, lcfs_pnt = flf.find_boundaries(init_pnt, 6)

main_id = 'main_coil_{}'.format(config_id.split('-')[0])
set_id = 'set_{}'.format(config_id.split('-')[1])
job_id = 'job_{}'.format(config_id.split('-')[2])
poin_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'poincare_500pnts_50surf.h5')

flf.save_read_out_domain('core', ma_pnt, lcfs_pnt, 50, fileName=poin_path)
"""
# Plot Surfaces #
plot = pd.plot_define()

plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True)
ax.set_aspect('equal')

flf.read_out_domain(ma_pnt, lcfs_pnt, 10, ax=ax, return_data=False, DeBug=True)
pnt1 = np.array([1.5, 0, 0])
pnt2 = np.array([1.520169, 0, 0])
# flf.read_out_domain(pnt1, pnt2, 3, ax=ax, return_data=False, DeBug=True)

plt.show()
"""
