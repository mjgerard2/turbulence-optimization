import os
import subprocess

import h5py as hf
import numpy as np

from datetime import datetime

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import flfTools.flf_class as flfc

from databaseTools import functions
from flfTools.exp_params import params
from multiprocessing import Process, Queue, cpu_count

def calc_lyp_exp(in_que, out_que, run_dict):
    dx = run_dict['dx']
    d0 = run_dict['d0']

    tor_ang = run_dict['tor_ang']
    dstp = run_dict['dstp']
    rots = run_dict['rots']
    npts = run_dict['npts']

    run_cnt = run_dict['run_cnt']
    mod_dict = run_dict['mod_dict']
    Z_dom = run_dict['Z_dom']

    pid = os.getpid()
    file_dict = {'namelist': 'flf.namelist_%s' % pid,
                 'input': 'point_temp_%s.in' % pid,
                 'output': 'point_temp_%s.out' % pid,
                 'bash': 'run_flf_%s.sh' % pid}
    
    flf = flfc.flf_wrapper('HSX', file_dict=file_dict)
    flf.change_params(mod_dict)

    while True:
        item = in_que.get()
        if item is None:
            out_que.put(None)
            break
        else:
            run_idx, R_val = item
            lyp_exp = np.empty((Z_dom.shape[0], 2))
            for i, Z_val in enumerate(Z_dom):
                init_pnt = np.array([R_val, Z_val, tor_ang])
                flf.set_transit_parameters(dstp, rots)
                pnt0 = flf.execute_flf(init_pnt, quiet_mode=True)
                if np.isnan(pnt0[:,3]).any():
                    lyp_exp[i] = [np.nan, np.nan]
                else:
                    lyp_R = np.empty(npts)
                    lyp_Z = np.empty(npts)
                    flf.set_transit_parameters(dstp, dstp*(np.pi/360))
                    for j, init_pnt in enumerate(pnt0[0:-1]):
                        shift_R = init_pnt[0:3] + d0*np.array([1, 0, 0])
                        shift_Z = init_pnt[0:3] + d0*np.array([0, 1, 0])

                        pntR = flf.execute_flf(shift_R, quiet_mode=True)
                        pntZ = flf.execute_flf(shift_Z, quiet_mode=True)

                        lyp_R[j] = np.log(np.linalg.norm(pntR[1,0:2]-pnt0[j+1,0:2])/d0)
                        lyp_Z[j] = np.log(np.linalg.norm(pntZ[1,0:2]-pnt0[j+1,0:2])/d0)

                    lyp_exp[i] = [np.nanmean(lyp_R), np.nanmean(lyp_Z)]

            print('({0:0.0f}|{1:0.0f})'.format(run_idx+1, run_cnt))
            out_que.put([run_idx, lyp_exp])

    for key, name in file_dict.items():
        file_path = os.path.join('/home', 'michael', name)
        subprocess.run(['rm', file_path])

# Number of CPUs to use #
num_of_cpus = 8
print('Number of CPUs: {0:0.0f}'.format(num_of_cpus))

config_id = '0-1-0'
main, aux = functions.readCrntConfig(config_id)
crnt = -10722. * np.r_[main, 14*aux]
mod_dict = {'mgrid_currents': ' '.join(['{}'.format(c) for c in crnt]),
            'mgrid_file': os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'mgrid_res1p0cm_30pln.nc')}

date_tag = '20230803'  # datetime.now().strftime('%Y%m%d')
run_dict = {'dx': 1e-3,
            'd0': 1e-4,
            'tor_ang': 0,
            'mod_dict': mod_dict}

num_of_points = 100
nfp_divisions = np.array([2**3])  # np.array([2**4, 2**5, 2**6])
for div in nfp_divisions:
    dstp = 90./div
    rots = (num_of_points/(4*div)) + (dstp/360)
    npts = int(round((rots/dstp)*360)-1)

    run_dict['dstp'] = dstp
    run_dict['rots'] = rots
    run_dict['npts'] = npts

    # get search domain from vessel wall #
    ves_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'hsx_vessel_dieter_RF.txt')
    with open(ves_path, 'r') as f:
        lines = f.readlines()
        line_strip = lines[1].strip().split()
        tor_pnts = int(line_strip[0])
        pol_pnts = int(line_strip[1])
        nfp = int(line_strip[2])

        tor_dom = np.empty(tor_pnts)
        R_vess = np.empty((tor_pnts, pol_pnts))
        Z_vess = np.empty((tor_pnts, pol_pnts))
        for i in range(tor_pnts):
            tor_idx = 2 + i*(1+pol_pnts)
            beg_idx = tor_idx + 1
            end_idx = beg_idx + pol_pnts
            tor_dom[i] = float(lines[tor_idx].strip())*(np.pi/180)
            for j, line in enumerate(lines[beg_idx:end_idx]):
                line_strip = line.strip().split()
                R_vess[i,j] = float(line_strip[0])*1e-2
                Z_vess[i,j] = float(line_strip[1])*1e-2
    
    R_min, R_max = np.min(R_vess), np.max(R_vess)
    Z_min, Z_max = np.min(Z_vess), np.max(Z_vess)

    Rpts = int(((R_max-R_min)/run_dict['dx'])+1)
    Zpts = int(((Z_max-Z_min)/run_dict['dx'])+1)
    run_dict['run_cnt'] = Rpts
    print('R points: {}\nZ points: {}\n'.format(Rpts, Zpts))

    R_dom = np.linspace(R_min, R_max, Rpts)
    Z_dom = np.linspace(Z_min, Z_max, Zpts)
    run_dict['Z_dom'] = Z_dom

    # save run data #
    main_id = 'main_coil_{}'.format(config_id.split('-')[0])
    set_id = 'set_{}'.format(config_id.split('-')[1])
    job_id = 'job_{}'.format(config_id.split('-')[2])
    lyp_dir = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'lyaponov_data')
    lyp_nums = [int(f.name.split('.')[0].split('_')[1]) for f in os.scandir(lyp_dir) if f.name.split('_')[0] == date_tag]
    if len(lyp_nums) > 0:
        lyp_tag = '0002'  # str(max(lyp_nums)+1).zfill(4)
    else:
        lyp_tag = '0001'
    lyp_path = os.path.join(lyp_dir, '%s_%s.h5' % (date_tag, lyp_tag))

    if os.path.isfile(lyp_path):
        with hf.File(lyp_path, 'r') as hf_:
            lyp_exponent = hf_['lyaponov exponents'][()]
    else:
        lyp_exponent = np.full((Rpts, Zpts, 2), np.nan)
        with hf.File(lyp_path, 'w') as hf_:
            hf_.create_dataset('lyaponov exponents', data=lyp_exponent)
            hf_.create_dataset('R domain', data=R_dom)
            hf_.create_dataset('Z domain', data=Z_dom)
            hf_.create_dataset('tor_ang', data=run_dict['tor_ang'])
            hf_.create_dataset('dx', data=run_dict['dx'])
            hf_.create_dataset('d0', data=run_dict['d0'])
            hf_.create_dataset('dstp', data=run_dict['dstp'])
            hf_.create_dataset('rots', data=run_dict['rots'])
            hf_.create_dataset('npts', data=run_dict['npts'])

    # populate queue #
    in_que = Queue()
    out_que = Queue()
    for R_idx, R_val in enumerate(R_dom):
        if np.isnan(lyp_exponent[R_idx, :]).all():
            continue
        else:
            break

    R_idx_beg = R_idx
    for R_idx, R_val in enumerate(R_dom[R_idx_beg::]):
        if np.isnan(lyp_exponent[R_idx_beg+R_idx, :]).all():
            in_que.put([R_idx_beg+R_idx, R_val])

    for i in range(num_of_cpus):
        in_que.put(None)

    # start exponent calculation processes #
    for i in range(num_of_cpus):
        proc = Process(target=calc_lyp_exp, args=(in_que, out_que, run_dict))
        proc.start()

    # save exponents #
    done_cnt = 0
    while True:
        item = out_que.get()
        if item is None:
            done_cnt+=1
            print('End Process: {0:0.0f} of {1:0.0f}'.format(done_cnt, num_of_cpus))
            if done_cnt == num_of_cpus:
                break
        else:
            R_idx, lyp_exp = item
            with hf.File(lyp_path, 'a') as hf_:
                lyp_exponent = hf_['lyaponov exponents'][()]
                lyp_exponent[R_idx] = lyp_exp
                del hf_['lyaponov exponents']
                hf_.create_dataset('lyaponov exponents', data=lyp_exponent)
