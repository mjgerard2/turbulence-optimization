import os
import subprocess

import h5py as hf
import numpy as np

from sklearn.linear_model import LinearRegression

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import flfTools.flf_class as flfc

from databaseTools import functions
from flfTools.exp_params import params
from multiprocessing import Process, Queue, cpu_count

def calc_lyp_exp(in_que, out_que, run_dict):
    dx = run_dict['dx']
    tor_ang = run_dict['tor_ang']
    dstp = run_dict['dstp']
    rots = run_dict['rots']
    npts = run_dict['npts']
    d0 = run_dict['d0']
    run_cnt = run_dict['run_cnt']
    mod_dict = run_dict['mod_dict']
    Z_dom = run_dict['Z_dom']

    pid = os.getpid()
    file_dict = {'namelist': 'flf.namelist_%s' % pid,
                 'input': 'point_temp_%s.in' % pid,
                 'output': 'point_temp_%s.out' % pid,
                 'bash': 'run_flf_%s.sh' % pid}
    
    flf = flfc.flf_wrapper('HSX', file_dict=file_dict)
    flf.change_params(mod_dict)
    flf.set_transit_parameters(dstp, rots)

    pnt_ang = np.linspace(0, 2*np.pi, npts, endpoint=False)
    x_data = np.linspace(0, rots, flf.params['n_iter']).reshape((-1, 1))

    while True:
        item = in_que.get()
        if item is None:
            out_que.put(None)
            break
        else:
            run_idx, R_val = item
            lyp_exponent = np.empty(Z_dom.shape)
            for i, Z_val in enumerate(Z_dom):
                # lyp_exponent[i] = 0
                init_pnt = np.array([R_val, Z_val, tor_ang])
                pnt0 = flf.execute_flf(init_pnt, quiet_mode=True)
                if np.isnan(pnt0).any():
                    lyp_exponent[i] = np.nan
                else:
                    succ = True
                    lyp_exp = np.empty(npts)
                    for k in range(npts):
                        d_shft = d0*np.array([np.cos(pnt_ang[k]), np.sin(pnt_ang[k]), 0])
                        pnts = flf.execute_flf(init_pnt+d_shft, quiet_mode=True)
                        if np.isnan(pnts).any():
                            lyp_exponent[i] = np.nan
                            succ = False
                            break
                        else:
                            dist = np.linalg.norm(pnt0[:, 0:2] - pnts[:, 0:2], axis=1)/d0
                            zero_idx = np.where(dist == 0)[0]
                            dist[zero_idx] = 1e-10
                            y_data = np.log(dist)
                            model = LinearRegression().fit(x_data, y_data)
                            lyp_exp[k] = model.coef_[0]
                    if succ:
                        lyp_exponent[i] = np.mean(lyp_exp)

            print('({0:0.0f}|{1:0.0f})'.format(run_idx+1, run_cnt))
            out_que.put([run_idx, lyp_exponent])

    for key, name in file_dict.items():
        file_path = os.path.join('/home', 'michael', name)
        subprocess.run(['rm', file_path])

# Number of CPUs to use #
num_of_cpus = 12  # cpu_count()-1
print('Number of CPUs: {0:0.0f}'.format(num_of_cpus))

config_id = '0-1-0'
main, aux = functions.readCrntConfig(config_id)
crnt = -10722. * np.r_[main, 14*aux]
mod_dict = {'mgrid_currents': ' '.join(['{}'.format(c) for c in crnt]), 
            'mgrid_file': os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'mgrid_res1p0mm_30pln.nc')}

date_tag = '20230802'
run_dict = {'dx': 2e-3, # spatial separation of sample points in meters
            'tor_ang': 0.25*np.pi, # toroidal angle of cross section
            'dstp': 5.625, # angular step size in field line following
            'rots': 5, # number of toroidal rotations
            'npts': 3, # number of shifted points around the sample points
            'd0': 1e-3, # distance of shift for shifted points in meters
            'mod_dict': mod_dict}

tor_angs = np.linspace(0, 0.25*np.pi, 5)
for tor_ang in [.25*np.pi]:
    run_dict['tor_ang'] = tor_ang

    # get search domain from vessel wall #
    ves_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'hsx_vessel_dieter_RF.txt')
    with open(ves_path, 'r') as f:
        lines = f.readlines()
        line_strip = lines[1].strip().split()
        tor_pnts = int(line_strip[0])
        pol_pnts = int(line_strip[1])
        nfp = int(line_strip[2])

        tor_dom = np.empty(tor_pnts)
        R_vess = np.empty((tor_pnts, pol_pnts))
        Z_vess = np.empty((tor_pnts, pol_pnts))
        for i in range(tor_pnts):
            tor_idx = 2 + i*(1+pol_pnts)
            beg_idx = tor_idx + 1
            end_idx = beg_idx + pol_pnts
            tor_dom[i] = float(lines[tor_idx].strip())*(np.pi/180)
            for j, line in enumerate(lines[beg_idx:end_idx]):
                line_strip = line.strip().split()
                R_vess[i,j] = float(line_strip[0])*1e-2
                Z_vess[i,j] = float(line_strip[1])*1e-2
    """
    vess_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'vessel90.h5')
    with hf.File(vess_path, 'r') as hf_:
        vess_data = hf_['data'][()]
        vess_dom = hf_['domain'][()]

    idx = np.argmin(np.abs(vess_dom - run_dict['tor_ang']))
    R_vess = np.linalg.norm(vess_data[idx][:, 0:2], axis=1)
    Z_vess = vess_data[idx][:, 2]
    """
    R_min, R_max = np.min(R_vess), np.max(R_vess)
    Z_min, Z_max = np.min(Z_vess), np.max(Z_vess)

    Rpts = int(((R_max-R_min)/run_dict['dx'])+1)
    Zpts = int(((Z_max-Z_min)/run_dict['dx'])+1)
    run_dict['run_cnt'] = Rpts
    print('R points: {}\nZ points: {}\n'.format(Rpts, Zpts))

    R_dom = np.linspace(R_min, R_max, Rpts)
    Z_dom = np.linspace(Z_min, Z_max, Zpts)
    run_dict['Z_dom'] = Z_dom

    # save run data #
    main_id = 'main_coil_{}'.format(config_id.split('-')[0])
    set_id = 'set_{}'.format(config_id.split('-')[1])
    job_id = 'job_{}'.format(config_id.split('-')[2])
    lyp_dir = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'lyaponov_data')
    lyp_nums = [int(f.name.split('.')[0].split('_')[1]) for f in os.scandir(lyp_dir) if f.name.split('_')[0] == date_tag]
    if len(lyp_nums) > 0:
        lyp_tag = str(max(lyp_nums)+1).zfill(4)
    else:
        lyp_tag = '0001'
    lyp_path = os.path.join(lyp_dir, '%s_%s.h5' % (date_tag, lyp_tag))

    if os.path.isfile(lyp_path):
        with hf.File(lyp_path, 'r') as hf_:
            lyp_exponent = hf_['lyaponov exponents'][()]
    else:
        lyp_exponent = np.full((Rpts, Zpts), np.nan)
        with hf.File(lyp_path, 'w') as hf_:
            hf_.create_dataset('lyaponov exponents', data=lyp_exponent)
            hf_.create_dataset('R domain', data=R_dom)
            hf_.create_dataset('Z domain', data=Z_dom)
            hf_.create_dataset('dx', data=run_dict['dx'])
            hf_.create_dataset('dstp', data=run_dict['dstp'])
            hf_.create_dataset('tor_ang', data=run_dict['tor_ang'])
            hf_.create_dataset('rots', data=run_dict['rots'])
            hf_.create_dataset('npts', data=run_dict['npts'])
            hf_.create_dataset('d0', data=run_dict['d0'])

    # populate queue #
    in_que = Queue()
    out_que = Queue()
    for R_idx, R_val in enumerate(R_dom):
        if np.isnan(lyp_exponent[R_idx, :]).all():
            in_que.put([R_idx, R_val])

    for i in range(num_of_cpus):
        in_que.put(None)

    # start exponent calculation processes #
    for i in range(num_of_cpus):
        proc = Process(target=calc_lyp_exp, args=(in_que, out_que, run_dict))
        proc.start()

    # save exponents #
    done_cnt = 0
    while True:
        item = out_que.get()
        if item is None:
            done_cnt+=1
            print('End Process: {0:0.0f} of {1:0.0f}'.format(done_cnt, num_of_cpus))
            if done_cnt == num_of_cpus:
                break
        else:
            R_idx, lyp_exp = item
            with hf.File(lyp_path, 'a') as hf_:
                lyp_exponent = hf_['lyaponov exponents'][()]
                lyp_exponent[R_idx] = lyp_exp
                del hf_['lyaponov exponents']
                hf_.create_dataset('lyaponov exponents', data=lyp_exponent)
