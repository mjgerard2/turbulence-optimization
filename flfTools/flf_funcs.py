# -*- coding: utf-8 -*-
"""
Created on Sat May 16 12:35:20 2020

@author: micha
"""


import numpy as np
import h5py as hf

import os, sys
path = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(path)

from vtkTools import vtk_grids


def plot_poin_data(fig, ax, fileName, tor_ang, scale='T'):
    """ Make poincare plot from specified hdf5 file.

    Parameters
    ----------
    plt : obj
        Plot object on which figure will be constructed.
    fileName : str
        Full path name of imported hdf5 file.
    tor_ang : float
        Toroidal plane plotted.
    scale : str, optional
        Specify the scale of the Magnetic field in Telsa. The default is 'T'.

    Raises
    ------
    ValueError
        Dataset in hdf5 file does not have proper dimensions (2 or 3).

    Returns
    -------
    None.

    """
    poinData = hf.File(fileName, 'r')

    key = 'core'
    core = poinData[key][:]
    t_dom = core[0, :, 2]
    dt = t_dom[1] - t_dom[0]

    npts = t_dom.shape[0]
    stps = int(round(2 * np.pi / dt))
    rots = int(round(t_dom[int(npts-1)] / (2*np.pi)))
    idx_stps = np.argmin(np.abs(t_dom - t_dom[0] - tor_ang)) + [int(i*stps) for i in range(rots)]

    Bmin = np.inf
    Bmax = -np.inf

    B_scale = {'kT': 1e-3,
               'T': 1,
               'mT': 1e3,
               'uT': 1e6,
               'nT': 1e9}

    try:
        B_mult = B_scale[scale]

    except KeyError:
        raise(scale+' is not a valid B field scalar.  Options are kT, T, mT, uT, or nT')

    points = {}
    for key in poinData:
        data = poinData[key][:]

        nsurf = core.shape[0]
        nDim = data.ndim

        if nDim == 2:
            data[:, 3] = B_mult * data[:, 3]

            point = np.empty((rots, 4))
            point[:] = np.nan
            for i, idx in enumerate(idx_stps):
                point[i] = data[idx]

            points[key] = point

            if not np.isnan(point[:, 3].any()):
                Bmin_chk = np.nanmin(point[:, 3])
                Bmax_chk = np.nanmax(point[:, 3])

                if Bmin_chk < Bmin:
                    Bmin = Bmin_chk
                if Bmax_chk > Bmax:
                    Bmax = Bmax_chk

        elif nDim == 3:
            data[:, :, 3] = B_mult * data[:, :, 3]

            point = np.empty((data.shape[0], rots, 4))
            point[:] = np.nan
            for j in range(data.shape[0]):
                for i, idx in enumerate(idx_stps):
                    point[j, i] = data[j, idx]

            points[key] = point

            cnt = data.shape[0]-1
            while cnt >= 0:
                if np.isnan(point[cnt, :, 3]).any():
                    cnt-=1
                    close_surf = False
                else:
                    close_surf = True
                    break

            if close_surf:
                Bmin_chk = np.nanmin(point[cnt, :, 3])
                Bmax_chk = np.nanmax(point[cnt, :, 3])

                if Bmin_chk < Bmin:
                    Bmin = Bmin_chk
                if Bmax_chk > Bmax:
                    Bmax = Bmax_chk

        else:
            raise ValueError("Dataset "+key+" does not have the proper dimensions.  Must be 2 or 3, is {}".format(nDim))

    poinData.close()

    # Plot Poincare #
    if Bmax >= 10:
        form = '%d'
    elif Bmax >= 1:
        form = '%.1f'
    elif Bmax >= 0.1:
        form = '%.2f'
    elif Bmax >= 0.01:
        form = '%.3f'
    else:
        form = '%.4f'

    # cm = plt.cm.get_cmap('jet')

    for key in points:
        data = points[key]

        nDim = data.ndim
        if nDim == 2:
            # smap = ax.scatter(data[0::,0], data[0::,1], c=data[0::,3], vmin=Bmin, vmax=Bmax, s=1, cmap='jet')
            smap = ax.scatter(data[0::,0], data[0::,1], c='w', s=1)
        if nDim == 3:
            # smap = ax.scatter(data[0::,0::,0], data[0::,0::,1], c=data[0::,0::,3], vmin=Bmin, vmax=Bmax, s=1, cmap='jet')
            smap = ax.scatter(data[0::,0::,0], data[0::,0::,1], c='w', s=1)

    # plt.colorbar(label=r'$|\mathbf{B}|$ ('+scale+')', format=form)
    # cbar = fig.colorbar(smap, ax=ax)
    # cbar.ax.set_ylabel(r'$|\mathbf{B}|$ ('+scale+')')

    ax.set_title(r'$\theta$ = %.2f $\pi$' % ((tor_ang-t_dom[0])/np.pi))
    ax.set_xlabel('R')
    ax.set_ylabel('Z')


def make_flf_ScalarVTK(savePath, fileName, data_key, idx=8, scale='T'):
    """ Make vtk file of field line object.

    Parameters
    ----------
    savePath : str
        Directory path where vtk file will be saved.
    fileName : str
        Directory path to hdf5 file where field line data is stored.
    data_key : str
        Data key to be imported from hdf5 file.
    idx : int, optional
        Index of field line to be rendered in specified data key. The default is 8.
    scale : str, optional
        Specify the scale of the Magnetic field in Telsa. The default is 'T'.

    Raises
    ------
    ValueError
        Dataset imported does not have proper dimensionality.
    """
    ### Import Poincare data ###
    poinData = hf.File(fileName, 'r')
    data_set = poinData[data_key][:]
    poinData.close()

    ### Identify Dimensionalisty of field line object ###
    ndim = data_set.ndim
    if ndim == 3:
        nan_loc = np.where(np.isnan(data_set[idx,0::,0::]))[0]
        if nan_loc.any():
            npts = nan_loc[0]
        else:
            npts = data_set[idx].shape[0]

        data = data_set[idx,0:npts,0::]
        dataName = data_key+'_{}.vtk'.format(idx)

    elif ndim == 2:
        nan_loc = np.where(np.isnan(data_set[0::,0::]))[0]
        if nan_loc.any():
            npts = nan_loc[0]
        else:
            npts = data_set.shape[0]

        data = data_set[0:npts]
        dataName = data_key+'.vtk'

    else:
        raise ValueError(fileName+' data set ('+data_key+') has dimensions {}, but should be 2 or 3'.format(ndim))

    ### Identify scale of B field ###
    B_scale = {'kT' : 1e-3,
               'T' : 1,
               'mT' : 1e3,
               'uT' : 1e6,
               'nT' : 1e9}

    try:
        B_mult = B_scale[scale]
    except KeyError:
        raise(scale+' is not a valid B field scalar.  Options are kT, T, mT, uT, or nT')

    ### Construct cartesian grid ###
    r_dom = data[0::,0]
    z_dom = data[0::,1]
    t_dom = data[0::,2]

    x_dom = r_dom * np.cos(t_dom)
    y_dom = r_dom * np.sin(t_dom)
    cartGrid = np.stack((x_dom, y_dom, z_dom), axis=1)

    ### Make vtk file ###
    B_dom = data[0::,3] * B_mult
    vtk_grids.scalar_line_mesh(savePath, dataName, cartGrid, B_dom)

def plot_vessel(plot, tor_ang):
    """ Plot the HSX vessel wall in a poincare plot.

    Parameters
    ----------
    plot : obj
        Matplotlib plotting axis.
    tor_ang : float
        Toroidal angle to plot vessel wall.
    """
    path = os.path.join('/mnt','HSX_Database','HSX_Configs','coil_data','vessel90.h5')
    with hf.File(path,'r') as hf_file:
        vessel = hf_file['data'][:]
        v_dom = hf_file['domain'][:]

    idx = np.argmin(np.abs(v_dom - tor_ang))
    ves_x = vessel[idx,0::,0]
    ves_y = vessel[idx,0::,1]
    ves_z = vessel[idx,0::,2]
    ves_r = np.hypot(ves_x, ves_y)

    plot.plt.plot(ves_r, ves_z, c='k', lineWidth=3)

def make_flf_VectorVTK(savePath, fileName, data_key, idx=8, scale='T'):
    """ Make vtk vector file of field line object.

    Parameters
    ----------
    savePath : str
        Directory path where vtk file will be saved.
    fileName : str
        Directory path to hdf5 file where field line data is stored.
    data_key : str
        Data key to be imported from hdf5 file.
    idx : int, optional
        Index of field line to be rendered in specified data key. The default is 8.
    scale : str, optional
        Specify the scale of the Magnetic field in Telsa. The default is 'T'.

    Raises
    ------
    ValueError
        Dataset imported does not have proper dimensionality.
    KeyError
        Not a valid scale for B field.
    """
    ### Import Poincare data ###
    poinData = hf.File(fileName, 'r')
    data_grid = poinData[data_key][:]
    data_vec = poinData[data_key+' Bvec'][:]
    poinData.close()

    ### Identify Dimensionalisty of field line object ###
    ndim = data_grid.ndim
    if ndim == 3:
        nan_loc = np.where(np.isnan(data_grid[idx,0::,0::]))[0]
        if nan_loc.any():
            npts = nan_loc[0]
        else:
            npts = data_grid[idx].shape[0]

        data_grid = data_grid[idx,0:npts,0::]
        data_vec = data_vec[idx,0:npts,0::]
        dataName = data_key+'_{}.vtk'.format(idx)

    elif ndim == 2:
        nan_loc = np.where(np.isnan(data_grid[0::,0::]))[0]
        if nan_loc.any():
            npts = nan_loc[0]
        else:
            npts = data_grid.shape[0]

        data_grid = data_grid[0:npts]
        data_vec = data_vec[0:npts]
        dataName = data_key+'.vtk'
    else:
        raise ValueError(fileName+' data set ('+data_key+') has dimensions {}, but should be 2 or 3'.format(ndim))

    ### Identify scale of B field ###
    B_scale = {'kT' : 1e-3,
               'T' : 1,
               'mT' : 1e3,
               'uT' : 1e6,
               'nT' : 1e9}

    try:
        B_mult = B_scale[scale]
    except KeyError:
        raise KeyError(scale+' is not a valid B field scalar.  Options are kT, T, mT, uT, or nT')

    ### Construct cartesian grid ###
    r_dom = data_grid[0::,0]
    z_dom = data_grid[0::,1]
    t_dom = data_grid[0::,2]

    x_dom = r_dom * np.cos(t_dom)
    y_dom = r_dom * np.sin(t_dom)
    cart_grid = np.stack((x_dom, y_dom, z_dom), axis=1)

    ### Rotate B vectos to cartesian coordinate system ###
    B_vec = np.empty((r_dom.shape[0], 3))
    for t_idx, t in enumerate(t_dom):
        cos, sin = np.cos(t), np.sin(t)
        mat = np.array([[cos, 0, -sin],
                        [sin, 0, cos],
                        [0, 1, 0]])
        B_vec[t_idx] = np.matmul(mat, data_vec[t_idx][0:3]) * B_mult

    ### Make vtk file ###
    vtk_grids.vector_line_mesh(savePath, dataName, cart_grid, B_vec)

if __name__ == '__main__':
    import plot_define as pd
    import vmecTools.wout_files.wout_read as wr

    # save_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_897', 'set_1', 'job_0', 'poincare_500pnts_50surf.h5')
    save_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_60', 'set_1', 'job_84', 'poincare_500pnts_50surf.h5')

    tor_ang = 0.0*np.pi
    plot = pd.plot_define()
    plt = plot.plt
    fig, ax = plt.subplots(1, 1, tight_layout=True)
    ax.set_aspect('equal')

    plot_poin_data(fig, ax, save_path, tor_ang*np.pi, scale='T')

    plot.plt.tight_layout()
    # plot.plt.savefig(plotPath)
    plot.plt.show()
