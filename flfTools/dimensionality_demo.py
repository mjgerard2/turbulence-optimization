import os
import h5py as hf
import numpy as np

import matplotlib.colors as mc

from sklearn.linear_model import LinearRegression

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd
import flfTools.flf_operations as flfo


def get_circle(radius, x_shift, y_shift):
    theta = np.linspace(-np.pi, np.pi, 1001, endpoint=False)
    x_data = radius * np.cos(theta) + x_shift
    y_data = radius * np.sin(theta) + y_shift
    return x_data, y_data

def inside_counter(poin_data, idx, radii):
    check_point = poin_data[idx]
    comp_points = np.delete(poin_data, idx, axis=0)
    norm_points = np.linalg.norm(comp_points - check_point, axis=1)

    inside = np.empty(radii.shape)
    for i, radius in enumerate(radii):
        inside[i] = norm_points[norm_points <= radius].shape[0]
    return inside

# poin_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'poincare.h5')
poin_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'poincare_data', '150-1-702.h5')
with hf.File(poin_path, 'r') as hf_:
    core = hf_['core'][()]

# FLF parameters #
tor_ang = 0.25*np.pi
t_dom = core[0, :, 2]
dt = t_dom[1] - t_dom[0]
npts = t_dom.shape[0]
stps = int(round(2 * np.pi / dt))
rots = int(round(t_dom[int(npts-1)] / (2*np.pi)))
idx_stps = np.argmin(np.abs(t_dom - t_dom[0] - tor_ang)) + [int(i*stps) for i in range(rots)]

# calculate radii sampling domain #
poin_data = core[-1][idx_stps, 0:2]
ma_axis = np.mean(core[0][idx_stps, 0:2], axis=0)
radius = np.mean(np.linalg.norm(poin_data - ma_axis, axis=1))
radii = np.logspace(np.log10(radius*5*(np.pi/180)), np.log10(radius), 25)

# generate N_x(r) #
# idx_set = np.linspace(0, poin_data.shape[0]-1, 3, dtype=int, endpoint=False)
idx_set = np.array([0, 2, 4], dtype=int)
inside = np.zeros((idx_set.shape[0], radii.shape[0]))
for i, idx in enumerate(idx_set):
    inside[i] = inside_counter(poin_data, idx, radii)

# average N_x and do linear fit #
in_mean = np.mean(inside, axis=0)
x_data = np.log10(radii.reshape((-1, 1)))
y_data = np.log10(in_mean)

model = LinearRegression().fit(x_data, y_data)
r_sq = model.score(x_data, y_data)
x_fit = np.array([x_data[0], x_data[-1]])
y_fit = model.coef_ * x_fit + model.intercept_

slope = model.coef_[0]
flux_surface_score = np.hypot((1.-r_sq), (1.-slope))

# plotting parameters #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(1, 2, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = axs[1]
ax2.set_aspect('equal')

colors = []
for (key, chex) in mc.TABLEAU_COLORS.items():
    colors.append(key)

# plot data #
for i in range(idx_set.shape[0]):
    ax1.scatter(radii, inside[i], marker='o', s=150, edgecolor=colors[i], facecolor='None')

ax1.plot(10**x_fit, 10**y_fit, c='k', ls='--', label='slope = {0:0.2f}'.format(slope))

# flfo.plot_vessel(ax2, tor_ang)
ax2.scatter(poin_data[:, 0], poin_data[:, 1], c='k', s=1)

radii_plot = np.linspace(radii[0], radii[-1], 4)
for i, idx in enumerate(idx_set):
    ax2.scatter(poin_data[idx, 0], poin_data[idx, 1], c=colors[i], s=250, marker='X')
    for radius in radii_plot:
        x_data, y_data = get_circle(radius, poin_data[idx, 0], poin_data[idx, 1])
        ax2.plot(x_data, y_data, c=colors[i])

# axis labels #
ax1.set_ylabel(r'$\langle N_x(r) \rangle_{x}$')
ax1.set_xlabel(r'$r \ / \ m$')

ax2.set_xlabel(r'$R \ / \ m$')
ax2.set_ylabel(r'$Z \ / \ m$')

# axis scale #
ax1.set_xscale('log')
ax1.set_yscale('log')

# legend/grid #
ax1.legend(frameon=False, loc='upper left', fontsize=18)
ax1.grid(which='both')

# save/show #
# plt.show()
save_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'figures', 'FS_demo_150-1-702.png')
plt.savefig(save_path)
