# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 15:04:34 2021

@author: micha
"""

import numpy as np
import h5py as hf

from matplotlib.colors import LogNorm
from scipy.special import legendre

import flf_class

import os, sys
path = os.path.dirname(os.path.abspath(os.getcwd()))
sys.path.append(path)

import plot_define as pd
import directory_dict as dd


def r_func(tor, params):
    n = params['N']
    ro = params['R_0']
    eps_r = params['epsilon r']
    alf_r = params['alpha r']

    return ro + ro * eps_r * np.cos(n * tor + alf_r * np.sin(n * tor))

def z_func(tor, params):
    n = params['N']
    ro = params['R_0']
    eps_z = params['epsilon z']
    alf_z = params['alpha z']

    return ro * eps_z * np.sin(n * tor + alf_z * np.sin(n * tor))


class multipole_tools():
    
    def __init__(self, origin, rad, npts, helical_1, helical_2, I_scl, I_base, tor_pts=1000):   
        og_x = origin[0] * np.cos( origin[2] )
        og_y = origin[0] * np.sin( origin[2] )

        self.og_cyld = origin
        self.og_cart = np.array([ og_x, og_y, origin[1] ])
                
        self.r_dom = np.linspace( -rad, rad, 100 )
        self.z_dom = np.linspace( -rad, rad, 100 )
        self.delta = 0.5 * ( self.r_dom[1] - self.r_dom[0] )
        
        r_grid, z_grid = np.meshgrid( self.r_dom, self.z_dom )
        x_grid = r_grid * np.cos( origin[2] ) 
        y_grid = r_grid * np.sin( origin[2] ) 
        self.grid = np.stack( ( x_grid, y_grid, z_grid ), axis=2 )
        
        self.helical_1 = helical_1
        self.helical_2 = helical_2
        
        self.I_scl = I_scl
        self.I_base = I_base
        
        tor_dom = np.linspace(0, 2*np.pi, tor_pts)
        
        r_dom_1 = r_func( tor_dom, helical_1 )
        x_dom_1 = r_dom_1 * np.cos(tor_dom)
        y_dom_1 = r_dom_1 * np.sin(tor_dom)
        z_dom_1 = z_func( tor_dom, helical_1 )
        self.coil_1 = np.stack( ( x_dom_1, y_dom_1, z_dom_1 ), axis=1 ) - self.og_cart
        self.coil_norm_inv_1 = 1. / np.linalg.norm( self.coil_1, axis=1 )
        
        r_dom_2 = r_func( tor_dom, helical_2 )
        x_dom_2 = r_dom_2 * np.cos(tor_dom)
        y_dom_2 = r_dom_2 * np.sin(tor_dom)
        z_dom_2 = z_func( tor_dom, helical_2 )
        self.coil_2 = np.stack( ( x_dom_2, y_dom_2, z_dom_2 ), axis=1 ) - self.og_cart
        self.coil_norm_inv_2 = 1. / np.linalg.norm( self.coil_2, axis=1 )
        
    def field_energy(self, flf, plot_key=None):
        r_grid, z_grid = np.meshgrid( self.r_dom + self.og_cyld[0], self.z_dom + self.og_cyld[1] )
        t_grid = np.full( r_grid.shape, self.og_cyld[2] )
        points = np.stack( ( r_grid, z_grid, t_grid ), axis=2 ).reshape( self.r_dom.shape[0] * self.z_dom.shape[0], 3 )
        
        mod_dict = {'general_option' : 2,
                    'points_number' : points.shape[0]}
        
        flf.change_params(mod_dict)
        points_out = flf.execute_flf(points, DeBug=True)
        energy = points_out[0:,3]**2
        self.energy = energy.reshape( self.r_dom.shape[0], self.z_dom.shape[0] )
        
        if plot_key:
            fig, ax = plot_key.fig, plot_key.ax
            s_map = ax.pcolormesh(self.r_dom * 1e2, self.z_dom * 1e2, self.energy, cmap='jet')
            cbar = fig.colorbar( s_map, ax=ax )
            cbar.ax.set_ylabel(r'$\mathbf{B} \cdot \mathbf{B}$')
    
    def expand_pole(self, mesh, n, plot_key=None):
        A_pole = np.empty(mesh.shape)
        
        leg_pole = legendre(n)
        for r_idx in range(mesh.shape[0]):
            for z_idx in range(mesh.shape[1]):
                r = mesh[r_idx, z_idx]
                r_norm = np.linalg.norm(r)
                r_norm_inv = 1. / r_norm
                
                cos_1 = np.dot( self.coil_1, r[:,None] ).reshape( self.coil_1.shape[0] ) * self.coil_norm_inv_1 * r_norm_inv
                cos_2 = np.dot( self.coil_2, r[:,None] ).reshape( self.coil_2.shape[0] ) * self.coil_norm_inv_2 * r_norm_inv
                                
                integ_1 = ( ( r_norm )**n ) * ( ( self.coil_norm_inv_1 )**(n+1) ) * leg_pole(cos_1)
                integ_2 = ( ( r_norm )**n ) * ( ( self.coil_norm_inv_2 )**(n+1) ) * leg_pole(cos_2)
        
                A_pole_x = 1e-7 * self.I_base * ( I_scl * np.trapz( integ_1, self.coil_1[0:,0] ) + np.trapz( integ_2, self.coil_2[0:,0] ) )
                A_pole_y = 1e-7 * self.I_base * ( I_scl * np.trapz( integ_1, self.coil_1[0:,1] ) + np.trapz( integ_2, self.coil_2[0:,1] ) )
                A_pole_z = 1e-7 * self.I_base * ( I_scl * np.trapz( integ_1, self.coil_1[0:,2] ) + np.trapz( integ_2, self.coil_2[0:,2] ) )
                
                A_pole[r_idx, z_idx] = np.array([A_pole_x, A_pole_y, A_pole_z])

        if plot_key:
            A_pole_norm = np.linalg.norm(A_pole, axis=2)
            
            #plt = plot.plt
            fig, ax = plot_key.fig, plot_key.ax
            #s_map = ax.pcolormesh(self.r_dom * 1e2, self.z_dom * 1e2, A_pole_norm, norm=LogNorm(vmin=np.min(A_pole_norm), vmax=np.max(A_pole_norm)), cmap='jet')
            s_map = ax.pcolormesh(self.r_dom * 1e2, self.z_dom * 1e2, A_pole_norm, cmap='jet')
            
            cbar = fig.colorbar(s_map, ax=ax)
            cbar.ax.set_ylabel(r'$A_{pole}$')
                
        return A_pole
    
    def pole_energy(self, n, plot_key=None):
        x_pts = self.grid.shape[0]
        y_pts = self.grid.shape[1]
        
        fill_0 = np.zeros( (x_pts, y_pts) )
        fill_d = np.full( (x_pts, y_pts), self.delta )
        
        stp_x = np.stack( ( fill_d, fill_0, fill_0 ), axis=2 )
        stp_y = np.stack( ( fill_0, fill_d, fill_0 ), axis=2 )
        stp_z = np.stack( ( fill_0, fill_0, fill_d ), axis=2 )
        
        mesh_x_plus = self.grid + stp_x
        mesh_y_plus = self.grid + stp_y
        mesh_z_plus = self.grid + stp_z
                
        mesh_x_minus = self.grid - stp_x
        mesh_y_minus = self.grid - stp_y
        mesh_z_minus = self.grid - stp_z
        
        A_plus_x = self.expand_pole(mesh_x_plus, n)
        A_plus_y = self.expand_pole(mesh_y_plus, n)
        A_plus_z = self.expand_pole(mesh_z_plus, n)
        
        A_mins_x = self.expand_pole(mesh_x_minus, n)
        A_mins_y = self.expand_pole(mesh_y_minus, n)
        A_mins_z = self.expand_pole(mesh_z_minus, n)
                
        coeff = 0.5 / self.delta
        
        Bx = coeff * ( A_plus_y[0:,0:,2] + A_mins_z[0:,0:,1] - ( A_mins_y[0:,0:,2] + A_plus_z[0:,0:,1] ) )
        By = coeff * ( A_plus_z[0:,0:,0] + A_mins_x[0:,0:,2] - ( A_mins_z[0:,0:,0] + A_plus_x[0:,0:,2] ) )
        Bz = coeff * ( A_plus_x[0:,0:,1] + A_mins_y[0:,0:,0] - ( A_mins_x[0:,0:,1] + A_plus_y[0:,0:,0] ) )
        
        self.B_pole = np.stack( ( Bx, By, Bz ), axis=2 )
    
        if plot_key:
            try:
                nrg_ratio = np.linalg.norm(self.B_pole, axis=2)**2 / self.energy
                c_lab = r'$E_{pole} / E_{total}$'
            except AttributeError:
                nrg_ratio = np.linalg.norm(self.B_pole, axis=2)**2
                c_lab = r'$E_{pole}$'
            
            r_dom = ( self.r_dom + self.og_cyld[0] ) * 1e2
            z_dom = ( self.z_dom + self.og_cyld[1] ) * 1e2
            
            fig, ax = plot_key.fig, plot_key.ax
            s_map = ax.pcolormesh( r_dom, z_dom, nrg_ratio, norm=LogNorm(vmin=1e-10, vmax=1), cmap='jet')
            #s_map = ax.pcolormesh(self.r_dom * 1e2, self.z_dom * 1e2, nrg_ratio, vmin=0, vmax=1, cmap='jet')
            cbar = fig.colorbar(s_map, ax=ax)
            cbar.ax.set_ylabel(c_lab)
            
if __name__ == '__main__':
    ### Import Poincare Data ###   
    I_scl = -4.5
    basePath = os.path.join(dd.exp_local['ITOS'],'configurations','5-fold_configs','YG_N5_alfR1_0.650')
    fileName = os.path.join(basePath,'poinData_scl_{0:0.2f}.h5'.format(-I_scl))
    
    tor_ang = 0.0 * np.pi
    with hf.File(fileName, 'r') as poinData:
        key = 'core'
        core = poinData[key][:]
    
    t_dom = core[0,0::,2]
    dt = t_dom[1] - t_dom[0]
            
    npts = t_dom.shape[0]
    stps = int( round(2 * np.pi / dt)) 
    rots = int( round(t_dom[int(npts-1)] / (2*np.pi)) )
    
    idx_stps = np.argmin(np.abs(t_dom - tor_ang)) + [int(i*stps) for i in range(rots)]
    lcfs_pnts = np.stack( ( core[-1, idx_stps, 0], core[-1, idx_stps, 1], core[-1, idx_stps, 2] ), axis=1 )
    ma_pnts = np.array([ np.mean( core[0, idx_stps, 0] ), np.mean( core[0, idx_stps, 1] ), tor_ang ])
    
    rad = np.max( np.linalg.norm( lcfs_pnts[0:,0:2] - ma_pnts[0:2], axis=1 ) )
    
    ### Instantiate FLF ###
    dataName = 'scale_scan_6k_4.9-2.7.h5'
    with hf.File(os.path.join(basePath, dataName), 'r') as hf_file:
        coil_crnts = hf_file['coil currents'][:]
    
    scales = coil_crnts[0::,0] / coil_crnts[0::,1]
    idx = np.argmin(np.abs(scales - I_scl))
    
    crnt = np.array([coil_crnts[idx,0], coil_crnts[idx,1], 0, 0, 0, 0])
    mod_dict = {'mgrid_currents' : ' '.join(['{}'.format(c) for c in crnt]),
                'mgrid_file' : 'itos_coils/5-fold_configs/mgrid_itos_YG_N5_alfR1_0.650.nc'}
        
    flf = flf_class.flf_wrapper('ITOS')
    flf.change_params(mod_dict)
    
    ### Multipole Expansion at Sample Points ###
    helical_1 = {'N' : 5,
                 'R_0' : 0.31,
                 'epsilon r' : 0.5827466883084267,
                 'alpha r' : 0.65,
                 'epsilon z' : 0.65,
                 'alpha z' : 0.65}
    
    helical_2 = {'N' : 5,
                 'R_0' : 0.3369565217391305,
                 'epsilon r' : 0,
                 'alpha r' : 0,
                 'epsilon z' : 0.4,
                 'alpha z' : 0.4}
    
    r_beg, r_end = ma_pnts[0] - rad, ma_pnts[0] + rad
    z_beg, z_end = ma_pnts[1] - rad, ma_pnts[1] + rad
    
    npts = 31
    
    #r_dom = np.linspace(r_beg, r_end, npts)
    #z_dom = np.linspace(z_beg, z_end, npts)
    
    #r_dom = np.linspace(1, 0.5, npts)
    #z_dom = np.linspace(-0.25, 0.25, npts)
    
    #delta = 0.5 * ( r_dom[1] - r_dom[0] )
    
    origin = ma_pnts#np.array([0.2, 0, 0])
    multi = multipole_tools(origin, rad, npts, helical_1, helical_2, I_scl, coil_crnts[idx,1])

    plot = pd.plot_define(eqAsp=True)        
    plt = plot.plt
    
    #A_dip = multi.dipole( multi.grid, plot )
    #A_quad = multi.quadrupole( multi.grid, plot )
    #A_octo = multi.octopole( multi.grid, plot )
    
    multi.field_energy(flf)
    
    #A_pole = multi.expand_pole(multi.grid, 1, plot)
    multi.pole_energy(1, plot)
    B_pole = multi.B_pole    
    
    plt.scatter(core[0,idx_stps,0] * 1e2, core[0,idx_stps,1] * 1e2, c='k', s=1)
    plt.scatter(core[-1,idx_stps,0] * 1e2, core[-1,idx_stps,1] * 1e2, c='k', s=1)
    
    #plt.scatter( 0, 0, c='k', s=1)
    #plt.scatter( (core[-1,idx_stps,0] - ma_pnts[0]) * 1e2, (core[-1,idx_stps,1] - ma_pnts[1]) * 1e2, c='k', s=1)
    
    plt.xlabel('R [cm]')
    plt.ylabel('Z [cm]')
    plt.title(r'$I_{1}/I_{2}$'+' = {0:0.2f}'.format(I_scl))
    
    plt.tight_layout()
    plt.show()
    