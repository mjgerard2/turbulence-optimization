import os
import h5py as hf
import numpy as np

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import flfTools.flf_operations as flfo

def get_vessel(tor_ang):
    # get search domain from vessel wall #
    ves_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'hsx_vessel_dieter_RF.txt')
    with open(ves_path, 'r') as f:
        lines = f.readlines()
        line_strip = lines[1].strip().split()
        tor_pnts = int(line_strip[0])
        pol_pnts = int(line_strip[1])
        nfp = int(line_strip[2])

        tor_dom = np.empty(tor_pnts)
        R_vess = np.empty((tor_pnts, pol_pnts))
        Z_vess = np.empty((tor_pnts, pol_pnts))
        for i in range(tor_pnts):
            tor_idx = 2 + i*(1+pol_pnts)
            beg_idx = tor_idx + 1
            end_idx = beg_idx + pol_pnts
            tor_dom[i] = float(lines[tor_idx].strip())*(np.pi/180)
            for j, line in enumerate(lines[beg_idx:end_idx]):
                line_strip = line.strip().split()
                R_vess[i,j] = float(line_strip[0])*1e-2
                Z_vess[i,j] = float(line_strip[1])*1e-2

    idx = np.argmin(np.abs(tor_dom - tor_ang))
    return R_vess[idx], Z_vess[idx]
    
config_id = '0-1-0'
main_id = 'main_coil_{}'.format(config_id.split('-')[0])
set_id = 'set_{}'.format(config_id.split('-')[1])
job_id = 'job_{}'.format(config_id.split('-')[2])
base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)

# read in lyaponov data #
lyp_tag = '20230803_0002'
lyp_path = os.path.join(base_path, 'lyaponov_data', '%s.h5' % lyp_tag)
with hf.File(lyp_path, 'r') as hf_:
    lyp_exp = hf_['lyaponov exponents'][()]
    R_dom = hf_['R domain'][()]
    Z_dom = hf_['Z domain'][()]
    tor_ang = hf_['tor_ang'][()]

# define poincare path #
if config_id == '0-1-0':
    poin_path = os.path.join(base_path, 'poincare.h5')
else:
    poin_path = os.path.join(base_path, '%s.h5' % config_id)

# get vessel wall #
R_vess, Z_vess = get_vessel(tor_ang)

# plot data #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(1, 2, sharex=True, sharey=True, tight_layout=True, figsize=(14, 8))

axs[0].set_aspect('equal')
axs[1].set_aspect('equal')

# plot data #
axs[0].plot(R_vess, Z_vess, c='k', linewidth=2)
axs[1].plot(R_vess, Z_vess, c='k', linewidth=2)
x_lim, y_lim = axs[0].get_xlim(), axs[0].get_ylim()

smap1 = axs[0].pcolormesh(R_dom, Z_dom, lyp_exp[:, :, 0].T, cmap='jet', vmin=0, vmax=0.1)
smap2 = axs[1].pcolormesh(R_dom, Z_dom, lyp_exp[:, :, 1].T, cmap='jet', vmin=0, vmax=0.1)

# flfo.plot_poincare(poin_path, tor_ang, fig, axs[0], show_vessel=False)
# flfo.plot_poincare(poin_path, tor_ang, fig, axs[1], show_vessel=False)

# axis labels #
axs[0].set_xlabel('R/m')
axs[0].set_ylabel('Z/m')
axs[1].set_xlabel('R/m')

cbar1 = fig.colorbar(smap1, ax=axs[0])
cbar1.ax.set_ylabel(r'$\lambda_{R}$')

cbar2 = fig.colorbar(smap2, ax=axs[1])
cbar2.ax.set_ylabel(r'$\lambda_{Z}$')

# axis limit #
axs[0].set_xlim(x_lim)
axs[0].set_ylim(y_lim)

# plt.show()
save_path = os.path.join(base_path, 'lyaponov_data', '%s.png' % lyp_tag)
plt.savefig(save_path)
# plt.show()
