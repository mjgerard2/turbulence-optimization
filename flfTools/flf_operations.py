# -*- coding: utf-8 -*-
"""
Created on Mon May 18 10:26:18 2020

@author: micha
"""

import numpy as np
import h5py as hf
import subprocess

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import flfTools.flf_class as flf_class
import flfTools.flf_funcs as flf_funcs

import plot_define as pd
import directory_dict as dd


def init_hsx(main, aux, mod_dict):
    """ Initialized flf object with the HSX experimental parameters.

    Parameters
    ----------
    main : array
        Main coil configuration, supplied in fractional percent of standard
        10.722 kA per turn.
    aux : array
        Auxiliary coil configuration, supplied in fractional percent of total
        150.108 kA main coil current.
    mod_dict : dict
        Dictionary of modified flf parameters.

    Returns
    -------
    flf : obj
        Field line following object, defined in flf_class.py.
    """
    ### HSX current profile ###
    crnt = -10722. * np.r_[main, 14*aux]
    mod_dict['mgrid_currents'] = ' '.join(['{}'.format(c) for c in crnt])
    mod_dict['mgrid_file'] = '/mnt/HSX_Database/HSX_Configs/coil_data/mgrid_hsx_wmain.nc'

    flf = flf_class.flf_wrapper('HSX')
    flf.change_params(mod_dict)

    return flf

def init_itos(helic_1, helic_2, pol, mod_dict, tag):
    """ Initialized flf object with the ITOS experimental parameters.

    Parameters
    ----------
    helic_1 : float
        Helical coil 1 current in Amperes.
    helic_2 : float
        Helical coil 2 current in Amperes
    pol : float
        Poloidal coil current in Amperes.
    mod_dict : dict
        Dictionary of modified flf parameters.

    Returns
    -------
    flf : obj
        Field line following object, defined in flf_class.py.
    """
    ### ITOS Current Profile ###
    crnt = np.array([helic_1, helic_2, pol, pol, pol, pol])
    mod_dict['mgrid_currents'] = ' '.join(['{}'.format(c) for c in crnt])
    mod_dict['mgrid_file'] = 'itos_coils/5-fold_configs/{}'.format(tag)

    flf = flf_class.flf_wrapper('ITOS')
    flf.change_params(mod_dict)

    return flf

def init_etos(helical, linear, mod_dict):
    """ Initialized flf object with the ITOS experimental parameters.

    Parameters
    ----------
    helical : float
        Helical coil current in Amperes.
    linear : float
        Linear coil current in Amperes
    mod_dict : dict
        Dictionary of modified flf parameters.

    Returns
    -------
    flf : obj
        Field line following object, defined in flf_class.py.
    """
    ### ITOS Current Profile ###
    flf = flf_class.flf_wrapper('ETOS')
    flf.change_params(mod_dict)

    return flf

def read_out_along_line(flf, tor_ang, ro_ang, ro_lim):
    """ Make poincare plot with initial points lying on a line that extends out
    from the magnetic axis at the specified angle out to the specified
    distance.

    Parameters
    ----------
    flf : obj
        Field line following object, defined in flf_class.py.
    tor_ang : float
        Toroidal angle where poincare plane lies.
    ro_ang : float
        Angle out from magnetic axis from which points are initialized.
    ro_lim : TYPE
        Distance from mangetic axis to which points are initialized.
    """
    plot = pd.plot_define(fontSize=24, labelSize=32, lineWidth=3, eqAsp=True)
    flf.read_wout()
    flf.read_out_along_line(tor_ang, ro_ang, ro_lim, plt=plot.plt)
    plot.plt.show()

def read_out_point(flf, init_point):
    plot = pd.plot_define()
    flf.read_out_point(init_point, plot.plt)
    plot.plt.show()

def read_out_domain(flf, pnt1, pnt2, nsurf):
    plot = pd.plot_define(fontSize=24, labelSize=32, lineWidth=3, eqAsp=True)
    flf.read_out_domain(pnt1, pnt2, plt=plot.plt, nsurf=nsurf)

    theta = np.linspace(0, 2*np.pi, 100)
    x_dom = 0.244856 + 0.07874 * np.cos(theta)
    y_dom = 0.07874 * np.sin(theta)
    plot.plt.plot(x_dom, y_dom, c='k', linewidth=5)

    return plot
    #plot.plt.show()

def plot_vessel(ax, tor_ang):
    path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'vessel90.h5')
    with hf.File(path,'r') as hf_file:
        vessel = hf_file['data'][:]
        v_dom = hf_file['domain'][:]

    idx = np.argmin(np.abs(v_dom - tor_ang))
    
    ves_x = vessel[idx,0::,0]
    ves_y = vessel[idx,0::,1]
    ves_z = vessel[idx,0::,2]
    ves_r = np.hypot(ves_x, ves_y)

    ax.plot(ves_r, ves_z, c='k', lw=3)

def plot_poincare(fileName, tor_ang, fig, ax, B_scale='T', show_vessel=True):
    # plot = pd.plot_define(fontSize=24, labelSize=32, lineWidth=3, eqAsp=True)
    if show_vessel:
        plot_vessel(ax, tor_ang)
    flf_funcs.plot_poin_data(fig, ax, fileName, tor_ang, scale=B_scale)

def current_scale_scan(tag, scale, init_point, limit_point):
    """ Generates magnetic axis and LCFS data for a scaled current scan
    between Helical coils 1 and 2.

    Parameters
    ----------
    tag : str
        This tag specifies the directory in which the mgrid file is stored
        within the working directory.
    scale : arr
        Current scale ratio between coils 1 and 2.
    init_point : arr
        [r, z, phi] point where the search for closed flux surfaces begins.
    limit_point : arr
        [r, z, phi] point that constrains the domain of the search for closed
        flux surface.
    """
    npts = scale.shape[0]

    r_init, z_init, t_init = init_point[0], init_point[1], init_point[2]

    ma_pnts = np.empty((npts, 3))
    lcfs_pnts = np.empty((npts, 3))
    coil_crnts = np.empty((npts, 2))
    for s, scl in enumerate(scale):
        hel_2 = -6000
        hel_1 = scl * hel_2
        coil_crnts[s] = np.array([hel_1, hel_2])

        flf = init_itos(hel_1, hel_2, 0, mod_dict, tag)

        if s == 0:
            print('\n\n-------------\n'
                  'Working With : '+flf.params['mgrid_file']+
                  '-------------')

        print('\n------------\n'
              '{0} - {1} : {2}\n'.format(s+1, len(scale), scl)+
              '------------')

        ma_pnts[s], lcfs_pnts[s] = flf.find_boundaries(init_point, 4, limit=limit_point)
        init_point = np.array([r_init, z_init, t_init])

    savePath = os.path.join(dd.exp_local['ITOS'],'configurations','5-fold_configs',tag)
    dataPath = os.path.join(savePath, 'scale_scan_6k_{0}-{1}.h5'.format(np.abs(scale[0]), np.abs(scale[-1])))
    plotPath = os.path.join(savePath, 'scale_scan_6k_{0}-{1}.png'.format(np.abs(scale[0]), np.abs(scale[-1])))

    with hf.File(dataPath, 'w') as hf_file:
        hf_file.create_dataset('axis points', data=ma_pnts)
        hf_file.create_dataset('lcfs points', data=lcfs_pnts)
        hf_file.create_dataset('coil currents', data=coil_crnts)

    plot = pd.plot_define(lineWidth=2)

    plot.plt.plot(scale, ma_pnts[0::,0]*100, c='k', marker='s', label='MA')
    plot.plt.plot(scale, lcfs_pnts[0::,0]*100, c='tab:red', marker='o', label='LCFS')

    plot.plt.xlabel(r'$C_1 / C_2$')
    plot.plt.ylabel('R [cm]')

    plot.plt.xlim(scale[0], scale[-1])

    plot.plt.grid()
    plot.plt.legend()
    plot.plt.tight_layout()
    plot.plt.savefig(plotPath)

def plot_itos_vessel_fit(savePath, dataName, scale, tag):
    dstp=1
    rots=500

    dphi = dstp * (np.pi/180)
    stps = int(2 * np.pi / dphi)
    npts = int(rots*stps)

    mod_dict = {'points_dphi' : dphi,
                'n_iter' : npts}

    dataPath = os.path.join(savePath, dataName)

    with hf.File(dataPath, 'r') as hf_file:
        ma_pnts = hf_file['axis points'][:]
        lcfs_pnts = hf_file['lcfs points'][:]
        coil_crnts = hf_file['coil currents'][:]

    if np.isnan(ma_pnts).all():
        return False

    crnt_cnts = coil_crnts.shape[0]
    scl = []
    r_lcfs = []
    r_ma = []
    z_lcfs = []
    z_ma = []
    for c, crnts in enumerate(coil_crnts):
        print('   Working : {0} of {1}'.format(c+1, crnt_cnts))
        if not np.isnan(ma_pnts[c]).any():
            flf = init_itos(crnts[0], crnts[1], 0, mod_dict, tag)
            ma_points = flf.execute_flf(ma_pnts[c])
            lcfs_points = flf.execute_flf(lcfs_pnts[c])

            scl.append(crnts[0]/crnts[1])

            r_lcfs.append([np.min(lcfs_points[0::,0]), np.max(lcfs_points[0::,0])])
            z_lcfs.append([np.min(lcfs_points[0::,1]), np.max(lcfs_points[0::,1])])

            r_ma.append([np.min(ma_points[0::,0]), np.max(ma_points[0::,0])])
            z_ma.append([np.min(ma_points[0::,1]), np.max(ma_points[0::,1])])

    r_lcfs = np.array(r_lcfs)
    r_ma = np.array(r_ma)
    z_lcfs = np.array(z_lcfs)
    z_ma = np.array(z_ma)

    ### R Fit Plot ###
    plot = pd.plot_define(lineWidth=2)
    plt, ax = plot.plt, plot.ax

    R_min, R_max = 0.244856-0.07874, 0.244856+0.07874
    ax.plot(scale, [R_min]*scale.shape[0], c='k')
    ax.plot(scale, [R_max]*scale.shape[0], c='k')

    ax.plot(scl, r_ma[0::,0], c='k', ls='--', markerfacecolor="None", markeredgecolor='k', markersize=10, marker='v', label='MA-min')
    ax.plot(scl, r_ma[0::,1], c='k', ls='--', markerfacecolor="None", markeredgecolor='k', markersize=10, marker='^', label='MA-max')

    ax.plot(scl, r_lcfs[0::,0], c='tab:red', ls='--', markerfacecolor="None", markeredgecolor='tab:red', markersize=10, marker='v', label='LCFS-min')
    ax.plot(scl, r_lcfs[0::,1], c='tab:red', ls='--', markerfacecolor="None", markeredgecolor='tab:red', markersize=10, marker='^', label='LCFS-max')

    plt.title(tag)
    plt.xlabel('C1/C2')
    plt.ylabel('R [m]')
    #plt.ylim(0.9*R_min, 1.*np.max(r_lcfs))

    plt.legend(fontsize=14)
    plt.tight_layout()

    plotPath = os.path.join(savePath, 'R_fit.png')
    plt.savefig(plotPath)

    ### Z Fit Plot ###
    plot = pd.plot_define(lineWidth=2)
    plt, ax = plot.plt, plot.ax

    Z_min, Z_max = -0.07874, 0.07874
    ax.plot(scale, [Z_min]*scale.shape[0], c='k')
    ax.plot(scale, [Z_max]*scale.shape[0], c='k')

    ax.plot(scl, z_ma[0::,0], c='k', ls='--', markerfacecolor="None", markeredgecolor='k', markersize=10, marker='v', label='MA-min')
    ax.plot(scl, z_ma[0::,1], c='k', ls='--', markerfacecolor="None", markeredgecolor='k', markersize=10, marker='^', label='MA-max')

    ax.plot(scl, z_lcfs[0::,0], c='tab:red', ls='--', markerfacecolor="None", markeredgecolor='tab:red', markersize=10, marker='v', label='LCFS-min')
    ax.plot(scl, z_lcfs[0::,1], c='tab:red', ls='--', markerfacecolor="None", markeredgecolor='tab:red', markersize=10, marker='^', label='LCFS-max')

    plt.title(tag)
    plt.xlabel('C1/C2')
    plt.ylabel('Z [m]')
    #plt.ylim(0.9*Z_min, 1.*np.max(r_lcfs))

    plt.legend(fontsize=14)
    plt.tight_layout()

    plotPath = os.path.join(savePath, 'Z_fit.png')
    plt.savefig(plotPath)

def input_VMEC(template, output, descur, psiEdge):
    read_segment = False

    mode_num = []
    BC_amps = []
    AX_amps = []

    with open(descur, 'r') as my_file:
        lines = my_file.readlines()
        for line in lines:
            line = line.split()
            line_len = len(line)
            if line_len > 0:
                if read_segment:
                    try:
                        int( line[0] )

                        mode_num.append( [ line[0], line[1] ] )
                        BC_amps.append( [ line[2], line[5] ] )
                        if line_len > 6:
                            AX_amps.append( [ line[6], line[7] ] )
                    except ValueError:
                        read_segment = False
                if line[0] == 'MB' and line_len > 3:
                    read_segment = True

    with open(template, 'r') as my_file:
        lines = my_file.readlines()

        new_lines = []
        for line in lines:
            line_split = line.split()

            if line_split[0] == 'PHIEDGE':
                new_lines.append( '  PHIEDGE = {}\n'.format( psiEdge ) )
            elif line_split[0] == 'RAXIS':
                new_lines.append( '  RAXIS = ' + ' '.join( [ Ax[0] for Ax in AX_amps ] ) + '\n' )
            elif line_split[0] == 'ZAXIS':
                new_lines.append( '  ZAXIS = ' + ' '.join( [ Ax[1] for Ax in AX_amps ] ) + '\n' )
            elif line_split[0] == 'RBC(00,00)':
                for idx, BC in enumerate(BC_amps):
                    RBC, ZBS = BC[0], BC[1]
                    pol_mode, tor_mode = mode_num[idx][1], mode_num[idx][0]
                    new_lines.append( '  RBC( {0},{1}) = {2}  ZBS( {0},{1}) = {3}\n'.format(pol_mode, tor_mode, RBC, ZBS) )
                break
            else:
                new_lines.append( line )

    with open(output, 'w') as my_file:
        for line in new_lines:
            my_file.write( line )
        my_file.write('/\n&END')

if __name__ == '__main__':
    ### flf Parameters ###
    dstp=1
    rots=500

    dphi = dstp * (np.pi/180)
    stps = int(2 * np.pi / dphi)
    npts = int(rots*stps)

    mod_dict = {'points_dphi' : dphi,
                'n_iter' : npts}

    # Generate Plot Axis #
    plot = pd.plot_define()

    plt = plot.plt
    fig, ax = plt.subplots(1, 1, tight_layout=True)
    ax.set_aspect('equal')

    poin_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_897', 'set_1', 'job_0', 'poincare_500pnts_50surf.h5')
    plot_poincare(poin_path, 0.01*np.pi, fig, ax, B_scale='T', show_vessel=True)

    plt.show()

