import os
import h5py as hf
import numpy as np

from sklearn.linear_model import LinearRegression

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd
import flfTools.flf_operations as flfo


# poin_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'poincare.h5')
poin_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'poincare_data', '293-1-702.h5')
with hf.File(poin_path, 'r') as hf_:
    core = hf_['core'][()]

# FLF parameters #
tor_ang = 0.25*np.pi
t_dom = core[0, :, 2]
dt = t_dom[1] - t_dom[0]
npts = t_dom.shape[0]
stps = int(round(2 * np.pi / dt))
rots = int(round(t_dom[int(npts-1)] / (2*np.pi)))
idx_stps = np.argmin(np.abs(t_dom - t_dom[0] - tor_ang)) + [int(i*stps) for i in range(rots)]

fs_slope_score = np.full(core.shape[0], np.nan)
fs_fit_score = np.full(core.shape[0], np.nan)
for pdx, poin_data in enumerate(core[:, idx_stps, 0:2]):
    is_nan = np.isnan(poin_data) 
    if any(is_nan[:, 0]):
        fs_slope_score[i] = 0.707106781
        fs_fit_score[i] = 0.707106781
    else:
        # dimensionality curve for initial point #
        ma_axis = np.mean(core[0][idx_stps, 0:2], axis=0)
        radius = np.mean(np.linalg.norm(poin_data - ma_axis, axis=1))
        radii = np.logspace(np.log10(radius*5*(np.pi/180)), np.log10(radius), 25)

        inside = np.zeros(radii.shape)
        for idx in range(poin_data.shape[0]):
            check_point = poin_data[idx]
            comp_points = np.delete(poin_data, idx, axis=0)
            norm_points = np.linalg.norm(comp_points - check_point, axis=1)

            idx_inside = np.empty(radii.shape)
            for i, radius in enumerate(radii):
                idx_inside[i] = norm_points[norm_points <= radius].shape[0]
            inside = inside + idx_inside

        inside = inside / poin_data.shape[0]

        x_data = np.log10(radii.reshape((-1, 1)))
        y_data = np.log10(inside)

        model = LinearRegression().fit(x_data, y_data)
        r_sq = model.score(x_data, y_data)
        x_fit = np.array([x_data[0], x_data[-1]])
        y_fit = model.coef_ * x_fit + model.intercept_

        slope = model.coef_[0]
        flux_surface_score = np.hypot((1.-r_sq), (1.-slope))
        fs_slope_score[pdx] = np.abs(1.-slope)
        fs_fit_score[pdx] = np.abs(1.-r_sq)

# plotting parameters #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(1, 2, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = axs[1]
ax2.set_aspect('equal')

# plot data #
# ax1.plot(np.arange(core.shape[0]), np.hypot(fs_slope_score, fs_fit_score), ls='--', marker='v', ms=10, mfc='None', label=r'$\sqrt{(1-m)^2 + (1-r^2)^2}$')
ax1.plot(np.arange(core.shape[0]), fs_slope_score, ls='--', marker='o', ms=10, mfc='None', label=r'$|1-m|$')
# ax1.plot(np.arange(core.shape[0]), fs_fit_score, ls='--', marker='s', ms=10, mfc='None', label=r'$|1-r^2|$')
ax1.plot([0, core.shape[0]+1], [0.05, 0.05], ls='--', c='k')

flfo.plot_poincare(poin_path, tor_ang, fig, ax2)

# axis labels #
ax1.set_xlabel('FS index')
ax1.set_ylabel(r'$|1 - m|$')

# axis limits #
ax1.set_xlim(0, core.shape[0]+1)
# ax1.set_ylim(0, ax1.get_ylim()[1])
ax1.set_yscale('log')

# legend/grid #
# ax1.legend(frameon=False, loc='upper left', fontsize=18)
ax1.grid()

# save/show #
# plt.show()
# save_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'figures', 'FS_score_QHS.png')
save_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'figures', 'FS_score_293-1-702.png')
plt.savefig(save_path)
