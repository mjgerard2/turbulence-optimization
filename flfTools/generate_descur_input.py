import os
import h5py as hf
import numpy as np

import flf_class as flfc
import databaseTools.functions as fun

import sys
WORKDIR = os.path.dirname(os.getcwd())
sys.path.append(WORKDIR)

import plot_define as pd

poin_dirc = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'poincare_data', 'poin_filtered')
poin_names = [f.name for f in os.scandir(poin_dirc)]

for cnt, name in enumerate(poin_names):
    print('({}|{})'.format(cnt+1, len(poin_names)))
    save_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'descur_inputs', '%s.txt' % name.split('.')[0])
    if os.path.isfile(save_path):
        continue

    # Define Coil Currents #
    config_id = name.split('.')[0]
    main_crnt, aux_crnt = fun.readCrntConfig(config_id)

    crnt = -10722. * np.r_[main_crnt, 14*aux_crnt]
    crnt_str = ' '.join(['{}'.format(c) for c in crnt])

    # Read in Poincare Data #
    main_id = 'main_coil_{}'.format(config_id.split('-')[0])
    set_id = 'set_{}'.format(config_id.split('-')[1])
    job_id = 'job_{}'.format(config_id.split('-')[2])
    poin_path = os.path.join(poin_dirc, name)

    with hf.File(poin_path, 'r') as hf_:
        poin_data = hf_['core'][()]

    nsurf = poin_data.shape[0]
    ma_pnt = poin_data[0, 0, 0:3]
    init_pnt = poin_data[nsurf-1, 0, 0:3]

    # Construct FLF Object #
    mod_dict = {'mgrid_currents': crnt_str}

    flf = flfc.flf_wrapper('HSX')
    flf.change_params(mod_dict)

    if False:
        # Plot Surfaces #
        plot = pd.plot_define()

        plt = plot.plt
        fig, ax = plt.subplots(1, 1, tight_layout=True)
        ax.set_aspect('equal')

        flf.read_out_domain(ma_pnt, init_pnt, 3, ax=ax, return_data=False, DeBug=True)
        plt.show()

    if True:
        # Generate DESCUR Input #
        flf.run_descur(ma_pnt, init_pnt, pol_pnts=80, tor_pnts=80, save_path=save_path)
