import os
import h5py as hf
import numpy as np

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import flfTools.flf_operations as flfo

def plot_poincare_surface(poin_path, ax, tor_ang):
    poinData = hf.File(poin_path, 'r')

    key = 'core'
    core = poinData[key][:]
    t_dom = core[0, :, 2]
    dt = t_dom[1] - t_dom[0]

    npts = t_dom.shape[0]
    stps = int(round(2 * np.pi / dt))
    rots = int(round(t_dom[int(npts-1)] / (2*np.pi)))
    idx_stps = np.argmin(np.abs(t_dom - t_dom[0] - tor_ang)) + [int(i*stps) for i in range(rots)]

    points = np.empty((idx_stps.shape[0], 2))
    for i, idx in enumerate(idx_stps):
        points[i] = core[-1,idx,0:2]

    ax.scatter(points[:,0], points[:,1], c='w', s=10)
    
    # plot vessel #
    path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'vessel90.h5')
    with hf.File(path,'r') as hf_file:
        vessel = hf_file['data'][:]
        v_dom = hf_file['domain'][:]

    idx = np.argmin(np.abs(v_dom - tor_ang))
    
    ves_x = vessel[idx,0::,0]
    ves_y = vessel[idx,0::,1]
    ves_z = vessel[idx,0::,2]
    ves_r = np.hypot(ves_x, ves_y)

    ax.plot(ves_r, ves_z, c='k', lw=3)

config_id = '0-1-0'
main_id = 'main_coil_{}'.format(config_id.split('-')[0])
set_id = 'set_{}'.format(config_id.split('-')[1])
job_id = 'job_{}'.format(config_id.split('-')[2])
base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)

# read in lyapunov data #
lyp_tag = '20230825_0001'
# lyp_tag = '20230703_0005'
lyp_path = os.path.join(base_path, 'lyapunov_data', '%s.h5' % lyp_tag)
with hf.File(lyp_path, 'r') as hf_:
    lyp_exp = hf_['lyapunov exponents'][()]
    R_dom = hf_['R domain'][()]
    Z_dom = hf_['Z domain'][()]
    tor_ang = hf_['tor_ang'][()]

# define poincare path #
if config_id == '0-1-0':
    poin_path = os.path.join(base_path, 'poincare.h5')
else:
    poin_path = os.path.join(base_path, '%s.h5' % config_id)

# plot data #
plot = pd.plot_define(fontSize=16, labelSize=20)
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True, figsize=(6.5, 10))
ax.set_aspect('equal')

smap = ax.pcolormesh(R_dom, Z_dom, np.abs(lyp_exp[:,:].T), cmap='viridis', vmin=0, vmax=0.5*np.nanmax(lyp_exp))
cbar = fig.colorbar(smap, ax=ax)
cbar.ax.set_ylabel(r'$\langle\lambda\rangle_n$')

# flfo.plot_poincare(poin_path, tor_ang, fig, ax)
plot_poincare_surface(poin_path, ax, tor_ang)

ax.set_xlabel('R/m')
ax.set_ylabel('Z/m')

# plt.show()
save_path = os.path.join(base_path, 'lyapunov_data', '%s.png' % lyp_tag)
plt.savefig(save_path)
# plt.show()
