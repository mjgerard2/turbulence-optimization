#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 18 14:12:41 2021

@author: michael
"""

import numpy as np
import h5py as hf
import multiprocessing as mp

from scipy.interpolate import interp1d

import flf_class

import os, sys
path = os.path.dirname(os.path.abspath(os.getcwd()))
sys.path.append(path)

import plot_define as pd
import directory_dict as dd

from databaseTools.functions import readCrntConfig
import vmecTools.wout_files.wout_read as wr


def divide(num, div):
    rem = num % div
    base = int(np.floor(num / div))
    base_plus = int(base + 1)

    return [div-rem, base], [rem, base_plus]


def perform_CHI_Test(path, conDircs, rots=500):
    """ Perform CHI-Squared test on the plasma boundary returned byfrom VMEC
    and the FLF code.  In a zero beta equilibrium, this can provide a
    qunatifiable assessment of equilibria deformation due to magnetic
    resonances.

    Parameters
    ----------
    wout : obj
        wout file object returned from the wout_read.py script
    main : array
        Main coil configuration, supplied in fractional percent of standard
        10.722 kA per turn.
    aux : array
        Auxiliary coil configuration, supplied in fractional percent of total
        150.108 kA main coil current.
    s_val : float
        VMEC flux surface coordinate (s) on which to perform the test.
    rots : int (optional)
        Number of toroidal transits to perfomr in the FLF code. Default is 250.
    """
    # Read in Currents #
    s_dom = np.array([0.2, 0.4, 0.6, 0.8])**2
    v_dom = np.array([0, 0.125*np.pi, 0.25*np.pi])
    u_dom = np.linspace(0, 2*np.pi, 161)

    chi_dict = {}
    for cdx, conDirc in enumerate(conDircs):
        print('({0}|{1})'.format(cdx+1, len(conDircs)))
        conID = conDirc.split('.')[0].split('_')[-1]

        # Read in WOUT File #
        wout = wr.readWout(path, name=conDirc)

        # Read in Currents #
        main, aux = readCrntConfig(conID)
        crnt = -10722. * np.r_[main, 14*aux]
        crnt_str = ' '.join(['{}'.format(c) for c in crnt])

        chi_set = np.empty((s_dom.shape[0], v_dom.shape[0]))
        chi_set[:] = np.nan

        for sdx, s_val in enumerate(s_dom):
            # Get VMEC Plasma Boundary #
            wout.transForm_2D_sSec(s_val, u_dom, v_dom, ['R', 'Z'])

            R_wout = wout.invFourAmps['R']
            Z_wout = wout.invFourAmps['Z']

            # Construct FLF Object #
            dphi = 0.125 * np.pi
            stps = int(2 * np.pi / dphi)
            npts = int(rots*stps)

            mod_dict = {'points_dphi': dphi,
                        'n_iter': npts,
                        'mgrid_currents': crnt_str}

            pid = mp.current_process().pid
            namelist = 'flf_{}.namelist'.format(pid)
            in_path = 'point_temp_{}.in'.format(pid)
            out_path = 'point_temp_{}.out'.format(pid)
            bash_file = 'run_flf_{}.sh'.format(pid)

            flf = flf_class.flf_wrapper('HSX', namelist=namelist, in_path=in_path, out_path=out_path, bash_file=bash_file)
            flf.change_params(mod_dict)

            # Read Out FLF Parameters For Indexing #
            rots = int((flf.params['n_iter'] * flf.params['points_dphi']) / (2*np.pi))
            v_stps = np.array([stp for stp in v_dom / flf.params['points_dphi']], dtype=int)
            idx_stps = np.array(np.arange(rots)*stps, dtype=int)

            # Get FLF Plasma Boundary
            init_point = np.array([R_wout[0, 0], Z_wout[0, 0], v_dom[0]])
            points = flf.execute_flf(init_point, DeBug=False)

            # Check For NANs #
            is_nan = np.isnan(points[:, 0])
            not_nan = ~is_nan
            if all(not_nan):
                for vdx, v_val in enumerate(v_dom):
                    # Calculate \Chi^2 for the VMEC and FLF flux surfaces #
                    ma = np.array([np.mean(R_wout[vdx, :]), np.mean(Z_wout[vdx, :])])

                    # Spline Fit FLF Points #
                    flf_pnts = points[v_stps[vdx] + idx_stps, 0:2] - ma
                    flf_phi = np.arctan2(flf_pnts[:, 1], flf_pnts[:, 0])

                    flf_r = np.stack((flf_phi, np.hypot(flf_pnts[:, 0], flf_pnts[:, 1])), axis=1)
                    flf_r = np.array(sorted(flf_r, key=lambda x: x[0]))

                    padIdx = np.r_[np.zeros(2, dtype=int), np.array([flf_r.shape[0]*2]*2, dtype=int)]
                    flf_rPad = np.array([flf_r[-1, 0]-2*np.pi, flf_r[-1, 1], flf_r[0, 0]+2*np.pi, flf_r[0, 1]])
                    flf_r = np.insert(flf_r, padIdx, flf_rPad).reshape(flf_r.shape[0]+2, 2)
                    flf_rFit = interp1d(flf_r[:, 0], flf_r[:, 1])

                    # Spline Fit WOUT Points #
                    wout_pnts = np.stack((R_wout[vdx, :], Z_wout[vdx, :]), axis=1) - ma
                    wout_phi = np.arctan2(wout_pnts[:, 1], wout_pnts[:, 0])

                    wout_r = np.stack((wout_phi, np.hypot(wout_pnts[:, 0], wout_pnts[:, 1])), axis=1)
                    wout_r = np.array(sorted(wout_r, key=lambda x: x[0]))

                    padIdx = np.r_[np.zeros(2, dtype=int), np.array([wout_r.shape[0]*2]*2, dtype=int)]
                    wout_rPad = np.array([wout_r[-1, 0]-2*np.pi, wout_r[-1, 1], wout_r[0, 0]+2*np.pi, wout_r[0, 1]])
                    wout_r = np.insert(wout_r, padIdx, wout_rPad).reshape(wout_r.shape[0]+2, 2)
                    wout_rFit = interp1d(wout_r[:, 0], wout_r[:, 1])

                    # Compare Fits #
                    phi_dom = np.linspace(-np.pi, np.pi, 361)

                    flf_rChk = flf_rFit(phi_dom)
                    wout_rChk = wout_rFit(phi_dom)

                    # chi_r = np.sum((flf_rChk - wout_rChk)**2 / np.abs(flf_rChk))
                    chi_r = 100 * np.mean(2*np.abs(flf_rChk - wout_rChk)/(flf_rChk + wout_rChk))
                    chi_set[sdx, vdx] = chi_r

        chi_dict[conDirc] = chi_set

    return chi_dict


if __name__ == '__main__':
    # Set of Configurations to Perform Chi-Squared Test #
    path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'geomdir', 'wout_nc')
    # path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0')
    chi_path = os.path.join('/home', 'michael', 'Desktop', 'paper_repos', 'linear_tem_in_qhs', 'data_files', 'chi-difference_test_QHS_roa_0p2_0p4_0p6_0p8.h5')
    if os.path.isdir(path) and not os.path.exists(chi_path):
        # Separate Jobs by CPU Count #
        cpu_cnt = mp.cpu_count() - 2
        conPath = [f.name for f in os.scandir(path)]

        set_1, set_2 = divide(len(conPath), cpu_cnt)

        conPath_pool = []
        for i in range(set_1[0]):
            idxBeg = i * set_1[1]
            idxEnd = (i + 1) * set_1[1]
            conPath_pool.append([path, conPath[idxBeg:idxEnd]])

        for j in range(set_2[0]):
            jdxBeg = idxEnd + j * set_2[1]
            jdxEnd = idxEnd + (j + 1) * set_2[1]
            conPath_pool.append([path, conPath[jdxBeg:jdxEnd]])

        # Perform Checks #
        with mp.Pool() as pool:
            results = pool.starmap(perform_CHI_Test, conPath_pool)

        # Save Data #
        chi_data = {}
        for res in results:
            for key in res:
                chi_data[key] = res[key]

        with hf.File(chi_path, 'w') as hf_file:
            for key in chi_data:
                conID = key.split('.')[0].split('_')[-1]
                hf_file.create_dataset(conID, data=chi_data[key])
