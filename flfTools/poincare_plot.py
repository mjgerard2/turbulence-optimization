import os, sys
import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.pyplot as plt

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.wout_read as wr


tor_ang = 0.25*np.pi

# Get Iota Profile #
wout_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'HSX_Hill_Well_Configuration_Convergence', 'Well Configurations', 'Well 8p')
wout = wr.readWout(wout_file, name='wout_Well8p0.nc')

s_grid = wout.s_grid
iota = wout.iota(s_grid)

# Get Poincare Data #
# poin_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_3', 'job_84', 'poincare.h5')
poin_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'HSX_Hill_Well_Configuration_Convergence', 'Well Configurations', 'Well 8p', 'poincare.h5')
poin_data = {}
with hf.File(poin_file, 'r') as hf_:
    t_dom = hf_['core'][0, :, 2]
    dt = t_dom[1] - t_dom[0]

    npts = t_dom.shape[0]
    stps = int(round(2 * np.pi / dt))
    rots = int(round(t_dom[int(npts-1)] / (2*np.pi)))

    idx_stps = np.argmin(np.abs(t_dom - tor_ang)) + [int(i*stps) for i in range(rots)]

    for key in hf_:
        poin_data[key] = hf_[key][:, idx_stps, :]

# Get Vessel Wall #
path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'vessel90.h5')
with hf.File(path,'r') as hf_file:
    vessel = hf_file['data'][:]
    v_dom = hf_file['domain'][:]

idx = np.argmin(np.abs(v_dom - tor_ang))
ves_x = vessel[idx, :, 0]
ves_y = vessel[idx, :, 1]
ves_z = vessel[idx, :, 2]
ves_r = np.hypot(ves_x, ves_y)

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axis #
fig = plt.figure(tight_layout=True, figsize=(14, 6))
gs = mpl.gridspec.GridSpec(1, 3)

ax1 = fig.add_subplot(gs[0:2])
ax2 = fig.add_subplot(gs[2])

ax1.set_aspect('equal')

# Plot Poincare Data #
for key in poin_data:
    ax1.scatter(poin_data[key][:, :, 0], poin_data[key][:, :, 1], c='k', s=1)
ax1.plot(ves_r, ves_z, c='k', lw=4)

# Plot Iota Profile #
ax2.plot(np.sqrt(s_grid), iota, c='k')
# ax2.plot([0, 1], [1.]*2, c='k', ls='--')
ax2.plot([0, 1], [16./15]*2, c='k', ls='-.')
ax2.plot([0, 1], [12./11]*2, c='k', ls=':')
ax2.plot([0, 1], [8./7]*2, c='k', ls='--')
# ax2.plot([0, 1], [12./10]*2, c='k', ls='-.')
# ax2.plot([0, 1], [16./13]*2, c='k', ls=':')
# ax2.plot([0, 1], [4./3]*2, c='k', ls=':')

# Axis Text #
gap = 0.002
# ax2.text(0.25, (4./4)+gap, '4/4', fontsize=16)
ax2.text(0.25, (16./15)+gap, '16/15', fontsize=16)
ax2.text(0.25, (12./11)+gap, '12/11', fontsize=16)
ax2.text(0.25, (8./7)-3*gap, '8/7', fontsize=16)
# ax2.text(0.25, (12./10)+.002, '12/10', fontsize=16)
# ax2.text(0.25, (16./13)-.006, '16/13', fontsize=16)
# ax2.text(0.25, (4./3)+.002, '4/3', fontsize=16)

# Axis Limits #
ax1.set_xlim(0.83, 1.28)
ax1.set_ylim(-.17, .17)
ax2.set_xlim(0, 1)

# Axis Labels #
ax1.set_xlabel(r'$R \ (m)$')
ax1.set_ylabel(r'$Z \ (m)$')

ax2.set_xlabel(r'$r/a$')
ax2.set_ylabel(r'$\iota/2\pi$')

# Axis Grids #
ax2.grid()

# Show/Save
# plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'for_henrique', 'poincare_well_8p0.png')
plt.savefig(save_path)
