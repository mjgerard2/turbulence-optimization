import os
import subprocess

import h5py as hf
import numpy as np

from sklearn.linear_model import LinearRegression

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import flfTools.flf_class as flfc

from databaseTools import functions
from flfTools.exp_params import params
from multiprocessing import Process, Queue, cpu_count

def calc_lyp_exp(run_dict, ax):
    dx = run_dict['dx']
    tor_ang = run_dict['tor_ang']
    dstp = run_dict['dstp']
    rots = run_dict['rots']
    npts = run_dict['npts']
    d0 = run_dict['d0']
    mod_dict = run_dict['mod_dict']
    init_point = run_dict['init_point']

    flf = flfc.flf_wrapper('HSX')
    flf.change_params(mod_dict)
    flf.set_transit_parameters(dstp, rots)

    pnt_ang = np.linspace(0, 2*np.pi, npts, endpoint=False)
    x_data = np.linspace(0, rots, flf.params['n_iter']).reshape((-1, 1))

    pnt0 = flf.execute_flf(init_point, quiet_mode=True)
    if np.isnan(pnt0).any():
        raise ValueError('NaNs along phase point trajectory.')

    # calculate exponents #
    clrs = ['tab:blue', 'tab:orange', 'tab:green']
    for k in range(npts):
        d_shft = d0*np.array([np.cos(pnt_ang[k]), np.sin(pnt_ang[k]), 0])
        pnts = flf.execute_flf(init_point+d_shft, quiet_mode=True)
        if np.isnan(pnts).any():
            raise ValueError('NaNs along phase point trajectory.')

        dist = np.linalg.norm(pnt0[:, 0:2] - pnts[:, 0:2], axis=1)/d0
        zero_idx = np.where(dist == 0)[0]
        dist[zero_idx] = 1e-10
        y_data = np.log(dist)
        model = LinearRegression().fit(x_data, y_data)
        lyp_exp = model.coef_[0]

        ax.plot(x_data, dist, c=clrs[k])
        ax.plot(x_data, np.exp(model.coef_[0]*x_data+model.intercept_), ls='--', c=clrs[k], label=r'$\lambda_{0} = {1:0.2f}$'.format(k, lyp_exp))

config_id = '0-1-0'
main, aux = functions.readCrntConfig(config_id)
crnt = -10722. * np.r_[main, 14*aux]
mod_dict = {'mgrid_currents': ' '.join(['{}'.format(c) for c in crnt]), 
            'mgrid_file': os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'mgrid_res1p0cm_30pln.nc')}

run_dict = {'dx': 1e-3, # spatial separation of sample points in meters
            'tor_ang': 0.25*np.pi, # toroidal angle of cross section
            'dstp': 1,  # 5.625, # angular step size in field line following
            'rots': 25, # number of toroidal rotations
            'npts': 3, # number of shifted points around the sample points
            'd0': 1e-4, # distance of shift for shifted points in meters
            'mod_dict': mod_dict}

# define plotting parameters #
plot = pd.plot_define(lineWidth=2)
plt = plot.plt
fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(8, 10))

run_dict['init_point'] = np.array([1.05, 0., run_dict['tor_ang']])
calc_lyp_exp(run_dict, axs[0])

run_dict['init_point'] = np.array([1.23, 0., run_dict['tor_ang']])
calc_lyp_exp(run_dict, axs[1])

# axis labels #
axs[1].set_xlabel(r'$\Delta\varphi/2\pi$')
axs[0].set_ylabel(r'$\delta_{\Delta\varphi} / \delta_0$')
axs[1].set_ylabel(r'$\delta_{\Delta\varphi} / \delta_0$')

axs[0].set_title(r'$\mathbf{x}_0 = (1.05/\mathrm{m})\hat{\mathbf{e}}_R$')
axs[1].set_title(r'$\mathbf{x}_0 = (1.23/\mathrm{m})\hat{\mathbf{e}}_R$')

# axis limits #
axs[0].set_xlim(0, run_dict['rots'])

# axis scale #
axs[0].set_yscale('log')
axs[1].set_yscale('log')

# axis grids and legends #
axs[0].grid(which='both')
axs[1].grid(which='both')

axs[0].legend(ncol=2, fontsize=14)
axs[1].legend(ncol=2, fontsize=14)

# plt.show()
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'personal_meetings', 'HSX_divertor', '20230811', 'figures', 'continuous_lyp_exmaple.png')
plt.savefig(save_path)
