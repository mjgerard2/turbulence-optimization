#!/bin/python3
import os, sys
import numpy as np

script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_dir, '..'))

import sfincsTools.readScan_Er as scanEr


class read_rN_scan():
    """ A data analysis class for reading SFINCS outputs from scanType 5.

    ...

    Attributes
    ----------
    path : str
        The path to the directory where the profile scan results are stored.

    Methods
    -------
    plot_data(ax, data_key)
        Plot the specified profile data.
    """

    def __init__(self, path):
        rN_dirs = [f.name for f in os.scandir(path) if f.name[0:2] == 'rN']

        cnt = 0
        rN_vals = []
        rN_data = []
        for rN_dir in rN_dirs:
            rN_path = os.path.join(path, rN_dir)
            ErData = scanEr.readErScan(rN_path)
            try:
                ErData.interp_ambipolar_data()
                if ErData.ambi_data.shape[2] != 1:
                    ErData.ambi_data[:, :, 0] = np.mean(ErData.ambi_data, axis=2)
                rN_data.append(ErData.ambi_data[:, :, 0])
                rN_vals.append(np.array([float(rN_dir.split('_')[1]), cnt]))
                cnt += 1

            except ValueError:
                print('   No ambipolar field found in '+rN_dir)

        rN_vals_sorted = np.array(sorted(rN_vals, key=lambda x: x[0]))
        self.rN_data = []
        for idx in rN_vals_sorted[0::, 1]:
            self.rN_data.append(rN_data[int(idx)])

        self.rN_data = np.array(self.rN_data)
        self.rN_vals = rN_vals_sorted[:, 0]

    def get_data_idx(self, data_key):
        """ Get the appropriate index for the profile data corresponding to
        the specified data key.

        Parameters
        ----------
        data_key : str
            Profile data idex being retrieved.  Options are listed below.
                ambi Er
                boot flow
                part flux
                heat flux
                part diff
                heat diff

        Raises
        ------
        KeyError
            The data key provided was not one of the available data keys
            listed above.
        """
        if data_key == 'ambi Er':
            self.data_idx = 0
            self.ylab = r'Ambipolar $E_r$ [$kV/m$]'
        elif data_key == 'boot flow':
            self.data_idx = 1
            self.ylab = r'bootstrap flow [$nA m^{-2}$]'
        elif data_key == 'part flux':
            self.data_idx = 2
            self.ylab = r'$\langle\Gamma_{part}\rangle \ \left[10^{20} \, m^{-2}s^{-1}\right]$'
        elif data_key == 'heat flux':
            self.data_idx = 3
            self.ylab = r'$\langle Q_{\mathrm{es}} \rangle$ [kW m$^{-2}$]'
        elif data_key == 'part diff':
            self.data_idx = 4
            self.ylab = r'$\langle D_{part}\rangle \ \left[m^{2}s^{-1}\right]$'
        elif data_key == 'heat diff':
            self.data_idx = 5
            self.ylab = r'$\langle D_{heat}\rangle \ \left[m^{2}s^{-1}\right]$'
        elif data_key == 'current':
            self.data_idx = 6
            self.ylab = r'bootstrap current [$nA m^{-2}$]'
        else:
            raise KeyError(data_key+' is not a valid data key.')

    def plot_data(self, ax, data_key):
        """ Plot the specified profile data.

        Parameters
        ----------
        ax : obj
            Matplotlib axis object on which the plot will be made.
        data_key : str
            Profile data to be plotted.  Options are listed below.
                boot flow
                part flux
                heat flux
                part diff
                heat diff
                ambi Er
        """
        self.get_data_idx(data_key)

        if data_key != 'ambi Er':
            ax.plot(self.rN_vals, self.rN_data[:, self.data_idx, 0], ls='--', marker='s', ms=10, markerfacecolor='None', c='k', label='Ion')
            ax.plot(self.rN_vals, self.rN_data[:, self.data_idx, 1], ls='-.', marker='o', ms=10, markerfacecolor='None', c='tab:red', label='Electron')
        else:
            ax.plot(self.rN_vals, self.rN_data[:, self.data_idx, 0], marker='s', ms=10, markerfacecolor='None', c='k', label='SFINCS')

        key_list = ['part flux', 'heat flux', 'part diff', 'heat diff']
        if any([ikey == data_key for ikey in key_list]):
            ax.set_ylim(0, 1.1*np.max(self.rN_data[:, self.data_idx, 0:2]))
        else:
            data_max = 1.1*np.max(np.abs(self.rN_data[:, self.data_idx, 0:2]))
            ax.set_ylim(-data_max, data_max)
        ax.set_xlim(0, 1)

        ax.set_xlabel(r'$r/a$')
        ax.set_ylabel(self.ylab)


if __name__ == '__main__':
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    # Dircetory path to Er data #
    rN_dirc = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'sfincs_data', '897-1-0')

    scan = read_rN_scan(rN_dirc)

    # Plotting Parameters #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 16}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 20
    mpl.rcParams['lines.linewidth'] = 2

    # Plotting Axes #
    fig, ax = plt.subplots(1, 1, tight_layout=True)

    scan.plot_data(ax, 'ambi Er')
    ax.grid()

    plt.show()
