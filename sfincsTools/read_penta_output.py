# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 16:46:01 2021

@author: bgeiger3
"""
import numpy as np
import scipy.constants as consts
def read_penta_output():
    file='PENTARun111721prof.txt'
    f = open(file, 'r')
    lines=f.readlines()[2:]
    nr=len(lines)
  
    ra=np.zeros(nr)
    qi=np.zeros(nr)    
    qe=np.zeros(nr)   
    Er=np.zeros(nr)        
    for i,lin in enumerate(lines):
        dummy=lin.split()

        ra[i]=float(dummy[0].strip())
        Er[i]=float(dummy[1].strip())  #kV/m      
        qi[i]=float(dummy[2].strip())   
        qe[i]=float(dummy[3].strip())   
    f.close()
    qi*=1.e20
    qi*=consts.e*1.e3 ## [W/m^2]
    qe*=1.e20
    qe*=consts.e*1.e3 ## [W/m^2]    
    
    return(ra,qi,qe,Er)
    
import scipy.io as sio   
def read_penta_mat(file):   
    penta = sio.loadmat(file,squeeze_me=True, struct_as_record=False)
    print(penta.keys())
    ra=penta['roa_vals']
    Ti=penta['pprof_info_allsurf'].Ti_used
    Te=penta['pprof_info_allsurf'].Te_used    
    
    
    # how many roots

    try:
        index=0
        qe=penta['QoT_e'][:,index]*consts.e*Te ## [W/m^2]
        qi=penta['QoT_i'][:,index]*consts.e*Ti ## [W/m^2]
        gamma_i=penta['gamma_i'][:,index]/1.e20 # [10^20 particles/m^2/s]
        gamma_e=penta['gamma_e'][:,index]/1.e20 # [10^20 particles/m^2/s]
        Er=penta['Er_ambi'][:,0] ##[V/m]
    except:
        qe=penta['QoT_e']*consts.e*Te ## [W/m^2]
        qi=penta['QoT_i']*consts.e*Ti ## [W/m^2]
        gamma_i=penta['gamma_i']/1.e20 # [10^20 particles/m^2/s]
        gamma_e=penta['gamma_e']/1.e20 # [10^20 particles/m^2/s]
        Er=penta['Er_ambi'] ##[V/m]    
        
    ni=penta['pprof_info_allsurf'].ni_used
    dTidr=penta['pprof_info_allsurf'].dTidr_used
    dTedr=penta['pprof_info_allsurf'].dTedr_used   
    dnidr=penta['pprof_info_allsurf'].dnidr_used
    dnedr=penta['pprof_info_allsurf'].dnedr_used    
    ne=penta['pprof_info_allsurf'].ne_used    

    

    
    
    
    ##
    # q= -ne gradT chi + 5/2 gamma T
    ##-- > chi= -(q-5/2* gamma*T)/(ne gradT)
    # gamma= -gradn chi
    ##--> q = -ne gradT chi - 5/2 gradn chi T
    ## chi = -q/(ne gradT + 5/2 gradn T)
    ## calcualte chi_i and chi_e
    ##   J/s m^-2  m^3    1/(J/m)
    

    #chi_i=-(qi-5/2*gamma_i*Ti*consts.e)/(ni*dTidr*consts.e) # m^2/s
    #chi_e=-(qe-5/2*gamma_e*Te*consts.e)/(ne*dTedr*consts.e) # m^2/s    
    #chi_i=-(qi)/(ni*dTidr*consts.e) # m^2/s    
    #chi_i=-(qi)/((ni*dTidr*consts.e)+(5/2*dnidr*Ti*consts.e)) # m^2/s
    #chi_e=-(qe)/((ne*dTedr*consts.e)+(5/2*dnedr*Te*consts.e))# m^2/s


    chi_i=-(qi)/(ni*dTidr*consts.e) # m^2/s
    chi_e=-(qe)/(ne*dTedr*consts.e) # m^2/s    
    index=(qe < 10.e3) & (Er < 20.e3)
  

    D_e=-gamma_e/dnedr*1.e20 
    D_i=-gamma_i/dnidr*1.e20 

    return(ra[index],Te[index],Ti[index],ne[index],ni[index],qe[index],qi[index],chi_e[index],chi_i[index],Er[index],gamma_e[index],gamma_i[index],D_e[index],D_i[index])



def calc_chi(reff,q,T,n):  
    ## calculate diffusivity
    ## requires 
    ## q in W/m^2
    ## T in eV
    ## n in m^-3
    ##  J/s m^-2  m^3                  1/J      m^1    
    chi=(q)/(-n*np.gradient(T*consts.e,reff)) # m^2/s
    return(chi)
    
    
if __name__ == "__main__":  
    import matplotlib.pyplot as plt
    cmap = plt.get_cmap("tab10")
    plt.close('all')
    import matplotlib  
    matplotlib.rcParams.update({'font.size': 20})
    
   
    ## Er
    fig1, ax1 = plt.subplots()
    ## Chi
    fig2, ax2 = plt.subplots()
    ## Te,Ti, ne, ni
    fig3, (ax3a,ax3b) = plt.subplots(nrows=2, sharex=True,figsize=([8.,8.]))
    ## Heat fluxes
    fig4, ax4 = plt.subplots()
    ## Particle fluxes
    fig5, ax5 = plt.subplots()      
    ## Particle diffusion
    fig6, ax6 = plt.subplots()     

    file='PENTA_output_preupgrade120921Noneutrals.mat'
    file='PENTA_output_preupgrade012122DKES'

    #file='PENTA_output_preupgrade121221Noneutrals.mat' 
    #file='PENTA_output_preupgrade121521SNNoneutrals.mat' 
    #file='PENTA_output_preupgrade121521DKESNoneutrals.mat' 
    ra,Te,Ti,ne,ni,qe,qi,chi_e_penta,chi_i_penta,Er,gamma_e,gamma_i,D_e,D_i=read_penta_mat(file)


    ## plot Er
    ax1.plot(ra,Er/1.e3,label=r'pre upgrade',color='k', ls='--', marker='s', ms=10, markerfacecolor='None')
    #for i in range(2):
    #    ax1.plot(ra,Er[:,i+1],color='k',linestyle='--')  

    ## plot CHI
    ax2.plot(ra,chi_i_penta,label=r'$\chi_i$ pre',color=cmap(1), ls='--', marker='s', ms=10, markerfacecolor='None')  
    ax2.plot(ra,chi_e_penta,label=r'$\chi_e$ pre',color=cmap(0), ls='--', marker='s', ms=10, markerfacecolor='None')
    ## plot profiles
    ax3a.plot(ra,ne,label=r'$n_i$ pre upgrade',color=cmap(1), ls='--', marker='s', ms=10, markerfacecolor='None')
    ax3a.plot(ra,ni,label=r'$n_e$ pre upgrade',color=cmap(0), ls='--', marker='s', ms=10, markerfacecolor='None')
    

    ax3b.plot(ra,Ti,label=r'$T_i$ pre upgrade',color=cmap(1), ls='--', marker='s', ms=10, markerfacecolor='None')
    ax3b.plot(ra,Te,label=r'$T_e$ pre upgrade',color=cmap(0), ls='--', marker='s', ms=10, markerfacecolor='None')

    ax4.plot(ra,qi/1.e3,label=r'$q_i$ pre upgrade',color=cmap(1), ls='--', marker='s', ms=10, markerfacecolor='None')
    ax4.plot(ra,qe/1.e3,label=r'$q_e$ pre upgrade',color=cmap(0), ls='--', marker='s', ms=10, markerfacecolor='None')

    ax5.plot(ra,gamma_i,label=r'$\Gamma_i$ pre upgrade',color=cmap(1), ls='--', marker='s', ms=10, markerfacecolor='None') 
    ax5.plot(ra,gamma_e,label=r'$\Gamma_e$ pre upgrade',color=cmap(0), ls='--', marker='s', ms=10, markerfacecolor='None') 

    ax6.plot(ra,D_i,label=r'$D_i$ pre upgrade',color=cmap(1), ls='--', marker='s', ms=10, markerfacecolor='None') 
    ax6.plot(ra,D_e,label=r'$D_e$ pre upgrade',color=cmap(0), ls='--', marker='s', ms=10, markerfacecolor='None') 



      
    file='PENTA_output_postupgrade120921Noneutrals.mat' 
    file='PENTA_output_postupgrade012122DKES'
    #file='PENTA_output_postupgrade121221Noneutrals.mat' 
    #file='PENTA_output_preupgrade121221Noneutrals.mat' 
    #file='PENTA_output_postupgrade121521SNNoneutrals.mat' 
    #file='PENTA_output_postupgrade121521DKESNoneutrals.mat'
    #file='PENTA_output_upgrade111721Noneutrals.mat'
    ra,Te,Ti,ne,ni,qe,qi,chi_e_penta,chi_i_penta,Er,gamma_e,gamma_i,D_e,D_i=read_penta_mat(file)
    ## plot Er
    ax1.plot(ra,Er/1.e3,label=r'post upgrade',color='k', ls='-.', marker='o', ms=10, markerfacecolor='None') 
    #for i in range(2):
    #    ax1.plot(ra,Er[:,i+1],color='k')  

    
    ## plot CHI
    ax2.plot(ra,chi_i_penta,label=r'$\chi_i$ post upgrade',color=cmap(1), ls='-.', marker='o', ms=10, markerfacecolor='None')   
    ax2.plot(ra,chi_e_penta,label=r'$\chi_e$ post upgrade',color=cmap(0), ls='-.', marker='o', ms=10, markerfacecolor='None') 
    ## plot profiles
    ax3a.plot(ra,ne,label=r'$n_i$ post upgrade',color=cmap(1), ls='-.', marker='o', ms=10, markerfacecolor='None') 
    ax3a.plot(ra,ni,label=r'$n_e$ post upgrade',color=cmap(0), ls='-.', marker='o', ms=10, markerfacecolor='None') 
    

    ax3b.plot(ra,Ti,label=r'$T_i$ post upgrade',color=cmap(1), ls='-.', marker='o', ms=10, markerfacecolor='None') 
    ax3b.plot(ra,Te,label=r'$T_e$ post upgrade',color=cmap(0), ls='-.', marker='o', ms=10, markerfacecolor='None') 
    
     
    ax4.plot(ra,qi/1.e3,label=r'$q_i$ post upgrade',color=cmap(1), ls='-.', marker='o', ms=10, markerfacecolor='None') 
    ax4.plot(ra,qe/1.e3,label=r'$q_e$ post upgrade',color=cmap(0), ls='-.', marker='o', ms=10, markerfacecolor='None') 



    ax5.plot(ra,gamma_i,label=r'$\Gamma_i$ post upgrade',color=cmap(1), ls='-.', marker='o', ms=10, markerfacecolor='None') 
    ax5.plot(ra,gamma_e,label=r'$\Gamma_e$ post upgrade',color=cmap(0), ls='-.', marker='o', ms=10, markerfacecolor='None') 

    ax6.plot(ra,D_i,label=r'$D_i$ post upgrade',color=cmap(1), ls='-.', marker='o', ms=10, markerfacecolor='None') 
    ax6.plot(ra,D_e,label=r'$D_e$ post upgrade',color=cmap(0), ls='-.', marker='o', ms=10, markerfacecolor='None') 



    
    ax1.grid()
    #ax1.plot([0,1],[0,0],linestyle='--',color='k')
    #ax1.set_ylim([-10,40])
    ax1.set_ylabel(r'E$_r$ [kV/m]')
    #ax1.set_ylabel(r'$10^{19}\mathrm{m}^{-3}$')
    #ax1.legend(loc=1)    
    ax1.set_xlim([0,1])  
    ax1.set_xlabel('r/a')
    ax1.legend(loc=1,fontsize=12) 
    fig1.tight_layout() 
    fig1.savefig('penta_Er_profiles.png')          
    
    ax2.grid()
    ax2.set_ylim([0,1])
    ax2.set_xlabel('r/a')
    ax2.set_ylabel(r'$\chi$ [m$^2$/s]')
    ax2.set_ylim([0,2])

    ax2.legend(loc=1,fontsize=12) 
    fig2.tight_layout() 
    fig2.savefig('penta_diffusivity_profiles.png')        
        

    #ax3a.set_ylim([0,2])
    ax3a.set_ylabel(r'n [m$^-3$]')
    ax3a.set_xlim([0,1])
    ax3a.legend(loc=1,fontsize=12)   
    
    ax3b.set_ylabel(r'T [eV]')
    ax3b.set_xlim([0,1])
    ax3b.legend(loc=1,fontsize=12)      
    
    
    fig3.tight_layout()     
    fig3.savefig('penta_input_profiles.png')    
    
    #ax3a.set_ylim([0,2])
    ax4.set_ylabel(r'q [kW/m$^2$]')
    ax4.set_ylim(bottom=0) 
    ax4.set_xlabel('r/a')
    ax4.set_xlim([0,1])
    ax4.set_ylim([0,2.5])    
    ax4.legend(loc=1,fontsize=12)   
    fig4.tight_layout()     
    fig4.savefig('heat_flux_profiles.png')    
    
        
    #ax3a.set_ylim([0,2])
    ax5.set_ylabel(r'$\Gamma\, [10^{20}/m^2/s$]')
    ax5.set_ylim(bottom=0) 
    ax5.set_xlabel('r/a')  
    ax5.legend(loc=1,fontsize=12)   
    fig5.tight_layout()     
    fig5.savefig('particle_flux_profiles.png')    
    
    ax6.set_ylabel(r'$D\, [m^2/s$]')
    ax6.set_ylim(bottom=0) 
    ax6.set_xlabel('r/a')  
    ax6.legend(loc=1,fontsize=12)   
    fig6.tight_layout()     
    fig6.savefig('particle_diffusion_profiles.png')    
    
                
    asdf
    plt.figure()
    plt.plot(ra,Er/1.e3,label=r'E$_r$ - Penta')  
    plt.ylim([-10.,10.])
    plt.xlim([0.,1])
    plt.xlabel('r/a')  
    plt.ylabel(r'$E_r\,[kV/m]$') 
    

    plt.tight_layout() 
    plt.legend(loc=1,fontsize=18) 
    plt.savefig('penta_Er_profiles.png')    
        
   
    
    plt.figure()
    plt.plot(ra,qe/1.e3,label=r'q$_e$ - Penta')   
    plt.plot(ra,qi/1.e3,label=r'q$_i$ - Penta')
    plt.ylim([0.,10.0])
    plt.xlim([0.,1])
    plt.xlabel('r/a')  
    plt.ylabel(r'$q_\mathrm{i}\,[kW/m^2]$') 
    
    plt.tight_layout() 
    plt.legend(loc=1,fontsize=18) 
    plt.savefig('penta_heat_flux_profiles.png')    
    
    
    ## -----------------------------------------------------------
    ## Get Equilibrium information from a VMEC "wout" file
    ## -----------------------------------------------------------
    #from get_VMEC_flux_surface_area_and_volume import get_VMEC_flux_surface_area_and_volume
    #file='data/wout_HSX_main_opt0.nc'
    #rminor,rmajor,reff_HSX,surface_HSX,volume_HSX=get_VMEC_flux_surface_area_and_volume(file)


    
    ## read in input kinetic profiles
    file='penta_input_profiles_111721.dat'
    #file='HSX_profiles_P=20kW_n=2e19m-3_1.dat'
    file='HSX_profiles_P=20kW_n=0e19m-3_1.dat'  
    file='HSX_profiles_P=30kW_n=2e19m-3_withNC.dat'  
    file='HSX_profiles_P=20kW_n=0e19m-3_withNC.dat'      
    #file='P=30kW_n=2e19m-3_1_upgrade_prediction.dat'
    f = open(file, 'r')
    lines=f.readlines()
    lin=lines[1]
    dummy=lin.split()
    nr=int(dummy[0])
    data=np.zeros((5,nr))
    jstart=3  
    for ii in range(5):
        vtmp=[]
        for jlin, lin in enumerate(lines[jstart::]):
            vtmp += lin.split()
            if len(vtmp) == int(nr):
                jstart += jlin + 2
                break
        data[ii,:] = np.array(vtmp, dtype=np.float64)
    ra_input=data[0,:]
    ne_input=data[1,:]    
    te_input=data[3,:]
    ti_input=data[4,:]   
    
    
    rmajor=1.2
    rminor=0.12

    nr_c=nr-1
    ra_c=ra_input[0:-1]+0.5*np.diff(ra_input)
    
    reff=ra_input*0.12 ## 12 cm a_eff
    reff_c=ra_c*rminor ## 12 cm a_eff
    Te=np.interp(ra_c,ra_input,te_input*1.e3) ## eV
    Ti=np.interp(ra_c,ra_input,ti_input*1.e3) ## eV    
    ne=np.interp(ra_c,ra_input,ne_input*1.e20) ## m^-3
    ni=ne

    qi_c=np.interp(ra_c,ra,qi)
    qe_c=np.interp(ra_c,ra,qe)

    
    
    
    
    ## ---------------------------------------
    ## --- plot exp. chivalues
    ## ---------------------------------------    
    kappa=1.25
    vol=np.pi*reff**2 * 2.* np.pi*rmajor
    asurf=2.*np.pi*reff * 2.* np.pi*rmajor*kappa
    asurf=asurf[1::]
    
   
    #asurf=np.interp(reff,reff_HSX,surface_HSX)
    #asurf=asurf[1::] 
    #plt.plot(ra_c,asurf)

    dvol=np.diff(vol)

    ## get heat-flux assuming core deposition
    ## consider ECRH depositin within r/a =0.2
    power=20.e3 # W    

    P_ecrh_dens=np.zeros(nr_c)
    index=ra_c <0.2
    P_ecrh_dens[index]+=power/np.sum(dvol[index]) #[W/m^3]
    
    
    Lambda=np.zeros(nr_c)
    index=Te > 0
    Lambda[index] = 15.9-.5*np.log(ne[index]/1.e19)+np.log(Te[index]/1.e3)
    Zi=1.
    Ai=1.
    P_ei=0.00246*Lambda*ne/1.e19*ni/1.e19*Zi**2 * (Te-Ti)/1.e3 / ( Ai*(Te/1.e3)**(1.5))*1.e6 # [W/m^3]



    ##calc ion heat flux
    qi_exp=np.cumsum((P_ei)*dvol)/asurf       
    plt.plot(ra_c,qi_exp/1.e3,color=cmap(1),linestyle='--')
    ##calc electron heat flux
    qe_exp=np.cumsum((P_ecrh_dens-P_ei)*dvol)/asurf   
    #qe_exp=np.cumsum((P_ecrh_dens)*dvol)/asurf       
    plt.plot(ra_c,qe_exp/1.e3,color=cmap(0),linestyle='--')

    
    
    
    ## ---------------------------------------------------------------
    ## --- plot ISS04, Penta, bohm and gyrobohm chi_i and chi_e 
    ## ---------------------------------------------------------------
    plt.figure()  
    ## PENTA
    plt.plot(ra,chi_e_penta,color=cmap(0),label=r'$\chi_e$ - Penta')
    plt.plot(ra,chi_i_penta,color=cmap(1), label=r'$\chi_i$ - Penta')
    ## ISS04
    chi_e_ISS04=calc_chi(reff_c,qe_exp,Te,ne)
    chi_i_ISS04=calc_chi(reff_c,qi_exp,Ti,ni)

    plt.plot(ra_c,chi_e_ISS04,color=cmap(0),linestyle='--',label=r'$\chi_e$ - ISS04')   
    plt.plot(ra_c,chi_i_ISS04,color=cmap(1),linestyle='--',label=r'$\chi_i$ - ISS04')

    
    '''
    ## PENTA
    chi_e=calc_chi(reff_c,qe_c,Te,ne)
    chi_i=calc_chi(reff_c,qi_c,Ti,ni)
    plt.plot(ra_c,chi_i,'x',color=cmap(0), label=r'$\chi_i$ - Penta')
    plt.plot(ra_c,chi_e,'x',color=cmap(1),label=r'$\chi_e$ - Penta')
    '''
  
    '''
    ## Bohm Diffusion from Waltz
    ## chi bohm=cs*rho_s = cs**2 /omega = q* T_e/m_i * m_i/(q*B) =  T_e/B
    ## now use pre-factor from NRL
    B=1.25
    chi_B = 1./16* Te/B
    plt.plot(ra_c,chi_B,color='green',label=r'$\chi_B$') 
    
    ## Gyro Bohm
    ## ----------------------------------- 
    m_i=1.*consts.atomic_mass
    q=1.*consts.e
    ## ion cyclotron frequency
    omega_i = q*B/m_i
    ## thermal ion velocity
    vi=np.sqrt(Ti*consts.e/m_i)
    ## ion larmor radius
    rho_i= (vi / omega_i) ## sqrt(q T/m) *m/qB ~ sqrt(T)/B
    ## gyro-Bohm
    ## --> rho_star*chi_B
    chi_GB_i = rho_i/rminor * chi_B  
    plt.plot(ra_c,chi_GB_i,label=r'$\chi_{iGB}$',linestyle='dashed',color=cmap(0)) 
    
    
    ## electron cyclotron frequency
    omega_e = q*B/consts.m_e
    ## thermal electron velocity
    ve=np.sqrt(Te*consts.e/consts.m_e)
    rho_e= (ve / omega_e) ## sqrt(q Te/m) *m/qB ~ sqrt(T)/B
    ## gyro-Bohm
    chi_GB_e = rho_e/rminor * chi_B    
    
    plt.plot(ra_c,chi_GB_e,label=r'$\chi_{eGB}$',linestyle='dashed',color=cmap(1)) 
    ''' 
    
    plt.ylim([0.001,20])
    plt.xlim([0,1.0])  
    plt.xlabel('r/a')  
    plt.ylabel(r'$\chi\,[m^2/s]$')  
    plt.tight_layout() 
    plt.legend(loc=1,fontsize=18) 
    plt.savefig('penta_heat_diffusivity.png')    
    

    
    
    
    plt.figure()
    plt.plot(ra_c,ne/1.e19)
    plt.xlim([0,1.0])  
    plt.xlabel('r/a')   
    plt.ylim(bottom=0)
    plt.ylabel(r'n [$10^{19}/m^3$]')      
    plt.tight_layout()
    
        
    plt.figure()
    plt.plot(ra_c,Te/1.e3)
    plt.plot(ra_c,Ti/1.e3)
    plt.xlim([0,1.0])  
    plt.xlabel('r/a')  
    plt.ylim(bottom=0)
    plt.ylabel('T [keV]')      
    plt.tight_layout()
    

    #chi_i_ISS04=np.ones(nr_c)
    #ni=np.ones(nr_c)*2.e19

    dTdr=-(qi_exp/consts.e)/chi_i_ISS04/ni 
    
    ## temperature derivative
    ti2=np.zeros(nr_c)
    for i in range(nr_c):
        index=np.arange(i+1)
        ti2[i]=np.trapz(dTdr[index],reff_c[index])
    ti2-=ti2[-1] ## make it zero at r/a=1
    ti2/=1.e3 ## convert to keV
   
    ti2+=0.05
    plt.plot(ra_c,ti2,linestyle='--')
    

    
    
    
    