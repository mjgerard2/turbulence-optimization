import os, sys
import numpy as np
import h5py as hf

from matplotlib.ticker import MultipleLocator

path = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization')
sys.path.append(path)
import plot_define as pd
import sfincsTools.readScan_Er as rE

# read in W7-X data #
# Er_dir = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.1237')
# Er_dir = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.189')
# Er_dir = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.2945')
Er_dir = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.3977')
# Er_dir = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.5')
# Er_dir = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.6019')
# Er_dir = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.7035')
# Er_dir = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.7986')
# Er_dir = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.9007')
scanEr = rE.readErScan(Er_dir)

# get particle flux data #
scanEr.interp_ambipolar_data()
Er = scanEr.data[:,0,0]
partF_i = scanEr.data[:,2,0]
partF_e = scanEr.data[:,2,1]
heatF_i = scanEr.data[:,3,0]
heatF_e = scanEr.data[:,3,1]

# plotting paramters #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(2, 1, tight_layout=True, figsize=(8, 11))
ax1, ax2 = axs[0], axs[1]

# plot data #
ax1.plot(Er, partF_i, ls='--', marker='s', markerfacecolor='None', ms=7, c='tab:blue', label='Ion')
ax1.plot(Er, partF_e, ls=':', marker='v', markerfacecolor='None', ms=7, c='tab:orange', label='Electron')
ax2.plot(Er, heatF_i, ls='--', marker='s', markerfacecolor='None', ms=7, c='tab:blue', label='Ion')
ax2.plot(Er, heatF_e, ls=':', marker='v', markerfacecolor='None', ms=7, c='tab:orange', label='Electron')

# axis labels #
ax1.set_title(r'$r \ / \ a = {0:0.2f}$'.format(scanEr.rN))
ax1.set_ylabel(r'$\langle \Gamma_\mathrm{part} \rangle \ / \ \left( 10^{20} \ \mathrm{m}^{-2} \ \mathrm{s}^{-1} \right)$')
ax2.set_ylabel(r'$\langle \Gamma_\mathrm{heat} \rangle \ / \ \left( \mathrm{kW} \ / \ \mathrm{m}^2 \right)$')
ax2.set_xlabel(r'$E_r \ / \ \left(\mathrm{kV} / \mathrm{m}\right)$')

# plot ambipolar electric field #
if len(scanEr.ambi_data[0,0]) == 1:
    lab = '{0:0.1f}'.format(scanEr.ambi_data[0,0][0])
else:
    lab = '('+', '.join(['{0:0.1f}'.format(Er) for Er in scanEr.ambi_data[0,0]])+')'

# loop axes #
for a, ax in enumerate(axs.flat):
    # limits #
    ax.set_xlim(ax.get_xlim())
    ax.set_ylim(ax.get_ylim())

    # zero axes #
    ax.plot([0,0], ax.get_ylim(), c='k', ls='-', lw=1)
    ax.plot(ax.get_xlim(), [0,0], c='k', ls='-', lw=1)

    # ambipolar Er #
    for Er_idx, Er in enumerate(scanEr.ambi_data[0,0]):
        if Er_idx == 0:
            ax.plot([Er, Er], ax.get_ylim(), c='k', ls=':', lw=1.5, label=r'$E_r^\mathrm{amb} = %s$'%lab)
        else:
            ax.plot([Er, Er], ax.get_ylim(), c='k', ls=':', lw=1.5)

    # legend #
    ax.legend(frameon=False, framealpha=1)

    # ticks #
    ax.tick_params(axis='both', which='major', direction='in', length=5)
    ax.tick_params(axis='both', which='minor', direction='in', length=2.5)
    ax.xaxis.set_ticks_position('default')
    ax.yaxis.set_ticks_position('default')
    ax.xaxis.set_minor_locator(MultipleLocator(1))
    if a == 0:
        ax.yaxis.set_minor_locator(MultipleLocator(0.01))
    else:
        ax.yaxis.set_minor_locator(MultipleLocator(1))

# save/show #
plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'Course_material', 'W7X_ref_168_rN_%s_Er_scan.pdf' % Er_dir.split('rN_')[1].replace('.','p'))
# plt.savefig(save_path, format='pdf')
