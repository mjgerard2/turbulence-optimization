# -*- coding: utf-8 -*-
"""
Created on Sun Sep  6 21:32:46 2020

@author: micha
"""

import os
import numpy as np
import h5py as hf

from scipy.interpolate import interp1d
from scipy.optimize import brentq
from scipy import constants


class readErScan():
    """ A data analysis class for reading SFINCS outputs from scanType 2.
    
    ...
    
    Attributes
    ----------
    path : str
        The path to the directory where the Er scan results are stored.
        
    Methods
    -------
    interp_ambipolar_data():
        This function uses the data provided by SFINCS to interpolate the 
        radial current as a function of the radial electric field.  It then 
        calculates the roots of that function to identify the ambipolar 
        electric field.
    
    plot_data(self, ax, data_key):
        Plot the specified Er scan data.
    """
    
    def __init__(self, path):
        self.path = path
        ### Read Er Directories ###
        Er_dirs = [f.name for f in os.scandir(path) if f.name[0:2]=='Er']
        
        cnt = 0
        Er_val = []
        Er_data = []
        
        ### Define variable normalizations ###
        vel_norm = np.sqrt(2000*(constants.e/constants.m_p))
        bootFlow_scl = 1. / (1e11*constants.e*vel_norm) # nA m^{-2}
        partFlux_scl = vel_norm # 1e20 m^{-2}s^{-1}
        heatFlux_scl = 1e17 * constants.m_p * vel_norm**3 # kW m^{-2}
        radCurnt_scl = 1e20 * constants.e

        for Er_dir in Er_dirs:
            path_Er = os.path.join(path, Er_dir, 'sfincsOutput.h5')
            
            if os.path.isfile(path_Er):
                with hf.File(path_Er, 'r') as hf_file:
                    fin_chk = 'finished' in hf_file
                    self.rN = float(hf_file['rN'][()])
                    if fin_chk:
                        try:
                            Zs = hf_file["Zs"][:]
                            Er = hf_file['Er'][()] 
                            dPhiHatdpsiN = hf_file['dPhiHatdpsiN'][()]
                            
                            FSABFlow = hf_file["FSABFlow"][0:2,0] * bootFlow_scl
                            partFlux = hf_file["particleFlux_vm_rHat"][0:2,0] * partFlux_scl
                            heatFlux = hf_file["heatFlux_vm_rHat"][0:2,0] * heatFlux_scl
                            
                            dn_drN = (hf_file['dnHatdrHat'][:] * hf_file['nHats']) / hf_file['rHat'][()]
                            dT_drN = (hf_file['dTHatdrHat'][:] * hf_file['THats'][:] * hf_file['nHats'][:] * 16.022) / hf_file['rHat'][()]
                            
                            partDiff = - partFlux / dn_drN
                            heatDiff = - heatFlux / dT_drN

                            radialCurrent = np.dot(Zs, partFlux) * radCurnt_scl
                            FSABjHat = np.dot(Zs, FSABFlow)
                            currents = np.array([radialCurrent, FSABjHat])
                    
                            Er_val.append(np.array([Er, dPhiHatdpsiN, cnt]))
                            
                            data = np.array([FSABFlow, partFlux, heatFlux, partDiff, heatDiff, currents])
                            Er_data.append(data)
                            
                            cnt+=1
                        except KeyError:
                            continue
                        
        Er_val = np.array(Er_val)
        Er_data = np.array(Er_data)
        
        ### Sort Data by Er ###
        Er_val_sorted = np.array( sorted(Er_val, key=lambda x: x[0]) )
        self.data = np.empty((Er_val.shape[0], 7, 2))
        for idx, order in enumerate(Er_val_sorted[0::]):
            self.data[idx][0] = order[0:2]
            self.data[idx][1::] = Er_data[int(order[2])]
            
    
    def interp_ambipolar_data(self):
        """ This function uses the data provided by SFINCS to interpolate the 
        radial current as a function of the radial electric field.  It then 
        calculates the roots of that function to identify the ambipolar 
        electric field.

        Raises
        ------
        ValueError
            No ambipolar electric field could be identified.
        """
        Er = self.data[0::,0,0]
        Ir = self.data[0::,6,0]

        interp = interp1d(Er, Ir)
        
        Ir_sign = np.sign(Ir)
        Ir_chg_sign = ((Ir_sign[1::] - Ir_sign[0:-1]) != 0).astype(int)
        Ir_chg_idx = np.argwhere(Ir_chg_sign)[0::,0]
        
        if Ir_chg_idx.size==0:
            raise ValueError('No ambipolar field found in:\n'+self.path)
        
        roots = np.empty(Ir_chg_idx.shape)
        for i, idx in enumerate(Ir_chg_idx):
            roots[i] = brentq(interp, Er[idx], Er[idx+1])        
        
        self.ambi_data = np.empty(np.r_[self.data.shape[1::], roots.shape[0]])
        
        self.ambi_data[0,0] = roots
        dPhi = self.data[0::,0,1]
        dPhi_interp = interp1d(Er, dPhi)
        self.ambi_data[0,1] = dPhi_interp(roots)
        
        self.ambi_data[6,0] = interp(roots)
        boot = self.data[0::,6,1]
        boot_interp = interp1d(Er, boot)
        self.ambi_data[6,1] = boot_interp(roots)
        
        for i in range(1,6):
            d0 = self.data[0::,i,0]
            d1 = self.data[0::,i,1]
            
            interp0 = interp1d(Er, d0)
            interp1 = interp1d(Er, d1)
            
            self.ambi_data[i,0] = interp0(roots)
            self.ambi_data[i,1] = interp1(roots)

        
    def plot_data(self, ax, data_key):
        """ Plot the specified Er scan data.

        Parameters
        ----------
        ax : obj
            Matplotlib axis object on which the plot will be made.
        data_key : str
            Profile data to be plotted.  Options are listed below.
                boot flow
                part flux
                heat flux
                part diff
                heat diff

        Raises
        ------
        KeyError
            The data key provided was not one of the available data keys 
            listed above.
        """
        try:
            self.ambi_data
            ambi_chk = True
        except AttributeError:
            ambi_chk = False
            
        if data_key == 'current':
            ax_twin = ax.twinx()
            
            ax.plot(self.data[0::,0,0], [0]*len(self.data[0::,0,0]), ls='--', c='k')
            ax.plot(self.data[0::,0,0], self.data[0::,6,0], marker='s', c='k')
            
            ax_twin.plot(self.data[0::,0,0], self.data[0::,6,1], marker='o', c='tab:blue')
            
            if ambi_chk:
                ax.scatter(self.ambi_data[0,0], self.ambi_data[6,0], marker='*', s=250, c='tab:red', zorder=3)
                ax_twin.scatter(self.ambi_data[0,0], self.ambi_data[6,1], marker='*', s=150, c='tab:red', zorder=3)
            
            ax.set_xlabel('Er [kv/m]')
            ax.set_ylabel(r'radial current [$A m^{-2}$]')
            ax_twin.set_ylabel(r'bootstrap current [$nA m^{-2}$]', c='tab:blue')
            ax_twin.tick_params(axis='y', labelcolor='tab:blue')
        
        else:
                    
            if data_key == 'boot flow':
                self.data_idx = 1
                ylab = r'bootstrap flow [$nA m^{-2}$]'
            elif data_key == 'part flux':
                self.data_idx = 2
                ylab = r'$\langle\Gamma_{part}\rangle \ \left[10^{20} \, m^{-2}s^{-1}\right]$'
            elif data_key == 'heat flux':
                self.data_idx = 3
                ylab = r'$\langle\Gamma_{heat}\rangle$ [kW m$^{-2}$]'
            elif data_key == 'part diff':
                self.data_idx = 4
                ylab = r'$\langle D_{part}\rangle \ \left[m^{2}s^{-1}\right]$'
            elif data_key == 'heat diff':
                self.data_idx = 5
                ylab = r'$\langle D_{heat}\rangle \ \left[m^{2}s^{-1}\right]$'
            else:
                raise KeyError(data_key+' is not a valid data key.')
        
            ax.plot(self.data[0::,0,0], self.data[0::,self.data_idx,0], marker='s', markerfacecolor='None', ms=10, c='k', label='SFINCS Ion')
            ax.plot(self.data[0::,0,0], self.data[0::,self.data_idx,1], marker='o', markerfacecolor='None', ms=10, c='tab:red', label='SFINCS Electron')
            '''
            if ambi_chk:
                ax.scatter(self.ambi_data[0,0], self.ambi_data[self.data_idx,0], marker='*', s=150, c='tab:red', zorder=3, label='Er = {0:0.3f}'.format(self.ambi_data[0,0,0]))
                ax.scatter(self.ambi_data[0,0], self.ambi_data[self.data_idx,1], marker='*', s=150, c='tab:red', zorder=3)
            '''
            ax.set_xlabel('Er [kv/m]')
            ax.set_ylabel(ylab)
            ax.set_title('r/a = {0:0.3f}'.format(self.rN))


if __name__ == '__main__':
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    # Plotting Parameters #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 18}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2

    # Read in SFINCS data #
    Er_path = os.path.join(os.getcwd(), 'UG_HSX', 'rN_0.498')
    Er_data = readErScan(Er_path)
    Er_data.interp_ambipolar_data()

    # Plot SFINCS data #
    fig, ax = plt.subplots(1, 1, tight_layout=True)

    Er_data.plot_data(ax, 'part flux')

    plt.legend()
    plt.grid()
    plt.show()

