# -*- coding: utf-8 -*-
"""
Created on Wed Jun 10 15:22:09 2020

@author: micha
"""

import numpy as np

import os
#import readScan_Er as scanEr
import h5py as hf

from scipy.interpolate import interp1d
from scipy.optimize import brentq
from scipy import constants
class readErScan():
    """ A data analysis class for reading SFINCS outputs from scanType 2.
    
    ...
    
    Attributes
    ----------
    path : str
        The path to the directory where the Er scan results are stored.
        
    Methods
    -------
    interp_ambipolar_data():
        This function uses the data provided by SFINCS to interpolate the 
        radial current as a function of the radial electric field.  It then 
        calculates the roots of that function to identify the ambipolar 
        electric field.
    
    plot_data(self, ax, data_key):
        Plot the specified Er scan data.
    """
    
    def __init__(self, path):
        self.path = path
        ### Read Er Directories ###
        Er_dirs = [f.name for f in os.scandir(path) if f.name[0:2]=='Er']
        
        cnt = 0
        Er_val = []
        Er_data = []
        
        ### Define variable normalizations ###
        vel_norm = np.sqrt(2000*(constants.e/constants.m_p))
        bootFlow_scl = 1. / (1e11*constants.e*vel_norm) # nA m^{-2}
        partFlux_scl = vel_norm # 1e20 m^{-2}s^{-1}
        heatFlux_scl = 1e17 * constants.m_p * vel_norm**3 # kW m^{-2}
        radCurnt_scl = 1e20 * constants.e

        '''
        #Reference values
        p_mass=1.6726219e-27 # Proton mass
        e_charge  = 1.6022e-19     # Electron charge
        ref_mass=p_mass # Proton mass
        ref_B = 1.0 #Tesla
        ref_R=1.0 #meter
        ref_temp=1000.0*e_charge #KT (1 keV)#1000.0*e_charge #KT (1 keV)
        ref_phi=1000.0 #1 kV
        ref_n=1e20 #/m^3. 
        ref_v=437697.90925833833
        
        nHat=3.9582e-02 # used in sfincs. from calcul_sfincsParamsv2.py for r/a 0.587
        
        FlowFactor=1e-3*ref_v/nHat                           # [T km/s]
        ParticleFluxFactor=ref_n*ref_v*1e-20/ref_R           # [10^20/m2s]
        HeatFluxFactor=(ref_n*ref_v**3*ref_mass)/ref_R/1e3   # [kW/m2]
        jHatFactor=e_charge*ref_n*ref_v/1e6                  # [T MA/m2]
        
        '''
                
        for Er_dir in Er_dirs:
            path_Er = os.path.join(path, Er_dir, 'sfincsOutput.h5')
            
            if os.path.isfile(path_Er):
                with hf.File(path_Er, 'r') as hf_file:
                    fin_chk = 'finished' in hf_file
                    self.rN = float(hf_file['rN'][()])
                    if fin_chk:
                        try:
                            Zs = hf_file["Zs"][:]
                            Er = hf_file['Er'][()] 
                            dPhiHatdpsiN = hf_file['dPhiHatdpsiN'][()]
                            
                            FSABFlow = hf_file["FSABFlow"][0:2,0] * bootFlow_scl
                            partFlux = hf_file["particleFlux_vm_rHat"][0:2,0] * partFlux_scl
                            heatFlux = hf_file["heatFlux_vm_rHat"][0:2,0] * heatFlux_scl
                            
                            dn_dr = hf_file['dnHatdrHat'][:]
                            partDiff = - partFlux / dn_dr             
                        
                            dT_drN = hf_file['dTHatdrHat'][:] * hf_file['nHats'][:] * 16.022
                            
                            
                            heatDiff = - (heatFlux)/ dT_drN 

                            radialCurrent = np.dot(Zs, partFlux) * radCurnt_scl
                            FSABjHat = np.dot(Zs, FSABFlow)
                            currents = np.array([radialCurrent, FSABjHat])
                    
                            Er_val.append(np.array([Er, dPhiHatdpsiN, cnt]))
                            
                            data = np.array([FSABFlow, partFlux, heatFlux, partDiff, heatDiff, currents])
                            Er_data.append(data)
                            
                            cnt+=1
                        except KeyError:
                            continue
                        
        Er_val = np.array(Er_val)
        Er_data = np.array(Er_data)
        
        ### Sort Data by Er ###
        Er_val_sorted = np.array( sorted(Er_val, key=lambda x: x[0]) )
        self.data = np.empty((Er_val.shape[0], 7, 2))
        for idx, order in enumerate(Er_val_sorted[0::]):
            self.data[idx][0] = order[0:2]
            self.data[idx][1::] = Er_data[int(order[2])]
            
    
    def interp_ambipolar_data(self):
        """ This function uses the data provided by SFINCS to interpolate the 
        radial current as a function of the radial electric field.  It then 
        calculates the roots of that function to identify the ambipolar 
        electric field.

        Raises
        ------
        ValueError
            No ambipolar electric field could be identified.
        """
        Er = self.data[0::,0,0]
        Ir = self.data[0::,6,0]

        interp = interp1d(Er, Ir)
        
        Ir_sign = np.sign(Ir)
        Ir_chg_sign = ((Ir_sign[1::] - Ir_sign[0:-1]) != 0).astype(int)
        Ir_chg_idx = np.argwhere(Ir_chg_sign)[0::,0]
        
        if Ir_chg_idx.size==0:
            raise ValueError('No ambipolar field found in:\n'+self.path)
        
        roots = np.empty(Ir_chg_idx.shape)
        for i, idx in enumerate(Ir_chg_idx):
            roots[i] = brentq(interp, Er[idx], Er[idx+1])        
        
        self.ambi_data = np.empty(np.r_[self.data.shape[1::], roots.shape[0]])
        
        self.ambi_data[0,0] = roots
        dPhi = self.data[0::,0,1]
        dPhi_interp = interp1d(Er, dPhi)
        self.ambi_data[0,1] = dPhi_interp(roots)
        
        self.ambi_data[6,0] = interp(roots)
        boot = self.data[0::,6,1]
        boot_interp = interp1d(Er, boot)
        self.ambi_data[6,1] = boot_interp(roots)
        
        for i in range(1,6):
            d0 = self.data[0::,i,0]
            d1 = self.data[0::,i,1]
            
            interp0 = interp1d(Er, d0)
            interp1 = interp1d(Er, d1)
            
            self.ambi_data[i,0] = interp0(roots)
            self.ambi_data[i,1] = interp1(roots)
            
            

class read_rN_scan():
    """ A data analysis class for reading SFINCS outputs from scanType 5.
    
    ...
    
    Attributes
    ----------
    path : str
        The path to the directory where the profile scan results are stored.
        
    Methods
    -------
    plot_data(ax, data_key)
        Plot the specified profile data.
    """
    
    def __init__(self, path):
        rN_dirs = [f.name for f in os.scandir(path) if f.name[0:2]=='rN']
        
        cnt = 0
        rN_vals = []
        rN_data = []
        for rN_dir in rN_dirs:
            rN_path = os.path.join(path, rN_dir)
            ErData = readErScan(rN_path)
            try:
                ErData.interp_ambipolar_data()
                if ErData.ambi_data.shape[2] != 1:
                    ErData.ambi_data[:,:,0] = np.mean(ErData.ambi_data, axis=2)
                rN_data.append(ErData.ambi_data[:,:,0])
                rN_vals.append(np.array([float(rN_dir.split('_')[1]), cnt]))
                cnt+=1
                    
            except ValueError:
                print('   No ambipolar field found in '+rN_dir)

        rN_vals_sorted = np.array(sorted(rN_vals, key=lambda x: x[0]))
        self.rN_data = []
        for idx in rN_vals_sorted[0::,1]:
            self.rN_data.append(rN_data[int(idx)])
        
        self.rN_data = np.array(self.rN_data)
        self.rN_vals = rN_vals_sorted[0::,0]
        
    def get_data_idx(self, data_key):
        """ Get the appropriate index for the profile data corresponding to 
        the specified data key.

        Parameters
        ----------
        data_key : str
            Profile data idex being retrieved.  Options are listed below.
                ambi Er
                boot flow
                part flux
                heat flux
                part diff
                heat diff

        Raises
        ------
        KeyError
            The data key provided was not one of the available data keys 
            listed above.
        """
        if data_key == 'ambi Er':
            self.data_idx = 0
            self.ylab = r'Ambipolar $E_r$ [$kV/m$]'
        elif data_key == 'boot flow':
            self.data_idx = 1
            self.ylab = r'bootstrap flow [$nA m^{-2}$]'
        elif data_key == 'part flux':
            self.data_idx = 2
            self.ylab = r'$\langle\Gamma_{part}\rangle \ \left[10^{20} \, m^{-2}s^{-1}\right]$'
        elif data_key == 'heat flux':
            self.data_idx = 3
            self.ylab = r'$\langle q_{heat}\rangle$ [kW m$^{-2}$]'
        elif data_key == 'part diff':
            self.data_idx = 4
            self.ylab = r'$\langle D_{part}\rangle \ \left[m^{2}s^{-1}\right]$'
        elif data_key == 'heat diff':
            self.data_idx = 5
            self.ylab = r'$\langle D_{heat}\rangle \ \left[m^{2}s^{-1}\right]$'
        elif data_key == 'current':
            self.data_idx = 6
            self.ylab = r'bootstrap current [$nA m^{-2}$]'
        else:
            raise KeyError(data_key+' is not a valid data key.')        
                
    def plot_data(self, ax, data_key):
        """ Plot the specified profile data.

        Parameters
        ----------
        ax : obj
            Matplotlib axis object on which the plot will be made.
        data_key : str
            Profile data to be plotted.  Options are listed below.
                boot flow
                part flux
                heat flux
                part diff
                heat diff
                ambi Er
        """
        self.get_data_idx(data_key)
        
        if data_key != 'ambi Er':
            ax.plot(self.rN_vals, self.rN_data[0::,self.data_idx,0], ls='--', marker='s', ms=10, markerfacecolor='None', c='k', label='Ion')
            ax.plot(self.rN_vals, self.rN_data[0::,self.data_idx,1], ls='-.', marker='o', ms=10, markerfacecolor='None', c='tab:red', label='Electron')
            
            rN_idx = np.argmin( np.abs(self.rN_vals - 0.2081 ) )
        else:
            ax.plot(self.rN_vals, self.rN_data[0::,self.data_idx,0], marker='s', ms=10, markerfacecolor='None', c='k', label='SFINCS')
        
        ax.set_xlabel(r'$r/a$')
        ax.set_ylabel(self.ylab)
    
    def get_data(self):
        return(self)

if __name__ == '__main__':
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    # Plotting Parameters #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 18}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 22
    mpl.rcParams['lines.linewidth'] = 2

    # Read in SFINCS data #
    sfincs_psiN = os.path.join(os.getcwd(), 'UG_HSX')
    scan_rN = read_rN_scan(sfincs_psiN)
    ben=scan_rN.get_data()
    
    # Plot SFINCS Heat diffusion
    fig, ax = plt.subplots(1, 1, tight_layout=True)    
    data_idx = 0
    ylab = r'Ambipolar $E_r$ [$kV/m$]'
    ax.plot(ben.rN_vals, ben.rN_data[0::,data_idx,0], ls='--', marker='s', ms=10, markerfacecolor='None', c='k')   
    ax.set_xlim([0,1])
    ax.set_ylim([-5,5])    
    ax.set_ylabel(ylab)  
    plt.grid()
    plt.show()
    
    
    # Plot SFINCS data #
    fig, ax = plt.subplots(1, 1, tight_layout=True)      
    data_idx = 3
    ylab = r'$\langle q_{heat}\rangle$ [kW m$^{-2}$]'
    ax.plot(ben.rN_vals, ben.rN_data[0::,data_idx,0], ls='--', marker='s', ms=10, markerfacecolor='None', c='k', label='Ion')
    ax.plot(ben.rN_vals, ben.rN_data[0::,data_idx,1], ls='-.', marker='o', ms=10, markerfacecolor='None', c='tab:red', label='Electron')
           
    ax.set_xlim([0,1])
    ax.set_ylabel(ylab) 
    plt.legend()
    plt.grid()
    plt.show()
    
    
    
     # Plot SFINCS Heat diffusion
    fig, ax = plt.subplots(1, 1, tight_layout=True)    
    data_idx = 5
    ylab = r'$\langle D_{heat}\rangle \ \left[m^{2}s^{-1}\right]$'
    ax.plot(ben.rN_vals, ben.rN_data[0::,data_idx,0], ls='--', marker='s', ms=10, markerfacecolor='None', c='k', label='Ion')
    ax.plot(ben.rN_vals, ben.rN_data[0::,data_idx,1], ls='-.', marker='o', ms=10, markerfacecolor='None', c='tab:red', label='Electron')
           
    ax.set_xlim([0,1])
    ax.set_ylabel(ylab)  
    plt.legend()
    plt.grid()
    plt.show()
