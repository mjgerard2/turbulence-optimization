#!/bin/bash
import os, sys
import numpy as np
import h5py as hf

from scipy import constants
from matplotlib.ticker import MultipleLocator

script_dir = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(script_dir, '..'))
import plot_define as pd
import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.coord_convert as cc
import vtkTools.vtk_grids as vtk


def flux_surface_perturbation(path_sfincs, path_wout, name_wout, path_save, data_key):
    """ Generate a vtk file for the specified data over a flux surface.

    Parameters
    ----------
    path_sfincs : str
        Path to the directory where the sfincsOutput.h5 file is stored.
    path_wout : str
        Path to the directory where the wout file is stored.
    name_wout : str
        Wout file name used in SFINCS run, must be stored in path_wout.
    path_save : str
        Path to the directory where the vtk file will be stored.
    data_key : str
        SFINCS data to be plotted in the vtk file. Options are listed below.
                density
                part flux
                heat flux
                part diff
                heat diff

    Raises
    ------
    KeyError
            The data key provided was not one of the available data keys
            listed above.
    """
    with hf.File(os.path.join(path_sfincs, 'sfincsOutput.h5'), 'r') as hf_file:
        psiN = hf_file['rN'][()]**2
        vel_norm = np.sqrt(2000*(constants.e/constants.m_p))
        if data_key == 'heat flux':
            scl_elec = 10e17 * constants.m_p * vel_norm**3 # kW m^{-2}
            scl_ion = scl_elec # kW m^{-2}
            key = 'heatFluxBeforeSurfaceIntegral_vm'
        elif data_key == 'part flux':
            scl_elec = vel_norm * 1e2 # 1e18 m^{-2}s^{-1}
            scl_ion = scl_elec * 1e2 # 1e18 m^{-2}s^{-1}
            key = 'particleFluxBeforeSurfaceIntegral_vm'
        elif data_key == 'heat diff':
            scl_flux = 10e17 * constants.m_p * vel_norm**3
            dn_dpsiN = hf_file['dnHatdpsiN'][:] / hf_file['psiAHat']
            scl_elec = - scl_flux / dn_dpsiN[1] # kW m^{2}
            scl_ion = - scl_flux / dn_dpsiN[0] # kW m^{2}
            key = 'heatFluxBeforeSurfaceIntegral_vm'
        elif data_key == 'part diff':
            scl_flux = vel_norm * 1e2
            dn_dpsiN = hf_file['dnHatdpsiN'][:] / hf_file['psiAHat']
            scl_elec = - scl_flux / dn_dpsiN[1] # 1e18 m^{2} s^{-1}
            scl_ion = - scl_flux / dn_dpsiN[0] # 1e18 m^{2} s^{-1}
            key = 'particleFluxBeforeSurfaceIntegral_vm'
        elif data_key == 'density':
            scl_elec = 1e2 # 1e18 m^{-3}
            scl_ion = 1e2 # 1e18 m^{-3}
            key = 'totalDensity'
        else:
            raise KeyError(data_key+' is not a valid data_key.')

        v_dom = hf_file['zeta'][:]
        u_dom = hf_file['theta'][:]

        data_elec = hf_file[key][0::,0::,1,0] * scl_elec
        data_ion = hf_file[key][0::,0::,0,0] * scl_ion

    v_num = v_dom.shape[0]
    u_num = u_dom.shape[0]

    wout = wr.readWout(path_wout, name=name_wout)

    data_elec_full = np.empty((v_num*wout.nfp+1, u_num))
    data_ion_full = np.empty((v_num*wout.nfp+1, u_num))
    for i in range(wout.nfp):
        data_elec_full[i*v_num:(i+1)*v_num] = data_elec
        data_ion_full[i*v_num:(i+1)*v_num] = data_ion
    data_elec_full[wout.nfp*v_num] = data_elec[0]
    data_ion_full[wout.nfp*v_num] = data_ion[0]

    segs = np.linspace(0, 2*np.pi, wout.nfp, endpoint=False)
    v_dom_full = np.empty(v_num*wout.nfp)
    for s, seg in enumerate(segs):
        v_dom_full[s*v_num:(s+1)*v_num] = seg+v_dom
    v_dom_full = np.append(v_dom_full, 2*np.pi)

    wout.transForm_2D_sSec(psiN, u_dom, v_dom_full, ['R','Z'])
    cart_coord = cc.cartCoord(wout)[0::,0::,0::]

    elec_name = data_key+'_elec_psiN_{0:0.3f}.vtk'.format(psiN)
    ion_name = data_key+'_ion__psiN_{0:0.3f}.vtk'.format(psiN)

    vtk.scalar_mesh(path_save, elec_name, cart_coord, data_ion_full)
    vtk.scalar_mesh(path_save, ion_name, cart_coord, data_elec_full)

def plot_Er_scan(path, data_key, save_path=None):
    """ Plot the specified data as a function of the radial electric field on
    a single flux surface.

    Parameters
    ----------
    path : str
        Path of directory where Er scan directories are stored.
    data_key : str
        SFINCS data to be plotted in the vtk file. Options are listed below.
                boot flow
                part flux
                heat flux
                part diff
                heat diff
    save_path : str, otpional
        Global path to where figure will be save. Default is None.
    """
    import readScan_Er as scanEr

    plot = pd.plot_define(lineWidth=2)
    mpl, plt = plot.mpl, plot.plt
    fig, ax = plt.subplots(1, 1, tight_layout=True)

    scan = scanEr.readErScan(path)
    try:
        scan.interp_ambipolar_data()
    except ValueError:
        print('No ambipolar field found in:\n'+scan.path)
    scan.plot_data(ax, data_key)

    if save_path is None:
        plt.show()
    else:
        plt.savefig(save_path, format='pdf')

def plot_rN_scan(path, data_key, save_path=None):
    """ Plot the specified profile data from sfincsScan type 5.

    Parameters
    ----------
    path : str
        Path to directory where rN directories are stored.
    data_key : str
        SFINCS data to be plotted in the vtk file. Options are listed below.
                boot flow
                part flux
                heat flux
                part diff
                heat diff
    save_path : None, or str (optional)
        If None, the plot will be shown. Otherwise, this should be
        a global path to where the plot will be saved. Default is None.
    """
    import readScan_rN as scan_rN
    plot = pd.plot_define(fontSize=18, labelSize=22, lineWidth=2)
    plt = plot.plt
    fig, ax = plt.subplots(1, 1, tight_layout=True)

    scan = scan_rN.read_rN_scan(path)
    scan.plot_data(ax, data_key)

    if data_key != 'boot flow' and data_key != 'heat diff' and data_key != 'part diff' and data_key!= 'ambi Er':
        if np.max(scan.rN_data[:, scan.data_idx, 0]) > np.max(scan.rN_data[:, scan.data_idx, 1]):
            ymax = np.max(scan.rN_data[:, scan.data_idx, 0])
        else:
            ymax = np.max(scan.rN_data[:, scan.data_idx, 1])

        if np.min(scan.rN_data[:, scan.data_idx, 0]) < np.min(scan.rN_data[:, scan.data_idx, 1]):
            ymin = np.min(scan.rN_data[:, scan.data_idx, 0])
        else:
            ymin = np.min(scan.rN_data[:, scan.data_idx, 1])

        # plt.yscale('log')
        # plt.ylim(0.5*ymin, 20*ymax)

    plt.grid()
    if data_key != 'current':
        plt.legend(loc='upper right')

    if save_path is None:
        plt.show()
    else:
        plt.savefig(save_path)


def plot_input_profiles(prof_path, save_path=None):
    """  A method for plotting the input profiles to a SFINCS run.

    Parameters
    ----------
    prof_path : str
        The global path to the profile input file
    save_path : None, or str (optional)
        If None, the plot will be shown. Otherwise, this should be
        a global path to where the plot will be saved. Default is None.
    """
    # import profile data #
    with open(prof_path) as f:
        lines = f.readlines()
        rad_coord = int(lines[0].strip())
        if rad_coord == 0:
            rad_label = r'$\hat{\psi}$'
        elif rad_coord == 1:
            rad_label = r'$\psi_N$'
        elif rad_coord == 2:
            rad_label = r'$\hat{r}$'
        elif rad_coord == 3:
            rad_label = r'$r/a$'

        prof_data = np.empty((8, len(lines)-1))
        for i, line in enumerate(lines[1:]):
            arr = [float(x) for x in line.strip().split()]
            prof_data[0,i] = arr[0]
            prof_data[1,i] = arr[1]
            prof_data[2,i] = arr[2]
            prof_data[3,i] = arr[3]
            prof_data[4,i] = arr[4]
            prof_data[5,i] = arr[5]
            prof_data[6,i] = arr[6]
            prof_data[7,i] = arr[7]

    # plotting parameters #
    plot = pd.plot_define(fontSize=22, labelSize=26, lineWidth=2)
    plt = plot.plt
    # fig, axs = plt.subplots(1, 2, sharex=True, tight_layout=True, figsize=(10.5, 5))
    fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(8, 11))

    axs[0].plot(prof_data[0], prof_data[5], c='tab:blue', label='ions')
    axs[0].plot(prof_data[0], prof_data[7], c='tab:orange', label='electrons')

    axs[1].plot(prof_data[0], prof_data[4], c='tab:blue', label='ions')
    axs[1].plot(prof_data[0], prof_data[6], c='tab:orange', label='electrons')

    # axs[0].set_xlabel(rad_label)
    axs[1].set_xlabel(rad_label)

    axs[1].set_ylabel(r'$n \ / \ \left( 10^{20} \ \mathrm{m}^{-3} \right)$')
    axs[0].set_ylabel(r'$T \ / \ \mathrm{keV}$')

    # axs[0].set_xlim(0, prof_data[0,-1])
    axs[0].set_xlim(0, 1)
    axs[0].set_ylim(0, axs[0].get_ylim()[1])
    axs[1].set_ylim(0, axs[1].get_ylim()[1])

    axs[0].legend(frameon=False)
    # axs[0].grid()
    # axs[1].grid()

    # axis ticks #
    for ax in axs.flat:
        ax.tick_params(axis='both', which='major', direction='in', length=5)
        ax.tick_params(axis='both', which='minor', direction='in', length=2.5)
        ax.xaxis.set_ticks_position('default')
        ax.yaxis.set_ticks_position('default')
        ax.xaxis.set_minor_locator(MultipleLocator(0.025))
    axs[0].yaxis.set_minor_locator(MultipleLocator(0.2))
    axs[1].yaxis.set_minor_locator(MultipleLocator(0.01))

    if save_path is None:
        plt.show()
    else:
        plt.savefig(save_path, format='pdf')

def plot_comparison_set(conIDs, data_key, plotDict, spec='ion'):
    import readScan_rN as scan_rN
    plot = pd.plot_define(fontSize=16, labelSize=20, lineWidth=2)

    if spec == 'ion':
        spec_idx = 0
    elif spec == 'electron':
        spec_idx = 1

    for c, conID in enumerate(conIDs):
        path = os.path.join(dd.pathSFINCS['HSX'], conID, 'profile_20000D01')
        scan = scan_rN.read_rN_scan(path)
        scan.get_data_idx(data_key)

        pltD = {}
        for plot_key in plotDict:
            pltD[plot_key] = plotDict[plot_key][c]

        plot.plt.plot(scan.rN_vals, scan.rN_data[0::,scan.data_idx,spec_idx], ms=10, markerfacecolor='None', **pltD)

    plot.plt.xlabel(r'$\Psi_N$')
    plot.plt.ylabel(scan.ylab)
    plot.plt.yscale('log')

    box = plot.ax.get_position()
    plot.ax.set_position([box.x0, box.y0, box.width * 0.95, box.height])
    plot.plt.legend(loc='upper left', bbox_to_anchor=(1.01, 1))

    plot.plt.grid()
    plot.plt.tight_layout()
    return plot


if __name__ == '__main__':
    # sfincs_psiN = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.189')
    # sfincs_psiN = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile_scan', 'rN_0.3977')
    sfincs_psiN = os.path.join('/mnt', 'HSX_Database', 'SFINCS', '0-1-0')
    prof_path = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'W7X_ref_168', 'profile.W7X')

    save_dir = os.path.join('/home', 'michael', 'Desktop', 'Course_material')
    save_prof = os.path.join(save_dir, 'W7X_ref_168_profiles.pdf')
    # plot_input_profiles(prof_path, save_path=save_prof)

    # save_Er = os.path.join(save_dir, 'W7X_ref_168_rN_%s_Er.pdf' % sfincs_psiN.split('rN_')[1].replace('.', 'p'))
    # save_heat = os.path.join(save_dir, 'W7X_ref_168_heat_flux.png')
    # save_part = os.path.join(save_dir, 'W7X_ref_168_part_flux.png')

    # plot_rN_scan(sfincs_psiN, 'ambi Er', save_path=save_Er)
    plot_rN_scan(sfincs_psiN, 'heat flux') #, save_path=save_heat)
    # plot_rN_scan(sfincs_psiN, 'part flux', save_path=save_part)

    # plot_Er_scan(sfincs_psiN, 'heat flux') #, save_path=save_Er)
