# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 16:46:01 2021

@author: bgeiger3
"""
import numpy as np
from read_penta_output import read_penta_mat


import matplotlib.pyplot as plt
cmap = plt.get_cmap("tab10")
plt.close('all')
   
## Er
fig1, ax1 = plt.subplots()
## Chi
fig2, ax2 = plt.subplots()
## Te,Ti, ne, ni
fig3, (ax3a,ax3b) = plt.subplots(nrows=2, sharex=True,figsize=([8.,8.]))
## Heat fluxes
fig4, ax4 = plt.subplots()
## Particle fluxes
fig5, ax5 = plt.subplots()      
## Particle diffusion
fig6, ax6 = plt.subplots()     



file='PENTA_output_upgrade111721Noneutrals.mat'
file='PENTA_output_postupgrade012122DKES.mat'

ra,Te,Ti,ne,ni,qe,qi,chi_e_penta,chi_i_penta,Er,gamma_e,gamma_i,D_e,D_i=read_penta_mat(file)

## plot Er
ax1.plot(ra,Er/1.e3,label=r'PENTA',color='k')  

## plot CHI
ax2.plot(ra,chi_i_penta,label=r'$\chi_i$ PENTA',color=cmap(1))  
ax2.plot(ra,chi_e_penta,label=r'$\chi_e$ PENTA',color=cmap(0))
## plot profiles
ax3a.plot(ra,ne,label=r'$n_i$ PENTA',color=cmap(1))  
ax3a.plot(ra,ni,label=r'$n_e$ PENTA',color=cmap(0))


ax3b.plot(ra,Ti,label=r'$T_i$ PENTA',color=cmap(1))
ax3b.plot(ra,Te,label=r'$T_e$ PENTA',color=cmap(0))  

 
ax4.plot(ra,qi/1.e3,label=r'$q_i$ PENTA',color=cmap(1))  
ax4.plot(ra,qe/1.e3,label=r'$q_e$ PENTA',color=cmap(0))   


ax5.plot(ra,gamma_i,label=r'$\Gamma_i$ PENTA',color=cmap(1))  
ax5.plot(ra,gamma_e,label=r'$\Gamma_e$ PENTA',color=cmap(0))   

ax6.plot(ra,D_i,label=r'$D_i$ PENTA',color=cmap(1))  
ax6.plot(ra,D_e,label=r'$D_e$ PENTA',color=cmap(0))   



import os
from sfincsTools.readScan_rN import read_rN_scan
import sys
sys.path.insert(1, 'sfincsTools/')
# Read in SFINCS data #
# sfincs_psiN = os.path.join(os.getcwd(), 'sfincsTools/UG_HSX')
sfincs_psiN = os.path.join('/mnt', 'HSX_Database', 'SFINCS', 'HSX_Profiles', 'UG_HSX')
scan_rN = read_rN_scan(sfincs_psiN)
sfnks=scan_rN.get_data()


# Plot SFINCS ER
data_idx = 0
ax1.plot(sfnks.rN_vals, sfnks.rN_data[0::,data_idx,0], ls='--', marker='s', ms=10, markerfacecolor='None', c='k',label='SFINCS')   


# Plot SFINCS Heat diffusion
data_idx = 5
ax2.plot(sfnks.rN_vals, sfnks.rN_data[0::,data_idx,0], ls='--', marker='s', ms=10, markerfacecolor='None',c=cmap(1), label=r'$\chi_i$ SFINCS')
ax2.plot(sfnks.rN_vals, sfnks.rN_data[0::,data_idx,1], ls='-.', marker='o', ms=10, markerfacecolor='None', c=cmap(0), label=r'$\chi_e$ SFINCS')



# Plot SFINCS heat flux
data_idx = 3
ax4.plot(sfnks.rN_vals, sfnks.rN_data[0::,data_idx,0], ls='--', marker='s', ms=10, markerfacecolor='None', c=cmap(1), label=r'$q_i$ SFINCS')
ax4.plot(sfnks.rN_vals, sfnks.rN_data[0::,data_idx,1], ls='-.', marker='o', ms=10, markerfacecolor='None', c=cmap(0), label=r'$q_e$ SFINCS')
  

# Plot SFINCS particle flux
data_idx = 2
ax5.plot(sfnks.rN_vals, sfnks.rN_data[0::,data_idx,0], ls='--', marker='s', ms=10, markerfacecolor='None', c=cmap(1), label=r'$\Gamma_i$ SFINCS')
ax5.plot(sfnks.rN_vals, sfnks.rN_data[0::,data_idx,1], ls='-.', marker='o', ms=10, markerfacecolor='None', c=cmap(0), label=r'$\Gamma_e$ SFINCS')
  

# Plot SFINCS particle diffusion
data_idx = 4
ax6.plot(sfnks.rN_vals, sfnks.rN_data[0::,data_idx,0], ls='--', marker='s', ms=10, markerfacecolor='None', c=cmap(1), label=r'$D_i$ SFINCS')
ax6.plot(sfnks.rN_vals, sfnks.rN_data[0::,data_idx,1], ls='-.', marker='o', ms=10, markerfacecolor='None', c=cmap(0), label=r'$D_e$ SFINCS')
  


ax1.grid()
#ax1.plot([0,1],[0,0],linestyle='--',color='k')
#ax1.set_ylim([-10,40])
ax1.set_ylabel(r'E$_r$ [kV/m]')
#ax1.set_ylabel(r'$10^{19}\mathrm{m}^{-3}$')
#ax1.legend(loc=1)    
ax1.set_xlim([0,1])  
ax1.set_xlabel('r/a')
ax1.legend(loc=1,fontsize=12) 
fig1.tight_layout() 
fig1.savefig('penta_Er_profiles.png')

ax2.grid()
ax2.set_ylim([0,1])
ax2.set_xlabel('r/a')
ax2.set_ylabel(r'$\chi$ [m$^2$/s]')
ax2.set_ylim([0,2])

ax2.legend(loc=1,fontsize=12) 
fig2.tight_layout() 
fig2.savefig('penta_diffusivity_profiles.png')



ax3a.grid()
ax3a.set_ylabel(r'n [m$^{-3}$]')
ax3a.set_xlim([0,1])
ax3a.set_ylim(bottom=0)
ax3a.legend(loc=1,fontsize=12)

ax3b.grid()
ax3b.set_ylabel(r'T [eV]')
ax3b.set_xlim([0,1])
ax3b.set_ylim(bottom=0)
ax3b.legend(loc=1,fontsize=12)
ax3b.set_xlabel('r/a')

fig3.tight_layout()
fig3.savefig('penta_input_profiles.png')

#ax3a.set_ylim([0,2])
ax4.set_ylabel(r'q [kW/m$^2$]')
ax4.set_ylim(bottom=0) 
ax4.set_xlabel('r/a')
ax4.set_xlim([0,1])
ax4.set_ylim([0,2])
ax4.legend(loc=1,fontsize=12)
fig4.tight_layout()
fig4.savefig('heat_flux_profiles.png')



#ax3a.set_ylim([0,2])
ax5.set_ylabel(r'$\Gamma\, [10^{20}/m^2/s$]')
ax5.set_ylim(bottom=0) 
ax5.set_xlabel('r/a')  
ax5.legend(loc=1,fontsize=12)   
fig5.tight_layout()     
fig5.savefig('particle_flux_profiles.png')    

ax6.set_ylabel(r'$D\, [m^2/s$]')
ax6.set_ylim(bottom=0) 
ax6.set_xlabel('r/a')  
ax6.legend(loc=1,fontsize=12)   
fig6.tight_layout()     
fig6.savefig('particle_diffusion_profiles.png')    




