import os
import copy
import numpy as np
import h5py as hf

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gs

from math import factorial
from scipy.interpolate import interp1d
from scipy.integrate import quadrature
from scipy.optimize import curve_fit
from scipy.optimize import brentq as find_root
from scipy.signal import argrelmin, argrelmax

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd


def get_extrema(Yarr, Xarr, idx):
    """ A method used to calculate the extrema of an array around the specified
    index with second order accuracy.

    Parameters
    ----------
    Yarr : array
        Dependent data to be extremzized.
    Xarr : array
        Independent data, used to find where the extrema occurs.
    idx : int
        Index of the extremized data point in the array.

    Returns
    -------
    x_ext : float
        Independent value where the extrema occurs
    y_ext : float
        Dependent value of extrema
    """
    if idx == 0:
        X1, X2, X3 = Xarr[idx], Xarr[idx+1], Xarr[idx+2]
        Y1, Y2, Y3 = Yarr[idx], Yarr[idx+1], Yarr[idx+2]
    elif idx == Xarr.shape[0]-1:
        X1, X2, X3 = Xarr[idx-2], Xarr[idx-1], Xarr[idx]
        Y1, Y2, Y3 = Yarr[idx-2], Yarr[idx-1], Yarr[idx]
    else:
        X1, X2, X3 = Xarr[idx-1], Xarr[idx], Xarr[idx+1]
        Y1, Y2, Y3 = Yarr[idx-1], Yarr[idx], Yarr[idx+1]

    Xmat = np.array([[X1**2, X1, 1],
                     [X2**2, X2, 1],
                     [X3**2, X3, 1]])
    Xmat_inv = np.linalg.inv(Xmat)

    Yvec = np.array([Y1, Y2, Y3])
    coef = np.dot(Xmat_inv, Yvec)

    x_ext = -0.5 * (coef[1] / coef[0])
    y_ext = coef[2] - 0.25 * coef[1] * coef[1] / coef[0]

    return x_ext, y_ext


class read_gist():
    """ A wrapper class for reading and analyzing a gist output.

    ...

    Attributes
    ----------
    filePath : string
        Absolute path to the gist file.

    Methods
    -------
    plot_fields(pol_lim=np.pi, saveName=None)
        Calculate all the component fields to the curvature drive term along
        with the curvature drive term itself.

    calc_Vpar(M, nfp, v_thresh=1e-6):
        Calculate the ratio of the parallel velocity to the total velocity
        for trapped particles described by the trapping parameters.
    """

    def __init__(self, filePath, nfp=4):

        if not os.path.isfile(filePath):
            raise IOError(filePath + " does not exist.")

        root, extension = os.path.splitext(filePath)
        self.nfp = nfp

        with open(filePath, 'r') as f:
            lines = f.readlines()
            if lines[10].strip() == '/':
                line_2 = lines[2].strip().split()
                self.s0 = float(line_2[3])
                self.alpha0 = float(line_2[4])

                line_3 = lines[3].strip().split()
                self.major = float(line_3[3])
                self.minor = float(line_3[4])

                line_4 = lines[4].strip().split()
                self.Bref = float(line_4[6])
                self.B_00 = float(line_4[7])
                self.B_01 = float(line_4[8])
                self.B_10 = float(line_4[9])
                self.B_11 = float(line_4[10])

                self.my_dpdx = float(lines[5].strip().split()[2])
                self.q0 = float(lines[6].strip().split()[2])
                self.shat = float(lines[7].strip().split()[2])
                self.gridpoints = int(lines[8].strip().split()[2])
                self.n_pol = int(lines[9].strip().split()[2])

                colm_data = np.empty((9, self.gridpoints))
                for ldx, line in enumerate(lines[11::]):
                    line = line.strip().split()
                    colm_data[:, ldx] = np.array([float(num) for num in line])
                self.colm_data = colm_data

            elif lines[11].strip() == '/':
                self.s0 = float(lines[1].strip().split()[2])
                self.alpha0 = float(lines[2].strip().split()[2])
                self.major = float(lines[3].strip().split()[2])
                self.minor = float(lines[4].strip().split()[2])
                self.Bref = float(lines[5].strip().split()[2])
                self.my_dpdx = float(lines[6].strip().split()[2])
                self.q0 = float(lines[7].strip().split()[2])
                self.shat = float(lines[8].strip().split()[2])
                self.gridpoints = int(lines[9].strip().split()[2])
                self.n_pol = int(lines[10].strip().split()[2])

                colm_data = np.empty((9, self.gridpoints))
                for ldx, line in enumerate(lines[12::]):
                    line = line.strip().split()
                    colm_data[:, ldx] = np.array([float(num) for num in line])
                self.colm_data = colm_data

            elif lines[12].strip() == '/':
                line_2 = lines[2].strip().split()
                self.s0 = float(line_2[3])
                self.alpha0 = float(line_2[4])

                line_3 = lines[3].strip().split()
                self.major = float(line_3[4])
                self.minor = float(line_3[5])

                line_4 = lines[4].strip().split()
                self.Bref = float(line_4[2])

                self.my_dpdx = float(lines[5].strip().split()[2])
                self.q0 = float(lines[6].strip().split()[2])
                self.shat = float(lines[7].strip().split()[2])
                self.gridpoints = int(lines[8].strip().split()[2])
                self.n_pol = int(lines[9].strip().split()[2])

                colm_data = np.empty((9, self.gridpoints))
                for ldx, line in enumerate(lines[13::]):
                    line = line.strip().split()
                    colm_data[:, ldx] = np.array([float(num) for num in line])

                self.colm_data = colm_data

            elif lines[18].strip() == '/':
                self.gridpoints = int(lines[1].strip().split()[2])
                self.q0 = float(lines[2].strip().split()[2])
                self.shat = float(lines[3].strip().split()[2])
                self.s0 = float(lines[4].strip().split()[2])
                self.minor = float(lines[5].strip().split()[1])
                self.major = float(lines[6].strip().split()[1])
                self.Bref = float(lines[10].strip().split()[2])
                self.n_pol = int(lines[11].strip().split()[2])
                self.my_dpdx = 0.0

                colm_data = np.empty((9, self.gridpoints))
                for ldx, line in enumerate(lines[19::]):
                    line = line.strip().split()
                    colm_arr = np.array([float(line[0]), float(line[1]), float(line[3]),
                                         float(line[6]), float(line[10]), float(line[7]),
                                         float(line[8]), float(line[9]), float(line[11])])
                    colm_data[:, ldx] = colm_arr
                self.colm_data = colm_data

            else:
                raise IOError("Gist file has an unfamiliar structure")

        self.Phi_edge = 0.5 * self.Bref * self.minor**2

        self.hlf_stp = np.pi * self.n_pol / (self.gridpoints - 1)
        self.pol_dom = np.pi * np.linspace(-self.n_pol, self.n_pol, self.gridpoints)
        self.pol_dom = self.pol_dom - self.hlf_stp

        self.data = {}

        self.data['gxx'] = colm_data[0, :]
        self.data['gxy'] = colm_data[1, :]
        self.data['gyy'] = colm_data[2, :]
        self.data['modB'] = colm_data[3, :]
        self.data['Jacob'] = colm_data[4, :]
        self.data['L2'] = colm_data[5, :]
        self.data['L1'] = -colm_data[6, :]
        self.data['dB_dThe'] = colm_data[7, :]

        self.data['K1'] = self.data['L1']
        self.data['K2'] = self.data['L2'] - 0.5*(self.my_dpdx/self.data['modB'])

        self.data['Kg'] = self.data['K1'] / np.sqrt(self.data['gxx'])
        self.data['Kn'] = (1./(self.data['modB']*np.sqrt(self.data['gxx']))) * (self.data['K2']*self.data['gxx'] - self.data['Kg']*self.data['gxy'])
        self.data['D'] = (self.shat/self.q0)*self.pol_dom - (self.data['gxy']/self.data['gxx'])

        insert_idx = [0, self.gridpoints]
        pol_add = [self.pol_dom[0] - 2*self.hlf_stp, self.pol_dom[-1] + 2*self.hlf_stp]
        pol_dom_ext = np.insert(self.pol_dom, insert_idx, pol_add)

        self.model = {}
        for key in self.data:
            data = self.data[key]
            data_add = [2*data[0] - data[1], 2*data[-1] - data[-2]]
            data_ext = np.insert(data, insert_idx, data_add)
            self.model[key] = interp1d(pol_dom_ext, data_ext, kind='linear')

    def balloon_data(self, ball_pol, nz0=None):
        """ Extend the flux-tube geometry data into the ballooning representation.

        Parameters
        ----------
        ball_pol : arr
            Array of poloidal ballooning angle
        """
        if nz0 is None:
            ball_cnt = int(ball_pol.shape[0]/self.gridpoints)
        else:
            ball_cnt = int(ball_pol.shape[0]/nz0)
        self.pol_dom_ball = ball_pol
        self.gridpoints_ball = ball_pol.shape[0]
        
        self.data_ball = {}
        for key, item in self.data.items():
            self.data_ball[key] = np.tile(self.data[key], ball_cnt)

        insert_idx = [0, self.gridpoints_ball]
        pol_add = [self.pol_dom_ball[0] - 2*self.hlf_stp, self.pol_dom_ball[-1] + 2*self.hlf_stp]
        pol_dom_ext = np.insert(self.pol_dom_ball, insert_idx, pol_add)
        for key, data in self.data_ball.items():
            data_add = [2*data[0] - data[1], 2*data[-1] - data[-2]]
            data_ext = np.insert(data, insert_idx, data_add)
            self.model[key] = interp1d(pol_dom_ext, data_ext, kind='linear')

    def plot_fields(self, pol_lim=np.pi, save_name=None):
        """ Calculate all the component fields to the curvature drive term along with
        the curvature drive term itself.

        Parameters
        ----------
        pol_lim : float, optional
            Extent along the poloidal direction to show in the plot.  The figure will
            show from (-pol_lim, pol_lim). Default is pi.
        save_name : string, optional
            Absolute path to where the figure should be saved.  Default is None.
        """
        # Plot Settings #
        plt.close('all')
        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': 18}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = 22
        mpl.rcParams['lines.linewidth'] = 2

        # Generate Figure Layout #
        fig = plt.figure(tight_layout=True, figsize=(16, 12))
        spec = gs.GridSpec(5, 1, figure=fig)

        ax11 = fig.add_subplot(spec[0])
        ax12 = ax11.twinx()

        ax2 = fig.add_subplot(spec[1], sharex=ax11)
        ax3 = fig.add_subplot(spec[2], sharex=ax11)
        ax4 = fig.add_subplot(spec[3], sharex=ax11)

        ax51 = fig.add_subplot(spec[4], sharex=ax11)
        ax52 = ax51.twinx()

        plt.setp(ax11.get_yticklabels(), visible=True, color='tab:orange')
        plt.setp(ax11.get_xticklabels(), visible=False)
        plt.setp(ax12.get_yticklabels(), visible=True, color='tab:blue')
        plt.setp(ax2.get_xticklabels(), visible=False)
        plt.setp(ax3.get_xticklabels(), visible=False)
        plt.setp(ax4.get_xticklabels(), visible=False)
        plt.setp(ax51.get_yticklabels(), visible=True, color='tab:orange')
        plt.setp(ax52.get_yticklabels(), visible=True, color='tab:blue')

        # Constrain Data #
        pol_sub = self.pol_dom[(self.pol_dom >= - pol_lim) & (self.pol_dom <= pol_lim)]
        Kn_sub = self.data['Kn'][(self.pol_dom >= - pol_lim) & (self.pol_dom <= pol_lim)]
        Kg_sub = self.data['Kg'][(self.pol_dom >= - pol_lim) & (self.pol_dom <= pol_lim)]
        D_sub = self.data['D'][(self.pol_dom >= - pol_lim) & (self.pol_dom <= pol_lim)]
        gxx_sub = self.data['gxx'][(self.pol_dom >= - pol_lim) & (self.pol_dom <= pol_lim)]
        modB_sub = self.data['modB'][(self.pol_dom >= - pol_lim) & (self.pol_dom <= pol_lim)]

        # Plot Data #
        pol_norm = pol_sub / np.pi

        ax11.plot(pol_norm, Kn_sub, c='tab:orange')
        ax12.plot(pol_norm, Kg_sub, c='tab:blue')
        ax2.plot(pol_norm, D_sub, c='k')
        ax3.plot(pol_norm, gxx_sub, c='k')
        ax4.plot(pol_norm, modB_sub, c='k')

        curve_drive = Kn_sub/gxx_sub + D_sub*Kg_sub*(gxx_sub / modB_sub)
        ax51.plot(pol_norm, curve_drive, c='tab:orange')
        ax51.plot(pol_norm, np.zeros(pol_norm.shape), c='tab:orange', ls='--')
        ax52.plot(pol_norm, Kn_sub / gxx_sub, c='tab:blue')
        ax52.plot(pol_norm, np.zeros(pol_norm.shape), c='tab:blue', ls='--')

        # Plot Labels #
        ax51.set_xlabel(r'$\theta / \pi$')

        ax11.set_ylabel(r'$\kappa_n$', c='tab:orange')
        ax12.set_ylabel(r'$\kappa_g$', c='tab:blue')
        ax2.set_ylabel(r'$D$')
        ax3.set_ylabel(r'$|\nabla \psi|$')
        ax4.set_ylabel(r'$B$')
        ax51.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D\kappa_g \frac{|\nabla \psi|}{B}$', c='tab:orange')
        ax52.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|}$', c='tab:blue')

        # Plot Limits #
        ax11.set_xlim(-pol_lim / np.pi, pol_lim / np.pi)

        Kn_sub = self.data['Kn'][(self.pol_dom >= - pol_lim) & (self.pol_dom <= pol_lim)]
        Kn_max = 1.1 * np.max(np.abs(Kn_sub))
        ax11.set_ylim(-Kn_max, Kn_max)

        Kg_sub = self.data['Kg'][(self.pol_dom >= - pol_lim) & (self.pol_dom <= pol_lim)]
        Kg_max = 1.1 * np.max(np.abs(Kg_sub))
        ax12.set_ylim(-Kg_max, Kg_max)

        # ax5.set_ylim(-50, 50)

        # Plot Styles #
        ax11.grid()
        ax2.grid()
        ax3.grid()
        ax4.grid()
        ax51.grid()

        ax11.set_title('GIST Fields')

        if save_name:
            plt.savefig(save_name)
        else:
            plt.show()

    def partition_wells(self):
        """ Find the poloidal angles of the B field maximums over all helically
        connected magnetic wells
        """
        # Identify approximate indices of the B-field #
        # extrema for individual magnetic wells #
        cos_model = lambda x, a1, a2: a1 + a2 * np.cos(x * (self.nfp * self.q0 - 1) - self.alpha0)
        p_guess = np.array([np.mean(self.data['modB']), 0.5*(np.max(self.data['modB']) - np.min(self.data['modB']))])
        popt, pcov = curve_fit(cos_model, self.pol_dom, self.data['modB'], p0=p_guess)
        if self.gridpoints % 2 == 0:
            B_chk = cos_model(self.pol_dom[1::], popt[0], popt[1])
        else:
            B_chk = cos_model(self.pol_dom, popt[0], popt[1])

        Bmin_idx = argrelmin(B_chk)[0]
        Bmax_idx = argrelmax(B_chk)[0]

        # Exclude partial magnetic wells from the extrema locations #
        if Bmin_idx.shape[0] == Bmax_idx.shape[0]:
            if self.pol_dom[Bmin_idx[0]] < self.pol_dom[Bmax_idx[0]]:
                Bmin_idx = np.r_[Bmin_idx, self.gridpoints-1]
            else:
                Bmin_idx = np.r_[0, Bmin_idx]
        elif Bmax_idx.shape[0] > Bmin_idx.shape[0]:
            Bmin_idx = np.r_[0, Bmin_idx, self.gridpoints-1]

        # Find poloidal angle and field strengths of well maxima #
        B_max = np.empty(Bmax_idx.shape[0])
        pol_max = np.empty(Bmax_idx.shape[0])
        idx_max = np.empty(Bmax_idx.shape[0], dtype=int)
        for i, idx in enumerate(Bmin_idx[0:-1]):
            index = idx + np.argmax(self.data['modB'][idx:Bmin_idx[i+1]])
            B_max[i] = self.data['modB'][index]
            pol_max[i] = self.pol_dom[index]
            idx_max[i] = index

        # Find poloidal angles and field strenghts of well minima #
        B_min = np.empty(Bmax_idx.shape[0]-1)
        pol_min = np.empty(Bmax_idx.shape[0]-1)
        idx_min = np.empty(Bmax_idx.shape[0]-1, dtype=int)
        for i, idx in enumerate(Bmax_idx[0:-1]):
            index = idx + np.argmin(self.data['modB'][idx:Bmax_idx[i+1]])
            B_min[i] = self.data['modB'][index]
            pol_min[i] = self.pol_dom[index]
            idx_min[i] = index

        # Well indices #
        idx_lim = -np.floor(0.5*pol_min.shape[0])
        well_indices = np.arange(idx_lim, -idx_lim+1, dtype=int)

        # create dictionary of partitioned values #
        self.partition = {}
        for i, idx in enumerate(well_indices):
            key = 'well {}'.format(idx)
            self.partition[key] = {}
            self.partition[key]['B'] = [B_max[i], B_min[i], B_max[i+1]]
            self.partition[key]['pol'] = [pol_max[i], pol_min[i], pol_max[i+1]]
            self.partition[key]['idx'] = [idx_max[i], idx_min[i], idx_max[i+1]]
        
        # add buffer wells #
        key_lft = 'well {}'.format(well_indices[0])
        key_rgt = 'well {}'.format(well_indices[-1])
        idx_lft = self.partition[key_lft]['idx'][0]
        idx_rgt = self.partition[key_rgt]['idx'][-1]

        Dpol = self.pol_dom[-1] - self.pol_dom[0]
        self.pol_dom_buff = np.r_[self.pol_dom[idx_rgt:-1]-Dpol, self.pol_dom, self.pol_dom[1:idx_lft+1]+Dpol]
        self.gridpoints_buff = self.pol_dom_buff.shape[0]

        # buffer gridpoint data #
        self.data_buff = {}
        for key, data in self.data.items():
            self.data_buff[key] = np.r_[data[idx_rgt:-1], data, data[1:idx_lft+1]]

        # buffer interpolation model #
        insert_idx = [0, self.gridpoints_buff]
        pol_add = [self.pol_dom_buff[0] - 2*self.hlf_stp, self.pol_dom_buff[-1] + 2*self.hlf_stp]
        pol_dom_ext = np.insert(self.pol_dom_buff, insert_idx, pol_add)
        for key, data in self.data_buff.items():
            data_add = [2*data[0] - data[1], 2*data[-1] - data[-2]]
            data_ext = np.insert(data, insert_idx, data_add)
            self.model[key] = interp1d(pol_dom_ext, data_ext, kind='linear')

        # add buffered wells to partition dictionary #
        idx_lft = np.where(self.pol_dom_buff <= pol_max[0])[0]
        B_lft = self.data_buff['modB'][idx_lft]
        pol_lft = self.pol_dom_buff[idx_lft]
        
        idx_rgt = np.where(self.pol_dom_buff >= pol_max[-1])[0]
        B_rgt = self.data_buff['modB'][idx_rgt]
        pol_rgt = self.pol_dom_buff[idx_rgt]

        self.well_indices = np.insert(well_indices, 0, well_indices[0]-1)
        self.well_indices = np.append(well_indices, well_indices[-1]+1)

        # left buffer #
        key_lft = 'well {}'.format(self.well_indices[0])
        self.partition[key_lft] = {}
        self.partition[key_lft]['B'] = [B_lft[0], np.min(B_lft), B_lft[-1]]
        self.partition[key_lft]['pol'] = [pol_lft[0], pol_lft[np.argmin(B_lft)], pol_lft[-1]]
        self.partition[key_lft]['idx'] = [idx_lft[0], idx_lft[np.argmin(B_lft)], idx_lft[-1]]

        # right buffer #
        key_rgt = 'well {}'.format(self.well_indices[-1])
        self.partition[key_rgt] = {}
        self.partition[key_rgt]['B'] = [B_rgt[0], np.min(B_rgt), B_rgt[-1]]
        self.partition[key_rgt]['pol'] = [pol_rgt[0], pol_rgt[np.argmin(B_rgt)], pol_rgt[-1]]
        self.partition[key_rgt]['idx'] = [idx_rgt[0], idx_rgt[np.argmin(B_rgt)], idx_rgt[-1]]
            
    def expansion_p_weight(self, pol_min, theta_gap, N=4):
        """ Calculate the p-weight function with a Taylor Expansion
        around a stationary point.

        Parameters
        ----------
        pol_min : float
            Poloidal anlge at which the local minimum in the magnetic field
            strength occurs
        theta_gap : float
            Half the domain over which to perform the expanion.
        """
        # Define Expansion Window #
        idx = np.argmin(np.abs(self.pol_dom - pol_min))
        theta_exp = self.pol_dom[(self.pol_dom >= pol_min-theta_gap) & (self.pol_dom <= pol_min+theta_gap)] - pol_min

        # Calculate Derivatives #
        deriv = {}
        deriv['d2B'] = np.gradient(self.data['dB_dThe']/self.data['modB'][idx], self.pol_dom)
        for n in range(N):
            deriv['d{}B'.format(n+3)] = np.gradient(deriv['d{}B'.format(n+2)], self.pol_dom)

        # Calculate Expanded Numerator and Denomenator #
        numer = np.zeros(theta_exp.shape)
        denom_1 = np.ones(theta_exp.shape)
        denom_2 = np.zeros(theta_exp.shape)
        for key in deriv:
            n = int(key[1])-2
            numer = numer + (deriv[key][idx]/factorial(n+1)) * ((theta_exp)**n)
            denom_1 = denom_1 + (deriv[key][idx]/factorial(n+2)) * ((theta_exp)**(n+2))
            denom_2 = denom_2 + (deriv[key][idx]/factorial(n+2)) * ((theta_exp)**n)

        denom_1[denom_1 < 0] = np.nan
        denom_2[denom_2 < 0] = np.nan

        # Calculate Expanded Weighting Function #
        theta_rng = theta_exp + pol_min
        p_wght_exp = np.abs(numer)/((denom_1**1.5)*np.sqrt(denom_2))

        return theta_rng, p_wght_exp

    def calc_Vpar(self, pol_well, B_well, k2, B_extrema, v_thresh=1e-12):
        """ Calculate the ratio of the parallel velocity to the total velocity
        for trapped particles described by the trapping parameters.

        Parameters
        ----------
        pol_well : arr
            poloidal domain over which to calculate the parallel velocity
        B_well : arr
            magnetic field strength over the poloidal domain
        k2 : float
            particle trapping parameter. 0 for deeply trapped particles
            and 1 for the trapped/passing boundary
        B_extrema : tuple
            minimum and maximum magnetic field strenght over poloidal domain
        v_thresh : float, optional
            threshold value at which we approach 0 parallel velocity
            (Default is 1e-12)
        """
        pol_stp = self.hlf_stp
        B_min_inv = 1./B_extrema[0]
        B_max_inv = 1./B_extrema[1]
        well_pnts = B_well.shape[0]

        # Calculate (v_{\parallel} / v)^2 #
        B_k2 = (1-k2)*B_min_inv + k2*B_max_inv

        v_sqrd = 1 - (v_thresh + B_well*B_k2)
        # v_sqrd = 1 - (v_thresh + self.model['modB'](pol_well)*B_k2)
        neg_idx = np.where(v_sqrd <= 0)[0]
        if neg_idx.shape[0] == 0:
            return [pol_well], [np.sqrt(v_sqrd)]
        elif neg_idx.shape[0] >= well_pnts-1:
            return [None], [None]
        
        pol_copy = np.copy(pol_well)
        pol_copy[neg_idx] = np.nan
        pol_segs = [pol_copy[v] for v in np.ma.clump_unmasked(np.ma.masked_invalid(pol_copy)) if pol_copy[v].shape[0] > 1]

        pol_sets = []
        Vpar_sets = []

        # Vsqrd = lambda p: 1 - (v_thresh + self.model['modB'](p) / B_mirror)
        Vsqrd = lambda p: 1 - (v_thresh + self.model['modB'](p)*B_k2)
        for i, pol_seg in enumerate(pol_segs):
            pol_l1, pol_l2 = pol_seg[0] - 2*pol_stp, np.mean(pol_seg[0:2])
            pol_r1, pol_r2 = np.mean(pol_seg[-2::]), pol_seg[-1] + 2*pol_stp

            ###################################################################
            # This print statement is here to help debug. If you're kind to   #
            # yourself, you won't delete it :-)                               #
            ###################################################################
            if False:
                print('Gridpoints = ({0:0.0f}/{1:0.0f}) (k^2={2:0.2f})'.format(pol_seg.shape[0], pol_well.shape[0], k2))
                print('Left Boundary : Vsqrd({0:0.4f}, {1:0.4f}) = ({2}, {3})'.format(pol_l1, pol_l2, Vsqrd(pol_l1), Vsqrd(pol_l2)))
                print('Right Boundary : Vsqrd({0:0.4f}, {1:0.4f}) = ({2}, {3})'.format(pol_r1, pol_r2, Vsqrd(pol_r1), Vsqrd(pol_r2)))
                print('Poloidal Domain: ({0:0.4f}, {1:0.4f})\n'.format(pol_well[0], pol_well[-1]))

            if (Vsqrd(pol_l1) < 0):
                pol_lft = find_root(Vsqrd, pol_l1, pol_l2)
            else:
                pol_lft = pol_seg[0]

            # print(self.pol_dom_buff[-1]/np.pi)
            # print(pol_r2/np.pi)
            if Vsqrd(pol_r2) < 0:
                pol_rgt = find_root(Vsqrd, pol_r1, pol_r2)
            else:
                pol_rgt = pol_seg[-1]
            
            npts = int((pol_rgt - pol_lft) / (2*pol_stp)) + 1
            if npts > 3:
                if pol_lft >= pol_well[0] and pol_rgt <= pol_well[-1]:
                    pol_set = np.linspace(pol_lft, pol_rgt, npts)
                elif pol_lft >= pol_well[0] and pol_rgt > pol_well[-1]:
                    npts = int((pol_well[-1] - pol_lft) / (2*pol_stp)) + 1
                    pol_set = np.linspace(pol_lft, pol_well[-1], npts)
                elif pol_lft < pol_well[0] and pol_rgt <= pol_well[-1]:
                    npts = int((pol_rgt - pol_well[0]) / (2*pol_stp)) + 1
                    pol_set = np.linspace(pol_well[0], pol_rgt, npts)
                else:
                    npts = int((pol_well[-1] - pol_well[0]) / (2*pol_stp)) + 1
                    pol_set = np.linspace(pol_well[0], pol_well[-1], npts)
                
                Vpar_set = np.sqrt(1 - (self.model['modB'](pol_set)*B_k2))
                pol_sets.append(pol_set)
                Vpar_sets.append(Vpar_set)

            else:
                pol_sets.append(None)
                Vpar_sets.append(None)

        return pol_sets, Vpar_sets

    def calculate_p_weight(self, exp_order=4, N=4, M=1, base_scale=0.25, ax=None):
        """ Calculate the p-weighting function over the entire flux-tube.
        Singularities at the local minimum are treated by a Taylor expansion
        about the stationary point.

        Parameters
        ----------
        exp_order : int
            Order of the Taylor expansion.
        N : int (optional)
            Toroidal component of helical symmetry ratio N/M. Default is 4.
        M : int (optional)
            Poloidal component of helical symmetry ratio N/M. Default is 1.
        base_scale : float (optional)
            The baseline scale that determines the width of the domain over which
            the weighting function is approximated via expansion. Default is 0.25.
        ax : obj (optional)
            Axis on which to plot p-weight function. Default is None.
        """
        """
        # Get left and right trapping boundaries #
        # idx_lft, idx_rgt = self.idx_max[0], self.idx_max[-1]
        idx_lft, idx_min, idx_rgt = self.partition['well 0']['idx']
        for key, data in self.partition.items():
            idx_chk = data['idx']
            if idx_chk[0] < idx_lft:
                idx_lft = idx_chk[0]
            elif idx_chk[2] > idx_rgt:
                idx_rgt = idx_chk[2]
        """
        # check for buffered wells #
        try:
            the_well = self.pol_dom_buff
        except AttributeError:
            self.partition_wells()
            the_well = self.pol_dom_buff
        
        B_well = self.data_buff['modB']
        dB_well = self.data_buff['dB_dThe']

        # Find local minima in B #
        min_pol = the_well[argrelmin(B_well, order=1)]
        max_pol = np.empty(min_pol.shape[0]-1)

        for i, pol in enumerate(min_pol[1::]):
            max_idx = the_well[the_well < min_pol[i]].shape[0] + np.argmax(B_well[(the_well >= min_pol[i]) & (the_well <= pol)]) - 1
            max_pol[i] = the_well[max_idx]
        
        min_pol = np.array(min_pol)
        max_pol = np.array(max_pol)

        max_pol = np.insert(max_pol, 0, the_well[0])
        max_pol = np.append(max_pol, the_well[-1])

        # Calculate well expansion gaps #
        base_gap = base_scale/(N/self.q0 - M)
        exp_gap = np.empty(min_pol.shape)
        for max_idx, max_val in enumerate(max_pol[1::]):
            well_width = max_val - max_pol[max_idx]
            if well_width > base_gap:
                exp_gap[max_idx] = base_gap
            else:
                exp_gap[max_idx] = 0.5*well_width

        # Get phase-space weighting function #
        the_fit = np.empty(0)
        p_wght_fit = np.empty(0)
        for i, pol in enumerate(min_pol):
            B_min = self.model['modB'](pol)
            gap = exp_gap[i]

            if i == 0:
                the_lft = the_well[the_well < pol - gap]
                the_rgt = the_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                B_lft = B_well[the_well < pol - gap]
                B_rgt = B_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                dB_lft = dB_well[the_well < pol - gap]
                dB_rgt = dB_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                the_gap, p_wght_gap = self.expansion_p_weight(pol, gap, N=exp_order)
                if len(the_lft) == 0:
                    the_lft = np.array([the_well[0]])
                    B_lft = np.array([B_well[0]])
                    dB_lft = np.array([dB_well[0]])

                    the_gap = the_gap[1::]
                    p_wght_gap = p_wght_gap[1::]

                p_wght_lft = (B_min * np.abs(dB_lft)) / ((B_lft**2)*np.sqrt(1. - B_min/B_lft))
                p_wght_rgt = (B_min * np.abs(dB_rgt)) / ((B_rgt**2)*np.sqrt(1. - B_min/B_rgt))

                is_nan = np.isnan(p_wght_gap)
                not_nan = ~is_nan
                the_gap = the_gap[not_nan]
                p_wght_gap = p_wght_gap[not_nan]

                the_fit = np.append(the_fit, the_lft)
                the_fit = np.append(the_fit, the_gap)
                the_fit = np.append(the_fit, the_rgt)

                p_wght_fit = np.append(p_wght_fit, p_wght_lft)
                p_wght_fit = np.append(p_wght_fit, p_wght_gap)
                p_wght_fit = np.append(p_wght_fit, p_wght_rgt)

                if ax:
                    ax.plot(the_lft/np.pi, p_wght_lft, c='tab:blue')
                    ax.plot(the_gap/np.pi, p_wght_gap, c='tab:blue', ls='--')
                    ax.plot(the_rgt/np.pi, p_wght_rgt, c='tab:blue')

                    if (len(the_lft) > 0) and (len(the_rgt) > 0):
                        ax.scatter([the_lft[-1]/np.pi, the_rgt[0]/np.pi], [p_wght_lft[-1], p_wght_rgt[0]], c='tab:blue', s=50)
                    elif len(the_lft) > 0:
                        ax.scatter([the_lft[-1]/np.pi], [p_wght_lft[-1]], c='tab:blue', s=50)
                    elif len(the_rgt) > 0:
                        ax.scatter([the_rgt[0]/np.pi], [p_wght_rgt[0]], c='tab:blue', s=50)

            elif i < min_pol.shape[0]-1:
                the_lft = the_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                the_rgt = the_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                B_lft = B_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                B_rgt = B_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]
                
                dB_lft = dB_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                dB_rgt = dB_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                p_wght_lft = (B_min * np.abs(dB_lft)) / ((B_lft**2)*np.sqrt(1. - B_min/B_lft))
                the_gap, p_wght_gap = self.expansion_p_weight(pol, gap, N=exp_order)
                p_wght_rgt = (B_min * np.abs(dB_rgt)) / ((B_rgt**2)*np.sqrt(1. - B_min/B_rgt))

                is_nan = np.isnan(p_wght_gap)
                not_nan = ~is_nan
                the_gap = the_gap[not_nan]
                p_wght_gap = p_wght_gap[not_nan]

                the_fit = np.append(the_fit, the_lft)
                the_fit = np.append(the_fit, the_gap)
                the_fit = np.append(the_fit, the_rgt)

                p_wght_fit = np.append(p_wght_fit, p_wght_lft)
                p_wght_fit = np.append(p_wght_fit, p_wght_gap)
                p_wght_fit = np.append(p_wght_fit, p_wght_rgt)

                if ax:
                    ax.plot(the_lft/np.pi, p_wght_lft, c='tab:blue')
                    ax.plot(the_gap/np.pi, p_wght_gap, c='tab:blue', ls='--')
                    ax.plot(the_rgt/np.pi, p_wght_rgt, c='tab:blue')

                    if (len(the_lft) > 0) and (len(the_rgt) > 0):
                        ax.scatter([the_lft[-1]/np.pi, the_rgt[0]/np.pi], [p_wght_lft[-1], p_wght_rgt[0]], c='tab:blue', s=50)
                    elif len(the_lft) > 0:
                        ax.scatter([the_lft[-1]/np.pi], [p_wght_lft[-1]], c='tab:blue', s=50)
                    elif len(the_rgt) > 0:
                        ax.scatter([the_rgt[0]/np.pi], [p_wght_rgt[0]], c='tab:blue', s=50)

            else:
                the_lft = the_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                the_rgt = the_well[the_well > pol + gap]

                B_lft = B_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                B_rgt = B_well[the_well > pol + gap]

                dB_lft = dB_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                dB_rgt = dB_well[the_well > pol + gap]

                the_gap, p_wght_gap = self.expansion_p_weight(pol, gap, N=exp_order)
                if len(the_rgt) == 0:
                    the_rgt = np.array([the_well[-1]])
                    B_rgt = np.array([B_well[0]])
                    dB_rgt = np.array([dB_well[0]])

                    the_gap = the_gap[0:-1]
                    p_wght_gap = p_wght_gap[0:-1]

                p_wght_lft = (B_min * np.abs(dB_lft)) / ((B_lft**2)*np.sqrt(1. - B_min/B_lft))
                p_wght_rgt = (B_min * np.abs(dB_rgt)) / ((B_rgt**2)*np.sqrt(1. - B_min/B_rgt))

                is_nan = np.isnan(p_wght_gap)
                not_nan = ~is_nan
                the_gap = the_gap[not_nan]
                p_wght_gap = p_wght_gap[not_nan]

                the_fit = np.append(the_fit, the_lft)
                the_fit = np.append(the_fit, the_gap)
                the_fit = np.append(the_fit, the_rgt)

                p_wght_fit = np.append(p_wght_fit, p_wght_lft)
                p_wght_fit = np.append(p_wght_fit, p_wght_gap)
                p_wght_fit = np.append(p_wght_fit, p_wght_rgt)

                if ax:
                    ax.plot(the_lft/np.pi, p_wght_lft, c='tab:blue')
                    ax.plot(the_gap/np.pi, p_wght_gap, c='tab:blue', ls='--')
                    ax.plot(the_rgt/np.pi, p_wght_rgt, c='tab:blue')

                    if (len(the_lft) > 0) and (len(the_rgt) > 0):
                        ax.scatter([the_lft[-1]/np.pi, the_rgt[0]/np.pi], [p_wght_lft[-1], p_wght_rgt[0]], c='tab:blue', s=50)
                    elif len(the_lft) > 0:
                        ax.scatter([the_lft[-1]/np.pi], [p_wght_lft[-1]], c='tab:blue', s=50)
                    elif len(the_rgt) > 0:
                        ax.scatter([the_rgt[0]/np.pi], [p_wght_rgt[0]], c='tab:blue', s=50)

        self.model['p-weight'] = interp1d(the_fit, p_wght_fit, kind='linear')
        self.data['p-weight'] = self.model['p-weight'](self.pol_dom)
        # self.pol_weight_dom = the_well

    def calculate_p_weight_well(self, well_ID, exp_order=4, N=4, M=1, base_scale=0.25, ax=None):
        """ Calculate the p-weighting function over a single trapping well.
        Singularities at the local minimum are treated by a Taylor expansion
        about the stationary point.

        Parameters
        ----------
        well_ID : int
            Index of the trapping well being considered.
        exp_order : int, optional
            Order of the Taylor expansion. Default is 4.
        N : int, optional
            Toroidal component of helical symmetry ratio N/M. Default is 4.
        M : int, optional
            Poloidal component of helical symmetry ratio N/M. Default is 1.
        base_scale : float, optional
            The baseline scale that determines the width of the domain over which
            the weighting function is approximated via expansion. Default is 0.25.
        ax : obj, optional
            Axis on which to plot p-weight function. Default is None.
        """
        # Parition helically connected magnetic wells #
        well_key = 'well {}'.format(well_ID)
        if not well_key in self.partition:
            raise IndexError('well_ID = {} is outside the parallel domain of the gist file.'.format(well_ID))

        # Identify trapping extrema in magnetic field data #
        B_max_lft, B_min, B_max_rgt = self.partition[well_key]['B']
        B_max = min([B_max_lft, B_max_rgt])
        B_extrema = np.array([B_min, B_max])

        # Identify trapping indices in magnetic field data #
        idx_max = np.array([self.partition[well_key]['idx'][0], self.partition[well_key]['idx'][2]], dtype=int)
        while B_max_lft > B_max:
            idx_max[0] += 1
            B_max_lft = self.data['modB'][idx_max[0]]
        while B_max_rgt > B_max:
            idx_max[1] -= 1
            B_max_rgt = self.data['modB'][idx_max[1]]

        the_well = self.pol_dom[idx_max[0]:idx_max[1]+1]
        B_well = self.data['modB'][idx_max[0]:idx_max[1]+1]
        dB_well = self.data['dB_dThe'][idx_max[0]:idx_max[1]+1]

        # Find local minima in B #
        min_pol = the_well[argrelmin(B_well, order=1)]
        max_pol = np.empty(min_pol.shape[0]-1)

        for i, pol in enumerate(min_pol[1::]):
            max_idx = the_well[the_well < min_pol[i]].shape[0] + np.argmax(B_well[(the_well >= min_pol[i]) & (the_well <= pol)]) - 1
            max_pol[i] = the_well[max_idx]
        
        min_pol = np.array(min_pol)
        max_pol = np.array(max_pol)

        max_pol = np.insert(max_pol, 0, the_well[0])
        max_pol = np.append(max_pol, the_well[-1])

        # Calculate well expansion gaps #
        base_gap = base_scale/(N/self.q0 - M)
        exp_gap = np.empty(min_pol.shape)
        for max_idx, max_val in enumerate(max_pol[1::]):
            well_width = max_val - max_pol[max_idx]
            if well_width > base_gap:
                exp_gap[max_idx] = base_gap
            else:
                exp_gap[max_idx] = 0.5*well_width

        # Get phase-space weighting function #
        the_fit = np.empty(0)
        p_wght_fit = np.empty(0)
        for i, pol in enumerate(min_pol):
            B_min = self.model['modB'](pol)
            gap = exp_gap[i]

            if i == 0:
                the_lft = the_well[the_well < pol - gap]
                the_rgt = the_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                B_lft = B_well[the_well < pol - gap]
                B_rgt = B_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                dB_lft = dB_well[the_well < pol - gap]
                dB_rgt = dB_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                the_gap, p_wght_gap = self.expansion_p_weight(pol, gap, N=exp_order)
                if len(the_lft) == 0:
                    the_lft = np.array([the_well[0]])
                    B_lft = np.array([B_well[0]])
                    dB_lft = np.array([dB_well[0]])

                    the_gap = the_gap[1::]
                    p_wght_gap = p_wght_gap[1::]

                p_wght_lft = (B_min * np.abs(dB_lft)) / ((B_lft**2)*np.sqrt(1. - B_min/B_lft))
                p_wght_rgt = (B_min * np.abs(dB_rgt)) / ((B_rgt**2)*np.sqrt(1. - B_min/B_rgt))

                is_nan = np.isnan(p_wght_gap)
                not_nan = ~is_nan
                the_gap = the_gap[not_nan]
                p_wght_gap = p_wght_gap[not_nan]

                the_fit = np.append(the_fit, the_lft)
                the_fit = np.append(the_fit, the_gap)
                the_fit = np.append(the_fit, the_rgt)

                p_wght_fit = np.append(p_wght_fit, p_wght_lft)
                p_wght_fit = np.append(p_wght_fit, p_wght_gap)
                p_wght_fit = np.append(p_wght_fit, p_wght_rgt)

                if ax:
                    ax.plot(the_lft/np.pi, p_wght_lft, c='tab:blue')
                    ax.plot(the_gap/np.pi, p_wght_gap, c='tab:blue', ls='--')
                    ax.plot(the_rgt/np.pi, p_wght_rgt, c='tab:blue')

                    if (len(the_lft) > 0) and (len(the_rgt) > 0):
                        ax.scatter([the_lft[-1]/np.pi, the_rgt[0]/np.pi], [p_wght_lft[-1], p_wght_rgt[0]], c='tab:blue', s=50)
                    elif len(the_lft) > 0:
                        ax.scatter([the_lft[-1]/np.pi], [p_wght_lft[-1]], c='tab:blue', s=50)
                    elif len(the_rgt) > 0:
                        ax.scatter([the_rgt[0]/np.pi], [p_wght_rgt[0]], c='tab:blue', s=50)

            elif i < min_pol.shape[0]-1:
                the_lft = the_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                the_rgt = the_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                B_lft = B_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                B_rgt = B_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]
                
                dB_lft = dB_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                dB_rgt = dB_well[(the_well > pol + gap) & (the_well <= max_pol[i+1])]

                p_wght_lft = (B_min * np.abs(dB_lft)) / ((B_lft**2)*np.sqrt(1. - B_min/B_lft))
                the_gap, p_wght_gap = self.expansion_p_weight(pol, gap, N=exp_order)
                p_wght_rgt = (B_min * np.abs(dB_rgt)) / ((B_rgt**2)*np.sqrt(1. - B_min/B_rgt))

                is_nan = np.isnan(p_wght_gap)
                not_nan = ~is_nan
                the_gap = the_gap[not_nan]
                p_wght_gap = p_wght_gap[not_nan]

                the_fit = np.append(the_fit, the_lft)
                the_fit = np.append(the_fit, the_gap)
                the_fit = np.append(the_fit, the_rgt)

                p_wght_fit = np.append(p_wght_fit, p_wght_lft)
                p_wght_fit = np.append(p_wght_fit, p_wght_gap)
                p_wght_fit = np.append(p_wght_fit, p_wght_rgt)

                if ax:
                    ax.plot(the_lft/np.pi, p_wght_lft, c='tab:blue')
                    ax.plot(the_gap/np.pi, p_wght_gap, c='tab:blue', ls='--')
                    ax.plot(the_rgt/np.pi, p_wght_rgt, c='tab:blue')

                    if (len(the_lft) > 0) and (len(the_rgt) > 0):
                        ax.scatter([the_lft[-1]/np.pi, the_rgt[0]/np.pi], [p_wght_lft[-1], p_wght_rgt[0]], c='tab:blue', s=50)
                    elif len(the_lft) > 0:
                        ax.scatter([the_lft[-1]/np.pi], [p_wght_lft[-1]], c='tab:blue', s=50)
                    elif len(the_rgt) > 0:
                        ax.scatter([the_rgt[0]/np.pi], [p_wght_rgt[0]], c='tab:blue', s=50)

            else:
                the_lft = the_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                the_rgt = the_well[the_well > pol + gap]

                B_lft = B_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                B_rgt = B_well[the_well > pol + gap]

                dB_lft = dB_well[(the_well > max_pol[i]) & (the_well < pol - gap)]
                dB_rgt = dB_well[the_well > pol + gap]

                the_gap, p_wght_gap = self.expansion_p_weight(pol, gap, N=exp_order)
                if len(the_rgt) == 0:
                    the_rgt = np.array([the_well[-1]])
                    B_rgt = np.array([B_well[0]])
                    dB_rgt = np.array([dB_well[0]])

                    the_gap = the_gap[0:-1]
                    p_wght_gap = p_wght_gap[0:-1]

                p_wght_lft = (B_min * np.abs(dB_lft)) / ((B_lft**2)*np.sqrt(1. - B_min/B_lft))
                p_wght_rgt = (B_min * np.abs(dB_rgt)) / ((B_rgt**2)*np.sqrt(1. - B_min/B_rgt))

                is_nan = np.isnan(p_wght_gap)
                not_nan = ~is_nan
                the_gap = the_gap[not_nan]
                p_wght_gap = p_wght_gap[not_nan]

                the_fit = np.append(the_fit, the_lft)
                the_fit = np.append(the_fit, the_gap)
                the_fit = np.append(the_fit, the_rgt)

                p_wght_fit = np.append(p_wght_fit, p_wght_lft)
                p_wght_fit = np.append(p_wght_fit, p_wght_gap)
                p_wght_fit = np.append(p_wght_fit, p_wght_rgt)

                if ax:
                    ax.plot(the_lft/np.pi, p_wght_lft, c='tab:blue')
                    ax.plot(the_gap/np.pi, p_wght_gap, c='tab:blue', ls='--')
                    ax.plot(the_rgt/np.pi, p_wght_rgt, c='tab:blue')

                    if (len(the_lft) > 0) and (len(the_rgt) > 0):
                        ax.scatter([the_lft[-1]/np.pi, the_rgt[0]/np.pi], [p_wght_lft[-1], p_wght_rgt[0]], c='tab:blue', s=50)
                    elif len(the_lft) > 0:
                        ax.scatter([the_lft[-1]/np.pi], [p_wght_lft[-1]], c='tab:blue', s=50)
                    elif len(the_rgt) > 0:
                        ax.scatter([the_rgt[0]/np.pi], [p_wght_rgt[0]], c='tab:blue', s=50)

        self.partition[well_key]['p-weight'] = interp1d(the_fit, p_wght_fit)
        return the_fit, p_wght_fit

    def calculate_omega_de(self, curve_method='GIST', integ_method='gtrapz'):
        """ Calculate omega_de along entire magnetic field-line.
        """
        """
        # Get left and right trapping boundaries #
        # idx_lft, idx_rgt = self.idx_max[0], self.idx_max[-1]
        idx_lft, idx_min, idx_rgt = self.partition['well 0']['idx']
        for key, data in self.partition.items():
            idx_chk = data['idx']
            if idx_chk[0] < idx_lft:
                idx_lft = idx_chk[0]
            elif idx_chk[2] > idx_rgt:
                idx_rgt = idx_chk[2]
        """

        # check for buffered wells #
        try:
            pol_well = self.pol_dom_buff
        except AttributeError:
            self.partition_wells()
            pol_well = self.pol_dom_buff

        B_well = self.data_buff['modB']

        # Calculate poloidal bounce angle, <\omega_D>, and the phase space #
        # weighting factor as functions of the trapping parameter k2 #
        well = []
        B_extrema = np.array([np.min(B_well), np.max(B_well)])
        k2_dom = np.linspace(0, 1, 101, endpoint=False)
        for mdx, k2 in enumerate(k2_dom):
            # print('({0:0.0f}|{1:0.0f})'.format(mdx+1, k2_dom.shape[0]))
            pol_set, Vpar_set = self.calc_Vpar(pol_well, B_well, k2, B_extrema)
            for pdx, pol in enumerate(pol_set):
                if pol is not None:
                    v_par = Vpar_set[pdx]
                    v_par2 = v_par*v_par
                    B = self.model['modB'](pol)

                    # calculate curvature terms #
                    if curve_method == 'NE3DLE':
                        sqrt_gxx = np.sqrt(self.model['gxx'](pol))
                        jacob = self.model['Jacob'](pol)
                        Kn = self.model['Kn'](pol)
                        Kg = self.model['Kg'](pol)
                        D = self.model['D'](pol)
                    
                        integ_scale = jacob * B / v_par
                        curve_drive = (Kn/sqrt_gxx) + (D*Kg*(sqrt_gxx/B))

                        dJ_dPsi_intg = ((2*(v_par2))/(self.nfp*self.q0 - 1)) * (self.shat/np.sqrt(self.s0))
                        dJ_dPsi_intg = dJ_dPsi_intg - ((v_par2)+1)*curve_drive
                        dJ_dPsi_intg = dJ_dPsi_intg - (1-(v_par2))*self.my_dpdx/(B**2)
                        dJ_dPsi_intg = integ_scale * dJ_dPsi_intg

                    elif curve_method == 'GIST':
                        Bhat = self.model['modB'](pol)
                        jacob = self.model['Jacob'](pol)
                        L1 = self.model['L1'](pol)
                        L2 = self.model['L2'](pol)
                        curve_drive_alf = (1./Bhat)*(((1-v_par)**2 + 2*v_par2)*L2 - (self.my_dpdx/Bhat)*v_par2)
                        curve_drive_psi = (1./Bhat)*((1-v_par)**2 + 2*v_par2)*L1
                        # K2 = self.model['K2'](pol)
                        # curve_drive = (1-v_par2)*L2 + (2./B)*(1-(1-v_par2)*B)*K2

                        integ_scale = jacob*B/v_par
                        dJ_dPsi_intg = integ_scale * curve_drive_alf
                        dJ_dAlf_intg = integ_scale * curve_drive_psi

                    # perform bounce-average integral #
                    if integ_method == 'quad':
                        dJ_dPsi_model = interp1d(pol, dJ_dPsi_intg, kind='quadratic')
                        dJ_dAlf_model = interp1d(pol, dJ_dAlf_intg, kind='quadratic')
                        dJ_dK_model = interp1d(pol, integ_scale, kind='quadratic')
                        dJ_dPsi = quadrature(dJ_dPsi_model, pol[0], pol[-1], maxiter=5000)[0]
                        dJ_dAlf = quadrature(dJ_dAlf_model, pol[0], pol[-1], maxiter=5000)[0]
                        dJ_dK = quadrature(dJ_dK_model, pol[0], pol[-1], maxiter=5000)[0]

                    elif integ_method == 'gtrapz':
                        integ_scale = integ_scale * v_par
                        dJ_dPsi_intg = dJ_dPsi_intg * v_par
                        dJ_dAlf_intg = dJ_dAlf_intg * v_par

                        dpol = pol[1:]-pol[0:-1]
                        dv2 = v_par[1:]**2 - v_par[0:-1]**2
                        dv3 = v_par[1:]**3 - v_par[0:-1]**3

                        dh_bnc = integ_scale[1:] - integ_scale[0:-1]
                        dhv_bnc = integ_scale[1:]*v_par[1:] - integ_scale[0:-1]*v_par[0:-1]

                        dh_psi = dJ_dPsi_intg[1:] - dJ_dPsi_intg[0:-1]
                        dhv_psi = dJ_dPsi_intg[1:]*v_par[1:] - dJ_dPsi_intg[0:-1]*v_par[0:-1]

                        dh_alf = dJ_dAlf_intg[1:] - dJ_dAlf_intg[0:-1]
                        dhv_alf = dJ_dAlf_intg[1:]*v_par[1:] - dJ_dAlf_intg[0:-1]*v_par[0:-1]

                        dJ_dPsi = np.empty(dpol.shape)
                        dJ_dAlf = np.empty(dpol.shape)
                        dJ_dK = np.empty(dpol.shape)

                        v_thresh = 1e-10
                        s_idx = np.where((np.abs(dv2) < v_thresh) & (np.abs(dv3) < v_thresh))[0]
                        nos_idx = np.where((np.abs(dv2) >= v_thresh) & (np.abs(dv3) >= v_thresh))[0]

                        dJ_dK[nos_idx] = (2*dpol[nos_idx]*dhv_bnc[nos_idx]/dv2[nos_idx])-(4./3)*(dpol[nos_idx]*dh_bnc[nos_idx]*dv3[nos_idx]/(dv2[nos_idx]**2))
                        dJ_dK[s_idx] = .5*dpol[s_idx]*((integ_scale[0:-1]/v_par[0:-1])+(integ_scale[1:]/v_par[1:]))[s_idx] 
                        dJ_dK = np.sum(dJ_dK)

                        dJ_dPsi[nos_idx] = (2*dpol[nos_idx]*dhv_psi[nos_idx]/dv2[nos_idx])-(4./3)*(dpol[nos_idx]*dh_psi[nos_idx]*dv3[nos_idx]/(dv2[nos_idx]**2))
                        dJ_dPsi[s_idx] = .5*dpol[s_idx]*((dJ_dPsi_intg[0:-1]/v_par[0:-1])+(dJ_dPsi_intg[1:]/v_par[1:]))[s_idx] 
                        dJ_dPsi = np.sum(dJ_dPsi)

                        dJ_dAlf[nos_idx] = (2*dpol[nos_idx]*dhv_alf[nos_idx]/dv2[nos_idx])-(4./3)*(dpol[nos_idx]*dh_alf[nos_idx]*dv3[nos_idx]/(dv2[nos_idx]**2))
                        dJ_dAlf[s_idx] = .5*dpol[s_idx]*((dJ_dAlf_intg[0:-1]/v_par[0:-1])+(dJ_dAlf_intg[1:]/v_par[1:]))[s_idx] 
                        dJ_dAlf = np.sum(dJ_dAlf)

                    elif integ_method == 'trapz':
                        dJ_dPsi = np.trapz(dJ_dPsi_intg, pol)
                        dJ_dAlf = np.trapz(dJ_dAlf_intg, pol)
                        dJ_dK = np.trapz(integ_scale, pol)

                    omg_alf = dJ_dPsi/dJ_dK
                    omg_psi = dJ_dAlf/dJ_dK
                    well.append(np.array([pol[0], k2, omg_alf, omg_psi]))
                    well.append(np.array([pol[-1], k2, omg_alf, omg_psi]))

        # Sort <\omega_D> and phase-space weighting in order of ascending poloidal bounce angle #
        well = np.array(sorted(well, key=lambda x: x[0]))
        # self.pol_well = pol_well
        # self.B_well = B_well

        # Define the poloidal bounce angle, <\omega_D>, phase-space #
        # weighting, and trapping parameter #
        # npts = int(1 + (well[-1, 0] - well[0, 0])/(2*self.hlf_stp))
        # self.pol_bnc = np.linspace(well[0, 0], well[-1, 0], npts)

        pol_well = well[:,0]
        M_well = well[:,1]
        oma_well = well[:,2]
        omp_well = well[:,3]

        if well[0,0] > self.pol_dom[0]:
            oma_fit = np.polyfit(pol_well[0:10], oma_well[0:10], 2)
            oma_ext = oma_fit[0]*(self.pol_dom[0]**2) + oma_fit[1]*self.pol_dom[0] + oma_fit[0]
            oma_well = np.insert(oma_well, 0, oma_ext)

            omp_fit = np.polyfit(pol_well[0:10], omp_well[0:10], 2)
            omp_ext = omp_fit[0]*(self.pol_dom[0]**2) + omp_fit[1]*self.pol_dom[0] + omp_fit[0]
            omp_well = np.insert(omp_well, 0, omp_ext)

            pol_well = np.insert(pol_well, 0, self.pol_dom[0])
            M_well = np.insert(M_well, 0, 1)

        if well[-1,0] < self.pol_dom[-1]:
            oma_fit = np.polyfit(pol_well[-10:], oma_well[-10:], 2)
            oma_ext = oma_fit[0]*(self.pol_dom[-1]**2) + oma_fit[1]*self.pol_dom[-1] + oma_fit[0]
            oma_well = np.append(oma_well, oma_ext)

            omp_fit = np.polyfit(pol_well[-10:], omp_well[-10:], 2)
            omp_ext = omp_fit[0]*(self.pol_dom[-1]**2) + omp_fit[1]*self.pol_dom[-1] + omp_fit[0]
            omp_well = np.append(omp_well, omp_ext)

            pol_well = np.append(pol_well, self.pol_dom[-1])
            M_well = np.append(M_well, 1)

        self.model['M'] = interp1d(pol_well, M_well)
        self.model['omega_alpha'] = interp1d(pol_well, oma_well)
        self.model['omega_psi'] = interp1d(pol_well, omp_well)

        # print('\n{0:0.2f} --> {1:0.2f}'.format(self.pol_dom[0]/np.pi, self.pol_dom[-1]/np.pi))
        # print('M(\\theta) = {0:0.2f} --> {1:0.2f}\n'.format(well[0,1], well[-1,1]))
        # print('\omega_a(\\theta) = {0:0.2f} --> {1:0.2f}\n'.format(well[0,2], well[-1,2]))
        # print('\omega_p(\\theta) = {0:0.2f} --> {1:0.2f}\n'.format(well[0,3], well[-1,3]))

        self.data['M'] = self.model['M'](self.pol_dom)
        self.data['omega_alpha'] = self.model['omega_alpha'](self.pol_dom)
        self.data['omega_psi'] = self.model['omega_psi'](self.pol_dom)

    def calculate_omega_de_well(self, well_ID, curve_method='GIST', integ_method='gtrapz'):
        """ Calculate omega_de in a single magnetic trapping well

        Parameters
        ----------
        well_ID : int
            Numerical identifier for trapping well being considered.
        curve_method : str, optional
            The method by which the curvature drive is calulated. The options are either
            GIST or NE3DLE. Default is GIST
        integ_method : str, optional
            The method by which the bounce-averaged integral is evaluated. The options are
            quad, gtrapz, or trapz. Default is gtrapz.
        """
        # Parition helically connected magnetic wells #
        well_key = 'well {}'.format(well_ID)
        if not well_key in self.partition:
            raise IndexError('well_ID = {} is outside the parallel domain of the gist file.'.format(well_ID))

        # Identify trapping extrema in magnetic field data #
        B_max_lft, B_min, B_max_rgt = self.partition[well_key]['B']
        B_max = min([B_max_lft, B_max_rgt])
        B_extrema = np.array([B_min, B_max])

        # Identify trapping indices in magnetic field data #
        idx_max = np.array([self.partition[well_key]['idx'][0], self.partition[well_key]['idx'][2]], dtype=int)
        while B_max_lft > B_max:
            idx_max[0] += 1
            B_max_lft = self.data['modB'][idx_max[0]]
        while B_max_rgt > B_max:
            idx_max[1] -= 1
            B_max_rgt = self.data['modB'][idx_max[1]]

        # Define partitioned data in the selected magnetic well #
        pol_well = self.pol_dom[idx_max[0]:idx_max[1]+1]
        B_well = self.data['modB'][idx_max[0]:idx_max[1]+1]
        
        # Calculate poloidal bounce angle, <\omega_D>, and the phase space #
        # weighting factor as functions of the trapping parameter M #
        well = []
        M_dom = np.linspace(0, 1, 1001, endpoint=False)
        for mdx, M in enumerate(M_dom):
            pol_set, Vpar_set = self.calc_Vpar(pol_well, B_well, M, B_extrema)
            for pdx, pol in enumerate(pol_set):
                if pol is not None:
                    v_par = Vpar_set[pdx]
                    v_par2 = v_par*v_par
                    B = self.model['modB'](pol)
                    if curve_method == 'NE3DLE':
                        sqrt_gxx = np.sqrt(self.model['gxx'](pol))
                        jacob = self.model['Jacob'](pol)
                        Kn = self.model['Kn'](pol)
                        Kg = self.model['Kg'](pol)
                        D = self.model['D'](pol)
                        
                        integ_scale = jacob * B / v_par
                        curve_drive = (Kn/sqrt_gxx) + (D*Kg*(sqrt_gxx/B))
                        
                        dJ_dPsi_intg = ((2*(v_par2))/(self.nfp*self.q0 - 1)) * (self.shat/np.sqrt(self.s0))
                        dJ_dPsi_intg = dJ_dPsi_intg - ((v_par2)+1)*curve_drive
                        dJ_dPsi_intg = dJ_dPsi_intg - (1-(v_par2))*self.my_dpdx/(B**2)
                        dJ_dPsi_intg = integ_scale * dJ_dPsi_intg
                    elif curve_method == 'GIST':
                        jacob = self.model['Jacob'](pol)
                        L2 = self.model['L2'](pol)
                        K2 = self.model['K2'](pol)
                        curve_drive = (1-v_par2)*L2 + (2./B)*(1-(1-v_par2)*B)*K2
                        
                        integ_scale = jacob*B/v_par
                        dJ_dPsi_intg = integ_scale*curve_drive

                    if integ_method == 'quad':
                        dJ_dPsi_model = interp1d(pol, dJ_dPsi_intg, kind='quadratic')
                        dJ_dK_model = interp1d(pol, integ_scale, kind='quadratic')
                        dJ_dPsi = quadrature(dJ_dPsi_model, pol[0], pol[-1], maxiter=5000)[0]
                        dJ_dK = quadrature(dJ_dK_model, pol[0], pol[-1], maxiter=5000)[0]
                    elif integ_method == 'gtrapz':
                        integ_scale = integ_scale * v_par
                        dJ_dPsi_intg = dJ_dPsi_intg * v_par

                        dpol = pol[1:]-pol[0:-1]
                        dv2 = v_par[1:]**2 - v_par[0:-1]**2
                        dv3 = v_par[1:]**3 - v_par[0:-1]**3

                        dh_bnc = integ_scale[1:] - integ_scale[0:-1]
                        dhv_bnc = integ_scale[1:]*v_par[1:] - integ_scale[0:-1]*v_par[0:-1]

                        dh_crv = dJ_dPsi_intg[1:] - dJ_dPsi_intg[0:-1]
                        dhv_crv = dJ_dPsi_intg[1:]*v_par[1:] - dJ_dPsi_intg[0:-1]*v_par[0:-1]

                        dJ_dPsi = np.empty(dpol.shape)
                        dJ_dK = np.empty(dpol.shape)

                        v_thresh = 1e-10
                        s_idx = np.where((np.abs(dv2) < v_thresh) & (np.abs(dv3) < v_thresh))[0]
                        nos_idx = np.where((np.abs(dv2) >= v_thresh) & (np.abs(dv3) >= v_thresh))[0]

                        dJ_dK[nos_idx] = (2*dpol[nos_idx]*dhv_bnc[nos_idx]/dv2[nos_idx])-(4./3)*(dpol[nos_idx]*dh_bnc[nos_idx]*dv3[nos_idx]/(dv2[nos_idx]**2))
                        dJ_dK[s_idx] = .5*dpol[s_idx]*((integ_scale[0:-1]/v_par[0:-1])+(integ_scale[1:]/v_par[1:]))[s_idx] 
                        dJ_dK = np.sum(dJ_dK)

                        dJ_dPsi[nos_idx] = (2*dpol[nos_idx]*dhv_crv[nos_idx]/dv2[nos_idx])-(4./3)*(dpol[nos_idx]*dh_crv[nos_idx]*dv3[nos_idx]/(dv2[nos_idx]**2))
                        dJ_dPsi[s_idx] = .5*dpol[s_idx]*((dJ_dPsi_intg[0:-1]/v_par[0:-1])+(dJ_dPsi_intg[1:]/v_par[1:]))[s_idx] 
                        dJ_dPsi = np.sum(dJ_dPsi)
                    elif integ_method == 'trapz':
                        dJ_dPsi = np.trapz(dJ_dPsi_intg, pol)
                        dJ_dK = np.trapz(integ_scale, pol)

                    omD = dJ_dPsi/dJ_dK
                    well.append(np.array([pol[0], M, omD]))
                    well.append(np.array([pol[-1], M, omD]))

        # Sort <\omega_D> in order of ascending poloidal bounce angle #
        well = np.array(sorted(well, key=lambda x: x[0]))
        self.partition[well_key]['omega_de'] = interp1d(well[:,0], well[:, 2])
        return well[:, 0], well[:,2]

    def plotting(self, fontsize=14, labelsize=18, linewidth=1):
        """ Define plotting parameters 

        Parameters
        ----------
        fontsize : int, optional
            Figure font size. Default is 14.

        labelsize : int, optional
            Figure label size. Default is 18.

        linewidth : int, optional
            Figure line width. Degault is 1.
        """
        plt.close('all')

        font = {'family': 'sans-serif',
                'serif': 'Times New Roman',
                'weight': 'normal',
                'size': fontsize}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = labelsize
        mpl.rcParams['lines.linewidth'] = linewidth

    def curve_drive_velocity_plot(self, well_ID, twn_label=True, make_cb=True):
        """
        # Parition helically connected magnetic wells #
        self.partition_wells()
        idx_lim = -np.floor(0.5*self.pol_min.shape[0])
        well_indices = np.arange(idx_lim, -idx_lim+1, dtype=int)
        if (well_ID < well_indices[0]) or (well_ID > well_indices[-1]):
            raise IndexError('well_ID = {} is outside the parallel domain of the gist file.'.format(well_ID))
        well_idx = np.argmin(np.abs(well_indices - well_ID))
        """
        # Identify trapping extrema in magnetic field data #
        well_key = 'well {}'.format(well_ID)
        B_min = self.partition[well_key]['B'][1]  # self.B_min[well_idx]
        B_max_lft = self.partition[well_key]['B'][0]  # self.B_max[well_idx]
        B_max_rgt = self.partition[well_key]['B'][2]  # self.B_max[well_idx+1]
        B_max_set = np.array([B_max_lft, B_max_rgt])
        B_max = np.min(B_max_set)
        B_extrema = np.array([B_min, B_max])

        # Identify trapping indices in magnetic field data #
        """
        idx_max = np.array([self.idx_max[well_idx], self.idx_max[well_idx+1]], dtype=int)
        while B_max_lft > B_max:
            idx_max[0] += 1
            B_max_lft = self.data['modB'][idx_max[0]]

        while B_max_rgt > B_max:
            idx_max[1] -= 1
            B_max_rgt = self.data['modB'][idx_max[1]]
        """
        # Define partitioned data in the selected magnetic well #
        # pol_well = self.pol_dom[idx_max[0]:idx_max[1]+1]
        # B_well = self.data['modB'][idx_max[0]:idx_max[1]+1]
        idx_set = gist.partition[well_key]['idx']
        pol_well = self.pol_dom[idx_set[0]:idx_set[2]+1]
        B_well = self.data['modB'][idx_set[0]:idx_set[2]+1]
        curve_drive = self.data['L2'][idx_set[0]:idx_set[2]+1]
        """
        pol_idx = np.where((self.pol_dom >= -1.166*np.pi) & (self.pol_dom <= 1.166*np.pi))[0]
        pol_well = self.pol_dom[pol_idx]
        B_well = self.data['modB'][pol_idx]
        B_extrema = np.array([np.min(B_well), np.max(B_well)])

        gradPsi = self.model['gxx'](pol_well)
        Kn = self.model['Kn'](pol_well)
        Kg = self.model['Kg'](pol_well)
        D = self.model['D'](pol_well)
        curve_drive = Kn / gradPsi + D * Kg * gradPsi / B_well
        KD_max = 1.1 * np.max(np.abs(curve_drive))
        """
        # plotting parameters #
        plot = pd.plot_define()
        plt = plot.plt
        fig, axs = plt.subplots(2, 1, sharex=True, sharey=False, tight_layout=True, figsize=(10, 10))

        # Plotting Axis #
        ax1 = axs[0]
        ax2 = axs[1]
        ax1_twn = ax1.twinx()

        # Plot B Field and Curvature Drive #
        plt1, = ax1.plot(pol_well/np.pi, B_well, c='k')
        plt1_twn, = ax1_twn.plot(pol_well/np.pi, curve_drive, c='tab:orange')
        # ax1_twn.plot(pol_well/np.pi, Kn/gradPsi, c=plt1_twn.get_color(), ls='--')

        # axis colors #
        ax1_twn.spines['right'].set_color(plt1_twn.get_color())
        ax1_twn.tick_params(axis='y', colors=plt1_twn.get_color())

        ax1.spines['left'].set_color(plt1.get_color())
        ax1.tick_params(axis='y', colors=plt1.get_color())

        # Plot Velocity Contours #
        Vpar_max = 0
        M_dom = np.flip(np.linspace(0, 1, 11))
        # cmap = plt.get_cmap('tab20b', M_dom.shape[0])
        cmap = plt.get_cmap('cividis', M_dom.shape[0])
        for mdx, M in enumerate(M_dom):
            pol_set, Vpar_set = self.calc_Vpar(pol_well, B_well, np.sqrt(M), B_extrema, V_thresh=1e-12)
            for pdx, pol in enumerate(pol_set):
                if pol is not None:
                    ax2.fill_between(pol/np.pi, np.zeros(pol.shape), Vpar_set[pdx], color=cmap((1.-1e-6)*M))
                    if mdx == 0:
                        if np.max(Vpar_set[pdx]) > Vpar_max:
                            Vpar_max = np.max(Vpar_set[pdx])

        # Highlight Negative Curvature Drive Region #
        neg_idx = np.where(curve_drive < 0)[0]
        neg_bool = ((neg_idx - np.roll(neg_idx, 1)) != 1)
        negDX = np.where(neg_bool)[0]

        alpha_clr = 0.3
        for i, idx in enumerate(negDX[0:-1]):
            pol_beg = neg_idx[idx]
            pol_end = neg_idx[negDX[i+1] - 1] + 1

            ax1_twn.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')
            x_beg = [pol_well[pol_beg]/np.pi]*2
            x_end = [pol_well[pol_end]/np.pi]*2

            # ax2.plot(x_beg, [0, 1], c=plt1_twn.get_color(), ls='--')
            # ax2.plot(x_end, [0, 1], c=plt1_twn.get_color(), ls='--')
            ax2.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')

        if len(negDX) > 0:
            pol_beg = neg_idx[negDX[-1]]
            pol_end = neg_idx[-1]

            ax1_twn.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')
            x_beg = [pol_well[pol_beg]/np.pi]*2
            x_end = [pol_well[pol_end]/np.pi]*2

            # ax2.plot(x_beg, [0, 1], c=plt1_twn.get_color(), ls='--')
            # ax2.plot(x_end, [0, 1], c=plt1_twn.get_color(), ls='--')
            ax2.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')

        # Label Colorbar #
        if make_cb:
            phi_bnc = np.arcsin(np.sqrt(B_extrema[0] / (B_extrema[1]*M_dom**2 + B_extrema[0]*(1-M_dom**2)))) / np.pi
            # phi_labs = ['{0:0.2f}'.format(p) for p in np.flip(phi_bnc)]
            phi_labs = ['{0:0.1f}'.format(p) for p in np.flip(M_dom)]

            pos3 = ax2.get_position()
            # ax2c = fig.add_axes([0.91, pos3.y0+.0, pos3.width/25, pos3.height])
            # Well 0 #
            # ax2c = fig.add_axes([0.93, pos3.y0+.023, pos3.width/25, pos3.height*0.955])
            # Well 1 #
            ax2c = fig.add_axes([0.862, pos3.y0-.004, pos3.width/25, pos3.height*1.169])
            norm = mpl.colors.Normalize(vmin=phi_bnc[0]-.005, vmax=phi_bnc[-1]+.005)
            cb3 = mpl.colorbar.ColorbarBase(ax2c, cmap=cmap, norm=norm, orientation='vertical')
            cb3.set_ticks(phi_bnc)
            cb3.set_ticklabels(phi_labs)
            # cb3.ax.set_ylabel(r'$\lambda / \pi$')
            cb3.ax.set_ylabel(r'$k^2$')

        ax1.set_ylabel(r'$B \ [\mathrm{T}]$', c=plt1.get_color())
        ax2.set_xlabel(r'$\theta/\pi$')
        ax2.set_ylabel(r'$(v_{\parallel}/v)^2$')

        ax1.set_xlim(pol_well[0]/np.pi, pol_well[-1]/np.pi)
        ax2.set_ylim(0, 1)

        ax1_twn.grid()

        # Axis Labels #
        if twn_label:
            ax1_twn.set_ylabel(r'$\frac{\kappa_{\mathrm{n}}}{|\nabla \psi|} + D\kappa_{\mathrm{g}} \frac{|\nabla \psi|}{B}$', c='tab:orange')
            # ax1_twn.set_ylabel(r'$C_{\mathrm{D}}$', c=plt1_twn.get_color())

        return plt, fig, ax1, ax1_twn, ax2

    def plot_Omega_dependencies(self, save_path=None):
        plt.close('all')

        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': 18}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = 22
        mpl.rcParams['lines.linewidth'] = 2

        fig, axs = plt.subplots(3, 1, sharex=True, tight_layout=False, figsize=(8, 10))

        ax1, ax3, ax5 = axs
        ax2 = ax1.twinx()
        ax4 = ax3.twinx()

        ax2.spines['right'].set_color('tab:red')
        ax2.tick_params(axis='y', colors='tab:red')

        ax4.spines['right'].set_color('tab:blue')
        ax4.tick_params(axis='y', colors='tab:blue')

        idx_mid = int(0.5*self.theta_bnc.shape[0])
        B = self.model['modB'](self.theta_bnc)
        B_max = np.max(np.array([np.max(B[0:idx_mid]), np.max(B[idx_mid::])]))
        B_extrema = np.array([np.min(B), B_max])

        gradPsi = self.model['gxx'](self.theta_bnc)
        Kn = self.model['Kn'](self.theta_bnc)
        Kg = self.model['Kg'](self.theta_bnc)
        D = self.model['D'](self.theta_bnc)
        curve_drive = (Kn / gradPsi) + D*Kg*(gradPsi / B)
        KD_max = 1.1 * np.max(np.abs(curve_drive))

        ax1.plot(self.theta_bnc/np.pi, B, c='k', linewidth=2)
        ax2.plot(self.theta_bnc/np.pi, curve_drive, c='tab:red')
        ax2.plot(self.theta_bnc/np.pi, np.zeros(self.theta_bnc.shape[0]), c='tab:red', ls='--', linewidth=1)

        ax3.plot(self.theta_bnc/np.pi, self.omegaD, c='k')
        ax3.plot(self.theta_bnc/np.pi, np.zeros(self.theta_bnc.shape), c='k', ls='--', linewidth=1)
        ax4.plot(self.theta_bnc/np.pi, self.p_wght, c='tab:blue')

        Vpar_max = 0
        M_dom = np.flip(np.linspace(0, 1, 10))
        cmap = plt.get_cmap('tab20b', M_dom.shape[0])
        for mdx, M in enumerate(M_dom):
            pol_set, Vpar_set = self.calc_Vpar(self.theta_bnc, B, M, B_extrema, V_thresh=1e-12)
            for pdx, pol in enumerate(pol_set):
                if pol is not None:
                    ax5.fill_between(pol/np.pi, np.zeros(pol.shape), Vpar_set[pdx], color=cmap((1.-1e-6)*M))
                    if mdx == 0:
                        if np.max(Vpar_set[pdx]) > Vpar_max:
                            Vpar_max = np.max(Vpar_set[pdx])

        phi_bnc = np.arcsin(np.sqrt(B_extrema[0] / (B_extrema[1]*M_dom**2 + B_extrema[0]*(1-M_dom**2)))) / np.pi
        phi_labs = ['{0:0.2f}'.format(p) for p in np.flip(phi_bnc)]

        pos5 = ax5.get_position()
        ax5c = fig.add_axes([0.91, pos5.y0+.0, pos5.width/25, pos5.height])
        norm = mpl.colors.Normalize(vmin=phi_bnc[0], vmax=phi_bnc[-1])
        cb5 = mpl.colorbar.ColorbarBase(ax5c, cmap=cmap, norm=norm, orientation='vertical')
        cb5.set_ticks(phi_bnc)
        cb5.set_ticklabels(phi_labs)
        cb5.ax.set_ylabel(r'$\phi_{pitch} / \pi$')

        res = int(self.gridpoints/self.n_pol)
        # ax1.set_title(r'$\Omega_{{{0:0.0f}}} = {1:0.4f}$ (res. = {2})'.format(self.well_ID, self.Omega, res))

        ax1.set_ylabel(r'$B$')
        ax2.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D\kappa_g \frac{|\nabla \psi|}{B}$', c='tab:red')
        ax3.set_ylabel(r'$\langle \omega_D \rangle$')
        ax4.set_ylabel(r'$p(\theta)$', c='tab:blue')
        ax5.set_xlabel(r'$\theta / \pi$')
        ax5.set_ylabel(r'$v_{\parallel} / v$')

        ax1.set_xlim(self.theta_bnc[0]/np.pi, self.theta_bnc[-1]/np.pi)
        ax2.set_ylim(-KD_max, KD_max)
        ax4.set_ylim(0, 1.1*np.max(self.p_wght))
        ax5.set_ylim(0, 1.1*Vpar_max)

        ax1.grid()
        ax3.grid()

        if save_path:
            save_fig = os.path.join(save_path, 'well{0:0.0f}_res{1}.png'.format(self.well_ID, res))
            plt.savefig(save_fig, bbox_inches='tight')
        else:
            plt.show()


if __name__ == "__main__":
    gist_path = os.path.join('/home', 'michael', 'gist_ncsx_alf0_s0p5_res512_npol2.dat')
    gist = read_gist(gist_path, nfp=3)
    gist.partition_wells()

    gist.plotting()
    fig, ax1 = plt.subplots(1, 1, tight_layout=True, figsize=(8, 5))
    ax2 = ax1.twinx()

    gist.calculate_omega_de(curve_method='GIST', integ_method='gtrapz')
    pol_beg = -1.16*np.pi  # gist.partition_pol['well 0'][0]
    pol_end = 1.16*np.pi  # gist.partition_pol['well 0'][-1]
    pol_dom = gist.pol_bnc[(gist.pol_bnc >= pol_beg) & (gist.pol_bnc <= pol_end)]

    plt1, = ax1.plot(pol_dom/np.pi, gist.Bref*gist.model['modB'](pol_dom), c='k')
    plt2, = ax2.plot(pol_dom/np.pi, gist.model['omega_de'](pol_dom), c='tab:blue', label='GIST')

    gist.calculate_omega_de(curve_method='NE3DLE', integ_method='gtrapz')
    pol_beg = -1.16*np.pi  # gist.partition_pol['well 0'][0]
    pol_end = 1.16*np.pi  # gist.partition_pol['well 0'][-1]
    pol_dom = gist.pol_bnc[(gist.pol_bnc >= pol_beg) & (gist.pol_bnc <= pol_end)]

    plt1, = ax1.plot(pol_dom/np.pi, gist.Bref*gist.model['modB'](pol_dom), c='k')
    # plt2, = ax2.plot(pol_dom/np.pi, -gist.model['omega_de'](pol_dom), c='tab:red', label='NE3DLE')

    ax1.set_xlabel(r'$\theta/\pi$')
    ax1.set_ylabel(r'$B/T$', color=plt1.get_color())
    ax2.set_ylabel(r'$\hat{\omega}_{\alpha}$', color=plt2.get_color())

    ax1.set_xlim(pol_dom[0]/np.pi, pol_dom[-1]/np.pi)
    oma_max = np.max(np.abs(ax2.get_ylim()))
    ax2.set_ylim(-oma_max, oma_max)

    ax2.grid()
    ax2.legend()

    plt.show()
