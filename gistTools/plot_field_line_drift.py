import os, sys
import numpy as np

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import plot_define as pd
import gistTools.gist_reader as gr


# define gist path #
# gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_QHS_mn1824_alf0_s0p50_res128_npol8.dat')
gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_QHS_mn1824_alf0_s0p50_res512_npol16.dat')

# import gist data #
gist = gr.read_gist(gist_path)
gist.partition_wells()
gist.calculate_p_weight()
gist.calculate_omega_de()

# plotting parameters #
plot = pd.plot_define(fontSize=14, labelSize=16)
plt = plot.plt
fig, axs = plt.subplots(3, 1, sharex=True, figsize=(12,8))
ax1 = axs[0]
ax2 = ax1.twinx()

# plot data
plt1, = ax1.plot(gist.pol_dom/np.pi, gist.data['modB'], c='tab:blue')
plt2, = ax2.plot(gist.pol_dom/np.pi, gist.data['p-weight'], c='tab:orange')

axs[1].plot(gist.pol_dom/np.pi, gist.data['omega_alpha'], c='k')
axs[2].plot(gist.pol_dom/np.pi, gist.data['omega_psi'], c='k')

# axis labels #
ax1.set_ylabel(r'$B \ / \ \mathrm{T}$', c=plt1.get_color())
ax2.set_ylabel(r'$p(\theta_\mathrm{b})$', c=plt2.get_color())

axs[1].set_ylabel(r'$\hat{\overline{\omega}}_\mathrm{y}$')
axs[2].set_ylabel(r'$\hat{\overline{\omega}}_\mathrm{x}$')
axs[2].set_xlabel(r'$\theta_\mathrm{b}/\pi$')

# axis limits #
# axs[2].set_xlim(gist.pol_dom[0]/np.pi, gist.pol_dom[-1]/np.pi)
axs[2].set_xlim(-4, 4)
ax2.set_ylim(0, ax2.get_ylim()[1])

omg_alf_max = np.max(np.abs(gist.data['omega_alpha']))
axs[1].set_ylim(-omg_alf_max, omg_alf_max)

omg_psi_max = np.max(np.abs(gist.data['omega_psi']))
axs[2].set_ylim(-omg_psi_max, omg_psi_max)

# save/show #
plt.show()
