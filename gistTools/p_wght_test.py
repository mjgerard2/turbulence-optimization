import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from math import factorial
from scipy.signal import argrelextrema
from scipy.interpolate import interp1d
from scipy.optimize import brentq as find_root


def modB(theta, cos_arr, N=4, M=1, iota=1):
    B = np.ones(theta.shape)
    for cos in cos_arr:
        e, n, m = cos
        B = B + e*np.cos((n*N*iota - m*M)*theta)
    return B


def dmodB(theta, cos_arr, N=4, M=1, iota=1):
    dB = np.zeros(theta.shape)
    for cos in cos_arr:
        e, n, m = cos
        dB = dB - e*(n*N*iota - m*M)*np.sin((n*N*iota - m*M)*theta)
    return dB


def expand_p_wght(B, dB, theta, idx, freq, eps, N=0):
    # Define Expansion Window #
    freq_inv = eps/freq
    theta_exp = theta[(theta >= theta[idx]-freq_inv) & (theta <= theta[idx]+freq_inv)] - theta[idx]

    # Calculate Derivatives #
    deriv = {}
    deriv['d2B'] = np.gradient(dB/B[idx], theta)
    for n in range(N):
        deriv['d{}B'.format(n+3)] = np.gradient(deriv['d{}B'.format(n+2)], theta)

    # Calculate Expanded Numerator and Denomenator #
    numer = np.zeros(theta_exp.shape)
    denom_1 = np.ones(theta_exp.shape)
    denom_2 = np.zeros(theta_exp.shape)
    for key in deriv:
        n = int(key[1])-2
        numer = numer + (deriv[key][idx]/factorial(n+1)) * ((theta_exp)**n)
        denom_1 = denom_1 + (deriv[key][idx]/factorial(n+2)) * ((theta_exp)**(n+2))
        denom_2 = denom_2 + (deriv[key][idx]/factorial(n+2)) * ((theta_exp)**n)

    # Calculate Expanded Weighting Function #
    theta_rng = theta_exp + theta[idx]
    p_wght_exp = np.abs(numer)/((denom_1**1.5)*np.sqrt(denom_2))

    return theta_rng, p_wght_exp


epsilon = 0.15
iota = 1.
N, M = 4, 1

cos_arr = np.array([[-.1, 1, 1],
                    [-.0, 2, 2],
                    [-.025, 10, 10]])

theta_dom = np.pi * np.linspace(-.5, .5, 1001)
stp = theta_dom[1] - theta_dom[0]
B = modB(theta_dom, cos_arr, N=N, M=M)

# Get left and right trapping boundaries #
idx_max = argrelextrema(B, np.greater)[0]
the_well = theta_dom[idx_max[0]:idx_max[-1]+1]
B_well = B[idx_max[0]:idx_max[-1]+1]
dB_well = dmodB(the_well, cos_arr, N=N, M=M)

# Make spline fit #
B_model = interp1d(the_well, B_well)
dB_model = interp1d(the_well, dB_well)

# Find where dB/dtheta changes sign #
dB_sign = np.sign(dB_well)
sz = dB_sign == 0
while sz.any():
    dB_sign[sz] = np.roll(dB_sign, 1)[sz]
    sz = dB_sign == 0
dB_sign_chg = ((np.roll(dB_sign, 1) - dB_sign) != 0).astype(int)
dB_sign_chg[0] = 0
chg_idx = np.where(dB_sign_chg == 1)[0]

# Find roots of dB/dtheta and calculate ddB/ddtheta and d3B/d3theta #
# with a second order finite differencing scheme #
min_data = []
max_data = []
for idx in chg_idx:
    if idx >= 3 and idx <= dB_well.shape[0]-4:
        # Central Difference #
        pol = find_root(dB_model, the_well[idx-1], the_well[idx+1])
        ddB = (B_model(pol-stp) + B_model(pol+stp) - 2*B_model(pol)) / stp**2
        if ddB > 0:
            d3B = (dB_model(pol-stp) + dB_model(pol+stp) - 2*dB_model(pol)) / stp**2
            min_data.append([pol, ddB, d3B])
        else:
            max_data.append(pol)
    elif idx >= 3:
        # Backwards Difference #
        pol = find_root(dB_model, the_well[idx-1], the_well[idx])
        ddB = (B_model(pol) + B_model(pol-2*stp) - 2*B_model(pol-stp)) / stp**2
        if ddB > 0:
            d3B = (dB_model(pol) + dB_model(pol-2*stp) - 2*dB_model(pol-stp)) / stp**2
            min_data.append([pol, ddB, d3B])
        else:
            max_data.append(pol)
    elif idx <= dB_well.shape[0]-4:
        # Forward Difference #
        pol = find_root(dB_model, the_well[idx-1], the_well[idx])
        ddB = (B_model(pol+2*stp) + B_model(pol) - 2*B_model(pol+stp)) / stp**2
        if ddB > 0:
            d3B = (dB_model(pol+2*stp) + dB_model(pol) - 2*dB_model(pol+stp)) / stp**2
            min_data.append([pol, ddB, d3B])
        else:
            max_data.append(pol)

min_data = np.array(min_data)
max_data = np.array(max_data)

# Make sure that the maxima define the theta boundaries #
if max_data.shape[0] > 0:
    if max_data[0] > min_data[0][0]:
        max_data = np.insert(max_data, 0, the_well[0])
    if max_data[-1] < min_data[-1][0]:
        max_data = np.append(max_data, the_well[-1])
else:
    max_data = np.array([the_well[0], the_well[-1]])

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axes #
fig, ax1 = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))
ax2 = ax1.twinx()

# Get phase-space weighting function #
freq = iota*N - M
gap = epsilon/freq

the_fit = np.empty(0)
p_wght_fit = np.empty(0)

for i, min_datum in enumerate(min_data):
    pol, ddB_min, d3B_min = min_datum
    B_min = B_model(pol)

    if i == 0:
        the_lft = the_well[the_well < pol - gap]
        the_rgt = the_well[(the_well > pol + gap) & (the_well <= max_data[i+1])]

        B_lft = B_well[the_well < pol - gap]
        B_rgt = B_well[(the_well > pol + gap) & (the_well <= max_data[i+1])]

        dB_lft = dB_well[the_well < pol - gap]
        dB_rgt = dB_well[(the_well > pol + gap) & (the_well <= max_data[i+1])]

        p_wght_lft = (B_min * np.abs(dB_lft)) / ((B_lft**2)*np.sqrt(1. - B_min/B_lft))
        p_wght_rgt = (B_min * np.abs(dB_rgt)) / ((B_rgt**2)*np.sqrt(1. - B_min/B_rgt))

        axj, = ax2.plot(the_lft/np.pi, p_wght_lft, c='tab:blue')
        ax2.plot(the_rgt/np.pi, p_wght_rgt, c=axj.get_color())

        the_sctr = np.array([the_lft[-1], the_rgt[0]]) / np.pi
        p_wght_sctr = np.array([p_wght_lft[-1], p_wght_rgt[0]])
        ax2.scatter(the_sctr, p_wght_sctr, s=50, c=axj.get_color())

    elif i < min_data.shape[0]-1:
        the_lft = the_well[(the_well > max_data[i]) & (the_well < pol - gap)]
        the_rgt = the_well[(the_well > pol + gap) & (the_well <= max_data[i+1])]

        B_lft = B_well[(the_well > max_data[i]) & (the_well < pol - gap)]
        B_rgt = B_well[(the_well > pol + gap) & (the_well <= max_data[i+1])]

        dB_lft = dB_well[(the_well > max_data[i]) & (the_well < pol - gap)]
        dB_rgt = dB_well[(the_well > pol + gap) & (the_well <= max_data[i+1])]

        p_wght_lft = (B_min * np.abs(dB_lft)) / ((B_lft**2)*np.sqrt(1. - B_min/B_lft))
        p_wght_rgt = (B_min * np.abs(dB_rgt)) / ((B_rgt**2)*np.sqrt(1. - B_min/B_rgt))

        ax2.plot(the_lft/np.pi, p_wght_lft, c=axj.get_color())
        ax2.plot(the_rgt/np.pi, p_wght_rgt, c=axj.get_color())

        the_sctr = np.array([the_lft[-1], the_rgt[0]]) / np.pi
        p_wght_sctr = np.array([p_wght_lft[-1], p_wght_rgt[0]])
        ax2.scatter(the_sctr, p_wght_sctr, s=50, c=axj.get_color())

    else:
        the_lft = the_well[(the_well > max_data[i]) & (the_well < pol - gap)]
        the_rgt = the_well[the_well > pol + gap]

        B_lft = B_well[(the_well > max_data[i]) & (the_well < pol - gap)]
        B_rgt = B_well[the_well > pol + gap]

        dB_lft = dB_well[(the_well > max_data[i]) & (the_well < pol - gap)]
        dB_rgt = dB_well[the_well > pol + gap]

        p_wght_lft = (B_min * np.abs(dB_lft)) / ((B_lft**2)*np.sqrt(1. - B_min/B_lft))
        p_wght_rgt = (B_min * np.abs(dB_rgt)) / ((B_rgt**2)*np.sqrt(1. - B_min/B_rgt))

        ax2.plot(the_lft/np.pi, p_wght_lft, c=axj.get_color())
        ax2.plot(the_rgt/np.pi, p_wght_rgt, c=axj.get_color())

        the_sctr = np.array([the_lft[-1], the_rgt[0]]) / np.pi
        p_wght_sctr = np.array([p_wght_lft[-1], p_wght_rgt[0]])
        ax2.scatter(the_sctr, p_wght_sctr, s=50, c=axj.get_color())

axi, = ax1.plot(the_well/np.pi, B_well, c='k')
for datum in min_data:
    pol, ddB, d3B = datum

    idx = np.argmin(np.abs(the_well - pol))
    the_exp, p_wght_exp = expand_p_wght(B_well, dB_well, the_well, idx, freq, epsilon, N=6)
    ax2.plot(the_exp/np.pi, p_wght_exp, c=axj.get_color(), ls=':')

ax1.spines["left"].set_edgecolor(axi.get_color())
ax1.tick_params(axis='y', colors=axi.get_color())

ax2.spines["right"].set_edgecolor(axj.get_color())
ax2.tick_params(axis='y', colors=axj.get_color())

ax1.set_xlabel(r'$\theta/\pi$')
ax1.set_ylabel(r'$B$', c=axi.get_color())
ax2.set_ylabel(r'$p(\theta_b)$', c=axj.get_color())

ax2.set_ylim(0, 10)

plt.grid()
plt.show()
