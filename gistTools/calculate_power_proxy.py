import os
import h5py as hf
import numpy as np
import gist_reader as gr
from scipy import constants as const
from scipy.interpolate import interp1d

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
WORKDIR = os.path.dirname(os.getcwd())
#WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import geneAnalysis.read_eigenvector as re
import geneAnalysis.read_parameters as rp


config_ids = []
file_name = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'config_list.txt')
with open(file_name, 'r') as f:
    lines = f.readlines()
    for line in lines:
        config_ids.append(line.strip())

num_of_configs = len(config_ids)

run_num = 1

psiN = 0.039418
psiHat = 0.5

Te_keV = 1.
Ti_keV = 0.2

# ky_tags = ['ky_0p1', 'ky_0p4', 'ky_0p7', 'ky_1p0']
ky_tags = ['ky_0p4']
for ky_tag in ky_tags:
    print('\n===============\n' +
          'Working: '+ky_tag +
          '\n===============\n')

    if ky_tag == 'ky_0p1':
        ky_idx = 0
    elif ky_tag == 'ky_0p4':
        ky_idx = 1
    elif ky_tag == 'ky_0p7':
        ky_idx = 2
    elif ky_tag == 'ky_1p0':
        ky_idx = 3

    fig_name = 'power_proxy_M_inf_'+ky_tag+'_{}_x.png'.format(run_num)
    save_name = 'power_proxy_M_inf_'+ky_tag+'_{}_x.h5'.format(run_num)
    save_file = os.path.join('/mnt', 'GENE', save_name)

    growth_rates = np.empty(num_of_configs)
    power_proxy = np.empty(num_of_configs)

    configs_wo_eigendata = []
    configs_wo_gamma = []

    for idx, config_id in enumerate(config_ids):
        print(config_id+' ({0}|{1})'.format(idx+1, num_of_configs))

        # Get Parameters File #
        param_dirc = os.path.join('/mnt', 'GENE', 'linear_data', config_id, ky_tag)
        param_files = [f.name for f in os.scandir(param_dirc) if f.name.split('_')[0] == 'parameters']
        param_names = sorted([name for name in param_files if name.split('_')[1] == str(run_num)])

        # Read in Parameters Data #
        param_file = os.path.join(param_dirc, param_names[0])
        param = rp.read_file(param_file)
        q0 = param.data_dict['q0']
        mref = const.m_p * param.data_dict['mref']
        Tref = 1e3 * const.e * param.data_dict['Tref']
        Lref = param.data_dict['Lref']
        Bref = param.data_dict['Bref']
        rho_ref = np.sqrt(mref*Tref)/(const.e*Bref)

        # Import TEM data #
        tem_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_{}_x.h5'.format(run_num))
        with hf.File(tem_file, 'r') as hf_:
            aLn = hf_['omne'][()]
            aLT = hf_['omte'][()]
            k_num = hf_[config_id][ky_idx, 0]
            gamma = hf_[config_id][ky_idx, 1]
            omega = hf_[config_id][ky_idx, 2]

        k_num_scld = k_num * (Lref/(q0*rho_ref))
        omega = omega * np.sqrt((Ti_keV*1e3*const.e)/(Lref*Lref*const.m_p))

        xi_T = aLT / (2*psiN*np.sqrt(psiHat))
        xi_n = aLn / (2*psiN*np.sqrt(psiHat))

        norm = (1e-3/(Te_keV*k_num_scld))
        omg = -norm * omega
        sig = 0  # (1./np.sqrt(10))*(omg/np.sqrt(2.))

        # Check if Eigendata Exsits #
        eigen_dirc = os.path.join('/mnt', 'GENE', 'linear_data', config_id, ky_tag)
        eigen_files = [f.name for f in os.scandir(eigen_dirc) if f.name.split('_')[0] == 'eigendata']
        eigen_name = [name for name in eigen_files if name.split('_')[1] == str(run_num)]

        if len(eigen_name) == 1 and not np.isnan(omega):
            # Read in GIST Data #
            config_split = config_id.split('-')
            main_id = 'main_coil_{}'.format(config_split[0])
            set_id = 'set_{}'.format(config_split[1])
            job_id = 'job_{}'.format(config_split[2])
            gist_name = 'gist_HSX_'+config_id+'_s0p5_res1024_npol8_alpha0p00.dat'
            gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, gist_name)

            gist = gr.read_gist(gist_file)
            gist.partition_wells()
            gist.calculate_p_weight()
            gist.calculate_omega_de()
            gist.calculate_resonance_function(omg, sig, xi_n, xi_T, 1)

            # Import Eigen Data #
            eigen_file = os.path.join('/mnt', 'GENE', 'linear_data', config_id, ky_tag, eigen_name[0])
            eigen = re.read_eigendata(eigen_file)
            # eigen.normalize_phi(gist.pol_dom)

            # Get left and right trapping boundaries #
            idx_lft, idx_rgt = gist.idx_max[0], gist.idx_max[-1]
            pol_well = gist.pol_dom[idx_lft:idx_rgt+1]
            B_well = gist.data['modB'][idx_lft:idx_rgt+1]
            B_extrema = np.array([np.min(B_well), np.max(B_well)])

            bnc_avg = []
            M_dom = np.linspace(0, 1, 1001, endpoint=False)
            for mdx, M in enumerate(M_dom):
                pol_set, Vpar_set = gist.calc_Vpar(pol_well, B_well, M, B_extrema)
                for pdx, pol in enumerate(pol_set):
                    if pol is not None:
                        phi_avg = eigen.average_sqrd_bar(pol)
                        bnc_avg.append(np.array([pol[0], phi_avg**2]))
                        bnc_avg.append(np.array([pol[-1], phi_avg**2]))

            phi_sqrd = np.array(sorted(bnc_avg, key=lambda x: x[0]))
            phi_sqrd_model = interp1d(phi_sqrd[:, 0], phi_sqrd[:, 1])

            # Calculate Integrand #
            omg_de = gist.model['omega_de'](gist.pol_bnc)
            jacob = gist.model['Jacob'](gist.pol_bnc)
            phi = phi_sqrd_model(gist.pol_bnc)
            phi = phi / np.trapz(phi, gist.pol_bnc)
            p_weight = gist.model['p-weight'](gist.pol_bnc)
            F_theta = gist.model['resonance_function'](gist.pol_bnc)
            B = gist.model['modB'](gist.pol_bnc)
            integ = jacob * phi * p_weight * F_theta / B

            integ_data = np.stack((gist.pol_bnc, jacob, phi, p_weight, F_theta, B, omg_de), axis=1)
            with hf.File(save_file, 'a') as hf_:
                hf_.create_dataset(config_id, data=integ_data)

            power_proxy[idx] = .5*np.sqrt(.5*np.pi)*np.trapz(integ, gist.pol_bnc)
            growth_rates[idx] = gamma

        else:
            power_proxy[idx] = np.nan
            growth_rates[idx] = np.nan

            if len(eigen_name) != 1:
                configs_wo_eigendata.append(config_id)

            elif np.isnan(omega):
                configs_wo_gamma.append(config_id)

    """
    # Save List of Configs with Missing Data #
    with open('configs_wo_eigendata_'+ky_tag+'_{}_x.txt'.format(run_num), 'w') as f:
        for config in configs_wo_eigendata:
            f.write(config+'\n')

    with open('configs_wo_gamma_'+ky_tag+'_{}_x.txt'.format(run_num), 'w') as f:
        for config in configs_wo_gamma:
            f.write(config+'\n')
    """

    # Plotting Parameters #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 20}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 24

    # Plotting Axis #
    fig, ax = plt.subplots(1, 1, tight_layout=True)

    ax.scatter(power_proxy, growth_rates, marker='o', c='None', edgecolor='k', s=50)

    ax.set_title(r'$k_y \rho_s = {0:0.1f}$'.format(k_num))
    ax.set_xlabel(r'$P_{\mathrm{e}} / (\mathrm{e} n_{\mathrm{e}} k_{\alpha})$')
    ax.set_ylabel(r'$\gamma \ (c_s/a)$')

    # ax.set_xlim(0, 1.05*np.nanmax(power_proxy))
    # ax.set_ylim(0, 1.05*np.nanmax(growth_rates))

    ax.grid()

    save_path = os.path.join('/mnt', 'GENE', 'linear_data', 'figures', fig_name)
    # plt.savefig(save_path)
    plt.show()
