import os
import numpy as np
import gist_reader as gr

import matplotlib as mpl
import matplotlib.pyplot as plt


# gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_s0p5_alpha0_npol4_nz8192.dat')
gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'gist_QHS_best_alf0_s0p5_res2048_npol4.dat')

gist = gr.read_gist(gist_file, nfp=4)
gist.partition_wells()

# Define poloidal domain #
idx_lim = np.floor(.5*gist.pol_min.shape[0])
well_idx = np.arange(-idx_lim, idx_lim+1, dtype=int)

well_ID = 2
idx_lft = gist.idx_max[np.argmin(np.abs(well_idx + well_ID))]
idx_rgt = gist.idx_max[np.argmin(np.abs(well_idx - well_ID - 1))]

pol_norm = gist.pol_dom[idx_lft:idx_rgt+1] / np.pi
B_well = gist.data['modB'][idx_lft:idx_rgt+1]

# Calculate curvature drive #
gradPsi = gist.data['gxx'][idx_lft:idx_rgt+1]
Kn = gist.data['Kn'][idx_lft:idx_rgt+1]
Kg = gist.data['Kg'][idx_lft:idx_rgt+1]
D = gist.data['D'][idx_lft:idx_rgt+1]
curve_drive = (Kn / gradPsi) + D*Kg*(gradPsi / B_well)
cd_max = 1.1 * np.max(np.abs(curve_drive))

# Calculate Omega over helically connected wells #
well_IDs = np.arange(-2, 3, dtype=int)

print('Well ID: {}'.format(well_IDs[0]))
gist.calculate_Omega_singleWell(well_IDs[0])
pol_wells = gist.theta_bnc
omegaD = gist.omegaD
p_wght = gist.p_wght

for i, well_ID in enumerate(well_IDs[1::]):
    print('Well ID: {}'.format(well_ID))
    gist.calculate_Omega_singleWell(well_ID)

    pol_wells = np.append(pol_wells, gist.theta_bnc)
    omegaD = np.append(omegaD, gist.omegaD)
    p_wght = np.append(p_wght, gist.p_wght)

wght_max = 1.1 * np.max(p_wght)

# Plotting parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Construct plotting axes #
fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(14, 6))
fig.subplots_adjust(hspace=0.1)

ax1 = axs[0]
ax2 = ax1.twinx()

ax3 = axs[1]
ax4 = ax3.twinx()

# Plot data #
cmap12 = plt.get_cmap('bwr', 2)
plt1, = ax1.plot(pol_norm, B_well, c='tab:blue')
plt2, = ax2.plot(pol_wells/np.pi, p_wght, c='tab:red')

cmap34 = plt.get_cmap('Spectral', 2)
plt3, = ax3.plot(pol_wells/np.pi, omegaD, c='k')
plt4, = ax4.plot(pol_norm, curve_drive, c='tab:orange')

# Label axes #
ax3.set_xlabel(r'$\theta / \pi$')

ax1.set_ylabel(r'$B$', color=plt1.get_color())
ax2.set_ylabel(r'$p\left( \theta_b \right)$', color=plt2.get_color())

ax3.set_ylabel(r'$\overline{\omega}_{d\mathrm{e}}$', color=plt3.get_color())
ax4.set_ylabel(r'$C_{\mathrm{d}}$', c=plt4.get_color())

# Color Spines #
ax1.spines['right'].set_color(plt1.get_color())
ax1.tick_params(axis='y', colors=plt1.get_color())

ax2.spines['right'].set_color(plt2.get_color())
ax2.tick_params(axis='y', colors=plt2.get_color())

ax3.spines['right'].set_color(plt3.get_color())
ax3.tick_params(axis='y', colors=plt3.get_color())

ax4.spines['right'].set_color(plt4.get_color())
ax4.tick_params(axis='y', colors=plt4.get_color())

# Axis limits #
B_min = np.min(B_well)
B_max = np.max(B_well)
omegaD_max = np.max(np.abs(omegaD))
curve_drive_max = np.max(np.abs(curve_drive))

ax1.set_xlim(pol_norm[0], pol_norm[-1])
ax2.set_ylim(0, ax2.get_ylim()[1])
ax3.set_ylim(-omegaD_max, omegaD_max)
ax4.set_ylim(-curve_drive_max, curve_drive_max)

# Axis grids #
ax1.grid()
ax3.grid()

# Show or save #
plt.show()
savePath = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Prelim_Defense', 'figures', 'field_line_omega.png')
# plt.savefig(savePath)
