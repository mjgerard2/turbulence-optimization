import os
import numpy as np
import gist_reader as gr

import matplotlib as mpl
import matplotlib.pyplot as plt

# gist_path_init = os.path.join('/home', 'michael', 'Desktop', 'joey_data', 'equilibria', 'opt_gist_files', 'd-25_8_17_2022_init_high_res')
# gist_path_opt = os.path.join('/home', 'michael', 'Desktop', 'joey_data', 'equilibria', 'opt_gist_files', 'd-25_8_17_2022_opt_high_res')

gist_path_init = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_60', 'set_1', 'job_84', 'gist_60-1-84_mn8-8_alf0_s0p5_res64_npol4.dat')
gist_path_opt = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_60', 'set_1', 'job_84', 'gist_60-1-84_mn18-24_alf0_s0p5_res64_npol4.dat')

gist_opt = gr.read_gist(gist_path_opt)
gist_init = gr.read_gist(gist_path_init)

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axes #
fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = ax1.twinx()

ax3 = axs[1]
ax4 = ax3.twinx()

# Construct Plots #
plt1, = ax1.plot(gist_init.pol_dom/np.pi, gist_init.data['modB'], c='tab:blue')
plt2, = ax2.plot(gist_init.pol_dom/np.pi, gist_init.data['Kn'], c='tab:red')

plt3, = ax3.plot(gist_opt.pol_dom/np.pi, gist_opt.data['modB'], c='tab:blue')
plt4, = ax4.plot(gist_opt.pol_dom/np.pi, gist_opt.data['Kn'], c='tab:red')

# Color Axes #
ax1.spines['right'].set_color(plt1.get_color())
ax1.tick_params(axis='y', colors=plt1.get_color())

ax3.spines['right'].set_color(plt3.get_color())
ax3.tick_params(axis='y', colors=plt3.get_color())

ax2.spines['right'].set_color(plt2.get_color())
ax2.tick_params(axis='y', colors=plt2.get_color())

ax4.spines['right'].set_color(plt4.get_color())
ax4.tick_params(axis='y', colors=plt4.get_color())

# Axis Labels #
ax3.set_xlabel(r'$\theta / \pi$')
ax1.set_ylabel(r'$B \ (T)$', c=plt1.get_color())
ax2.set_ylabel(r'$\kappa_n$', c=plt2.get_color())
ax3.set_ylabel(r'$B \ (T)$', c=plt3.get_color())
ax4.set_ylabel(r'$\kappa_n$', c=plt4.get_color())

# Axis Limits #
ax1.set_xlim(-gist_init.n_pol, gist_init.n_pol)
kn_max = np.max(np.abs(gist_init.data['Kn']))
ax2.set_ylim(-kn_max, kn_max)

kn_max = np.max(np.abs(gist_opt.data['Kn']))
ax4.set_ylim(-kn_max, kn_max)

# Axis Grids #
ax2.grid()
ax4.grid()

# Save/Show #
plt.show()
