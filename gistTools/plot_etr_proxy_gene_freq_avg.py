import os
import h5py as hf
import numpy as np
import read_power_proxy_data as rpp

from scipy import constants as const
import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)
import geneAnalysis.read_parameters as rp


M1 = 10
M2 = 100
omg_scl = 1.

psiN = 0.039418
psiHat = 0.5

Te_keV = 1.
Ti_keV = 0.2

# Get Parameters File #
param_file = os.path.join('/mnt', 'GENE', 'linear_data', '897-1-0', 'ky_0p1', 'parameters_1_1')
param = rp.read_file(param_file)

q0 = param.data_dict['q0']
mref = const.m_p * param.data_dict['mref']
Tref = 1e3 * const.e * param.data_dict['Tref']
Lref = param.data_dict['Lref']
Bref = param.data_dict['Bref']
rho_ref = np.sqrt(mref*Tref)/(const.e*Bref)

# Calculate Normalizations #
gene_norm = (1e-3/Te_keV)*np.sqrt(Ti_keV*1e3*const.e/(Lref*Lref*const.m_p))
sig1_norm = 1./np.sqrt(2*M1)
sig2_norm = 1./np.sqrt(2*M2)

# Import GENE Data #
gene_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')
with hf.File(gene_file, 'r') as hf_:
    aLn = hf_['omne'][()]
    aLT = hf_['omte'][()]

    tem_data = np.full((100, 7, 3), np.nan)
    for i, key in enumerate(hf_):
        if len(key.split('-')) == 3:
            tem_data[i] = hf_[key][()]

xi_T = aLT / (2*psiN*np.sqrt(psiHat))
xi_n = aLn / (2*psiN*np.sqrt(psiHat))

# Define wave number tags #
ky_tags = ['ky_0p1', 'ky_0p4', 'ky_0p7', 'ky_1p0']

proxy_comp_M1 = {}
proxy_comp_M2 = {}

for ky_idx, ky_tag in enumerate(ky_tags):
    # Normalize mean drift wave frequency from GENE data #
    is_nan = np.isnan(tem_data[:, ky_idx, 1])
    not_nan = ~is_nan
    tem = tem_data[not_nan]

    k_num = tem[0, ky_idx, 0] * (Lref/(q0*rho_ref))
    omg = -(gene_norm/k_num) * np.mean(tem[:, ky_idx, 2]) * omg_scl
    sig1 = omg * sig1_norm
    sig2 = omg * sig2_norm

    # Read in Proxy Data #
    file = os.path.join('/mnt', 'GENE', 'power_proxy_M_inf_'+ky_tag+'_1_x.h5')

    pwr = rpp.read_power_proxy_data(file)

    # pwr.calculate_modified_etr_proxy(omg, sig1, xi_n, xi_T)
    pwr.calculate_test_proxy(omg, sig1, xi_n, xi_T)
    pwr.compare_proxy_with_GENE(gene_file, ky_tag, test=True)
    proxy_comp_M1[ky_tag] = pwr.proxy_comparison

    # pwr.calculate_modified_etr_proxy(omg, sig2, xi_n, xi_T)
    pwr.calculate_test_proxy(omg, sig2, xi_n, xi_T)
    pwr.compare_proxy_with_GENE(gene_file, ky_tag, test=True)
    proxy_comp_M2[ky_tag] = pwr.proxy_comparison

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24

# Plot Data #
fig, axs = plt.subplots(2, 2, sharex=True, sharey=False, tight_layout=True, figsize=(12, 10))

ax1 = axs[0, 0]
ax2 = axs[0, 1]
ax3 = axs[1, 0]
ax4 = axs[1, 1]

for idx, ky_tag in enumerate(ky_tags):
    i = int(idx / 2)
    j = idx % 2

    data_M1 = proxy_comp_M1[ky_tag]
    data_M2 = proxy_comp_M2[ky_tag]

    axs[i, j].scatter(data_M1[:, 0], data_M1[:, 1], c='None', edgecolor='tab:blue', marker='o', s=50, label=r'$M = {}$'.format(M1))
    # axs[i, j].scatter(data_M2[:, 0], data_M2[:, 1], c='None', edgecolor='tab:orange', marker='s', s=50, label=r'$M = {}$'.format(M2))

    axs[i, j].set_title(r'$k_y\rho_s = {}$'.format(ky_tag.split('_')[1].replace('p', '.')))
    if i == 1:
        # axs[i, j].set_xlabel(r'$P_{\mathrm{e}}^* / \mathrm{e} n_{\mathrm{e}} k_{\alpha}$')
        axs[i, j].set_xlabel(r'$\langle F(\theta_b) \rangle$')
    if j == 0:
        axs[i, j].set_ylabel(r'$\gamma \ (c_s/a)$')

    axs[i, j].set_ylim(0, 1.05*np.max(data_M1[:, 1]))

    axs[i, j].legend()
    axs[i, j].grid()

save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations',
                         'HSX_Group', '2023', '2023-01-06', 'figures',
                         'etr_proxy_gene_freq_avg_Fmean.png')
plt.savefig(save_path)
# plt.show()
