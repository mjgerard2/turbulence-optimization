import os
import h5py as hf
import numpy as np
import read_power_proxy_data as rpp

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
WORKDIR = os.path.dirname(os.getcwd())
sys.path.append(WORKDIR)

import geneAnalysis.read_phases as rp


# Define GENE Data Path #
gene_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24

# Read in Proxy Comparison Data #
ky_tags = ['ky_0p4', 'ky_1p0']

proxy_comp = {}
for ky_tag in ky_tags:
    file = os.path.join('/mnt', 'GENE', 'power_proxy_M_inf_'+ky_tag+'_1_x.h5')

    pwr = rpp.read_power_proxy_data(file)
    pwr.calculate_etr_proxy()
    pwr.compare_proxy_with_GENE(gene_file, ky_tag)

    proxy_data = np.empty((len(pwr.etr_proxy), 5))
    base_dirc = os.path.join('/mnt', 'GENE', 'linear_data')
    for idx, key in enumerate(pwr.etr_proxy):
        chk_dirc = os.path.join('/mnt', 'GENE', 'linear_data', key, ky_tag)
        dat_files = [f.name for f in os.scandir(chk_dirc) if f.name.endswith('.dat')]

        phase_chk = False
        for file in dat_files:
            base_name = file.strip().split('_')[0]
            if base_name == 'phasese':
                phase_path = os.path.join(chk_dirc, file)
                phase_chk = True

        if phase_chk:
            phs = rp.read_phases(phase_path)
            phase_n = phs.get_average_phase('phi_x_n,t')
            phase_Tpa = phs.get_average_phase('phi_x_Tpa')
            phase_Tpe = phs.get_average_phase('phi_x_Tpe')

        else:
            phase_n = np.nan
            phase_Tpa = np.nan
            phase_Tpe = np.nan

        proxy_data[idx] = [pwr.proxy_comparison[idx][0], pwr.proxy_comparison[idx][1], phase_n, phase_Tpa, phase_Tpe]

    proxy_comp[ky_tag] = proxy_data

# Plot Data #
fig, axs = plt.subplots(3, 2, sharex=True, sharey=False, tight_layout=True, figsize=(12, 24))

for idx, ky_tag in enumerate(ky_tags):
    data = proxy_comp[ky_tag]

    is_nan = np.isnan(data[:, 2])
    data_nan = data[is_nan]
    data_cln = data[~is_nan]

    cbar_labs = [r'$\tilde{\phi} \times \tilde{n}_{tr} \ (\pi)$',
                 r'$\tilde{\phi} \times \tilde{T}_{\parallel} \ (\pi)$',
                 r'$\tilde{\phi} \times \tilde{T}_{\perp} \ (\pi)$']

    for i in range(3):
        axs[i, idx].scatter(data_nan[:, 0], data_nan[:, 1], c='None', edgecolor='k', marker='o', s=50)
        smap = axs[i, idx].scatter(data_cln[:, 0], data_cln[:, 1], c=data_cln[:, 2+i]/np.pi, edgecolor='k', marker='o', s=50)

        cbar = fig.colorbar(smap, ax=axs[i, idx])
        cbar.ax.set_ylabel(cbar_labs[i])

        axs[i, idx].set_ylim(0, 1.05*np.max(data[:, 1]))
        axs[i, idx].grid()

    axs[0, idx].set_title(r'$k_y\rho_s = {}$'.format(ky_tag.split('_')[1].replace('p', '.')))
    axs[2, idx].set_xlabel(r'$P_{\mathrm{e}}^* / \mathrm{e} n_{\mathrm{e}} k_{\alpha}$')
    if idx == 0:
        axs[0, idx].set_ylabel(r'$\gamma \ (c_s/a)$')
        axs[1, idx].set_ylabel(r'$\gamma \ (c_s/a)$')
        axs[2, idx].set_ylabel(r'$\gamma \ (c_s/a)$')

save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations',
                         'HSX_Group', '2023', '2023-01-06', 'figures',
                         'etr_proxy_gene_comparison.png')
# plt.savefig(save_path)
plt.show()
