import os, sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence_optimization', 'pythonTools')
sys.path.append(ModDir)
import gist_reader as gr


gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_QHS_mn1824_alf0_s0p50_res256_npol4.dat')
gist = gr.read_gist(gist_path)
