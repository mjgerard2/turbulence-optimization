import os
import numpy as np
import gist_reader as gr

import matplotlib as mpl
import matplotlib.pyplot as plt


gist_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_s0p5_alpha0_npol4_nz8192.dat')

gist = gr.read_gist(gist_path)
gist.partition_wells()

pol_norm = gist.pol_dom / np.pi
num_of_wells = gist.pol_max.shape[0] - 1
idx_lim = -np.floor(0.5*num_of_wells)
well_idx = np.arange(idx_lim, -idx_lim+1, dtype=int)

pol_lft = gist.pol_max[np.argmin(np.abs(well_idx + 2))] / np.pi
pol_rgt = gist.pol_max[np.argmin(np.abs(well_idx - 2))+1] / np.pi

B = gist.data['modB']
gradPsi = gist.data['gxx']
Kn = gist.data['Kn']
Kg = gist.data['Kg']
D = gist.data['D']
curve_drive = (Kn / gradPsi) + D*Kg*(gradPsi / B)
KD_max = 1.1 * np.max(np.abs(curve_drive))

omD = np.zeros(3)
for i in range(3):
    gist.calc_PhaseSpace_omD_singleWell(well_ID=i)
    omD[i] = gist.omegaD

# Generate Plot #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

fig, ax1 = plt.subplots(1, 1, tight_layout=True, figsize=(16, 4))
ax2 = ax1.twinx()

ax2.spines['right'].set_color('tab:red')
ax2.tick_params(axis='y', colors='tab:red')

ax1.plot(pol_norm, B, c='k')
ax2.plot(pol_norm, curve_drive, c='tab:red')
ax2.plot(pol_norm, np.zeros(pol_norm.shape[0]), c='tab:red', ls='--', linewidth=1)

ax1.text(-1.6, 1.12, r'$\Omega_{{-2}} = {0:0.2f}$'.format(omD[2]))
ax1.text(1.21, 1.12, r'$\Omega_{{2}} = {0:0.2f}$'.format(omD[2]))

ax1.text(-0.92, 1.1, r'$\Omega_{{-1}} = {0:0.2f}$'.format(omD[1]))
ax1.text(0.58, 1.1, r'$\Omega_{{1}} = {0:0.2f}$'.format(omD[1]))

ax1.text(-0.17, 1.12, r'$\Omega_{{0}} = {0:0.2f}$'.format(omD[0]))

ax1.set_xlabel(r'$\theta / \pi$')
ax1.set_ylabel(r'$B$ (T)')
ax2.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D \kappa_g \frac{|\nabla \psi|}{B}$', c='tab:red')

ax1.set_xlim(pol_lft, pol_rgt)
ax1.set_ylim(0.99*np.min(B), 1.03*np.max(B))
ax2.set_ylim(-KD_max, KD_max)

ax1.grid()
# plt.show()
saveFig = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'TTF_2022', 'figures', 'QHS_field_line.png')
plt.savefig(saveFig, bbox_inches='tight')
