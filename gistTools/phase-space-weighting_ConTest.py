import numpy as np
from math import factorial

import matplotlib as mpl
import matplotlib.pyplot as plt

eps = 1
freq = 3
npts = 20
L_set = np.arange(1, 11, dtype=float)

arr_evn = np.array([2*i for i in np.arange(1, npts+1)])
arr_odd = np.array([2*i+1 for i in np.arange(1, npts+1)])

coef_evn_B = np.empty((L_set.shape[0], arr_evn.shape[0]))
coef_odd_B = np.empty((L_set.shape[0], arr_odd.shape[0]))

coef_evn_dB = np.empty((L_set.shape[0], arr_evn.shape[0]))
coef_odd_dB = np.empty((L_set.shape[0], arr_odd.shape[0]))

# Generate Plot Axes #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 16}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['lines.linewidth'] = 2

fig, axs = plt.subplots(2, 1, sharex=True, sharey=True, tight_layout=True, figsize=(8, 12))

ax1 = axs[0]
ax2 = axs[1]

for l, L in enumerate(L_set):
    eL = eps*L
    for i in np.arange(npts, dtype=int):
        evn = arr_evn[i]
        odd = arr_odd[i]

        coef_odd_B[l, i] = (eL*(eL**(odd-1) - eps**(odd-1)))/factorial(odd)
        coef_evn_B[l, i] = (eL**evn)/factorial(evn)

        coef_odd_dB[l, i] = (L*(eL**odd))*freq/factorial(odd)
        coef_evn_dB[l, i] = L*(eL**evn - eps**evn)*freq/factorial(evn)

    arr = np.stack((arr_evn, arr_odd), axis=1)
    arr = arr.flatten()

    coef_B = np.stack((coef_evn_B[l], coef_odd_B[l]), axis=1)
    coef_B = coef_B.flatten()

    coef_dB = np.stack((coef_evn_dB[l], coef_odd_dB[l]), axis=1)
    coef_dB = coef_dB.flatten()

    # Plot Data #
    ax1.plot(arr, coef_B, marker='s', ls='--', mfc='None', markersize=10, label=int(L))
    ax2.plot(arr, coef_dB, marker='s', ls='--', mfc='None', markersize=10, label=int(L))

# Axis Limits #
ax1.set_xlim(0, arr[-1])
max_coef = np.max(np.array([np.max(coef_B), np.max(coef_dB)]))
ax1.set_ylim(1e-1, 10**np.ceil(np.log10(max_coef)))

# Axis Scale #
ax1.set_yscale('log')
ax2.set_yscale('log')

# Axis Labels #
ax1.set_ylabel(r'Coef. of Highest Order $(B)$')
ax2.set_ylabel(r'Coef. of Highest Order $(dB/d\theta)$')
ax2.set_xlabel(r'Order of Poly. Exp.')

# Axis Legend #
ax1.legend(title='Order of mod-B', fontsize=16)
ax2.legend(title='Order of mod-B', fontsize=16)

# Axis Grid #
ax1.grid()
ax2.grid()

# Save/Show Figure #
plt.show()
save_name = 'phase-space-weighting_convergence.png'
# plt.savefig(save_name)
