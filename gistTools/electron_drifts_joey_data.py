import os
import numpy as np
import gist_reader as gr

import matplotlib as mpl
import matplotlib.pyplot as plt

# name_base = 'd-25_7_8_2022'
# gist_init_path = os.path.join('/home', 'michael', 'Desktop', 'joey_data', 'equilibria', 'opt_gist_files', name_base+'_init')
# gist_opt_path = os.path.join('/home', 'michael', 'Desktop', 'joey_data', 'equilibria', 'opt_gist_files', name_base+'_opt')

name_base = 'd-25_8_17_2022'
# name_base = 'd25_8_15_2022'
gist_init_path = os.path.join('/home', 'michael', 'Desktop', 'joey_data', 'equilibria', 'opt_gist_files', name_base+'_init_high_res')
gist_opt_path = os.path.join('/home', 'michael', 'Desktop', 'joey_data', 'equilibria', 'opt_gist_files', name_base+'_opt_high_res')

# Get Initialized Configuration Data #
gist_init = gr.read_gist(gist_init_path)
gist_init.partition_wells()
gist_init.calculate_p_weight()
gist_init.calculate_omega_de()

beg_init = gist_init.idx_max[np.argmin(np.abs(gist_init.well_indices))]
pol_norm_init = gist_init.pol_dom[beg_init:gist_init.idx_max[-1]+1]/np.pi
modB_init = gist_init.data['modB'][beg_init:gist_init.idx_max[-1]+1]

pol_omg_init = gist_init.pol_bnc[gist_init.pol_bnc >= gist_init.pol_dom[beg_init]]
omega_init = gist_init.model['omega_de'](pol_omg_init)

# Get Optimized Configuration Data #
gist_opt = gr.read_gist(gist_opt_path)
gist_opt.partition_wells()
gist_opt.calculate_p_weight()
gist_opt.calculate_omega_de()

beg_opt = gist_opt.idx_max[np.argmin(np.abs(gist_opt.well_indices))]
pol_norm_opt = gist_opt.pol_dom[beg_opt:gist_opt.idx_max[-1]+1]/np.pi
modB_opt = gist_opt.data['modB'][beg_opt:gist_opt.idx_max[-1]+1]

pol_omg_opt = gist_opt.pol_bnc[gist_opt.pol_bnc >= gist_opt.pol_dom[beg_opt]]
omega_opt = gist_opt.model['omega_de'](pol_omg_opt)

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axes #
fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(14, 6))

ax1 = axs[0]
ax2 = ax1.twinx()

ax3 = axs[1]
ax4 = ax3.twinx()

# Plot Data #
axi, = ax1.plot(pol_norm_init, modB_init, c='k')
twn, = ax2.plot(pol_omg_init/np.pi, omega_init, c='tab:blue')
ax2.plot(pol_omg_init/np.pi, np.zeros(pol_omg_init.shape), c=twn.get_color(), ls='--')

ax3.plot(pol_norm_opt, modB_opt, c=axi.get_color())
ax4.plot(pol_omg_opt/np.pi, omega_opt, c=twn.get_color())
ax4.plot(pol_omg_opt/np.pi, np.zeros(pol_omg_opt.shape), c=twn.get_color(), ls='--')

# Axis Title #
ax1.set_title(name_base+' (initial)')
ax3.set_title(name_base+' (optimized)')

# Axis Labels #
ax3.set_xlabel(r'$\theta/\pi$')

ax1.set_ylabel(r'$B \ (T)$', c=axi.get_color())
ax3.set_ylabel(r'$B \ (T)$', c=axi.get_color())

ax2.set_ylabel(r'$\hat{\overline{\omega}}_{de}$', c=twn.get_color())
ax4.set_ylabel(r'$\hat{\overline{\omega}}_{de}$', c=twn.get_color())

# Axis Colors #
ax1.spines["left"].set_edgecolor(axi.get_color())
ax3.spines["left"].set_edgecolor(axi.get_color())

ax2.spines["right"].set_edgecolor(twn.get_color())
ax4.spines["right"].set_edgecolor(twn.get_color())

ax1.tick_params(axis='y', colors=axi.get_color())
ax3.tick_params(axis='y', colors=axi.get_color())

ax2.tick_params(axis='y', colors=twn.get_color())
ax4.tick_params(axis='y', colors=twn.get_color())

# Axis Limits #
ax1.set_xlim(pol_norm_init[0], pol_norm_init[-1])

omD_max_init = np.max(np.abs(omega_init))
ax2.set_ylim(-omD_max_init, omD_max_init)

omD_max_opt = np.max(np.abs(omega_opt))
ax4.set_ylim(-omD_max_opt, omD_max_opt)

# Axis Grids #
# ax2.grid()
# ax4.grid()

# Save/Show #
save_name = os.path.join('/home', 'michael', 'Desktop', 'joey_data', 'figures', 'electron_drifts', name_base+'.png')
plt.savefig(save_name)
# plt.show()
