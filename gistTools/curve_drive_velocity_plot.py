import os
import numpy as np
import gist_reader as gr
import matplotlib as mpl

import sys
WORKDIR = os.path.dirname(os.getcwd())
sys.path.append(WORKDIR)

import plot_define as pd


gist_file = os.path.join('/home', 'michael', 'gist_ncsx_alf0_s0p5_res512_npol2.dat')
gist = gr.read_gist(gist_file, nfp=3)
gist.partition_wells()
gist.calculate_omega_de(integ_method='gtrapz')

pol_set = gist.partition_pol['well 0']
pol_idx = np.where((gist.pol_dom >= pol_set[0]) & (gist.pol_dom <= pol_set[2]))[0]
pol_well = gist.pol_dom[pol_idx]
B_well = gist.data['modB'][pol_idx]
B_extrema = np.array([np.min(B_well), np.max(B_well)])

curve_drive = gist.data['L2'][pol_idx]
omega_de = gist.model['omega_de'](pol_well)

# plotting parameters #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(3, 1, sharex=True, sharey=False, tight_layout=True, figsize=(7.5, 11))

# Plotting Axis #
ax1 = axs[0]
ax2 = axs[1]
ax3 = axs[2]
ax2_twn = ax2.twinx()

# Plot B Field and Curvature Drive #
plt1, = ax1.plot(pol_well/np.pi, B_well*gist.Bref, c='k')
plt2, = ax2.plot(pol_well/np.pi, omega_de, c='tab:blue')
plt2_twn, = ax2_twn.plot(pol_well/np.pi, curve_drive, c='tab:orange')
ax2.plot(pol_well/np.pi, np.zeros(pol_well.shape), c=plt2.get_color(), ls='--')

# axis colors #
ax2_twn.spines['right'].set_color(plt2_twn.get_color())
ax2_twn.tick_params(axis='y', colors=plt2_twn.get_color())

ax2.spines['left'].set_color(plt2.get_color())
ax2.tick_params(axis='y', colors=plt2.get_color())

# Plot Velocity Contours #
Vpar_max = 0
M_dom = np.flip(np.linspace(0, 1, 11))
cmap = plt.get_cmap('cividis', M_dom.shape[0])
for mdx, M in enumerate(M_dom):
    pol_set, Vpar_set = gist.calc_Vpar(pol_well, B_well, np.sqrt(M), B_extrema, v_thresh=1e-12)
    for pdx, pol in enumerate(pol_set):
        if pol is not None:
            ax3.fill_between(pol/np.pi, np.zeros(pol.shape), Vpar_set[pdx], color=cmap((1.-1e-6)*M))
            if mdx == 0:
                if np.max(Vpar_set[pdx]) > Vpar_max:
                    Vpar_max = np.max(Vpar_set[pdx])

# Highlight Negative Curvature Drive Region #
neg_idx = np.where(curve_drive < 0)[0]
neg_bool = ((neg_idx - np.roll(neg_idx, 1)) != 1)
negDX = np.where(neg_bool)[0]

alpha_clr = 0.3
for i, idx in enumerate(negDX[0:-1]):
    pol_beg = neg_idx[idx]
    pol_end = neg_idx[negDX[i+1] - 1] + 1

    ax1.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')
    ax2_twn.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')
    ax3.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')

if len(negDX) > 0:
    pol_beg = neg_idx[negDX[-1]]
    pol_end = neg_idx[-1]

    ax1.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')
    ax2_twn.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')
    ax3.axvspan(pol_well[pol_beg]/np.pi, pol_well[pol_end]/np.pi, alpha=alpha_clr, color='tab:orange')

# Label Colorbar #
M_dom = np.flip(np.linspace(0, 1, 6))
phi_bnc = np.arcsin(np.sqrt(B_extrema[0] / (B_extrema[1]*M_dom**2 + B_extrema[0]*(1-M_dom**2)))) / np.pi
phi_labs = ['{0:0.1f}'.format(p) for p in np.flip(M_dom)]

pos3 = ax3.get_position()
ax3c = fig.add_axes([0.81, pos3.y0-.0135, pos3.width/25, pos3.height*1.165])
norm = mpl.colors.Normalize(vmin=phi_bnc[0]-.005, vmax=phi_bnc[-1]+.005)
cb3 = mpl.colorbar.ColorbarBase(ax3c, cmap=cmap, norm=norm, orientation='vertical')
cb3.set_ticks(phi_bnc)
cb3.set_ticklabels(phi_labs)
cb3.ax.set_ylabel(r'$k^2$')

# Axis Labels #
ax1.set_ylabel(r'$B \ [\mathrm{T}]$', c=plt1.get_color())
ax2.set_ylabel(r'$\langle \hat{\mathbf{v}}_D \cdot \nabla y \rangle$', c=plt2.get_color())
ax2_twn.set_ylabel(r'$C_{\mathrm{D}} - \frac{\mu_0}{B^2}\frac{dp}{d\psi}$', c='tab:orange')
ax3.set_xlabel(r'$\theta/\pi$')
ax3.set_ylabel(r'$(v_{\parallel}/v)^2$')

# axis limits #
ax2.set_xlim(pol_well[0]/np.pi, pol_well[-1]/np.pi)
ax3.set_ylim(0, 1.0)

# axis grids #
ax1.grid()
ax2.grid()

plt.show()
save_path = os.path.join('/home', 'michael', 'ncsx_BAD_OG.png')
# plt.savefig(save_path)
