import os
import numpy as np
import gist_reader as gr

import matplotlib as mpl
import matplotlib.pyplot as plt

config_id = '0-1-0'
main_id = 'main_coil_{}'.format(config_id.split('-')[0])
set_id = 'set_{}'.format(config_id.split('-')[1])
job_id = 'job_{}'.format(config_id.split('-')[2])

third = False

# gist_path_one = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'gist_1')
# gist_path_two = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'omn_2p0_omti_0p0_omte_2p0', 'gist_16')
gist_path_one = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'gist_jasons_qhs')
gist_path_two = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_QHS_mn1824_alf0_s0p50_res128_npol4.dat')

gist_one = gr.read_gist(gist_path_one)
gist_two = gr.read_gist(gist_path_two)
if third:
    gist_tre = gr.read_gist(gist_path_tre)

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24
mpl.rcParams['lines.linewidth'] = 2

# Plotting Axes #
fig, axs = plt.subplots(6, 1, sharex=True, tight_layout=True, figsize=(14, 14))

ax11 = axs[0]
ax12 = axs[1]
ax13 = axs[2]
ax14 = axs[3]
ax15 = axs[4]
ax16 = axs[5]

pol_norm_one = gist_one.pol_dom / np.pi
pol_norm_two = gist_two.pol_dom / np.pi
if third:
    pol_norm_tre = gist_tre.pol_dom / np.pi

# Construct Plots #
ax11.plot(pol_norm_one, gist_one.data['modB'], c='k', label=r'Jason $(\hat{{s}} = {0:0.4f}, \ q_0 = {1:0.4f})$'.format(gist_one.shat, gist_one.q0))
ax12.plot(pol_norm_one, gist_one.data['Kn'], c='tab:red', label=r'$\kappa_n$')
ax12.plot(pol_norm_one, gist_one.data['Kg'], c='tab:blue', label=r'$\kappa_g$')
ax13.plot(pol_norm_one, gist_one.data['gxx'], c='k')
ax14.plot(pol_norm_one, gist_one.data['gxy'], c='k')
ax15.plot(pol_norm_one, gist_one.data['gyy'], c='k')
ax16.plot(pol_norm_one, gist_one.data['Jacob'], c='k')

ax11.plot(pol_norm_two, gist_two.data['modB'], c='k', ls='--', label=r'Michael $(\hat{{s}} = {0:0.4f}, \ q_0 = {1:0.4f})$'.format(gist_two.shat, gist_two.q0))
ax12.plot(pol_norm_two, gist_two.data['Kn'], c='tab:red', ls='--')
ax12.plot(pol_norm_two, gist_two.data['Kg'], c='tab:blue', ls='--')
ax13.plot(pol_norm_two, gist_two.data['gxx'], c='k', ls='--')
ax14.plot(pol_norm_two, gist_two.data['gxy'], c='k', ls='--')
ax15.plot(pol_norm_two, gist_two.data['gyy'], c='k', ls='--')
ax16.plot(pol_norm_two, gist_two.data['Jacob'], c='k', ls='--')

if third:
    ax11.plot(pol_norm_tre, gist_tre.data['modB'], c='k', ls=':', label=r'QHS_HSX_best | $\hat{{s}} = {0:0.4f}$'.format(gist_tre.shat))
    ax12.plot(pol_norm_tre, gist_tre.data['Kn'], c='tab:red', ls=':')
    ax12.plot(pol_norm_tre, gist_tre.data['Kg'], c='tab:blue', ls=':')
    ax13.plot(pol_norm_tre, gist_tre.data['gxx'], c='k', ls=':')
    ax14.plot(pol_norm_tre, gist_tre.data['gxy'], c='k', ls=':')
    ax15.plot(pol_norm_tre, gist_tre.data['gyy'], c='k', ls=':')
    ax16.plot(pol_norm_tre, gist_tre.data['Jacob'], c='k', ls=':')

# Axis Labels #
ax11.set_ylabel(r'$\hat{B}$')
ax13.set_ylabel(r'$g^{xx}$')
ax14.set_ylabel(r'$g^{xy}$')
ax15.set_ylabel(r'$g^{yy}$')
ax16.set_ylabel(r'$\hat{\sqrt{g}}$')
ax16.set_xlabel(r'$\theta / \pi$')

# Axis Limits #
ax11.set_xlim(-gist_one.n_pol, gist_one.n_pol)
kmax = 1.1*np.max(np.abs(ax12.get_ylim()))
ax12.set_ylim(-kmax, kmax)

# Axis Legends #
ax11.legend(bbox_to_anchor=(.5, 0.93), loc='lower center', frameon=False, ncol=2)
ax12.legend(bbox_to_anchor=(.5, 0.63), loc='lower center', frameon=False, ncol=2)

# Axis Grids #
ax11.grid()
ax12.grid()
ax13.grid()
ax14.grid()
ax15.grid()
ax16.grid()

# Save/Show #
# plt.show()
save_path = os.path.join('/mnt', 'GENE', 'nonlinear_data', '0-1-0_jason', 'figures', 'jason_vs_my_gist_data.pdf')
plt.savefig(save_path, format='pdf')
