import h5py as hf
import numpy as np
import os

import gist_reader as gr


res = 1024
well_IDs = np.arange(0, 6)
# alpha_tags = ['{0:0.2f}'.format(a).replace('.', 'p') for a in alpha_scan]

conID_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'conID_list.txt')
with open(conID_file, 'r') as f:
    lines = f.readlines()
    conIDs_list = ['0-1-0']
    for line in lines:
        line = line.strip().split()
        conIDs_list.append(line[0])

for cdx, conID in enumerate(conIDs_list):
    print('Working: '+conID+' ({0:0.0f}|{1:0.0f})'.format(cdx+1, len(conIDs_list)))
    conID_split = conID.split('-')

    mainID = 'main_coil_{}'.format(conID_split[0])
    setID = 'set_{}'.format(conID_split[1])
    jobID = 'job_{}'.format(conID_split[2])

    saveDirc = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID)
    saveFile = os.path.join(saveDirc, 'trapping_wells_res{}_2022-10-22.h5'.format(res))

    alpha_list = []
    if not os.path.isdir(saveDirc):
        os.system('mkdir -p '+saveDirc)
    # os.system('rm '+saveFile)

    if not os.path.exists(saveFile):
        try:
            if conID == '0-1-0':
                filePath = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID, 'gist_HSX_'+conID+'_s0p5_res{}_npol8_alpha0p00.dat'.format(res))
            else:
                filePath = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID, 'gist_HSX_'+conID+'_s0p5_res{}_npol8_alpha0p00.dat'.format(res))

            gist = gr.read_gist(filePath, nfp=4)

            gist.partition_wells()
            gist.calculate_p_weight()
            gist.calculate_omega_de()

            idx_lim = -np.floor(0.5*gist.pol_min.shape[0])
            well_indices = np.arange(idx_lim, -idx_lim+1, dtype=int)

            for idx, well_ID in enumerate(well_IDs):
                if (well_ID < well_indices[0]) or (well_ID > well_indices[-1]):
                    raise IndexError('well_ID = {} is outside the parallel domain of the gist file.'.format(well_ID))

                well_idx = np.argmin(np.abs(well_indices - well_ID))
                idx_lft = gist.idx_max[well_idx]
                idx_rgt = gist.idx_max[well_idx+1]

                if gist.pol_dom[idx_rgt] < gist.pol_bnc[-1]:
                    npts = int(1 + (gist.pol_dom[idx_rgt] - gist.pol_dom[idx_lft])/(2*gist.hlf_stp))
                    pol_well = np.linspace(gist.pol_dom[idx_lft], gist.pol_dom[idx_rgt], npts, endpoint=False)
                else:
                    npts = 1 + (gist.pol_dom[gist.pol_bnc[-1]] - gist.pol_dom[idx_lft])/(2*gist.hlf_stp)
                    pol_well = np.linspace(gist.pol_dom[idx_lft], gist.pol_bnc[-1], npts)

                well_key = 'well {0:0.0f}'.format(idx)
                with hf.File(saveFile, 'a') as hf_:
                    hf_.create_dataset(well_key+'/theta_bnc', data=pol_well)
                    hf_.create_dataset(well_key+'/M', data=gist.model['M'](pol_well))
                    hf_.create_dataset(well_key+'/omegaD', data=gist.model['omega_de'](pol_well))
                    hf_.create_dataset(well_key+'/p_wght', data=gist.model['p-weight'](pol_well))
                    hf_.create_dataset(well_key+'/gyy_avg', data=gist.model['bounce-average-gyy'](pol_well))

        except IndexError:
            print('Well is not in domain!')
