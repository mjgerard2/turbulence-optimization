import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


# GENE Path #
tem_idx = 1
tem_ky = 0.4
gene_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')

# Read in Metric Data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# Append Metric Data with QHS #
qhs_data = np.array([0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1])
met_data = np.append(met_data, qhs_data).reshape(met_data.shape[0]+1, met_data.shape[1])

# Read in Drift Data #
xi_Te = 3
xi_ne = 3

Omega = np.empty((met_data.shape[0], 3))
gene_tem = np.empty(met_data.shape[0])
for mdx, met in enumerate(met_data):
    conID = '-'.join(['{0:0.0f}'.format(iD) for iD in met[0:3]])
    print(conID+' ({0:0.0f}|{1:0.0f})'.format(mdx+1, met_data.shape[0]))

    if conID == '0-1-0':
        qhs_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_normalizations.h5')
        with hf.File(qhs_path, 'r') as hf_:
            gene_tem[mdx] = hf_['TEM'][tem_idx, 1]
    else:
        with hf.File(gene_path, 'r') as hf_:
            gene_tem[mdx] = hf_[conID][tem_idx, 1]

    mainID = 'main_coil_{0:0.0f}'.format(met[0])
    setID = 'set_{0:0.0f}'.format(met[1])
    jobID = 'job_{0:0.0f}'.format(met[2])
    data_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mainID, setID, jobID, 'trapping_wells_res1024.h5')

    with hf.File(data_path, 'r') as hf_:
        for i in range(Omega.shape[1]):
            well_key = 'well {0:0.0f}'.format(i)
            omegaD = hf_[well_key+'/omegaD'][()]
            p_wght = hf_[well_key+'/p_wght'][()]
            theta_bnc = hf_[well_key+'/theta_bnc'][()]

            D = np.trapz(omegaD*omegaD*p_wght, theta_bnc)
            E = np.trapz(omegaD*p_wght, theta_bnc)
            Omega[mdx, i] = (3.5*D - (2*xi_Te + xi_ne) * E)

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

# Define Axes #
fig, axs = plt.subplots(2, 2, sharex=True, sharey=True, tight_layout=False, figsize=(12, 10))

# Plot Omegas #
clr_min = 0
clr_max = 1.01*np.nanmax(gene_tem)

im = axs[0, 0].scatter(met_data[:, 9], Omega[:, 0], s=150, marker='o', c=gene_tem, edgecolor='k', vmin=clr_min, vmax=clr_max, cmap='jet')
axs[0, 1].scatter(met_data[:, 9], Omega[:, 1], s=150, marker='s', c=gene_tem, edgecolor='k', vmin=clr_min, vmax=clr_max, cmap='jet')
axs[1, 0].scatter(met_data[:, 9], Omega[:, 2], s=150, marker='v', c=gene_tem, edgecolor='k', vmin=clr_min, vmax=clr_max, cmap='jet')
axs[1, 1].scatter(met_data[:, 9], np.mean(Omega, axis=1), s=150, marker='d', c=gene_tem, edgecolor='k', vmin=clr_min, vmax=clr_max, cmap='jet')

# Colorbar #
fig.subplots_adjust(right=0.83)
cbar_ax = fig.add_axes([0.85, 0.15, 0.02, 0.7])
cbar = fig.colorbar(im, cax=cbar_ax)
cbar.ax.set_ylabel(r'$\gamma \ (c_s / a)$')

# Label Axes #
# axs[0, 0].set_ylabel(r'$\langle \overline{\omega}_{de} \rangle$')
# axs[1, 0].set_ylabel(r'$\langle \overline{\omega}_{de} \rangle$')
axs[0, 0].set_ylabel(r'$\langle \overline{\omega}_{de} \left(\overline{\omega}_{de} - \omega_{*e}^T\right) \rangle$')
axs[1, 0].set_ylabel(r'$\langle \overline{\omega}_{de} \left(\overline{\omega}_{de} - \omega_{*e}^T\right) \rangle$')
axs[1, 0].set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')
axs[1, 1].set_xlabel(r'$\mathcal{K} / \mathcal{K}^*$')

# Axis Titles #
axs[0, 0].set_title('Well 0')
axs[0, 1].set_title('Well 1')
axs[1, 0].set_title('Well 2')
axs[1, 1].set_title('Well Avg.')

# Axis Grids #
axs[0, 0].grid()
axs[0, 1].grid()
axs[1, 0].grid()
axs[1, 1].grid()

# Axis Limits #
y_min = -13
y_max = 1.05*np.max(Omega)
axs[0, 0].set_ylim(y_min, y_max)

x_min = 0.99 * np.min(met_data[:, 9])
x_max = 1.01 * np.max(met_data[:, 9])
axs[0, 0].set_xlim(x_min, x_max)

# Save/Show #
# save_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'figures', 'gene_figs', 'TEM', 'power_trans_ky0p4_tmp7.png')
# plt.savefig(save_path)
plt.show()
