import os
import numpy as np
import gist_reader as gr


path_lowK = os.path.join('/mnt', 'GENE', 'linear_data', '5-1-485', 'ky_0p1', 'gist_1_1')
gist_lowK = gr.read_gist(path_lowK)

kn = gist_lowK.data['Kn']/gist_lowK.minor
jacob = gist_lowK.data['Jacob'] * (2*gist_lowK.q0/(gist_lowK.minor**3))
kn_avg = np.trapz(kn*jacob, gist_lowK.pol_dom) / np.trapz(jacob, gist_lowK.pol_dom)
print(kn_avg)

path_qhs = os.path.join('/mnt', 'GENE', 'linear_data', '0-1-0', 'ky_0p1', 'gist_1_1')
gist_qhs = gr.read_gist(path_qhs)

kn = gist_qhs.data['Kn']/gist_qhs.minor
jacob = gist_qhs.data['Jacob'] * (2*gist_qhs.q0/(gist_qhs.minor**3))
kn_avg = np.trapz(kn*jacob, gist_qhs.pol_dom) / np.trapz(jacob, gist_qhs.pol_dom)
print(kn_avg)

path_hghK = os.path.join('/mnt', 'GENE', 'linear_data', '897-1-0', 'ky_0p1', 'gist_1_1')
gist_hghK = gr.read_gist(path_hghK)

kn = gist_hghK.data['Kn']/gist_hghK.minor
jacob = gist_hghK.data['Jacob'] * (2*gist_hghK.q0/(gist_hghK.minor**3))
kn_avg = np.trapz(kn*jacob, gist_hghK.pol_dom) / np.trapz(jacob, gist_hghK.pol_dom)
print(kn_avg)
