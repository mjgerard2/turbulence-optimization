import os
import gist_reader as gr


gist_output = os.path.join('/mnt', 'HSX_Database', 'GENE', 'nonlinear_data', '0-1-0_jason', 'gist_3000')
gist_convrt = os.path.join('/mnt', 'HSX_Database', 'GENE', 'nonlinear_data', '0-1-0_jason', 'gist_QHS')

alpha0 = 0
sign_Ip_CW = -1
sign_Bt_CW = -1
gist = gr.read_gist(gist_output)

with open(gist_convrt, 'w') as f:
    f.write('&parameters\n')
    f.write('!PEST coordinates\n')
    f.write('!s0, alpha0 = {} {}\n'.format(gist.s0, alpha0))
    f.write('!major, minor radius[m] = {} {}\n'.format(gist.major, gist.minor))
    f.write('!Bref = {}\n'.format(gist.Bref))
    f.write('my_dpdx = {}\n'.format(gist.my_dpdx))
    f.write('q0 = {}\n'.format(gist.q0))
    f.write('shat = {}\n'.format(gist.shat))
    f.write('gridpoints = {0:0.0f}\n'.format(gist.gridpoints))
    f.write('n_pol = {0:0.0f}\n'.format(gist.n_pol))
    f.write('sign_Ip_CW = {0:0.0f}\n'.format(sign_Ip_CW))
    f.write('sign_Bt_CW = {0:0.0f}\n'.format(sign_Bt_CW))
    f.write('/\n')
    for i in range(gist.gridpoints):
        line = '\t'.join(['{}'.format(val) for val in gist.colm_data[:, i]])
        f.write('\t'+line+'\n')
