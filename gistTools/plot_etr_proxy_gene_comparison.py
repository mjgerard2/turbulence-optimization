import os
import h5py as hf
import numpy as np
import read_power_proxy_data as rpp

import matplotlib as mpl
import matplotlib.pyplot as plt



# Define GENE Data Path #
gene_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 20}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 24

# Read in Proxy Comparison Data #
ky_tags = ['ky_0p1', 'ky_0p4', 'ky_0p7', 'ky_1p0']
# ky_tags = ['ky_0p4']

proxy_comp_exact = {}
proxy_comp_apprx = {}

for ky_tag in ky_tags:
    file_exact = os.path.join('/mnt', 'GENE', 'power_proxy_M_inf_'+ky_tag+'_1_x.h5')
    file_apprx = os.path.join('/mnt', 'GENE', 'power_proxy_M_10_'+ky_tag+'_1_x.h5')

    pwr_exact = rpp.read_power_proxy_data(file_exact)
    pwr_apprx = rpp.read_power_proxy_data(file_apprx)

    pwr_exact.calculate_etr_proxy()
    pwr_exact.compare_proxy_with_GENE(gene_file, ky_tag)

    pwr_apprx.calculate_etr_proxy()
    pwr_apprx.compare_proxy_with_GENE(gene_file, ky_tag)

    proxy_comp_exact[ky_tag] = pwr_exact.proxy_comparison
    proxy_comp_apprx[ky_tag] = pwr_apprx.proxy_comparison

# Plot Data #
fig, axs = plt.subplots(2, 2, sharex=True, sharey=False, tight_layout=True, figsize=(12, 10))

ax1 = axs[0, 0]
ax2 = axs[0, 1]
ax3 = axs[1, 0]
ax4 = axs[1, 1]

for idx, ky_tag in enumerate(ky_tags):
    i = int(idx / 2)
    j = idx % 2

    data_exact = proxy_comp_exact[ky_tag]
    data_apprx = proxy_comp_apprx[ky_tag]

    axs[i, j].scatter(data_exact[:, 0], data_exact[:, 1], c='None', edgecolor='tab:blue', marker='o', s=50, label=r'$M = \infty$')
    axs[i, j].scatter(data_apprx[:, 0], data_apprx[:, 1], c='None', edgecolor='tab:orange', marker='s', s=50, label=r'$M = 10$')

    axs[i, j].set_title(r'$k_y\rho_s = {}$'.format(ky_tag.split('_')[1].replace('p', '.')))
    if i == 1:
        axs[i, j].set_xlabel(r'$P_{\mathrm{e}}^* / \mathrm{e} n_{\mathrm{e}} k_{\alpha}$')
    if j == 0:
        axs[i, j].set_ylabel(r'$\gamma \ (c_s/a)$')

    axs[i, j].set_ylim(0, 1.05*np.max(data_exact[:, 1]))

    axs[i, j].grid()
    axs[i, j].legend()

save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations',
                         'HSX_Group', '2023', '2023-01-06', 'figures',
                         'etr_proxy_gene_comparison.png')
# plt.savefig(save_path)
plt.show()
