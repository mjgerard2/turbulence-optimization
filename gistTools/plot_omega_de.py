import os
import numpy as np
import gist_reader as gr

import matplotlib as mpl
import matplotlib.pyplot as plt

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd


# Import QHS gist file #
# gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_QHS_mn1824_alf0_s0p50_res256_npol4.dat')
gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_1088', 'set_1', 'job_0', 'gist_1088-1-0_mn1824_alf0_s0p59_res128_npol4.dat')
# gist_file = os.path.join('/home', 'michael', 'gist_ncsx_alf0_s0p5_res512_npol2.dat')
gist = gr.read_gist(gist_file)
gist.partition_wells()
gist.calculate_omega_de(integ_method='gtrapz')

pol_bnc = gist.pol_bnc
B_field = gist.model['modB'](pol_bnc)
omg_alf = gist.model['omega_alpha'](pol_bnc)
omg_psi = gist.model['omega_psi'](pol_bnc)

# Plottng Parameters #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(3, 1, sharex=True, tight_layout=True, figsize=(7, 14))

# plot data #
plt1, = axs[0].plot(pol_bnc/np.pi, B_field, c='k')
plt2, = axs[1].plot(pol_bnc/np.pi, omg_alf, c='k')
plt3, = axs[2].plot(pol_bnc/np.pi, omg_psi, c='k')

# axis limits #
max_alf = np.max(np.abs(omg_alf))
max_psi = np.max(np.abs(omg_psi))

axs[0].set_xlim(-2, 2)
axs[1].set_ylim(-max_alf, max_alf)
axs[2].set_ylim(-max_psi, max_psi)

# axis labels #
axs[2].set_xlabel(r'$\theta_{\mathrm{b}}/\pi$')
axs[0].set_ylabel(r'$B$', color=plt1.get_color())
axs[1].set_ylabel(r'$\omega_{\alpha}$', color=plt2.get_color())
axs[2].set_ylabel(r'$\omega_{\psi}$', color=plt2.get_color())

# axis color #
axs[1].spines['right'].set_color(plt2.get_color())
axs[1].tick_params(axis='y', colors=plt2.get_color())

axs[2].spines['right'].set_color(plt3.get_color())
axs[2].tick_params(axis='y', colors=plt3.get_color())

# axis grid #
axs[0].grid()
axs[1].grid()
axs[2].grid()

# save/show #
plt.show()
