import os
import numpy as np
import gist_reader as gr


gist_dirc = os.path.join('/home', 'michael', 'Desktop', 'joey_data', 'equilibria', 'opt_gist_files')
save_dirc = os.path.join('/home', 'michael', 'Desktop', 'joey_data', 'figures', 'field_calculations')
gist_names = [f.name for f in os.scandir(gist_dirc)]

for gist_name in gist_names:
    print(gist_name)
    gist_path = os.path.join(gist_dirc, gist_name)
    save_name = os.path.join(save_dirc, gist_name)

    gist = gr.read_gist(gist_path)
    # gist.calculate_Omega_singleWell(0)
    # gist.plot_Omega_dependencies()
    # gist.curve_drive_velocity_plot(0)
    gist.plot_fields(pol_lim=gist.n_pol*np.pi, save_name=save_name)
