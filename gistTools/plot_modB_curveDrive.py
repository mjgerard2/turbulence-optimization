import os
import numpy as np
import gist_reader as gr

import matplotlib as mpl
import matplotlib.pyplot as plt


gist_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'gist_s0p5_alpha0_npol4_nz8192.dat')

gist = gr.read_gist(gist_file, nfp=4)
gist.partition_wells()

# Define poloidal domain #
idx_lim = np.floor(.5*gist.pol_min.shape[0])
well_idx = np.arange(-idx_lim, idx_lim+1, dtype=int)

well_ID = 2
idx_lft = gist.idx_max[np.argmin(np.abs(well_idx + well_ID))]
idx_rgt = gist.idx_max[np.argmin(np.abs(well_idx - well_ID - 1))]

pol_norm = gist.pol_dom[idx_lft:idx_rgt+1] / np.pi
B_well = gist.data['modB'][idx_lft:idx_rgt+1]

# Calculate curvature drive #
gradPsi = gist.data['gxx'][idx_lft:idx_rgt+1]
Kn = gist.data['Kn'][idx_lft:idx_rgt+1]
Kg = gist.data['Kg'][idx_lft:idx_rgt+1]
D = gist.data['D'][idx_lft:idx_rgt+1]
curve_drive = (Kn / gradPsi) + D*Kg*(gradPsi / B_well)

cd_max = 1.1 * np.max(np.abs(curve_drive))

# Plotting parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 22
mpl.rcParams['lines.linewidth'] = 2

fig, ax1 = plt.subplots(1, 1, sharex=True, tight_layout=True, figsize=(14, 4))
ax2 = ax1.twinx()

ax2.spines['right'].set_color('tab:red')
ax2.tick_params(axis='y', colors='tab:red')

ax1.plot(pol_norm, B_well, c='k')
ax2.plot(pol_norm, curve_drive, c='tab:red')
ax2.plot(pol_norm, np.zeros(pol_norm.shape[0]), c='tab:red', ls='--')

ax1.set_xlabel(r'$\theta / \pi$')
ax1.set_ylabel(r'$B$ (T)')
ax2.set_ylabel(r'$\frac{\kappa_n}{|\nabla \psi|} + D\kappa_g \frac{|\nabla \psi|}{B}$', c='tab:red')

ax1.set_xlim(pol_norm[0], pol_norm[-1])
ax2.set_ylim(-cd_max, cd_max)

ax1.grid()
# plt.show()

savePath = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'TTF_2022', 'figures', 'fieldline_modB_curveDrive.png')
plt.savefig(savePath)
