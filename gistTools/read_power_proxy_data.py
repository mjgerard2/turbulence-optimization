import os
import h5py as hf
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt


class read_power_proxy_data():

    def __init__(self, data_file):

        if not os .path.isfile(data_file):
            raise IOError(data_file + " does not exist.")

        self.power_data = {}
        with hf.File(data_file, 'r') as hf_:
            for key in hf_:
                self.power_data[key] = hf_[key][()]

        self.data_idx = {'pol': 0,
                         'jacob': 1,
                         'phi': 2,
                         'p_wght': 3,
                         'res_func': 4,
                         'modB': 5,
                         'omega_de': 6}

        self.label_list = [r'$\theta_b/\pi$', r'$\sqrt{g}$',
                           r'$|\overline{\phi}|^2$', r'$p(\theta_b)$',
                           r'$F(\theta_b)$', r'$B$',
                           r'$\hat{\overline{\omega}}_{\mathrm{de}}$']

    def plot_data(self, ax, config_id, data_key, plot_info={}):
        """ Plot the specified data as a function of poloidal angle.

        Parameters
        ----------
        ax : obj
            matplotlib plotting axis
        config_id : str
            Configuration ID that specifies which configuration's data you want
            plotted.
        data_key : str
            Data key that specifies what data is to be plotted.
        plot_info : dict, optional
            Dictionary containing the keyword options for the plotting script.
            Default is empty.
        """
        data = self.power_data[config_id]
        x_idx = self.data_idx['pol']
        y_idx = self.data_idx[data_key]

        ax.plot(data[:, x_idx]/np.pi, data[:, y_idx], **plot_info)

        ax.set_xlabel(self.label_list[x_idx])
        ax.set_ylabel(self.label_list[y_idx])

    def calculate_etr_proxy(self):
        """ Calculate the ETR proxy.
        """
        self.etr_proxy = {}
        for config_id in self.power_data:
            data = self.power_data[config_id]

            pol = data[:, 0]
            jacob = data[:, 1]
            phi = data[:, 2]
            p_wght = data[:, 3]
            res_func = data[:, 4]
            B = data[:, 5]

            integ = .5*np.sqrt(.5*np.pi)*jacob*phi*p_wght*res_func/B
            self.etr_proxy[config_id] = np.trapz(integ, pol)

    def calculate_modified_etr_proxy(self, omg, sig, xi_n, xi_T):
        """ Calculate the ETR proxy with a different set of constants.

        Parameters
        ----------
        omg : float
            Drift wave frequency
        sig : float
            Drift wave frequency uncertainty
        xi_n : float
            Normalized electron denstiy gradient
        xi_T : float
            Normalized electron temperature gradient
        """
        self.etr_proxy = {}
        for config_id in self.power_data:
            data = self.power_data[config_id]

            pol = data[:, 0]
            jacob = data[:, 1]
            phi = data[:, 2]
            p_wght = data[:, 3]
            B = data[:, 5]
            omg_de = data[:, 6]

            idx_list = np.where(omg*omg_de > sig*sig)[0]
            omg_rat = omg / omg_de[idx_list]
            sig_rat2 = (sig / omg_de[idx_list])**2

            res_func = np.zeros(omg_de.shape)
            res_func[idx_list] = omg_rat * np.sqrt(omg_rat - sig_rat2) *\
                (omg - xi_n - (omg_rat - sig_rat2 - 1.5)*xi_T) *\
                np.exp(-omg_rat + .5*sig_rat2)

            integ = .5*np.sqrt(.5*np.pi)*jacob*phi*p_wght*res_func/B
            self.etr_proxy[config_id] = np.trapz(integ, pol)

    def calculate_test_proxy(self, omg, sig, xi_n, xi_T):
        """ Use the data to calculate different combinations of the variables
        in the ETR proxy.
        """
        self.test_proxy = {}
        for key in self.power_data:
            data = self.power_data[key]

            pol = data[:, 0]
            # jacob = data[:, 1]
            # phi = data[:, 2]
            p_wght = data[:, 3]
            # res_func = data[:, 4]
            # modB = data[:, 5]
            omg_de = data[:, 6]

            idx_list = np.where(omg*omg_de > sig*sig)[0]
            omg_rat = omg / omg_de[idx_list]
            sig_rat2 = (sig / omg_de[idx_list])**2

            res_func = np.zeros(omg_de.shape)
            res_func[idx_list] = omg_rat * np.sqrt(omg_rat - sig_rat2) *\
                (omg - xi_n - (omg_rat - sig_rat2 - 1.5)*xi_T) *\
                np.exp(-omg_rat + .5*sig_rat2)

            norm_inv = 1./np.trapz(p_wght, pol)
            self.test_proxy[key] = norm_inv * np.trapz(p_wght * res_func, pol)
            # self.test_proxy[key] = norm_inv * np.trapz(p_wght * omega_de, pol)
            # self.test_proxy[key] = np.trapz(jacob*phi*p_wght*res_func/modB, pol)

    def compare_proxy_with_GENE(self, gene_file, ky_tag, test=False):
        """ Compare the experimental proxy values with GENE data.

        Parameters
        ----------
        gene_file : str
            Global path to the linear GENE data.
        ky_tag : str
            String that specifies which ky data is being compared.
        """
        if ky_tag == 'ky_0p1':
            ky_idx = 0
        elif ky_tag == 'ky_0p4':
            ky_idx = 1
        elif ky_tag == 'ky_0p7':
            ky_idx = 2
        elif ky_tag == 'ky_1p0':
            ky_idx = 3
        else:
            raise NameError(ky_tag+' is not currently an allowable tag.')

        if test:
            self.proxy_comparison = np.empty((len(self.test_proxy), 2))
            with hf.File(gene_file, 'r') as hf_:
                for idx, key in enumerate(self.test_proxy):
                    self.proxy_comparison[idx, 0] = self.test_proxy[key]
                    self.proxy_comparison[idx, 1] = hf_[key][ky_idx, 1]
        else:
            self.proxy_comparison = np.empty((len(self.etr_proxy), 2))
            with hf.File(gene_file, 'r') as hf_:
                for idx, key in enumerate(self.etr_proxy):
                    self.proxy_comparison[idx, 0] = self.etr_proxy[key]
                    self.proxy_comparison[idx, 1] = hf_[key][ky_idx, 1]


if __name__ == "__main__":
    # Plotting Parameters #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 20}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 24

    # Read in Power Proxy File #
    ky_tag = 'ky_1p0'

    file_exact = os.path.join('/mnt', 'GENE', 'power_proxy_M_inf_'+ky_tag+'_1_x.h5')
    file_apprx = os.path.join('/mnt', 'GENE', 'power_proxy_M_10_'+ky_tag+'_1_x.h5')

    pwr_exact = read_power_proxy_data(file_exact)
    pwr_apprx = read_power_proxy_data(file_apprx)

    # Compare Proxy With GENE Data #
    if True:
        gene_file = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'omega_data_1_x.h5')

        pwr_exact.calculate_etr_proxy()
        pwr_exact.compare_proxy_with_GENE(gene_file, ky_tag)

        pwr_apprx.calculate_etr_proxy()
        pwr_apprx.compare_proxy_with_GENE(gene_file, ky_tag)

        fig, ax = plt.subplots(1, 1, tight_layout=True)

        data_exact = pwr_exact.proxy_comparison
        data_apprx = pwr_apprx.proxy_comparison

        ax.scatter(data_exact[:, 0], data_exact[:, 1], c='None', edgecolor='k', marker='o', s=50, label=r'$M = \infty$')
        ax.scatter(data_apprx[:, 0], data_apprx[:, 1], c='None', edgecolor='tab:green', marker='s', s=50, label=r'$M = 10$')

        ax.set_xlabel(r'$P_{\mathrm{e}}^* / \mathrm{e} n_{\mathrm{e}} k_{\alpha}$')
        ax.set_ylabel(r'$\gamma \ (c_s/a)$')
        ax.set_title(r'$k_y\rho_s = {}$'.format(ky_tag.split('_')[1].replace('p', '.')))

        gamma_max = np.array([np.max(data_exact[:, 1]), np.max(data_apprx[:, 1])])
        ax.set_ylim(0, 1.05*np.max(gamma_max))

        ax.grid()
        ax.legend()
        plt.show()

    # Plot Resonance Function #
    if False:
        config_id = '56-1-728'

        pol_dom = pwr_exact.power_data[config_id][:, 0]/np.pi
        p_wght = pwr_exact.power_data[config_id][:, 3]
        F_res = pwr_exact.power_data[config_id][:, 4]
        B = pwr_exact.power_data[config_id][:, 5]
        omg_de = pwr_exact.power_data[config_id][:, 6]

        F_res_wght = F_res * p_wght

        # Plotting Axis #
        fig, axs = plt.subplots(2, 1, sharex=True, tight_layout=True, figsize=(8, 6))

        ax1 = axs[0]
        ax2 = ax1.twinx()

        ax3 = axs[1]
        ax4 = ax3.twinx()

        ax1.plot(pol_dom, B, c='tab:blue')
        ax2.plot(pol_dom, omg_de, c='tab:orange')

        ax3.plot(pol_dom, B, c='tab:blue')
        ax4.plot(pol_dom, F_res_wght, c='tab:red')

        ax1.set_ylabel(r'$B$')
        ax2.set_ylabel(r'$\hat{\overline{\omega}}_{\mathrm{de}}$', c='tab:orange')
        ax3.set_ylabel(r'$B$')
        ax4.set_ylabel(r'$F(\theta_b) p(\theta_b)$', c='tab:red')
        ax3.set_xlabel(r'$\theta / \pi$')

        ax2.spines["right"].set_edgecolor('tab:orange')
        ax2.tick_params(axis='y', colors='tab:orange')

        ax4.spines["right"].set_edgecolor('tab:red')
        ax4.tick_params(axis='y', colors='tab:red')

        ax1.set_xlim(-8, 8)

        ax2.grid()
        ax4.grid()

        # ax.legend()
        plt.show()

    # Compare Resonance Functions #
    if False:
        config_id = '56-1-728'

        pol_exact = pwr_exact.power_data[config_id][:, 0]/np.pi
        p_wght_exact = pwr_exact.power_data[config_id][:, 3]
        F_res_exact = pwr_exact.power_data[config_id][:, 4]
        B = pwr_exact.power_data[config_id][:, 5]
        F_res_wght_exact = F_res_exact * p_wght_exact

        pol_apprx = pwr_apprx.power_data[config_id][:, 0]/np.pi
        p_wght_apprx = pwr_apprx.power_data[config_id][:, 3]
        F_res_apprx = pwr_apprx.power_data[config_id][:, 4]
        B = pwr_apprx.power_data[config_id][:, 5]
        F_res_wght_apprx = F_res_apprx * p_wght_apprx

        # Plotting Axis #
        fig, ax1 = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))
        ax2 = ax1.twinx()

        ax1.plot(pol_exact, B, c='tab:blue')
        ax2.plot(pol_exact, F_res_wght_exact, c='tab:orange')
        ax2.plot(pol_apprx, F_res_wght_apprx, c='tab:orange', ls='--')

        ax1.set_ylabel(r'$B$')
        ax2.set_ylabel(r'$F(\theta_b) p(\theta_b)$', c='tab:orange')
        ax1.set_xlabel(r'$\theta / \pi$')

        ax2.spines["right"].set_edgecolor('tab:orange')
        ax2.tick_params(axis='y', colors='tab:orange')

        ax1.set_xlim(-8, 8)

        ax2.grid()

        # ax.legend()
        plt.show()

    # Plot Eigenfunction Quantity #
    if False:
        config_id = '5-1-485'

        data = pwr_exact.power_data[config_id]
        pol = data[:, 0]/np.pi
        jacob = data[:, 1]
        phi = data[:, 2]
        p_wght = data[:, 3]
        res_func = data[:, 4]
        modB = data[:, 5]
        omega_de = data[:, 6]

        phi = phi / np.trapz(phi, pol)

        integ = jacob*p_wght*res_func/modB

        P_proxy = np.trapz(integ*phi, pol*np.pi)
        print('Power proxy = {0:0.2f}'.format(P_proxy))

        # Plotting Axis #
        fig, ax1 = plt.subplots(1, 1, tight_layout=True, figsize=(8, 6))
        ax2 = ax1.twinx()

        ax1.plot(pol, phi, c='k')
        ax2.plot(pol, integ, c='tab:orange')

        ax1.set_ylabel(r'$|\overline{\phi}|^2$')
        ax2.set_ylabel(r'$\sqrt{g}pF/B$', c='tab:orange')
        ax1.set_xlabel(r'$\theta / \pi$')

        ax2.spines["right"].set_edgecolor('tab:orange')
        ax2.tick_params(axis='y', colors='tab:orange')

        ax1.set_xlim(-8, 8)

        ax2.grid()

        # ax.legend()
        plt.show()

