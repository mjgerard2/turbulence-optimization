# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 10:46:07 2021

@author: micha
"""

import time
import random

import numpy as np
import h5py as hf

from multiprocessing import Process, Queue, cpu_count

import trapped_resonance_calculation as tRes

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.wout_read as wr


def run_cell(job_Q, done_Q):
    while True:
        job = job_Q.get()
        if job['end']:
            done_Q.put(job)
            break

        else:
            job_track = job['job tracker']
            nfp = job['number of field periods']
            geomDir = job['parent directory of wout file']
            geomFile = job['wout file name']
            Brng = job['range of mod B values']
            Vmod = job['velocity']
            pitch_key = job['pitch key']
            pitch_dom = job['pitch domain']
            pol_dom = job['poloidal domain']
            psi_val = job['radial coordinate']
            temp = job['temperature']
            bnc_num = job['number of bounce periods']
            npts = job['particles per sampling cell']
            tor_width = job['half toroidal sampling width']

            sign_pop = np.array([-1, 1])
            Dpol = pol_dom[1] - pol_dom[0]
            Dpitch = pitch_dom[1] - pitch_dom[0]

            cnts = 0
            while cnts < npts:
                u_pnt = pol_dom[0] + Dpol * random.uniform(0, 1)
                v_pnt = (u_pnt/nfp)
                v_pnt = v_pnt + tor_width * random.choices(sign_pop)[0] * random.uniform(0, 1)

                pos_init = np.array([psi_val, u_pnt, v_pnt])
                pitch = pitch_dom[0] + Dpitch * random.uniform(0, 1)

                t_res = tRes.trapped_resonance(geomDir, geomFile, pos_init, Vmod, pitch, bnc_num=bnc_num)

                try:
                    t_res.RG_integrate(1e-8)
                    Bmirr, alf_drift, psi_drift = t_res.calc_drift_freq(temp=temp)
                    M = (Bmirr - Brng[0]) / (Brng[1] - Brng[0])
                    M = np.sign(M) * np.sqrt(np.abs(M))

                    results = np.array([psi_val, u_pnt, v_pnt, pitch, M, alf_drift, psi_drift])
                    datum = {'end': False,
                             'pitch key': pitch_key,
                             'data': results}

                    done_Q.put(datum)

                    cnts += 1

                except ValueError:
                    continue

            print(job_track)


def calc_resonance_parallel(runInfo):
    npts = runInfo['particles per sampling cell']
    cores = runInfo['number of cores']
    pitch_set = runInfo['pitch domain']
    pol_set = runInfo['poloidal domain']

    pitch_num = pitch_set.shape[0] - 1
    pol_num = pol_set.shape[0] - 1
    total_num = pitch_num * pol_num

    keys = ['number of field periods', 'parent directory of wout file',
            'wout file name', 'range of mod B values', 'velocity',
            'radial coordinate', 'temperature', 'number of bounce periods',
            'particles per sampling cell', 'half toroidal sampling width']

    job_Q = Queue()
    done_Q = Queue()

    workers = {}
    for ii in range(cores):
        key = str(ii)

        workers[key] = Process(target=run_cell, args=(job_Q, done_Q,))
        workers[key].daemon = True
        workers[key].start()

    pitch_keys = []
    for pitch_idx, pitch_base in enumerate(pitch_set[0:-1]):
        for pol_idx, pol_base in enumerate(pol_set[0:-1]):
            jdx = pitch_idx * pol_num + pol_idx + 1
            job_track = '{0:0.0f}-{1:0.0f}'.format(jdx, total_num)

            pitch_key = '{0:0.0f}-{1:0.0f}'.format(pitch_idx, pitch_idx+1)
            pitch_keys.append(pitch_key)

            pitch_dom = np.array([pitch_base, pitch_set[pitch_idx + 1]])
            pol_dom = np.array([pol_base, pol_set[pol_idx + 1]])

            job = {'end': False,
                   'job tracker': job_track,
                   'pitch key': pitch_key,
                   'pitch domain': pitch_dom,
                   'poloidal domain': pol_dom}

            for key in keys:
                job[key] = runInfo[key]

            job_Q.put(job)

    for ii in range(cores):
        job = {'end': True}
        job_Q.put(job)

    data = np.empty((npts*total_num, 7))
    data_idx = 0

    done_cnt = 0
    while done_cnt < cores:
        datum = done_Q.get()
        if datum['end']:
            done_cnt += 1
        else:
            data[data_idx] = datum['data']
            data_idx += 1

    return data


if __name__ == '__main__':
    temp_eV = 1e3
    m = 9.1094e-31
    bnc_num = 11
    npts = 100

    # Define Constants #
    k = 1.3807e-23
    T = temp_eV * 11604
    alf = np.sqrt(m / (2 * k * T))
    Vmod = np.sqrt(k * T / m)

    # Define Grid #
    tor_div = 4  # pi / (tor_div * Nfp) = half width around ideal minimum
    pol_num = 2  # pol_num * tor_div = number of poloidal cells
    pitch_num = 10  # number of pitch angle cells

    # Define Flux Space Grid #
    upts = 251
    psi_val = 0.9
    pol_dom = np.linspace(0, 2*np.pi, upts)
    tor_dom = np.linspace(0, 2*np.pi, upts*4)

    path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'NL_TEM_jobs_20210721')
    conID_file = os.path.join(path, 'NL_Select_conIDs.txt')
    with open(conID_file, 'r') as f:
        lines = f.readlines()
        conID_list = []
        for line in lines:
            line = line.strip()
            if line != '0-1-0':
                conID_list.append(line)

    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs')
    for conID in conID_list:
        conID_Arr = conID.split('-')

        mainID = 'main_coil_{}'.format(conID_Arr[0])
        setID = 'set_{}'.format(conID_Arr[1])
        jobID = 'job_{}'.format(conID_Arr[2])

        # Import WOUT File #
        geomDir = os.path.join(wout_path, mainID, setID, jobID)
        geomFile = 'wout_HSX_main_opt0.nc'
        wout = wr.readWout(geomDir, name=geomFile)

        # Run Simulation # 
        start_time = time.time()

        pol_pnts = np.linspace(-np.pi, np.pi, 250)
        tor_pnts_min = (1./wout.nfp) * pol_pnts
        tor_pnts_max1 = (1./wout.nfp) * (pol_pnts - np.pi)
        tor_pnts_max2 = (1./wout.nfp) * (pol_pnts + np.pi)
        psi_pnts = np.full(pol_pnts.shape[0], psi_val)

        points_min = np.stack((psi_pnts, pol_pnts, tor_pnts_min), axis=1)
        points_max1 = np.stack((psi_pnts, pol_pnts, tor_pnts_max1), axis=1)
        points_max2 = np.stack((psi_pnts, pol_pnts, tor_pnts_max2), axis=1)

        wout.transForm_listPoints(points_min, ['Bmod'])
        Bmin = np.mean(wout.invFourAmps['Bmod'])

        wout.transForm_listPoints(points_max1, ['Bmod'])
        Bmax1 = wout.invFourAmps['Bmod']
        wout.transForm_listPoints(points_max2, ['Bmod'])
        Bmax2 = wout.invFourAmps['Bmod']
        Bmax = np.mean(np.append(Bmax1, Bmax2))

        Brng = [Bmin, Bmax]

        pitch_min = np.arctan(np.sqrt(Bmin / (Bmax - Bmin)))
        pitch_dom = np.linspace(pitch_min, 0.5*np.pi, pitch_num+1)
        pol_dom = np.linspace(-np.pi, np.pi, pol_num*tor_div+1)
        tor_width = np.pi / (tor_div * wout.nfp)

        # Define Run Parameters #
        runInfo = {'number of field periods': wout.nfp,
                   'number of cores': cpu_count() - 1,
                   'parent directory of wout file': geomDir,
                   'wout file name': geomFile,
                   'range of mod B values': Brng,
                   'velocity': Vmod,
                   'pitch domain': pitch_dom,
                   'poloidal domain': pol_dom,
                   'radial coordinate': psi_val,
                   'temperature': k*T,
                   'number of bounce periods': bnc_num,
                   'particles per sampling cell': npts,
                   'half toroidal sampling width': tor_width}

        results = calc_resonance_parallel(runInfo)

        savePath = os.path.join(path, 'data_files', 'MC_data', 'mcData_{}_Te_1keV_bnc_11.h5'.format(conID))
        with hf.File(savePath, 'w') as hf_:
            hf_.create_dataset('data', data=results)

        end_time = time.time() - start_time
        print('\n{} part/sec\n'.format((npts * tor_div * pol_num * pitch_num) / end_time))
