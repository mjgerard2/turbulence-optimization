import numpy as np
import h5py as hf

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd


#path = os.path.join('/mnt','HSX_Database','GENE', 'eps_valley', 'NL_TEM_jobs_20210721', 'data_files', 'MC_data')
path = os.path.join('/home', 'michael', 'Desktop', '3D_chrisModel_kScan', 'scan_1', 'k0.75')
# conID = '779-1-9'
# dataPath = os.path.join(path, 'mcData_{}_Te_1keV_bnc_11.h5'.format(conID))
dataPath = os.path.join(path, 'trapElec_bnc_2_Te_1keV_Npts_5k.h5')
with hf.File(dataPath, 'r') as hf_:
    data = hf_['psiN 0.9'][()]

npts = data.shape[0]

M_vals = data[:,5]
M_bins = np.linspace(np.min(M_vals), np.max(M_vals), 25)

M_mids = np.empty(M_bins.shape[0]-1)
omegaD = np.empty((M_bins.shape[0]-1, 2))
for mdx, M in enumerate(M_bins[0:-1]):
    key = '{0:0.0f}-{1:0.0f}'.format(mdx, mdx+1)
    M_mids[mdx] = .5 * (M + M_bins[mdx+1])

    omD_set = data[(data[:, 5] >= M_bins[mdx]) & (data[:, 5] < M_bins[mdx+1]), 6]
    omD_ratio = omD_set[omD_set <= 0].shape[0] / omD_set.shape[0]
    omegaD[mdx] = [omD_ratio, 100./np.sqrt(omD_set.shape[0])]

M_err = .5 * (M_bins[1] - M_bins[0])

plot = pd.plot_define()
plt = plot.plt

plt.errorbar(M_mids, omegaD[:, 0]*1e2, xerr=M_err, yerr=omegaD[:, 1], c='k', fmt='o')
# plt.scatter(M_mids, omegaD[:, 0], edgecolor='k', facecolor='None', marker='s', s=150)

plt.xlabel(r'$M$')
# plt.ylabel(r'$\frac{|e|}{k_{\alpha} K} \langle \omega_D \rangle$')
plt.ylabel(r'% of stable particles')

#plt.xlim(np.min(M_vals), np.max(M_vals))
plt.xlim(0, 1)
#plt.ylim(np.min(data[:, 5]), np.max(data[:, 5]))
plt.ylim(0, 100)

plt.grid()
plt.tight_layout()
#plt.show()
#saveName = os.path.join(path, 'mcData_{}.png'.format(conID))
saveName = os.path.join(path, 'mcData_oldRoutine.png')
plt.savefig(saveName)
