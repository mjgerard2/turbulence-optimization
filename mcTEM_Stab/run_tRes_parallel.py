# -*- coding: utf-8 -*-
"""
Created on Thu Mar 18 10:46:07 2021

@author: micha
"""

import time
import random

import numpy as np
import h5py as hf

from multiprocessing import Pool
from multiprocessing import cpu_count

import trapped_resonance_calculation as tRes

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.wout_read as wr


def divide(num, div):
    rem = num % div
    base = int(np.floor(num / div))
    base_plus = int(base + 1)

    return [div-rem, base], [rem, base_plus]

def calc_resonance(runInfo, npts):
    cnts = 0
    data = np.full((npts, 8), np.nan)
    while cnts < npts:
        u_pnt = 2 * np.pi * np.random.random()
        v_pnt = 2 * np.pi * np.random.random()

        runInfo['wout'].transForm_1D(runInfo['s_val'], u_pnt, v_pnt, ['Bmod'])
        Bo = runInfo['wout'].invFourAmps['Bmod']

        pos_init = np.array([runInfo['s_val'], u_pnt, v_pnt])
        if Bo > runInfo['Brng'][1] or Bo < runInfo['Brng'][0]:
            print('PsiN = {0} : Bo = {1:0.6f} : Bmax = {2:0.6f}'.format(runInfo['s_val'], Bo, runInfo['Brng'][1]))
        else:
            pitch_thresh = np.arctan(np.sqrt(Bo / (runInfo['Brng'][1] - Bo)))

            pitch_pop = np.linspace(pitch_thresh, 0.5 * np.pi, 100000)
            pitch_PDF = np.sin(pitch_pop)
            pitch_PDF = pitch_PDF / np.sum(pitch_PDF)
            pitch = random.choices(pitch_pop, weights=pitch_PDF)[0]
            Vmod = random.choices(runInfo['Vmod_pop'], weights=runInfo['Vmod_PDF'])[0]

            t_res = tRes.trapped_resonance(runInfo['geomDir'], runInfo['geomFile'], pos_init, Vmod, pitch, bnc_num=runInfo['bnc_num'])
            try:
                t_res.RG_integrate(1e-8)
                Bmirr, alf_drift, psi_drift = t_res.calc_drift_freq(temp=runInfo['temp'])
                M = np.sqrt((Bmirr - runInfo['Brng'][0]) / (runInfo['Brng'][1] - runInfo['Brng'][0]))

                init_con = np.array([runInfo['s_val'], u_pnt, v_pnt, Vmod, pitch])
                om_drift = np.array([M, alf_drift, psi_drift])

                datum = np.r_[init_con, om_drift]
                data[cnts] = datum

                cnts+=1

            except ValueError:
                continue

    return data


if __name__ == '__main__':
    spec = sys.argv[1]
    bnc_num = int(sys.argv[2])
    temp_eV = float(sys.argv[3])
    npts = int(sys.argv[4])

    # Define Save Name #
    if spec == 'electron':
        saveName = 'trapElec_bnc_{0:0.0f}_Te_'.format(bnc_num)
        m = 9.1094e-31
    elif spec == 'proton':
        saveName = 'trapElec_bnc_{0:0.0f}_Te_'.format(bnc_num)
        m = 1.6726e-27

    temp_num = temp_eV * 1e-3
    if temp_num >= 1:
        saveName = saveName + '{0:0.0f}'.format(int(temp_num))
        temp_num_sub = temp_num - int(temp_num)
        if temp_num_sub != 0:
            saveName = saveName + 'p{0:0.0f}keV_Npts_'.format(int(temp_num_sub * 1e3))
        else:
            saveName = saveName + 'keV_Npts_'
    else:
        saveName = saveName + '0p{}keV_Npts_'.format(int(temp_num * 1e3))

    npts_num = npts * 1e-3
    if npts_num >= 1:
        saveName = saveName + '{0:0.0f}'.format(int(npts_num))
        npts_num_sub = npts_num - int(npts_num)
        if npts_num_sub != 0:
            saveName = saveName + 'p{0:0.0f}k.h5'.format(int(npts_num_sub * 1e3))
        else:
            saveName = saveName + 'k.h5'
    else:
        saveName = saveName + '0p{}k.h5'.format(int(npts_num * 1e3))

    # Define Constants #
    k = 1.3807e-23
    T = temp_eV * 11604
    alf = np.sqrt(m / (2 * k * T))

    # Calculate Velocity PDF #
    Vmod_pop = np.linspace(0, 1e8, int(1e6)+1)
    Vmod_PDF = Vmod_pop * Vmod_pop * np.exp(- (Vmod_pop * alf)**2)
    Vmod_PDF = Vmod_PDF / np.sum(Vmod_PDF)

    # Define Flux Space Grid #
    upts = 251
    psi_dom = np.array([0.9])  # np.array([0.0625, 0.25, 0.5625])
    pol_dom = np.linspace(0, 2*np.pi, upts)
    tor_dom = np.linspace(0, 2*np.pi, upts*4)

    # Set Working Directory #
    basePath = os.path.join('/home', 'michael', 'Desktop', '3D_chrisModel_kScan', 'scan_1')
    dircs = [f.name for f in os.scandir(basePath) if os.path.isdir(f)]

    for dx, dirc in enumerate(dircs):
        print('{0}: {1:0.0f}-{2:0.0f}'.format(dirc, dx+1, len(dircs)))
        savePath = os.path.join(basePath, dirc, saveName)

        geomDir = os.path.join(basePath, dirc)
        geomFile = 'wout_3D_model_{}.nc'.format(dirc)
        wout = wr.readWout(geomDir, geomFile)

        # Read Con ID List #
        """
        conID_list = os.path.join(WORKDIR,'mcTEM_Stab','conID_list.txt')

        conIDs = []
        with open(conID_list, 'r') as f:
            lines = f.readlines()
            for line in lines:
                conIDs.append(line.strip())

        # Loop Over Configurations #
        for idx, conID in enumerate(conIDs):
            print('Working : ' + conID + ' : {0} of {1}'.format(idx+1, len(conIDs)))
            mainID = 'main_coil_{}'.format(conID.split('-')[0])
            setID = 'set_{}'.format(conID.split('-')[1])
            jobID = 'job_{}'.format(conID.split('-')[2])

            # Import WOUT File #
            geomDir = os.path.join('/mnt','HSX_Database','HSX_Configs',mainID,setID,jobID)
            geomFile = 'wout_HSX_main_opt0.nc'
            wout = wr.readWout(geomDir, name=geomFile)
        """
        # Loop Over Psi Values #
        start_time = time.time()
        for psi_val in psi_dom:
            print('    psiN = {}'.format(psi_val))
            wout.transForm_2D_sSec(psi_val, pol_dom, tor_dom, ['Bmod'])
            Bmax = np.max(wout.invFourAmps['Bmod'])
            Bmin = np.min(wout.invFourAmps['Bmod'])
            Brng = [Bmin, Bmax]

            # Define Run Parameters #
            runInfo = {'wout': wout,
                       'geomDir': geomDir,
                       'geomFile': geomFile,
                       'Vmod_PDF': Vmod_PDF,
                       'Vmod_pop': Vmod_pop,
                       'Brng': Brng,
                       's_val': psi_val,
                       'temp': k*T,
                       'bnc_num': bnc_num}

            # Divide Jobs Over CPUs #
            cpus = cpu_count() - 2
            tup1, tup2 = divide(npts, cpus)

            pool_pts = [tup1[1]] * tup1[0]
            pool_pts.extend([tup2[1]] * tup2[0])

            pool_args = []
            for pts in pool_pts:
                pool_args.append([runInfo.copy(), pts])

            # Start Processes #
            with Pool() as pool:
                result = pool.starmap(calc_resonance, pool_args)

            # Map Results to Output Variable #
            om = result[0]
            for res in result[1::]:
                count = om.shape[0] + res.shape[0]
                om = np.append(om, res).reshape(count, 8)

            # Save Output Data #
            psi_tag = 'psiN {0}'.format(psi_val)
            with hf.File(savePath, 'a') as hf_file:
                hf_key = '{0}'.format(psi_tag)
                hf_file.create_dataset(hf_key, data=om)

            # Save Output Data #
            """
            psi_tag = 'psiN {0}'.format(psi_val)
            with hf.File(savePath, 'a') as hf_file:
                hf_key = '{0}/{1}'.format(conID, psi_tag)
                hf_file.create_dataset(hf_key, data=om)
            """
        end_time = time.time() - start_time
        print('\n{} part/sec\n'.format((npts*psi_dom.shape[0]) / end_time))
