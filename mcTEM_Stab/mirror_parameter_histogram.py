import numpy as np
import h5py as hf

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import trapped_resonance_calculation as tRes

import vmecTools.wout_files.wout_read as wr
from vmecTools.wout_files.coord_convert import compute_metric

### Specify Configuraton ID ###
conID = '897-1-0'
psiN_key = 'psiN 0.5625'
psiN = float(psiN_key.split()[1])

### Import Data File ###
mc_data_path = os.path.join('/mnt','HSX_Database','GENE','eps_valley','data_files','trapElec_Te_1keV_Npts_10k.h5')
with hf.File(mc_data_path, 'r') as hf_:
    mc_data = hf_[conID]['psiN 0.5625'][()]

### Import Wout File ###
conID_split = conID.split('-')
mainID = 'main_coil_' + conID_split[0]
setID = 'set_' + conID_split[1]
jobID = 'job_' + conID_split[2]

wout_path = os.path.join('/mnt','HSX_Database','HSX_Configs', mainID, setID, jobID)
wout = wr.readWout(wout_path, diffAmps=True)

### Approximate Magnetic Field Structure ###
upts = 500
u_dom = np.linspace(0, 2*np.pi, upts, endpoint=False)
v_dom = np.linspace(0, 2*np.pi, 4*upts, endpoint=False)

ampKeys = ['R','Jacobian','dR_ds','dR_du','dR_dv','dZ_ds','dZ_du','dZ_dv','Bmod']
wout.transForm_2D_sSec(psiN, u_dom ,v_dom, ampKeys)
Gss, Gsu, Gsv, Guu, Guv, Gvv, G_ss, G_su, G_sv, G_uu, G_uv, G_vv = compute_metric(wout)

Bmod = wout.invFourAmps['Bmod']
Bmod_min = np.min(Bmod)
Bmod_max = np.max(Bmod)

Bmod_avg = np.trapz(np.trapz(Bmod * np.sqrt(Guu * Gvv - Guv**2), u_dom, axis=1), v_dom) / np.trapz(np.trapz(np.sqrt(Guu * Gvv - Guv**2), u_dom, axis=1), v_dom)
Bmod_var_inv = 1. / (0.5 * (Bmod_max - Bmod_min))

### Check Particles ###
for datum in mc_data[0:10]:
    tRun = tRes.trapped_resonance(wout_path, 'wout_HSX_main_opt0.nc', datum[0:3], datum[3], datum[4], spec='electron', bnc_num=1)
    tRun.RG_integrate()
    data_out = tRun.data_out

    wout.transForm_1D(data_out[-1,0], data_out[-1,1], data_out[-1,2], ['Bmod'])
    Bmod_mrr = wout.invFourAmps['Bmod']

    k = np.sqrt(0.5 - 0.5 * (Bmod_var_inv * (Bmod_avg - Bmod_mrr)))
    print('{0} : {1}'.format(k, datum[5]))
