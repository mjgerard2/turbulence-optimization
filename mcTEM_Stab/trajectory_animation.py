import os, sys
import h5py as hf
import numpy as np
import matplotlib.animation as ani

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import vmecTools.wout_files.wout_read as wr
import plot_define as pd



# import stable trjectory data #
stable_path = os.path.join('/home', 'michael', 'Desktop', 'trajData_stable.h5')
with hf.File(stable_path, 'r') as hf_:
    stable_data = hf_['trajectory'][()]
    s_init = hf_['initial conditions'][0]
stable_data = stable_data[np.abs(stable_data[:,1]) < np.pi]

# import unstable trjectory data #
unstable_path = os.path.join('/home', 'michael', 'Desktop', 'trajData_unstable.h5')
with hf.File(unstable_path, 'r') as hf_:
    unstable_data = hf_['trajectory'][()]
unstable_data = unstable_data[np.abs(unstable_data[:,1]) < np.pi]

# truncate data #
last_idx = min(stable_data.shape[0], unstable_data.shape[0])-1
time_stp = int(last_idx/35)
time_idx = np.linspace(0, last_idx, time_stp, dtype=int)
stable_data = stable_data[time_idx]
unstable_data = unstable_data[time_idx]

# import wout file #
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
wout = wr.readWout(wout_path)

ppts = 151
tpts = 4*ppts
pol_dom = np.linspace(-np.pi, np.pi, ppts)
tor_dom = np.linspace(-np.pi, np.pi, tpts)
amp_keys = ['Bmod']

wout.transForm_2D_sSec(s_init, pol_dom, tor_dom, amp_keys)
B_grid = wout.invFourAmps['Bmod']

# plottting parameters #
plot = pd.plot_define(fontSize=14, labelSize=16, lineWidth=2)
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True)

# plot data #
cmap = ax.pcolormesh(tor_dom/np.pi, pol_dom/np.pi, B_grid.T, cmap='jet')

plt_stab, = ax.plot([], [], c='w')
dot_stab = ax.scatter([], [], facecolor='tab:red', edgecolor='k', s=100, zorder=10)

plt_unstab, = ax.plot([], [], c='w')
dot_unstab = ax.scatter([], [], facecolor='tab:red', edgecolor='k', s=100, zorder=10)

# axis labels #
ax.set_xlabel(r'$\varphi/\pi$')
ax.set_ylabel(r'$\theta/\pi$')
cbar = fig.colorbar(cmap, ax=ax)
cbar.ax.set_ylabel(r'$B/T$')

# define frame function #
def frame(idx):
    stab_tor, stab_pol = stable_data[0:idx+1,2]/np.pi, stable_data[0:idx+1,1]/np.pi
    plt_stab.set_data(stab_tor, stab_pol)
    dot_stab.set_offsets([stab_tor[idx], stab_pol[idx]])

    unstab_tor, unstab_pol = unstable_data[0:idx+1,2]/np.pi, unstable_data[0:idx+1,1]/np.pi
    plt_unstab.set_data(unstab_tor, unstab_pol)
    dot_unstab.set_offsets([unstab_tor[idx], unstab_pol[idx]])

    return plt_stab, dot_stab, plt_unstab, dot_unstab 

# save/show #
# plt.show()
anim = ani.FuncAnimation(fig, frame, frames=time_stp, blit=True)
# save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'APS', '2023', 'Figures', 'trajectory.mp4')
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'APS', '2023', 'Figures', 'trajectory_frames', 'frame.png')
# anim.save(save_path, writer='ffmpeg', fps=30)
anim.save(save_path, writer='imagemagick', fps=30)
