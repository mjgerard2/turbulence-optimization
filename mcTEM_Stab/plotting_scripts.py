import numpy as np
import h5py as hf

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','Research','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd

### User Input ###
psi_key = 'psiN 0.5625'

x_select = 'kappa'
c_select = 'eps'

mc_select = 'ratio'

saveFig = True
saveName = os.path.join('/mnt','HSX_Database','GENE','eps_valley_control','figures','mcTEM_Stab','bounce_2_kappa_vs_Fkappa.png')

### Import Monte-Carlo TEM Stability Data ###
#hf_path = os.path.join('/mnt','HSX_Database','GENE','eps_valley_control','data_files','trapElec_bounce_11_Te_1keV_Npts_5k.h5')
hf_path = os.path.join('/mnt','HSX_Database','GENE','eps_valley_control','data_files','trapElec_Te_1keV_Npts_10k.h5')
with hf.File(hf_path, 'r') as hf_file:
    mc_dict = {}
    for key_1 in hf_file:
        mc_dict[key_1] = {}
        for key_2 in hf_file[key_1]:
            mc_dict[key_1][key_2] = hf_file[key_1+'/'+key_2][()]

### Import Metric Data ###
met_path = os.path.join('/mnt','HSX_Database','GENE','eps_valley_control','data_files','metric_data.h5')
with hf.File(met_path, 'r') as met_file:
    met_data = met_file['metric data'][()]

### Sort Data for Plotting ###
mc_data = np.empty(met_data.shape[0])
for mdx, met in enumerate(met_data):
    conID = '-'.join(['{0:0.0f}'.format(ID) for ID in met[0:3]])
    data_set = mc_dict[conID][psi_key]
   
    if mc_select == 'ratio':
        stable = np.where(data_set[:,5] <= 0)[0].shape[0]
        unstable = np.where(data_set[:,5] > 0)[0].shape[0]
    elif mc_select == 'drift':
        print('Write Data Analysis Here!')

    mc_data[mdx] = stable / unstable

### Plot Labels ###
if x_select == 'eps':
    xdx = 3
    xlab = r'$\mathcal{E}_{eff} / \mathcal{E}_{eff}^*$'
elif x_select == 'qhs':
    xdx = 4
    xlab = r'$\mathcal{Q} / \mathcal{Q}^*$'
elif x_select == 'F_kappa':
    xdx = 6
    xlab = r'$\mathcal{F}_{\kappa} / \mathcal{F}_{\kappa}^*$'
elif x_select == 'G_psi':
    xdx = 7
    xlab = r'$\mathcal{G}_{\psi} / \mathcal{G}_{\psi}^*$'
elif x_select == 'shear':
    xdx = 8
    xlab = r'$\bar{\iota}^{\prime} / \bar{\iota}^{\prime *}$'
elif x_select == 'kappa':
    xdx = 9
    xlab=r'$\bar{\kappa} / \bar{\kappa}^*$'

if c_select == 'eps':
    cdx = 3
    clab = r'$\mathcal{E}_{eff} / \mathcal{E}_{eff}^*$'
elif c_select == 'qhs':
    cdx = 4
    clab = r'$\mathcal{Q} / \mathcal{Q}^*$'
elif c_select == 'F_kappa':
    cdx = 6
    clab = r'$\mathcal{F}_{\kappa} / \mathcal{F}_{\kappa}^*$'
elif c_select == 'G_psi':
    cdx = 7
    clab = r'$\mathcal{G}_{\psi} / \mathcal{G}_{\psi}^*$'
elif c_select == 'shear':
    cdx = 8
    clab = r'$\bar{\iota}^{\prime} / \bar{\iota}^{\prime *}$'
elif c_select == 'kappa':
    cdx = 9
    clab=r'$\bar{\kappa} / \bar{\kappa}^*$'

### Plot Data ###
plot = pd.plot_define()
plt, fig, ax = plot.plt, plot.fig, plot.ax

smap = plt.scatter(met_data[:,xdx], mc_data, edgecolor='k', c=met_data[:,cdx], s=150, cmap='jet')

cbar = fig.colorbar(smap, ax=ax)
cbar.ax.set_ylabel(clab)

plt.ylim(0, 0.8)

plt.xlabel(xlab)
plt.ylabel(r'$N_- / N_+$')

plt.grid()
plt.tight_layout()
plt.show()

if saveFig:
    plt.savefig(saveName)
else:
    plt.show()
