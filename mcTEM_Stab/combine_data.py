import numpy as np
import h5py as hf

file1 = 'parTest_omegaD_QHS_1.h5'
file2 = 'parTest_omegaD_QHS_2.h5'

with hf.File(file1, 'r') as hf_:
    data1 = hf_['data'][()]

with hf.File(file2, 'r') as hf_:
    data2 = hf_['data'][()]

npts = data1.shape[0] + data2.shape[0]
data_full = np.empty((npts, data1.shape[1]))

idxBeg = 0
idxEnd = data1.shape[0]
data_full[idxBeg:idxEnd] = data1

idxBeg = data1.shape[0]
idxEnd = data1.shape[0] + data2.shape[0]
data_full[idxBeg:idxEnd] = data2

with hf.File('parTest_omegaD_QHS.h5', 'w') as hf_:
    hf_.create_dataset('data', data=data_full)
