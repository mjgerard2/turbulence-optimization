import matplotlib as mpl
import matplotlib.pyplot as plt

import numpy as np
import h5py as hf

from scipy.interpolate import interp1d

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.curveB_tools as cBT


num_of_frames = 150
base_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'figures', 'flat_torus', 'QHS', 'precession_animation')

# Read in Stable Trajectory Data #
stable_path = os.path.join(base_path, 'stable_orbit.h5')
with hf.File(stable_path, 'r') as hf_:
    traj_data = hf_['trajectory'][()]
    time_dom = hf_['time domain'][()]

time_stable = time_dom[(traj_data[:, 1] > -np.pi) & (traj_data[:, 1] < np.pi)]
pol_stable = traj_data[(traj_data[:, 1] > -np.pi) & (traj_data[:, 1] < np.pi), 1]
tor_stable = traj_data[(traj_data[:, 1] > -np.pi) & (traj_data[:, 1] < np.pi), 2]

pol_stable_model = interp1d(time_stable, pol_stable, kind='quadratic')
tor_stable_model = interp1d(time_stable, tor_stable, kind='quadratic')

# Read in Unstable Trajectory Data #
unstable_path = os.path.join(base_path, 'unstable_orbit.h5')
with hf.File(unstable_path, 'r') as hf_:
    traj_data = hf_['trajectory'][()]
    time_dom = hf_['time domain'][()]

time_unstable = time_dom[(traj_data[:, 1] > -np.pi) & (traj_data[:, 1] < np.pi)]
pol_unstable = traj_data[(traj_data[:, 1] > -np.pi) & (traj_data[:, 1] < np.pi), 1]
tor_unstable = traj_data[(traj_data[:, 1] > -np.pi) & (traj_data[:, 1] < np.pi), 2]

pol_unstable_model = interp1d(time_unstable, pol_unstable, kind='quadratic')
tor_unstable_model = interp1d(time_unstable, tor_unstable, kind='quadratic')

# Read in Wout File and Calculate Normal Curvature #
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0')
wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)

upts = 251
vpts = 4 * upts

u_norm = np.linspace(-1, 1, upts)
v_norm = np.linspace(-1, 1, vpts)

s_val = 0.25
u_dom = np.pi * u_norm
v_dom = np.pi * v_norm

keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs_covar', 'Bu_covar', 'Bv_covar', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']
wout.transForm_2D_sSec(s_val, u_dom, v_dom, keys)
curB = cBT.BcurveTools(wout)
curB.calc_curvature()

modB = wout.invFourAmps['Bmod']
normK = curB.k_s
Kmax = np.max(np.abs(normK))

# Full Trajectory Plot #
if False:
    # Plot Flat Torus #
    plt.close('all')

    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 16}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 20
    mpl.rcParams['lines.linewidth'] = 2

    fig, axs = plt.subplots(2, 1, figsize=(6, 8), tight_layout=True, sharex=True)

    # Generate Flat Tori #
    Bmap = axs[0].pcolormesh(v_norm, u_norm, modB.T, vmin=np.min(modB), vmax=np.max(modB), cmap=plt.get_cmap('jet'))
    Kmap = axs[1].pcolormesh(v_norm, u_norm, normK.T, vmin=-Kmax, vmax=Kmax, cmap=plt.get_cmap('seismic'))

    # Plot Trajectories #
    axs[0].plot(tor_stable/np.pi, pol_stable/np.pi, c='k')
    axs[1].plot(tor_stable/np.pi, pol_stable/np.pi, c='k')

    axs[0].plot(tor_unstable/np.pi, pol_unstable/np.pi, c='k')
    axs[1].plot(tor_unstable/np.pi, pol_unstable/np.pi, c='k')

    # Construct Labels #
    cbar = fig.colorbar(Bmap, ax=axs[0])
    cbar.ax.set_ylabel(r'$B$ (T)')

    cbar = fig.colorbar(Kmap, ax=axs[1])
    cbar.ax.set_ylabel(r'$\kappa_n$')

    axs[0].set_title(r'$\psi_N = $'+'{0:0.2f}'.format(s_val)+r' ($r/a = $'+'{0:0.2f})'.format(np.sqrt(s_val)))
    axs[0].set_ylabel(r'$\theta / \pi$')
    axs[1].set_xlabel(r'$\zeta / \pi$')
    axs[1].set_ylabel(r'$\theta / \pi$')

    plt.show()

# Animation Plots #
if True:
    # Plot Flat Torus #
    font = {'family': 'sans-serif',
            'weight': 'normal',
            'size': 16}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = 20
    mpl.rcParams['lines.linewidth'] = 2

    t_stable = np.linspace(time_stable[0], time_stable[-1], num_of_frames)
    t_unstable = np.linspace(time_unstable[0], time_unstable[-1], num_of_frames)
    for i in range(1, num_of_frames+1):
        print('Frame {0:0.0f}-{1:0.0f}'.format(i, num_of_frames))
        fig, axs = plt.subplots(2, 1, figsize=(6, 8), tight_layout=True, sharex=True)

        # Generate Flat Tori #
        Bmap = axs[0].pcolormesh(v_norm, u_norm, modB.T, vmin=np.min(modB), vmax=np.max(modB), cmap=plt.get_cmap('jet'))
        Kmap = axs[1].pcolormesh(v_norm, u_norm, normK.T, vmin=-Kmax, vmax=Kmax, cmap=plt.get_cmap('seismic'))

        # Plot Stable Trajectories #
        v_stab = tor_stable_model(t_stable[0:int(i)]) / np.pi
        u_stab = pol_stable_model(t_stable[0:int(i)]) / np.pi

        axs[0].plot(v_stab, u_stab, c='k')
        axs[1].plot(v_stab, u_stab, c='k')

        axs[0].scatter([v_stab[-1]], [u_stab[-1]], s=150, marker='o', c='tab:red', zorder=10)
        axs[1].scatter([v_stab[-1]], [u_stab[-1]], s=150, marker='o', c='tab:red', zorder=10)

        # Plot Unstable Trajectories #
        v_unstab = tor_unstable_model(t_unstable[0:int(i)]) / np.pi
        u_unstab = pol_unstable_model(t_unstable[0:int(i)]) / np.pi

        axs[0].plot(v_unstab, u_unstab, c='k')
        axs[1].plot(v_unstab, u_unstab, c='k')

        axs[0].scatter([v_unstab[-1]], [u_unstab[-1]], s=150, marker='o', c='tab:red', zorder=10)
        axs[1].scatter([v_unstab[-1]], [u_unstab[-1]], s=150, marker='o', c='tab:red', zorder=10)

        # Construct Labels #
        cbar = fig.colorbar(Bmap, ax=axs[0])
        cbar.ax.set_ylabel(r'$B$ (T)')

        cbar = fig.colorbar(Kmap, ax=axs[1])
        cbar.ax.set_ylabel(r'$\kappa_n$')

        axs[0].set_title(r'$\psi_N = $'+'{0:0.2f}'.format(s_val)+r' ($r/a = $'+'{0:0.2f})'.format(np.sqrt(s_val)))
        axs[0].set_ylabel(r'$\theta / \pi$')
        axs[1].set_xlabel(r'$\zeta / \pi$')
        axs[1].set_ylabel(r'$\theta / \pi$')

        # Set Limits #
        axs[0].set_xlim(-1, 1)
        axs[0].set_ylim(-1, 1)

        axs[1].set_xlim(-1, 1)
        axs[1].set_ylim(-1, 1)

        savePath = os.path.join(base_path, 'frame_{0:0.0f}.png'.format(i))
        plt.savefig(savePath)
        plt.close('all')
