import os
import numpy as np
import h5py as hf

import w7x_cp_module as w7x

mpts = 21
apts = 11

main_crnts = 13.86e3 * (1 + np.linspace(-.1, .1, mpts))
aux_crnts = 13.86e3 * np.linspace(-.25, .25, apts)

cpts = mpts**5 * apts**2
ipts = mpts**5

hf_path = os.path.join('/mnt','HSX_Database','w7x_fit_data.h5')

mag_func = {'b00sq' : w7x.b00sq,
            'b01sq' : w7x.b01sq,
            'b02sq' : w7x.b02sq,
            'b03sq' : w7x.b03sq,
            'b04sq' : w7x.b04sq,
            'b05sq' : w7x.b05sq,
            'b0sq' : w7x.b0sq,
            'bav1sq' : w7x.bav1sq,
            'bav3sq' : w7x.bav3sq,
            'Btech2' : w7x.Btech2,
            'tcbsq' : w7x.tcbsq,
            'hcbsq' : w7x.hcbsq,
            'tbhsq' : w7x.tbhsq,
            'mrsq' : w7x.mrsq,
            'mrasymsq' : w7x.mrasymsq}

iota_func = {'io10' : w7x.io10,
             'io20' : w7x.io20,
             'io30' : w7x.io30,
             'io40' : w7x.io40,
             'ioax' : w7x.ioax}

geom_func = {'r00' : w7x.r00,
             'r18' : w7x.r18,
             'r36' : w7x.r36}

eps00_funcs = {'epseffc0m' : w7x.epseffc0m,
               'epseffc0p' : w7x.epseffc0p}

eps03_funcs = {'epseff1m03' : w7x.epseff1m03,
               'epseff1p03' : w7x.epseff1p03}

eps13_funcs = {'epseff1m13' : w7x.epseff1m13,
               'epseff1p13' : w7x.epseff1p13}

eps24_funcs = {'epseff1m24' : w7x.epseff1m24,
               'epseff1p24' : w7x.epseff1p24}

eps35_funcs = {'epseff1m35' : w7x.epseff1m35,
               'epseff1p35' : w7x.epseff1p35}

coil_keys = ['coil 1','coil 2','coil 3','coil 4','coil 5','coil A','coil B']
eps_keys = ['eps00','eps03','eps13','eps24','eps35']

mag_keys = []
for key in mag_func:
    mag_keys.append(key)

iota_keys = []
for key in iota_func:
    iota_keys.append(key)

geom_keys = []
for key in geom_func:
    geom_keys.append(key)

for cA, xA in enumerate(aux_crnts):
    for cB, xB in enumerate(aux_crnts):
        adx = cB + cA*apts
        print('{0:0.0f}-{1:0.0f} : {2:0.0f}-{3:0.0f}'.format(cA+1, apts, cB+1, apts))

        coil_data = {}
        for key in coil_keys:
            coil_data[key] = np.empty(ipts)

        eps_data = {}
        for key in eps_keys:
            eps_data[key] = np.empty(ipts)

        mag_data = {}
        for key in mag_keys:
            mag_data[key] = np.empty(ipts)

        iota_data = {}
        for key in iota_keys:
            iota_data[key] = np.empty(ipts)

        geom_data = {}
        for key in geom_keys:
            geom_data[key] = np.empty(ipts)

        for c1, x1 in enumerate(main_crnts):
            for c2, x2 in enumerate(main_crnts):
                for c3, x3 in enumerate(main_crnts):
                    for c4, x4 in enumerate(main_crnts):
                        for c5, x5 in enumerate(main_crnts):
                            mdx = c5 + c4*mpts + c3*mpts**2 + c2*mpts**3 + c1*mpts**4
                            
                            for key in coil_data:
                                coil_data['coil 1'][mdx] = x1
                                coil_data['coil 2'][mdx] = x2
                                coil_data['coil 3'][mdx] = x3
                                coil_data['coil 4'][mdx] = x4
                                coil_data['coil 5'][mdx] = x5
                                coil_data['coil A'][mdx] = xA
                                coil_data['coil B'][mdx] = xB

                            for key in mag_func:
                                mag_datum = mag_func[key](x1,x2,x3,x4,x5,xA,xB)
                                mag_data[key][mdx] = mag_datum
                            
                            for key in iota_func:
                                iota_datum = iota_func[key](x1,x2,x3,x4,x5,xA,xB)
                                iota_data[key][mdx] = iota_datum
                            
                            for key in geom_func:
                                geom_datum = geom_func[key](x1,x2,x3,x4,x5,xA,xB)
                                geom_data[key][mdx] = geom_datum
                            
                            if mag_data['mrsq'][mdx] <= 0:
                                eps03_datum = eps03_funcs['epseff1m03'](x1,x2,x3,x4,x5,xA,xB)
                                eps13_datum = eps13_funcs['epseff1m13'](x1,x2,x3,x4,x5,xA,xB)
                                eps24_datum = eps24_funcs['epseff1m24'](x1,x2,x3,x4,x5,xA,xB)
                                eps35_datum = eps35_funcs['epseff1m35'](x1,x2,x3,x4,x5,xA,xB)

                                eps_data['eps03'][mdx] = eps03_datum
                                eps_data['eps13'][mdx] = eps13_datum
                                eps_data['eps24'][mdx] = eps24_datum
                                eps_data['eps35'][mdx] = eps35_datum
                            
                            elif mag_data['mrsq'][mdx] >= 0 and np.abs(mag_data['mrasymsq'][mdx]) <= mag_data['mrsq'][mdx]:
                                eps03_datum = eps03_funcs['epseff1p03'](x1,x2,x3,x4,x5,xA,xB)
                                eps13_datum = eps13_funcs['epseff1p13'](x1,x2,x3,x4,x5,xA,xB)
                                eps24_datum = eps24_funcs['epseff1p24'](x1,x2,x3,x4,x5,xA,xB)
                                eps35_datum = eps35_funcs['epseff1p35'](x1,x2,x3,x4,x5,xA,xB)

                                eps_data['eps03'][mdx] = eps03_datum
                                eps_data['eps13'][mdx] = eps13_datum
                                eps_data['eps24'][mdx] = eps24_datum
                                eps_data['eps35'][mdx] = eps35_datum

                            if (mag_data['b01sq'][mdx] + mag_data['b03sq'][mdx]) < 0:
                                eps00_datum = eps00_funcs['epseffc0m'](x1,x2,x3,x4,x5,xA,xB)
                                eps_data['eps00'][mdx] = eps00_datum

                            elif (mag_data['b01sq'][mdx] + mag_data['b03sq'][mdx]) >= 0:
                                eps00_datum = eps00_funcs['epseffc0p'](x1,x2,x3,x4,x5,xA,xB)
                                eps_data['eps00'][mdx] = eps00_datum

        with hf.File(hf_path, 'a') as hf_file:
            for key in coil_keys:
                if adx >= 1:
                    data = hf_file[key]
                    data.resize(data.shape[0]+1, axis=0)
                    data[-1:] = coil_data[key]
                else:
                    hf_file.create_dataset(key, data=coil_data[key].reshape(1,mpts**5), maxshape=(None, mpts**5), chunks=(1,mpts**5))

            for key in mag_keys:
                if adx >= 1:
                    data = hf_file[key]
                    data.resize(data.shape[0]+1, axis=0)
                    data[-1:] = mag_data[key]
                else:
                    hf_file.create_dataset(key, data=mag_data[key].reshape(1,mpts**5), maxshape=(None, mpts**5), chunks=(1,mpts**5))

            for key in geom_keys:
                if adx >= 1:
                    data = hf_file[key]
                    data.resize(data.shape[0]+1, axis=0)
                    data[-1:] = geom_data[key]
                else:
                    hf_file.create_dataset(key, data=geom_data[key].reshape(1,mpts**5), maxshape=(None, mpts**5), chunks=(1,mpts**5))

            for key in iota_keys:
                if adx >= 1:
                    data = hf_file[key]
                    data.resize(data.shape[0]+1, axis=0)
                    data[-1:] = iota_data[key]
                else:
                    hf_file.create_dataset(key, data=iota_data[key].reshape(1,mpts**5), maxshape=(None, mpts**5), chunks=(1,mpts**5))

            for key in eps_keys:
                if adx >= 1:
                    data = hf_file[key]
                    data.resize(data.shape[0]+1, axis=0)
                    data[-1:] = eps_data[key]
                else:
                    hf_file.create_dataset(key, data=eps_data[key].reshape(1,mpts**5), maxshape=(None, mpts**5), chunks=(1,mpts**5))
