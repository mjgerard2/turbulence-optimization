import h5py as hf
import numpy as np

from sympy.utilities.iterables import multiset_permutations
from sklearn.linear_model import LinearRegression

import w7x_cp_module as w7x

import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','turbulence-optimization','pythonTools')
sys.path.append(WORKDIR)

import plot_define as pd
import fft_class as FFT

delta_t = 1. / 56
c1 = .1

kn_choices = 2 * np.pi * ( 1 + np.arange(7) )
phase_set = np.zeros(7)

cnt = 0
clim = 500
B_set = np.empty(clim+1)
dB_set = np.empty(clim)

B_set[0] = w7x.epseff1m13(13.86e3, 13.86e3, 13.86e3, 13.86e3, 13.86e3, 0, 0)

while cnt < clim:
    dBdt = np.empty(5040)
    kn_mat = np.empty((5040, 7))
    for kn_idx, kn_select in enumerate( multiset_permutations(kn_choices) ):
        kn_select = np.array(kn_select)
        kn_mat[kn_idx] = 13.86e3 * c1 * kn_select * np.cos(phase_set)

        crnts_for = 13.86e3 * ( 1 + .1 * np.sin(kn_select[0:5] * delta_t + phase_set[0:5]) )
        crnts_bak = 13.86e3 * ( 1 - .1 * np.sin(kn_select[0:5] * delta_t + phase_set[0:5]) )

        aux_for = 13.86e3 * .1 * np.sin(kn_select[5::] * delta_t + phase_set[5::])
        aux_bak = - 13.86e3 * .1 * np.sin(kn_select[5::] * delta_t + phase_set[5::])

        B_for = w7x.epseff1m13(crnts_for[0], crnts_for[1], crnts_for[2], crnts_for[3], crnts_for[4], aux_for[0], aux_for[1])
        B_bak = w7x.epseff1m13(crnts_bak[0], crnts_bak[1], crnts_bak[2], crnts_bak[3], crnts_bak[4], aux_for[0], aux_for[1])

        dBdt[kn_idx] = (B_for - B_bak) / (2 * delta_t)

    print('{0:0.3f} (+/-) {1:0.3f}'.format(np.mean(dBdt), np.std(dBdt)))

    reg = LinearRegression(fit_intercept=False).fit(kn_mat, dBdt)
    dBdC = reg.coef_ / np.linalg.norm(reg.coef_)

    arc_shft = .01 * dBdC + np.sin(phase_set)
    for a, arc in enumerate(arc_shft):
        if np.abs(arc) > 1:
            arc_shft[a] = 1
    phase_set = np.arcsin(arc_shft)

    cnt+=1

    main = 13.86e3 * ( 1 + .1 * np.sin(phase_set[0:5]) )
    aux = 13.86e2 * np.sin(phase_set[5::])
    B_set[cnt] = w7x.epseff1m13(main[0], main[1], main[2], main[3], main[4], aux[0], aux[1])
    dB_set[cnt-1] = np.mean(dBdt)

plot = pd.plot_define()
plt, fig, ax1 = plot.plt, plot.fig, plot.ax
ax2 = ax1.twinx()

ax1.scatter(np.arange(clim+1), B_set, c='k', s=250)
ax2.scatter(np.arange(clim), dB_set, c='tab:red', s=250)

ax1.set_xlabel('Iteration')
ax1.set_ylabel('B')
ax2.set_ylabel('dB')

crnts = .1 * np.sin(phase_set)
title = '('+', '.join(['{0:0.2f}'.format(c) for c in crnts])+')'
plt.title(title)

plt.show()

"""
crnt_for = 13.86e3 * ( 1 + .1 * np.sin(10 * np.pi * delta_t) )
crnt_bak = 13.86e3 * ( 1 - .1 * np.sin(10 * np.pi * delta_t) )
crnt_dif = crnt_for - crnt_bak

dBdC_dif = np.empty(7)
for i in range(7):
    crnts_for = np.r_[np.full(5, 13.86e3), np.zeros(2)]
    crnts_bak = np.r_[np.full(5, 13.86e3), np.zeros(2)]

    crnts_for[i] = crnt_for
    crnts_bak[i] = crnt_bak

    if i >= 5:
        crnts_for[i] = crnt_for - 13.86e3
        crnts_bak[i] = crnt_bak - 13.86e3

    B_for = w7x.b00sq(crnts_for[0], crnts_for[1], crnts_for[2], crnts_for[3], crnts_for[4], crnts_for[0], crnts_for[1])
    B_bak = w7x.b00sq(crnts_bak[0], crnts_bak[1], crnts_bak[2], crnts_bak[3], crnts_bak[4], crnts_bak[0], crnts_bak[1])

    dBdC_dif[i] = (B_for - B_bak) / crnt_dif

dBdC_dif = dBdC_dif / np.linalg.norm(dBdC_dif)

print(dBdC_fit)
print(dBdC_dif)
"""
"""
dBdt_chk = np.empty(dBdt.shape)
for kn_idx, kn_set in enumerate(kn_mat):
    dBdt_chk[kn_idx] = np.dot(kn_set, reg.coef_)

plot = pd.plot_define()
plt = plot.plt

plt.scatter(dBdt_chk, dBdt)
plt.plot([0.5, 1.5], [0.5, 1.5], ls='--', c='k', linewidth=2)

plt.xlabel('dBdt Fit')
plt.ylabel('dBdt True')

plt.show()
"""
