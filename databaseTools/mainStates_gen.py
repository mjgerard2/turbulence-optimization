# -*- coding: utf-8 -*-
"""
Created on Sat Mar 28 18:01:13 2020

@author: micha
"""


import numpy as np

npts = 11
coil_base = np.linspace(.9, 1., npts)
idx_base = np.arange(6)

npts = 11
Npts = npts**6

coil_base = np.linspace(.9, 1., npts)
#states = np.array([0.,0.,0.,0.,0.,0.]).reshape(1,6)
states = []

#states = np.empty((Npts, 6))
'''
for i, c1 in enumerate(coil_base):
    for j, c2 in enumerate(coil_base):
        for k, c3 in enumerate(coil_base):
            for l, c4 in enumerate(coil_base):
                for m, c5 in enumerate(coil_base):
                    for n, c6 in enumerate(coil_base):
                        idx = i * npts**5 + j * npts**4 + k * npts**3 + l * npts**2 + m * npts + n
                        states[idx] = np.array([c1, c2, c3, c4, c5, c6])
'''
for c in coil_base[0:-1]:
    for d in idx_base:
        state = np.array([1., 1., 1., 1., 1., 1.])
        state[d] = c

        states.append(state)

for c1 in coil_base[0:-1]:
    for c2 in coil_base[0:-1]:
        for x, d1 in enumerate(idx_base[0:5]):
            for d2 in idx_base[(1+x)::]:
                state = np.array([1., 1., 1., 1., 1., 1.])
                state[d1] = c1
                state[d2] = c2

                states.append(state)

output = open('mainStates.txt', 'w')
for state in states:
    output.write(' '.join(['%.4f' % s for s in state]) + '\n')
output.close()
