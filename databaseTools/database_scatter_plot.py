import os
import h5py as hf
import numpy as np

from matplotlib.colors import LogNorm

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd
import databaseTools.database_class as db


x_axis = 'Bk'
y_axis = 'Cn'
c_axis = 'qhs'

cmap = 'viridis'

dbase = db.database_reader()

# read in 100 selected data #
met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
with hf.File(met_path, 'r') as hf_:
    met_data = hf_['metric data'][()]

# plotting parameters #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(1, 2, sharex=False, sharey=False, tight_layout=True, figsize=(15, 6))

ax1 = axs[0]
ax2 = axs[1]

# plot data #
met_neg = dbase.met_data[dbase.met_data[:, dbase.met_idx['Bk']] < 0]
met_pos = dbase.met_data[dbase.met_data[:, dbase.met_idx['Bk']] > 0]
met_sel_neg = met_data[met_data[:, dbase.met_idx['Bk']] < 0]

met_neg = np.array(sorted(met_neg, key=lambda x: x[dbase.met_idx[c_axis]], reverse=True))
met_pos = np.array(sorted(met_pos, key=lambda x: x[dbase.met_idx[c_axis]], reverse=True))
met_sel_neg = np.array(sorted(met_sel_neg, key=lambda x: x[dbase.met_idx[c_axis]], reverse=True))

v1min, v1max = np.nanmin(met_neg[:, dbase.met_idx[c_axis]]), np.nanmax(met_neg[:, dbase.met_idx[c_axis]])
v2min, v2max = np.nanmin(met_pos[:, dbase.met_idx[c_axis]]), np.nanmax(met_pos[:, dbase.met_idx[c_axis]])
smap1 = ax1.scatter(met_neg[:, dbase.met_idx[x_axis]], met_neg[:, dbase.met_idx[y_axis]], c=met_neg[:, dbase.met_idx[c_axis]], s=1, cmap=cmap, norm=LogNorm(vmin=v1min, vmax=v1max))
smap2 = ax2.scatter(met_pos[:, dbase.met_idx[x_axis]], met_pos[:, dbase.met_idx[y_axis]], c=met_pos[:, dbase.met_idx[c_axis]], s=1, cmap=cmap, norm=LogNorm(vmin=v2min, vmax=v2max))
ax1.scatter(met_sel_neg[:, dbase.met_idx[x_axis]], met_sel_neg[:, dbase.met_idx[y_axis]], c=met_sel_neg[:, dbase.met_idx[c_axis]], edgecolor='w',
            marker='s', s=100, cmap=cmap, norm=LogNorm(vmin=v1min, vmax=v1max))

# axis labels #
cbar1 = fig.colorbar(smap1, ax=ax1)
cbar1.ax.set_ylabel(dbase.met_lab[c_axis])
cbar2 = fig.colorbar(smap2, ax=ax2)
cbar2.ax.set_ylabel(dbase.met_lab[c_axis])

ax1.set_ylabel(dbase.met_lab[y_axis])
ax2.set_ylabel(dbase.met_lab[y_axis])
ax1.set_xlabel(dbase.met_lab[x_axis])
ax2.set_xlabel(dbase.met_lab[x_axis])

# Save/Show #
# plt.show()
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'TTF_2023', 'figures', 'database-scatter-plot.png')
plt.savefig(save_path)
