# -*- coding: utf-8 -*-
"""
Created on Tue May  5 09:49:43 2020

@author: micha
"""


import os
import sys
import numpy as np
import h5py as hf

import functions as fun


exp = 'HSX'
ID = 1
setID = 'set_{}'.format(ID)

#basePath = 'D:\\'
#dirConfigs = os.path.join(basePath, '{}_Configs'.format(exp))
dirConfigs = os.path.join('C:\\', 'Users', 'micha', 'Documents', 'Research', 'Turbulence_Optimization', 'dataVMEC', 'HSX', 'coil_configs')

# Import Base Input File
file_name = os.path.join(dirConfigs, 'input.{}_main'.format(exp))

f = open(file_name, 'r')
lines = f.readlines()
f.close()

# Import Coil States
mainStates = 'mainStates.txt'
main_states = fun.readStates(mainStates)

do_list='do_list_qhs{}'.format(ID)
dirc_names = []

job_crnts = np.empty((len(main_states), 7))
# Construct Main Coil Configuration Files
for c, main in enumerate(main_states):
    
    # Construct Job MetaData    
    mult = 14
    base_crnt = -10722.0
    main_crnts = base_crnt * main
    crnts = np.r_[main_crnts, np.zeros(6)]
    
    job_crnts[c] = np.r_[c, main]
    
    # Construct Job Input Files
    crnt_str = ' '.join(['{}'.format(i) for i in crnts])
        
    dirc_name = 'job_{}'.format(c)
    dirc_names.append(dirc_name)
    
    new_dirc = os.path.join(do_list, dirc_name)
    os.makedirs(new_dirc)
            
    new_lines = lines
    new_lines[22] = '  EXTCUR = '+crnt_str+'\n'
    
    file_name_new = os.path.join(new_dirc, 'input.HSX_main')
    
    f = open(file_name_new, 'w')
    for l in new_lines:
        f.write(l)
    f.close()
'''
# Construct conData.h5
h5_pathName = dirConfigs = os.path.join('C:\\', 'Users', 'micha', 'Documents', 'Research', 'Turbulence_Optimization', 'dataVMEC', 'HSX', 'QHS_Configs', 'conData.h5')
h5_file = hf.File(h5_pathName, 'a')
h5_file.create_dataset(setID+'/config', data=job_crnts)
h5_file.close()
'''                      
# Construct Job Do List      
jobs = len(dirc_names)
runs = np.ceil(jobs/9000)
if runs > 1:
    sets = np.linspace(0, jobs, runs+1, dtype=int)
    
    for s_ind, s in enumerate(sets[1::]):
        name = 'job_list_{}.txt'.format(s_ind+1)
        list_name = os.path.join(do_list, name)
        f = open(list_name, 'w')  
        for i in np.arange(sets[s_ind], sets[s_ind+1]):
            loc = do_list+'/'+dirc_names[i]
            f.write(loc+'\n')
        f.close()
else:
    name = 'job_list.txt'
    list_name = os.path.join(do_list, name)
    f = open(list_name, 'w')  
    for i in range(jobs):
        loc = do_list+'/'+dirc_names[i]
        f.write(loc+'\n')
    f.close()