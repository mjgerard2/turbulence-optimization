# -*- coding: utf-8 -*-
"""
Created on Sat Mar 28 17:54:35 2020

@author: micha
"""

import numpy as np
import h5py as hf

import os, sys
WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)

import vmecTools.profile_analysis.profile_reader as pr

def advMetaNum(fileName):
    with open(fileName, 'r+') as file:
        for metaNum in file:
            pass
        file.write('\n{}'.format(int(metaNum)+1))

    return metaNum


def readStates(stateFile):
    states = []
    with open(stateFile, 'r') as file:
        for l, line in enumerate(file):
            line = line.strip()
            line = line.split()

            state = []
            for s in line:
                state.append(float(s))

            states.append(np.array(state))

    return np.array(states)


def readCrntConfig(conID):
    """ Get the coil current configuration from the configuration ID.

    Parameters
    ----------
    conID : str
        configuration ID in form of {mainID}-{setID}-{jobID}.

    Returns
    -------
    main_crnt : arr
        Main coil current configuration, returned as normalized quantities.
    aux_crnt : arr
        Auxiliary coil current configuration, returned as normalized
        quantities.
    """
    conArr = conID.split('-')
    path = os.path.join('/mnt','HSX_Database','HSX_Configs','main_coil_'+conArr[0])
    if not os.path.exists(path):
        raise KeyError('Main ID %s does not exist.' % conArr[0]) 

    filePath = os.path.join(path, 'conData.txt')
    with open(filePath, 'r') as f:
        lines = f.readlines()
        arr = lines[3].strip().split(' : ')[1]
        main_crnt = np.array([float(x) for x in arr[1:-1].split(', ')])

    filePath = os.path.join(path, 'conData.h5')
    with hf.File(filePath, 'r') as hf_:
        set_key = 'set_{}'.format(conArr[1])
        if set_key in hf_:
            job_list = hf_[set_key+'/config'][()]
        else:
            raise KeyError('Set ID %s does not exist for Main coil ID %s' % (conArr[1], conArr[0]))
    
    job_id = int(conArr[2])
    job_idx = np.where(job_list[:,0] == job_id)[0]
    if job_idx.shape[0] == 1:
        aux_crnt = job_list[job_idx[0], 1::]
    else:
        raise KeyError('Job ID %s does not exist for Main coil ID %s and Set ID %s' % (conArr[2], conArr[0], conArr[1]))
    
    return main_crnt, aux_crnt


def findPathFromCrnt(main_crnt, aux_crnt, base=os.path.join('/mnt', 'HSX_Database', 'HSX_Configs')):
    """ Find the configuration ID from the provided coil currents.

    Parameters
    ----------
    main_crnt : arr
        Array of the normalized main coil currents.
    aux_crnt : arr
        1D array of the normalized auxiliary coil currents.
    base : path, optional
        Directory in which the database is stored. Default is \"/mnt/HSX_Database/HSX_Configs\"

    Returns
    -------
    config_id : str 
        Configuration ID of the specified coil currents.
    """
    found, main_id = checkMainConfig(main_crnt)
    if found:
        main_key = 'main_coil_{}'.format(main_id)
        mainPath = os.path.join(base, main_key)

        succ = False
        hf_file = os.path.join(mainPath, 'conData.h5')
        with hf.File(hf_file, 'r') as hf_:
            for key in hf_:
                job_list = hf_[key+'/config'][()]
                job_idx = np.where(np.sum(np.abs(job_list[:,1:]-aux_crnt), axis=1) == 0)[0]
                if job_idx.shape[0] == 1:
                    set_id = key.split('_')[1]
                    job_id = str(int(job_list[job_idx[0], 0]))
                    succ = True
                    break

        if not succ:
            raise ValueError('Auxiliary coil currents not found.')
        else:
            config_id = '-'.join([main_id, set_id, job_id])
            return config_id
    
    else:
        raise ValueError('Main coil currents not found.')

def checkMainConfig(main):
    main_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs')
    dir_names = [f.name for f in os.scandir(main_path) if os.path.isdir(f.path)]
    dir_names = [name for name in dir_names if len(name) > 10]
    dir_names = [name for name in dir_names if name[0:10] == 'main_coil_']
    for dir_name in dir_names:
        filePath = os.path.join(main_path, dir_name, 'conData.txt')
        with open(filePath, 'r') as f:
            lines = f.readlines()
            arr = lines[3].strip().split(' : ')[1]
            main_chk = np.array([float(x) for x in arr[1:-1].split(', ')])
        comp = np.sum(np.abs(main-main_chk))
        if comp == 0:
            main_id = dir_name.split('_')[-1]
            return True, main_id
    return False, 0

def writeMetaFile(path, num, main, aux, base=10722.0, mult=14):
    nameTxt = os.path.join(path, 'conData.txt')
    nameH5 = os.path.join(path, 'conData.h5')

    job_num = str(aux.shape[0])
    set_id = 'set_{}'.format(num)

    aux_wID = np.empty((aux.shape[0], aux.shape[1]+1))
    for a, ax in enumerate(aux):
        aux_wID[a] = np.r_[a, ax]

    if os.path.exists(nameTxt):
        f = open(nameTxt, 'a')
        f.write(job_num+' Aux. Coil Configurations in '+set_id+'\n' +
                '(Current [A] = %.1f * %.1f * <percent>)\n\n' % (base, mult) )
        f.close()

        h5 = hf.File(nameH5, 'a')
        h5.create_dataset(set_id+'/config', data=aux_wID)
        h5.close()

    else:
        main_str = '[' + ', '.join(['{}'.format(i) for i in main]) + ']'

        f = open(nameTxt, 'w')
        f.write('Main Coil Multiplier\n' +
                '(Current [A] = %.1f * %.1f * <multiplier>)\n\n' % (base, mult) +
                '1-6 : ' + main_str + '\n\n' +
                str(job_num)+' Aux. Coil Configurations in '+set_id+'\n' +
                '(Current [A] = %.1f * %.1f * <percent>)\n\n' % (base, mult) )
        f.close()

        h5 = hf.File(nameH5, 'w')
        h5.create_dataset(set_id+'/config', data=aux_wID)
        h5.close()


def generate_input(main_state, aux_state, input_dump):
    # Construct Job MetaData
    mult = 14
    base_crnt = -10722.0
    main_crnts = base_crnt * main_state
    aux_crnts = mult * base_crnt * aux_state

    # Construct Job Input Files
    main_crnt_str = ' '.join(['{}'.format(i) for i in main_crnts])
    aux_crnt_str = ' '.join(['{}'.format(i) for i in aux_crnts])

    # Import Base Input File
    input_base = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'input.HSX_main')
    with open(input_base, 'r') as f:
        lines = f.readlines()

    new_lines = lines
    new_lines[22] = '  EXTCUR =  '+main_crnt_str+' '+aux_crnt_str+'\n'

    with open(input_dump, 'w') as f:
        for l in new_lines:
            f.write(l)

if __name__ == '__main__':
    if True:
        # Get Coil Currents From Configuration ID #
        # main_state, aux_state = readCrntConfig('0-3-488')
        # main_state, aux_state = readCrntConfig('0-3-702')
        main_state, aux_state = readCrntConfig('0-3-84')
        print(main_state)
        print(aux_state)

    if False:
        # get configuration ID from coil currents #
        main_crnt = np.array([1, 1, 0.95, 0.93, 1, 1])
        aux_crnt = np.array([-0.1, -0.1, -0.1, 0.1, 0.1, 0.1])
        config_id = findPathFromCrnt(main_crnt, aux_crnt)
        print(config_id)
