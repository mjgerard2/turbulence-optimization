import os
import h5py as hf
import numpy as np

from matplotlib.colors import LogNorm

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import plot_define as pd


class database_reader():

    def __init__(self):
        # import metric data #
        met_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'metric_data.h5')
        with hf.File(met_path, 'r') as hf_:
            self.met_data = hf_['metric data'][()]

        # Define metric data indices #
        self.met_idx = {'main_id': 0,
                        'set_id': 1,
                        'job_id': 2,
                        'eps': 3,
                        'qhs': 4,
                        'modB': 5,
                        'Bk': 6,
                        'Cn': 7,
                        'shat': 8,
                        'K': 9,
                        'delta': 10,
                        'rho': 11,
                       }

        # Define metric data labels #
        self.met_lab = {'eps': '$\mathcal{E}/\mathcal{E}^*$',
                        'qhs': '$\mathcal{Q}/\mathcal{Q}^*$',
                        'modB':'$\mathcal{B}_{\mathrm{mod}}/\mathcal{B}_{\mathrm{mod}}^*$',
                        'Bk': '$\mathcal{B}_{\kappa}/|\mathcal{B}_{\kappa}^*|$',
                        'Cn': '$\mathcal{C}_{\mathrm{n}}/|\mathcal{C}_{\mathrm{n}}^*|$',
                        'shat': '$\hat{\mathcal{S}}/\hat{\mathcal{S}}^*$',
                        'K': '$\mathcal{K}/\mathcal{K}^*$',
                        'delta': '$\Delta/\Delta^*$',
                        'rho': '$\rho/\rho^*$',
                       }

    def colorbar_plot(self, x_axis, y_axis, c_axis, reverse=True, cmap='viridis_r', log_norm=False):
        # plotting parameters #
        plot = pd.plot_define()
        plt = plot.plt
        fig, axs = plt.subplots(1, 2, sharex=False, sharey=False, tight_layout=True, figsize=(14, 6))

        ax1 = axs[0]
        ax2 = axs[1]

        # plot data #
        ae_neg = self.met_data[self.met_data[:, self.met_idx['Bk']] < 0]
        ae_neg = np.array(sorted(ae_neg, key=lambda x: x[self.met_idx[c_axis]], reverse=reverse))
        ae_pos = self.met_data[self.met_data[:, self.met_idx['Bk']] > 0]
        ae_pos = np.array(sorted(ae_pos, key=lambda x: x[self.met_idx[c_axis]], reverse=reverse))
        if log_norm:
            smap1 = ax1.scatter(ae_neg[:, self.met_idx[x_axis]], ae_neg[:, self.met_idx[y_axis]], c=ae_neg[:, self.met_idx[c_axis]], s=1, cmap=cmap,
                                norm=LogNorm(vmin=np.min(ae_neg[:, self.met_idx[c_axis]]), vmax=np.max(ae_neg[:, self.met_idx[c_axis]])))
            smap2 = ax2.scatter(ae_pos[:, self.met_idx[x_axis]], ae_pos[:, self.met_idx[y_axis]], c=ae_pos[:, self.met_idx[c_axis]], s=1, cmap=cmap,
                                norm=LogNorm(vmin=np.min(ae_neg[:, self.met_idx[c_axis]]), vmax=np.max(ae_neg[:, self.met_idx[c_axis]])))
        else:
            smap1 = ax1.scatter(ae_neg[:, self.met_idx[x_axis]], ae_neg[:, self.met_idx[y_axis]], c=ae_neg[:, self.met_idx[c_axis]], s=1, cmap=cmap)
            smap2 = ax2.scatter(ae_pos[:, self.met_idx[x_axis]], ae_pos[:, self.met_idx[y_axis]], c=ae_pos[:, self.met_idx[c_axis]], s=1, cmap=cmap)

        # axis labels #
        cbar1 = fig.colorbar(smap1, ax=ax1)
        cbar1.ax.set_ylabel(self.met_lab[c_axis])
        cbar2 = fig.colorbar(smap2, ax=ax2)
        cbar2.ax.set_ylabel(self.met_lab[c_axis])
        ax1.set_xlabel(self.met_lab[x_axis])
        ax2.set_xlabel(self.met_lab[x_axis])
        ax1.set_ylabel(self.met_lab[y_axis])

        return plt, fig, axs

    def colorbar_plot_DS(self, x_axis, y_axis, c_axis, bin_key, reverse=True, cmap='viridis_r'):
        # plotting parameters #
        plot = pd.plot_define()
        plt = plot.plt
        fig, axs = plt.subplots(1, 2, sharex=False, sharey=False, tight_layout=True, figsize=(14, 6))

        ax1 = axs[0]
        ax2 = axs[1]

        # plot data #
        ae_DS = self.met_data_DS[bin_key]
        ae_neg = ae_DS[ae_DS[:, self.met_idx['Bk']] < 0]
        ae_neg = np.array(sorted(ae_neg, key=lambda x: x[self.met_idx[c_axis]], reverse=reverse))
        ae_pos = ae_DS[ae_DS[:, self.met_idx['Bk']] > 0]
        ae_pos = np.array(sorted(ae_pos, key=lambda x: x[self.met_idx[c_axis]], reverse=reverse))
        smap1 = ax1.scatter(ae_neg[:, self.met_idx[x_axis]], ae_neg[:, self.met_idx[y_axis]], c=ae_neg[:, self.met_idx[c_axis]], s=1, cmap=cmap)
        smap2 = ax2.scatter(ae_pos[:, self.met_idx[x_axis]], ae_pos[:, self.met_idx[y_axis]], c=ae_pos[:, self.met_idx[c_axis]], s=1, cmap=cmap)

        # axis labels #
        cbar1 = fig.colorbar(smap1, ax=ax1)
        cbar1.ax.set_ylabel(self.met_lab[c_axis])
        cbar2 = fig.colorbar(smap2, ax=ax2)
        cbar2.ax.set_ylabel(self.met_lab[c_axis])
        ax1.set_xlabel(self.met_lab[x_axis])
        ax2.set_xlabel(self.met_lab[x_axis])
        ax1.set_ylabel(self.met_lab[y_axis])

        return plt, fig, axs

    def down_sample_data(self, var, bins, LogBins=False):
        var_min = 0.9999*np.min(self.met_data[:, self.met_idx[var]])
        var_max = np.max(self.met_data[:, self.met_idx[var]])
        if LogBins:
            self.var_rng = np.logspace(np.log10(var_min), np.log10(var_max), bins+1)
        else:
            self.var_rng = np.linspace(var_min, var_max, bins+1)

        var_idx = self.met_idx[var]
        self.met_data_DS = {}
        for i in range(bins):
            var_min, var_max = self.var_rng[i], self.var_rng[i+1]
            self.met_data_DS['bin {}'.format(i)] = self.met_data[(self.met_data[:, var_idx] > var_min) & (self.met_data[:, var_idx] <= var_max)]

    def filter_Bk(self, x_axis, y_axis, c_axis, Bk_min, Bk_max, reverse=True, cmap='jet'):
        ae_data = self.met_data[(self.met_data[:, self.met_idx['Bk']] >= Bk_min) & (self.met_data[:, self.met_idx['Bk']] < Bk_max)]
        clr_min = np.min(self.met_data[:, self.met_idx['ae']])
        clr_max = np.max(self.met_data[:, self.met_idx['ae']])
        """
        if np.sign(Bk_min) < 0:
            neg_data = self.met_data[self.met_data[:, self.met_idx['Bk']] < 0]
            clr_min = np.min(neg_data[:, self.met_idx['Bk']])
            clr_max = np.max(neg_data[:, self.met_idx['Bk']])
        else:
            pos_data = self.met_data[self.met_data[:, self.met_idx['Bk']] > 0]
            clr_min = np.min(pos_data[:, self.met_idx['Bk']])
            clr_max = np.max(pos_data[:, self.met_idx['Bk']])
        """

        # plotting parameters #
        plot = pd.plot_define()
        plt = plot.plt
        fig, ax = plt.subplots(1, 1, tight_layout=True)

        # plot data #
        ae_data = np.array(sorted(ae_data, key=lambda x: x[self.met_idx[c_axis]], reverse=reverse))
        smap = ax.scatter(ae_data[:, self.met_idx[x_axis]], ae_data[:, self.met_idx[y_axis]], c=ae_data[:, self.met_idx[c_axis]], s=1, cmap=cmap, vmin=clr_min, vmax=clr_max)

        # axis labels #
        cbar = fig.colorbar(smap, ax=ax)
        cbar.ax.set_ylabel(self.met_lab[c_axis])
        ax.set_xlabel(self.met_lab[x_axis])
        ax.set_ylabel(self.met_lab[y_axis])

        return plt, fig, ax


if __name__ == '__main__':
    x_axis = 'K'
    y_axis = 'qhs'
    c1_axis = 'Bk'
    c2_axis = 'Cn'

    cmap = 'viridis'

    dbase = database_reader()

    # read in 100 selected data #
    met_path = os.path.join('/mnt', 'HSX_Database', 'GENE', 'eps_valley', 'data_files', 'metric_data.h5')
    with hf.File(met_path, 'r') as hf_:
        met_data = hf_['metric data'][()]

    met_sel_neg = met_data[met_data[:, dbase.met_idx['Bk']] < 0]

    # plotting parameters #
    plot = pd.plot_define()
    plt = plot.plt
    fig, axs = plt.subplots(2, 2, sharex=False, sharey=True, tight_layout=True, figsize=(14, 10))

    ax1 = axs[0, 0]
    ax2 = axs[0, 1]
    ax3 = axs[1, 0]
    ax4 = axs[1, 1]

    # plot data #
    met_neg = dbase.met_data[dbase.met_data[:, dbase.met_idx['Bk']] < 0]
    met_pos = dbase.met_data[dbase.met_data[:, dbase.met_idx['Bk']] > 0]

    met_neg = np.array(sorted(met_neg, key=lambda x: x[dbase.met_idx[c1_axis]], reverse=False))
    met_pos = np.array(sorted(met_pos, key=lambda x: x[dbase.met_idx[c1_axis]], reverse=True))
    met_sel_neg = np.array(sorted(met_sel_neg, key=lambda x: x[dbase.met_idx[c1_axis]], reverse=False))
    smap1 = ax1.scatter(met_neg[:, dbase.met_idx[x_axis]], met_neg[:, dbase.met_idx[y_axis]], c=met_neg[:, dbase.met_idx[c1_axis]], s=1, cmap=cmap)
    smap2 = ax2.scatter(met_pos[:, dbase.met_idx[x_axis]], met_pos[:, dbase.met_idx[y_axis]], c=met_pos[:, dbase.met_idx[c1_axis]], s=1, cmap=cmap)
    ax1.scatter(met_sel_neg[:, dbase.met_idx[x_axis]], met_sel_neg[:, dbase.met_idx[y_axis]], c=met_sel_neg[:, dbase.met_idx[c1_axis]], edgecolor='k',
            marker='s', s=100, vmin=np.min(met_neg[:, dbase.met_idx[c1_axis]]), vmax=np.max(met_neg[:, dbase.met_idx[c1_axis]]), cmap=cmap)
    
    met_neg = np.array(sorted(met_neg, key=lambda x: x[dbase.met_idx[c2_axis]], reverse=False))
    met_pos = np.array(sorted(met_pos, key=lambda x: x[dbase.met_idx[c2_axis]], reverse=False))
    met_sel_neg = np.array(sorted(met_sel_neg, key=lambda x: x[dbase.met_idx[c1_axis]], reverse=False))
    smap3 = ax3.scatter(met_neg[:, dbase.met_idx[x_axis]], met_neg[:, dbase.met_idx[y_axis]], c=met_neg[:, dbase.met_idx[c2_axis]], s=1, cmap=cmap)
    smap4 = ax4.scatter(met_pos[:, dbase.met_idx[x_axis]], met_pos[:, dbase.met_idx[y_axis]], c=met_pos[:, dbase.met_idx[c2_axis]], s=1, cmap=cmap)
    ax3.scatter(met_sel_neg[:, dbase.met_idx[x_axis]], met_sel_neg[:, dbase.met_idx[y_axis]], c=met_sel_neg[:, dbase.met_idx[c2_axis]], edgecolor='k',
            marker='s', s=100, vmin=np.min(met_neg[:, dbase.met_idx[c2_axis]]), vmax=np.max(met_neg[:, dbase.met_idx[c2_axis]]), cmap=cmap)

    # axis labels #
    cbar1 = fig.colorbar(smap1, ax=ax1)
    cbar1.ax.set_ylabel(dbase.met_lab[c1_axis])
    cbar2 = fig.colorbar(smap2, ax=ax2)
    cbar2.ax.set_ylabel(dbase.met_lab[c1_axis])
    cbar3 = fig.colorbar(smap3, ax=ax3)
    cbar3.ax.set_ylabel(dbase.met_lab[c2_axis])
    cbar4 = fig.colorbar(smap4, ax=ax4)
    cbar4.ax.set_ylabel(dbase.met_lab[c2_axis])

    ax1.set_ylabel(dbase.met_lab[y_axis])
    ax2.set_ylabel(dbase.met_lab[y_axis])
    ax3.set_ylabel(dbase.met_lab[y_axis])
    ax4.set_ylabel(dbase.met_lab[y_axis])

    ax1.set_xlabel(dbase.met_lab[x_axis])
    ax2.set_xlabel(dbase.met_lab[x_axis])
    ax3.set_xlabel(dbase.met_lab[x_axis])
    ax4.set_xlabel(dbase.met_lab[x_axis])

    # axis limits #
    ax1.set_xlim(ax1.get_xlim())
    ax1.set_ylim(0, ax1.get_ylim()[1])

    ax1.plot(ax1.get_xlim(), [1, 1], c='k', ls='--')
    ax2.plot(ax2.get_xlim(), [1, 1], c='k', ls='--')
    ax3.plot(ax3.get_xlim(), [1, 1], c='k', ls='--')
    ax4.plot(ax4.get_xlim(), [1, 1], c='k', ls='--')

    # Save/Show #
    # plt.show()
    save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'TTF_2023', 'figures', 'database-scatter-plot.png')
    plt.savefig(save_path)
