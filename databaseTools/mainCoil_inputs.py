#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  1 13:43:00 2019

@author: michael
"""

import os
import sys
import numpy as np

import functions as fun


exp = 'HSX'

basePath = os.path.join('/mnt', 'HSX_Database')
dirConfigs = os.path.join(basePath, '{}_Configs'.format(exp))
dirOut = os.path.join(basePath, '{}_Configs'.format(exp), 'queued_out')

# Import Base Input File
file_name = os.path.join(dirConfigs, 'coil_data', 'input.{}_main'.format(exp))

f = open(file_name, 'r')
lines = f.readlines()
f.close()

# Import Coil States
mainStates = 'mainStates.txt'
main_states = fun.readStates(mainStates)

auxStates = 'auxStates.txt'
aux_states = fun.readStates(auxStates)

# Construct Main Coil Configuration Files
for main_state in main_states:

    # Manage Config Tracker
    mainConfig_exists, num = fun.checkMainConfig(dirConfigs, main_state)

    if mainConfig_exists:
        conNum = num
    else:
        conTrack = os.path.join(dirConfigs, 'config_tracker.txt')
        conNum = fun.advMetaNum(conTrack)

    name = 'main_coil_{}'.format(conNum)

    # Construct Job Directory Structure
    config_dir = os.path.join(dirConfigs, name)
    set_track = os.path.join(config_dir, 'set_tracker.txt')

    if os.path.isdir(config_dir):
        setNum = fun.advMetaNum(set_track)

    else:
        os.mkdir(config_dir)

        setNum = 0

        f = open(set_track, 'w')
        f.write('0\n1')
        f.close()

    setDir = 'set_{}'.format(setNum)

    config_set_dir = os.path.join(config_dir, setDir)
    os.mkdir(config_set_dir)

    do_list = os.path.join(dirOut, 'do_list_{0}_{1}'.format(conNum, setNum))
    if not os.path.isdir(do_list):
        os.mkdir(do_list)

    # Construct Job MetaData
    mult = 14
    base_crnt = -10722.0
    main_crnts = base_crnt * main_state
    aux_crnts = mult * base_crnt * aux_states

    fun.writeMetaFile(config_dir, setNum, main_state, aux_states)

    # Construct Job Input Files
    main_crnt_str = ' '.join(['{}'.format(i) for i in main_crnts])

    dirc_names = []
    for c, crnt in enumerate(aux_crnts):
        dirc_name = 'job_{}'.format(c)
        dirc_names.append(dirc_name)
        new_dirc = os.path.join(do_list, dirc_name)
        os.makedirs(new_dirc)

        crnt_str = ' '.join(['{}'.format(i) for i in crnt])

        new_lines = lines
        new_lines[22] = '  EXTCUR =  '+main_crnt_str+' '+crnt_str+'\n'

        file_name_new = os.path.join(new_dirc, 'input.HSX_main')

        f = open(file_name_new, 'w')
        for l in new_lines:
            f.write(l)
        f.close()

    # Construct Job Do List
    jobs = len(dirc_names)
    runs = np.ceil(jobs/9000)
    if runs > 1:
        sets = np.linspace(0, jobs, runs+1, dtype=int)

        for s_ind, s in enumerate(sets[1::]):
            name = 'job_list_{}.txt'.format(s_ind+1)
            list_name = os.path.join(do_list, name)
            f = open(list_name, 'w')
            for i in np.arange(sets[s_ind], sets[s_ind+1]):
                loc = 'do_list_{0}_{1}/{2}'.format(conNum, setNum, dirc_names[i])
                f.write(loc+'\n')
            f.close()
    else:
        name = 'job_list.txt'
        list_name = os.path.join(do_list, name)
        f = open(list_name, 'w')
        for i in range(jobs):
            loc = 'do_list_{0}_{1}/{2}'.format(conNum, setNum, dirc_names[i])
            f.write(loc+'\n')
        f.close()
