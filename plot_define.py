import os

from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
import matplotlib.pyplot as plt


class plot_define():

    def __init__(self, fontSize=14, labelSize=16, lineWidth=1):

        plt.close('all')

        font = {'family': 'sans-serif',
                'serif': 'Times New Roman',
                'weight': 'normal',
                'size': fontSize}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = labelSize
        mpl.rcParams['lines.linewidth'] = lineWidth

        self.mpl = mpl
        self.plt = plt

    def construct_axis(self, figsize=(8, 6)):
        self.fig, self.ax = plt.subplots(1, 1, figsize=figsize)
        return self.fig, self.ax

    def construct_3D_axis(self):
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111, projection='3d')
        return self.fig, self.ax

