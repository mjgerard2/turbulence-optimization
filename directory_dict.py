# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 11:29:05 2020

@author: micha
"""

import os

### pythonTools directory ###
base_path = os.path.join('/home','michael','Desktop','Research','turbulence-optimization','pythonTools')

### Base directory where local configuration data is stored ###
exp_local_base = os.path.join('C:\\','Users','micha','Documents','Research','Turbulence_Optimization','dataVMEC')

### Base directory where external configuration data is stored ###
exp_ext_base = os.path.join('D:\\','HSX_Configs')

### Experiment directories where local configuration data is stored ###
exp_local = {'HSX' : os.path.join(exp_local_base, 'HSX'),
             'W7X' : os.path.join(exp_local_base,'W7X'),
             'ITOS' : os.path.join(exp_local_base,'ITOS'),
             'LHD' : os.path.join(exp_local_base, 'LHD')}

### HSX QHS configuration ###
pathQHS = os.path.join(exp_local['HSX'],'coil_configs','main_coil_0','set_1','job_0')

### W7X 0 Beta Configuration ###
pathW7X = os.path.join(exp_local['W7X'],'coil_configs')

### SFINCS Data Directory ###
baseSFINCS = os.path.join('C:\\','Users','micha','AppData','Local','Packages','CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc','LocalState','rootfs','home','mjgerard2','fortran_repos','sfincs','sfincs_output')

pathSFINCS = {'HSX' : os.path.join(baseSFINCS, 'hsx'),
              'W7X' : os.path.join(baseSFINCS, 'w7x')}

### DESCUR Directory ###
pathDESCUR = os.path.join('C:\\','Users','micha','AppData','Local','Packages','CanonicalGroupLimited.UbuntuonWindows_79rhkp1fndgsc','LocalState','rootfs','home','mjgerard2','fortran_repos','stellinstall','trunk','build','bin')

### GIST Directory ###
pathGIST = os.path.join('D:\\','GENE')

### One Drive Directory ###
#pathDrive = os.path.join('C:\\','Users','micha','Desktop','OneDrive - UW-Madison','Figures')
pathDrive = os.path.join('C:\\','Users','micha','Desktop','OneDrive - UW-Madison')
