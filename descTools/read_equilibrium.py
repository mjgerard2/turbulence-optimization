import os, sys
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from desc.io import load
from desc.plotting import LinearGrid
from desc.plotting import plot_1d, plot_2d, plot_3d, plot_section, plot_surfaces

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)
from vmecTools.wout_files import wout_read as wr


# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['lines.linewidth'] = 2

kappa_tag = 'k2p00'
base_path = os.path.join('/home',
                         'michael',
                         'Desktop',
                         '3D_model_kScan',
                         'scan_12',
                         kappa_tag)

desc_path = os.path.join(base_path, 'desc_3D_model_'+kappa_tag+'.h5')
wout_name = 'wout_3D_model_'+kappa_tag+'.nc'

wout = wr.readWout(base_path, wout_name)
rho_vals = np.sqrt(wout.s_grid)
npts = rho_vals.shape[0]
iota_wout = wout.iota(rho_vals)

eq = load(desc_path)
grid_1D = np.stack((rho_vals, np.zeros(npts), np.zeros(npts)), axis=1)
iota_data = eq.compute('iota', grid=grid_1D)
iota = iota_data['iota']

# Plot iota Profile #
plt.plot(rho_vals, iota, c='k')
plt.plot(rho_vals, iota, c='tab:red', ls='--')
plt.show()
"""
# Make Straight Field-Line Plot #
grid_2d_0p5 = LinearGrid(M=100, N=100, rho=1.0)
mod_B_data = eq.compute('|B|', grid=grid_2d_0p5)
mod_B = mod_B_data['|B|']

zeta = grid_2d_0p5.nodes[:, 2].reshape((grid_2d_0p5.M, grid_2d_0p5.L, grid_2d_0p5.N), order="F").squeeze()
theta = grid_2d_0p5.nodes[:, 1].reshape((grid_2d_0p5.M, grid_2d_0p5.L, grid_2d_0p5.N), order="F").squeeze()
mod_B = mod_B.reshape((grid_2d_0p5.M, grid_2d_0p5.L, grid_2d_0p5.N), order='F')

plt.contourf(zeta, theta, mod_B[:, 0, :], cmap='jet')
plt.xlabel(r'$\zeta$')
plt.ylabel(r'$\theta$')
plt.show()
"""
