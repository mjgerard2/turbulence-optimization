import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from desc.io import load
from desc.plotting import LinearGrid
from desc.plotting import plot_1d, plot_2d, plot_3d, plot_section, plot_surfaces

kappa_tag = 'k0p50'
desc_path = os.path.join('/home',
                         'michael',
                         'Desktop',
                         '3D_model_kScan',
                         'scan_12',
                         kappa_tag,
                         'desc_3D_model_'+kappa_tag+'.h5')

# Plotting Parameters #
plt.close('all')

font = {'family': 'sans-serif',
        'weight': 'normal',
        'size': 18}

mpl.rc('font', **font)

mpl.rcParams['axes.labelsize'] = 20
mpl.rcParams['lines.linewidth'] = 2

eq = load(desc_path)

# fig, ax = plot_3d(eq, '|B|')
fig, ax = plot_section(eq, '|F|', norm_F=True, log=True)
plt.show()
