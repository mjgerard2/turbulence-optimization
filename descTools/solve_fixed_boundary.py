import os, sys
import numpy as np

from desc.equilibrium import Equilibrium
from desc.geometry import FourierRZToroidalSurface
from desc.profiles import (PowerSeriesProfile,
                           SplineProfile)
from desc.optimize import Optimizer
from desc.plotting import plot_1d, plot_2d, plot_3d, plot_section, plot_surfaces
from desc.objectives import (get_fixed_boundary_constraints,
                             ObjectiveFunction,
                             FixBoundaryR,
                             FixBoundaryZ,
                             FixLambdaGauge,
                             FixPressure,
                             FixIota,
                             FixPsi,
                             ForceBalance)

WORKDIR = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(WORKDIR)
from vmecTools.wout_files import wout_read as wr

kappa_set = np.linspace(0.5, 2., 7)

base_dirc = os.path.join('/home', 'michael', 'Desktop', '3D_model_kScan', 'scan_12')
for kdx, kappa in enumerate(kappa_set):
    kappa_tag = 'k{0:0.2f}'.format(kappa).replace('.', 'p')
    print('Working: '+kappa_tag+' ({0:0.0f}|{1:0.0f})'.format(kdx+1, kappa_set.shape[0]))

    # Get iota Profile From WOUT #
    wout_dirc = os.path.join(base_dirc, kappa_tag)
    wout_name = 'wout_3D_model_'+kappa_tag+'.nc'
    wout = wr.readWout(wout_dirc, name=wout_name)

    rho_profile = np.sqrt(wout.s_grid)
    iota_profile = wout.iota(rho_profile)
    print(iota_profile)
    """
    # Equilibrium Parameters
    Ro = 1.2
    a = 0.06
    Delta = 0.06
    rho = a*np.sqrt(2/(kappa**2 + 1))

    # Boundary Equilibrium #
    R_lmn = [Ro, Delta, .5*rho*(kappa+1), -.5*rho*(kappa-1), -.5*rho*(kappa-1)]
    modes_R = [[0, 0], [0, 1], [1, 0], [1, 1], [-1, -1]]
    Z_lmn = [Delta, .5*rho*(kappa+1), -.5*rho*(kappa-1), .5*rho*(kappa-1)]
    modes_Z = [[0, -1], [-1, 0], [1, -1], [-1, 1]]

    # Generate Surface Object #
    surface = FourierRZToroidalSurface(R_lmn=R_lmn, modes_R=modes_R,
                                       Z_lmn=Z_lmn, modes_Z=modes_Z,
                                       NFP=10)

    # Define Profiles #
    pressure = PowerSeriesProfile(params=[0, 0], modes=[0, 2])
    iota = SplineProfile(values=iota_profile, knots=rho_profile)

    # Equilibrium Object #
    eq = Equilibrium(surface=surface,
                     pressure=pressure,
                     iota=iota,
                     Psi=1.0,
                     NFP=10,
                     M=10,
                     N=10,
                     sym=True)

    # Define, Optimizer, Constraints and Objectives #
    optimizer = Optimizer("lsq-exact")
    constraints = (FixBoundaryR(fixed_boundary=True),
                   FixBoundaryZ(fixed_boundary=True),
                   FixPressure(),
                   FixIota(),
                   FixPsi())
    objectives = ForceBalance()
    obj = ObjectiveFunction(objectives=objectives)

    # Solve Equilibrium #
    eq.solve(verbose=3,
             maxiter=100,
             constraints=constraints,
             optimizer=optimizer,
             objective=obj)

    # Save Equilibrium Solution #
    file_path = os.path.join('/home',
                             'michael',
                             'Desktop',
                             '3D_model_kScan',
                             'scan_12',
                             kappa_tag,
                             'desc_3D_model_'+kappa_tag+'.h5')
    eq.save(file_path)
    """
