import os
import h5py as hf
import numpy as np

import curveB_tools as cb
import boundary_fits as bf

from multiprocessing import Process, Queue, cpu_count

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import databaseTools.functions as fun
import vmecTools.wout_files.wout_read as wr 


def job_loop(in_que, out_que):

    while True:

        config_id, cnt, tot, run_dict = in_que.get()
        if config_id == 'Done':
            break

        else:
            # unpack dictionary #
            u_dom = run_dict['poloidal grid']
            v_dom = run_dict['toroidal grid']
            keys = run_dict['transform keys']
            norms = run_dict['normalizations']

            # Calculate Metrics #
            mID, sID, jID = config_id.split('-')
            main_id = 'main_coil_%s' % mID
            set_id = 'set_%s' % sID
            job_id = 'job_%s' % jID
            wout_name = 'wout_%s_mn1824_ns101.nc' % config_id
            # wout_name = 'wout_HSX_main_opt0.nc'
            wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, wout_name)

            if os.path.isfile(wout_path):
                wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)
                s_val = 0.5*((norms[-1]/wout.a_minor)**2)
                wout.transForm_3D(np.array([s_val]), u_dom, v_dom, keys)

                # Calculate Flux Surface Averaged Metrics #
                curB = cb.BcurveTools(wout)
                Bk = curB.calc_Bk() / norms[0]
                Cn = curB.calc_Cn() / norms[1]
                main, aux = fun.readCrntConfig(config_id)
                job_set = np.r_[main, aux, np.array([Bk, Cn])]

            else:
                job_set = np.r_[main, aux, np.array([np.nan, np.nan])]

            out_que.put([config_id, cnt, tot, job_set])


def save_loop(in_que):
    while True:
        config_id, cnt, tot, data = in_que.get()
        if config_id == 'Done':
            break
        else:
            print('{0}: ({1}|{2})'.format(config_id, cnt, tot))
            save_file = os.path.join('/home', 'michael', 'Desktop', 'calc_metrics', 'data_files', 'ML_data_prep_m1824.h5')
            with hf.File(save_file, 'a') as hf_:
                hf_.create_dataset(config_id, data=data)


# Number of CPUs to use #
num_of_cpus = 8 # cpu_count()-1

# define computational grid #
upts = 101
vpts = upts
u_dom = np.linspace(-np.pi, np.pi, upts)
v_dom = np.linspace(-.25*np.pi, .25*np.pi, vpts)

# Calculate Variables Throughout Domain #
keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du',
        'dZ_dv', 'Bmod', 'dBmod_ds', 'dBmod_du', 'dBmod_dv', 'Bs_covar', 'Bu_covar', 'Bv_covar',
        'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du']

wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
# wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)
wout.transForm_3D(np.array([0.5]), u_dom, v_dom, keys)

# Calculate Flux Surface Averaged Metrics #
curB = cb.BcurveTools(wout)
Bk = np.abs(curB.calc_Bk())
Cn = np.abs(curB.calc_Cn())

# metric dictionary #
run_dict = {'poloidal grid': u_dom,
            'toroidal grid': v_dom,
            'transform keys': keys,
            'normalizations': np.array([Bk, Cn, wout.a_minor])}

# generate configuration list #
sel_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'ae_mn1824_res128_npol16.h5')
with hf.File(sel_path, 'r') as hf_:
    config_arrs = hf_['config ids']
    config_list = []
    for config_arr in config_arrs:
        config_id = '-'.join(['{}'.format(int(x)) for x in config_arr])
        config_list.append(config_id)

# Check Saved Data #
saved_data = {}
save_file = os.path.join('/home', 'michael', 'Desktop', 'calc_metrics', 'data_files', 'ML_data_prep_mn1824.h5')
if os.path.isfile(save_file):
    with hf.File(save_file, 'r') as hf_:
        for key in hf_:
            saved_data[key] = hf_[key][()]

# populate queue #
in_que = Queue()
out_que = Queue()
for cdx, config_id in enumerate(config_list):
    cnt = cdx+1
    tot = len(config_list)
    if not config_id in saved_data:
        in_que.put([config_id, cnt, tot, run_dict])

# start metric calculation processes #
work_procs = []
for j in range(num_of_cpus-1):
    in_que.put(['Done', 0, 0, run_dict])
    proc = Process(target=job_loop, args=(in_que, out_que,))
    proc.start()
    work_procs.append(proc)

# print('start saver')
save_proc = Process(target=save_loop, args=(out_que,))
save_proc.start()

for k, proc in enumerate(work_procs):
    proc.join()

out_que.put(['Done', 0, 0, 0])
