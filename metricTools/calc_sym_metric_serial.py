import os, sys
import shlex
import subprocess
import h5py as hf
import numpy as np
from netCDF4 import Dataset

from multiprocessing import Process, Queue, cpu_count

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
# import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.booz_read as br

hgh_res = True

# get normalization data #
if hgh_res:
    wout_qhs = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
else:
    wout_qhs = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
rootgrp = Dataset(wout_qhs, 'r')
qhs_minor = rootgrp['/Aminor_p'][:]
rootgrp.close()

# read xbooz_xform output #
if hgh_res:
    booz_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'boozmn_QHS_mn7294_ns101.nc')
else:
    booz_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'boozmn_wout_HSX_main_opt0_32_32.nc')
booz = br.readBooz(booz_path)
xm = booz.xm
xn = booz.xn
Bmod_amps = booz.fourierAmps['Bmod'](0.5)

# calculate symmetry metric #
Bmn = 0
for i in range(xn.shape[0]):
    if xm[i] == 0 and xn[i] == 0:
        B_00 = Bmod_amps[i]
    elif xm[i] == 0 and xn[i] != 0:
        Bmn = Bmn + Bmod_amps[i]**2
    else:
        sym_chk = xn[i]/xm[i]
        if not sym_chk == 4:
            Bmn = Bmn + Bmod_amps[i]**2
sym_metric_qhs = np.sqrt(Bmn)/B_00

# generate configuration list #
config_list = ['well11p0']

# check saved data #
saved_data = {}
if hgh_res:
    save_file = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'ML_sym_metric_mn7294.h5')
else:
    save_file = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'ML_sym_metric_mn3232.h5')
if os.path.isfile(save_file):
    with hf.File(save_file, 'r') as hf_:
        for key in hf_:
            saved_data[key] = hf_[key][()]

for config in config_list:
    if config in saved_data:
        continue
    # copy over wout file #
    config_split = config.split('-')
    if len(config_split) == 3:
        mID = 'main_coil_{}'.format(config_split[0])
        sID = 'set_{}'.format(config_split[1])
        jID = 'job_{}'.format(config_split[2])
        if hgh_res:
            wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mID, sID, jID, 'wout_%s_mn1824_ns101.nc' % config)
        else:
            wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', mID, sID, jID, 'wout_HSX_main_opt0.nc')
    else:
        wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'unique_configurations', 'wout_HSX_%s_mn1824_ns101.nc' % config)
    wout_name = 'wout_%s' % config
    subprocess.run(['cp', wout_path, './%s.nc' % wout_name])

    # get minor radius of configuration #
    rootgrp = Dataset(wout_path, 'r')
    ns = rootgrp['/ns'][:]
    a_minor = rootgrp['/Aminor_p'][:]
    rootgrp.close()

    # get psi surface #
    s_vmec = np.linspace(0, 1, ns)
    s_val = .5*((qhs_minor/a_minor)**2)
    s_idx = np.argmin(np.abs(s_vmec-s_val))

    # generate xbooz_input #
    xbooz_file = './xbooz_input_%s' % config
    with open(xbooz_file, 'w') as f:
        if hgh_res:
            f.write('72 94\n')
        else:
            f.write('32 32\n')
        f.write('{}\n'.format(wout_name))
        f.write('{}'.format(s_idx))

    # run xbooz_xform #
    xbooz_exe = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec', 'xbooz_xform')
    cmmd = '%s %s' % (xbooz_exe, xbooz_file)
    subprocess.run(shlex.split(cmmd), stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

    # read xbooz_xform output #
    booz_path = os.path.join(os.getcwd(), 'boozmn_{}.nc'.format(wout_name)) 
    rootgrp = Dataset(booz_path, 'r')
    xm = rootgrp['/ixm_b'][:]
    xn = rootgrp['/ixn_b'][:]
    Bmod_amps = rootgrp['/bmnc_b'][0, :]
    rootgrp.close()

    # calculate symmetry metric #
    Bmn = 0
    for i in range(xn.shape[0]):
        if xm[i] == 0 and xn[i] == 0:
            B_00 = Bmod_amps[i]
        elif xm[i] == 0 and xn[i] != 0:
            Bmn = Bmn + Bmod_amps[i]**2
        else:
            sym_chk = xn[i]/xm[i]
            if not sym_chk == 4:
                Bmn = Bmn + Bmod_amps[i]**2
    sym_metric = np.sqrt(Bmn)/(B_00*sym_metric_qhs)

    # remove unnecessary files #
    subprocess.run(['rm', xbooz_file])
    subprocess.run(['rm', booz_path])
    subprocess.run(['rm', '%s.nc' % wout_name])

    # pass output to output queue #
    with hf.File(save_file, 'a') as hf_:
        hf_.create_dataset(config, data=sym_metric)

