import os, sys
import numpy as np

from scipy.optimize import brentq
from scipy.interpolate import CubicSpline

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as ani
# from matplotlib.patches import Arrow

from matplotlib.gridspec import GridSpec, SubplotSpec


ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import vmecTools.wout_files.wout_read as wr

class shape_calculator:
    """
    A class that calculates the plasma boundary shaping parameters in a rotated
    reference frame. The boundary shapes are consistent with those described in
    T. C. Luce Plasma Phys. Control. Fusion 55 (2013) 095009

    ...

    Attributes
    ----------
    wout : obj
        VMEC output equilibrium file, interpreted by the read_wout class in wout_reader.py
    tpts : int
        Number of toroidal points along which shaping parameters will be calculated

    Methods
    -------
    """
    def __init__(self, wout, psiN, tpts, half_grid=True, rotation_method='full helix'):
        # define equilibrium constants #
        self.nfp = wout.nfp
        self.minor = wout.a_minor
        self.wout = wout

        # define computational grid #
        self.psiN = psiN
        self.tpts = tpts
        if half_grid:
            self.ppts = 2*self.tpts
            self.tor_dom = np.linspace(0, np.pi/self.nfp, self.tpts)
        else:
            self.ppts = self.tpts
            self.tor_dom = np.linspace(-np.pi/self.nfp, np.pi/self.nfp, self.tpts)
        self.pol_dom = np.linspace(0, 2*np.pi, self.ppts, endpoint=True)

        # calculate magnetic axis #
        wout.transForm_2D_sSec(0, np.array([0]), self.tor_dom, ['R', 'Z'])
        self.R_ma = wout.invFourAmps['R'][:, 0]
        self.Z_ma = wout.invFourAmps['Z'][:, 0]
        self.ma_vec = np.stack((self.R_ma, self.Z_ma), axis=1)

        # define amplitude keys #
        if rotation_method == 'half helix':
            amp_keys = ['R', 'Z']
        elif rotation_method == 'full helix':
            amp_keys = ['R', 'Z']
        elif rotation_method == 'field minimum':
            amp_keys = ['R', 'Z', 'Bmod']
        else:
            raise KeyError(rotation_method+' is not a valid key for rotation_method.')

        # calculate |B| distribution #
        s_grid = wout.s_grid[wout.s_grid <= self.psiN]
        wout.transForm_3D(s_grid[1::], self.pol_dom, self.tor_dom, ['R','Z','Bmod'])
        self.R_grid = self.wout.invFourAmps['R']
        self.Z_grid = self.wout.invFourAmps['Z']
        self.B_grid = self.wout.invFourAmps['Bmod']

        # calculate R-Z coordinates #
        wout.transForm_2D_sSec(self.psiN, self.pol_dom, self.tor_dom, amp_keys)
        self.R = wout.invFourAmps['R']
        self.Z = wout.invFourAmps['Z']

        # populate rotation matrices #
        if rotation_method == 'half helix':
            cos_Ntor = np.cos(.5*self.nfp*self.tor_dom)
            sin_Ntor = np.sin(.5*self.nfp*self.tor_dom)
        elif rotation_method == 'full helix':
            cos_Ntor = np.cos(self.nfp*self.tor_dom)
            sin_Ntor = np.sin(self.nfp*self.tor_dom)
        elif rotation_method == 'field minimum':
            B = wout.invFourAmps['Bmod']
            idx_min = np.argmin(B, axis=1)
            
            cos_Ntor = np.empty(self.tpts)
            sin_Ntor = np.empty(self.tpts)
            for i, mdx in enumerate(idx_min):
                R = self.R[i,mdx]-wout.R_major
                Z = self.Z[i,mdx]
                phi = np.arctan2(Z, R)
                cos_Ntor[i] = np.cos(phi)
                sin_Ntor[i] = np.sin(phi)
        else:
            raise KeyError(rotation_method+' is not a valid key for rotation_method.')
        self.cos_Ntor = cos_Ntor
        self.sin_Ntor = sin_Ntor
        self.rot_matrix = np.stack((cos_Ntor, sin_Ntor, -sin_Ntor, cos_Ntor), axis=1).reshape(self.tpts, 2, 2)

        # calculate rotated R-Z coordaintes #
        cyld_vec = np.stack((self.R, self.Z), axis=2)
        rot_cyld_vec = np.empty(cyld_vec.shape)
        for i in range(self.tpts):
            for j in range(self.ppts):
                rot_cyld_vec[i, j] = np.matmul(self.rot_matrix[i], cyld_vec[i, j])
        self.R_prime = rot_cyld_vec[:, :, 0]
        self.Z_prime = rot_cyld_vec[:, :, 1]

        # get extrema in rotated reference frame #
        self.R_max = np.max(self.R_prime, axis=1)
        self.R_min = np.min(self.R_prime, axis=1)
        self.R_Zmax = np.array([self.R_prime[i, idx] for i, idx in enumerate(np.argmax(self.Z_prime, axis=1))])
        self.R_Zmin = np.array([self.R_prime[i, idx] for i, idx in enumerate(np.argmin(self.Z_prime, axis=1))])

        self.Z_max = np.max(self.Z_prime, axis=1)
        self.Z_min = np.min(self.Z_prime, axis=1)
        self.Z_Rmax = np.array([self.Z_prime[i, idx] for i, idx in enumerate(np.argmax(self.R_prime, axis=1))])
        self.Z_Rmin = np.array([self.Z_prime[i, idx] for i, idx in enumerate(np.argmin(self.R_prime, axis=1))])

        self.R_geo = 0.5*(self.R_max+self.R_min)
        self.a = 0.5*(self.R_max-self.R_min)
        self.a_inv = 1./self.a

    def calc_shaping_parameters(self):
        """ Calculate the shaping parameters as described in Luce.
        """
        # elongation (kappa) and triangularity (delta) #
        self.kappa_u = self.a_inv*(self.Z_max-self.Z_Rmax)
        self.kappa_l = self.a_inv*(self.Z_Rmax-self.Z_min)
        self.delta_u = self.a_inv*(self.R_geo-self.R_Zmax)
        self.delta_l = self.a_inv*(self.R_geo-self.R_Zmin)

        # ellipse parameters for calculating squareness (zeta) #
        self.A_uo = self.R_max-self.R_Zmax
        self.A_ui = self.R_Zmax-self.R_min
        self.A_li = self.R_Zmin-self.R_min
        self.A_lo = self.R_max-self.R_Zmin

        self.B_uo = self.Z_max-self.Z_Rmax
        self.B_ui = self.Z_max-self.Z_Rmin
        self.B_li = self.Z_Rmin-self.Z_min
        self.B_lo = self.Z_Rmax-self.Z_min

        c1 = .5*np.sqrt(2)
        c2 = 1-c1

        # upper-outer quadrant segments #
        PO_uo = np.stack((self.R_Zmax, self.Z_Rmax), axis=1)
        PE_uo = np.stack((self.R_max, self.Z_max), axis=1)
        PC_uo = np.stack((c1*self.R_max+c2*self.R_Zmax, c1*self.Z_max+c2*self.Z_Rmax), axis=1)

        LOC_uo = np.linalg.norm(PC_uo-PO_uo, axis=1)
        LCE_uo = np.linalg.norm(PE_uo-PC_uo, axis=1)

        # lower-outer quadrant segments #
        PO_lo = np.stack((self.R_Zmin, self.Z_Rmax), axis=1)
        PE_lo = np.stack((self.R_max, self.Z_min), axis=1)
        PC_lo = np.stack((c1*self.R_max+c2*self.R_Zmin, c1*self.Z_min+c2*self.Z_Rmax), axis=1)

        LOC_lo = np.linalg.norm(PC_lo-PO_lo, axis=1)
        LCE_lo = np.linalg.norm(PE_lo-PC_lo, axis=1)

        # lower-inner quadrant segments #
        PO_li = np.stack((self.R_Zmin, self.Z_Rmin), axis=1)
        PE_li = np.stack((self.R_min, self.Z_min), axis=1)
        PC_li = np.stack((c1*self.R_min+c2*self.R_Zmin, c1*self.Z_min+c2*self.Z_Rmin), axis=1)

        LOC_li = np.linalg.norm(PC_li-PO_li, axis=1)
        LCE_li = np.linalg.norm(PE_li-PC_li, axis=1)

        # upper-inner quadrant segments #
        PO_ui = np.stack((self.R_Zmax, self.Z_Rmin), axis=1)
        PE_ui = np.stack((self.R_min, self.Z_max), axis=1)
        PC_ui = np.stack((c1*self.R_min+c2*self.R_Zmax, c1*self.Z_max+c2*self.Z_Rmin), axis=1)

        LOC_ui = np.linalg.norm(PC_ui-PO_ui, axis=1)
        LCE_ui = np.linalg.norm(PE_ui-PC_ui, axis=1)

        # generate squareness arrays #
        self.zeta_uo = np.empty(self.tpts)
        self.zeta_lo = np.empty(self.tpts)
        self.zeta_li = np.empty(self.tpts)
        self.zeta_ui = np.empty(self.tpts)

        # loop over toroidal angles #
        for i in range(self.tpts):
            # upper-outer squareness #
            i_beg = np.argmax(self.R_prime[i])
            i_end = np.argmax(self.Z_prime[i])+1
            if i_end < i_beg:
                R_surf = np.r_[self.R_prime[i, i_beg::], self.R_prime[i, 0:i_end]]
                Z_surf = np.r_[self.Z_prime[i, i_beg::], self.Z_prime[i, 0:i_end]]
            else:
                R_surf = self.R_prime[i, i_beg:i_end]
                Z_surf = self.Z_prime[i, i_beg:i_end]
            Z_surf, unique_i = np.unique(Z_surf, return_index=True)
            R_surf = R_surf[unique_i]
            R_fit = CubicSpline(Z_surf, R_surf)
            f = lambda z: R_fit(z) - (self.A_uo[i]/self.B_uo[i])*(z-self.Z_max[i]) - self.R_max[i]
            root = brentq(f, self.Z_Rmax[i], self.Z_max[i])
            Z_root, R_root = root, R_fit(root)
            PD_uo = np.array([R_root, Z_root])
            LOD_uo = np.linalg.norm(PD_uo-PO_uo[i])
            self.zeta_uo[i] = (LOD_uo-LOC_uo[i])/LCE_uo[i]

            # lower-outer squareness #
            i_beg = np.argmin(self.Z_prime[i])
            i_end = np.argmax(self.R_prime[i])+1
            if i_end < i_beg:
                R_surf = np.r_[self.R_prime[i, i_beg::], self.R_prime[i, 0:i_end]]
                Z_surf = np.r_[self.Z_prime[i, i_beg::], self.Z_prime[i, 0:i_end]]
            else:
                R_surf = self.R_prime[i, i_beg:i_end]
                Z_surf = self.Z_prime[i, i_beg:i_end]
            Z_surf, unique_i = np.unique(Z_surf, return_index=True)
            R_surf = R_surf[unique_i]
            R_fit = CubicSpline(Z_surf, R_surf)
            f = lambda z: R_fit(z) + (self.A_lo[i]/self.B_lo[i])*(z-self.Z_min[i]) - self.R_max[i]
            root = brentq(f, self.Z_min[i], self.Z_Rmax[i])
            Z_root, R_root = root, R_fit(root)
            PD_lo = np.array([R_root, Z_root])
            LOD_lo = np.linalg.norm(PD_lo-PO_lo[i])
            self.zeta_lo[i] = (LOD_lo-LOC_lo[i])/LCE_lo[i]

            # lower-inner squareness #
            i_beg = np.argmin(self.R_prime[i])
            i_end = np.argmin(self.Z_prime[i])+1
            if i_end < i_beg:
                R_surf = np.r_[self.R_prime[i, i_beg::], self.R_prime[i, 0:i_end]]
                Z_surf = np.r_[self.Z_prime[i, i_beg::], self.Z_prime[i, 0:i_end]]
            else:
                R_surf = self.R_prime[i, i_beg:i_end]
                Z_surf = self.Z_prime[i, i_beg:i_end]
            Z_surf, unique_i = np.unique(Z_surf, return_index=True)
            R_surf = R_surf[unique_i]
            R_fit = CubicSpline(Z_surf, R_surf)
            f = lambda z: R_fit(z) - (self.A_li[i]/self.B_li[i])*(z-self.Z_min[i]) - self.R_min[i]
            root = brentq(f, self.Z_min[i], self.Z_Rmin[i])
            Z_root, R_root = root, R_fit(root)
            PD_li = np.array([R_root, Z_root])
            LOD_li = np.linalg.norm(PD_li-PO_li[i])
            self.zeta_li[i] = (LOD_li-LOC_li[i])/LCE_li[i]

            # upper-inner squareness #
            i_beg = np.argmax(self.Z_prime[i])
            i_end = np.argmin(self.R_prime[i])+1
            if i_end < i_beg:
                R_surf = np.r_[self.R_prime[i, i_beg::], self.R_prime[i, 0:i_end]]
                Z_surf = np.r_[self.Z_prime[i, i_beg::], self.Z_prime[i, 0:i_end]]
            else:
                R_surf = self.R_prime[i, i_beg:i_end]
                Z_surf = self.Z_prime[i, i_beg:i_end]
            Z_surf, unique_i = np.unique(Z_surf, return_index=True)
            R_surf = R_surf[unique_i]
            R_fit = CubicSpline(Z_surf, R_surf)
            f = lambda z: R_fit(z) + (self.A_ui[i]/self.B_ui[i])*(z-self.Z_max[i]) - self.R_min[i]
            root = brentq(f, self.Z_Rmin[i], self.Z_max[i])
            Z_root, R_root = root, R_fit(root)
            PD_ui = np.array([R_root, Z_root])
            LOD_ui = np.linalg.norm(PD_ui-PO_ui[i])
            self.zeta_ui[i] = (LOD_ui-LOC_ui[i])/LCE_ui[i]

        # average shaping parameters #
        self.kappa = 0.5*(self.kappa_u+self.kappa_l)
        self.delta = 0.5*(self.delta_u+self.delta_l)
        self.zeta = 0.25*(self.zeta_uo+self.zeta_lo+self.zeta_li+self.zeta_ui)

        integ_norm = 1./(self.tor_dom[-1]-self.tor_dom[0])
        self.kappa_avg = integ_norm*np.trapz(self.kappa, self.tor_dom)
        self.delta_avg = integ_norm*np.trapz(self.delta, self.tor_dom)
        self.zeta_avg = integ_norm*np.trapz(self.zeta, self.tor_dom)

    def plot_define(self, fontsize=20, labelsize=24, linewidth=2):
        """ Define the plotting parameters.

        Parameters
        ----------
        fontsize : int
            Size of the font.
        labelsize : int
            Size of the labels.
        linewidth : int
            Parameter defining the width of the lines.
        """
        plt.close('all')

        font = {'family': 'sans-serif',
                'weight': 'normal',
                'size': fontsize}

        mpl.rc('font', **font)

        mpl.rcParams['axes.labelsize'] = labelsize
        mpl.rcParams['lines.linewidth'] = linewidth

    def plot_rotated_shapes(self, save_path=None, upper_outer=False, lower_outer=False, lower_inner=False, upper_inner=False):
        """ Plot the rotated and unrotated poloidal cross-sections, and show the R and Z basis
        vectors of the rotated reference frame in the unrotated cross-sections.

        Parameters
        ----------
        save_path : str, optional
            Global path to where the figure will be save. If None, then vector graphic will be
            generated with plt.show(). Default is None.

        upper_outer : bool, optional
            If True, plot the shaping points and line segments in the upper-outer quadrant. Default is False.

        lower_outer : bool, optional
            If True, plot the shaping points and line segments in the lower-outer quadrant. Default is False.

        lower_inner : bool, optional
            If True, plot the shaping points and line segments in the lower-inner quadrant. Default is False.

        upper_inner : bool, optional
            If True, plot the shaping points and line segments in the upper-inner quadrant. Default is False.
        """
        # define plotting parameters #
        self.plot_define()
        fig, axs = plt.subplots(2, 3, tight_layout=True, figsize=(14, 10))

        # set aspect ratio #
        axs[0,0].set_aspect('equal')
        axs[0,1].set_aspect('equal')
        axs[0,2].set_aspect('equal')
        axs[1,0].set_aspect('equal')
        axs[1,1].set_aspect('equal')
        axs[1,2].set_aspect('equal')

        # plot data #
        indices = np.array([0, .5*(self.tpts-1), (self.tpts-1)], dtype=int)
        # indices = np.array([0.0*(self.tpts-1), .125*(self.tpts-1), .25*(self.tpts-1)], dtype=int)
        norm = 0.5*self.minor*np.sqrt(self.psiN)
        for i, idx in enumerate(indices):
            axs[0,i].plot(self.R[idx,:], self.Z[idx,:], c='k')
            axs[1,i].plot(self.R_prime[idx,:], self.Z_prime[idx,:], c='k')

            # plot rotated basis vecors #
            ma = np.array([self.R_ma[idx], self.Z_ma[idx]])
            mat = np.linalg.inv(self.rot_matrix[idx])
            R_basis = np.matmul(mat, np.array([norm, 0]))
            Z_basis = np.matmul(mat, np.array([0, norm]))
            R_pnt = ma + R_basis
            Z_pnt = ma + Z_basis
            axs[0, i].plot([ma[0], R_pnt[0]], [ma[1], R_pnt[1]], c='tab:orange')
            axs[0, i].plot([ma[0], Z_pnt[0]], [ma[1], Z_pnt[1]], c='tab:blue')

            # plot extrema points #
            R_max, R_min = self.R_max[idx], self.R_min[idx]
            Z_max, Z_min = self.Z_max[idx], self.Z_min[idx]
            R_Zmax, R_Zmin = self.R_Zmax[idx], self.R_Zmin[idx]
            Z_Rmax, Z_Rmin = self.Z_Rmax[idx], self.Z_Rmin[idx]
            axs[1,i].scatter([R_Zmax], [Z_max], c='tab:blue', marker='^', s=150, zorder=10)
            axs[1,i].scatter([R_max], [Z_Rmax], c='tab:orange', marker='>', s=150, zorder=10)
            axs[1,i].scatter([R_Zmin], [Z_min], c='tab:blue', marker='v', s=150, zorder=10)
            axs[1,i].scatter([R_min], [Z_Rmin], c='tab:orange', marker='<', s=150, zorder=10)

            # plot shaping tools #
            if upper_outer:
                # plot quadrant box #
                axs[1,i].plot([self.R_Zmax[idx], self.R_Zmax[idx]], [self.Z_Rmax[idx], self.Z_max[idx]], c='k')
                axs[1,i].plot([self.R_Zmax[idx], self.R_max[idx]], [self.Z_max[idx], self.Z_max[idx]], c='k')
                axs[1,i].plot([self.R_max[idx], self.R_max[idx]], [self.Z_max[idx], self.Z_Rmax[idx]], c='k')
                axs[1,i].plot([self.R_max[idx], self.R_Zmax[idx]], [self.Z_Rmax[idx], self.Z_Rmax[idx]], c='k')
                axs[1,i].plot([self.R_Zmax[idx], self.R_max[idx]], [self.Z_Rmax[idx], self.Z_max[idx]], c='k')

                # plot ellipse #
                Z_ell = np.linspace(self.Z_Rmax[idx], self.Z_max[idx], 50)
                R_ell = self.R_Zmax[idx] + self.A_uo[idx]*np.sqrt(1 - ((Z_ell-self.Z_Rmax[idx])/self.B_uo[idx])**2)
                axs[1,i].plot(R_ell, Z_ell, c='k', ls='--')

                # plot intersection with ellipse #
                c1 = .5*np.sqrt(2)
                c2 = 1-c1
                Z_int = c1*self.Z_max[idx]+c2*self.Z_Rmax[idx]
                R_int = c1*self.R_max[idx]+c2*self.R_Zmax[idx]
                axs[1,i].scatter([R_int], [Z_int], edgecolor='tab:green', facecolor='None', s=50, zorder=20)

                # plot intersection with flux surface #
                idx_beg = np.argmax(self.R_prime[idx])
                idx_end = np.argmax(self.Z_prime[idx])+1
                if idx_end < idx_beg:
                    R_surf = np.r_[self.R_prime[idx, idx_beg::], self.R_prime[idx, 0:idx_end]]
                    Z_surf = np.r_[self.Z_prime[idx, idx_beg::], self.Z_prime[idx, 0:idx_end]]
                else:
                    R_surf = self.R_prime[idx, idx_beg:idx_end]
                    Z_surf = self.Z_prime[idx, idx_beg:idx_end]
                Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
                R_surf = R_surf[unique_idx]
                R_fit = CubicSpline(Z_surf, R_surf)
                f = lambda z: R_fit(z) - (self.A_uo[idx]/self.B_uo[idx])*(z-self.Z_max[idx]) - self.R_max[idx]
                root = brentq(f, self.Z_Rmax[idx], self.Z_max[idx])
                Z_root, R_root = root, R_fit(root)
                axs[1,i].scatter(R_root, Z_root, c='tab:red', s=50, zorder=15)

            if lower_outer:
                # plot quadrant box #
                axs[1,i].plot([self.R_Zmin[idx], self.R_max[idx]], [self.Z_Rmax[idx], self.Z_Rmax[idx]], c='k')
                axs[1,i].plot([self.R_max[idx], self.R_max[idx]], [self.Z_Rmax[idx], self.Z_min[idx]], c='k')
                axs[1,i].plot([self.R_max[idx], self.R_Zmin[idx]], [self.Z_min[idx], self.Z_min[idx]], c='k')
                axs[1,i].plot([self.R_Zmin[idx], self.R_Zmin[idx]], [self.Z_min[idx], self.Z_Rmax[idx]], c='k')
                axs[1,i].plot([self.R_Zmin[idx], self.R_max[idx]], [self.Z_Rmax[idx], self.Z_min[idx]], c='k')

                # plot ellipse #
                Z_ell = np.linspace(self.Z_min[idx], self.Z_Rmax[idx], 50)
                R_ell = self.R_Zmin[idx] + self.A_lo[idx]*np.sqrt(1 - ((self.Z_Rmax[idx]-Z_ell)/self.B_lo[idx])**2)
                axs[1,i].plot(R_ell, Z_ell, c='k', ls='--')

                # plot intersection with ellipse #
                c1 = .5*np.sqrt(2)
                c2 = 1-c1
                Z_int = c1*self.Z_min[idx]+c2*self.Z_Rmax[idx]
                R_int = c1*self.R_max[idx]+c2*self.R_Zmin[idx]
                axs[1,i].scatter([R_int], [Z_int], edgecolor='tab:green', facecolor='None', s=50, zorder=20)

                # plot intersection with flux surface #
                idx_beg = np.argmin(self.Z_prime[idx])
                idx_end = np.argmax(self.R_prime[idx])+1
                if idx_end < idx_beg:
                    R_surf = np.r_[self.R_prime[idx, idx_beg::], self.R_prime[idx, 0:idx_end]]
                    Z_surf = np.r_[self.Z_prime[idx, idx_beg::], self.Z_prime[idx, 0:idx_end]]
                else:
                    R_surf = self.R_prime[idx, idx_beg:idx_end]
                    Z_surf = self.Z_prime[idx, idx_beg:idx_end]
                Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
                R_surf = R_surf[unique_idx]
                R_fit = CubicSpline(Z_surf, R_surf)
                f = lambda z: R_fit(z) + (self.A_lo[idx]/self.B_lo[idx])*(z-self.Z_min[idx]) - self.R_max[idx]
                root = brentq(f, self.Z_min[idx], self.Z_Rmax[idx])
                Z_root, R_root = root, R_fit(root)
                axs[1,i].scatter(R_root, Z_root, c='tab:red', s=50, zorder=15)

            if lower_inner:
                # plot quadrant box #
                axs[1,i].plot([self.R_Zmin[idx], self.R_min[idx]], [self.Z_Rmin[idx], self.Z_Rmin[idx]], c='k')
                axs[1,i].plot([self.R_min[idx], self.R_min[idx]], [self.Z_Rmin[idx], self.Z_min[idx]], c='k')
                axs[1,i].plot([self.R_min[idx], self.R_Zmin[idx]], [self.Z_min[idx], self.Z_min[idx]], c='k')
                axs[1,i].plot([self.R_Zmin[idx], self.R_Zmin[idx]], [self.Z_min[idx], self.Z_Rmin[idx]], c='k')
                axs[1,i].plot([self.R_Zmin[idx], self.R_min[idx]], [self.Z_Rmin[idx], self.Z_min[idx]], c='k')

                # plot ellipse #
                Z_ell = np.linspace(self.Z_min[idx], self.Z_Rmin[idx], 50)
                R_ell = self.R_Zmin[idx] - self.A_li[idx]*np.sqrt(1 - ((self.Z_Rmin[idx]-Z_ell)/self.B_li[idx])**2)
                axs[1,i].plot(R_ell, Z_ell, c='k', ls='--')

                # plot intersection with ellipse #
                c1 = .5*np.sqrt(2)
                c2 = 1-c1
                Z_int = c1*self.Z_min[idx]+c2*self.Z_Rmin[idx]
                R_int = c1*self.R_min[idx]+c2*self.R_Zmin[idx]
                axs[1,i].scatter([R_int], [Z_int], edgecolor='tab:green', facecolor='None', s=50, zorder=20)

                # plot intersection with flux surface #
                idx_beg = np.argmin(self.R_prime[idx])
                idx_end = np.argmin(self.Z_prime[idx])+1
                if idx_end < idx_beg:
                    R_surf = np.r_[self.R_prime[idx, idx_beg::], self.R_prime[idx, 0:idx_end]]
                    Z_surf = np.r_[self.Z_prime[idx, idx_beg::], self.Z_prime[idx, 0:idx_end]]
                else:
                    R_surf = self.R_prime[idx, idx_beg:idx_end]
                    Z_surf = self.Z_prime[idx, idx_beg:idx_end]
                Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
                R_surf = R_surf[unique_idx]
                R_fit = CubicSpline(Z_surf, R_surf)
                f = lambda z: R_fit(z) - (self.A_li[idx]/self.B_li[idx])*(z-self.Z_min[idx]) - self.R_min[idx]
                root = brentq(f, self.Z_min[idx], self.Z_Rmin[idx])
                Z_root, R_root = root, R_fit(root)
                axs[1,i].scatter(R_root, Z_root, c='tab:red', s=50, zorder=15)

            if upper_inner:
                # plot quadrant box #
                axs[1,i].plot([self.R_Zmax[idx], self.R_min[idx]], [self.Z_Rmin[idx], self.Z_Rmin[idx]], c='k')
                axs[1,i].plot([self.R_min[idx], self.R_min[idx]], [self.Z_Rmin[idx], self.Z_max[idx]], c='k')
                axs[1,i].plot([self.R_min[idx], self.R_Zmax[idx]], [self.Z_max[idx], self.Z_max[idx]], c='k')
                axs[1,i].plot([self.R_Zmax[idx], self.R_Zmax[idx]], [self.Z_max[idx], self.Z_Rmin[idx]], c='k')
                axs[1,i].plot([self.R_Zmax[idx], self.R_min[idx]], [self.Z_Rmin[idx], self.Z_max[idx]], c='k')

                # plot ellipse #
                Z_ell = np.linspace(self.Z_Rmin[idx], self.Z_max[idx], 50)
                R_ell = self.R_Zmax[idx] - self.A_ui[idx]*np.sqrt(1 - ((Z_ell-self.Z_Rmin[idx])/self.B_ui[idx])**2)
                axs[1,i].plot(R_ell, Z_ell, c='k', ls='--')

                # plot intersection with ellipse #
                c1 = .5*np.sqrt(2)
                c2 = 1-c1
                Z_int = c1*self.Z_max[idx]+c2*self.Z_Rmin[idx]
                R_int = c1*self.R_min[idx]+c2*self.R_Zmax[idx]
                axs[1,i].scatter([R_int], [Z_int], edgecolor='tab:green', facecolor='None', s=50, zorder=20)

                # plot intersection with flux surface #
                idx_beg = np.argmax(self.Z_prime[idx])
                idx_end = np.argmin(self.R_prime[idx])+1
                if idx_end < idx_beg:
                    R_surf = np.r_[self.R_prime[idx, idx_beg::], self.R_prime[idx, 0:idx_end]]
                    Z_surf = np.r_[self.Z_prime[idx, idx_beg::], self.Z_prime[idx, 0:idx_end]]
                else:
                    R_surf = self.R_prime[idx, idx_beg:idx_end]
                    Z_surf = self.Z_prime[idx, idx_beg:idx_end]
                Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
                R_surf = R_surf[unique_idx]
                R_fit = CubicSpline(Z_surf, R_surf)
                f = lambda z: R_fit(z) + (self.A_ui[idx]/self.B_ui[idx])*(z-self.Z_max[idx]) - self.R_min[idx]
                root = brentq(f, self.Z_Rmin[idx], self.Z_max[idx])
                Z_root, R_root = root, R_fit(root)
                axs[1,i].scatter(R_root, Z_root, c='tab:red', s=50, zorder=15)

        # label axes #
        axs[0, 0].set_ylabel(r'$Z/m$')
        axs[0, 0].set_xlabel(r'$R/m$')
        axs[0, 1].set_xlabel(r'$R/m$')
        axs[0, 2].set_xlabel(r'$R/m$')

        axs[1, 0].set_ylabel(r'$Z^{\prime}/m$')
        axs[1, 0].set_xlabel(r'$R^{\prime}/m$')
        axs[1, 1].set_xlabel(r'$R^{\prime}/m$')
        axs[1, 2].set_xlabel(r'$R^{\prime}/m$')

        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

    def plot_shaping_parameters(self, save_path=None):
        # define plotting parameters #
        self.plot_define()
        fig, axs = plt.subplots(3, 1, sharex=True, tight_layout=True, figsize=(10, 8))

        # plot data #
        axs[0].plot(self.tor_dom/np.pi, self.kappa_u, c='tab:blue', ls=':', label=r'$\kappa_u$')
        axs[0].plot(self.tor_dom/np.pi, self.kappa_l, c='tab:blue', ls='--', label=r'$\kappa_l$')
        axs[0].plot(self.tor_dom/np.pi, .5*(self.kappa_l+self.kappa_u), c='tab:blue')

        axs[1].plot(self.tor_dom/np.pi, self.delta_u, c='tab:orange', ls=':', label=r'$\delta_u$')
        axs[1].plot(self.tor_dom/np.pi, self.delta_l, c='tab:orange', ls='--', label=r'$\delta_l$')
        axs[1].plot(self.tor_dom/np.pi, .5*(self.delta_l+self.delta_u), c='tab:orange')

        axs[2].plot(self.tor_dom/np.pi, self.zeta_uo, c='tab:green', ls=(0, (3, 1, 1, 1, 1, 1)), label=r'$\zeta_{uo}$')
        axs[2].plot(self.tor_dom/np.pi, self.zeta_lo, c='tab:green', ls='--', label=r'$\zeta_{lo}$')
        axs[2].plot(self.tor_dom/np.pi, self.zeta_li, c='tab:green', ls='-.', label=r'$\zeta_{li}$')
        axs[2].plot(self.tor_dom/np.pi, self.zeta_ui, c='tab:green', ls=':', label=r'$\zeta_{ui}$')
        zeta_avg = .25*(self.zeta_uo+self.zeta_lo+self.zeta_li+self.zeta_ui)
        axs[2].plot(self.tor_dom/np.pi, zeta_avg, c='tab:green')

        # axis labels #
        axs[2].set_xlabel(r'$\varphi/\pi$')
        axs[0].set_title(r'$\langle \kappa \rangle={0:0.2f}$'.format(self.kappa_avg))
        axs[1].set_title(r'$\langle \delta \rangle={0:0.2f}$'.format(self.delta_avg))
        axs[2].set_title(r'$\langle \zeta \rangle={0:0.2f}$'.format(self.zeta_avg))

        # axis limits #
        axs[0].set_ylim(0, axs[0].get_ylim()[1])
        axs[0].set_xlim(0, self.tor_dom[-1]/np.pi)
        axs[1].set_xlim(0, self.tor_dom[-1]/np.pi)
        axs[2].set_xlim(0, self.tor_dom[-1]/np.pi)
        axs[2].set_ylim(axs[2].get_ylim()[0], 1.5*axs[2].get_ylim()[1])

        # axis legend/grid #
        axs[0].legend(frameon=False, fontsize=16)
        axs[1].legend(frameon=False, fontsize=16)
        axs[2].legend(frameon=False, fontsize=16, loc='upper left', ncol=4)

        axs[0].grid()
        axs[1].grid()
        axs[2].grid()

        if save_path is None:
            plt.show()
        else:
            plt.savefig(save_path)

    def init_animation(self, tor_ang=0., animate=True):
        # define plotting parameters #
        self.plot_define(fontsize=14, labelsize=16)
        fig = plt.figure(figsize=(10, 10))
        gs = GridSpec(5, 3, hspace=1., wspace=1.)
        sub_gs = gs[2::, :].subgridspec(3, 2, hspace=0.2)

        ax1 = fig.add_subplot(gs[0:2, 0:2])
        ax2 = fig.add_subplot(gs[0:2, 2])
        ax3 = fig.add_subplot(sub_gs[0, 0:3])
        ax4 = fig.add_subplot(sub_gs[1, 0:3])
        ax5 = fig.add_subplot(sub_gs[2, 0:3])
        self.axes = {'ax1': ax1,
                     'ax2': ax2,
                     'ax3': ax3,
                     'ax4': ax4,
                     'ax5': ax5}

        ax3.set_xticklabels([])
        ax4.set_xticklabels([])

        ax1.set_aspect('equal')
        ax2.set_aspect('equal')

        idx = np.argmin(np.abs(self.tor_dom - tor_ang))
        ani_dict = {}

        # magnetic axis in rotated reference frame #
        ma_rots = np.empty((self.tpts, 2))
        for i in range(self.tpts):
            ma_rots[i] = np.matmul(self.rot_matrix[i], np.array([self.R_ma[i], self.Z_ma[i]]))
        ani_dict['Rotated MA'] = ma_rots
        
        # plot |B| distribution #
        ani_dict['mod B'] = ax1.pcolormesh(self.R_grid[:,0,:], self.Z_grid[:,0,:], self.B_grid[:,0,:], cmap='jet', vmin=np.min(self.B_grid), vmax=np.max(self.B_grid), shading='gouraud')
        cbar = fig.colorbar(ani_dict['mod B'], ax=ax1)
        cbar.ax.set_ylabel(r'$B/T$')

        # plot flux surface with basis vectors #
        norm = 0.25*self.minor*np.sqrt(self.psiN)
        ma = np.array([self.R_ma[idx], self.Z_ma[idx]])
        mat = np.linalg.inv(self.rot_matrix[idx])
        R_basis = np.matmul(mat, np.array([norm, 0]))
        Z_basis = np.matmul(mat, np.array([0, norm]))
        R_pnt = ma + R_basis
        Z_pnt = ma + Z_basis
        ani_dict['flux surface'], = ax1.plot(self.R[idx,:], self.Z[idx,:], c='k')
        ani_dict['basis R'] = ax1.arrow(ma[0], ma[1], R_basis[0], R_basis[1], head_width=0.008, color='k', zorder=10)
        ani_dict['basis Z'] = ax1.arrow(ma[0], ma[1], Z_basis[0], Z_basis[1], head_width=0.008, color='k', zorder=10)

        # plot rotated flux surface with extrema points #
        ma_rot = ma_rots[idx]
        R_max, R_min = self.R_max[idx], self.R_min[idx]
        Z_max, Z_min = self.Z_max[idx], self.Z_min[idx]
        R_Zmax, R_Zmin = self.R_Zmax[idx], self.R_Zmin[idx]
        Z_Rmax, Z_Rmin = self.Z_Rmax[idx], self.Z_Rmin[idx]
        ani_dict['rotated flux surface'], = ax2.plot((self.R_prime[idx,:]-ma_rot[0])*1e2, (self.Z_prime[idx,:]-ma_rot[1])*1e2, c='k')
        ani_dict['Z max'] = ax2.scatter([(R_Zmax-ma_rot[0])*1e2], [(Z_max-ma_rot[1])*1e2], c='tab:blue', marker='^', s=150, zorder=10)
        ani_dict['R max'] = ax2.scatter([(R_max-ma_rot[0])*1e2], [(Z_Rmax-ma_rot[1])*1e2], c='tab:orange', marker='>', s=150, zorder=10)
        ani_dict['Z min'] = ax2.scatter([(R_Zmin-ma_rot[0])*1e2], [(Z_min-ma_rot[1])*1e2], c='tab:blue', marker='v', s=150, zorder=10)
        ani_dict['R min'] = ax2.scatter([(R_min-ma_rot[0])*1e2], [(Z_Rmin-ma_rot[1])*1e2], c='tab:orange', marker='<', s=150, zorder=10)

        # plot upper_outer and lower_outer quadrant boxes #
        ani_dict['UO Box 1'], = ax2.plot([(self.R_Zmax[idx]-ma_rot[0])*1e2, (self.R_Zmax[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_max[idx]-ma_rot[1])*1e2], c='k', lw=1)
        ani_dict['UO Box 2'], = ax2.plot([(self.R_Zmax[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_max[idx]-ma_rot[1])*1e2, (self.Z_max[idx]-ma_rot[1])*1e2], c='k', lw=1)
        ani_dict['UO Box 3'], = ax2.plot([(self.R_max[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_max[idx]-ma_rot[1])*1e2, (self.Z_Rmax[idx]-ma_rot[1])*1e2], c='k', lw=1)
        ani_dict['UO Box 4'], = ax2.plot([(self.R_max[idx]-ma_rot[0])*1e2, (self.R_Zmax[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_Rmax[idx]-ma_rot[1])*1e2], c='k', lw=1)
        ani_dict['UO Box 5'], = ax2.plot([(self.R_Zmax[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_max[idx]-ma_rot[1])*1e2], c='k', lw=1)

        ani_dict['LO Box 1'], = ax2.plot([(self.R_Zmin[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_Rmax[idx]-ma_rot[1])*1e2], c='k', lw=1)
        ani_dict['LO Box 2'], = ax2.plot([(self.R_max[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_min[idx]-ma_rot[1])*1e2], c='k', lw=1)
        ani_dict['LO Box 3'], = ax2.plot([(self.R_max[idx]-ma_rot[0])*1e2, (self.R_Zmin[idx]-ma_rot[0])*1e2], [(self.Z_min[idx]-ma_rot[1])*1e2, (self.Z_min[idx]-ma_rot[1])*1e2], c='k', lw=1)
        ani_dict['LO Box 4'], = ax2.plot([(self.R_Zmin[idx]-ma_rot[0])*1e2, (self.R_Zmin[idx]-ma_rot[0])*1e2], [(self.Z_min[idx]-ma_rot[1])*1e2, (self.Z_Rmax[idx]-ma_rot[1])*1e2], c='k', lw=1)
        ani_dict['LO Box 5'], = ax2.plot([(self.R_Zmin[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_min[idx]-ma_rot[1])*1e2], c='k', lw=1)

        # plot upper_outer and lower_outer ellipse #
        Z_ell = np.linspace(self.Z_Rmax[idx], self.Z_max[idx], 50)
        R_ell = self.R_Zmax[idx] + self.A_uo[idx]*np.sqrt(1 - ((Z_ell-self.Z_Rmax[idx])/self.B_uo[idx])**2)
        ani_dict['UO Ellipse'], = ax2.plot((R_ell-ma_rot[0])*1e2, (Z_ell-ma_rot[1])*1e2, c='k', ls='--', lw=1)

        Z_ell = np.linspace(self.Z_min[idx], self.Z_Rmax[idx], 50)
        R_ell = self.R_Zmin[idx] + self.A_lo[idx]*np.sqrt(1 - ((self.Z_Rmax[idx]-Z_ell)/self.B_lo[idx])**2)
        ani_dict['LO Ellipse'], = ax2.plot((R_ell-ma_rot[0])*1e2, (Z_ell-ma_rot[1])*1e2, c='k', ls='--', lw=1)

        # plot upper_outer and lower_outer intersection with ellipse #
        c1 = .5*np.sqrt(2)
        c2 = 1-c1

        Z_int = c1*self.Z_max[idx]+c2*self.Z_Rmax[idx]
        R_int = c1*self.R_max[idx]+c2*self.R_Zmax[idx]
        ani_dict['UO Ell Intersection'] = ax2.scatter([(R_int-ma_rot[0])*1e2], [(Z_int-ma_rot[1])*1e2], edgecolor='tab:green', facecolor='None', linewidth=2, s=50, zorder=20)

        Z_int = c1*self.Z_min[idx]+c2*self.Z_Rmax[idx]
        R_int = c1*self.R_max[idx]+c2*self.R_Zmin[idx]
        ani_dict['LO Ell Intersection'] = ax2.scatter([(R_int-ma_rot[0])*1e2], [(Z_int-ma_rot[1])*1e2], edgecolor='tab:green', facecolor='None', linewidths=2, s=50, zorder=20)

        # plot upper_outer and inner_outer intersection with flux surface #
        idx_beg = np.argmax(self.R_prime[idx])
        idx_end = np.argmax(self.Z_prime[idx])+1
        if idx_end < idx_beg:
            R_surf = np.r_[self.R_prime[idx, idx_beg::], self.R_prime[idx, 0:idx_end]]
            Z_surf = np.r_[self.Z_prime[idx, idx_beg::], self.Z_prime[idx, 0:idx_end]]
        else:
            R_surf = self.R_prime[idx, idx_beg:idx_end]
            Z_surf = self.Z_prime[idx, idx_beg:idx_end]
        Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
        R_surf = R_surf[unique_idx]
        R_fit = CubicSpline(Z_surf, R_surf)
        f = lambda z: R_fit(z) - (self.A_uo[idx]/self.B_uo[idx])*(z-self.Z_max[idx]) - self.R_max[idx]
        root = brentq(f, self.Z_Rmax[idx], self.Z_max[idx])
        Z_root, R_root = root, R_fit(root)
        ani_dict['UO FS Intersection'] = ax2.scatter((R_root-ma_rot[0])*1e2, (Z_root-ma_rot[1])*1e2, c='tab:red', s=50, zorder=15)

        idx_beg = np.argmin(self.Z_prime[idx])
        idx_end = np.argmax(self.R_prime[idx])+1
        if idx_end < idx_beg:
            R_surf = np.r_[self.R_prime[idx, idx_beg::], self.R_prime[idx, 0:idx_end]]
            Z_surf = np.r_[self.Z_prime[idx, idx_beg::], self.Z_prime[idx, 0:idx_end]]
        else:
            R_surf = self.R_prime[idx, idx_beg:idx_end]
            Z_surf = self.Z_prime[idx, idx_beg:idx_end]
        Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
        R_surf = R_surf[unique_idx]
        R_fit = CubicSpline(Z_surf, R_surf)
        f = lambda z: R_fit(z) + (self.A_lo[idx]/self.B_lo[idx])*(z-self.Z_min[idx]) - self.R_max[idx]
        root = brentq(f, self.Z_min[idx], self.Z_Rmax[idx])
        Z_root, R_root = root, R_fit(root)
        ani_dict['LO FS Intersection'] = ax2.scatter((R_root-ma_rot[0])*1e2, (Z_root-ma_rot[1])*1e2, c='tab:red', s=50, zorder=15)

        tor_rng = [self.tor_dom[0]/np.pi, self.tor_dom[-1]/np.pi]
        # plot elongation #
        ax3.plot(self.tor_dom/np.pi, self.kappa, c='tab:blue', alpha=0.3)
        ax3.plot(tor_rng, [self.kappa_avg]*2, c='tab:blue', ls='--', label=r'$\overline{{\kappa}} = {0:0.2f}$'.format(self.kappa_avg))
        ani_dict['kappa'], = ax3.plot([], [], c='tab:blue')
        ani_dict['kappa dot'], = ax3.plot([], [], marker='o', markersize=10, mec='k', mfc='tab:blue', zorder=10)

        # plot triangularity #
        ax4.plot(self.tor_dom/np.pi, self.delta, c='tab:orange', alpha=0.3)
        ax4.plot(tor_rng, [self.delta_avg]*2, c='tab:orange', ls='--', label=r'$\overline{{\delta}} = {0:0.2f}$'.format(self.delta_avg))
        ani_dict['delta'], = ax4.plot(self.tor_dom/np.pi, self.delta, c='tab:orange')
        ani_dict['delta dot'], = ax4.plot([], [], marker='o', markersize=10, mec='k', mfc='tab:orange')

        # plot squareness #
        ax5.plot(self.tor_dom/np.pi, .5*(self.zeta_uo+self.zeta_lo), c='tab:green', alpha=0.3)
        ax5.plot(tor_rng, [self.zeta_avg]*2, c='tab:green', ls='--', label=r'$\overline{{\zeta}} = {0:0.2f}$'.format(self.zeta_avg))
        ani_dict['zeta'], = ax5.plot([], [], c='tab:green')
        ani_dict['zeta dot'], = ax5.plot([], [], marker='o', markersize=10, mec='k', mfc='tab:green')

        # axis limits #
        ax1.set_xlim(0.99*np.min(self.R), 1.01*np.max(self.R))
        ax1.set_ylim(1.01*np.min(self.Z), 1.01*np.max(self.Z))

        ax2.set_xlim(1.25*1e2*np.min(self.R_min-ma_rots[:,0]), 1.25*1e2*np.max(self.R_max-ma_rots[:,0]))
        ax2.set_ylim(1.1*1e2*np.min(self.Z_min-ma_rots[:,1]), 1.1*1e2*np.max(self.Z_max-ma_rots[:,1]))

        ax3.set_xlim(self.tor_dom[0]/np.pi, self.tor_dom[-1]/np.pi)
        ax4.set_xlim(self.tor_dom[0]/np.pi, self.tor_dom[-1]/np.pi)
        ax5.set_xlim(self.tor_dom[0]/np.pi, self.tor_dom[-1]/np.pi)

        delta_max = max(np.abs(ax4.get_ylim()))
        zeta_max = max(np.abs(ax5.get_ylim()))
        ax3.set_ylim(0, ax3.get_ylim()[1])
        ax4.set_ylim(-delta_max, delta_max)
        ax5.set_ylim(-zeta_max, zeta_max)

        # axis labels #
        ax1.set_xlabel(r'$R/\mathrm{m}$')
        ax1.set_ylabel(r'$Z/\mathrm{m}$')

        ax2.set_xlabel('$R^{\prime}/\mathrm{cm}$')
        ax2.set_ylabel('$Z^{\prime}/\mathrm{cm}$')

        ax3.set_ylabel(r'$\kappa$')
        ax4.set_ylabel(r'$\delta$')
        ax5.set_ylabel(r'$\zeta$')
        ax5.set_xlabel(r'$\varphi/\pi$')

        # axis legends #
        ax3.legend(loc='upper left', frameon=True, ncol=1)
        ax4.legend(loc='upper left', frameon=True, ncol=1)
        ax5.legend(loc='upper center', frameon=True, ncol=1)

        # axis grids #
        ax3.grid()
        ax4.grid()
        ax5.grid()

        self.fig = fig
        self.ani_dict = ani_dict
        if not animate:
            plt.show()

    def animation_frame(self, idx):
        # plot |B| distribution #
        self.ani_dict['mod B'].remove()
        self.ani_dict['mod B'] = self.axes['ax1'].pcolormesh(self.R_grid[:,idx,:], self.Z_grid[:,idx,:], self.B_grid[:,idx,:], cmap='jet', vmin=np.min(self.B_grid), vmax=np.max(self.B_grid), shading='gouraud')

        # plot flux surface with basis vectors #
        norm = 0.3*self.minor*np.sqrt(self.psiN)
        ma = np.array([self.R_ma[idx], self.Z_ma[idx]])
        mat = np.linalg.inv(self.rot_matrix[idx])
        R_basis = np.matmul(mat, np.array([norm, 0]))
        Z_basis = np.matmul(mat, np.array([0, norm]))
        R_pnt = ma + R_basis
        Z_pnt = ma + Z_basis
        self.ani_dict['flux surface'].set_data(self.R[idx,:], self.Z[idx,:])
        self.ani_dict['basis R'].set_data(x=ma[0], y=ma[1], dx=R_basis[0], dy=R_basis[1])
        self.ani_dict['basis Z'].set_data(x=ma[0], y=ma[1], dx=Z_basis[0], dy=Z_basis[1])

        # plot rotated flux surface with extrema points #
        ma_rot = self.ani_dict['Rotated MA'][idx]
        R_max, R_min = self.R_max[idx], self.R_min[idx]
        Z_max, Z_min = self.Z_max[idx], self.Z_min[idx]
        R_Zmax, R_Zmin = self.R_Zmax[idx], self.R_Zmin[idx]
        Z_Rmax, Z_Rmin = self.Z_Rmax[idx], self.Z_Rmin[idx]
        self.ani_dict['rotated flux surface'].set_data((self.R_prime[idx,:]-ma_rot[0])*1e2, (self.Z_prime[idx,:]-ma_rot[1])*1e2)
        self.ani_dict['Z max'].set_offsets([(R_Zmax-ma_rot[0])*1e2, (Z_max-ma_rot[1])*1e2])
        self.ani_dict['R max'].set_offsets([(R_max-ma_rot[0])*1e2, (Z_Rmax-ma_rot[1])*1e2])
        self.ani_dict['Z min'].set_offsets([(R_Zmin-ma_rot[0])*1e2, (Z_min-ma_rot[1])*1e2])
        self.ani_dict['R min'].set_offsets([(R_min-ma_rot[0])*1e2, (Z_Rmin-ma_rot[1])*1e2])

        # plot upper_outer and lower_outer quadrant boxes #
        self.ani_dict['UO Box 1'].set_data([(self.R_Zmax[idx]-ma_rot[0])*1e2, (self.R_Zmax[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_max[idx]-ma_rot[1])*1e2])
        self.ani_dict['UO Box 2'].set_data([(self.R_Zmax[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_max[idx]-ma_rot[1])*1e2, (self.Z_max[idx]-ma_rot[1])*1e2])
        self.ani_dict['UO Box 3'].set_data([(self.R_max[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_max[idx]-ma_rot[1])*1e2, (self.Z_Rmax[idx]-ma_rot[1])*1e2])
        self.ani_dict['UO Box 4'].set_data([(self.R_max[idx]-ma_rot[0])*1e2, (self.R_Zmax[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_Rmax[idx]-ma_rot[1])*1e2])
        self.ani_dict['UO Box 5'].set_data([(self.R_Zmax[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_max[idx]-ma_rot[1])*1e2])

        self.ani_dict['LO Box 1'].set_data([(self.R_Zmin[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_Rmax[idx]-ma_rot[1])*1e2])
        self.ani_dict['LO Box 2'].set_data([(self.R_max[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_min[idx]-ma_rot[1])*1e2])
        self.ani_dict['LO Box 3'].set_data([(self.R_max[idx]-ma_rot[0])*1e2, (self.R_Zmin[idx]-ma_rot[0])*1e2], [(self.Z_min[idx]-ma_rot[1])*1e2, (self.Z_min[idx]-ma_rot[1])*1e2])
        self.ani_dict['LO Box 4'].set_data([(self.R_Zmin[idx]-ma_rot[0])*1e2, (self.R_Zmin[idx]-ma_rot[0])*1e2], [(self.Z_min[idx]-ma_rot[1])*1e2, (self.Z_Rmax[idx]-ma_rot[1])*1e2])
        self.ani_dict['LO Box 5'].set_data([(self.R_Zmin[idx]-ma_rot[0])*1e2, (self.R_max[idx]-ma_rot[0])*1e2], [(self.Z_Rmax[idx]-ma_rot[1])*1e2, (self.Z_min[idx]-ma_rot[1])*1e2])

        # plot upper_outer and lower_outer ellipse #
        Z_ell = np.linspace(self.Z_Rmax[idx], self.Z_max[idx], 50)
        R_ell = self.R_Zmax[idx] + self.A_uo[idx]*np.sqrt(1 - ((Z_ell-self.Z_Rmax[idx])/self.B_uo[idx])**2)
        self.ani_dict['UO Ellipse'].set_data((R_ell-ma_rot[0])*1e2, (Z_ell-ma_rot[1])*1e2)

        Z_ell = np.linspace(self.Z_min[idx], self.Z_Rmax[idx], 50)
        R_ell = self.R_Zmin[idx] + self.A_lo[idx]*np.sqrt(1 - ((self.Z_Rmax[idx]-Z_ell)/self.B_lo[idx])**2)
        self.ani_dict['LO Ellipse'].set_data((R_ell-ma_rot[0])*1e2, (Z_ell-ma_rot[1])*1e2)

        # plot upper_outer and lower_outer intersection with ellipse #
        c1 = .5*np.sqrt(2)
        c2 = 1-c1

        Z_int = c1*self.Z_max[idx]+c2*self.Z_Rmax[idx]
        R_int = c1*self.R_max[idx]+c2*self.R_Zmax[idx]
        self.ani_dict['UO Ell Intersection'].set_offsets([(R_int-ma_rot[0])*1e2, (Z_int-ma_rot[1])*1e2])

        Z_int = c1*self.Z_min[idx]+c2*self.Z_Rmax[idx]
        R_int = c1*self.R_max[idx]+c2*self.R_Zmin[idx]
        self.ani_dict['LO Ell Intersection'].set_offsets([(R_int-ma_rot[0])*1e2, (Z_int-ma_rot[1])*1e2])

        # plot upper_outer and inner_outer intersection with flux surface #
        idx_beg = np.argmax(self.R_prime[idx])
        idx_end = np.argmax(self.Z_prime[idx])+1
        if idx_end < idx_beg:
            R_surf = np.r_[self.R_prime[idx, idx_beg::], self.R_prime[idx, 0:idx_end]]
            Z_surf = np.r_[self.Z_prime[idx, idx_beg::], self.Z_prime[idx, 0:idx_end]]
        else:
            R_surf = self.R_prime[idx, idx_beg:idx_end]
            Z_surf = self.Z_prime[idx, idx_beg:idx_end]
        Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
        R_surf = R_surf[unique_idx]
        R_fit = CubicSpline(Z_surf, R_surf)
        f = lambda z: R_fit(z) - (self.A_uo[idx]/self.B_uo[idx])*(z-self.Z_max[idx]) - self.R_max[idx]
        root = brentq(f, self.Z_Rmax[idx], self.Z_max[idx])
        Z_root, R_root = root, R_fit(root)
        self.ani_dict['UO FS Intersection'].set_offsets([(R_root-ma_rot[0])*1e2, (Z_root-ma_rot[1])*1e2])

        idx_beg = np.argmin(self.Z_prime[idx])
        idx_end = np.argmax(self.R_prime[idx])+1
        if idx_end < idx_beg:
            R_surf = np.r_[self.R_prime[idx, idx_beg::], self.R_prime[idx, 0:idx_end]]
            Z_surf = np.r_[self.Z_prime[idx, idx_beg::], self.Z_prime[idx, 0:idx_end]]
        else:
            R_surf = self.R_prime[idx, idx_beg:idx_end]
            Z_surf = self.Z_prime[idx, idx_beg:idx_end]
        Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
        R_surf = R_surf[unique_idx]
        R_fit = CubicSpline(Z_surf, R_surf)
        f = lambda z: R_fit(z) + (self.A_lo[idx]/self.B_lo[idx])*(z-self.Z_min[idx]) - self.R_max[idx]
        root = brentq(f, self.Z_min[idx], self.Z_Rmax[idx])
        Z_root, R_root = root, R_fit(root)
        self.ani_dict['LO FS Intersection'].set_offsets([(R_root-ma_rot[0])*1e2, (Z_root-ma_rot[1])*1e2])

        # plot elongation #
        self.ani_dict['kappa'].set_data(self.tor_dom[0:idx+1]/np.pi, self.kappa[0:idx+1])
        self.ani_dict['kappa dot'].set_data(self.tor_dom[idx]/np.pi, self.kappa[idx])

        # plot triangularity #
        self.ani_dict['delta'].set_data(self.tor_dom[0:idx+1]/np.pi, self.delta[0:idx+1])
        self.ani_dict['delta dot'].set_data(self.tor_dom[idx]/np.pi, self.delta[idx])

        # plot squareness #
        self.ani_dict['zeta'].set_data(self.tor_dom[0:idx+1]/np.pi, 0.5*(self.zeta_uo[0:idx+1]+self.zeta_lo[0:idx+1]))
        self.ani_dict['zeta dot'].set_data(self.tor_dom[idx]/np.pi, 0.5*(self.zeta_uo[idx]+self.zeta_lo[idx]))

        return self.ani_dict

    def make_animation(self, save_path, fps=10, save_frames=False):
        self.init_animation()
        anim = ani.FuncAnimation(self.fig, self.animation_frame, frames=self.tpts)
        if save_frames:
            # anim.save(save_path, writer='ffmpeg', fps=fps)
            anim.save(save_path, writer='imagemagick', fps=fps)
        else:
            anim.save(save_path, writer='imagemagick', fps=fps)

if __name__ == "__main__":
    # wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_3', 'job_84', 'wout_0-3-84_7p0_mn1824_ns101.nc')
    wout_path = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec', 'wout_0-3-84_11p0_mn1824_ns101.nc')
    wout = wr.readWout(wout_path)

    shape_calc = shape_calculator(wout, 0.5, 50)
    shape_calc.calc_shaping_parameters()
    shape_calc.plot_shaping_parameters()

    # print(shape_calc.kappa_avg)
    # print(shape_calc.delta_avg)
    # print(shape_calc.zeta_avg)

    """
    save_dir = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'figures')
    surf_fig = os.path.join(save_dir, '%s_shaping_rotations.png'%config_id)
    shape_fig = os.path.join(save_dir, '%s_shaping_parameters.png'%config_id)
    # shape_calc.plot_rotated_shapes(upper_outer=True, lower_outer=True, lower_inner=True, upper_inner=True)
    # shape_calc.plot_shaping_parameters()
    # shape_calc.animation_test_frame(0.*np.pi)
    # save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'APS', '2023', 'Figures', 'shaping_%s.mp4' % config_id)
    save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'APS', '2023', 'Figures', 'shape_frames', config_id, 'frame.png')
    shape_calc.make_animation(save_path, fps=15, save_frames=False)
    """
