import os, sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as ani

from matplotlib.gridspec import GridSpec, SubplotSpec
from scipy.interpolate import interp1d
from scipy.optimize import brentq

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import plot_define as pd
import vmecTools.wout_files.wout_read as wr
import metricTools.equilibrium_shapes as shapes 


config_id = '0-1-0'
main_id = 'main_coil_{}'.format(config_id.split('-')[0])
set_id = 'set_{}'.format(config_id.split('-')[1])
job_id = 'job_{}'.format(config_id.split('-')[2])
if config_id == '0-1-0':
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_QHS_mn1824_ns101.nc')
    config_name = 'QHS'
else:
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_%s_mn1824_ns101.nc' % config_id)
    config_name = config_id
wout = wr.readWout(wout_path)
es = shapes.shape_calculator(wout, 0.5, 101, half_grid=False, rotation_method='full helix')
es.calc_shaping_parameters()

# define plotting parameters #
es.plot_define(fontsize=14, labelsize=16)
fig = plt.figure(figsize=(10, 7))
gs = GridSpec(3, 3, hspace=0.5, wspace=0.8)

ax1 = fig.add_subplot(gs[0:2, 0:2])
ax2 = fig.add_subplot(gs[0:2, 2])
ax3 = fig.add_subplot(gs[2, 0::])
axes = {'ax1': ax1,
        'ax2': ax2,
        'ax3': ax3}

ax1.set_aspect('equal')
ax2.set_aspect('equal')

ani_dict = {}

# magnetic axis in rotated reference frame #
ma_rots = np.empty((es.tpts, 2))
for i in range(es.tpts):
    ma_rots[i] = np.matmul(es.rot_matrix[i], np.array([es.R_ma[i], es.Z_ma[i]]))
ani_dict['Rotated MA'] = ma_rots

# plot |B| distribution #
ani_dict['mod B'] = ax1.pcolormesh(es.R_grid[:,0,:], es.Z_grid[:,0,:], es.B_grid[:,0,:], cmap='jet', vmin=np.min(es.B_grid), vmax=np.max(es.B_grid), shading='gouraud')
cbar = fig.colorbar(ani_dict['mod B'], ax=ax1)
cbar.ax.set_ylabel(r'$B/T$')

# plot flux surface with basis vectors #
norm = 0.25*es.minor*np.sqrt(es.psiN)
ma = np.array([es.R_ma[0], es.Z_ma[0]])
mat = np.linalg.inv(es.rot_matrix[0])
R_basis = np.matmul(mat, np.array([norm, 0]))
Z_basis = np.matmul(mat, np.array([0, norm]))
R_pnt = ma + R_basis
Z_pnt = ma + Z_basis
ani_dict['flux surface'], = ax1.plot(es.R[0,:], es.Z[0,:], c='k')
ani_dict['basis R'] = ax1.arrow(ma[0], ma[1], R_basis[0], R_basis[1], head_width=0.008, color='k', zorder=10)
ani_dict['basis Z'] = ax1.arrow(ma[0], ma[1], Z_basis[0], Z_basis[1], head_width=0.008, color='k', zorder=10)

# plot rotated flux surface with extrema points #
ma_rot = ma_rots[0]
R_max, R_min = es.R_max[0], es.R_min[0]
Z_max, Z_min = es.Z_max[0], es.Z_min[0]
R_Zmax, R_Zmin = es.R_Zmax[0], es.R_Zmin[0]
Z_Rmax, Z_Rmin = es.Z_Rmax[0], es.Z_Rmin[0]
ani_dict['rotated flux surface'], = ax2.plot((es.R_prime[0,:]-ma_rot[0])*1e2, (es.Z_prime[0,:]-ma_rot[1])*1e2, c='k')
ani_dict['Z max'] = ax2.scatter([(R_Zmax-ma_rot[0])*1e2], [(Z_max-ma_rot[1])*1e2], c='tab:blue', marker='^', s=150, zorder=10)
ani_dict['R max'] = ax2.scatter([(R_max-ma_rot[0])*1e2], [(Z_Rmax-ma_rot[1])*1e2], c='tab:orange', marker='>', s=150, zorder=10)
ani_dict['Z min'] = ax2.scatter([(R_Zmin-ma_rot[0])*1e2], [(Z_min-ma_rot[1])*1e2], c='tab:blue', marker='v', s=150, zorder=10)
ani_dict['R min'] = ax2.scatter([(R_min-ma_rot[0])*1e2], [(Z_Rmin-ma_rot[1])*1e2], c='tab:orange', marker='<', s=150, zorder=10)

# plot upper_outer and lower_outer quadrant boxes #
ani_dict['UO Box 1'], = ax2.plot([(es.R_Zmax[0]-ma_rot[0])*1e2, (es.R_Zmax[0]-ma_rot[0])*1e2], [(es.Z_Rmax[0]-ma_rot[1])*1e2, (es.Z_max[0]-ma_rot[1])*1e2], c='k', lw=1)
ani_dict['UO Box 2'], = ax2.plot([(es.R_Zmax[0]-ma_rot[0])*1e2, (es.R_max[0]-ma_rot[0])*1e2], [(es.Z_max[0]-ma_rot[1])*1e2, (es.Z_max[0]-ma_rot[1])*1e2], c='k', lw=1)
ani_dict['UO Box 3'], = ax2.plot([(es.R_max[0]-ma_rot[0])*1e2, (es.R_max[0]-ma_rot[0])*1e2], [(es.Z_max[0]-ma_rot[1])*1e2, (es.Z_Rmax[0]-ma_rot[1])*1e2], c='k', lw=1)
ani_dict['UO Box 4'], = ax2.plot([(es.R_max[0]-ma_rot[0])*1e2, (es.R_Zmax[0]-ma_rot[0])*1e2], [(es.Z_Rmax[0]-ma_rot[1])*1e2, (es.Z_Rmax[0]-ma_rot[1])*1e2], c='k', lw=1)
ani_dict['UO Box 5'], = ax2.plot([(es.R_Zmax[0]-ma_rot[0])*1e2, (es.R_max[0]-ma_rot[0])*1e2], [(es.Z_Rmax[0]-ma_rot[1])*1e2, (es.Z_max[0]-ma_rot[1])*1e2], c='k', lw=1)

ani_dict['LO Box 1'], = ax2.plot([(es.R_Zmin[0]-ma_rot[0])*1e2, (es.R_max[0]-ma_rot[0])*1e2], [(es.Z_Rmax[0]-ma_rot[1])*1e2, (es.Z_Rmax[0]-ma_rot[1])*1e2], c='k', lw=1)
ani_dict['LO Box 2'], = ax2.plot([(es.R_max[0]-ma_rot[0])*1e2, (es.R_max[0]-ma_rot[0])*1e2], [(es.Z_Rmax[0]-ma_rot[1])*1e2, (es.Z_min[0]-ma_rot[1])*1e2], c='k', lw=1)
ani_dict['LO Box 3'], = ax2.plot([(es.R_max[0]-ma_rot[0])*1e2, (es.R_Zmin[0]-ma_rot[0])*1e2], [(es.Z_min[0]-ma_rot[1])*1e2, (es.Z_min[0]-ma_rot[1])*1e2], c='k', lw=1)
ani_dict['LO Box 4'], = ax2.plot([(es.R_Zmin[0]-ma_rot[0])*1e2, (es.R_Zmin[0]-ma_rot[0])*1e2], [(es.Z_min[0]-ma_rot[1])*1e2, (es.Z_Rmax[0]-ma_rot[1])*1e2], c='k', lw=1)
ani_dict['LO Box 5'], = ax2.plot([(es.R_Zmin[0]-ma_rot[0])*1e2, (es.R_max[0]-ma_rot[0])*1e2], [(es.Z_Rmax[0]-ma_rot[1])*1e2, (es.Z_min[0]-ma_rot[1])*1e2], c='k', lw=1)

# plot upper_outer and lower_outer ellipse #
Z_ell = np.linspace(es.Z_Rmax[0], es.Z_max[0], 50)
R_ell = es.R_Zmax[0] + es.A_uo[0]*np.sqrt(1 - ((Z_ell-es.Z_Rmax[0])/es.B_uo[0])**2)
ani_dict['UO Ellipse'], = ax2.plot((R_ell-ma_rot[0])*1e2, (Z_ell-ma_rot[1])*1e2, c='k', ls='--', lw=1)

Z_ell = np.linspace(es.Z_min[0], es.Z_Rmax[0], 50)
R_ell = es.R_Zmin[0] + es.A_lo[0]*np.sqrt(1 - ((es.Z_Rmax[0]-Z_ell)/es.B_lo[0])**2)
ani_dict['LO Ellipse'], = ax2.plot((R_ell-ma_rot[0])*1e2, (Z_ell-ma_rot[1])*1e2, c='k', ls='--', lw=1)

# plot upper_outer and lower_outer intersection with ellipse #
c1 = .5*np.sqrt(2)
c2 = 1-c1

Z_int = c1*es.Z_max[0]+c2*es.Z_Rmax[0]
R_int = c1*es.R_max[0]+c2*es.R_Zmax[0]
ani_dict['UO Ell Intersection'] = ax2.scatter([(R_int-ma_rot[0])*1e2], [(Z_int-ma_rot[1])*1e2], edgecolor='tab:green', facecolor='None', linewidth=2, s=50, zorder=20)

Z_int = c1*es.Z_min[0]+c2*es.Z_Rmax[0]
R_int = c1*es.R_max[0]+c2*es.R_Zmin[0]
ani_dict['LO Ell Intersection'] = ax2.scatter([(R_int-ma_rot[0])*1e2], [(Z_int-ma_rot[1])*1e2], edgecolor='tab:green', facecolor='None', linewidths=2, s=50, zorder=20)

# plot upper_outer and inner_outer intersection with flux surface #
idx_beg = np.argmax(es.R_prime[0])
idx_end = np.argmax(es.Z_prime[0])+1
if idx_end < idx_beg:
    R_surf = np.r_[es.R_prime[0, idx_beg::], es.R_prime[0, 0:idx_end]]
    Z_surf = np.r_[es.Z_prime[0, idx_beg::], es.Z_prime[0, 0:idx_end]]
else:
    R_surf = es.R_prime[0, idx_beg:idx_end]
    Z_surf = es.Z_prime[0, idx_beg:idx_end]
Z_surf, unique_0 = np.unique(Z_surf, return_index=True)
R_surf = R_surf[unique_0]
R_fit = interp1d(Z_surf, R_surf, kind='quadratic')
f = lambda z: R_fit(z) - (es.A_uo[0]/es.B_uo[0])*(z-es.Z_max[0]) - es.R_max[0]
root = brentq(f, es.Z_Rmax[0], es.Z_max[0])
Z_root, R_root = root, R_fit(root)
ani_dict['UO FS Intersection'] = ax2.scatter((R_root-ma_rot[0])*1e2, (Z_root-ma_rot[1])*1e2, c='tab:red', s=50, zorder=15)

idx_beg = np.argmin(es.Z_prime[0])
idx_end = np.argmax(es.R_prime[0])+1
if idx_end < idx_beg:
    R_surf = np.r_[es.R_prime[0, idx_beg::], es.R_prime[0, 0:idx_end]]
    Z_surf = np.r_[es.Z_prime[0, idx_beg::], es.Z_prime[0, 0:idx_end]]
else:
    R_surf = es.R_prime[0, idx_beg:idx_end]
    Z_surf = es.Z_prime[0, idx_beg:idx_end]
Z_surf, unique_0 = np.unique(Z_surf, return_index=True)
R_surf = R_surf[unique_0]
R_fit = interp1d(Z_surf, R_surf, kind='quadratic')
f = lambda z: R_fit(z) + (es.A_lo[0]/es.B_lo[0])*(z-es.Z_min[0]) - es.R_max[0]
root = brentq(f, es.Z_min[0], es.Z_Rmax[0])
Z_root, R_root = root, R_fit(root)
ani_dict['LO FS Intersection'] = ax2.scatter((R_root-ma_rot[0])*1e2, (Z_root-ma_rot[1])*1e2, c='tab:red', s=50, zorder=15)

tor_rng = [es.tor_dom[0]/np.pi, es.tor_dom[-1]/np.pi]
# plot elongation #
ax3.plot(es.tor_dom/np.pi, es.kappa, c='tab:blue', alpha=0.3)
ax3.plot(tor_rng, [es.kappa_avg]*2, c='tab:blue', ls='--', label=r'$\overline{{\kappa}} = {0:0.2f}$'.format(es.kappa_avg))
ani_dict['kappa'], = ax3.plot([], [], c='tab:blue')
ani_dict['kappa dot'], = ax3.plot([], [], marker='o', markersize=10, mec='k', mfc='tab:blue', zorder=10)

# axis limits #
ax1.set_xlim(0.99*np.min(es.R), 1.01*np.max(es.R))
ax1.set_ylim(1.01*np.min(es.Z), 1.01*np.max(es.Z))

ax2.set_xlim(1.25*1e2*np.min(es.R_min-ma_rots[:,0]), 1.25*1e2*np.max(es.R_max-ma_rots[:,0]))
ax2.set_ylim(1.1*1e2*np.min(es.Z_min-ma_rots[:,1]), 1.1*1e2*np.max(es.Z_max-ma_rots[:,1]))

ax3.set_xlim(es.tor_dom[0]/np.pi, es.tor_dom[-1]/np.pi)

ax3.set_ylim(0, ax3.get_ylim()[1])

# axis labels #
ax1.set_xlabel(r'$R/\mathrm{m}$')
ax1.set_ylabel(r'$Z/\mathrm{m}$')

ax2.set_xlabel('$R^{\prime}/\mathrm{cm}$')
ax2.set_ylabel('$Z^{\prime}/\mathrm{cm}$')

ax3.set_ylabel(r'$\kappa$')
ax3.set_xlabel(r'$\varphi/\pi$')

# axis legends #
ax3.legend(loc='upper left', frameon=True, ncol=1)

# axis grids #
ax3.grid()

def animation_frame(idx):
    # plot |B| distribution #
    ani_dict['mod B'].remove()
    ani_dict['mod B'] = axes['ax1'].pcolormesh(es.R_grid[:,idx,:], es.Z_grid[:,idx,:], es.B_grid[:,idx,:], cmap='jet', vmin=np.min(es.B_grid), vmax=np.max(es.B_grid), shading='gouraud')

    # plot flux surface with basis vectors #
    norm = 0.3*es.minor*np.sqrt(es.psiN)
    ma = np.array([es.R_ma[idx], es.Z_ma[idx]])
    mat = np.linalg.inv(es.rot_matrix[idx])
    R_basis = np.matmul(mat, np.array([norm, 0]))
    Z_basis = np.matmul(mat, np.array([0, norm]))
    R_pnt = ma + R_basis
    Z_pnt = ma + Z_basis
    ani_dict['flux surface'].set_data(es.R[idx,:], es.Z[idx,:])
    ani_dict['basis R'].set_data(x=ma[0], y=ma[1], dx=R_basis[0], dy=R_basis[1])
    ani_dict['basis Z'].set_data(x=ma[0], y=ma[1], dx=Z_basis[0], dy=Z_basis[1])

    # plot rotated flux surface with extrema points #
    ma_rot = ani_dict['Rotated MA'][idx]
    R_max, R_min = es.R_max[idx], es.R_min[idx]
    Z_max, Z_min = es.Z_max[idx], es.Z_min[idx]
    R_Zmax, R_Zmin = es.R_Zmax[idx], es.R_Zmin[idx]
    Z_Rmax, Z_Rmin = es.Z_Rmax[idx], es.Z_Rmin[idx]
    ani_dict['rotated flux surface'].set_data((es.R_prime[idx,:]-ma_rot[0])*1e2, (es.Z_prime[idx,:]-ma_rot[1])*1e2)
    ani_dict['Z max'].set_offsets([(R_Zmax-ma_rot[0])*1e2, (Z_max-ma_rot[1])*1e2])
    ani_dict['R max'].set_offsets([(R_max-ma_rot[0])*1e2, (Z_Rmax-ma_rot[1])*1e2])
    ani_dict['Z min'].set_offsets([(R_Zmin-ma_rot[0])*1e2, (Z_min-ma_rot[1])*1e2])
    ani_dict['R min'].set_offsets([(R_min-ma_rot[0])*1e2, (Z_Rmin-ma_rot[1])*1e2])

    # plot upper_outer and lower_outer quadrant boxes #
    ani_dict['UO Box 1'].set_data([(es.R_Zmax[idx]-ma_rot[0])*1e2, (es.R_Zmax[idx]-ma_rot[0])*1e2], [(es.Z_Rmax[idx]-ma_rot[1])*1e2, (es.Z_max[idx]-ma_rot[1])*1e2])
    ani_dict['UO Box 2'].set_data([(es.R_Zmax[idx]-ma_rot[0])*1e2, (es.R_max[idx]-ma_rot[0])*1e2], [(es.Z_max[idx]-ma_rot[1])*1e2, (es.Z_max[idx]-ma_rot[1])*1e2])
    ani_dict['UO Box 3'].set_data([(es.R_max[idx]-ma_rot[0])*1e2, (es.R_max[idx]-ma_rot[0])*1e2], [(es.Z_max[idx]-ma_rot[1])*1e2, (es.Z_Rmax[idx]-ma_rot[1])*1e2])
    ani_dict['UO Box 4'].set_data([(es.R_max[idx]-ma_rot[0])*1e2, (es.R_Zmax[idx]-ma_rot[0])*1e2], [(es.Z_Rmax[idx]-ma_rot[1])*1e2, (es.Z_Rmax[idx]-ma_rot[1])*1e2])
    ani_dict['UO Box 5'].set_data([(es.R_Zmax[idx]-ma_rot[0])*1e2, (es.R_max[idx]-ma_rot[0])*1e2], [(es.Z_Rmax[idx]-ma_rot[1])*1e2, (es.Z_max[idx]-ma_rot[1])*1e2])

    ani_dict['LO Box 1'].set_data([(es.R_Zmin[idx]-ma_rot[0])*1e2, (es.R_max[idx]-ma_rot[0])*1e2], [(es.Z_Rmax[idx]-ma_rot[1])*1e2, (es.Z_Rmax[idx]-ma_rot[1])*1e2])
    ani_dict['LO Box 2'].set_data([(es.R_max[idx]-ma_rot[0])*1e2, (es.R_max[idx]-ma_rot[0])*1e2], [(es.Z_Rmax[idx]-ma_rot[1])*1e2, (es.Z_min[idx]-ma_rot[1])*1e2])
    ani_dict['LO Box 3'].set_data([(es.R_max[idx]-ma_rot[0])*1e2, (es.R_Zmin[idx]-ma_rot[0])*1e2], [(es.Z_min[idx]-ma_rot[1])*1e2, (es.Z_min[idx]-ma_rot[1])*1e2])
    ani_dict['LO Box 4'].set_data([(es.R_Zmin[idx]-ma_rot[0])*1e2, (es.R_Zmin[idx]-ma_rot[0])*1e2], [(es.Z_min[idx]-ma_rot[1])*1e2, (es.Z_Rmax[idx]-ma_rot[1])*1e2])
    ani_dict['LO Box 5'].set_data([(es.R_Zmin[idx]-ma_rot[0])*1e2, (es.R_max[idx]-ma_rot[0])*1e2], [(es.Z_Rmax[idx]-ma_rot[1])*1e2, (es.Z_min[idx]-ma_rot[1])*1e2])

    # plot upper_outer and lower_outer ellipse #
    Z_ell = np.linspace(es.Z_Rmax[idx], es.Z_max[idx], 50)
    R_ell = es.R_Zmax[idx] + es.A_uo[idx]*np.sqrt(1 - ((Z_ell-es.Z_Rmax[idx])/es.B_uo[idx])**2)
    ani_dict['UO Ellipse'].set_data((R_ell-ma_rot[0])*1e2, (Z_ell-ma_rot[1])*1e2)

    Z_ell = np.linspace(es.Z_min[idx], es.Z_Rmax[idx], 50)
    R_ell = es.R_Zmin[idx] + es.A_lo[idx]*np.sqrt(1 - ((es.Z_Rmax[idx]-Z_ell)/es.B_lo[idx])**2)
    ani_dict['LO Ellipse'].set_data((R_ell-ma_rot[0])*1e2, (Z_ell-ma_rot[1])*1e2)

    # plot upper_outer and lower_outer intersection with ellipse #
    c1 = .5*np.sqrt(2)
    c2 = 1-c1

    Z_int = c1*es.Z_max[idx]+c2*es.Z_Rmax[idx]
    R_int = c1*es.R_max[idx]+c2*es.R_Zmax[idx]
    ani_dict['UO Ell Intersection'].set_offsets([(R_int-ma_rot[0])*1e2, (Z_int-ma_rot[1])*1e2])

    Z_int = c1*es.Z_min[idx]+c2*es.Z_Rmax[idx]
    R_int = c1*es.R_max[idx]+c2*es.R_Zmin[idx]
    ani_dict['LO Ell Intersection'].set_offsets([(R_int-ma_rot[0])*1e2, (Z_int-ma_rot[1])*1e2])

    # plot upper_outer and inner_outer intersection with flux surface #
    idx_beg = np.argmax(es.R_prime[idx])
    idx_end = np.argmax(es.Z_prime[idx])+1
    if idx_end < idx_beg:
        R_surf = np.r_[es.R_prime[idx, idx_beg::], es.R_prime[idx, 0:idx_end]]
        Z_surf = np.r_[es.Z_prime[idx, idx_beg::], es.Z_prime[idx, 0:idx_end]]
    else:
        R_surf = es.R_prime[idx, idx_beg:idx_end]
        Z_surf = es.Z_prime[idx, idx_beg:idx_end]
    Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
    R_surf = R_surf[unique_idx]
    R_fit = interp1d(Z_surf, R_surf, kind='quadratic')
    f = lambda z: R_fit(z) - (es.A_uo[idx]/es.B_uo[idx])*(z-es.Z_max[idx]) - es.R_max[idx]
    root = brentq(f, es.Z_Rmax[idx], es.Z_max[idx])
    Z_root, R_root = root, R_fit(root)
    ani_dict['UO FS Intersection'].set_offsets([(R_root-ma_rot[0])*1e2, (Z_root-ma_rot[1])*1e2])

    idx_beg = np.argmin(es.Z_prime[idx])
    idx_end = np.argmax(es.R_prime[idx])+1
    if idx_end < idx_beg:
        R_surf = np.r_[es.R_prime[idx, idx_beg::], es.R_prime[idx, 0:idx_end]]
        Z_surf = np.r_[es.Z_prime[idx, idx_beg::], es.Z_prime[idx, 0:idx_end]]
    else:
        R_surf = es.R_prime[idx, idx_beg:idx_end]
        Z_surf = es.Z_prime[idx, idx_beg:idx_end]
    Z_surf, unique_idx = np.unique(Z_surf, return_index=True)
    R_surf = R_surf[unique_idx]
    R_fit = interp1d(Z_surf, R_surf, kind='quadratic')
    f = lambda z: R_fit(z) + (es.A_lo[idx]/es.B_lo[idx])*(z-es.Z_min[idx]) - es.R_max[idx]
    root = brentq(f, es.Z_min[idx], es.Z_Rmax[idx])
    Z_root, R_root = root, R_fit(root)
    ani_dict['LO FS Intersection'].set_offsets([(R_root-ma_rot[0])*1e2, (Z_root-ma_rot[1])*1e2])

    # plot elongation #
    ani_dict['kappa'].set_data(es.tor_dom[0:idx+1]/np.pi, es.kappa[0:idx+1])
    ani_dict['kappa dot'].set_data(es.tor_dom[idx]/np.pi, es.kappa[idx])

    return ani_dict


# save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'APS', '2023', 'Figures', 'shaping_%s.mp4' % config_id)
save_path = os.path.join('/home', 'michael', 'onedrive', 'Presentations', 'Conferences', 'APS', '2023', 'Figures', 'shape_frames', config_id, 'frame.png')
anim = ani.FuncAnimation(fig, animation_frame, frames=es.tpts)
# anim.save(save_path, writer='ffmpeg', fps=15)
anim.save(save_path, writer='imagemagick', fps=15)
