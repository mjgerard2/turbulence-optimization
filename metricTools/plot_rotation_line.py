import os, sys
import numpy as np

import maptloblib as mpl
import matplotlib.pyplot as plt

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
import metricTools.equilibrium_shapes as shape
import vmecTools.wout_files.wout_read as wr


wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', '')
