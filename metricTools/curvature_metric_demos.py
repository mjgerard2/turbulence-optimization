import os, sys
import string
import numpy as np

import matplotlib as mpl
from matplotlib.ticker import MultipleLocator

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import plot_define as pd
import vmecTools.wout_files.curveB_tools as cb
import vmecTools.wout_files.wout_read as wr


# import wout file #
wout_path = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec', 'wout_QHS_mn1824_ns101.nc')
# wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'wout_QHS_mn1824_ns101.nc')
# wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'wout_well_11p0_mn1824_ns101.nc')
# wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_60', 'set_1', 'job_84', 'wout_60-1-84_mn1824_ns101.nc')
wout = wr.readWout(wout_path, space_derivs=True, field_derivs=True)

# computational grid #
beg_idx = np.argmin(np.abs(wout.s_grid-0.05))
psi_dom = wout.s_grid[beg_idx:]
pol_dom = np.linspace(-np.pi, np.pi, 251)
tor_dom = np.linspace(0, .25*np.pi, 3)
npsi = psi_dom.shape[0]
npol = pol_dom.shape[0]
ntor = tor_dom.shape[0]

# compute equilibrium variables #
amp_keys = ['R', 'Z', 'Jacobian', 'dR_ds', 'dR_du', 'dR_dv', 'dZ_ds', 'dZ_du', 'dZ_dv',
        'Bmod', 'Bu_contra', 'Bv_contra', 'Bs_covar', 'Bu_covar', 'Bv_covar', 'dBmod_ds',
        'dBmod_du', 'dBmod_dv', 'dBs_du', 'dBs_dv', 'dBu_ds', 'dBu_dv', 'dBv_ds', 'dBv_du',
        'dL_ds', 'dL_du', 'dL_dv']
wout.transForm_3D(psi_dom, pol_dom, tor_dom, amp_keys)
curB = cb.BcurveTools(wout)
curB.calc_curvature()
R = wout.invFourAmps['R']
Z = wout.invFourAmps['Z']
B = wout.invFourAmps['Bmod']
jacob = wout.invFourAmps['Jacobian']
kn = curB.k_norm

# compute metrics #
vol_inv = 1./np.trapz(np.trapz(np.trapz(jacob, pol_dom, axis=2), tor_dom, axis=1), psi_dom)
Bmax = np.repeat(np.max(np.max(B, axis=2), axis=1), npol*ntor).reshape(npsi, ntor, npol)
Bk = (Bmax-B)*kn
Cn = (kn/np.sqrt(curB.gss))
Bk_max = np.abs(Bk).max()
Cn_max = np.abs(Cn).max()

# plotting parameters #
plot = pd.plot_define()
plt = plot.plt
fig, axs = plt.subplots(1, 2, figsize=(12, 4))
plt.subplots_adjust(left=0.035, right=0.92, bottom=0.14, top=0.97, wspace=0.35)

# color map Bk #
ncolors_Bk = 17
cmap_Bk = 'PiYG_r'
cmap_dom_Bk = plt.get_cmap(cmap_Bk, ncolors_Bk)
cmap_bounds_Bk = np.linspace(-Bk_max, Bk_max, ncolors_Bk+1)
cmap_norm_Bk = mpl.colors.BoundaryNorm(cmap_bounds_Bk, ncolors=cmap_dom_Bk.N)

# color map Cn #
ncolors_Cn = 17
cmap_Cn = 'RdBu_r'
cmap_dom_Cn = plt.get_cmap(cmap_Cn, ncolors_Cn)
cmap_bounds_Cn = np.linspace(-Cn_max, Cn_max, ncolors_Cn+1)
cmap_norm_Cn = mpl.colors.BoundaryNorm(cmap_bounds_Cn, ncolors=cmap_dom_Cn.N)

# plot Bk data #
axs[0].pcolormesh(R[:,0], Z[:,0], Bk[:,0], shading='gouraud', cmap=cmap_dom_Bk, norm=cmap_norm_Bk)
axs[0].contour(R[:,0], Z[:,0], Bk[:,0], cmap_bounds_Bk, colors='k', linewidths=0.5)
axs[0].plot(R[0,0,:], Z[0,0,:], c='k')
axs[0].plot(R[npsi-1,0,:], Z[npsi-1,0,:], c='k')
axs[0].pcolormesh(R[:,1], Z[:,1], Bk[:,1], shading='gouraud', cmap=cmap_dom_Bk, norm=cmap_norm_Bk)
axs[0].contour(R[:,1], Z[:,1], Bk[:,1], cmap_bounds_Bk, colors='k', linewidths=0.5)
axs[0].plot(R[0,1,:], Z[0,1,:], c='k')
axs[0].plot(R[npsi-1,1,:], Z[npsi-1,1,:], c='k')
axs[0].pcolormesh(R[:,2], Z[:,2], Bk[:,2], shading='gouraud', cmap=cmap_dom_Bk, norm=cmap_norm_Bk)
axs[0].contour(R[:,2], Z[:,2], Bk[:,2], cmap_bounds_Bk, colors='k', linewidths=0.5)
axs[0].plot(R[0,2,:], Z[0,2,:], c='k')
axs[0].plot(R[npsi-1,2,:], Z[npsi-1,2,:], c='k')

# plot Cn data #
axs[1].pcolormesh(R[:,0], Z[:,0], Cn[:,0], shading='gouraud', cmap=cmap_dom_Cn, norm=cmap_norm_Cn)
axs[1].contour(R[:,0], Z[:,0], Cn[:,0], cmap_bounds_Cn, colors='k', linewidths=0.5)
axs[1].plot(R[0,0,:], Z[0,0,:], c='k')
axs[1].plot(R[npsi-1,0,:], Z[npsi-1,0,:], c='k')
axs[1].pcolormesh(R[:,1], Z[:,1], Cn[:,1], shading='gouraud', cmap=cmap_dom_Cn, norm=cmap_norm_Cn)
axs[1].contour(R[:,1], Z[:,1], Cn[:,1], cmap_bounds_Cn, colors='k', linewidths=0.5)
axs[1].plot(R[0,1,:], Z[0,1,:], c='k')
axs[1].plot(R[npsi-1,1,:], Z[npsi-1,1,:], c='k')
axs[1].pcolormesh(R[:,2], Z[:,2], Cn[:,2], shading='gouraud', cmap=cmap_dom_Cn, norm=cmap_norm_Cn)
axs[1].contour(R[:,2], Z[:,2], Cn[:,2], cmap_bounds_Cn, colors='k', linewidths=0.5)
axs[1].plot(R[0,2,:], Z[0,2,:], c='k')
axs[1].plot(R[npsi-1,2,:], Z[npsi-1,2,:], c='k')

# axis limits #
zmax = 0.33
rmin = .5*np.sum(axs[0].get_xlim())-zmax
rmax = .5*np.sum(axs[0].get_xlim())+zmax
axs[0].set_xlim(rmin, rmax)
axs[0].set_ylim(-zmax, zmax)
axs[1].set_xlim(rmin, rmax)
axs[1].set_ylim(-zmax, zmax)
axs[0].set_aspect('equal')
axs[1].set_aspect('equal')

# axis labels #
axs[0].set_xlabel(r'$R \ / \ \mathrm{m}$')
axs[0].set_ylabel(r'$Z \ / \ \mathrm{m}$')
axs[1].set_xlabel(r'$R \ / \ \mathrm{m}$')
axs[1].set_ylabel(r'$Z \ / \ \mathrm{m}$')

# Bk colorbar #
lr_disp, ur_disp = axs[0].transAxes.transform((1, 0)), axs[0].transAxes.transform((1, 1))
lr_fig, ur_fig = fig.transFigure.inverted().transform(lr_disp), fig.transFigure.inverted().transform(ur_disp)
cbar_ax = fig.add_axes([lr_fig[0]-0.03, lr_fig[1], 0.02, ur_fig[1]-lr_fig[1]])
cbar = mpl.colorbar.ColorbarBase(cbar_ax, cmap=cmap_dom_Bk, norm=cmap_norm_Bk, orientation='vertical')
tick_shift = 0.5*(cmap_bounds_Bk[1]-cmap_bounds_Bk[0])
tick_bounds = np.linspace(cmap_bounds_Bk[0]+tick_shift, cmap_bounds_Bk[-1]-tick_shift, int(0.5*(ncolors_Bk-1))+1)
cbar.set_ticks(tick_bounds)
cbar.set_ticklabels(['{0:0.2g}'.format(x) for x in tick_bounds])
cbar.ax.set_ylabel(r'$(B_\mathrm{max} - B) \kappa_\mathrm{n}$')

# Cn colorbar #
lr_disp, ur_disp = axs[1].transAxes.transform((1, 0)), axs[1].transAxes.transform((1, 1))
lr_fig, ur_fig = fig.transFigure.inverted().transform(lr_disp), fig.transFigure.inverted().transform(ur_disp)
cbar_ax = fig.add_axes([lr_fig[0]-0.03, lr_fig[1], 0.02, ur_fig[1]-lr_fig[1]])
cbar = mpl.colorbar.ColorbarBase(cbar_ax, cmap=cmap_dom_Cn, norm=cmap_norm_Cn, orientation='vertical')
tick_shift = 0.5*(cmap_bounds_Cn[1]-cmap_bounds_Cn[0])
tick_bounds = np.linspace(cmap_bounds_Cn[0]+tick_shift, cmap_bounds_Cn[-1]-tick_shift, int(0.5*(ncolors_Cn-1))+1)
tick_bounds[np.where(np.abs(tick_bounds) < 1e-10)] = 0
cbar.set_ticks(tick_bounds)
cbar.set_ticklabels(['{0:0.2g}'.format(x) for x in tick_bounds])
cbar.ax.set_ylabel(r'$\kappa_\mathrm{n}/|\nabla\psi|$')

# loop axes #
for a, ax in enumerate(axs.flat):
    # ticks #
    ax.tick_params(axis='both', which='both', direction='in')
    ax.xaxis.set_ticks_position('default')
    ax.yaxis.set_ticks_position('default')
    ax.xaxis.set_minor_locator(MultipleLocator(0.05))
    ax.yaxis.set_minor_locator(MultipleLocator(0.05))

    # text #
    ax.text(0.83, 0.9, r'$\zeta = 0$', transform=ax.transAxes, va='top', ha='left')
    ax.text(0.55, 0.95, r'$\zeta = \pi/8$', transform=ax.transAxes, va='top', ha='left')
    ax.text(0.1, 0.72, r'$\zeta = \pi/4$', transform=ax.transAxes, va='top', ha='left')
    ax.text(0.05, 0.05, f'({string.ascii_lowercase[a]})', transform=ax.transAxes, va='bottom', ha='left')

# save/show #
plt.show()
save_path = os.path.join('/home', 'michael', 'Desktop', 'CHTC_OSG', 'figures', 'metric_demo.pdf')
# plt.savefig(save_path, format='pdf')
