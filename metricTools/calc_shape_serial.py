import os
import pandas as pd
import h5py as hf
import numpy as np

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import metricTools.equilibrium_shapes as es
import vmecTools.wout_files.wout_read as wr


# Number of CPUs to use #
resolution = 'high'

# define computational grid #
tor_pnts = 101

if resolution == 'low':
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
elif resolution == 'high':
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
else:
    raise KeyError(resolution+' is not a valide resolution.')

wout = wr.readWout(wout_path)
a_qhs = wout.a_minor
shape = es.shape_calculator(wout, 0.5, tor_pnts)
shape.calc_shaping_parameters()

kappa_qhs = np.abs(shape.kappa_avg)
delta_qhs = np.abs(shape.delta_avg)
zeta_qhs = np.abs(shape.zeta_avg)

# generate configuration list #
if resolution == 'low':
    data_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'HSX_coil_current_data_mn88.h5')
elif resolution == 'high':
    data_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'HSX_coil_current_data_mn1824.h5')
else:
    raise KeyError(resolution+' is not a valide resolution.')
df = pd.read_hdf(data_path, key='DataFrame')
config_list = df.drop('0-1-0').index.values

# Check Saved Data #
saved_data = {}
if resolution == 'low':
    save_file = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'equilibrium_shapes_mn88_tor{}.h5'.format(tor_pnts))
elif resolution == 'high':
    save_file = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'equilibrium_shapes_mn1824_tor{}.h5'.format(tor_pnts))
else:
    raise KeyError(resolution+' is not a valide resolution.')
if os.path.isfile(save_file):
    with hf.File(save_file, 'r') as hf_:
        for key in hf_:
            saved_data[key] = hf_[key][()]

config_list = ['well11p0']
config_data = np.empty((len(config_list), 3))
for i, config in enumerate(config_list):
    # define wout file name #
    config_split = config.split('-')
    if len(config_split) == 3:
        mID, sID, jID = config_split
        main_id = 'main_coil_%s' % mID
        set_id = 'set_%s' % sID
        job_id = 'job_%s' % jID
        if resolution == 'low':
            wout_name = 'wout_HSX_main_opt0.nc'
        elif resolution == 'high':
            wout_name = 'wout_%s_mn1824_ns101.nc' % config_id
        else:
            raise KeyError(resolution+' is not a valide resolution.')
        wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, wout_name)
    else:
        if resolution == 'low':
            wout_name = 'wout_HSX_main_opt0.nc'
        elif resolution == 'high':
            wout_name = 'wout_HSX_%s_mn1824_ns101.nc' % config
        else:
            raise KeyError(resolution+' is not a valide resolution.')
        wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'unique_configurations', wout_name)
    
    # instantiate wout object #
    wout = wr.readWout(wout_path)
    s_val = 0.5*((a_qhs/wout.a_minor)**2)

    # calculate shaping parameters
    shape = es.shape_calculator(wout, s_val, tor_pnts)
    shape.calc_shaping_parameters()
    kappa = shape.kappa_avg/kappa_qhs
    delta = shape.delta_avg/delta_qhs
    zeta = shape.zeta_avg/zeta_qhs
    config_data[i] = [kappa, delta, zeta]

with hf.File(save_file, 'a') as hf_:
    for i, config in enumerate(config_list):
        hf_.create_dataset(config, data=config_data[i])

