import os, sys
import h5py as hf
import numpy as np
from netCDF4 import Dataset

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import plot_define as pd
import vmecTools.wout_files.booz_read as br

booz_dir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools', 'metricTools')
# text_path = os.path.join(booz_dir, 'Qsym.txt')
text_path = os.path.join(booz_dir, 'Qsym_no-mn14.txt')

with open(text_path, 'r') as f:
    lines = f.readlines()
    data_dict = {}
    for line in lines:
        line = line.strip().split()
        len_line = len(line)
        if len_line == 1:
            key = line[0]
            data_dict[key] = {}
        elif len_line > 1:
            data_dict[key][line[0]] = np.array([float(x) for x in line[1:]])
        else:
            continue
"""
tags = ['QHS_mn1824', '0-3-84_mn1824', 'hill4p4_mn1824', 'well8p0_mn1824']
booz_names = ['boozmn_wout_QHS_mn1824_ns101.nc', 'boozmn_wout_HSX_0-3-84_mn1824_ns101.nc', 'boozmn_wout_HSX_hill4p4_mn1824_ns101.nc', 'boozmn_wout_HSX_well8p0_mn1824_ns101.nc']

s_doms = np.empty((len(booz_names), 99))
Q_profs = np.empty((len(booz_names), 99))
for i, name in enumerate(booz_names):
    print('({}|{})'.format(i+1, len(booz_names)))
    booz_path = os.path.join(booz_dir, name)
    booz = br.readBooz(booz_path)
    s_doms[i] = booz.s_dom
    Q_profs[i] = booz.qhs_sym_metric()
"""
color_dict = {'Hill4p4': 'tab:green',
              'well8p0': 'tab:red'}

plot = pd.plot_define(fontSize=14, labelSize=18, lineWidth=2)
plt = plot.plt
fig, ax = plt.subplots(1, 1, tight_layout=True)
for key, prof_dict in data_dict.items():
    # tag = name.split('.')[0].split('_')[-1]
    key_split = key.split('_')
    if key_split[1] == 'mn1824':
        plt1, = ax.plot(prof_dict['psiN'], prof_dict['Q'], label=key)
    else:
        plt1, = ax.plot(prof_dict['psiN'], prof_dict['Q'], c=plt1.get_color(), ls='--', label=key)

ax.set_xlim(0, 1)
ax.set_ylim(0, ax.get_ylim()[1])

ax.set_xlabel(r'$\psi/\psi_{\mathrm{edge}}$')
ax.set_ylabel(r'$\mathcal{Q}_\mathrm{sym} \ (m \neq 1, \, n\neq 4)$')

ax.legend(fontsize=12)
save_path = os.path.join(booz_dir, 'Qsym_no-mn14.png')
plt.savefig(save_path)
# plt.show()
"""
with open(text_path, 'w') as f:
    for i, name in enumerate(booz_names):
        tag = tags[i]
        f.write(tag+'\n')
        f.write('psiN '+' '.join([str(x) for x in s_doms[i]])+'\n')
        f.write('Q '+' '+' '.join([str(x) for x in Q_profs[i]])+'\n\n')
"""
