import os, sys
import shlex
import subprocess
import h5py as hf
import numpy as np
from netCDF4 import Dataset

from multiprocessing import Process, Queue, cpu_count

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
# import vmecTools.wout_files.wout_read as wr
import vmecTools.wout_files.booz_read as br

# def calc_sym_metric(wout_path, qhs_minor):
def calc_sym_metric(in_que, out_que):

    while True:
        config_id, cnt, tot, run_dict = in_que.get()
        if config_id == 'Done':
            break
        else:
            # unpack dictionary #
            qhs_minor = run_dict['minor radius']
            qhs_sym_metric = run_dict['symmetry metric']
            hgh_res = run_dict['high resolution boolean']

            # copy over wout file #
            main_id = 'main_coil_{}'.format(config_id.split('-')[0])
            set_id = 'set_{}'.format(config_id.split('-')[1])
            job_id = 'job_{}'.format(config_id.split('-')[2])
            if hgh_res:
                wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_%s_mn1824_ns101.nc' % config_id)
            else:
                wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_HSX_main_opt0.nc')
            wout_name = 'wout_%s' % config_id
            subprocess.run(['cp', wout_path, './%s.nc' % wout_name])

            # get minor radius of configuration #
            rootgrp = Dataset(wout_path, 'r')
            ns = rootgrp['/ns'][:]
            a_minor = rootgrp['/Aminor_p'][:]
            rootgrp.close()

            # get psi surface #
            s_vmec = np.linspace(0, 1, ns)
            s_val = .5*((qhs_minor/a_minor)**2)
            s_idx = np.argmin(np.abs(s_vmec-s_val))

            # generate xbooz_input #
            xbooz_file = './xbooz_input_%s' % config_id
            with open(xbooz_file, 'w') as f:
                if hgh_res:
                    f.write('72 94\n')
                else:
                    f.write('32 32\n')
                f.write('{}\n'.format(wout_name))
                f.write('{}'.format(s_idx))

            # run xbooz_xform #
            xbooz_exe = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec', 'xbooz_xform')
            cmnd = shlex.split('%s %s' % (xbooz_exe, xbooz_file))
            subprocess.run(cmnd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

            # read xbooz_xform output #
            booz_path = os.path.join(os.getcwd(), 'boozmn_{}.nc'.format(wout_name)) 
            rootgrp = Dataset(booz_path, 'r')
            xm = rootgrp['/ixm_b'][:]
            xn = rootgrp['/ixn_b'][:]
            Bmod_amps = rootgrp['/bmnc_b'][0, :]
            rootgrp.close()

            # calculate symmetry metric #
            Bmn = 0
            for i in range(xn.shape[0]):
                if xm[i] == 0 and xn[i] == 0:
                    B_00 = Bmod_amps[i]
                elif xm[i] == 0 and xn[i] != 0:
                    Bmn = Bmn + Bmod_amps[i]**2
                else:
                    sym_chk = xn[i]/xm[i]
                    if not sym_chk == 4:
                        Bmn = Bmn + Bmod_amps[i]**2
            sym_metric = np.sqrt(Bmn)/(B_00*qhs_sym_metric)

            # remove unnecessary files #
            subprocess.run(['rm', xbooz_file])
            subprocess.run(['rm', booz_path])
            subprocess.run(['rm', '%s.nc' % wout_name])

            # pass output to output queue #
            out_que.put([config_id, cnt, tot, sym_metric])

def save_loop(in_que, save_file):
    while True:
        config_id, cnt, tot, data = in_que.get()
        if config_id == 'Done':
            break
        else:
            print('{0} : Q={1:0.2f} : ({2}|{3})'.format(config_id, data, cnt, tot))
            with hf.File(save_file, 'a') as hf_:
                hf_.create_dataset(config_id, data=data)

if __name__ == '__main__':
    # Number of CPUs to use #
    hgh_res = True
    num_of_cpus = 8  # cpu_count()-1

    # get normalization data #
    if hgh_res:
        wout_qhs = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
    else:
        wout_qhs = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
    rootgrp = Dataset(wout_qhs, 'r')
    qhs_minor = rootgrp['/Aminor_p'][:]
    rootgrp.close()

    # read xbooz_xform output #
    if hgh_res:
        booz_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'boozmn_QHS_mn7294_ns101.nc')
    else:
        booz_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'boozmn_wout_HSX_main_opt0_32_32.nc')
    booz = br.readBooz(booz_path)
    xm = booz.xm
    xn = booz.xn
    Bmod_amps = booz.fourierAmps['Bmod'](0.5)
    
    # calculate symmetry metric #
    Bmn = 0
    for i in range(xn.shape[0]):
        if xm[i] == 0 and xn[i] == 0:
            B_00 = Bmod_amps[i]
        elif xm[i] == 0 and xn[i] != 0:
            Bmn = Bmn + Bmod_amps[i]**2
        else:
            sym_chk = xn[i]/xm[i]
            if not sym_chk == 4:
                Bmn = Bmn + Bmod_amps[i]**2
    sym_metric_qhs = np.sqrt(Bmn)/B_00

    # generate configuration list #
    sel_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'ae_mn1824_res128_npol16.h5')
    with hf.File(sel_path, 'r') as hf_:
        config_arrs = hf_['config ids']
        config_list = []
        for config_arr in config_arrs:
            config_id = '-'.join(['{}'.format(int(x)) for x in config_arr])
            config_list.append(config_id)

    # check saved data #
    saved_data = {}
    if hgh_res:
        save_file = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'ML_sym_metric_mn7294.h5')
    else:
        save_file = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'ML_sym_metric_mn3232.h5')
    if os.path.isfile(save_file):
        with hf.File(save_file, 'r') as hf_:
            for key in hf_:
                saved_data[key] = hf_[key][()]

    # run dictionary #
    run_dict = {'minor radius': qhs_minor,
                'symmetry metric': sym_metric_qhs,
                'high resolution boolean': hgh_res}
    
    # populate queue #
    in_que = Queue()
    out_que = Queue()
    for cdx, config_id in enumerate(config_list):
        cnt = cdx+1
        tot = len(config_list)
        if not config_id in saved_data:
            in_que.put([config_id, cnt, tot, run_dict])

    # start metric calculation processes #
    work_procs = []
    for j in range(num_of_cpus-1):
        in_que.put(['Done', 0, 0, run_dict])
        proc = Process(target=calc_sym_metric, args=(in_que, out_que,))
        proc.start()
        work_procs.append(proc)

    # print('start saver')
    save_proc = Process(target=save_loop, args=(out_que, save_file,))
    save_proc.start()

    for k, proc in enumerate(work_procs):
        proc.join()

    out_que.put(['Done', 0, 0, 0])
