import os, sys
import h5py as hf
import numpy as np

from netCDF4 import Dataset

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import vmecTools.wout_files.wout_read as wr

# get configuration list #
config_list = []
main_dir = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0')
set_dirs = [f.name for f in os.scandir(main_dir) if os.path.isdir(f.path)]
for set_dir in set_dirs:
    set_num = set_dir.split('_')[1]
    set_path = os.path.join(main_dir, set_dir)
    job_dirs = [f.name for f in os.scandir(set_path) if os.path.isdir(f.path)]
    for job_dir in job_dirs:
        job_num = job_dir.split('_')[1]
        config_id = '-'.join(['0', set_num, job_num])
        config_list.append(config_id)

# import vessel file #
path = os.path.join('/mnt','HSX_Database','HSX_Configs','coil_data','vessel90.h5')
with hf.File(path,'r') as hf_file:
    vessel = hf_file['data'][:]
    v_dom = hf_file['domain'][:]

idx = np.argmin(np.abs(v_dom))
ves_x = vessel[idx,:,0]
ves_y = vessel[idx,:,1]
ves_z = vessel[idx,:,2]
ves = np.stack((ves_x, ves_y, ves_z), axis=1)

u_dom = np.linspace(-np.pi, np.pi, 101)
num_of_pol = u_dom.shape[0]
num_of_ves = ves.shape[0]
num_of_pnts = num_of_pol * num_of_ves
ves_full = np.empty((num_of_pnts, 3))
for i in range(num_of_pol):
    ves_full[i*num_of_ves:(i+1)*num_of_ves] = ves

# loop over configurations #
num_of_configs = len(config_list)
min_dist = np.empty(num_of_configs)
iota = np.empty(num_of_configs)
for i, config_id in enumerate(config_list):
    print('{0}: ({1:0.0f}|{2:0.0f})'.format(config_id, i+1, num_of_configs))
    config_split = config_id.split('-')
    main_id = 'main_coil_{}'.format(config_split[0])
    set_id = 'set_{}'.format(config_split[1])
    job_id = 'job_{}'.format(config_split[2])
    if config_split[1] == '1' or config_split[1] == '3':
        wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_HSX_main_opt0.nc')
    else:
        wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, 'wout_HSX_aux_opt0.nc')

    wout = wr.readWout(wout_path)
    wout.transForm_2D_sSec(1, u_dom, np.array([0]), ['R', 'Z'])
    surf_x = wout.invFourAmps['R'][0]
    surf_y = np.zeros(u_dom.shape)
    surf_z = wout.invFourAmps['Z'][0]
    surf = np.stack((surf_x, surf_y, surf_z), axis=1)
    surf = np.tile(surf, num_of_ves).reshape(u_dom.shape[0]*num_of_ves, 3)
    min_dist[i] = np.min(np.linalg.norm(surf-ves_full, axis=1))
    iota[i] = wout.iota(1)

# save data #
save_path = os.path.join('/home', 'michael', 'Desktop', 'Dieter_min_volume_candidates.h5')
with hf.File(save_path, 'w') as hf_:
    hf_.create_dataset('configuration IDs', data=config_list)
    hf_.create_dataset('minimum distance', data=min_dist)
    hf_.create_dataset('LCFS iota', data=iota)
