import os, sys
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt

ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)
import vmecTools.wout_files.wout_read as wr

def get_minimum(wout):
    R_wout = wout.invFourAmps['R'] - wout.R_major
    Z_wout = wout.invFourAmps['Z']
    B_wout = wout.invFourAmps['Bmod']

    R_min = np.empty(R_wout.shape[0])
    Z_min = np.empty(R_wout.shape[0])
    B_min = np.empty(R_wout.shape[0])

    idx_min = np.argmin(B_wout, axis=1)
    for i, mdx in enumerate(idx_min):
        R_min[i] = R_wout[i,mdx]
        Z_min[i] = Z_wout[i,mdx]
        B_min[i] = B_wout[i,mdx]

    return R_min, Z_min, B_min

def get_minimal_set(wout, thresh):
    R_wout = wout.invFourAmps['R'] - wout.R_major
    Z_wout = wout.invFourAmps['Z']
    B_wout = wout.invFourAmps['Bmod']

    min_arr, min_key = {}, []
    idx_min = np.argmin(B_wout, axis=1)
    for i, mdx in enumerate(idx_min):
        key = '{}'.format(i)
        idx = np.where(B_wout[i] <= (1+thresh)*B_wout[i,mdx])[0]
        min_key.append(key)
        min_arr[key] = np.stack((R_wout[i,idx], Z_wout[i,idx], B_wout[i,idx]), axis=1)

    return min_arr, min_key
        

def plotting(fontsize=14, labelsize=16, linewidth=2):
    plt.close('all')

    font = {'family': 'sans-serif',
            'serif': 'Times New Roman',
            'weight': 'normal',
            'size': fontsize}

    mpl.rc('font', **font)

    mpl.rcParams['axes.labelsize'] = labelsize
    mpl.rcParams['lines.linewidth'] = linewidth

    fig, ax = plt.subplots(1, 1, tight_layout=True)
    return fig, ax


# import equilibrium #
wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
wout = wr.readWout(wout_path)

pol_points = 101
tor_points = pol_points

pol_dom = np.linspace(np.pi, np.pi, pol_points)
tor_dom = np.linspace(0, 0.25*np.pi, tor_points)

amp_keys = ['R', 'Z', 'Bmod']
wout.transForm_2D_sSec(0.5, pol_dom, tor_dom, amp_keys)
R_min, Z_min, B_min = get_minimum(wout)
# Arr, key = get_minimal_set(wout, 0.01)

# plotting #
fig, ax1 = plotting()
ax2 = ax1.twinx()

plt1, = ax1.plot(tor_dom/np.pi, R_min, c='tab:blue', label='R')
plt2, = ax1.plot(tor_dom/np.pi, Z_min, c='tab:orange', label='Z')
# plt3, = ax2.plot(tor_dom/np.pi, B_min, c='tab:green', label='B')
plt3, = ax2.plot(tor_dom/np.pi, np.arctan2(Z_min, R_min)/np.pi, c='tab:green', label='ArcTan')

if False:
    for i, tor in enumerate(tor_dom):
        arr = Arr[key[i]]
        cnt = arr.shape[0]
        Rarr = arr[:,0]
        Zarr = arr[:,1]
        Barr = arr[:,2]

        ax1.scatter([tor/np.pi]*cnt, Rarr, c=plt1.get_color(), s=10)
        ax1.scatter([tor/np.pi]*cnt, Zarr, c=plt2.get_color(), s=10)
        ax2.scatter([tor/np.pi]*cnt, Barr, c=plt3.get_color(), s=10)

ax1.set_xlabel(r'$\varphi/\pi$')
ax2.set_ylabel(r'$B/T$', c=plt3.get_color())

ax1.set_xlim(tor_dom[0]/np.pi, tor_dom[-1]/np.pi)

ax1.legend(frameon=False)

ax1.grid()
plt.show()
