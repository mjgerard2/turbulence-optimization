import os
import pandas as pd
import h5py as hf
import numpy as np

from multiprocessing import Process, Queue, cpu_count

import sys
ModDir = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDir)

import metricTools.equilibrium_shapes as es
import vmecTools.wout_files.wout_read as wr


def job_loop(in_que, out_que, resolution):

    while True:

        item = in_que.get()
        if item is None:
            break
        else:
            config_id, cnt, tot, run_dict = item

            # unpack dictionary #
            tpts = run_dict['toroidal points']
            norms = run_dict['normalizations']

            # instantiate wout object #
            mID, sID, jID = config_id.split('-')
            main_id = 'main_coil_%s' % mID
            set_id = 'set_%s' % sID
            job_id = 'job_%s' % jID
            if resolution == 'low':
                wout_name = 'wout_HSX_main_opt0.nc'
            elif resolution == 'high':
                wout_name = 'wout_%s_mn1824_ns101.nc' % config_id
            else:
                raise KeyError(resolution+' is not a valide resolution.')
            wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, wout_name)
            wout = wr.readWout(wout_path)
            s_val = 0.5*((norms[-1]/wout.a_minor)**2)

            # calculate shaping parameters
            shape = es.shape_calculator(wout, s_val, tor_pnts)
            shape.calc_shaping_parameters()
            kappa = shape.kappa_avg/norms[0]
            delta = shape.delta_avg/norms[1]
            zeta = shape.zeta_avg/norms[2]

            # output results #
            out_que.put([config_id, cnt, tot, np.array([kappa, delta, zeta])])

def save_loop(in_que, save_file):
    while True:
        item = in_que.get()
        if item is None:
            break
        else:
            config_id, cnt, tot, data = item
            print('=====================\n({0}|{1}): {2}\n=====================\n'.format(cnt, tot, config_id))
            # save_file = os.path.join('/home', 'michael', 'Desktop', 'calc_metrics', 'data_files', 'ML_data_prep.h5')
            with hf.File(save_file, 'a') as hf_:
                hf_.create_dataset(config_id, data=data)


# Number of CPUs to use #
num_of_save_cpus = 1
num_of_calc_cpus = 4 # cpu_count()-num_of_save_cpus-1
resolution = 'high'

# define computational grid #
tor_pnts = 101

if resolution == 'low':
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_HSX_main_opt0.nc')
elif resolution == 'high':
    wout_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
else:
    raise KeyError(resolution+' is not a valide resolution.')

wout = wr.readWout(wout_path)
shape = es.shape_calculator(wout, 0.5, tor_pnts)
shape.calc_shaping_parameters()

kappa = np.abs(shape.kappa_avg)
delta = np.abs(shape.delta_avg)
zeta = np.abs(shape.zeta_avg)

# metric dictionary #
run_dict = {'toroidal points': tor_pnts,
            'normalizations': np.array([kappa, delta, zeta, wout.a_minor])}

# generate configuration list #
if resolution == 'low':
    data_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'HSX_coil_current_data_mn88.h5')
elif resolution == 'high':
    data_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'HSX_coil_current_data_mn1824.h5')
else:
    raise KeyError(resolution+' is not a valide resolution.')
df = pd.read_hdf(data_path, key='DataFrame')
config_list = df.drop('0-1-0').index.values

# Check Saved Data #
saved_data = {}
if resolution == 'low':
    save_file = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'equilibrium_shapes_mn88_tor{}.h5'.format(tor_pnts))
elif resolution == 'high':
    save_file = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'equilibrium_shapes_mn1824_tor{}.h5'.format(tor_pnts))
else:
    raise KeyError(resolution+' is not a valide resolution.')
if os.path.isfile(save_file):
    with hf.File(save_file, 'r') as hf_:
        for key in hf_:
            saved_data[key] = hf_[key][()]

# populate queue #
in_que = Queue()
out_que = Queue()

for cdx, config_id in enumerate(config_list):
    cnt = cdx+1
    tot = len(config_list)
    if not config_id in saved_data:
        in_que.put([config_id, cnt, tot, run_dict])

# start metric calculation processes #
work_procs = []
for j in range(num_of_calc_cpus):
    in_que.put(None)
    proc = Process(target=job_loop, args=(in_que, out_que, resolution,))
    proc.start()
    work_procs.append(proc)

# print('start saver')
for k in range(num_of_save_cpus):
    save_proc = Process(target=save_loop, args=(out_que, save_file,))
    save_proc.start()

for k, proc in enumerate(work_procs):
    proc.join()

for k in range(num_of_save_cpus):
    out_que.put(None)
