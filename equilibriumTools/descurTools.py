# -*- coding: utf-8 -*-
"""
Created on Sat Nov 14 19:53:36 2020

@author: micha
"""

import numpy as np
import os, sys
WORKDIR = os.path.join('/home','michael','Desktop','python_repos','turbulence-optimization')
sys.path.append(WORKDIR)

# import directory_dict as dd
import plot_define as pd

nfp = 4
path = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec', 'outcurve')

u_pts = 50
v_pts = 10

u_dom = np.linspace(0, 2*np.pi, u_pts)
v_dom = np.linspace(0, np.pi/4, v_pts)

fourMode = []
fourAmps = []

# read DESCUR output #
with open(path, 'r') as file:
    lines = file.readlines()

    for line in lines:
        line = line.split()
        if len(line) > 0 and line[0][0:3] == 'RBC':
            if len(line) == 16:
                RBC = [int(amp) for amp in [line[i+1] for i, iline in enumerate(line) if iline == 'RBC('][0][0:-1].split(',')]
                RBS = [int(amp) for amp in [line[i+1] for i, iline in enumerate(line) if iline == 'RBS('][0][0:-1].split(',')]
                ZBC = [int(amp) for amp in [line[i+1] for i, iline in enumerate(line) if iline == 'ZBC('][0][0:-1].split(',')]
                ZBS = [int(amp) for amp in [line[i+1] for i, iline in enumerate(line) if iline == 'ZBS('][0][0:-1].split(',')]
                fourAmps.append([float(line[3]), float(line[7]), float(line[11]), float(line[15])])
                fourMode.append([RBC, RBS, ZBC, ZBS])
            elif len(line) == 12:
                RBC = [int(amp) for amp in [line[i+1] for i, iline in enumerate(line) if iline == 'RBC('][0][0:-1].split(',')]
                RBS = [int(amp) for amp in [line[i+1] for i, iline in enumerate(line) if iline == 'RBS('][0][0:-1].split(',')]
                ZBC = [int(amp) for amp in [line[i+1] for i, iline in enumerate(line) if iline == 'ZBC('][0][0:-1].split(',')]
                ZBS = [int(amp) for amp in [line[i+1] for i, iline in enumerate(line) if iline == 'ZBS('][0][0:-1].split(',')]
                fourAmps.append([float(line[2]), float(line[5]), float(line[8]), float(line[11])])
                fourMode.append([RBC, RBS, ZBC, ZBS])

fourMode = np.array(fourMode)
fourAmps = np.array(fourAmps)

R_vals = np.zeros((v_pts, u_pts))
Z_vals = np.zeros((v_pts, u_pts))
for v_idx, v_val in enumerate(v_dom):
    print('{0}-{1}'.format(v_idx+1, v_pts))
    for u_idx, u_val in enumerate(u_dom):
        r_vals = np.zeros(fourAmps.shape[0])
        z_vals = np.zeros(fourAmps.shape[0])
        for i, amp in enumerate(fourAmps):
            mode = fourMode[i]
            r_vals[i] = amp[0] * np.cos(mode[0,1]*u_val-4*mode[0,0]*v_val) + amp[1]*np.sin(mode[1,1]*u_val-4*mode[1,0]*v_val)
            z_vals[i] = amp[2] * np.cos(mode[2,1]*u_val-4*mode[2,0]*v_val) + amp[3]*np.sin(mode[3,1]*u_val-4*mode[3,0]*v_val)
        R_vals[v_idx, u_idx] = np.sum(r_vals)
        Z_vals[v_idx, u_idx] = np.sum(z_vals)

X_vals = np.empty(R_vals.shape)
Y_vals = np.empty(R_vals.shape)
for v_idx, v in enumerate(v_dom):
    X_vals[v_idx] = R_vals[v_idx] * np.cos(v)
    Y_vals[v_idx] = R_vals[v_idx] * np.sin(v)

if True:
    # 3D plot #
    plot = pd.plot_define()
    fig, ax = plot.construct_3D_axis()
    for i in range(v_pts):
        ax.plot(X_vals[i], Y_vals[i], Z_vals[i])
    plot.plt.show()

if False:
    # 2D plot #
    plot = pd.plot_define(lineWidth=2)
    fig, ax = plot.construct_axis()
    ax.set_aspect('equal')
    for i in range(v_pts):
        ax.plot(R_vals[i], Z_vals[i])
    plot.plt.show()
