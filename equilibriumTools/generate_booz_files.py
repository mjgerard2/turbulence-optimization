import os
import h5py as hf
import numpy as np

from multiprocessing import Process, Queue, cpu_count

def gen_booz_no_multiprocessing(config_list):
    tot = len(config_list)
    home_dir = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'python_scripts')
    for cnt, config_id in enumerate(config_list):
        print('=====================\n({}|{}): {}\n=====================\n'.format(cnt+1, tot, config_id))

        # change working directory #
        main_id = 'main_coil_%s' % config_id.split('-')[0]
        set_id = 'set_%s' % config_id.split('-')[1]
        job_id = 'job_%s' % config_id.split('-')[2]
        os.chdir(os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id))

        # write booz_input #
        booz_input = os.path.join(os.getcwd(), 'xbooz_input')
        with open(booz_input, 'w') as f:
            f.write('72 96\n')
            f.write('wout_%s_mn1824_ns101\n' % config_id)
            f.write(' '.join(['{}'.format(int(x)) for x in np.arange(101)]))

        # run xbooz_xform #
        xbooz_path = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec', 'xbooz_xform')
        # os.system('%s xbooz_input' % xbooz_path)
        # os.system('rm sum_gmncb.txt xbooz_input')
        os.system('%s xbooz_input > temp.txt' % xbooz_path)
        os.system('rm sum_gmncb.txt xbooz_input temp.txt')
        os.chdir(home_dir)

def gen_booz(in_que):
    home_dir = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'python_scripts')
    while True:
        config_id, cnt, tot = in_que.get()
        if config_id == 'Done':
            break
        else:
            print('=====================\n({}|{}): {}\n=====================\n'.format(cnt, tot, config_id))

            # change working directory #
            main_id = 'main_coil_%s' % config_id.split('-')[0]
            set_id = 'set_%s' % config_id.split('-')[1]
            job_id = 'job_%s' % config_id.split('-')[2]
            os.chdir(os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id))

            # write booz_input #
            booz_input = os.path.join(os.getcwd(), 'xbooz_input')
            with open(booz_input, 'w') as f:
                f.write('72 96\n')
                f.write('wout_%s_mn1824_ns101\n' % config_id)
                f.write(' '.join(['{}'.format(int(x)) for x in np.arange(101)]))

            # run xbooz_xform #
            xbooz_path = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec', 'xbooz_xform')
            # os.system('%s xbooz_input' % xbooz_path)
            # os.system('rm sum_gmncb.txt xbooz_input')
            os.system('%s xbooz_input > temp.txt' % xbooz_path)
            os.system('rm sum_gmncb.txt xbooz_input temp.txt')
            os.chdir(home_dir)


if __name__ == "__main__":
    num_of_cpus = 4
    
    """
    # import selection data #
    sel_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'ae-selections-final.h5')
    with hf.File(sel_path, 'r') as hf_:
        config_check = []
        for data in hf_['negative right'][()]:
            config_check.append('-'.join(['{}'.format(int(x)) for x in data[0:3]]))
        for data in hf_['negative left'][()]:
            config_check.append('-'.join(['{}'.format(int(x)) for x in data[0:3]]))
        for data in hf_['positive right'][()]:
            config_check.append('-'.join(['{}'.format(int(x)) for x in data[0:3]]))
        for data in hf_['positive left'][()]:
            config_check.append('-'.join(['{}'.format(int(x)) for x in data[0:3]]))

    # check for existing data #
    config_list = []
    # in_que = Queue()
    for cnt, config_id in enumerate(config_check):
        main_id = 'main_coil_%s' % config_id.split('-')[0]
        set_id = 'set_%s' % config_id.split('-')[1]
        job_id = 'job_%s' % config_id.split('-')[2]

        file_name = 'boozmn_wout_%s_mn1824_ns101.nc' % config_id
        check_file = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id, file_name)
        if os.path.isfile(check_file):
            continue
        else:
            config_list.append(config_id)
            # in_que.put([config_id, cnt+1, len(config_check)])
    """
    config_list = ['60-1-84']
    gen_booz_no_multiprocessing(config_list)
    """
    # start metric calculation processes #
    work_procs = []
    for j in range(num_of_cpus):
        in_que.put('Done')
        proc = Process(target=gen_booz, args=(in_que,))
        proc.start()
        work_procs.append(proc)

    # wait for processes to finish #
    for k, proc in enumerate(work_procs):
        proc.join()
    """
