import os
import shlex
import subprocess
import h5py as hf
import numpy as np

from netCDF4 import Dataset

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import vmecTools.wout_files.wout_read as wr


# import selection data #
config_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'linear_data', 'config_list.txt')
with open(config_path, 'r') as f:
    config_list = ['QHS']
    for line in f.readlines():
        config_list.append(line.strip())

# read in QHS minor radius #
qhs_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'main_coil_0', 'set_1', 'job_0', 'wout_QHS_mn1824_ns101.nc')
rootgrp = Dataset(qhs_path, 'r')
minor_qhs = rootgrp['/Aminor_p'][:]
rootgrp.close()

# define gist template and executable files #
gist_exec_temp = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'GIST', 'gist_template.jl')
gist_exec = os.path.join('/home', 'michael', 'Desktop', 'julia_tools', 'GIST', 'gist_temp.jl')

# define gist parameters #
alf0 = 0.0
npol = 16
nz = 16384

# loop over selected configurations #
for cnt, config_id in enumerate(config_list):
    if config_id == 'QHS':
        main_id = 'main_coil_0'
        set_id = 'set_1'
        job_id = 'job_0'
    else:
        main_id = 'main_coil_{}'.format(config_id.split('-')[0])
        set_id = 'set_{}'.format(config_id.split('-')[1])
        job_id = 'job_{}'.format(config_id.split('-')[2])

    wout_dirc = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id) 
    gist_dirc = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', main_id, set_id, job_id)
    
    if config_id == '0-1-0':
        wout_path = os.path.join(wout_dirc, 'wout_QHS_mn1824_ns101.nc')
        # wout_path = os.path.join(wout_dirc, 'wout_HSX_main_opt0.nc')
    else:
        wout_path = os.path.join(wout_dirc, 'wout_%s_mn1824_ns101.nc' % config_id)
        # wout_path = os.path.join(wout_dirc, 'wout_HSX_main_opt0.nc')
    
    wout = wr.readWout(wout_path)
    s = 0.5*((minor_qhs/wout.a_minor)**2)
    
    gist_name = 'gist_%s_mn1824_alf0_s%s_res%0.0f_npol%0.0f' % (config_id, '{0:0.2f}'.format(s).replace('.', 'p'), nz/npol, npol)
    gist_path = os.path.join(gist_dirc, gist_name)
    gist_path_chk = os.path.join(gist_dirc, gist_name+'.dat')
    if not os.path.isfile(gist_path_chk):
        print('({}|{}): {}'.format(cnt+1, len(config_list), config_id))

        wout_string = '\/'.join([x for x in wout_path.split('/')])
        gist_string = '\/'.join([x for x in gist_path.split('/')])
        
        subprocess.run(shlex.split('sed \'s/<wout_path>/%s/\' %s' % (wout_string, gist_exec_temp)), stdout=open(gist_exec, 'w'))
        subprocess.run(shlex.split('sed -i \'s/<file_path>/%s/\' %s' % (gist_string, gist_exec)))
        subprocess.run(shlex.split('sed -i \'s/<s>/%f/\' %s' % (s, gist_exec)))
        subprocess.run(shlex.split('sed -i \'s/<alpha>/%0.1f/\' %s' % (alf0, gist_exec)))
        subprocess.run(shlex.split('sed -i \'s/<npol>/%0.0f/\' %s' % (npol, gist_exec)))
        subprocess.run(shlex.split('sed -i \'s/<nz>/%0.0f/\' %s' % (nz, gist_exec)))
        subprocess.run(shlex.split('julia %s' % gist_exec))
        subprocess.run(shlex.split('rm %s' % gist_exec))
