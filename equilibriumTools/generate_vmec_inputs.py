import os
import numpy as np


def read_descur(descur_file):
    with open(descur_file, 'r') as f:
        lines = f.readlines()
        for ldx, line in enumerate(lines):
            line_split = line.strip().split()
            line_len = len(line_split)
            if line_len > 0:
                if line_split[0] == 'MB':
                    ldx_beg = ldx+1
                    break
        
        desc_dict = {'RAXIS': [],
                     'ZAXIS': [],
                     'MB': [],
                     'NB': [],
                     'RBC': [],
                     'RBS': [],
                     'ZBC': [],
                     'ZBS': []}

        for ldx, line in enumerate(lines[ldx_beg::]):
            line_split = line.strip().split()
            line_len = len(line_split)
            if len(line_split) != 8:
                desc_dict['MB'].append(int(line_split[0]))
                desc_dict['NB'].append(int(line_split[1]))
                desc_dict['RBC'].append(float(line_split[2]))
                desc_dict['RBS'].append(float(line_split[3]))
                desc_dict['ZBC'].append(float(line_split[4]))
                desc_dict['ZBS'].append(float(line_split[5]))
                ldx_beg = ldx_beg + ldx + 1
                break
            else:
                desc_dict['MB'].append(int(line_split[0]))
                desc_dict['NB'].append(int(line_split[1]))
                desc_dict['RBC'].append(float(line_split[2]))
                desc_dict['RBS'].append(float(line_split[3]))
                desc_dict['ZBC'].append(float(line_split[4]))
                desc_dict['ZBS'].append(float(line_split[5]))
                desc_dict['RAXIS'].append(float(line_split[6]))
                desc_dict['ZAXIS'].append(float(line_split[7]))

        for ldx, line in enumerate(lines[ldx_beg::]):
            line_split = line.strip().split()
            line_len = len(line_split)
            if len(line_split) != 6:
                break
            else:
                desc_dict['MB'].append(int(line_split[0]))
                desc_dict['NB'].append(int(line_split[1]))
                desc_dict['RBC'].append(float(line_split[2]))
                desc_dict['RBS'].append(float(line_split[3]))
                desc_dict['ZBC'].append(float(line_split[4]))
                desc_dict['ZBS'].append(float(line_split[5]))

    for key in desc_dict:
        desc_dict[key] = np.array(desc_dict[key])

    return desc_dict

def generate_vmec_input(desc_dict, vmec_output):
    # vmec_template = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'vmec_inputs', 'input.QHS_best')
    vmec_template = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'input.QHS_best')
    with open(vmec_template, 'r') as f:
        vmec_temp = f.readlines()
        vmec_temp = vmec_temp[0:23]

    with open(vmec_output, 'w') as f:
        for line in vmec_temp:
            f.write(line)

        f.write('  RAXIS= '+' '.join(['{}'.format(x) for x in desc_dict['RAXIS']])+'\n')
        f.write('  ZAXIS= '+' '.join(['{}'.format(x) for x in desc_dict['ZAXIS']])+'\n')
        
        md = desc_dict['MB'].shape[0]
        for i in range(md):
            MB = desc_dict['MB'][i]
            NB = desc_dict['NB'][i]
            RBC = desc_dict['RBC'][i]
            ZBS = desc_dict['ZBS'][i]
            f.write('  RBC({0:0.0f}, {1:0.0f}) = {2}  RBS({0:0.0f}, {1:0.0f}) = 0.000000E+00  ZBC({0:0.0f}, {1:0.0f}) = 0.000000E+00  ZBS({0:0.0f}, {1:0.0f}) = {3}\n'.format(NB, MB, RBC, ZBS))

        f.write('/\n')

# read in VMEC template #
# vmec_temp_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'vmec_inputs', 'input.QHS_best')
vmec_temp_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'input.QHS_best')
with open(vmec_temp_path, 'r') as f:
    vmec_temp = f.readlines()
    vmec_temp = vmec_temp[0:23]

# read in DESCUR file names #
# descur_dirc = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'descur_outputs')
# descur_names = [f.name for f in os.scandir(descur_dirc)]
descur_dirc = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec')
descur_names = ['outcurve']

# loop over DESCUR files #
for cnt, name in enumerate(descur_names):
    # config_id = name.split('.')[0]
    # vmec_output = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'vmec_inputs', 'input.%s_mn1824_ns101' % config_id)
    vmec_output = os.path.join('/home', 'michael', 'Desktop', 'fortran_repos', 'Stellarator-Tools', 'Stell-Exec', 'input.0-3-84_9p0_mn1824_ns101')
    if os.path.isfile(vmec_output):
        continue
    print('({}|{})'.format(cnt+1, len(descur_names)))

    # read in DESCUR data #
    descur_file = os.path.join(descur_dirc, name)
    desc_dict = read_descur(descur_file)
    generate_vmec_input(desc_dict, vmec_output)
