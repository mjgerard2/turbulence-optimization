import os
import numpy as np

import sys
ModDirc = os.path.join('/home', 'michael', 'Desktop', 'python_repos', 'turbulence-optimization', 'pythonTools')
sys.path.append(ModDirc)

import vmecTools.wout_files.wout_read as wr


# read in QHS phi edge #
qhs_input = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data', 'input.QHS_best')
with open(qhs_input, 'r') as f:
    input_lines = f.readlines()
    phi_edge_qhs = float(input_lines[15].strip().split()[2])

# read in QHS minor radius #
qhs_path = os.path.join('/mnt', 'HSX_Database', 'HSX_Configs', 'coil_data')
wout = wr.readWout(qhs_path, name='wout_QHS_best.nc')
qhs_minor = wout.a_minor

# calculate average |B| on desired flux surface #
s_qhs = 0.5
pol_dom = np.linspace(-np.pi, np.pi, 51)
tor_dom = np.linspace(-np.pi, np.pi, (pol_dom.shape[0]-1)*4+1)
wout.transForm_2D_sSec(s_qhs, pol_dom, tor_dom, ['Bmod', 'Jacobian'])

Bmod = wout.invFourAmps['Bmod']
jacob = wout.invFourAmps['Jacobian']
B_norm = np.trapz(np.trapz(jacob, pol_dom, axis=1), tor_dom)
B_qhs = np.trapz(np.trapz(Bmod*jacob, pol_dom, axis=1), tor_dom) / B_norm

# read in VMEC inputs #
in_dirc = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'vmec_inputs')
in_names = [f.name for f in os.scandir(in_dirc) if f.name.split('.')[1] != 'QHS_best']

# read in VMEC ouputs #
out_dirc = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'vmec_outputs')
out_dirc_abv = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'vmec_outputs_above_s1p0')
out_names = [f.name for f in os.scandir(in_dirc)]

# read in failed configurations #
failed_path = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'wout_failures.txt')
failed_configs = []
if os.path.isfile(failed_path):
    with open(failed_path, 'r') as f:
        lines = f.readlines()
        for line in lines:
            failed_configs.append(line.strip())

# loop over uncompleted VMEC inputs #
cwd = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'python_scripts')
for cnt, name in enumerate(in_names):
    config_id = name.split('.')[1].split('_')[0]
    if any([icon == config_id for icon in failed_configs]):
        continue

    tag = name.split('.')[1]
    rm_files = ['jxbout_%s.nc' % tag, 'mercier.%s' % tag, 'threed1.%s' % tag, 'parvmecinfo.txt', 'timings.txt']
    mv_file = os.path.join(cwd, 'wout_%s.nc' % tag)
    wout_file = os.path.join(out_dirc, 'wout_%s.nc' % tag)
    if os.path.isfile(wout_file):
        continue

    print('\nWORKING {} of {}\n'.format(cnt+1, len(in_names)))

    # perform inital VMEC run #
    input_file = os.path.join(in_dirc, name)
    os.system('mpiexec -n 8 ~/Desktop/fortran_repos/Stellarator-Tools/Stell-Exec/xvmec %s' % input_file)

    # identify desired wout surface #
    try:
        wout = wr.readWout(cwd, name='wout_%s.nc' % tag)
    except:
        with open(failed_path, 'a') as f:
            f.write('%s\n' % config_id)
        if os.path.isfile(mv_file):
            os.system('rm %s' % mv_file)
        for file in rm_files:
            rm_file = os.path.join(cwd, file)
            if os.path.isfile(rm_file):
                os.system('rm %s' % rm_file)
        continue
    s_new = ((qhs_minor/wout.a_minor)**2) * s_qhs

    if s_new <= 1.0:
        wout.transForm_2D_sSec(s_new, pol_dom, tor_dom, ['Bmod', 'Jacobian'])
        Bmod = wout.invFourAmps['Bmod']
        jacob = wout.invFourAmps['Jacobian']
        B_norm = np.trapz(np.trapz(jacob, pol_dom, axis=1), tor_dom)
        B_chk = np.trapz(np.trapz(Bmod*jacob, pol_dom, axis=1), tor_dom) / B_norm
        B_diff = np.abs(2*(B_qhs - B_chk)/(B_qhs + B_chk))
        print('\n<B> percent difference is {0:0.4f}\n'.format(B_diff))
        while B_diff >= 0.01:
            # read in edge flux #
            phi_edge_new = (B_qhs/B_chk) * phi_edge_qhs
            with open(input_file, 'r') as f:
                input_lines = f.readlines()
                input_lines[15] = '  PHIEDGE = {}\n'.format(phi_edge_new)
            with open(input_file, 'w') as f:
                for line in input_lines:
                    f.write(line)
            os.system('mpiexec -n 8 ~/Desktop/fortran_repos/Stellarator-Tools/Stell-Exec/xvmec %s' % input_file)
            try:
                wout = wr.readWout(cwd, name='wout_%s.nc' % tag)
            except:
                with open(failed_path, 'a') as f:
                    f.write('%s\n' % config_id)
                if os.path.isfile(mv_file):
                    os.system('rm %s' % mv_file)
                for file in rm_files:
                    rm_file = os.path.join(cwd, file)
                    if os.path.isfile(rm_file):
                        os.system('rm %s' % rm_file)
                continue
            wout.transForm_2D_sSec(s_new, pol_dom, tor_dom, ['Bmod', 'Jacobian'])
            Bmod = wout.invFourAmps['Bmod']
            jacob = wout.invFourAmps['Jacobian']
            B_norm = np.trapz(np.trapz(jacob, pol_dom, axis=1), tor_dom)
            B_chk = np.trapz(np.trapz(Bmod*jacob, pol_dom, axis=1), tor_dom) / B_norm
            B_diff = np.abs(2*(B_qhs - B_chk)/(B_qhs + B_chk))
            print('\n<B> percent difference is {0:0.4f}\n'.format(B_diff))

        for file in rm_files:
            rm_file = os.path.join(cwd, file)
            if os.path.isfile(rm_file):
                os.system('rm %s' % rm_file)

        if os.path.isfile(mv_file):
            os.system('mv %s %s' % (mv_file, out_dirc))

    else:
        # with open(failed_path, 'a') as f:
        #     f.write('%s\n' % config_id)
        if os.path.isfile(mv_file):
            os.system('mv %s %s' % (mv_file, out_dirc_abv))
        for file in rm_files:
            rm_file = os.path.join(cwd, file)
            if os.path.isfile(rm_file):
                os.system('rm %s' % rm_file)
