import os
import h5py as hf
import numpy as np

# read inputs #
in_dirc = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'descur_inputs')
in_names = [f.name for f in os.scandir(in_dirc) if f.name.endswith('.txt')]

os.chdir(in_dirc)

# read outputs #
out_dirc = os.path.join('/mnt', 'HSX_Database', 'AE_sample', 'descur_outputs')
out_names = [f.name for f in os.scandir(out_dirc)]

# loop over inputs #
for cnt, name in enumerate(in_names):
    if any(iname == name for iname in out_names):
        continue

    out_path = os.path.join(out_dirc, name)
    print('\n\nFile path: %s\n' % name)
    os.system('/home/michael/Desktop/fortran_repos/Stellarator-Tools/Stell-Exec/xdescur')
    os.system('mv outcurve %s' % out_path)
    input('ENTER to continue...')
