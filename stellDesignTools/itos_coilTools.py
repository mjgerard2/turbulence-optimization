# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 12:04:00 2020

@author: micha
"""

import numpy as np

import os, sys
path = os.path.dirname(os.path.abspath(os.getcwd()))
sys.path.append(path)

import directory_dict as dd
from vtkTools import vtk_grids

def round_up(num, dec):
    factor = 10 ** dec
    return np.ceil(num * factor) / factor

def round_down(num, dec):
    factor = 10 ** dec
    return np.floor(num * factor) / factor

class itos_coils():
    
    def __init__(self, params, mod_params=None):
        self.params = params
        
        if mod_params:
            self.mods = True
            self.mod_params = mod_params
        else:
            self.mods = False
            
    def r_func(self, tor, params):
        n = params['N']
        ro = params['R_0']
        eps_r = params['epsilon r']
        alf_r = params['alpha r']

        return ro + ro * eps_r * np.cos(n * tor + alf_r * np.sin(n * tor))
    
    def z_func(self, tor, params):
        n = params['N']
        ro = params['R_0']
        eps_z = params['epsilon z']
        alf_z = params['alpha z']

        return ro * eps_z * np.sin(n * tor + alf_z * np.sin(n * tor))
        
    def generate_coil_file(self):
        ntor = self.params['toroidal points']
        num_of_hcoils = self.params['helical coils']
        num_of_pcoils = self.params['poloidal coils']
        
        tor_dom = np.linspace(0, 2*np.pi, ntor)
        
        if self.mods == True:
            num_of_mcoils = self.mod_params['planar coils']
            self.num_of_coils = num_of_hcoils + num_of_pcoils + num_of_mcoils
            
        else:
            self.num_of_coils = num_of_hcoils + num_of_pcoils            
        
        r_dom = np.empty((self.num_of_coils, ntor))
        
        self.x_dom = np.empty((self.num_of_coils, ntor))
        self.y_dom = np.empty((self.num_of_coils, ntor))
        self.z_dom = np.empty((self.num_of_coils, ntor))
        
        for i in range(num_of_hcoils):
            params = self.params['helical params {}'.format(i+1)]
            
            r_dom[i] = self.r_func(tor_dom, params)
            self.z_dom[i] = self.z_func(tor_dom, params)
        
        for j in range(num_of_pcoils):
            idx = j + num_of_hcoils

            r_dom[idx][:] = self.params['poloidal r'][j]
            self.z_dom[idx][:] = self.params['poloidal z'][j]
        
        self.x_dom = r_dom * np.cos(tor_dom)
        self.y_dom = r_dom * np.sin(tor_dom)
            
        if self.mods:
            r_maj = self.mod_params['major r']
            r_min = self.mod_params['minor r']
            
            theta_beg = self.mod_params['planar begin']
            theta_end = self.mod_params['planar end']
            theta_dom = np.linspace(theta_beg, theta_end, num_of_mcoils)
            
            for k, theta in enumerate(theta_dom): 
                idx = k + num_of_hcoils + num_of_pcoils

                self.x_dom[idx] = (r_maj + r_min * np.cos(tor_dom)) * np.cos(theta)
                self.y_dom[idx] = (r_maj + r_min * np.cos(tor_dom)) * np.sin(theta)
                self.z_dom[idx] = r_min * np.sin(tor_dom) * np.sin(theta)  
                
        r_dom = np.hypot(self.x_dom, self.y_dom) 
        self.r_min, self.r_max = np.min(r_dom), np.max(r_dom)
        self.z_min, self.z_max = np.min(self.z_dom), np.max(self.z_dom)
        
    def plot_coils(self, ax):
        for i in range(self.num_of_coils):
            ax.plot(self.x_dom[i], self.y_dom[i], self.z_dom[i])
            
    def plot_coils_2DR(self, plot, scale):
        major = 0.244856
        minor = 0.07874
        
        r_dom_1 = np.hypot(self.x_dom[0], self.y_dom[0])
        r_dom_2 = np.hypot(self.x_dom[1], self.y_dom[1])
        
        t_dom = np.linspace(scale[0], scale[-1], r_dom_1.shape[0])
        
        plot.plt.plot(t_dom, r_dom_1, c='tab:blue', label='Helical 1')
        plot.plt.plot(t_dom, r_dom_2, c='tab:orange', label='Helical 2')
        
        r_min = major - minor
        r_max = major + minor
        
        plot.plt.plot(t_dom, [r_min]*t_dom.shape[0], linewidth=3, c='k')
        plot.plt.plot(t_dom, [r_max]*t_dom.shape[0], linewidth=3, c='k', label='Vessel')
        
        delta_1 = r_min - np.min(r_dom_1)
        delta_2 = np.max(r_dom_2) - r_max
        plot.plt.title(r'$(\delta_1, \delta_2)$ = ({0:0.2f}, {1:0.2f}) cm'.format(delta_1*1e2, delta_2*1e2))
        
        plot.plt.xlabel(r'$\theta$')
        plot.plt.ylabel(r'R [m]')
    
    def plot_coils_2DZ(self, plot, scale):
        minor = 0.07874
        
        t_dom = np.linspace(scale[0], scale[-1], self.z_dom.shape[1])
        
        plot.plt.plot(t_dom, self.z_dom[0], c='tab:blue', label='Helical 1')
        plot.plt.plot(t_dom, self.z_dom[1], c='tab:orange', label='Helical 2')
        
        z_min = -minor
        z_max = minor
        
        plot.plt.plot(t_dom, [z_min]*t_dom.shape[0], linewidth=3, c='k')
        plot.plt.plot(t_dom, [z_max]*t_dom.shape[0], linewidth=3, c='k', label='Vessel')
        
        delta_1 = z_min - np.min(self.z_dom[0])
        delta_2 = np.max(self.z_dom[1]) - z_max
        plot.plt.title(r'$(\delta_1, \delta_2)$ = ({0:0.2f}, {1:0.2f}) cm'.format(delta_1*1e2, delta_2*1e2))
        
        plot.plt.xlabel(r'$\theta$')
        plot.plt.ylabel(r'Z [m]')
        
        box = plot.ax.get_position()
        plot.ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        plot.plt.legend(loc='upper left', bbox_to_anchor=(1.05, 1))
                
    def save_coils(self, path, name):
        pathName = os.path.join(path, name)
        
        with open(pathName, 'w') as file:
            file.write('periods {}\n'.format(self.params['helical params 1']['N']) + 
                       'begin filament\n' + 
                       'mirror NUL\n')
            
            for i in range(self.num_of_coils):
                for j in range(self.params['toroidal points']):
                    file.write('\t {0:0.8f} \t {1:0.8f} \t {2:0.8f} \t -1\n'.format(self.x_dom[i,j], self.y_dom[i,j], self.z_dom[i,j]))
                file.write('\t {0:0.8f} \t {1:0.8f} \t {2:0.8f} \t 0 \t {3} MainCoil{3}\n'.format(self.x_dom[i,j], self.y_dom[i,j], self.z_dom[i,j], i+1))
            
            file.write('end')
            file.close()
            
    def save_coilsVTK(self, path, main_crnts=[1.,-.25,-.08,-.08,-.08,-.08], mod_crnts=[.05,.05,.05,.05]):
        hcoils_lim = self.params['helical coils']
        pcoils_lim = self.params['poloidal coils']
        
        cartGrid = np.stack((self.x_dom, self.y_dom, self.z_dom), axis=2)
        for i in range(self.num_of_coils):
            if i < hcoils_lim:
                scale = np.empty(cartGrid.shape[1])
                scale[:] = main_crnts[i]
                
                vtk_grids.scalar_line_mesh(path, 'HelCoils_{}.vtk'.format(i+1), cartGrid[i], scale)
                
            elif (i >= hcoils_lim) and (i < hcoils_lim +  pcoils_lim):
                scale = np.empty(cartGrid.shape[1])
                scale[:] = main_crnts[i]
                
                vtk_grids.scalar_line_mesh(path, 'PolCoils_{}.vtk'.format(int(i-hcoils_lim+1)), cartGrid[i], scale)
            elif (self.mods == True) and (i >= hcoils_lim +  pcoils_lim) and (i < self.num_of_coils):
                scale = np.empty(cartGrid.shape[1])
                scale[:] = mod_crnts[int(i-hcoils_lim-pcoils_lim)]
                
                vtk_grids.scalar_line_mesh(path, 'ModCoils_{}.vtk'.format(int(i-hcoils_lim-pcoils_lim+1)), cartGrid[i], scale)
            
    def save_coils_meta(self, path, name):
        pathName = os.path.join(path, name)
        with open(pathName, 'w') as file:
            file.write('Coils Parameters\n' +
                       '----------------\n\n')
            for key in self.params:
                if key == 'helical params 1' or key == 'helical params 2':
                    file.write(key+' : {\n')
                    for key_sub in self.params[key]:
                        file.write('\t'+key_sub+' : {}\n'.format(self.params[key][key_sub]))
                    file.write('\t}\n')
                else:
                    file.write(key+' : {}\n'.format(self.params[key]))
            
            xr_min, xr_max = round_down(self.r_min, 4), round_up(self.r_max, 4)
            xz_min, xz_max = round_down(self.z_min, 4), round_up(self.z_max, 4)
            
            file.write('\nXGrid Inputs\n'+
                       '------------\n\n'+
                       'Scaled OR Raw Currents : S\n'+
                       'Stelarator Symmetry : y\n'+
                       'r min : {}\n'.format(xr_min)+
                       'r max : {}\n'.format(xr_max)+
                       'z min : {}\n'.format(xz_min)+
                       'z max : {}\n'.format(xz_max)+
                       'toroidal planes/period : 90\n'+
                       'r mesh points : 121\n'+
                       'z mesh points : 121\n')


if __name__ == '__main__':
    N = 5
    
    R_o1 = 0.31
    eps_r1 = 0.55
    eps_z1 = 0.7
    alf_r1 = 0.75
    alf_z1 = 0.75
    
    R_o2 = 0.34
    eps_z2 = 0
    alf_z2 = 0
        
    params = {'helical coils' : 2,
              'poloidal coils' : 4,
              'toroidal points' : 360,
              
              'helical params 1' : {'N' : N,
                                    'R_0' : R_o1,
                                    'epsilon r' : eps_r1,
                                    'alpha r' : alf_r1,
                                    'epsilon z' : eps_z1,
                                    'alpha z' : alf_z1},
              
              'helical params 2' : {'N' : N,
                                    'R_0' : R_o2,
                                    'epsilon r' : 0,
                                    'alpha r' : 0,
                                    'epsilon z' : eps_z2,
                                    'alpha z' : alf_z2},
              
              'poloidal r' : np.array([.5, .5, 1.8, 1.8]),
              'poloidal z' : np.array([-.6, .6, -.8, .8])}
    
    path = os.path.join( dd.pathDrive, 'Stellarators', 'ITOS', 'single_helical_coil' )
    
    coils = itos_coils(params)
    coils.generate_coil_file()
    #coils.save_coilsVTK(path, main_crnts=[5.5,-1,0,0,0,0] )
    coils.save_coils(path, 'coils.single_helical_coil')
    coils.save_coils_meta(path, 'meta_coils.single_helical_coil')
    