# -*- coding: utf-8 -*-
"""
Created on Wed May 20 12:23:38 2020

@author: ethri
"""

import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import matplotlib.pyplot as plt

plt.close()

def Radfunc(N,R0,epsilonR,alphaR,phi):
    return R0*epsilonR*np.cos(N*phi+alphaR*np.sin(N*phi))+R0

def Zfunc(N,R0,epsilonZ,alphaZ,phi):
    return R0*epsilonZ*np.sin(N*phi+alphaZ*np.sin(N*phi))

fig = plt.figure()
ax = fig.gca(projection='3d')

#Yamaguchi parameters
N=4
R_0=(1.15,1.25)
epsilon_R=(.5,0)
alpha_R=(.6,0)
epsilon_Z=(.6,.4)
alpha_Z=(.6,.4)
Z_pol=(-.6,.6,-.8,.8)
R_pol=(.5,.5,1.8,1.8)

helic_coils=2
Poloidal_coils=4
phi_res=500

#Helicotor Parameters
Planar_num=4
Planar_Start=np.pi*1/4
Planar_End=np.pi*7/4
Rh_min=.1713 
Rh_maj=.25 

coilNum=helic_coils+Planar_num+Poloidal_coils

phi_dom = np.linspace(0,2*np.pi,phi_res)
coil_dom = np.linspace(1,coilNum,coilNum)
theta_dom=np.linspace(Planar_Start,Planar_End,Planar_num)
r_dom=np.empty((coilNum,phi_res))
z_dom=np.empty((coilNum,phi_res))

for t in range(helic_coils):
    r_dom[t]=Radfunc(N,R_0[t],epsilon_R[t],alpha_R[t],phi_dom)/3

for t in range(helic_coils):
    z_dom[t]=Zfunc(N,R_0[t],epsilon_Z[t],alpha_Z[t],phi_dom)/3

for t in range(Poloidal_coils):
    r_dom[t+helic_coils]=R_pol[t]/3
    
for i in range(Poloidal_coils):
    for t in range(phi_res):
        z_dom[i+helic_coils,t]=Z_pol[i]/3
        
for t in range(Planar_num):
    z_dom[t+coilNum-Planar_num]=Rh_min*np.sin(phi_dom)

for t in range(phi_res):
    x_dom=r_dom*np.cos(phi_dom)
    y_dom=r_dom*np.sin(phi_dom)

for t, theta in enumerate(theta_dom):
    x_dom[t+coilNum-Planar_num] = (Rh_maj+Rh_min*np.cos(phi_dom)) * np.cos(theta)
    y_dom[t+coilNum-Planar_num] = (Rh_maj+Rh_min*np.cos(phi_dom)) * np.sin(theta)
 
color=np.empty(coilNum,dtype='str') 

for t in range(helic_coils):
    color[t]=str('b')
for t in range(Poloidal_coils):
    color[t+helic_coils]=str('g')
for t in range(Planar_num):
    color[t+helic_coils+Poloidal_coils]=str('r')
    
for t in range(coilNum):
    x=np.transpose(x_dom[t])
    y=np.transpose(y_dom[t])
    z=np.transpose(z_dom[t])
    ax.plot(x,y,z,color[t])
ax.legend()
ax.set_xlabel('X axis [m]')
ax.set_ylabel('Y axis [m]')
ax.set_zlabel('Z axis [m]')

f=open("coils.itos_mod_wmain","w")
f.write('periods 4\n' + 
        'begin filament\n' + 
        'mirror NUL\n')

for i in range(coilNum):
    for j in range(phi_res):
        f.write('\t {0:0.8f} \t {1:0.8f} \t {2:0.8f} \t -1\n'.format(x_dom[i,j], y_dom[i,j], z_dom[i,j]))
    f.write('\t {0:0.8f} \t {1:0.8f} \t {2:0.8f} \t 0 \t {3} MainCoil{3}\n'.format(x_dom[i,j], y_dom[i,j], z_dom[i,j], i+1))

f.write('end')
f.close()

r_dom = np.hypot(x_dom, y_dom) 
print('R (min, max) : ({0}, {1})\nZ (min, max) : ({2}, {3})'.format(np.min(r_dom), np.max(r_dom), np.min(z_dom), np.max(z_dom)))
