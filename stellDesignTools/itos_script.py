# -*- coding: utf-8 -*-
"""
Created on Fri Jun 12 08:42:42 2020

@author: micha
"""

import numpy as np
import itos_coilTools

import os, sys
path = os.path.dirname(os.path.abspath(os.getcwd()))
sys.path.append(path)

import directory_dict as dirDict

### Vacuum Vessel ###
major = 0.244856
minor = 0.07874
aspect = major / minor

### Free Parameters ###
dist_1 = .075
dist_2 = .0625

del_1 = 12.7 * dist_1
del_2 = 12.7 * dist_2
'''
### Yamaguchi Scaling ###
rho_pts = 5
rho_base = (1.25 / 1.15)
rho_set = np.linspace(0.8 * rho_base, 1.2 * rho_base, rho_pts)
'''
### Base Parameters ###
N = 5

R_o1 = 0.31
R_o2 = 0.3369565217391305

eps_r1 = 0.5827466883084267
#eps_z1 = 0.6
#alf_r1 = 0.6
#alf_z1 = 0.6
alf_r1_set = np.linspace(0.55, 0.65, 11)

eps_z2 = 0.4
alf_z2 = 0.4

for a_idx, alf_r1 in enumerate(alf_r1_set):
    eps_z1 = alf_r1
    alf_z1 = alf_r1

    ### Yamaguchi Parameters ###
    params = {'helical coils' : 2,
              'poloidal coils' : 4,
              'toroidal points' : 360,
              
              'helical params 1' : {'N' : 5,
                                    'R_0' : R_o1,
                                    'epsilon r' : eps_r1,
                                    'alpha r' : alf_r1,
                                    'epsilon z' : eps_z1,
                                    'alpha z' : alf_z1},
              
              'helical params 2' : {'N' : 5,
                                    'R_0' : R_o2,
                                    'epsilon r' : 0,
                                    'alpha r' : 0,
                                    'epsilon z' : eps_z2,
                                    'alpha z' : alf_z2},
              
              'poloidal r' : np.array([.5, .5, 1.8, 1.8]),
              'poloidal z' : np.array([-.6, .6, -.8, .8])}
    
    ### Planar Coil Parameters
    mod_params = {'planar coils' : 4,
                  'planar begin' : .25 * np.pi,
                  'planar end' : 1.75 * np.pi,
                  'major r' : .25,
                  'minor r' : .1713}
    
    ### Instantiate/Generate coil data ###
    itos = itos_coilTools.itos_coils(params)
    itos.generate_coil_file()
    
    ### Save coil files ###  
    tag = 'YG_N5_alfR1_{0:0.3f}'.format(alf_r1)
    name = 'coils.itos_'+tag
    metaName = tag+'_meta.txt'
    
    savePath = os.path.join(dirDict.exp_local['ITOS'], 'configurations', '5-fold_configs', tag)
    if not os.path.isdir(savePath):
        os.mkdir(savePath)
     
    vtkPath = os.path.join(savePath, 'vtk_files')
    if not os.path.isdir(vtkPath):
        os.mkdir(vtkPath)
    
    itos.save_coils(savePath, name)
    itos.save_coils_meta(savePath, metaName)
    itos.save_coilsVTK(vtkPath, main_crnts=[1,-0.25,0,0,0,0])
